<!-- Vợt cầu lông-->
<?php
	global $cp_options;
	$catid = $this->cat_id;
	$cattitle = $this->cat_title;
	$sancau = $this->san_cau;
?>
<?php		
		query_posts(array( 'post_type' => APP_POST_TYPE, 'posts_per_page' => 5,'post_status' => 'publish', "orderby" => "date",
			'tax_query' => 
				array( 
					array( 
					'taxonomy' => 'ad_cat',
					'field' => 'term_id',
					'terms' => array($catid)
					) 
				)  
			));
		if ( have_posts() ) : ?>
<div id="ctl40_HeaderContainer" class="tit_l">
	  <h2><a><span style="white-space:nowrap;"><?php echo $cattitle;?></span></a></h2>
</div>
<div style="clear: both;"></div>
<div class="line_gr"></div>
<div class="product-list">
	

		<?php  $counter = 0; ?>
		<?php while ( have_posts() ) : the_post(); ?>
	<div class="vip5">
		<div class="item">
			<div class="p-main">
				<div class="p-main-image-crop">
					<a class="product-avatar" href="<?php the_permalink(); ?>">
						<?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?>
					</a>
				</div>
				<div class="p-content">
					<div class="p-title">
						<h3><a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
							<?php echo get_the_title(); ?>
						</a></h3>
					</div>
					
				</div>
				<div class="p-bottom-crop">
					<div class="p-bottom-left">
						<?php if(get_post_meta( get_the_ID(), 'cp_price', true)){ ?>
						<div>
							<div class="left">Giá</div>
							:&nbsp;<?php cp_get_price( get_the_ID(), 'cp_price' ); ?></div>
						<?php }?>
						<?php if(get_post_meta( get_the_ID(), 'cp_status', true)){ ?>
						<div>
							<div class="left">Tình trạng</div>
							:&nbsp;<?php echo get_post_meta( get_the_ID(), 'cp_status', true);?></div>
						<?php }?>
						<?php
							if($sancau){
						?>
						<div>
							<div class="left">Số Lượng Sân</div>
							:&nbsp;<?php echo get_post_meta( get_the_ID(), 'cp_s_lng_sn', true ); ?></div>
							<?php }?>
						<?php if(get_post_meta( get_the_ID(), 'cp_m_sp', true)){ ?>
						<div>
							<div class="left">Tình trạng</div>
							:&nbsp;<?php echo get_post_meta( get_the_ID(), 'cp_m_sp', true);?></div>
						<?php }?>
						<?php if(get_post_meta( get_the_ID(), 'post_content', true)){ ?>
						<div>
							<div class="left">Mô Tả</div>
							:&nbsp;<?php echo get_post_meta( get_the_ID(), 'post_content', true);?></div>
						<?php }?>
						<?php if(get_post_meta( get_the_ID(), 'cp_a_ch', true)){ ?>
						<div>
							<div class="left">Địa Chỉ</div>
							:&nbsp;<?php echo get_post_meta( get_the_ID(), 'cp_a_ch', true);?></div>
						<?php }?>
					</div>

				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	
	<?php endwhile; ?>
</div>
<?php endif;wp_reset_query();?>
<!--End of Vợt cầu lông-->