<?php
/**
 * Generic Footer template.
 *
 * @package ClassiPress\Templates
 * @author  AppThemes
 * @since   ClassiPress 1.0
 */

global $cp_options;
?>

<div class="footer">
	<div class="footer_main">
	<div class="td-main-content-wrap">
		<div class="new-footer">

			<div class="td-container">
			<div class="footer-bottom">
			<div class="footer-col">
			<div class="k14-address">
			<h3 class="col-title">Yêu cầu lông</h3>
			<p>Trang thông tin điện tử chuyên về tin tức, đánh giá, giới thiệu sản phẩm mới nhất về cầu lông. Là nơi học tập, trao đổi rèn luyện kỹ năng môn cầu lông.</p>
			</div>
			<div class="k14-address">
			<p class="btn-view-map">Nhà A chung cư 789, đường Mỹ Đình, Nam Từ Liêm, Hà Nội</p>
			</div>
			<div>
			<span class='st_sharethis_large'></span>
			<span class='st_facebook_large'></span>
			<span class='st_twitter_large'></span>
			<span class='st_googleplus_large'></span>
			<span class='st_email_large'></span>
			</div>
			</div> <!-- End .footer-col -->

			<div class="footer-col">
			<div class="associate">
			<h3 class="col-title">HỢP TÁC NỘI DUNG</h3>
			<p class="phone-footer">0936.226.246</p>
			<a rel="nofollow" href="mailto:vietbai@yeucaulong.com" class="associate-mail">vietbai@yeucaulong.com</a>
			</div>
			<div class="associate">
			<h3 class="col-title">LIÊN HỆ QUẢNG CÁO</h3>
			<p class="admicro-phone-footer">0986.053.302</p>
			<a rel="nofollow" href="mailto:lienhe@yeucaulong.com" class="associate-mail">lienhe@yeucaulong.com</a>
			</div>
			</div> <!-- End .footer-col -->
			<div class="footer-col">
				<?php
					query_posts("page_id=56");
						if(have_posts()):while(have_posts()):the_post();
					?>
					   <?php the_content();?>
					<?php endwhile;endif;wp_reset_query();	
								?>
			</div> <!-- End .footer-col -->

			</div> <!-- End .footer-bottom -->
			</div> <!-- End .footer-bottom -->

		</div>
		</div>
		<div class="clear"></div>
			<div class="td-main-content-wrap pad_footer">
			<div class="td-container">

			<link href="<?php echo get_stylesheet_directory_uri() ?>/owl-carousel/owl.carousel.css" media="all" rel="stylesheet" type="text/css"/>
			<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() ?>/owl-carousel/owl.carousel.js"></script>
			<script type="text/javascript">
			jQuery(document).ready(function($) {
			 
			  $("#owl-demo").owlCarousel({
			 
				  autoPlay: 3000,
			 
				  items : 6,
				  itemsDesktop : [1199,3],
				  itemsDesktopSmall : [979,3]
			 
			  });
			 
			});
			</script>
			<ul id="owl-demo">

			<li class="menu-seo-item item"><a href="#" title="Gia đình" target="_blank"><img src="<?php echo get_stylesheet_directory_uri()."/images/logo-hang/adidas-logo.png"?>" width="127" height="50" alt="yeu cau long"></a></li>

			<li class="menu-seo-item"><a href="#" title="Gia đình" target="_blank"><img src="<?php echo get_stylesheet_directory_uri()."/images/logo-hang/Asics_logo.jpg"?>" width="127" height="50" alt="yeu cau long"></a></li>

			<li class="menu-seo-item item"><a href="#" title="Gia đình" target="_blank"><img src="<?php echo get_stylesheet_directory_uri()."/images/logo-hang/Lining.jpg"?>" width="127" height="50" alt="yeu cau long"></a></li>

			<li class="menu-seo-item item"><a href="#" title="Gia đình" target="_blank"><img src="<?php echo get_stylesheet_directory_uri()."/images/logo-hang/mizuno-logo.jpg"?>" width="127" height="50" alt="yeu cau long"></a></li>

			<li class="menu-seo-item item"><a href="#" title="Gia đình" target="_blank"><img src="<?php echo get_stylesheet_directory_uri()."/images/logo-hang/victor-logo.jpg"?>" width="127" height="50" alt="yeu cau long"></a></li>

			<li class="menu-seo-item item"><a href="#" title="Gia đình" target="_blank"><img src="<?php echo get_stylesheet_directory_uri()."/images/logo-hang/yonex.jpg"?>" width="127" height="50" alt="yeu cau long"></a></li>

			</ul>

			</div>
		</div>
		<div class="clear"></div>
		<div class="">
			<div class="td-sub-footer-container">
				<div class="td-container">
				<div class="td-pb-row">
				<div class="td-pb-span7 td-sub-footer-menu">
				</div>
				<div class="td-pb-span5 td-sub-footer-copy">
				© Copyright 2015 - Yêu cầu lông </div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div><!-- /footer -->
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>