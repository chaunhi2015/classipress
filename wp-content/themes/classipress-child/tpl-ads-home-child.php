<?php
/**
 * Template Name: Ads HomeChild Template
 *
 * @package ClassiPress\Templates
 * @author  AppThemes
 * @since   ClassiPress 3.3
 */
?>


<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<?php get_template_part( 'featured' ); ?>

			<!-- left block -->
			<div class="content_left">

				<div class="tabcontrol">
				
					<!-- Mua Bán-->
					<?php
						smk_get_template_part('tpl_cat.php', array(
						   'cat_id' => 1,
						   "cat_title" => "Mua Bán"
						));
					?>
					<!-- End of Mua Bán-->
					
					<!-- Câu Lạc Bộ-->
					<?php
						smk_get_template_part('tpl_cat.php', array(
						   'cat_id' => 10,
						   "cat_title" => "Câu Lạc Bộ"
						));
					?>
					<!-- End of Câu Lạc Bộ-->
										
					
					<!-- Cửa Hàng-->
					<?php
						smk_get_template_part('tpl_cat.php', array(
						   'cat_id' => 22,
						   "cat_title" => "Cửa Hàng Uy Tín"
						));
					?>
					<!-- End of Cửa hàng-->
					
					<!-- Lớp học-->
					<?php
						smk_get_template_part('tpl_cat.php', array(
						   'cat_id' => 9,
						   "cat_title" => "Lớp Học Cầu Lông"
						));
					?>
					<!-- End of Lớp học-->
					
					<!-- Sân Cầu-->
					<?php
						smk_get_template_part('tpl_cat.php', array(
						   'cat_id' => 8,
						   'san_cau' => true,
						   "cat_title" => "Sân Cầu"
						));
					?>
					<!-- End of Sân Cầu-->
										
					
				</div><!-- /tabcontrol -->

			</div><!-- /content_left -->


			<?php get_sidebar(); ?>


			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
