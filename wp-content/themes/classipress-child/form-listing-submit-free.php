<?php
/**
 * Free Listing Received Template.
 *
 * @package ClassiPress\Templates
 * @author  AppThemes
 * @since   ClassiPress 3.4
 */
?>


<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<div class="shadowblock_out">

				<div class="shadowblock">

					<?php appthemes_display_form_progress(); ?>

					<div id="step3">

						<h2 class="dotted"><?php _e( 'Đã nhận được bài viết', APP_TD ); ?></h2>

						<?php do_action( 'appthemes_notices' ); ?>

						<div class="thankyou">
						<?php
							if ( 'publish' == get_post_status( $listing->ID ) ) {

								echo html( 'h3', __( 'Cảm ơn bạn! danh sách bài viết của bạn đã được gửi và hiện đang tồn tại.', APP_TD ) );
								echo html( 'p', __( 'Vào bảng điều khiển để thực hiện bất kỳ thay đổi danh sách bài viết hoặc hồ sơ của bạn.', APP_TD ) );
								echo html( 'a', array( 'href' => esc_url( get_permalink( $listing->ID ) ), 'class' => 'btn_orange' ), __( 'Xem bài viết mới nhất của bạn', APP_TD ) );

							} else {

								echo html( 'h3', __( 'Cảm ơn bạn! danh sách bài viết của bạn đã được gửi.', APP_TD ) );
								echo html( 'p', __( 'Bạn có thể kiểm tra tình trạng bằng cách xem bảng điều khiển của bạn.', APP_TD ) );
								echo html( 'a', array( 'href' => esc_url( get_permalink( $listing->ID ) ), 'class' => 'btn_orange' ), __( 'Xem bài viết mới nhất của bạn', APP_TD ) );

							}
						?>

						<?php do_action( 'cp_listing_form_end_free', $listing ); ?>
						</div>

					</div>

				</div><!-- /shadowblock -->

			</div><!-- /shadowblock_out -->

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
