<?php

/*
Plugin Name: AWS Credit Payments
Plugin URI: http://www.arctic-websolutions.com/
Description: The very best and most convenient way to add credit payments to CP Auction.
Version: 1.0.8
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

/*
Copyright 2013 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AWS_CP_VERSION' ) ) {
	define( 'AWS_CP_VERSION', '1.0.8' );
}

if ( ! defined( 'AWS_CP_LATEST_RELEASE' ) ) {
	define( 'AWS_CP_LATEST_RELEASE', '12 June 2015' );
}

if ( ! defined( 'AWS_CP_PLUGIN_FILE' ) ) {
	define( 'AWS_CP_PLUGIN_FILE', __FILE__ );
}

/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION - EASY TRANSLATION USING CODESTYLING LOCALIZATION
|--------------------------------------------------------------------------
*/

function aws_cp_textdomain() {
	load_plugin_textdomain( 'AwsCP', false, dirname( plugin_basename( AWS_CP_PLUGIN_FILE ) ) . '/languages/' );
}
add_action( 'init', 'aws_cp_textdomain' );


/*
|--------------------------------------------------------------------------
| SETTINGS & TRANSACTIONS TABLE
|--------------------------------------------------------------------------
*/

if( function_exists('cp_auction_plugin_credit_handler') ) {
include_once(ABSPATH.'wp-content/plugins/cp-auction-plugin/scripts/pagination.class.php');
}

function aws_cp_settings_transactions() {

	global $cp_auction, $siteinfo, $post, $cpurl, $cp_options, $wpdb;

	$limit = ""; $msg = ""; $type = ""; $author = "";
	$lc = $cp_auction['paypal_lc'];
	$cc = $cp_options->currency_code;

?>

<script type="text/javascript">
		function update_direct_status(id)
	{
		 jQuery.ajax({
						method: 'get',
						url : '<?php echo cp_auction_plugin_url('url');?>/index.php/?_cp_auction_update_direct_status='+id,
						dataType : 'text',
						success: function() { window.location.reload(true); }
					 });
}
</script>

<!--
|--------------------------------------------------------------------------
| CREDIT TRANSACTIONS
|--------------------------------------------------------------------------
-->

	<?php if($msg) echo $msg; ?>

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Search Transactions', 'AwsCP'); ?></h3>
            <div class="inside">

   	<form action="" method="post">
<table width="350">
<tbody>
<tr valign="top" height="30"><td colspan="2" valign="top"><strong><?php _e('Search any transaction history', 'AwsCP'); ?></strong></td>
</tr>
<tr valign="top"><td><?php _e('Select user (optional):', 'AwsCP'); ?></td><td><?php wp_dropdown_users(array('show_option_none' => 'Select User', 'name' => 'author')); ?></td>
</tr>
<tr valign="top"><td><?php _e('Type of Transaction:', 'AwsCP'); ?></td><td>
    <select name="trans_type">
    <option value=""><?php _e('Select..', 'AwsCP'); ?></option>
    <option value="Rewards"><?php _e('Rewards', 'AwsCP'); ?></option>
    <option value="Credits Escrow"><?php _e('Escrow Payment', 'AwsCP'); ?></option>
    <option value="Credits by PayPal"><?php _e('Credits by PayPal', 'AwsCP'); ?></option>
    <option value="Credits by Bank Transfer"><?php _e('Credits by Bank Transfer', 'AwsCP'); ?></option>
    <option value="Credited by Admin"><?php _e('Credited by Admin', 'AwsCP'); ?></option>
    <option value="Charged by Admin"><?php _e('Charged by Admin', 'AwsCP'); ?></option>
    <option value="Withdraw"><?php _e('Withdraw', 'AwsCP'); ?></option>
    <option value="Direct by Credits"><?php _e('Product Purchases', 'AwsCP'); ?></option>
    <option value="Paid Author"><?php _e('Paid Author / Seller (OLD)', 'AwsCP'); ?></option>
    <option value="All"><?php _e('All Transactions by User', 'AwsCP'); ?></option>
    </select></td>
</tr>
</tbody>
</table>
<p class="submit"><input type="submit" name="search" id="submit" class="button-secondary" value="<?php _e('Search', 'AwsCP'); ?>" /></p>
   </form>

           </div>
         </div>

<?php

	if( isset($_POST['search']) ) {
	$type = isset( $_POST['trans_type'] ) ? esc_attr( $_POST['trans_type'] ) : '';
	$author = isset( $_POST['author'] ) ? esc_attr( $_POST['author'] ) : '';

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	if( $type == "All" && $author > 0 ) {
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE uid = '$author' AND aws = 'aws-credits'" );
	} elseif( $type != "All" && $author > 0 ) {
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE uid = '$author' AND trans_type = '$type' AND aws = 'aws-credits'" );
	} elseif( $type && $author < 1 ) {
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE trans_type = '$type' AND aws = 'aws-credits'" );
	} else {
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE aws = 'aws-credits'" );
	}
	}
	else {
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE aws = 'aws-credits'" );
}

if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-auction&tab=credits");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	if( $type == "All" && $author > 0 ) {
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$author' AND aws = 'aws-credits' ORDER BY id DESC {$limit}");
	} elseif( $type != "All" && $author > 0 ) {
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$author' AND trans_type = '$type' AND aws = 'aws-credits' ORDER BY id DESC {$limit}");
	} elseif( $type && $author < 1 ) {
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE trans_type = '$type' AND aws = 'aws-credits' ORDER BY id DESC {$limit}");
	} else {
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE aws = 'aws-credits' ORDER BY id DESC {$limit}");
	}
	?>

<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID', 'AwsCP'); ?></th>
        <th><?php _e('User', 'AwsCP'); ?></th>
        <th><?php _e('IP', 'AwsCP'); ?></th>
        <th><?php _e('Type', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Credits', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Amount', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Fees/Profit', 'AwsCP'); ?></th>
        <th><?php _e('TransID', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Available', 'AwsCP'); ?></th>
        <th><?php _e('Date', 'AwsCP'); ?></th>
        <th><?php _e('Status', 'AwsCP'); ?></th>
        <th><?php _e('Action', 'AwsCP'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID', 'AwsCP'); ?></th>
        <th><?php _e('User', 'AwsCP'); ?></th>
        <th><?php _e('IP', 'AwsCP'); ?></th>
        <th><?php _e('Type', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Credits', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Amount', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Fees/Profit', 'AwsCP'); ?></th>
        <th><?php _e('TransID', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Available', 'AwsCP'); ?></th>
        <th><?php _e('Date', 'AwsCP'); ?></th>
        <th><?php _e('Status', 'AwsCP'); ?></th>
        <th><?php _e('Action', 'AwsCP'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
    	    $id       	 = $row->id;
            $order_id 	 = $row->order_id;
            $post_ids 	 = $row->post_ids;
            $uid      	 = $row->uid;
            $seller_id	 = $row->post_author;
            $IP	         = $row->ip_address;
            $fullname    = "".$row->first_name." ".$row->last_name."";
            $email       = $row->payer_email;
            $item_name   = $row->item_name;
            $trans_type  = $row->trans_type;
            $currency    = $row->mc_currency;
            $fee    	 = $row->processor_fee;
            $receiver	 = $row->payer_email;
            $numbers     = $row->quantity;
            $credits     = cp_auction_format_amount($row->moved_credits, 'process');
	    $available   = cp_auction_format_amount($row->available, 'process');
            $price       = cp_auction_format_amount($row->mc_gross, 'process');
            $fees        = cp_auction_format_amount($row->mc_fee, 'process');
            $payout_option = $row->payment_option;
            $payout_details = $row->payout_details;
            $transid     = $row->txn_id;
            $date        = date_i18n('Y-m-d', $row->datemade, true);
            $type 	 = $row->type;
            $status      = $row->status;
            $item = ''.__('Order ID:','AwsCP').' '.$order_id.'';

            $now_available = get_user_meta( $uid, 'cp_credits', true );
            if ( $type == "escrow" ) $uids = $seller_id;
            else $uids = $uid;
            if( empty($receiver) ) $receiver = get_user_meta( $uids, 'paypal_email', true );

            $user = get_userdata( $uid );

            $available = '<a title="'.$now_available.' '.__('credits available now','AwsCP').'">'.$available.'</a>';
            if( $IP == 0 || empty($IP) ) $IP = "N/A";
            if( !$transid ) $transid = "N/A";

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td>#'.$id.'</td>';
            echo '<td><a target="_blank" title="'.$fullname.'" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.'').'">'.$user->user_login.'</a></td>';
            echo '<td>'.$IP.'</td>';
            echo '<td><a title="'.$item_name.'">'.$trans_type.'</a></td>';
            echo '<td style="text-align:right">'.$credits.' '.__('credits', 'AwsCP').'</td>';
            echo '<td style="text-align:right">'.cp_auction_format_amount($price).' '.$currency.'</td>';
            echo '<td style="text-align:right">'.cp_auction_format_amount($fees).' '.$currency.'</td>';
            echo '<td>'.$transid.'</td>';
            echo '<td style="text-align:right">'.$available.' '.__('credits', 'AwsCP').'</td>';
            echo '<td>'.$date.'</td>';
            if( $trans_type == "Credits Escrow" ) {
            if( $status == "pending" ) {
            echo '<td><a href="#" class="update-escrow-status" title="'.__('Update escrow status.', 'AwsCP').'" id="'.$id.'">'.$status.'</a></td>';
            }
            else {
            echo '<td>'.$status.'</td>';
            }
	    }
	    elseif(( $trans_type == "Credits by Bank Transfer" || $trans_type == "Credits by PayPal" ) && ( $status == "pending" )) {
	    echo '<td><a href="#" class="update-credit-status" title="'.__('Update credit status.', 'AwsCP').'" id="'.$id.'">'.$status.'</a></td>';
	    }
	    elseif( $trans_type == "Paid Author" && $status == "pending" ) {
	    echo '<td><a href="#" class="update-direct-status" title="'.$item_name.'" id="'.$id.'">'.$status.'</a></td>';
	    }
	    elseif( $trans_type == "Withdraw" && $status == "pending" ) {
	    echo '<td><a href="#" class="update-withdraw-status" title="'.__('Update withdraw status.', 'AwsCP').'" id="'.$id.'">'.$status.'</a></td>';
	    }
	    else {
	    echo '<td>'.$status.'</td>';
	    }

	    if( !$fees ) $fees = '0.00';
	    $payout = ($price - $fees) - fees_covered($fee);
            if( $trans_type == 'Escrow Payment' )
            echo '<td><a href="https://www.paypal.com/cgi-bin/webscr?on0=Buyer&os0='.$user->user_login.'&on1=Reference&os1='.$transid.'&amount='.$payout.'&item_name='.$item.'&cmd=_xclick&business='.$receiver.'&no_shipping=1&currency_code='.$cc.'&lc='.$lc.'" target="_blank">'.__('Pay', 'AwsCP').'</a> | <a href="#" class="trans-delete-pid" title="'.__('Delete transaction.', 'AwsCP').'" id="'.$id.'">'.__('Delete', 'AwsCP').'</a></td>';
            elseif( $trans_type == 'Withdraw' && $payout_option == "paypal" && $status == "pending" )
            echo '<td><a title="'.cp_auction_format_amount($payout).' '.__('with PayPal','AwsCP').'" href="https://www.paypal.com/cgi-bin/webscr?on0=Buyer&os0='.$user->user_login.'&on1=Reference&os1='.__('Withdraw', 'AwsCP').'&amount='.$payout.'&item_name='.$item_name.'&cmd=_xclick&business='.$receiver.'&no_shipping=1&currency_code='.$cc.'&lc='.$lc.'" target="_blank">'.__('Pay', 'AwsCP').'</a> | <a href="#" class="trans-delete-withdraw" title="'.__('Delete withraw request.', 'AwsCP').'" id="'.$id.'">'.__('Delete', 'AwsCP').'</a></td>';
            elseif( $trans_type == 'Withdraw' && $payout_option == "bank_transfer" && $status == "pending" )
            echo '<td><a title="'.$payout_details.'">'.__('Info', 'AwsCP').'</a> | <a href="#" class="trans-delete-withdraw" title="'.__('Delete withraw request.', 'AwsCP').'" id="'.$id.'">'.__('Delete', 'AwsCP').'</a></td>';
            else
            echo '<td><a href="#" class="trans-delete-pid" title="'.__('Delete transaction.', 'AwsCP').'" id="'.$id.'">'.__('Delete', 'AwsCP').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="12"><?php _e('No Records Found!', 'AwsCP'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>

<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<div class="admin_text"><?php _e('<strong>Escrow Payment:</strong> Click on the status pending, the transaction will change the status to complete and an notification email is automatically sent to author/seller. <a style="text-decoration: none;" href="/wp-admin/admin.php?page=cp-transactions&tab=escrow">View Transactions</a>.<br /><strong>Credits by Bank Transfer:</strong> Once payment is received, click on the status pending, the transaction will change the status to complete and purchased credits are automatically transferred to the user`s account.<br /><strong>Credits by PayPal:</strong> Once payment is received, click on the status pending, the transaction will change the status to complete and purchased credits are automatically transferred to the user`s account.<br /><strong>Paid Author:</strong> Click on the status pending, the transaction will change the status to complete and an notification email is automatically sent to author/seller.', 'AwsCP'); ?></div>


<?php } else { ?>

<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID', 'AwsCP'); ?></th>
        <th><?php _e('User', 'AwsCP'); ?></th>
        <th><?php _e('IP', 'AwsCP'); ?></th>
        <th><?php _e('Type', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Credits', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Amount', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Fees/Profit', 'AwsCP'); ?></th>
        <th><?php _e('TransID', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Available', 'AwsCP'); ?></th>
        <th><?php _e('Date', 'AwsCP'); ?></th>
        <th><?php _e('Status', 'AwsCP'); ?></th>
        <th><?php _e('Action', 'AwsCP'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID', 'AwsCP'); ?></th>
        <th><?php _e('User', 'AwsCP'); ?></th>
        <th><?php _e('IP', 'AwsCP'); ?></th>
        <th><?php _e('Type', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Credits', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Amount', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Fees/Profit', 'AwsCP'); ?></th>
        <th><?php _e('TransID', 'AwsCP'); ?></th>
        <th style="text-align:center"><?php _e('Available', 'AwsCP'); ?></th>
        <th><?php _e('Date', 'AwsCP'); ?></th>
        <th><?php _e('Status', 'AwsCP'); ?></th>
        <th><?php _e('Action', 'AwsCP'); ?></th>
    </tr>
</tfoot>
<tbody>
        <tr>
        <td colspan="12"><?php _e('No Records Found!', 'AwsCP'); ?></td>
        </tr>
</tbody>
</table>

<?php } ?>

<div class="clear20"></div>

<?php
}
?>