=== AWS Credit Payments ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: auction, classipress, auction plugin, classipress theme, cpauction
Requires at least: 3.9.2
Tested up to: 4.2.2
Stable tag: 1.0.8
License: GPL
License URI: http://www.gnu.org/licenses/gpl.html

---------------------------------------------------------------------------------------------------

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://mixed.wp-build.com) - Not open for backend testing.
* [Manuals & Support](http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Some Admin Features ===

* Super easy management of all transactions.
* View & approve escrow payments.
* View & approve direct payments.
* Approve and payout withdrawals.
* Search transactions by user and transaction type.
* Add or remove credits from any user.
* Check any users available credits.
* Check any users complete credit pay/withdraw history.
* Add credits as payment option for classified ads.

=== Some User Features ===

* Users can purchase credits using paypal or bank transfer.
* Users can withdraw / exchange their credits to paypal or bank transfer.
* Users can pay seller/author directly and instant using credits.
* Credits can be used as payment option for new ads etc.

=== Installation ===

* [Instructions] (http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.