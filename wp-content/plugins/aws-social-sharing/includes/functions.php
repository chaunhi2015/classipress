<?php

/*
Copyright 2013 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| SOCIAL SHARING - FACEBOOK LIKES
|--------------------------------------------------------------------------
*/

if ( !function_exists('cp_fb_like') ) {
function cp_fb_like() {

		$layout = get_option("cp_fb_like_layout");
		if($layout == '') { $layout = 'standard'; }

		$action = get_option("cp_fb_like_action");
		if($action == '') { $layout = 'like'; }

		$font = get_option("cp_fb_like_font");
		if($font == '') { $font = 'arial'; }

		$colorscheme = get_option("cp_fb_like_colorscheme");
		if($colorscheme == '') { $colorscheme = 'light'; }

		$width = get_option("cp_fb_like_width");
		if($width == '') { $width = '450'; }

		$height = get_option("cp_fb_like_height");
		if($height == '') { $height = '100'; }

		$fbsend = get_option("cp_fb_like_send");
		if($fbsend == '') { $fbsend = 'false'; }

		$output = '<div id="cp_fb_like_button" style="height:'.$height.'px;"><fb:like href="'.get_permalink().'" send="'.$fbsend.'" layout="'.$layout.'" width="'.$width.'" font="'.$font.'" action="'.$action.'" colorscheme="'.$colorscheme.'" show_faces="false"></fb:like></div>';

		return $output;
}
}


/*
|--------------------------------------------------------------------------
| ADD THE FACEBOOK LIKE CODE TO THE HEADER
|--------------------------------------------------------------------------
*/

function aws_ss_fblike_header_code() {

	$lang = get_bloginfo( 'language' );
	$lc = preg_replace('/-/', '_', $lang).''; 
?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
  	var js, fjs = d.getElementsByTagName(s)[0];
  	if (d.getElementById(id)) return;
  	js = d.createElement(s); js.id = id;
  	js.src = "//connect.facebook.net/<?php echo $lc; ?>/all.js#xfbml=1";
  	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<?php
}
add_action('appthemes_before', 'aws_ss_fblike_header_code');


/*
|--------------------------------------------------------------------------
| ADD THE SOCIAL SHARING CODE ON WANTED & AUCTION ADS
|--------------------------------------------------------------------------
*/

function aws_ss_social_sharing_auctions() {

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ); ?>

	<?php if( ( get_option("cp_add_this_activate") == "true" && is_single() ) && ( ! empty( $my_type ) && $my_type != "classified" ) ) { ?>
            <div class="clear5"></div>
            <?php echo get_option("cp_add_this_code"); ?>
	    <div class="clr"></div>
	    <?php } ?>
<?php
}
if( get_option('cp_auction_addthis_top') == '1' ) 
add_action( 'appthemes_after_post_title', 'aws_ss_social_sharing_auctions' );
if( get_option('cp_auction_addthis_middle') == '1' )
add_action( 'appthemes_before_post_content', 'aws_ss_social_sharing_auctions' );
if( get_option('cp_auction_addthis_bottom') == '1' )
add_action( 'appthemes_after_post_content', 'aws_ss_social_sharing_auctions' );


/*
|--------------------------------------------------------------------------
| ADD THE FACEBOOK SEND & LIKE BUTTONS ON WANTED & AUCTION ADS
|--------------------------------------------------------------------------
*/

function aws_ss_fb_like_auctions() {

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ); ?>

	<?php if( ( get_option("cp_fb_like_activate") == "true" && is_single() ) && ( ! empty( $my_type ) && $my_type != "classified" ) ) { ?>
	<div class="clear10"></div>
	<?php echo cp_fb_like(); ?>
	<div class="clr"></div>
	<?php } ?>
<?php
}
if( get_option("cp_fb_like_top") == '1' )
add_action( 'appthemes_after_post_title', 'aws_ss_fb_like_auctions' );
if( get_option("cp_fb_like_middle") == '1' )
add_action( 'appthemes_before_post_content', 'aws_ss_fb_like_auctions' );
if( get_option("cp_fb_like_bottom") == '1' )
add_action( 'appthemes_after_post_content', 'aws_ss_fb_like_auctions' );


/*
|--------------------------------------------------------------------------
| ADD THE SOCIAL SHARING CODE ON CLASSIFIEDS
|--------------------------------------------------------------------------
*/

function aws_ss_social_sharing_classifieds() {

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ); ?>

	<?php if( ( get_option("cp_add_this_activate_classifieds") == "true" && is_single() ) && ( empty( $my_type ) || $my_type == "classified" ) ) { ?>
            <div class="clear5"></div>
            <?php echo get_option("cp_add_this_code"); ?>
	    <div class="clr"></div>
	    <?php } ?>
<?php
}
if( get_option('cp_auction_addthis_top') == '1' )
add_action( 'appthemes_after_post_title', 'aws_ss_social_sharing_classifieds' );
if( get_option('cp_auction_addthis_middle') == '1' )
add_action( 'appthemes_before_post_content', 'aws_ss_social_sharing_classifieds' );
if( get_option('cp_auction_addthis_bottom') == '1' )
add_action( 'appthemes_after_post_content', 'aws_ss_social_sharing_classifieds' );


/*
|--------------------------------------------------------------------------
| ADD THE FACEBOOK SEND & LIKE BUTTONS ON CLASSIFIED ADS
|--------------------------------------------------------------------------
*/

function aws_ss_fb_like_classified() {

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ); ?>

	<?php if( ( get_option("cp_fb_like_activate_classifieds") == "true" && is_single() ) && ( empty( $my_type ) || $my_type == "classified" ) ) { ?>
	<div class="clear10"></div>
	<?php echo cp_fb_like(); ?>
	<div class="clr"></div>
	<?php } ?>
<?php
}
if( get_option("cp_fb_like_top") == '1' )
add_action( 'appthemes_after_post_title', 'aws_ss_fb_like_classified' );
if( get_option("cp_fb_like_middle") == '1' )
add_action( 'appthemes_before_post_content', 'aws_ss_fb_like_classified' );
if( get_option("cp_fb_like_bottom") == '1' )
add_action( 'appthemes_after_post_content', 'aws_ss_fb_like_classified' );


/*
|--------------------------------------------------------------------------
| ADD THE SOCIAL SHARING CODE IN HEADER
|--------------------------------------------------------------------------
*/

function aws_ss_social_sharing_header() {

	if( get_option("cp_add_this_activate_header") == "true" && get_option("cp_auction_addthis_header") == '1' ) { ?>

            <div class="clear10"></div>
            <div style="float: right;"><?php echo get_option("cp_add_this_code_header"); ?></div>
	    <div class="clr"></div>
	    <?php } if( get_option("cp_fb_like_activate_header") == "true" && get_option("cp_fb_like_header") == '1' ) { ?>
	    <div class="clear10"></div>
	    <?php echo cp_fb_like(); ?>
	    <div class="clr"></div>
	    <?php } ?>

<?php
}
if( get_option("cp_add_this_activate_header") == "true" || get_option("cp_fb_like_activate_header") == "true" )
add_action( 'appthemes_advertise_header', 'aws_ss_social_sharing_header', 11 );