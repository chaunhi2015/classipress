=== AWS Social Sharing ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: auction, classipress, auction plugin, classipress theme, cpauction
Requires at least: 3.9.2
Tested up to: 4.2.2
Stable tag: 1.0.4
License: GPL
License URI: http://www.gnu.org/licenses/gpl.html

---------------------------------------------------------------------------------------------------

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://offer.wp-build.com) - Not open for backend testing.
* [Manuals & Support](http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Some Admin Features ===

* Super easy management of all settings.
* Enable on Classified Ads.
* Enable on Auctions & Marketplace Ads if CP Auction is installed.
* Add AddThis or Facebook like and send buttons to header, top of ads, middle of ads and bottom of ads.
* Use an single button/share function or use them all at once.

=== Some User Features ===

* Users can share their ads to more than 300 social sites, or like & send to Facebook.

=== Installation ===

* [Instructions] (http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.