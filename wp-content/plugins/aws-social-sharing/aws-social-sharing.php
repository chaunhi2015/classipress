<?php

/*
Plugin Name: AWS Social Sharing
Plugin URI: http://www.arctic-websolutions.com/
Description: The very best and most convenient way to add Social Sharing buttons to ClassiPress & CP Auction.
Version: 1.0.4
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

/*
Copyright 2013 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AWS_SS_VERSION' ) ) {
	define( 'AWS_SS_VERSION', '1.0.4' );
}

if ( ! defined( 'AWS_SS_LATEST_RELEASE' ) ) {
	define( 'AWS_SS_LATEST_RELEASE', '16 May 2015' );
}

if ( ! defined( 'AWS_SS_PLUGIN_FILE' ) ) {
	define( 'AWS_SS_PLUGIN_FILE', __FILE__ );
}

/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION - EASY TRANSLATION USING CODESTYLING LOCALIZATION
|--------------------------------------------------------------------------
*/

function aws_ss_textdomain() {
	load_plugin_textdomain( 'AwsSS', false, dirname( plugin_basename( AWS_SS_PLUGIN_FILE ) ) . '/languages/' );
}
add_action( 'init', 'aws_ss_textdomain' );


/*
|--------------------------------------------------------------------------
| ADD LINK TO SETTINGS MENU PAGE
|--------------------------------------------------------------------------
*/

function aws_ss_add_link() {

	global $aws_ss_admin_menu;
	$aws_ss_admin_menu = add_menu_page( __( 'AWS Social Sharing', 'AwsSS' ), __( 'Social Sharing', 'AwsSS' ), 'manage_options', 'aws-social-sharing', 'aws_social_sharing_settings', plugins_url( 'aws-social-sharing/css/images/aws-icon.png' ), '6,2' );

	global $aws_ss_settings;
	$aws_ss_settings = add_submenu_page( 'aws-social-sharing', __('Settings','AwsSS'), __('Settings','AwsSS'), 'manage_options', 'aws-social-sharing', 'aws_social_sharing_settings');

}
add_action( 'admin_menu', 'aws_ss_add_link' );


/*
|--------------------------------------------------------------------------
| ADD SETTINGS LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'aws_ss_action_links' );
function aws_ss_action_links( $links ) {

return array_merge(
array(
'settings' => '<a href="' . get_bloginfo( 'url' ) . '/wp-admin/admin.php?page=aws-social-sharing">'.__( 'Settings', 'AwsSS' ).'</a>'
),
$links
);
}


/*
|--------------------------------------------------------------------------
| ADD SUPPORT LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_row_meta', 'aws_ss_meta_links', 10, 2 );
function aws_ss_meta_links( $links, $file ) {

$plugin = plugin_basename(__FILE__);

// create link
if ( $file == $plugin ) {
return array_merge(
$links,
array( '<a href="http://www.arctic-websolutions.com/forums/" target="_blank">'.__( 'Support', 'AwsSS' ).'</a>' )
);
}
return $links;
}


/*
|--------------------------------------------------------------------------
| REGISTER ADMIN BACKEND / FRONTEND SCRIPTS AND STYLESHEETS
|--------------------------------------------------------------------------
*/

function aws_load_admin_menu_scripts() {

	wp_enqueue_style( 'aws-admin-style', plugins_url( 'aws-social-sharing/css/admin.css'), false, '1.0.0' );

}
if ( is_admin() ) {
	add_action( 'admin_print_styles', 'aws_load_admin_menu_scripts' );
}

function aws_load_frontend_scripts() {

	wp_enqueue_style( 'aws-ss-style', plugins_url( 'aws-social-sharing/css/style.css'), false, '1.0.0' );

}
if ( !is_admin() ) {
	add_action('wp_head','aws_load_frontend_scripts');
}


/*
|--------------------------------------------------------------------------
| INCLUDES
|--------------------------------------------------------------------------
*/

include_once('includes/functions.php');


/*
|--------------------------------------------------------------------------
| SETTINGS & TRANSACTIONS TABLE
|--------------------------------------------------------------------------
*/

function aws_social_sharing_settings() {

	$msg = "";

		if(isset($_POST['cp_fb_form_submit'])) {

			$activate = isset( $_POST['cp_add_this_activate'] ) ? esc_attr( $_POST['cp_add_this_activate'] ) : '';
			$act_classifieds = isset( $_POST['cp_add_this_activate_classifieds'] ) ? esc_attr( $_POST['cp_add_this_activate_classifieds'] ) : '';
			$act_header = isset( $_POST['cp_add_this_activate_header'] ) ? esc_attr( $_POST['cp_add_this_activate_header'] ) : '';
			$add_top = isset( $_POST['cp_auction_addthis_top'] ) ? esc_attr( $_POST['cp_auction_addthis_top'] ) : '';
			$add_middle = isset( $_POST['cp_auction_addthis_middle'] ) ? esc_attr( $_POST['cp_auction_addthis_middle'] ) : '';
			$add_bottom = isset( $_POST['cp_auction_addthis_bottom'] ) ? esc_attr( $_POST['cp_auction_addthis_bottom'] ) : '';
			$add_header = isset( $_POST['cp_auction_addthis_header'] ) ? esc_attr( $_POST['cp_auction_addthis_header'] ) : '';
			$add_code = isset( $_POST['cp_add_this_code'] ) ? esc_attr( $_POST['cp_add_this_code'] ) : '';
			$add_code_header = isset( $_POST['cp_add_this_code_header'] ) ? esc_attr( $_POST['cp_add_this_code_header'] ) : '';
			$like_activate = isset( $_POST['cp_fb_like_activate'] ) ? esc_attr( $_POST['cp_fb_like_activate'] ) : '';
			$like_act_classifieds = isset( $_POST['cp_fb_like_activate_classifieds'] ) ? esc_attr( $_POST['cp_fb_like_activate_classifieds'] ) : '';
			$like_act_header = isset( $_POST['cp_fb_like_activate_header'] ) ? esc_attr( $_POST['cp_fb_like_activate_header'] ) : '';
			$like_top = isset( $_POST['cp_fb_like_top'] ) ? esc_attr( $_POST['cp_fb_like_top'] ) : '';
			$like_middle = isset( $_POST['cp_fb_like_middle'] ) ? esc_attr( $_POST['cp_fb_like_middle'] ) : '';
			$like_bottom = isset( $_POST['cp_fb_like_bottom'] ) ? esc_attr( $_POST['cp_fb_like_bottom'] ) : '';
			$like_header = isset( $_POST['cp_fb_like_header'] ) ? esc_attr( $_POST['cp_fb_like_header'] ) : '';
			$like_layout = isset( $_POST['cp_fb_like_layout'] ) ? esc_attr( $_POST['cp_fb_like_layout'] ) : '';
			$like_action = isset( $_POST['cp_fb_like_action'] ) ? esc_attr( $_POST['cp_fb_like_action'] ) : '';
			$like_font = isset( $_POST['cp_fb_like_font'] ) ? esc_attr( $_POST['cp_fb_like_font'] ) : '';
			$like_colorscheme = isset( $_POST['cp_fb_like_colorscheme'] ) ? esc_attr( $_POST['cp_fb_like_colorscheme'] ) : '';
			$like_width = isset( $_POST['cp_fb_like_width'] ) ? esc_attr( $_POST['cp_fb_like_width'] ) : '';
			$like_height = isset( $_POST['cp_fb_like_height'] ) ? esc_attr( $_POST['cp_fb_like_height'] ) : '';
			$like_send = isset( $_POST['cp_fb_like_send'] ) ? esc_attr( $_POST['cp_fb_like_send'] ) : '';

			if( $activate ) {
			update_option("cp_add_this_activate", $activate);
			} else {
			delete_option("cp_add_this_activate");
			}
			if( $act_classifieds ) {
			update_option("cp_add_this_activate_classifieds", $act_classifieds);
			} else {
			delete_option("cp_add_this_activate_classifieds");
			}
			if( $act_header ) {
			update_option("cp_add_this_activate_header", $act_header);
			} else {
			delete_option("cp_add_this_activate_header");
			}
			if( $add_top ) {
			update_option("cp_auction_addthis_top", $add_top);
			} else {
			delete_option("cp_auction_addthis_top");
			}
			if( $add_middle ) {
			update_option("cp_auction_addthis_middle", $add_middle);
			} else {
			delete_option("cp_auction_addthis_middle");
			}
			if( $add_bottom ) {
			update_option("cp_auction_addthis_bottom", $add_bottom);
			} else {
			delete_option("cp_auction_addthis_bottom");
			}
			if( $add_header ) {
			update_option("cp_auction_addthis_header", $add_header);
			} else {
			delete_option("cp_auction_addthis_header");
			}
			if( $add_code ) {
			update_option("cp_add_this_code", stripslashes(html_entity_decode($add_code)));
			} else {
			delete_option("cp_add_this_code");
			}
			if( $add_code_header ) {
			update_option("cp_add_this_code_header", stripslashes(html_entity_decode($add_code_header)));
			} else {
			delete_option("cp_add_this_code_header");
			}
			if( $like_activate ) {
			update_option("cp_fb_like_activate", $like_activate);
			} else {
			delete_option("cp_fb_like_activate");
			}
			if( $like_act_classifieds ) {
			update_option("cp_fb_like_activate_classifieds", $like_act_classifieds);
			} else {
			delete_option("cp_fb_like_activate_classifieds");
			}
			if( $like_act_header ) {
			update_option("cp_fb_like_activate_header", $like_act_header);
			} else {
			delete_option("cp_fb_like_activate_header");
			}
			if( $like_top ) {
			update_option("cp_fb_like_top", $like_top);
			} else {
			delete_option("cp_fb_like_top");
			}
			if( $like_middle ) {
			update_option("cp_fb_like_middle", $like_middle);
			} else {
			delete_option("cp_fb_like_middle");
			}
			if( $like_bottom ) {
			update_option("cp_fb_like_bottom", $like_bottom);
			} else {
			delete_option("cp_fb_like_bottom");
			}
			if( $like_header ) {
			update_option("cp_fb_like_header", $like_header);
			} else {
			delete_option("cp_fb_like_header");
			}
			if( $like_layout ) {
			update_option("cp_fb_like_layout", $like_layout);
			} else {
			delete_option("cp_fb_like_layout");
			}
			if( $like_action ) {
			update_option("cp_fb_like_action", $like_action);
			} else {
			delete_option("cp_fb_like_action");
			}
			if( $like_font ) {
			update_option("cp_fb_like_font", $like_font);
			} else {
			delete_option("cp_fb_like_font");
			}
			if( $like_colorscheme ) {
			update_option("cp_fb_like_colorscheme", $like_colorscheme);
			} else {
			delete_option("cp_fb_like_colorscheme");
			}
			if( $like_width ) {
			update_option("cp_fb_like_width", intval($like_width));
			} else {
			delete_option("cp_fb_like_width");
			}
			if( $like_height ) {
			update_option("cp_fb_like_height", intval($like_height));
			} else {
			delete_option("cp_fb_like_height");
			}
			if( $like_send ) {
			update_option("cp_fb_like_send", $like_send);
			} else {
			delete_option("cp_fb_like_send");
			}
		}

	if(isset($_POST['cp_fb_form_submit'])) {
	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Settings was saved!','AwsSS').'</strong></p></div>';
	}
	?>

	<div class="wrap">

	<h2><?php _e( 'AWS Social Sharing Management', 'AwsSS' ); ?></h2>

	<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Social Sharing Settings', 'AwsSS'); ?></h3>
            <div class="inside">

	<fieldset>
	<form name="cp_fb_option_form" method="post">

	<h4><?php _e('Activate AddThis Social Sharing?', 'AwsSS'); ?> <a href="http://www.addthis.com/" target="_blank"><?php _e('Get Code', 'AwsSS'); ?></a>.</h4>
	<div class="admin_text"><?php _e('Increase traffic to your site by helping visitors share to Facebook, Twitter and over 300 other social sites.', 'AwsSS'); ?></div>

	<select name="cp_add_this_activate_classifieds" id="cp_add_this_activate_classifieds">
		<?php 
		if ( !get_option("cp_add_this_activate_classifieds") )
		{
		?>
		<option value=""><?php _e('Select', 'AwsSS'); ?></option>
		<?php
		}
		?>
		<option value="true" <?php if (get_option("cp_add_this_activate_classifieds") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsSS'); ?></option>
		<option value="" <?php if (get_option("cp_add_this_activate_classifieds") == "") echo 'selected'; ?>><?php _e('No', 'AwsSS'); ?></option>
	</select> <?php _e('Enable to use on Classified Ads.', 'AwsSS'); ?>

	<div class="clear5"></div>

	<select name="cp_add_this_activate" id="cp_add_this_activate">
		<?php 
		if ( !get_option("cp_add_this_activate") )
		{
		?>
		<option value=""><?php _e('Select', 'AwsSS'); ?></option>
		<?php
		}
		?>
		<option value="true" <?php if (get_option("cp_add_this_activate") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsSS'); ?></option>
		<option value="" <?php if (get_option("cp_add_this_activate") == "") echo 'selected'; ?>><?php _e('No', 'AwsSS'); ?></option>
	</select> <?php _e('Enable to use on Wanted & Auction Ads.', 'AwsSS'); ?>

	<div class="clear5"></div>

<select name="cp_add_this_activate_header" id="cp_add_this_activate_header">
		<?php 
		if ( !get_option("cp_add_this_activate_header") )
		{
		?>
		<option value=""><?php _e('Select', 'AwsSS'); ?></option>
		<?php
		}
		?>
		<option value="true" <?php if (get_option("cp_add_this_activate_header") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsSS'); ?></option>
		<option value="" <?php if (get_option("cp_add_this_activate_header") == "") echo 'selected'; ?>><?php _e('No', 'AwsSS'); ?></option>
	</select> <?php _e('Enable to use in Header.', 'AwsSS'); ?>

	<div class="clear10"></div>

	<input type="checkbox" name="cp_auction_addthis_header" value="1" <?php if(get_option('cp_auction_addthis_header') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned at the top of the page, just below the banner advert.', 'AwsSS'); ?>

	<div class="clr"></div>

	<input type="checkbox" name="cp_auction_addthis_top" value="1" <?php if(get_option('cp_auction_addthis_top') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned at the top of the ad, just below the title.', 'AwsSS'); ?>

	<div class="clr"></div>

	<input type="checkbox" name="cp_auction_addthis_middle" value="1" <?php if(get_option('cp_auction_addthis_middle') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned in the middle of the ad, just above description.', 'AwsSS'); ?>

	<div class="clr"></div>

	<input type="checkbox" name="cp_auction_addthis_bottom" value="1" <?php if(get_option('cp_auction_addthis_bottom') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned at the bottom of the ad.', 'AwsSS'); ?>

	<div class="clear10"></div>

	<h4><?php _e('Ad Content AddThis Code', 'AwsSS'); ?></h4>
	<div class=""><?php _e('Copy and paste your addthis code here:', 'AwsSS'); ?></div>
	<textarea class="options" name="cp_add_this_code" id="cp_add_this_code" rows="8" cols="60"><?php echo get_option("cp_add_this_code"); ?></textarea><br><small><?php _e('Paste the complete code that you get from AddThis.com', 'AwsSS'); ?></small><br />
	<br />

	<div class="clear10"></div>

	<h4><?php _e('Header AddThis Code', 'AwsSS'); ?></h4>
	<div class=""><?php _e('Copy and paste your addthis code here:', 'AwsSS'); ?></div>
	<textarea class="options" name="cp_add_this_code_header" id="cp_add_this_code_header" rows="8" cols="60"><?php echo get_option("cp_add_this_code_header"); ?></textarea><br><small><?php _e('Paste the complete code that you get from AddThis.com', 'AwsSS'); ?></small><br />
	<br />
	<input type="submit" value="<?php _e('Save Settings', 'AwsSS'); ?>" class="button-primary"><br /><br />

	<div class="dotted"></div>

	<h4><?php _e('Would you rather use Facebook like and send buttons?', 'AwsSS'); ?></h4>
	<div class="admin_text"><?php _e('The Like Button allows your users to easily like any listing on your site.', 'AwsSS'); ?></div>

	<select name="cp_fb_like_activate_classifieds" id="cp_fb_like_activate_classifieds">
		<?php 
		if ( !get_option("cp_fb_like_activate_classifieds") )
		{
		?>
		<option value=""><?php _e('Select', 'AwsSS'); ?></option>
		<?php
		}
		?>
		<option value="true" <?php if (get_option("cp_fb_like_activate_classifieds") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsSS'); ?></option>
		<option value="" <?php if (get_option("cp_fb_like_activate_classifieds") == "") echo 'selected'; ?>><?php _e('No', 'AwsSS'); ?></option>
	</select> <?php _e('Enable to use on Classified Ads.', 'AwsSS'); ?>

	<div class="clear5"></div>

	<select name="cp_fb_like_activate" id="cp_fb_like_activate">
		<?php 
		if ( !get_option("cp_fb_like_activate") )
		{
		?>
		<option value=""><?php _e('Select', 'AwsSS'); ?></option>
		<?php
		}
		?>
		<option value="true" <?php if (get_option("cp_fb_like_activate") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsSS'); ?></option>
		<option value="" <?php if (get_option("cp_fb_like_activate") == "") echo 'selected'; ?>><?php _e('No', 'AwsSS'); ?></option>
	</select> <?php _e('Enable to use on Wanted & Auction Ads.', 'AwsSS'); ?>

	<div class="clear5"></div>

<select name="cp_fb_like_activate_header" id="cp_fb_like_activate_header">
		<?php 
		if ( !get_option("cp_fb_like_activate_header") )
		{
		?>
		<option value=""><?php _e('Select', 'AwsSS'); ?></option>
		<?php
		}
		?>
		<option value="true" <?php if (get_option("cp_fb_like_activate_header") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsSS'); ?></option>
		<option value="" <?php if (get_option("cp_fb_like_activate_header") == "") echo 'selected'; ?>><?php _e('No', 'AwsSS'); ?></option>
	</select> <?php _e('Enable to use in Header.', 'AwsSS'); ?>

	<div class="clear10"></div>

	<input type="checkbox" name="cp_fb_like_header" value="1" <?php if(get_option('cp_fb_like_header') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned at the top of the page, just below the banner advert.', 'AwsSS'); ?>

	<div class="clr"></div>

	<input type="checkbox" name="cp_fb_like_top" value="1" <?php if(get_option('cp_fb_like_top') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned at the top of the ad, just below the title.', 'AwsSS'); ?>

	<div class="clr"></div>

	<input type="checkbox" name="cp_fb_like_middle" value="1" <?php if(get_option('cp_fb_like_middle') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned in the middle of the ad, just above description.', 'AwsSS'); ?>

	<div class="clr"></div>

	<input type="checkbox" name="cp_fb_like_bottom" value="1" <?php if(get_option('cp_fb_like_bottom') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned at the bottom of the ad.', 'AwsSS'); ?>

	<div class="clear10"></div>

	<h4><?php _e('Layout Style', 'AwsSS'); ?></h4>
	<div class=""><?php _e('Determines the size and amount of social context next to the button', 'AwsSS'); ?></div>
	<select name="cp_fb_like_layout" id="cp_fb_like_layout">
		<option value="standard" <?php if (get_option("cp_fb_like_layout") == "standard") echo 'selected'; ?>><?php _e('Standard', 'AwsSS'); ?></option>
		<option value="button_count" <?php if (get_option("cp_fb_like_layout") == "button_count") echo 'selected'; ?>><?php _e('Button Count', 'AwsSS'); ?></option>
		<option value="box_count" <?php if (get_option("cp_fb_like_layout") == "box_count") echo 'selected'; ?>><?php _e('Box Count', 'AwsSS'); ?></option>
	</select>

	<h4><?php _e('Verb to display', 'AwsSS'); ?></h4>
	<div class=""><?php _e('The verb to display in the button. Currently only Like and Recommend are supported.', 'AwsSS'); ?></div>
	<select name="cp_fb_like_action" id="cp_fb_like_action">
		<option value="like" <?php if (get_option("cp_fb_like_action") == "like") echo 'selected'; ?>><?php _e('Like', 'AwsSS'); ?></option>
		<option value="recommend" <?php if (get_option("cp_fb_like_action") == "recommend") echo 'selected'; ?>><?php _e('Recommend', 'AwsSS'); ?></option>
	</select>

	<h4><?php _e('Font type', 'AwsSS'); ?></h4>
	<div class=""><?php _e('The font type of the plugin', 'AwsSS'); ?></div>
	<select name="cp_fb_like_font" id="cp_fb_like_font">
		<option value="arial" <?php if (get_option("cp_fb_like_font") == "arial") echo 'selected'; ?>>Arial</option>
		<option value="lucida+grande" <?php if (get_option("cp_fb_like_font") == "lucida+grande") echo 'selected'; ?>>Lucida Grande</option>
		<option value="segoe+ui" <?php if (get_option("cp_fb_like_font") == "segoe+ui") echo 'selected'; ?>>Segoe UI</option>
		<option value="tahoma" <?php if (get_option("cp_fb_like_font") == "tahoma") echo 'selected'; ?>>Tahoma</option>
		<option value="trebuchet+ms" <?php if (get_option("cp_fb_like_font") == "trebuchet+ms") echo 'selected'; ?>>Trebuchet MS</option>
		<option value="verdana" <?php if (get_option("cp_fb_like_font") == "verdana") echo 'selected'; ?>>Verdana</option>
	</select>

	<h4><?php _e('Color Scheme', 'AwsSS'); ?></h4>
	<div class=""><?php _e('The color scheme of the plugin (Default: light)', 'AwsSS'); ?></div>
	<select name="cp_fb_like_colorscheme" id="cp_fb_like_colorscheme">
	<option value="light" <?php if (get_option("cp_fb_like_colorscheme") == "light") echo 'selected'; ?>><?php _e('Light', 'AwsSS'); ?></option>
	<option value="dark" <?php if (get_option("cp_fb_like_colorscheme") == "dark") echo 'selected'; ?>><?php _e('Dark', 'AwsSS'); ?></option>
	</select>

	<?php
	$cp_fb_like_width = get_option("cp_fb_like_width");
	  	if($cp_fb_like_width == '') { $cp_fb_like_width = '450'; }

	$cp_fb_like_height = get_option("cp_fb_like_height");
	  	if($cp_fb_like_height == '') { $cp_fb_like_height = '30'; }        
	?>

	<h4><?php _e('Width', 'AwsSS'); ?></h4>
	<div class=""><?php _e('Width of the Facebook Like (Default: 450)', 'AwsSS'); ?></div>
	<input maxlength="4" size="1" type="text" name="cp_fb_like_width" id="cp_fb_like_width" value="<?= $cp_fb_like_width ?>">

	<h4><?php _e('Height', 'AwsSS'); ?></h4>
	<div class=""><?php _e('Height of the Facebook Like (Default: 30)', 'AwsSS'); ?></div>
	<input maxlength="4" size="1" type="text" name="cp_fb_like_height" id="cp_fb_like_height" value="<?= $cp_fb_like_height ?>">

	<h4><?php _e('Add the Facebook Send button?', 'AwsSS'); ?></h4>
	<div class=""><?php _e('The Send Button allows your users to easily send your content to their friends. Like Button must be enabled for this to work.', 'AwsSS'); ?></div>
	<select name="cp_fb_like_send" id="cp_fb_like_send">
		<?php 
		if ( !get_option("cp_fb_like_send") )
		{
		?>
		<option value=""><?php _e('Select', 'AwsSS'); ?></option>
		<?php
		}
		?>
		<option value="true" <?php if (get_option("cp_fb_like_send") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsSS'); ?></option>
		<option value="" <?php if (get_option("cp_fb_like_send") == "") echo 'selected'; ?>><?php _e('No', 'AwsSS'); ?></option>
	</select>

	<br />
	<br />
	<h4><?php _e('Finally, save your settings!', 'AwsSS'); ?></h4>

	<input type="submit" value="<?php _e('Save Settings', 'AwsSS'); ?>" class="button-primary">
	<input type="hidden" name="cp_fb_form_submit" value="true" />
	</form>
	<br />

	</fieldset>
           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'AwsSS'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'AwsSS'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'AwsSS'); ?></a></td>
    </tr>
    </tbody>
    </table>

<div class="clear15"></div>

<?php _e('<a style="text-decoration:none" href="http://www.arctic-websolutions.com" target="_blank"><strong>Arctic WebSolutions</strong></a> offers WordPress and ClassiPress plugins “as is” and with no implied meaning that they will function exactly as you would like or will be compatible with all 3rd party components and plugins. We do not offer support via email or otherwise support WordPress or other WordPress plugins we have not developed.', 'AwsSS'); ?>

<div class="clear15"></div>

<div class="admin_header"><?php _e( 'Other Plugins', 'AwsSS' ); ?></div>
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/bundle-pack/" target="_blank">Bundle Pack $143.90 off</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/cp-auction-social-sharing/" target="_blank">CP Auction & eCommerce</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-grid-view/" target="_blank">ClassiPress Grid View</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-projects-portfolio/" target="_blank">AWS Projects Portfolio
</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-affiliate-plugin/" target="_blank">AWS Affiliate Plugin</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-ecommerce/" target="_blank">ClassiPress eCommerce</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/simplepay4u-gateway/" target="_blank">SimplePay4u Gateway</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/grandchild-plugin/" target="_blank">AWS Grandchild Plugin</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-theme-customizer/" target="_blank">AWS Theme Customizer</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/best-offer-for-cp-auction/" target="_blank">CP Auction Best Offer</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-best-offer/" target="_blank">ClassiPress Best Offer</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-social-sharing/" target="_blank">ClassiPress Social Sharing</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-cartpauj-pm/" target="_blank">ClassiPress Cartpauj PM</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-deny-access/" target="_blank">AWS Deny Access</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-user-roles/" target="_blank">AWS User Roles</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-embed-video/" target="_blank">AWS Embed Video</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank">AWS Credit Payments</a>', 'AwsSS' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">AppThemes Critic Reviews</a>', 'AwsSS' ); ?>

<div class="clear15"></div>

<?php _e('AppThemes has released a plugin <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> which has the same rating and review functionality as you can see around in their marketplace. The only minus is that it is not modded / available for ClassiPress Theme <strong>BUT</strong>, I have a modified version which you can review on our main site or any of our child theme demos.', 'AwsSS'); ?>

<div class="clear15"></div>

<?php _e('Purchase the <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> plugin through my <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">affiliate link</a>, send me a copy of your purchase receipt and i will send you my modified version of the Critic Reviews plugin.', 'AwsSS'); ?>

	<div class="clear15"></div>

	<p><strong><?php _e('Was this plugin useful? If you like it and it is/was useful, please consider donating.. Many thanks!', 'AwsSS'); ?></strong></p>

	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="RFJB58JUE8WUJ">
	<p style="text-align: center;">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	</p>
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>

            </div>
          </div>
        </div>
     </div>
</div>
<?php
}
?>