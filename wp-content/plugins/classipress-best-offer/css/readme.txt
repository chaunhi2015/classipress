Plugin Name: ClassiPress Best Offer
Plugin URI: http://www.arctic-websolutions.com/
Description: Add Best Offer functionality to ClassiPress Classified Ads
Version: 1.0.7
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/

=================================================================================================================

To customize the plugin to other child themes create a css file with the same name as the child theme you use.
If your child theme name is ie. ClassiPress-Child then name the file, classipress-child.css (only lowercase)

See the other css files for working samples.

Please check your themes directory for correct name to use, and name your new css file with all lowercase chars.

If any help needed feel free to contact us.