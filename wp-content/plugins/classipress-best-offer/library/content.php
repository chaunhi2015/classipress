<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


function clp_bo_view_dashboard_page() {

	global $current_user, $post, $wpdb;
    	get_currentuserinfo();
	$uid = $current_user->ID;
	$site_name = get_bloginfo('name');
	$paypal = get_option( 'clp_bo_paypal' );
	$ct = strtolower(get_option('stylesheet'));
	$mssg = false; $bo_error = false; $bo_success = false;

	$counter = false; if(isset($_GET['counter'])) $counter = $_GET['counter'];

	$exp_time = get_option('clp_bo_expiration');

	if(isset($_POST['uid_counter_offer'])) {

		$ok = 1;
		if(isset($_POST['id'])) $id = $_POST['id'];
		if(isset($_POST['counter_offer_amount'])) $bid = clp_best_offer_sanitize_amount($_POST['counter_offer_amount']);
		$new_msg = false; if(isset($_POST['message'])) $new_msg = $_POST['message'];
		if( $new_msg ) $mssg = '<br /><br /><strong>'.__('Message from bidder:','clpOffer').'</strong><br />'.$new_msg.'';

		$tm = time();
		$table_name = $wpdb->prefix . "clp_best_offer";
		$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
		$pid = $get->pid;
		$cid = $get->uid;
		$uid_usage = $get->uid_usage;
		$upd_usage = $uid_usage + 1;
		$post = get_post($pid);

		if( (!is_numeric($bid)) || ( empty( $bid ) ) ) {
		$ok = 2;
		}
		if( $uid_usage >= get_option('clp_bo_no_user_counter_offer' )) {
		$ok = 3;
		}
		if( $ok == 2 ) {
		$bo_error = ''.__('Your counter offer needs to be a numeric value!','clpOffer').'';
		}
		if( $ok == 3 ) {
		$bo_error = ''.__('Sorry, you do not have permission to post more counter offers!','clpOffer').'';
		}
		if( $ok == 1 ) {
		$message = ''.$get->message.''.$mssg.'';
		$where_array = array('id' => $id);
		$wpdb->update($table_name, array('uid_counter_offer' => $bid, 'message' => stripslashes(html_entity_decode($message)), 'uid_usage' => $upd_usage, 'uid_date' => $tm, 'latest_offer' => $bid, 'date_type' => 'uid_date'), $where_array);

		$bo_success = ''.__('Your counter offer was successfully delivered!','clpOffer').'';
		clp_bo_uid_counter_offer_posted_email( $pid, $post, $cid, $bid, $new_msg );
		}
		else {
		$bo_success = ''.__('Sorry, something went wrong!','clpOffer').'';
		}
	}

		if(isset($_POST['aut_counter_offer'])) {

		$ok = 1;
		if(isset($_POST['id'])) $id = $_POST['id'];
		if(isset($_POST['counter_offer_amount'])) $bid = clp_best_offer_sanitize_amount($_POST['counter_offer_amount']);
		$new_msg = false; if(isset($_POST['message'])) $new_msg = $_POST['message'];
		if( $new_msg ) $mssg = '<br /><br /><strong>'.__('Message from seller:','clpOffer').'</strong><br />'.$new_msg.'';

		$tm = time();
		$table_name = $wpdb->prefix . "clp_best_offer";
		$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
		$pid = $get->pid;
		$cid = $get->uid;
		$aut_usage = $get->aut_usage;
		$upd_usage = $aut_usage + 1;
		$post = get_post($pid);

		if( (!is_numeric($bid)) || ( empty( $bid ) ) ) {
		$ok = 2;
		}
		if( $aut_usage >= get_option('clp_bo_no_author_counter_offer' )) {
		$ok = 3;
		}
		if( $ok == 2 ) {
		$bo_error = ''.__('Your counter offer needs to be a numeric value!','clpOffer').'';
		}
		if( $ok == 3 ) {
		$bo_error = ''.__('Sorry, you do not have permission to post more counter offers!','clpOffer').'';
		}
		if( $ok == 1 ) {
		$message = ''.$get->message.''.$mssg.'';
		$where_array = array('id' => $id);
		$wpdb->update($table_name, array('aut_counter_offer' => $bid, 'message' => stripslashes(html_entity_decode($message)), 'aut_usage' => $upd_usage, 'aut_date' => $tm, 'latest_offer' => $bid, 'date_type' => 'aut_date'), $where_array);

		$bo_success = ''.__('Your counter offer was successfully delivered!','clpOffer').'';
		clp_bo_aut_counter_offer_posted_email( $pid, $post, $cid, $bid, $new_msg );
		}
		else {
		$bo_error = ''.__('Sorry, something went wrong!','clpOffer').'';
		}
}
?>

<div id="container">
	<div id="content" role="main">

<div class="aws-box">

	    	<?php if( $bo_error ) echo '<div class="notice error">'.$bo_error.'</div><div class="clear10"></div>'; ?>
	    	<?php if( $bo_success ) echo '<div class="notice success">'.$bo_success.'</div><div class="clear10"></div>'; ?>

	<p><?php echo sprintf(__('In the field Status you will get the result, accepted, rejected, or counteroffer displayed like this, ie. <strong>Amount | Accept</strong>. All offers are valid for <strong>%s</strong> hours so if you do not do anything against the received counter offer within the time deadline the counteroffer will expire.', 'clpOffer'), $exp_time ); ?></p>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
	<thead>
	<tr>
	<th class="footable-first-column" style="width:90px"><div style="text-align: left;">&nbsp; <?php _e('Date', 'clpOffer'); ?></div></th>
	<th style="width:100px"><div style="text-align: left;"><?php _e('Ads', 'clpOffer'); ?></div></th>
	<th data-hide="phone" style="width:60px"><div style="text-align: left;"><?php _e('Author', 'clpOffer'); ?></div></th>
	<th data-hide="phone" style="width:95px"><div style="text-align: center;"><?php _e('Offer', 'clpOffer'); ?></div></th>
	<th data-hide="phone" style="width:180px"><div style="text-align: center;"><?php _e('Status', 'clpOffer'); ?></div></th>
	<th class="footable-last-column" data-hide="phone" style="width:140px"><div style="text-align: center;"><?php _e('Action', 'clpOffer'); ?></div></th>
	</tr>
	</thead>
	<tbody>

<?php

	$table_name = $wpdb->prefix . "clp_best_offer";
	$numposts = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND type IN ('bestoffer', 'boffer-item','boffer-ads')");
	$i=0;
	$exs_bid = false;
	if( $numposts ) {
	$rowclass = '';
        foreach( $numposts as $num ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$i++;

	$id = $num->id;
	$pid = $num->pid;
	$datemade = $num->datemade;
	$bid = $num->bid;
	$uid = $num->uid;
	$uid_counter_offer = $num->uid_counter_offer;
	$aut_counter_offer = $num->aut_counter_offer;
	$latest_offer = $num->latest_offer;
	$acceptance = $num->acceptance;
	$status = $num->status;

?>

<div id="msg<?php echo $num->id; ?>" class="wanted">
	<div>
	<p align="left"><strong><?php _e('Product Description', 'clpOffer'); ?></strong></p>
	<?php if( empty($num->message) ) echo ''.__('Sorry, there have been no description of the product.','clpOffer').''; else echo $num->message; ?>
	<p align="right">[<a href="#" onclick="msg(<?php echo $num->id; ?>)"><?php _e('CLOSE', 'clpOffer'); ?></a>]</p>
	</div>
</div>

<?php

	global $post, $pos, $cp_options;
	$locale = $cp_options->currency_code;
	$post = get_post( $pid );
	if( $num->product_pid > 0 ) $pos = get_post( $num->product_pid );
	$date = date_i18n('d M. y', $num->datemade, true);
	$user = get_userdata($post->post_author);
	$receiver = get_user_meta($post->post_author, 'paypal_email', true);
	if( $aut_counter_offer > 0 ) $coffer = ''.cp_display_price( $aut_counter_offer, 'ad', false ).' | <a href="#" class="accept-offer" title="'.__('Accept their counter offer!', 'clpOffer').'" id="'.$id.'">'.__('Accept', 'clpOffer').'</a><br /><a title="'.__('Submit a counter offer!', 'clpOffer').'" href="?id='.$id.'&counter=1">'.__('Counter Offer', 'clpOffer').'</a>';
	else $coffer = ''.__('Pending', 'clpOffer').'';
	if( $acceptance ) $coffer = $acceptance;
	elseif(( empty($aut_counter_offer) ) && ( empty($acceptance) )) $coffer = ''.__('Pending', 'clpOffer').'';
	if( $coffer == "Declined" ) $coffer = '<span class="declined">'.__('Rejected', 'clpOffer').'</span>';
	if( $coffer == "Accepted" ) $coffer = '<span class="accepted">'.__('Accepted', 'clpOffer').'</span>';
	if(( empty( $acceptance )) && ( $num->uid_date > $num->aut_date )) $coffer = ''.__('Pending', 'clpOffer').'';
	if( $uid_counter_offer > $bid ) $bid = $uid_counter_offer;
	if( get_post_status( $pid ) != "publish" ) $coffer = ''.__('Listing Closed', 'clpOffer').'';

	if( $num->bid > 0 ) $exs_bid = '<br />+ '.cp_display_price( $bid, 'ad', false ).'';

	if( $num->product_pid > 0 )
	$pro_title = '<a title="'.$pos->post_title.'" href="'.get_permalink($num->product_pid).'" target="_blank">'.clp_truncate(strip_tags($pos->post_title), 15, '..').'</a>';
	$ads_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'">'.clp_truncate(strip_tags($post->post_title), 15, '..').'</a>';
	$author = '<a title="'.$user->user_login.'" href="#">'.clp_truncate(strip_tags($user->user_login), 12, '..').'</a>';

	if( $num->latest_offer > 0 ) $win_bid = $num->latest_offer;
	else $win_bid = $num->bid;

   echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-left">'.$date.'</td>';
	if(( $acceptance == "Accepted" ) || ( get_post_status( $num->pid ) != "publish" ))
	echo '<td class="text-left">'.clp_truncate(strip_tags($post->post_title), 20).'</td>';
	else
	echo '<td class="text-left">'.$ads_title.'</td>';
	echo '<td class="text-left">'.$author.'</td>';
	if( $num->item_title )
	echo '<td class="text-center"><a href="#" title="'.__('Click here to read the message!', 'clpOffer').'" onclick="msg(\''.$num->id.'\')">'.clp_truncate(strip_tags($num->item_title), 15, '..').'</a><br />+ '.cp_display_price( $bid, 'ad', false ).'</td>';
	elseif( $num->product_pid > 0 )
	echo '<td class="text-center">'.$pro_title.''.$exs_bid.'</td>';
	else
	echo '<td class="text-right">'.cp_display_price( $bid, 'ad', false ).'</td>';
	if( $num->message )
	echo '<td class="text-center">'.$coffer.'<br /><a href="#" title="'.__('Click here to read the message!', 'clpOffer').'" onclick="msg(\''.$num->id.'\')">'.__('Message', 'clpOffer').'</a></td>';
	else
	echo '<td class="text-center">'.$coffer.'</td>';
	if( $acceptance == "Accepted" && $receiver == true && $status != "Paid" )
	echo '<td class="text-center"><a href="https://www.paypal.com/cgi-bin/webscr?on0=Buyer&os0='.$current_user->user_login.'&on1=Reference&os1='.$site_name.'&amount='.$latest_offer.'&item_name='.$post->post_title.'&cmd=_xclick&business='.$receiver.'&no_shipping=1&currency_code='.$locale.'&lc=EN" target="_blank">'.__('Pay Now', 'clpOffer').'</a></td>
	</tr>';
	else
	echo '<td class="text-center">'; if( $status == "Paid" ) { echo ''.__('Paid','clpOffer').' | '; } echo '<a href="#" class="delete-offer" title="'.__('Delete your offer!', 'clpOffer').'" id="'.$id.'">'.__('Delete', 'clpOffer').'</a></td>
	</tr>';

}
} else {
   echo '<tr><td colspan="6">'.__('No Records Found!', 'clpOffer').'</td></tr>';
   }
	?>
	</tbody>
	</table>

	<?php if( $counter == 1 ) {
	if(isset($_GET['id'])) $id = $_GET['id'];
	global $wpdb;
	$table_name = $wpdb->prefix . "clp_best_offer";
	$type = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'"); ?>
	<div class="clear20"></div>
	<form method="post" action="?id=<?php echo $id; ?>&counter=1">
	<input type="hidden" name="id" value="<?php echo $id; ?>">
	<div class="counter_wrap">
                <div class="item_detail my_bo">
                <div class="item_detail1"><label for="my_bo">&nbsp; <?php _e('Amount:', 'clpOffer'); ?></label></div>
                <div class="item_detail2"><input type="text" name="counter_offer_amount" id="my_bo" value="" tabindex="1" /></div>
                </div>
                <?php if( $type->type != "bestoffer" ) { ?>
                <div class="clear5"></div>
                <div class="item_detail my_bo">
                <div class="item_detail bid"> &nbsp; <label for="message"><?php _e('Enter a message:', 'clpOffer'); ?></label>
        	<textarea name="message" id="message" tabindex="2"></textarea>
		</div>
		</div>
		<?php } ?>

		<div class="clear10"></div>

                <input type="submit" name="uid_counter_offer" value="<?php _e('Submit Counter Offer!', 'clpOffer'); ?>" class="mbtn btn_orange aws_button" tabindex="3" />

                </div></form>
	<?php } ?>

<script type="text/javascript">
function msg(id) {
	el = document.getElementById("msg"+id);
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>

	<div class="clear50"></div>

<h1 class="single dotted"><?php _e('Offers Received', 'clpOffer'); ?></h1>

<p><?php echo sprintf(__('You can accept the Best Offer and end the listing, reject the Best Offer, or respond with a counter offer. If the buyer doesn`t respond within the time deadline of <strong>%s</strong> hours the counter offer will expire.', 'clpOffer'), $exp_time ); ?></p>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
	<thead>
	<tr>
	<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Date', 'clpOffer'); ?></div></th>
	<th data-hide="phone" style="width:100px"><div style="text-align: left;"><?php _e('Product', 'clpOffer'); ?></div></th>
	<th data-hide="phone" style="width:60px"><div style="text-align: left;"><?php _e('Bidder', 'clpOffer'); ?></div></th>
	<th data-hide="phone" style="width:95px"><div style="text-align: center;"><?php _e('Offer', 'clpOffer'); ?></div></th>
	<th data-hide="phone" style="width:180px"><div style="text-align: center;"><?php _e('Counter Offer', 'clpOffer'); ?><br /><div class="small_text2"><?php _e('Bidder | Author', 'clpOffer'); ?></div></div></th>
	<th class="footable-last-column" data-hide="phone" style="width:140px"><div style="text-align: center;"><?php _e('Action', 'clpOffer'); ?></div></th>
	</tr>
	</thead>
	<tbody>

<?php
	$ok = 0;
	$ex_bid = false;
	$has_posts = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_author = '$uid' AND post_status = 'publish'");
	$i=0;
	if( $has_posts ) {
	$rowclass = '';
        foreach( $has_posts as $has ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$i++;
	$post_id = $has->ID;

	$table_bids = $wpdb->prefix . "clp_best_offer";
	$result = $wpdb->get_results("SELECT * FROM {$table_bids} WHERE pid = '$post_id' AND type IN ('bestoffer', 'boffer-item','boffer-ads')");

	if( $result ) {
	foreach( $result as $res ) {
	$ok++;
	$ids		= $res->id;
	$pid		= $res->pid;
	$datemade	= $res->datemade;
	$bid 		= $res->bid;
	$cid 		= $res->uid;
	$uid_counter_offer = $res->uid_counter_offer;
	$aut_counter_offer = $res->aut_counter_offer;
	$acceptance	= $res->acceptance;
	$status = $res->status;

?>

<div id="msg<?php echo $res->id; ?>" class="wanted">
	<div>
	<p align="left"><strong><?php _e('Product Description', 'clpOffer'); ?></strong></p>
	<?php if( empty($res->message) ) echo ''.__('Sorry, there have been no description of the product.','clpOffer').''; else echo $res->message; ?>
	<p align="right">[<a href="#" onclick="msg(<?php echo $res->id; ?>)"><?php _e('CLOSE', 'clpOffer'); ?></a>]</p>
	</div>
</div>

<?php

	global $post, $pos;
	$post = get_post( $pid );
	if( $res->product_pid > 0 ) $pos = get_post( $res->product_pid );
	$date = date_i18n('d M. y', $res->datemade, true);
	$user = get_userdata($cid);
	if( $res->product_pid > 0 )
	$pr_title = '<a title="'.$pos->post_title.'" href="'.get_permalink($res->product_pid).'" target="_blank">'.clp_truncate(strip_tags($pos->post_title), 12, '..').'</a>';
	$ad_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'">'.clp_truncate(strip_tags($post->post_title), 12, '..').'</a>';
	if( get_option('clp_bo_contact_email') == "account" || get_option('clp_bo_contact_email') == "both" )
	$bidder = '<a title="'.$user->user_login.'" href="mailto:'.$user->user_email.'">'.clp_truncate(strip_tags($user->user_login), 12, '..').'</a>';
	else $bidder = '<a title="'.$user->user_login.'" href="#">'.clp_truncate(strip_tags($user->user_login), 12, '..').'</a>';

	if( $res->bid > 0 ) $ex_bid = '<br />+ '.cp_display_price( $bid, 'ad', false ).'';

   echo '<tr id="'.$ids.'" class="'.$rowclass.'">
	<td class="text-left">'.$date.'</td>
	<td class="text-left">'.$ad_title.'</td>
	<td class="text-left">'.$bidder.'</td>';
	if( $res->item_title )
	echo '<td class="text-center"><a href="#" title="'.__('Click here to read the message!', 'clpOffer').'" onclick="msg(\''.$res->id.'\')">'.clp_truncate(strip_tags($res->item_title), 12, '..').'</a><br />+ '.cp_display_price( $bid, 'ad', false ).'</td>';
	elseif( $res->product_pid > 0 )
	echo '<td class="text-center">'.$pr_title.''.$ex_bid.'</td>';
	else
	echo '<td class="text-right">'.cp_display_price( $bid, 'ad', false ).'</td>';
	if( $res->message )
	echo '<td class="text-center">'.cp_display_price( $uid_counter_offer, 'ad', false ).' | '.cp_display_price( $aut_counter_offer, 'ad', false ).'<br /><a href="#" title="'.__('Click here to read the message!', 'clpOffer').'" onclick="msg(\''.$res->id.'\')">'.__('Message', 'clpOffer').'</a></td>';
	else
	echo '<td class="text-center">'.cp_display_price( $uid_counter_offer, 'ad', false ).' | '.cp_display_price( $aut_counter_offer, 'ad', false ).'</td>';
	if( $acceptance == "Accepted" ) {
	echo '<td class="text-center"><span class="accepted">'.__('Accepted', 'clpOffer').''; if( $status != "Paid" ) { echo '<br /><a href="#" class="mark-paid" title="'.__('Mark the accepted offer as paid!', 'clpOffer').'" id="'.$ids.'">'.__('Mark as Paid', 'clpOffer').'</a>'; } elseif( $status == "Paid" ) { echo ' | '.__('Paid','clpOffer').''; } echo '</span></td>';
	} elseif( $acceptance == "Declined" )
	echo '<td class="text-center"><span class="declined">'.__('Rejected', 'clpOffer').'</span></td>';
	elseif( $res->aut_date > $res->uid_date )
	echo '<td class="text-center"><a href="#" class="decline-offer" title="'.__('Reject their offer!', 'clpOffer').'" id="'.$ids.'">'.__('Reject', 'clpOffer').'</a></td>';
	else
	echo '<td class="text-center"><a href="#" class="accept-offer" title="'.__('Accept their offer and end your listing!', 'clpOffer').'" id="'.$ids.'">'.__('Accept', 'clpOffer').'</a> | <a href="#" class="decline-offer" title="'.__('Reject their offer!', 'clpOffer').'" id="'.$ids.'">'.__('Reject', 'clpOffer').'</a><br /><a title="'.__('Submit a counter offer!', 'clpOffer').'" href="?ids='.$ids.'&counter=2">'.__('Counter Offer', 'clpOffer').'</a></td>
	</tr>';
	}
	}
}
}
	if(( empty( $has_posts ) ) || ( $ok == 0 )) {
   	echo '<tr><td colspan="6">'.__('No Records Found!', 'clpOffer').'</td></tr>';
	}
	?>
	</tbody>
	</table>

	<?php if( $counter == 2 ) {
	if(isset($_GET['ids'])) $ids = $_GET['ids'];
	global $wpdb;
	$table_name = $wpdb->prefix . "clp_best_offer";
	$type = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$ids'"); ?>
	<div class="clear20"></div>
	<form method="post" action="?ids=<?php echo $ids; ?>&counter=2">
	<input type="hidden" name="id" value="<?php echo $ids; ?>">
	<div class="counter_wrap">
                <div class="item_detail my_bo">
                <div class="item_detail1"><label for="my_bo">&nbsp; <?php _e('Amount:', 'clpOffer'); ?></label></div>
                <div class="item_detail2"><input type="text" name="counter_offer_amount" id="my_bo" value="" tabindex="1" /></div>
                </div>
                <?php if( $type->type != "bestoffer" ) { ?>
                <div class="clear5"></div>
                <div class="item_detail my_bo">
                <div class="item_detail bid"> &nbsp; <label for="message"><?php _e('Enter a message:', 'clpOffer'); ?></label>
        	<textarea name="message" id="message" tabindex="2"></textarea>
		</div>
		</div>
		<?php } ?>

		<div class="clear10"></div>

                <input type="submit" name="aut_counter_offer" value="<?php _e('Submit Counter Offer!', 'clpOffer'); ?>" class="mbtn btn_orange aws_button" tabindex="3" />

                </div></form>
	<?php } ?>

	<div class="clear20"></div>

			</div><!-- /aws-box -->

	<div class="clear20"></div>

	</div><!-- /container -->
	</div><!-- /content - main -->

	<div class="clr"></div>

<?php
}
add_shortcode('clp-bo-dashboard-page', 'clp_bo_view_dashboard_page');
add_action('classipress_best_offer_panel', 'clp_bo_view_dashboard_page');