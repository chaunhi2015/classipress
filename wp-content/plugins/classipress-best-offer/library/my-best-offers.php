<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

<head>

	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />

	<title><?php wp_title(''); ?></title>

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php echo appthemes_get_feed_url(); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<?php appthemes_before(); ?>

	<div class="container">

		<?php appthemes_before_header(); ?>
		<?php get_header( app_template_base() ); ?>
		<?php appthemes_after_header(); ?>

<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<div id="breadcrumb">

				<?php if ( function_exists('cp_breadcrumb') ) cp_breadcrumb(); ?>

			</div>

			<div class="content_left">

				<?php appthemes_before_page_loop(); ?>

				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php appthemes_before_page(); ?>

						<div class="shadowblock_out">

							<div class="shadowblock">

								<div class="post">

									<?php appthemes_before_page_title(); ?>

									<h1 class="single dotted"><?php the_title(); ?></h1>

									<?php appthemes_after_page_title(); ?>

									<?php appthemes_before_page_content(); ?>

									<?php the_content(); ?>

									<?php appthemes_after_page_content(); ?>

									<div class="prdetails">

										<?php edit_post_link( '<p class="edit">' . __( 'Edit Page', APP_TD ), '', '' ) . '</p>'; ?>

									</div>

								</div><!--/post-->

							</div><!-- /shadowblock -->

						</div><!-- /shadowblock_out -->

						<?php appthemes_after_page(); ?>

					<?php endwhile; ?>

					<?php appthemes_after_page_endwhile(); ?>

				<?php else: ?>

					<?php appthemes_page_loop_else(); ?>

					<p><?php _e( 'Sorry, no pages matched your criteria.', APP_TD ); ?></p>

				<?php endif; ?>

				<div class="clr"></div>

				<?php appthemes_after_page_loop(); ?>

			</div><!-- /content_left -->

			<?php get_sidebar( 'user' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->

		<?php appthemes_before_footer(); ?>
		<?php get_footer( app_template_base() ); ?>
		<?php appthemes_after_footer(); ?>

	</div><!-- /container -->

	<?php wp_footer(); ?>

	<?php appthemes_after(); ?>

</body>

</html>