<?php

/*
Plugin Name: ClassiPress Best Offer
Plugin URI: http://www.arctic-websolutions.com/
Description: Add Best Offer functionality to ClassiPress Classified Ads
Version: 1.1.8
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

/*
Copyright 2012-2015 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


if ( ! defined( 'CLP_BO_VERSION' ) ) {
	define( 'CLP_BO_VERSION', '1.1.8' );
}

if ( ! defined( 'CLP_BO_LATEST_RELEASE' ) ) {
	define( 'CLP_BO_LATEST_RELEASE', '15 October 2015' );
}

if ( ! defined( 'CLP_BO_PLUGIN_FILE' ) ) {
	define( 'CLP_BO_PLUGIN_FILE', __FILE__ );
}

/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION - EASY TRANSLATION USING CODESTYLING LOCALIZATION
|--------------------------------------------------------------------------
*/

function clp_bo_textdomain() {
	load_plugin_textdomain( 'clpOffer', false, dirname( plugin_basename( CLP_BO_PLUGIN_FILE ) ) . '/languages/' );
}
add_action( 'init', 'clp_bo_textdomain' );


function clp_bo_new_menu_items() {
	global $clp_bo_admin_menu;
	$clp_bo_admin_menu = add_submenu_page( 'edit.php?post_type=ad_listing', __( 'ClassiPress Best Offer', 'clpOffer' ), __( 'Best Offer', 'clpOffer' ), 'manage_options', 'clp-bestoffer', 'clp_best_offer_settings' );
}
add_action( 'admin_menu', 'clp_bo_new_menu_items', 10 );


/*
|--------------------------------------------------------------------------
| DASHBOARD HOOKS
|--------------------------------------------------------------------------
*/

function classipress_best_offer_panel() {
	do_action( 'classipress_best_offer_panel' );
}


/*
|--------------------------------------------------------------------------
| BEST OFFER HOOKS
|--------------------------------------------------------------------------
*/

function classipress_best_offer_here() {
	do_action( 'classipress_best_offer_here' );
}

function clp_bo_bidder_box_tabbed() {
	do_action( 'clp_bo_bidder_box_tabbed' );
}


include_once('includes/functions.php');
include_once('includes/actions.php');
include_once('includes/ajax-functions.php');
include_once('includes/step-functions.php');
include_once('includes/activate.php');
include_once('includes/enqueue-scripts.php');
include_once('emails/emails.php');
include_once('includes/clp-settings.php');
include_once('includes/create-pages.php');
include_once('library/content.php');

function clp_best_offer_url() { return get_bloginfo('wpurl')."/wp-content/plugins/classipress-best-offer"; }


/*
|--------------------------------------------------------------------------
| THEME TEMPLATE REDIRECTS
|--------------------------------------------------------------------------
*/

add_action( 'template_redirect', 'clp_bo_template_redirect', 1 );

function clp_bo_template_redirect() {

	    	global $wp_query, $wp;
	    	$wp_query->is_404 = false;

		$tmp = explode('?',$_SERVER["REQUEST_URI"]);
		$uri = array_shift($tmp);
		$uri = trim($uri,"/");
		if( $uri == "my-best-offers" ) {
		appthemes_auth_redirect_login(); // if not logged in, redirect to login page
		nocache_headers();
		add_filter( 'the_title', 'clp_bo_boffer_title' );
		function clp_bo_boffer_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('My Best Offers', 'clpOffer').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/classipress-best-offer/library/my-best-offers.php');
			exit;
		}
}


/*
|--------------------------------------------------------------------------
| PLACE AN BEST OFFER OPTION IN SIDEBAR
|--------------------------------------------------------------------------
*/

class CLP_BestOffer_Widget extends WP_Widget {

function __construct() {
        $widget_ops = array( false, 'description' => __('Display an best offer option box in the sidebar.','clpOffer') );
        parent::__construct( false, 'ClassiPress Best Offer', $widget_ops );
}

function widget( $args, $instance ) {

	global $current_user, $post, $wpdb;
    	get_currentuserinfo();
	$uid = $current_user->ID;

	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );

if( $bestoffer == true && get_option('clp_bo_use_widget') == "yes" && is_singular(APP_POST_TYPE) ) {

		extract($args);
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
			else
		echo $before_title; ?><?php $arr = get_option('AwsBestOfferBox_widget'); echo $arr['title']; ?><?php echo $after_title; ?>

	<div style="margin:10px 0;overflow:hidden">

	<?php clp_bestoffer_in_widget(); ?>

	</div><!-- /style -->

<?php
		echo $after_widget;
}
}

function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

function form( $instance ) {
 $options = get_option("AwsBestOfferBox_widget");
  if (!is_array( $options ))
	{

	$options = array(
	'title' => ''.__('Make an Offer','clpOffer').''
      );
  	}

  if (isset($_POST['submit']))
  {

    $options['title'] = htmlspecialchars($_POST['WidgetTitle']);
    update_option("AwsBestOfferBox_widget", $options);

  }

?>

  <table width="100%">
  <tr>
  <td>
    <label for="WidgetTitle"><?php _e('Widget Title:', 'clpOffer'); ?> </label></td>
   <td> <input type="text" id="WidgetTitle" name="WidgetTitle" value="<?php echo $options['title'];?>" /></td>
   </tr>

    </table>

    <input type="hidden" id="submit" name="submit" value="1" />
  </p>

<?php
}
}
add_action('widgets_init', create_function('', 'register_widget("CLP_BestOffer_Widget");'));


/*
|--------------------------------------------------------------------------
| SCHEDULE CRON JOBS
|--------------------------------------------------------------------------
*/

add_action( 'wp', 'clp_bo_setup_schedule' );
/**
 * On an early action hook, check if the hook is scheduled - if not, schedule it.
 */
function clp_bo_setup_schedule() {
	if ( ! wp_next_scheduled( 'clp_bo_hourly_event' ) ) {
		wp_schedule_event( time(), 'hourly', 'clp_bo_hourly_event');
	}
}

add_action( 'clp_bo_hourly_event', 'clp_bo_do_this_hourly' );
/**
 * On the scheduled action hook, run a function.
 */
function clp_bo_do_this_hourly() {
	// do something every hour
	global $post, $wpdb;
	$exp_valid = get_option('clp_bo_exp_valid');
	$exp_time = get_option('clp_bo_expiration');
	$nowtime = date_i18n("Y-m-d H:i:s");
	$table_name = $wpdb->prefix . "clp_best_offer";
	$numposts = $wpdb->get_results("SELECT * FROM {$table_name} WHERE type IN ('bestoffer', 'boffer-item','boffer-ads') AND acceptance = ''");

	if( $numposts ) {
        foreach( $numposts as $num ) {

	$id = $num->id;
	$pid = $num->pid;
	$cid = $num->uid;
	$post = get_post($pid);
	$bid = $num->latest_offer;
	$date_type = $num->date_type;
	$last_date = $num->$date_type;
	$lastdate = date_i18n('Y-m-d H:i:s', $last_date, true);
	$exp_date = date_i18n('Y-m-d H:i:s', strtotime($lastdate . ' + '.$exp_time.' hours'));

	if( $nowtime > $exp_date ) {
		if( ( $exp_valid == "yes" ) && ( $num->uid_counter_offer > 0 || $num->aut_counter_offer > 0 ) ) {
		clp_bo_boffer_has_expired_email( $pid, $post, $cid, $bid );
		$table_name = $wpdb->prefix . "clp_best_offer";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d' AND date_type != 'datemade'", $id));
		} else if ( $exp_valid != "yes" ) {
		clp_bo_boffer_has_expired_email( $pid, $post, $cid, $bid );
		$table_name = $wpdb->prefix . "clp_best_offer";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));
		}
	}
	}
}
}


/*
|--------------------------------------------------------------------------
| ACTIVATE / ENABLE CLASSIPRESS BEST OFFER PLUGIN
|--------------------------------------------------------------------------
*/

function clp_best_offer_plugin_activate() {

    	clp_bo_plugin_activate();
    	clp_bo_create_page();

	$info_text = get_option('clp_bo_info_text');
	$item_help = get_option('clp_bo_item_help');
	$amount_help = get_option('clp_bo_amount_help');
	$buy_text = get_option('clp_bo_buy_text');
	if( empty( $info_text ) ) {
	update_option( 'clp_bo_info_text', '<strong>THE BEST OFFER</strong> - This option allows you to offer to purchase this item at the price you suggest, you can specify a product you want to swap with, or you can offer to swap with one of your already announced products. The seller can then accept your offer, reject it, or make a counteroffer.' );
	}
	if( empty( $item_help ) ) {
	update_option( 'clp_bo_item_help', 'Enter the Product Name and a short description of the product you want to trade with. Or <a href="http://cp-best-offer.wp-build.com/add-new/">click here</a> to create an ad.' );
	}
	if( empty( $amount_help ) ) {
	update_option( 'clp_bo_amount_help', 'Improve your offer, add an amount in addition to the specified product.' );
	}
	if( empty( $buy_text ) ) {
	update_option( 'clp_bo_buy_text', 'Or use the opportunity below to ensure that you get this product, click the button to purchase this product now.' );
	}


	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}
register_activation_hook( __FILE__, 'clp_best_offer_plugin_activate' );


/*
|--------------------------------------------------------------------------
| DEACTIVATE & UNINSTALL CLASSIPRESS BEST OFFER PLUGIN
|--------------------------------------------------------------------------
*/

function clp_boffer_deactivate_actions() {

	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}
register_deactivation_hook( __FILE__, 'clp_boffer_deactivate_actions' );


function clp_boffer_uninstall_actions() {

	require_once dirname( __FILE__ ) . '/includes/clp-uninstall.php';
}
if( get_option('clp_bo_uninstall') == "yes" )
register_uninstall_hook( __FILE__, 'clp_boffer_uninstall_actions' );

?>