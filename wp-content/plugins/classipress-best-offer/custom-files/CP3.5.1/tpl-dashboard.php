<?php
/**
 * Template Name: User Dashboard
 *
 * @package ClassiPress\Templates
 * @author  AppThemes
 * @since   ClassiPress 3.0
 */
?>

<div class="content user-dashboard">

	<div class="content_botbg">

		<div class="content_res">

			<!-- left block -->
			<div class="content_left">

					<div class="tabcontrol">

					<ul class="tabnavig">
						<li><a href="#block1"><span class="big"><?php _e( 'My Classifieds', APP_TD ); ?></span></a></li>
						<li><a href="#block2"><span class="big"><?php _e( 'My Best Offers', APP_TD ); ?></span></a></li>
					</ul>

					<div id="block1">

					<div class="undertab"><span class="big"><strong><span class="colour"><?php _e( 'My Classified Ads', APP_TD ); ?></span></strong></span></div>

				<div class="shadowblock_out">

					<div class="shadowblock">

						<?php do_action( 'appthemes_notices' ); ?>

						<div id="ads" class="ads_section">
							<?php get_template_part('dashboard-ads'); ?>
						</div>

					</div><!-- /shadowblock -->

				</div><!-- /shadowblock_out -->

					</div><!-- /block1 -->

					<div id="block2">

					<div class="undertab"><span class="big"><strong><span class="colour"><?php _e( 'My Best Offers', APP_TD ); ?></span></strong></span></div>

				<div class="shadowblock_out">

					<div class="shadowblock">

						<?php classipress_best_offer_panel(); ?>

					</div><!-- /shadowblock -->

				</div><!-- /shadowblock_out -->

					</div><!-- /block2 -->

					</div><!-- /tabcontrol -->

			</div><!-- /content_left -->

			<?php get_sidebar( 'user' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
