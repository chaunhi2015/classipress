Plugin Name: ClassiPress Best Offer
Plugin URI: http://www.arctic-websolutions.com/
Description: Add Best Offer functionality to ClassiPress Classified Ads
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/

---------------------------------------------------------------------------------------------------

=== Setup Instructions ===
 - Locate the tpl-dashboard.php file in the classipress directory, wp-content/themes/classipress/tpl-dashboard.php
 - Put your site into maintenace mode and rename that file to eg. tpl-dashboard.phpBAK
 - Copy the file tpl-dashboard.php from wp-content/plugins/classipress-best-offer/custom-files/CP_VERSION into the
   classipress directory to replace the old dashboard page. (Make sure you use the correct file in accordance with your CP version).

That's it...

Please note that child themes may use their own custom dashboards.