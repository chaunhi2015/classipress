=== ClassiPress Best Offer ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: wordpress, classipress, best offer, classipress theme
Requires at least: 3.9.2
Tested up to: 4.3.1
Stable tag: 1.1.8
License: GPL
License URI: http://www.gnu.org/licenses/gpl.html

---------------------------------------------------------------------------------------------------

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://offer.wp-build.com) - Testdrive every aspect of the plugin.
* [Manuals & Support](http://www.arctic-websolutions.com/cp-best-offer/) - Setup instructions and support.

=== Some Admin Features ===

* Set which offer types should be available for bidder, cash, product or select product from already announced ad.
* Set which options available for the bidder, offer an amount, offer a product, offer a advertised product and combine cash & product.
* Set how many times any user can post a best offer on the same listing.
* Set how many times bidder can post a counter offer on the same listing.
* Set how many times author can post a counter offer on the same listing.
* Set best offer expiration, 6 to 72 hours.
* Set counteroffer expiration.
* Best offer option in widget.
* Best offer option in the top of page tabbed widget.
* Action hooks for better placement.

=== Some User Features ===

* Users can post best offers, offer an amount, offer a product, offer a advertised product and combine cash & product.
* Author can enable/disable best offers on their posts.
* Both buyer and author can post counter offer.

=== Installation ===

* Download ClassiPress Best Offer.
* Extract the downloaded .zip file. (Not necessary if filename dosen’t say so, ie. unzip-first)
* Login to your websites FTP and navigate to the “wp-contents/plugins/” folder.
* Upload the resulting ”aws-best-offer.zip” or “classipress-best-offer-X.X.X.zip” folder to the websites plugin folder.
* Go to your websites Dashboard, access the “plugins” section on the left hand menu.
* Locate “ClassiPress Best Offer” in the plugin list and click “activate”.

Or, install using WP`s plugin installation tool.
Make sure to backup your translations before any upgrade.

Visit Ads -> Best Offer, configure any options as desired. That’s it!

NOTES: Included in the plugin package there is also a folder named custom-files, in this folder we have included a customized dashboard file (tpl-dashboard.php) which will display authors ads and best offers in separate tabs. Please see the readme file for how this customized dashboard can be setup, the readme file is located in the same folder.

A new page has been created, this page displays authors and users best offer stats. If you choose to NOT setup the customized dashboard then go to appearance -> menus and create a link to this page in the user options, read more below.

If you have not already setup a custom User Options menu you should do so by going to Apperance -> Menus. Create a new menu and name it eg. Dashboard, then go to Manage Locations and attach the new menu you just created to the Theme Dashboard and save. You now have a new customized User Options menu ready to set up with your best offer link. 


=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.