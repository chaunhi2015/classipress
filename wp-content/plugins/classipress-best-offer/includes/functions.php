<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| ENABLE BEST OFFER FROM SINGLE POST IF FEATURED
|--------------------------------------------------------------------------
*/

add_action( 'appthemes_after_post_content', 'clp_bo_post_content', 15 );
function clp_bo_post_content() {
	global $post, $current_user;

	$boffer = get_post_meta( $post->ID, 'cp_bestoffer', true );

	if ( $current_user->ID != $post->post_author )
		return false;

	if ( ! is_singular( APP_POST_TYPE ) || ! is_sticky( $post->ID ) )
		return false;

	if ( $boffer ) return false;
?>
	<p class="best-offer">
		<span class="best-offer-icon"></span>
		<?php echo clp_bo_feature_ad_html( $post ); ?>
	</p>
<?php
}

function clp_bo_feature_ad_html( $post, $inner = '', $class = '' ) {

	$nonce = false;

	$title = esc_attr__( 'Enable best offer on your ad', 'clpOffer' );
	if ( empty( $inner ) ) {
		$inner = __( 'Enable Best Offer', 'clpOffer' );
	}
	$nonce = clp_bo_get_nonce( $post );
	$html = '<a class="best-offer-link ' . $class . '" href="#" data-rel="' . $post->ID . '" data-nonce="' . $nonce . '" title="' . $title . '">' . $inner . '</a>';

		return $html;

}

function clp_bo_get_nonce( $post ) {
	return wp_create_nonce( 'best-offer-' .  $post->ID );
}

add_action( 'wp_ajax_best_offer', 'ajax_enable_best_offer' );
function ajax_enable_best_offer() {

	if ( ! isset( $_GET['post_id'] ) ) {
		die();
	}
	$post_id = $_GET['post_id'];
	check_ajax_referer( 'best-offer-' . $post_id, 'security' );

	$post = get_post( $post_id );

	if ( ! $post ) {
		die();
	}

	$ret = clp_bo_enable_best_offer( $post_id );
	$response = array( 'success' => $ret );

	die( json_encode( $response ) );

	}

function clp_bo_enable_best_offer( $post_id ) {

	$post_id = absint( $post_id );
	if ( ! $post_id || ! is_sticky( $post_id ) ) {
		return false;
	}

	update_post_meta( $post_id, 'cp_bestoffer', __( 'Tick to enable', 'clpOffer' ) );

	return true;

}


/*
|--------------------------------------------------------------------------
| ENABLE OR DISABLE BEST OFFER FROM QUICK EDIT ADMIN
|--------------------------------------------------------------------------
*/

if ( get_option( 'cp_version' ) < '3.4' ) {
	add_filter( 'manage_ad_listing_posts_columns', 'clp_bo_manage_ads_columns', 11 );
} else {
	add_filter( 'manage_posts_columns', 'clp_bo_manage_posts_columns', 10, 2 );
}

function clp_bo_manage_ads_columns( $columns ) {

		$new_columns = array();
			
			foreach( $columns as $key => $value ) {
			
				// default-ly add every original column
				$new_columns[ $key ] = $value;
				
				/**
				 * If currently adding the title column,
				 * follow immediately with our custom columns.
				 */
				if ( $key == 'title' ) {
					$new_columns[ 'cp_bestoffer_column' ] = __( 'Best Offer', 'clpOffer' );
				}
					
			}
			
		return $new_columns;

}

function clp_bo_manage_posts_columns( $columns, $post_type ) {

	switch ( $post_type ) {
	
		case APP_POST_TYPE:
		
			// building a new array of column data
			$new_columns = array();
			
			foreach( $columns as $key => $value ) {
			
				// default-ly add every original column
				$new_columns[ $key ] = $value;
				
				/**
				 * If currently adding the title column,
				 * follow immediately with our custom columns.
				 */
				if ( $key == 'title' ) {
					$new_columns[ 'cp_bestoffer_column' ] = __( 'Best Offer', 'clpOffer' );
				}
					
			}
			
			return $new_columns;
			
	}
	
	return $columns;
	
}

add_action( 'manage_posts_custom_column', 'clp_bo_manage_posts_custom_column', 10, 2 );
function clp_bo_manage_posts_custom_column( $column_name, $post_id ) {

	switch( $column_name ) {

		case 'cp_bestoffer_column':
		$meta_value = false;
		$custom = get_post_custom();
    		if ( isset( $custom['cp_bestoffer'] ) ) $meta_value = $custom['cp_bestoffer'][0];

			if ( $meta_value ) {
			echo '<div style="visibility: hidden; display: none;" id="cp_bestoffer-' . $post_id . '">' . get_post_meta( $post_id, 'cp_bestoffer', true ) . '</div>';
			echo '<font color="green">'.__( 'Enabled', 'clpOffer' ).'</font>';
			} else {
			echo '<font color="red">'.__( 'Disabled', 'clpOffer' ).'</font>';
			}

			break;

	}
	
}

//add_action( 'bulk_edit_custom_box', 'clp_bo_quick_edit_custom_box', 10, 2 );
add_action( 'quick_edit_custom_box', 'clp_bo_quick_edit_custom_box', 10, 2 );
function clp_bo_quick_edit_custom_box( $column_name, $post_type ) {

	switch ( $post_type ) {
	
		case APP_POST_TYPE:
		
			switch( $column_name ) {

				case 'cp_bestoffer_column':
				
					?><fieldset class="inline-edit-col-left">
						<div class="inline-edit-col">
								<span class="input-text-wrap">
									<label style="display:inline;">
										<input type="checkbox" name="cp_bestoffer" value="<?php _e( 'Tick to enable', 'clpOffer' ); ?>" /> 
										<span class="checkbox-title"><?php _e( 'Enable Best Offer', 'clpOffer' ); ?></span>
									</label>
								</span>
						</div>
					</fieldset><?php

					break;

			}
			
			break;
			
	}
	
}

add_action( 'save_post', 'clp_bo_save_post', 10, 2 );
function clp_bo_save_post( $post_id, $post ) {

	if ( ! is_admin() )
		return;

	// pointless if $_POST is empty (this happens on bulk edit)
	if ( empty( $_POST ) )
		return $post_id;
		
	// verify quick edit nonce
	if ( isset( $_POST[ '_inline_edit' ] ) && ! wp_verify_nonce( $_POST[ '_inline_edit' ], 'inlineeditnonce' ) )
		return $post_id;
			
	// don't save for autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return $post_id;
		
	// dont save for revisions
	if ( isset( $post->post_type ) && $post->post_type == 'revision' )
		return $post_id;
		
	switch( $post->post_type ) {
	
		case APP_POST_TYPE:

			if ( isset( $_REQUEST['cp_bestoffer'] ) ) {
    			update_post_meta( $post_id, 'cp_bestoffer', __( 'Tick to enable', 'clpOffer' ) );
  			} else {
    			delete_post_meta( $post_id, 'cp_bestoffer' );
			}

			break;

		}
	
}


/*
|--------------------------------------------------------------------------
| BEST OFFER HOOKS
|--------------------------------------------------------------------------
*/

function clp_best_offer_here() {
	do_action( 'clp_best_offer_here' );
}

function clp_bestoffer_in_widget() {
	do_action( 'clp_bestoffer_in_widget' );
}


/*
|--------------------------------------------------------------------------
| TRANSLATE .JS CONFIRM TEXT
|--------------------------------------------------------------------------
*/

function clp_bo_localize_vars() {

	if ( get_option( 'clp_bo_enable_offer' ) == "featured" ) $featured = "true";
	else $featured = "false";

    return array(
        'ConfirmAccept' => __('Please confirm your acceptance of this offer?', 'clpOffer'),
        'ConfirmReject' => __('Are you sure you want to reject this offer?', 'clpOffer'),
        'ConfirmDelete' => __('Are you sure you want to delete this offer?', 'clpOffer'),
        'ConfirmPaid' => __('Are you sure you want to mark this offer as paid?', 'clpOffer'),
        'EnableBestOffer' => __('Enable Best Offer:', 'clpOffer'),
        'TickToEnable' => __('Tick to enable', 'clpOffer'),
        'Featured' => $featured,
        'ToolTip' => get_option( 'clp_bo_boffer_tooltip' ),
        'BoffError' => __('An error occurred.', 'clpOffer'),
        'BoffEnabled' => __('Best offer was successfully enabled! Please wait while page is refreshed.', 'clpOffer')
    );
} //End localize_vars


/*
|--------------------------------------------------------------------------
| FIND ALL ADS BASED ON POST TYPE AND AUTHOR - DROPDOWN
|--------------------------------------------------------------------------
*/

function clp_bo_options_dropdown($uid = '', $select = '') {

	global $wpdb, $cp_options;
	$new_class = "";
	$opt = false;
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$enable = get_option( 'clp_bo_enable_selection' );
	$options = get_option('clp_bo_selection');
	if( $enable == "yes" )
	$opt = explode(',', $options['selection']);
	else $opt = false;

	if( $uid && $opt ) {
	if( in_array("ads", $opt )) {
	$table_name = $wpdb->prefix . "posts";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_author = '$uid' AND post_status = 'publish' AND post_type = '".APP_POST_TYPE."'");
	}
	}
	else {
	$results = false;
	}

?>
<style type="text/css">
#page_id,
.selectBox,
.cp-dropdown {
	width:100% !important;
	margin-bottom: 0 !important;
}
</style>
<?php

	echo "<select class='cp-dropdown' name = 'option' id = 'OpTion'>";
      	echo "<option value=''>".__('Select Option','clpOffer')."</option>";
      	if( $opt ) { if( in_array("cash", $opt ))
      	echo "<option value='cash-option1'>".__('Offer an Amount','clpOffer')."</option>";
      	} if( $opt ) { if( in_array("item", $opt ))
      	echo "<option value='product-option1'>".__('Offer a Product','clpOffer')."</option>";
	}
	if( $results ) {
	foreach($results as $res) {
	echo "<option value='$res->ID'>$res->post_title</option>";
	}
	}
	echo "</select>";
}


/*
|--------------------------------------------------------------------------
| GET YES / NO DROPDOWN
|--------------------------------------------------------------------------
*/

function clp_bo_yes_no( $select = '' ) {

$options = "";
$what = array(
		'no' => __('No','clpOffer'),
		'yes' => __('Yes','clpOffer')
		);

	foreach($what as $what_id=>$yes_no) {
	if($select == $what_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$what_id.'">'.$yes_no.'</option>';
	} else {
	$options .= '<option value="'.$what_id.'">'.$yes_no.'</option>';
	}
	}
		return $options;
}


/*
|--------------------------------------------------------------------------
| GET POST ID BY META KEY AND VALUE
|--------------------------------------------------------------------------
*/

function clp_bo_get_post_id($key, $value) {
	global $wpdb;
	$meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='".$wpdb->escape($key)."' AND meta_value='".$wpdb->escape($value)."'");
	if (is_array($meta) && !empty($meta) && isset($meta[0])) {
	$meta = $meta[0];
	}	
	if (is_object($meta)) {
	return $meta->post_id;
	}
	else {
	return false;
	}
}


/*
|--------------------------------------------------------------------------
| ADD THE BIDDER BOX MENU ON SINGLE ADS SIDEBAR AS AN TABBED WIDGET
|--------------------------------------------------------------------------
*/

function clp_bo_bidder_box_tabbed_code() {

	global $post, $gmap_active, $cp_auction;
	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );
	$ct = strtolower(get_option('stylesheet'));

	if( $ct == "jibo" ) { ?>
	<ul class="tabmenu">
	<?php } else { ?>
	<ul class="tabnavig">
	<?php } ?>
	<?php if( get_option('clp_bo_use_widget') == "yes" && $bestoffer == true ) { ?>
	<li><a href="#priceblock0"><span class="big"><?php _e( 'Offer', 'clpOffer' ); ?></span></a></li>
	<?php } if ( $gmap_active && $ct != "twinpress_classifieds" && $ct != "phoenix" && $ct != "citrus-night" 
	&& $ct != "eldorado" && $ct != "multiport" && $ct != "redrondell" && $ct != "rondell_370" ) { ?>
	<li><a href="#priceblock1"><span class="big"><?php _e( 'Map', 'clpOffer' ); ?></span></a></li>
	 <?php } ?>
	<li><a href="#priceblock2"><span class="big"><?php _e( 'Contact', 'clpOffer' ); ?></span></a></li>
	<li><a href="#priceblock3"><span class="big"><?php _e( 'Poster', 'clpOffer' ); ?></span></a></li>
	</ul>

	<?php if( get_option('clp_bo_use_widget') == "yes" && $bestoffer == true ) { ?>

	<div id="priceblock0">

	<div class="clr"></div>

	<div class="singletab">

	<?php if( function_exists('clp_best_offer_box') ) clp_best_offer_box(); ?>

	</div><!-- /singletab -->

	</div>
<?php
}
}
add_action( 'clp_bo_bidder_box_tabbed', 'clp_bo_bidder_box_tabbed_code' );


/*
|--------------------------------------------------------------------------
| SANITIZE AMOUNT
|--------------------------------------------------------------------------
*/

function clp_best_offer_sanitize_amount( $amount ) {

	global $cp_options;

	$thousands_sep = $cp_options->thousands_separator;
	$decimal_sep   = $cp_options->decimal_separator;

	// Sanitize the amount
	if( $decimal_sep == ',' && false !== ( $found = strpos( $amount, $decimal_sep ) ) ) {

		if( $thousands_sep == '.' && false !== ( $found = strpos( $amount, $thousands_sep ) ) ) {
			$amount = str_replace( $thousands_sep, '', $amount );
		}

		$amount = str_replace( $decimal_sep, '.', $amount );

	}

	return apply_filters( 'clp_best_offer_sanitize_amount', $amount );
}


/*
|--------------------------------------------------------------------------
| REDIRECT OPTION
|--------------------------------------------------------------------------
*/

function clp_bo_page_redirect($location) {
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
}


/*
|--------------------------------------------------------------------------
| TRUNCATE POST CONTENT ON ADS
|--------------------------------------------------------------------------
*/

function clp_truncate($str, $length = '', $trailing = '...'){
	$length-=mb_strlen($trailing);
	if (mb_strlen($str)> $length){
	return mb_substr($str,0,$length).$trailing;
	}else{
	$res = $str;
}
	return $res;
}


/*
|--------------------------------------------------------------------------
| DISABLE ENABLE BEST OFFER NOT TO DISPLAY IN AD DETAILS
|--------------------------------------------------------------------------
*/

function clp_bo_details_fields( $result ) {

if ( !$result )
return false;

// Hooks into cp_ad_details_field to disable fields not to display in details.

	$fname = $result->field_name;

if ( $fname == "cp_bestoffer" ) return false;

else

return $result;
}
add_filter( 'cp_ad_details_field', 'clp_bo_details_fields' );


/*
|--------------------------------------------------------------------------
| ADD EXTRA PROFILE FIELDS
|--------------------------------------------------------------------------
*/

if( get_option( 'clp_bo_paypal_email' ) == "yes" ) {
add_action( 'show_user_profile', 'clp_bo_extra_profile_fields' );
add_action( 'edit_user_profile', 'clp_bo_extra_profile_fields' );
}
function clp_bo_extra_profile_fields( $user ) {

?>

	<table class="form-table">

		<tr>
			<th><label for="paypal_email"><?php _e('PayPal Email:', 'clpOffer'); ?></label></th>

			<td>
				<input type="text" name="paypal_email" id="paypal_email" value="<?php echo esc_attr( get_the_author_meta( 'paypal_email', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your PayPal email for use by Best Offers.', 'clpOffer'); ?></span>
			</td>
		</tr>

	</table>
<?php }


/*
|--------------------------------------------------------------------------
| SAVE THE EXTRA PROFILE FIELDS
|--------------------------------------------------------------------------
*/

add_action( 'personal_options_update', 'clp_bo_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'clp_bo_save_extra_profile_fields' );

function clp_bo_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	update_user_meta( $user_id, 'paypal_email', $_POST['paypal_email'] );

}