<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| AJAX ACTIONS
|--------------------------------------------------------------------------
*/

function clp_bo_accept_offer() {

	global $post, $wpdb;

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];
	$discount = 0;

	$table_name = $wpdb->prefix . "clp_best_offer";
	$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	$pid = $get->pid;
	$cid = $get->uid;
	$post = get_post($pid);

	if( get_post_status( $pid ) != "publish" ) return false;

	if( $get->latest_offer > 0 ) $win_bid = $get->latest_offer;
	else $win_bid = $get->bid;
	// update into bids table
	$where_array = array('id' => $id, 'pid' => $pid);
	$wpdb->update($table_name, array('bid' => clp_best_offer_sanitize_amount($win_bid), 'winner' => '1', 'acceptance' => 'Accepted'), $where_array);

	$tm = time();
	update_post_meta( $pid, 'cp_ad_sold', 'yes' );
	update_post_meta( $pid, 'cp_ad_sold_date', $tm );

	clp_bo_boffer_was_accepted_email( $pid, $post, $cid, $win_bid );
	exit;
	}
	else {
		return false;
	}
}
add_action('wp_ajax_clp_accept_offer', 'clp_bo_accept_offer');


function clp_bo_decline_offer() {

	global $post, $wpdb;

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];

	$table_name = $wpdb->prefix . "clp_best_offer";
	$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	$pid = $get->pid;
	$bid = $get->bid;
	$cid = $get->uid;
	$post = get_post($pid);

	if( get_post_status( $pid ) != "publish" ) return false;

	// update into db table
	$where_array = array('id' => $id, 'pid' => $pid);
	$wpdb->update($table_name, array('acceptance' => 'Declined'), $where_array);

	clp_bo_boffer_was_declined_email( $pid, $post, $cid, $bid );
	exit;
	}
	else {
		return false;
	}
}
add_action('wp_ajax_clp_decline_offer', 'clp_bo_decline_offer');


function clp_bo_delete_offer() {

	global $post, $wpdb;

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];

	$table_name = $wpdb->prefix . "clp_best_offer";
	$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	$pid = $get->pid;
	$bid = $get->bid;
	$cid = $get->uid;
	$post = get_post($pid);

	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));

	clp_bo_boffer_was_deleted_email( $pid, $post, $cid, $bid );
	exit;
	}
	else {
		return false;
	}
}
add_action('wp_ajax_clp_delete_offer', 'clp_bo_delete_offer');


function clp_bo_mark_paid() {

	global $post, $wpdb;

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];

	// update into db table
	$table_name = $wpdb->prefix . "clp_best_offer";
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('status' => 'Paid'), $where_array);

	exit;
	}
	else {
		return false;
	}
}
add_action('wp_ajax_clp_mark_paid', 'clp_bo_mark_paid');