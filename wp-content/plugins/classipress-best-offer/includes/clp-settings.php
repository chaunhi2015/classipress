<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| CLASSIPRESS BEST OFFER SETTINGS PAGE
|--------------------------------------------------------------------------
*/

function clp_best_offer_settings() {
	global $wpdb;
	$msg = "";
	$opt = false;

	if(isset($_POST['save_settings'])) {
		$enable_bestoffer = isset( $_POST['clp_bo_enable_offer'] ) ? esc_attr( $_POST['clp_bo_enable_offer'] ) : '';
		$tooltip = isset( $_POST['clp_bo_boffer_tooltip'] ) ? esc_attr( $_POST['clp_bo_boffer_tooltip'] ) : '';
		$enable_selection = isset( $_POST['clp_bo_enable_selection'] ) ? esc_attr( $_POST['clp_bo_enable_selection'] ) : '';
		$cash = isset( $_POST['cash'] ) ? esc_attr( $_POST['cash'] ) : '';
		$item = isset( $_POST['item'] ) ? esc_attr( $_POST['item'] ) : '';
		$ads = isset( $_POST['ads'] ) ? esc_attr( $_POST['ads'] ) : '';
		$combo = isset( $_POST['combo'] ) ? esc_attr( $_POST['combo'] ) : '';
		$best_offer = isset( $_POST['clp_bo_no_best_offer'] ) ? esc_attr( $_POST['clp_bo_no_best_offer'] ) : '';
		$user_counter_offer = isset( $_POST['clp_bo_no_user_counter_offer'] ) ? esc_attr( $_POST['clp_bo_no_user_counter_offer'] ) : '';
		$author_counter_offer = isset( $_POST['clp_bo_no_author_counter_offer'] ) ? esc_attr( $_POST['clp_bo_no_author_counter_offer'] ) : '';
		$clp_expiration = isset( $_POST['clp_bo_expiration'] ) ? esc_attr( $_POST['clp_bo_expiration'] ) : '';
		$info_text = isset( $_POST['clp_bo_info_text'] ) ? esc_attr( $_POST['clp_bo_info_text'] ) : '';
		$item_help = isset( $_POST['clp_bo_item_help'] ) ? esc_attr( $_POST['clp_bo_item_help'] ) : '';
		$amount_help = isset( $_POST['clp_bo_amount_help'] ) ? esc_attr( $_POST['clp_bo_amount_help'] ) : '';
		$del_data = isset( $_POST['clp_bo_uninstall'] ) ? esc_attr( $_POST['clp_bo_uninstall'] ) : '';
		$exp_valid = isset( $_POST['clp_bo_exp_valid'] ) ? esc_attr( $_POST['clp_bo_exp_valid'] ) : '';
		$paypal = isset( $_POST['clp_bo_paypal'] ) ? esc_attr( $_POST['clp_bo_paypal'] ) : '';
		$paypal_email = isset( $_POST['clp_bo_paypal_email'] ) ? esc_attr( $_POST['clp_bo_paypal_email'] ) : '';
		$contact_email = isset( $_POST['clp_bo_contact_email'] ) ? esc_attr( $_POST['clp_bo_contact_email'] ) : '';
		$use_hooks = isset( $_POST['clp_bo_use_hooks'] ) ? esc_attr( $_POST['clp_bo_use_hooks'] ) : '';
		$use_widget = isset( $_POST['clp_bo_use_widget'] ) ? esc_attr( $_POST['clp_bo_use_widget'] ) : '';
		$clp_html = isset( $_POST['clp_bo_html'] ) ? esc_attr( $_POST['clp_bo_html'] ) : '';
		$clp_url = isset( $_POST['clp_bo_banner_url'] ) ? esc_attr( $_POST['clp_bo_banner_url'] ) : '';
		$clp_link = isset( $_POST['clp_bo_banner_link'] ) ? esc_attr( $_POST['clp_bo_banner_link'] ) : '';
		$terms = isset( $_POST['clp_bo_terms'] ) ? esc_attr( $_POST['clp_bo_terms'] ) : '';

		$opt = ''.$cash.','.$item.','.$ads.','.$combo.'';
		if( $opt ) {
		$arrays = explode( ',', $opt );
		$new_opt = implode(",", $arrays);
		$opt_fields = trim($new_opt, ',');
		}

		if( $enable_bestoffer == "yes" || $enable_bestoffer == "featured" ) {
		$created = date('Y-m-d h:i:s');
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = 'cp_bestoffer'");
		if( empty( $res ) ) {
		$query = "INSERT INTO {$table_name} (field_name, field_label, field_desc, field_type, field_values, field_perm, field_core, field_req, field_owner, field_created, field_modified) VALUES (%s, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s)";
		$wpdb->query($wpdb->prepare($query, 'cp_bestoffer', __('Enable Best Offer','clpOffer'), __('Enable Best Offer','clpOffer'), 'checkbox', __('Tick to enable','clpOffer'), '0', '1', '0', 'ClassiPress Best Offer', $created, $created));
		}

		update_option( 'clp_bo_enable_offer', $enable_bestoffer );
		} else {
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE field_name = '%s'", 'cp_bestoffer'));

		delete_option( 'clp_bo_enable_offer' );
		}
		if( $tooltip ) {
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$wpdb->query( $wpdb->prepare("UPDATE $table_name SET field_tooltip = %s WHERE field_name = %s", stripslashes(html_entity_decode($tooltip)), 'cp_bestoffer' ) );
		update_option( 'clp_bo_boffer_tooltip', stripslashes(html_entity_decode($tooltip)) );
		} else {
		delete_option( 'clp_bo_boffer_tooltip' );
		}
		if( $enable_selection == "yes" ) {
		update_option('clp_bo_enable_selection', $enable_selection);
		} else {
		delete_option( 'clp_bo_enable_selection' );
		delete_option( 'clp_bo_selection' );
		}
		if( $opt ) {
		$options = get_option('clp_bo_selection');
		$options['selection'] = $opt_fields;
		if ( ! empty( $options ) )
   		update_option('clp_bo_selection', $options);
   		}
		if( $best_offer ) {
		update_option( 'clp_bo_no_best_offer', $best_offer );
		} else {
		delete_option( 'clp_bo_no_best_offer' );
		}
		if( $user_counter_offer ) {
		update_option( 'clp_bo_no_user_counter_offer', $user_counter_offer );
		} else {
		delete_option( 'clp_bo_no_user_counter_offer' );
		}
		if( $author_counter_offer ) {
		update_option( 'clp_bo_no_author_counter_offer', $author_counter_offer );
		} else {
		delete_option( 'clp_bo_no_author_counter_offer' );
		}
		if( $clp_expiration ) {
		update_option( 'clp_bo_expiration', $clp_expiration );
		} else {
		delete_option( 'clp_bo_expiration' );
		}
		if( $info_text ) {
		update_option( 'clp_bo_info_text', stripslashes(html_entity_decode($info_text)) );
		} else {
		delete_option( 'clp_bo_info_text' );
		}
		if( $item_help ) {
		update_option( 'clp_bo_item_help', stripslashes(html_entity_decode($item_help)) );
		} else {
		delete_option( 'clp_bo_item_help' );
		}
		if( $amount_help ) {
		update_option( 'clp_bo_amount_help', stripslashes(html_entity_decode($amount_help)) );
		} else {
		delete_option( 'clp_bo_amount_help' );
		}
		if( $del_data == "yes" ) {
		update_option( 'clp_bo_uninstall', $del_data );
		} else {
		delete_option( 'clp_bo_uninstall' );
		}
		if( $exp_valid == "yes" ) {
		update_option( 'clp_bo_exp_valid', $exp_valid );
		} else {
		delete_option( 'clp_bo_exp_valid' );
		}
		if( $paypal == "yes" ) {
		update_option( 'clp_bo_paypal', $paypal );
		} else {
		delete_option( 'clp_bo_paypal' );
		}
		if( $paypal_email == "yes" ) {
		update_option( 'clp_bo_paypal_email', $paypal_email );
		} else {
		delete_option( 'clp_bo_paypal_email' );
		}
		if( $contact_email ) {
		update_option( 'clp_bo_contact_email', $contact_email );
		} else {
		delete_option( 'clp_bo_contact_email' );
		}
		if( $use_hooks == "yes" ) {
		update_option( 'clp_bo_use_hooks', $use_hooks );
		} else {
		delete_option( 'clp_bo_use_hooks' );
		}
		if( $use_widget == "yes" ) {
		update_option( 'clp_bo_use_widget', $use_widget );
		} else {
		delete_option( 'clp_bo_use_widget' );
		}
		if( $clp_html ) {
		update_option( 'clp_bo_html', $clp_html );
		} else {
		delete_option( 'clp_bo_html' );
		}
		if( $clp_url ) {
		update_option( 'clp_bo_banner_url', $clp_url );
		} else {
		delete_option( 'clp_bo_banner_url' );
		}
		if( $clp_link ) {
		update_option( 'clp_bo_banner_link', $clp_link );
		} else {
		delete_option( 'clp_bo_banner_link' );
		}
		if( $terms ) {
		update_option( 'clp_bo_terms', stripslashes(html_entity_decode($terms) ) );
		} else {
		delete_option( 'clp_bo_terms' );
		}

		$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Settings was updated!','clpOffer').'</strong></p></div>';

}

	$enable = get_option( 'clp_bo_enable_selection' );
	$options = get_option('clp_bo_selection');
	if( $enable == "yes" )
	$opt = explode(',', $options['selection']);
	else $opt = false;
?>

	<?php if($msg) echo $msg; ?>

	<div class="wrap">
		<div id="icon-options-general" class="icon32"></div>
		<h2><?php _e( 'ClassiPress Best Offer', 'clpOffer' ); ?></h2>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('General Settings', 'clpOffer'); ?></h3>
            <div class="inside">

	<?php _e('Included in the plugin package there is also a folder named <strong>custom-files</strong>, in this folder we have included a customized dashboard file (tpl-dashboard.php) which will display authors ads and best offers in separate tabs. Please see the readme file for how this customized dashboard can be setup, the readme file is located in the same folder.', 'clpOffer'); ?>

	<div class="clear10"></div>

	<?php _e('A new page has been created, this page displays authors and users best offer stats. If you choose to NOT setup the customized dashboard then go to <strong>appearance -> menus</strong> and create a link to this page in the user options, read more below.', 'clpOffer'); ?>

	<div class="clear10"></div>

	<?php _e('If you have not already setup a custom User Options menu you should do so by going to Apperance -> Menus. Create a new menu and name it eg. Dashboard, then go to Manage Locations and attach the new menu you just created to the Theme Dashboard and save. You now have a new customized User Options menu ready to set up with your best offer link.', 'clpOffer'); ?>


	<form method="post" action="">

    <table class="form-table">

	<tr valign="top">
        <th scope="row"><?php _e('Enable Best Offer:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_enable_offer">
    <option value="" <?php if(get_option('clp_bo_enable_offer') == '') echo 'selected="selected"'; ?>><?php _e('Disabled', 'clpOffer'); ?></option>
    <option value="yes" <?php if(get_option('clp_bo_enable_offer') == 'yes') echo 'selected="selected"'; ?>><?php _e('All Ads', 'clpOffer'); ?></option>
    <option value="featured" <?php if(get_option('clp_bo_enable_offer') == 'featured') echo 'selected="selected"'; ?>><?php _e('Featured Ads', 'clpOffer'); ?></option>
    </select> <?php _e('Select your preferred setting. All ads or featured ads only.', 'clpOffer'); ?><br/>
   <small><?php _e('Making the best offer option available on all ads or featured ads only.', 'clpOffer'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Enter a Tooltip:', 'clpOffer'); ?></th>
    <td><textarea class="options" name="clp_bo_boffer_tooltip" rows="2" cols="60"><?php echo get_option('clp_bo_boffer_tooltip'); ?></textarea><br><small><?php _e('Enter some Best Offer information for the ad author. The tooltip will be applied to the Enable Best Offer checkbox in your ad submission forms.', 'clpOffer'); ?></small></td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Enable Selectable Options:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_enable_selection"><?php echo clp_bo_yes_no(get_option('clp_bo_enable_selection')); ?></select> <?php _e('Enable an dropdown menu where users can choose between multiple offer options. Set options below.', 'clpOffer'); ?></td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Selectable Options:', 'clpOffer'); ?></th>
   <td><input type="checkbox" name="cash" value="cash" <?php if( $opt ) { if( in_array("cash", $opt )) echo 'checked="checked"'; } ?>> <?php _e('Offer an Amount.', 'clpOffer'); ?><br/>
   <input type="checkbox" name="item" value="item" <?php if( $opt ) { if( in_array("item", $opt )) echo 'checked="checked"'; } ?>> <?php _e('Offer a Product.', 'clpOffer'); ?><br/>
   <input type="checkbox" name="ads" value="ads" <?php if( $opt ) { if( in_array("ads", $opt )) echo 'checked="checked"'; } ?>> <?php _e('Offer an Advertised Product.', 'clpOffer'); ?><br/>
   <input type="checkbox" name="combo" value="combo" <?php if( $opt ) { if( in_array("combo", $opt )) echo 'checked="checked"'; } ?>> <?php _e('Combine cash and product.', 'clpOffer'); ?><br/>
   <small><?php _e('Select the options that should be available in the dropdown menu.', 'clpOffer'); ?></small>
        </td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('No. of Best Offers:', 'clpOffer'); ?></th>
    <td><input type="text" name="clp_bo_no_best_offer" value="<?php echo get_option('clp_bo_no_best_offer'); ?>" size="8" /> <?php _e('How many times any user can post a best offer on the same listing.', 'clpOffer'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Buyers Counter Offers:', 'clpOffer'); ?></th>
    <td><input type="text" name="clp_bo_no_user_counter_offer" value="<?php echo get_option('clp_bo_no_user_counter_offer'); ?>" size="8" /> <?php _e('How many times bidder can post a counter offer on the same listing.', 'clpOffer'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Authors Counter Offers:', 'clpOffer'); ?></th>
    <td><input type="text" name="clp_bo_no_author_counter_offer" value="<?php echo get_option('clp_bo_no_author_counter_offer'); ?>" size="8" /> <?php _e('How many times author can post a counter offer on the same listing.', 'clpOffer'); ?></td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('Best Offer Expiration:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_expiration">
    <option value="" <?php if(get_option('clp_bo_expiration') == '') echo 'selected="selected"'; ?>><?php _e('Select', 'clpOffer'); ?></option>
    <option value="6" <?php if(get_option('clp_bo_expiration') == '6') echo 'selected="selected"'; ?>><?php _e('6 hours', 'clpOffer'); ?></option>
    <option value="12" <?php if(get_option('clp_bo_expiration') == '12') echo 'selected="selected"'; ?>><?php _e('12 hours', 'clpOffer'); ?></option>
    <option value="24" <?php if(get_option('clp_bo_expiration') == '24') echo 'selected="selected"'; ?>><?php _e('24 hours', 'clpOffer'); ?></option>
    <option value="48" <?php if(get_option('clp_bo_expiration') == '48') echo 'selected="selected"'; ?>><?php _e('48 hours', 'clpOffer'); ?></option>
    <option value="60" <?php if(get_option('clp_bo_expiration') == '60') echo 'selected="selected"'; ?>><?php _e('60 hours', 'clpOffer'); ?></option>
    </select></td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Expiry Only Counteroffers:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_exp_valid"><?php echo clp_bo_yes_no(get_option('clp_bo_exp_valid')); ?></select> <?php _e('Enable to use expiration on counter offers only.', 'clpOffer'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Information to Buyer:', 'clpOffer'); ?></th>
    <td><textarea class="options" name="clp_bo_info_text" rows="5" cols="80"><?php echo get_option('clp_bo_info_text'); ?></textarea><br><small><?php _e('Enter some Best Offer information for the buyer here. HTML can be used.', 'clpOffer'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Help with Product:', 'clpOffer'); ?></th>
    <td><textarea class="options" name="clp_bo_item_help" rows="5" cols="80"><?php echo get_option('clp_bo_item_help'); ?></textarea><br><small><?php _e('Let the buyer know why they should enter a product name. HTML can be used.', 'clpOffer'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Help with Amount:', 'clpOffer'); ?></th>
    <td><textarea class="options" name="clp_bo_amount_help" rows="5" cols="80"><?php echo get_option('clp_bo_amount_help'); ?></textarea><br><small><?php _e('Let the buyer know why they should add an amount. HTML can be used.', 'clpOffer'); ?></small></td>
    </tr>

	<tr valign="top">	
	<th scope="row"><?php _e('PayPal Payment:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_paypal"><?php echo clp_bo_yes_no(get_option('clp_bo_paypal')); ?></select> <?php _e('The buyer can pay for their purchases from their dashboard.', 'clpOffer'); ?><br /><small><?php _e('Enable to give the buyer an opportunity to pay for the product from their best offer dashboard. Be sure that the users are able to enter their paypal email in their profile.', 'clpOffer'); ?></small></td>
    </tr>

	<tr valign="top">	
	<th scope="row"><?php _e('PayPal Email:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_paypal_email"><?php echo clp_bo_yes_no(get_option('clp_bo_paypal_email')); ?></select> <?php _e('Enable the ability for users to enter their paypal email in their profile.', 'clpOffer'); ?><br /><small><?php _e('Enable this option if the users do not have the ability to update their profile with their paypal email, or if already given paypal email is not working with ClassiPress Best Offer.', 'clpOffer'); ?></small></td>
    </tr>

	<tr valign="top">	
	<th scope="row"><?php _e('Contact Email:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_contact_email">
    <option value="" <?php if(get_option('clp_bo_contact_email') == '') echo 'selected="selected"'; ?>><?php _e('Disabled', 'clpOffer'); ?></option>
    <option value="mail" <?php if(get_option('clp_bo_contact_email') == 'mail') echo 'selected="selected"'; ?>><?php _e('Notification Emails', 'clpOffer'); ?></option>
    <option value="account" <?php if(get_option('clp_bo_contact_email') == 'account') echo 'selected="selected"'; ?>><?php _e('My Best Offers Page', 'clpOffer'); ?></option>
    <option value="both" <?php if(get_option('clp_bo_contact_email') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Options', 'clpOffer'); ?></option>
    </select> <?php _e('Enable the ability for post author to contact the bidder.', 'clpOffer'); ?><br /><small><?php _e('Contact information will be provided according to the settings selected.', 'clpOffer'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Enable Hooks:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_use_hooks"><?php echo clp_bo_yes_no(get_option('clp_bo_use_hooks')); ?></select> <?php _e('Use the hooks provided.', 'clpOffer'); ?><br /><small><?php _e('Some child themes have placed the google maps just below the ad description, this may mean that the best offer box is placed at the very bottom of the ad. To make use of another placement put the below hooks where you want the the best offer box to appear in the single ad listing.', 'clpOffer'); ?></small>

	<div class="code_box">
	&lt;?php if ( function_exists('classipress_best_offer_here') ) classipress_best_offer_here(); ?&gt;
	</div>

    </td>
    </tr>

	<tr valign="top">	
	<th scope="row"><?php _e('Best Offer Widget:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_use_widget"><?php echo clp_bo_yes_no(get_option('clp_bo_use_widget')); ?></select> <?php _e('Display an best offer option box in the sidebar.', 'clpOffer'); ?><br /><small><?php _e('Display an best offer option box in the sidebar. Go to the widget settings and enable the CP Best Offer widget, <strong>OR</strong> you can use the below hook and add the best offer box / option to the on top tabbed sidebar widget, just replace the tabs code in the file <strong>sidebar-ad.php</strong> with the below code. The sidebar-ad.php file is located in "<strong>wp-content/themes/classipress</strong>".', 'clpOffer'); ?></small>

	<div class="code_box">
	&lt;?php if ( function_exists('clp_bo_bidder_box_tabbed') ) clp_bo_bidder_box_tabbed(); ?&gt;
	</div>

	<div class="clear10"></div>

	<small><strong><?php _e('When done code should look like below', 'clpOffer'); ?></strong>:</small>

	<div class="code_box">
	&lt;div class="tabprice"&gt;
	<br />
	&lt;?php if ( function_exists('clp_bo_bidder_box_tabbed') ) clp_bo_bidder_box_tabbed(); ?&gt;
	<br />
	&lt;?php if ( $gmap_active ) { ?&gt;
	</div>

    </td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('HTML or TEXT Emails:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_html" id="clp_bo_html" style="min-width: 100px;">
	<option value="html" <?php if(get_option('clp_bo_html') == "html") echo 'selected="selected"'; ?>><?php _e('HTML', 'clpOffer'); ?></option>
	<option value="plain" <?php if(get_option('clp_bo_html') == "plain") echo 'selected="selected"'; ?>><?php _e('TEXT', 'clpOffer'); ?></option>
	</select> <?php _e('Enable the built in html email template.', 'clpOffer'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Ad Image URL:', 'clpOffer'); ?></th>
    <td><input type="text" name="clp_bo_banner_url" value="<?php echo get_option('clp_bo_banner_url'); ?>" size="50" /> <?php _e('468x60 banner image URL.', 'clpOffer'); ?><br /><small><?php _e('Enter the image url to a 468x60 banner image here, this banner ad will be included with all outgoing html emails.', 'clpOffer'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Ad Destination URL:', 'clpOffer'); ?></th>
    <td><input type="text" name="clp_bo_banner_link" value="<?php echo get_option('clp_bo_banner_link'); ?>" size="50" /> <?php _e('Banner ad destination URL.', 'clpOffer'); ?><br /><small><?php _e('Enter the destination URL of your banner ad here (i.e. http://www.yoursite.com/landing-page.html).', 'clpOffer'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Terms & Conditions:', 'clpOffer'); ?></th>
    <td><textarea class="options" name="clp_bo_terms" rows="10" cols="80"><?php echo get_option('clp_bo_terms'); ?></textarea><br><small><?php _e('Enter your specific terms & conditions here. HTML can be used.', 'clpOffer'); ?></small></td>
    </tr>

	<tr valign="top">	
	<th scope="row"><?php _e('Delete All Plugin Data:', 'clpOffer'); ?></th>
    <td><select name="clp_bo_uninstall"><?php echo clp_bo_yes_no(get_option('clp_bo_uninstall')); ?></select> <?php _e('Delete all data upon plugin uninstall.', 'clpOffer'); ?><br /><br />
    <?php _e('<strong>DISCLAIMER</strong>: If for any reason your data is lost, damaged or otherwise becomes unusable in any way or by any means in whichever way I will not take responsibility. You should always have a backup of your database. Use with caution and make a backup first!', 'clpOffer'); ?></td>
    </tr>

    <tr valign="top">
    <td colspan="2"><input type="submit" value="<?php _e('Save Settings', 'clpOffer'); ?>" name="save_settings" class="button-primary"/></td>
    </tr>
    </table>
	</form>

	</div>
     </div>
     </div>
     </div>

    	<div class="inner-sidebar">

                <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'clpOffer'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'clpOffer'); ?></th>
    <td><a class="button-secondary" href="http://wp-build.com/wp-content/plugins/classipress-best-offer/change-log.txt" onclick="javascript:void window.open('http://wp-build.com/wp-content/plugins/classipress-best-offer/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'clpOffer'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'clpOffer'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'clpOffer'); ?></a></td>
    </tr>
    </tbody>
    </table>

<div class="clear15"></div>

<?php _e('<a style="text-decoration:none" href="http://www.arctic-websolutions.com" target="_blank"><strong>Arctic WebSolutions</strong></a> offers WordPress and ClassiPress plugins “as is” and with no implied meaning that they will function exactly as you would like or will be compatible with all 3rd party components and plugins. We do not offer support via email or otherwise support WordPress or other WordPress plugins we have not developed.', 'clpOffer'); ?>

<div class="clear15"></div>

<div class="admin_header"><?php _e( 'Other Plugins', 'clpOffer' ); ?></div>
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/bundle-pack/" target="_blank">Bundle Pack $317.00 off</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/cp-auction-social-sharing/" target="_blank">CP Auction & eCommerce</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-documents/" target="_blank">AWS Documents</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-grid-view/" target="_blank">ClassiPress Grid View</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-projects-portfolio/" target="_blank">AWS Projects Portfolio</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-affiliate-plugin/" target="_blank">AWS Affiliate Plugin</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-ecommerce/" target="_blank">ClassiPress eCommerce</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/simplepay4u-gateway/" target="_blank">SimplePay4u Gateway</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/grandchild-plugin/" target="_blank">AWS Grandchild Plugin</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-theme-customizer/" target="_blank">AWS Theme Customizer</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/best-offer-for-cp-auction/" target="_blank">CP Auction Best Offer</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-best-offer/" target="_blank">ClassiPress Best Offer</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-social-sharing/" target="_blank">ClassiPress Social Sharing</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-cartpauj-pm/" target="_blank">ClassiPress Cartpauj PM</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-deny-access/" target="_blank">AWS Deny Access</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-user-roles/" target="_blank">AWS User Roles</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-embed-video/" target="_blank">AWS Embed Video</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank">AWS Credit Payments</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">AppThemes Critic Reviews</a>', 'clpOffer' ); ?>

<div class="clear15"></div>

<?php _e('AppThemes has released a plugin <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> which has the same rating and review functionality as you can see around in their marketplace. The only minus is that it is not modded / available for ClassiPress Theme <strong>BUT</strong>, I have a modified version which you can review on our main site or any of our child theme demos.', 'clpOffer'); ?>

<div class="clear15"></div>

<?php _e('Purchase the <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> plugin through my <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">affiliate link</a>, send me a copy of your purchase receipt and i will send you my modified version of the Critic Reviews plugin.', 'clpOffer'); ?>

	<div class="clear15"></div>

	<p><strong><?php _e('Was this plugin useful? If you like it and it is/was useful, please consider donating.. Many thanks!', 'clpOffer'); ?></strong></p>

	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="RFJB58JUE8WUJ">
	<p style="text-align: center;">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	</p>
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>

            </div>
          </div>
	</div>
</div>
</div>

<?php
}