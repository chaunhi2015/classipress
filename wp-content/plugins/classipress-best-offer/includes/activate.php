<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| INSTALL DATABASE AND ACTIVATE PLUGIN
|--------------------------------------------------------------------------
*/

	global $clp_bo_db_version;
	$clp_bo_db_version = "1.5";

function clp_bo_plugin_activate() {
	global $wpdb;

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	// check if it is a multisite network
	if (is_multisite()) {
	// check if the plugin has been activated on the network or on a single site
	if (is_plugin_active_for_network(__FILE__)) {
	// get ids of all sites
	$blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
	foreach ($blogids as $blog_id) {
	switch_to_blog($blog_id);
	// activate for each site
	clp_bo_plugin_install($blog_id);
	restore_current_blog();
	}
	}
	else
	{
	// activated on a single site, in a multi-site
	clp_bo_plugin_install($wpdb->blogid);
	}
	}
	else
	{
	// activated on a single site
	clp_bo_plugin_install($wpdb->blogid);
	}
}

function clp_bo_plugin_install() {

	global $wpdb, $clp_bo_db_version;
	$clp_bo_db_version = "1.5";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	$table_name = $wpdb->prefix . "clp_best_offer";
	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

	$sql = "CREATE TABLE $table_name (
	id INT NOT NULL AUTO_INCREMENT,
	pid INT NOT NULL,
	post_author INT( 11 ) UNSIGNED NOT NULL,
	anonymous VARCHAR( 4 ) NOT NULL,
	datemade BIGINT( 20 ) NOT NULL,
	bid DECIMAL( 8,2 ) NOT NULL,
	uid INT NOT NULL,
	message TEXT NOT NULL,
	winner VARCHAR( 4 ) NOT NULL,
	boff_usage INT( 8 ) NOT NULL,
	uid_counter_offer DECIMAL( 8,2 ) NOT NULL,
	uid_date BIGINT( 20 ) NOT NULL,
	uid_usage INT( 8 ) NOT NULL,
	aut_counter_offer DECIMAL( 8,2 ) NOT NULL,
	aut_date BIGINT( 20 ) NOT NULL,
	aut_usage INT( 8 ) NOT NULL,
	latest_offer DECIMAL( 8,2 ) NOT NULL,
	acceptance VARCHAR( 20 ) NOT NULL,
	date_type VARCHAR( 20 ) NOT NULL,
	item_title VARCHAR( 75 ) NOT NULL,
	product_pid INT( 8 ) NOT NULL,
	type VARCHAR( 20 ) NOT NULL,
	status VARCHAR( 20 ) NOT NULL,
	PRIMARY KEY  (id)
	);";

	dbDelta($sql);

	update_option( "clp_bo_db_version", $clp_bo_db_version );

	}

	if( get_option( 'clp_bo_db_version' ) < 1.2 ) {
	$table_name = $wpdb->prefix . "clp_best_offer";
	$sql = "ALTER TABLE {$table_name} ADD item_title VARCHAR( 75 ) NOT NULL";
	@$wpdb->query($sql);
	
	$table_name = $wpdb->prefix . "clp_best_offer";
	$sql1 = "ALTER TABLE {$table_name} ADD product_pid INT( 8 ) NOT NULL";
	@$wpdb->query($sql1);

	$table_name = $wpdb->prefix . "clp_best_offer";
	$sql2 = "ALTER TABLE {$table_name} ADD type VARCHAR( 20 ) NOT NULL";
	@$wpdb->query($sql2);

	$table_name = $wpdb->prefix . "clp_best_offer";
	$sql3 = "ALTER TABLE {$table_name} ADD post_author INT( 11 ) UNSIGNED NOT NULL";
	@$wpdb->query($sql3);

	$table_name = $wpdb->prefix . "clp_best_offer";
	$sql4 = "ALTER TABLE {$table_name} ADD anonymous VARCHAR( 4 ) NOT NULL";
	@$wpdb->query($sql4);

	$table_name = $wpdb->prefix . "clp_best_offer";
	$sql5 = "ALTER TABLE {$table_name} ADD status VARCHAR( 20 ) NOT NULL";
	@$wpdb->query($sql5);

	}

	update_option( "clp_bo_db_version", $clp_bo_db_version );
}
?>