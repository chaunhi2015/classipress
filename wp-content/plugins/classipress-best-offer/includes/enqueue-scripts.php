<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| ADD NECESSARY SCRIPTS TO HEADER OR FOOTER
|--------------------------------------------------------------------------
*/

add_action( 'admin_print_scripts-edit.php', 'clp_bo_enqueue_quick_edit_admin_scripts' );
function clp_bo_enqueue_quick_edit_admin_scripts() {

	wp_enqueue_script( 'clp-quick-edit', trailingslashit( plugins_url( 'classipress-best-offer' ) ) . 'js/admin.js', array( 'jquery', 'inline-edit-post' ), '', true );
	
}


function clp_bo_ajax_load_scripts() {
	// load our jquery file that sends the $.post request
	wp_enqueue_script( 'ajax-clp-bo', plugins_url( 'classipress-best-offer/js/jquery-boffer.js', 'jquery', '1.0.0' ) );
	wp_localize_script( 'ajax-clp-bo', 'translatable_text_clp', clp_bo_localize_vars() );

	// make the ajaxurl var available to the above script
	wp_localize_script( 'ajax-clp-bo', 'clp_bo_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );	
}
if ( !is_admin() ) {
	add_action('wp_print_scripts', 'clp_bo_ajax_load_scripts');
}


function clp_bo_custom_style_sheets() {

	global $cp_options;
	$ct = strtolower(get_option('stylesheet')); // returns lowercase child theme names
	$stylesheet = ''.$ct.'.css';

		wp_enqueue_style( 'clp-bo-style', plugins_url( 'classipress-best-offer/css/style.css') );

		wp_enqueue_style( 'clp-child-custom', plugins_url( 'classipress-best-offer/css/'.$stylesheet, false ) );

}
add_action('wp_head','clp_bo_custom_style_sheets', 100 );

function clp_bo_admin_style_sheet() {

	wp_enqueue_style( 'clp-admin-style', plugins_url( 'classipress-best-offer/css/admin.css') );

}

if ( is_admin() ) {
	add_action( 'admin_print_styles','clp_bo_admin_style_sheet' );
}