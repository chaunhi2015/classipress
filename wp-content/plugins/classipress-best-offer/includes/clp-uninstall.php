<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

    	$delete = get_option('clp_bo_uninstall');

function uninstall_clp_boffer() {

	global $wpdb;
	$page_id = get_option( 'clp_bo_page_id' );
	wp_delete_post( $page_id, true );

	$table = $wpdb->prefix . "clp_best_offer";
	$wpdb->query("DROP TABLE IF EXISTS $table");

	$table_name = $wpdb->prefix . "cp_ad_fields";
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE field_name = '%s'", 'cp_bestoffer'));

	$clp_prefix = "clp_bo";
	$sql = "DELETE FROM ". $wpdb->options
         ." WHERE option_name LIKE '".$clp_prefix."%'";
	$wpdb->query($sql);

}

if ( $delete == "yes" ) {
	uninstall_clp_boffer();
}
?>