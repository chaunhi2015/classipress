<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| CREATE THE BEST OFFER DASHBOARD PAGE
|--------------------------------------------------------------------------
*/

function clp_bo_create_page() {

global $admin_id;
$admin_id = get_current_user_id();

$page = get_option( 'clp_bo_page_id' );

if( $page < 1 ) {
$dashboard_page = array(
	'post_title' => 'My Best Offers',
	'post_content' => '[clp-bo-dashboard-page]',
	'comment_status' => 'closed',
	'post_status' => 'publish',
	'post_name' => 'my-best-offers',
	'post_type' => 'page',
	'post_author' => $admin_id
);

$page_id = wp_insert_post($dashboard_page);
add_post_meta( $page_id, 'clp_bo_dashboard_page', $admin_id );
add_post_meta( $page_id, '_wp_page_template', 'tpl-best-offer.php' );
add_option( 'clp_bo_page_id', $page_id );
}
}