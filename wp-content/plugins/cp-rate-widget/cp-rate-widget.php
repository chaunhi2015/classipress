<?php
/*
Plugin Name: ClassiPress Rate Author Widget
Plugin URI: http://www.arctic-websolutions.com/
Description: Add the author rating and reviews to Ad sidebar as widget.
Version: 1.0.3
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

function cp_rate_textdomain() {
	load_plugin_textdomain( 'cpRate', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'cp_rate_textdomain' );


function cp_rate_scripts() {	

	wp_register_script('cp_rate_ad_js', plugins_url('cp-rate-adpage.js', __FILE__) , array('jquery'), '2.50', true);

	if(is_single()) {
       	wp_enqueue_script( 'cp_rate_ad_js' );
       	}

}
add_action( 'wp_enqueue_scripts', 'cp_rate_scripts' );



function cp_rate_my_adpage_rating() { ?>

<style type="text/css">
.shadowblock .cprate-widget {
	margin-top: 10px;
}
.shadowblock .cprate-widget div.cprate-myrating {
	margin: 0 0 0 0 !important;
	font-size:100%;
	border:none;
	font-weight:bold;
}
.shadowblock .cprate-widget div.cprate-myrating img {
	padding-top:0 !important;
	margin-bottom: -2px;
}
.shadowblock .cprate-widget-btn {
	margin: 10px 0 0 0;
}
</style>

<?php
		$this_id = $_POST['this_id'];
		$ratecal = cprate_calculate($this_id);
		$count = round($ratecal);
		$rateperc = $ratecal/5 * 100;
		$myrating = '<li><div class="cprate-myrating">' .get_option('cprate_header1'). ': <span id="cprate_avgrating">'; 
		$myrating .= print_all_stars($count);
		$myrating .='</span> (<span id="cprate_per">'.$rateperc. '%</span>) </div></li>';
		$myrating .='<li><div class="cprate-myrating">' . get_option('cprate_header2')  . ' ( <span id="total_count">' .cprating_count($this_id). '</span> )</div></li>';
	echo $myrating;
exit;
}
add_action('wp_ajax_cp_rate_widget_rating_action', 'cp_rate_my_adpage_rating');
add_action('wp_ajax_nopriv_cp_rate_widget_rating_action', 'cp_rate_my_adpage_rating');



// Creating the widget 
class cp_rate_widget extends WP_Widget {

function __construct() {
parent::__construct(
// Base ID of your widget
'cp_rate_widget', 

// Widget name will appear in UI
__('Author Reviews', 'cpRate'), 

// Widget description
array( 'description' => __( 'Display the author ratings as widget', 'cpRate' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
global $post, $cpurl;
$apage = get_option( 'cp_auction_author_page' );
$title = apply_filters( 'widget_title', $instance['title'] );

if ( function_exists('cp_auction_url') ) {
if( $apage == "apage" ) $view_all_url = get_author_posts_url($post->post_author);
else $view_all_url = cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.'');
}
else {
$view_all_url = get_author_posts_url($post->post_author);
}

// before and after widget arguments are defined by themes
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
echo '<ul><div class="cprate-widget"></div></ul>';
echo '<div class="cprate-widget-btn"><a class="obtn btn_orange" href="'.$view_all_url.'" />'.__('View All','cpRate').'</a></div>';

echo $args['after_widget'];
}
		
// Widget Backend 
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Author Reviews', 'cpRate' );
}
// Widget admin form
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:','cpRate'); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
} // Class cp_rate_widget ends here

// Register and load the widget
function cp_rate_load_widget() {
	register_widget( 'cp_rate_widget' );
}
add_action( 'widgets_init', 'cp_rate_load_widget' );