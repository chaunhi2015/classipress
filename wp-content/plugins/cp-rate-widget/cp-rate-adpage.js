/*
ClassiPress Rate Author Widget
Current Version: 1.3
Plugin Author: Rune Kristoffersen
Author URL: http://www.arctic-websolutions.com
*/

jQuery(document).ready(function($) {   
		var data = {
			action: "cp_rate_widget_rating_action",
			this_id: cprateAdPage.cprate_id
		};
		jQuery.post(cprateAjax.cprate_ajax, data, function(response) {	
			var resp = response;
			response = resp.substring(0,resp.length-1);
			jQuery(".cprate-widget").first().append(response);		
		});
});