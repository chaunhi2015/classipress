=== AWS Affiliate ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: wordpress, classipress, affiliate, classipress theme, auction, cpauction
Requires at least: 3.8
Tested up to: 4.0
Stable tag: 1.0.3
License: GPL
License URI: http://www.gnu.org/licenses/gpl.html

---------------------------------------------------------------------------------------------------

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://wp-build.com/demo) - Testdrive every aspect of the plugin.
* [Manuals & Support](http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Some Admin Features ===

* Set which system the affiliate plugin should work with, revenue on new ads or on product sales.
* Set affiliate revenue.
* Set minimum withdraw amount.
* Activate second level revenue.
* Set revenue of the top most referrer.
* Add affiliate banners.

=== Some User/Author Features ===

* Earn affiliate revenue on new ads and/or product sales.
* Set affiliate revenue.
* Set minimum withdraw amount.
* Activate second level revenue.
* Set revenue of the top most referrer.
* Add affiliate banners.

=== Installation ===

* Download AWS Affiliate.
* Extract the downloaded .zip file. (Not necessary if filename dosen’t say so, ie. unzip-first)
* Login to your websites FTP and navigate to the “wp-contents/plugins/” folder.
* Upload the resulting ”aws-affiliate.zip” or “aws-affiliate-X.X.X.zip” folder to the websites plugin folder.
* Go to your websites Dashboard, access the “plugins” section on the left hand menu.
* Locate “AWS Affiliate” in the plugin list and click “activate”.
* Place the widget to your user sidebar, OR add the below shortcode in any page.

Shortcode ClassiPress: [aws_clp_affiliate]
Shortcode CP Auction: [aws_cpa_affiliate]

Or, install using WP`s plugin installation tool.
Make sure to backup your translations before any upgrade.

Visit your AWS Affiliate, configure any options as desired. That’s it!


=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.