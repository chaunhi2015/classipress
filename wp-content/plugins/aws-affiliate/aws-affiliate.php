<?php

/**
Plugin Name: AWS Affiliate
Plugin URI: http://www.arctic-websolutions
Description: Add's an affiliate feature to your ClassiPress & CP Auction installation.
Version: 1.0.3
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com
*/

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AWS_AFFILIATE_VERSION' ) ) {
	define( 'AWS_AFFILIATE_VERSION', '1.0.3' );
}

if ( ! defined( 'AWS_AFFILIATE_LATEST_RELEASE' ) ) {
	define( 'AWS_AFFILIATE_LATEST_RELEASE', '21 June 2014' );
}


/*
|--------------------------------------------------------------------------
| INCLUDE THE NECESSARY FILES
|--------------------------------------------------------------------------
*/

include_once('includes/functions.php');
include_once('includes/ajax-functions.php');
include_once('includes/activate.php');
include_once('includes/admin-menu.php');
include_once('includes/general-settings.php');
include_once('includes/classipress-settings.php');
include_once('includes/cpauction-settings.php');
include_once('includes/scripts-enqueue.php');
include_once('emails/emails.php');


/*
|--------------------------------------------------------------------------
| TRANSLATION
|--------------------------------------------------------------------------
*/

function awsAffiliate_textdomain() {
	load_plugin_textdomain( 'awsAffiliate', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}
add_action( 'init', 'awsAffiliate_textdomain' );


/*
|--------------------------------------------------------------------------
| OPTION PAGE MENU
|--------------------------------------------------------------------------
*/

function AWS_plugin_menu() {
    	global $aws_affiliate_admin_menu;
	$aws_affiliate_admin_menu = add_menu_page( __( 'AWS Affiliate Management', 'awsAffiliate' ), __( 'AWS Affiliate', 'awsAffiliate' ), 'manage_options', 'aws-affiliate', 'aws_affiliate_settings', '', '6,4' );
}
add_action( 'admin_menu', 'AWS_plugin_menu' );


/*
|--------------------------------------------------------------------------
| PLUGIN URL
|--------------------------------------------------------------------------
*/
function aws_affiliate_plugin_url() { return get_bloginfo('wpurl')."/wp-content/plugins/aws-affiliate"; }


/*
|--------------------------------------------------------------------------
| ACTIVATION HOOK & ACTIONS
|--------------------------------------------------------------------------
*/

register_activation_hook(__FILE__, 'AWS_install');

add_action('init', 'AWS_track_click');
add_action('register_form', 'AWS_add_extra_reg_field');
add_action('user_register', 'AWS_add_ref_id');
if( get_option('AWS_classipress_affiliate') == "yes" )
add_action('publish_ad_listing', 'AWS_after_publish_post');


/*
|--------------------------------------------------------------------------
| DEACTIVATION HOOK
|--------------------------------------------------------------------------
*/

function aws_affiliate_deactivate_actions() {

	update_option('aws_affiliate_installed', 'no');

}
register_deactivation_hook( __FILE__, 'aws_affiliate_deactivate_actions' );

?>