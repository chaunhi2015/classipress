<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	$site_name = get_bloginfo('name');
	$description = get_bloginfo('description');
	$blog_url = get_bloginfo('wpurl');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
  </head>
  <body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F4F3F4">
      <tbody>
        <tr>
          <td style="padding: 15px;"><center>
            <table width="550" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
              <tbody>
                <tr>
                  <td align="left">
                    <div style="border: solid 1px #d9d9d9;">
                      <table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; color: #444;" width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr>
                            <td style="color: #ffffff;" colspan="2" valign="bottom" height="30">.</td>
                          </tr>
                          <tr>
                            <td style="line-height: 32px; padding-left: 30px;" colspan="2" valign="baseline"><span style="font-size: 32px;"><a style="text-decoration: none;" href="<?php echo $blog_url; ?>" target="_blank"><?php echo $site_name; ?></a></span><br /><span style="font-size: 14px; color: #777777;"><?php echo $description; ?></span></td>
                          </tr>
                        </tbody>
                      </table>
                      <table id="content" style="margin-top: 15px; margin-right: 30px; margin-left: 30px; color: #444; line-height: 1.6; font-size: 12px; font-family: Arial, sans-serif;" width="490" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr>
                            <td style="border-top: solid 1px #d9d9d9;" colspan="2">
                              <div style="padding: 15px 0;">