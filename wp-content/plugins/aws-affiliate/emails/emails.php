<?php

/*
The following emails can be edited here, or by using the codestyling localization plugin.
 - ADMIN WITHDRAW REQUEST POSTED
 - SELLER WITHDRAW REQUEST POSTED
 - WITHDRAW APPROVED
 - WITHDRAW REJECTED
 - TELL A FRIEND
*/

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| ADMIN WITHDRAW REQUEST POSTED
|--------------------------------------------------------------------------
*/

function aws_affiliate_admin_withdraw_request_mail() {

	$plain_html = get_option('AWS_affiliate_html');
	$site_name = get_bloginfo('name');
	$admin_email = get_bloginfo('admin_email');

	$subject = __('New Withdraw Request','awsAffiliate');

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Hello,','awsAffiliate'); ?><br /><br />

	<?php _e('A new withdraw request is in the queue, visit','awsAffiliate'); ?> <a href="<?php echo get_bloginfo('wpurl'); ?>/wp-admin/admin.php?page=aws-affiliate&tab=cpauction"><?php _e('admin area','awsAffiliate'); ?></a> <?php _e('for approve/reject actions.','awsAffiliate'); ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a href="<?php echo get_bloginfo('wpurl'); ?>"><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'Content-type: text/'.$plain_html.'; charset=UTF-8' . "\r\n";
	$headers[] = "From: $site_name <$admin_email>";
	$headers[] = "Reply-To: $site_name <$admin_email>";

	wp_mail( $admin_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| SELLER WITHDRAW REQUEST POSTED
|--------------------------------------------------------------------------
*/

function aws_affiliate_seller_withdraw_request_mail($seller_id) {

	$plain_html = get_option('AWS_affiliate_html');
	$site_name = get_bloginfo('name');
	$admin_email = get_bloginfo('admin_email');

	$user = get_userdata($seller_id);
	$subject = __('New Withdraw Request','awsAffiliate');

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','awsAffiliate'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('A new withdraw request is in the queue, visit','awsAffiliate'); ?> <a href="<?php echo get_bloginfo('wpurl'); ?>"><?php _e('dashboard','awsAffiliate'); ?></a> <?php _e('for approve/reject actions.','awsAffiliate'); ?><br /><br /><br />


	<?php _e('Thanks for your efforts as referrer!','awsAffiliate'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a href="<?php echo get_bloginfo('wpurl'); ?>"><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'Content-type: text/'.$plain_html.'; charset=UTF-8' . "\r\n";
	$headers[] = "From: $site_name <$admin_email>";
	$headers[] = "Reply-To: $site_name <$admin_email>";

	wp_mail( $user->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| WITHDRAW APPROVED
|--------------------------------------------------------------------------
*/

function aws_affiliate_withdraw_approved_mail( $user_id ) {

	$plain_html = get_option('AWS_affiliate_html');
	$site_name = get_bloginfo('name');
	$admin_email = get_bloginfo('admin_email');

	// REFERRER NOTIFICATION
	$user = get_userdata($user_id);
	$subject = __('Your withdraw was approved','awsAffiliate');

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','awsAffiliate'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('Your withdraw request was approved, you will receive your payment shortly.','awsAffiliate'); ?><br /><br /><br />

	
	<?php _e('Thanks for your efforts as referrer!','awsAffiliate'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a href="<?php echo get_bloginfo('wpurl'); ?>"><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'Content-type: text/'.$plain_html.'; charset=UTF-8' . "\r\n";
	$headers[] = "From: $site_name <$admin_email>";
	$headers[] = "Reply-To: $site_name <$admin_email>";

	wp_mail( $user->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| WITHDRAW REJECTED
|--------------------------------------------------------------------------
*/

function aws_affiliate_withdraw_rejected_mail( $user_id ) {

	$plain_html = get_option('AWS_affiliate_html');
	$site_name = get_bloginfo('name');
	$admin_email = get_bloginfo('admin_email');

	// REFERRER NOTIFICATION
	$user = get_userdata($user_id);
	$subject = __('Your withdraw was rejected','awsAffiliate');

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','awsAffiliate'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('Your withdraw request was rejected, you should contact the seller or the site administrator directly to clarify this.','awsAffiliate'); ?><br /><br /><br />

	
	<?php _e('Thanks for your efforts as referrer!','awsAffiliate'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a href="<?php echo get_bloginfo('wpurl'); ?>"><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'Content-type: text/'.$plain_html.'; charset=UTF-8' . "\r\n";
	$headers[] = "From: $site_name <$admin_email>";
	$headers[] = "Reply-To: $site_name <$admin_email>";

	wp_mail( $user->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| TELL A FRIEND
|--------------------------------------------------------------------------
*/

function aws_affiliate_tell_friends_email($uid, $name, $sender, $receiver, $msg, $i) {

	$plain_html = get_option('AWS_affiliate_html');
	$site_name = get_bloginfo('name');
	$admin_email = get_bloginfo('admin_email');
	$allowed = get_option('AWS_number_emails');
	$timeInFuture = time() + (60 * 60 * 24);
	if( $i > $allowed ) {
	update_user_meta( $uid, 'AWS_used_emails', 0 );
	update_user_meta( $uid, 'AWS_next_emails', $timeInFuture );
		return;
	}
	$unsubscribed = get_option('cp_auction_unsubscribed');
	$list = explode(',', $unsubscribed);

	$subject = __('Message from your friend','awsAffiliate');

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php echo $msg; ?><br /><br /><br />


	<?php _e('This email was sent from our <strong>tell a friend</strong> form by','awsAffiliate'); ?> <?php echo $name; ?>, <a href="<?php echo get_bloginfo('wpurl'); ?>/?confirmation=8&unsubscribe=<?php echo $receiver; ?>"><?php _e('click here','awsAffiliate'); ?></a> <?php _e('if you prefer not to receive further e-mails like this.','awsAffiliate'); ?><br /><br />
	

	<strong><?php echo $site_name; ?></strong><br />
	<a href="<?php echo get_bloginfo('wpurl'); ?>"><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'Content-type: text/'.$plain_html.'; charset=UTF-8' . "\r\n";
	$headers[] = "From: $name <$sender>";
	$headers[] = "Reply-To: $name <$sender>";

	if( !in_array($receiver, $list) )
	wp_mail( $receiver, $subject, $message, $headers );

}