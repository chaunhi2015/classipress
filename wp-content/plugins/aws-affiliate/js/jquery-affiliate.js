/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

jQuery(document).ready( function($) {
	jQuery(".aws-cpauction-approve-withdraw").click( function() {
		var retVal = confirm(translatable_aws_string.ConfirmApprove);
   		if( retVal == true ) {
		var data = {
			action: 'AWS_cpauction_approve_withraw',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(aws_affiliate_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});


jQuery(document).ready( function($) {
	jQuery(".aws-cpauction-deny-withdraw").click( function() {
		var retVal = confirm(translatable_aws_string.ConfirmDeny);
   		if( retVal == true ) {
		var data = {
			action: 'AWS_cpauction_deny_withraw',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(aws_affiliate_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".aws-classipress-approve-withdraw").click( function() {
		var retVal = confirm(translatable_aws_string.ConfirmApprove);
   		if( retVal == true ) {
		var data = {
			action: 'AWS_classipress_approve_withraw',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(aws_affiliate_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});


jQuery(document).ready( function($) {
	jQuery(".aws-classipress-deny-withdraw").click( function() {
		var retVal = confirm(translatable_aws_string.ConfirmDeny);
   		if( retVal == true ) {
		var data = {
			action: 'AWS_classipress_deny_withraw',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(aws_affiliate_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});