<?php

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| INSTALL DATABASE
|--------------------------------------------------------------------------
*/

global $aws_affiliate_db_version, $aws_affiliate_db_date, $wpdb;
$aws_affiliate_db_version = "1.0";
$aws_affiliate_db_date = "08 June 2014";

function AWS_install() {
	global $aws_affiliate_db_version, $aws_affiliate_db_date, $wpdb, $admin_id;

require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	$latest = get_option('aws_affiliate_db_version');
	update_option('aws_affiliate_installed', 'yes');

	// Create database
	$table_clp = $wpdb->prefix . "aws_clp_affiliates";
	if($wpdb->get_var("show tables like '$table_clp'") != $table_clp) {

	$sql = "CREATE TABLE $table_clp (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`referer_id` int(11) NOT NULL,
	`post_id` int(11) NOT NULL,
	`poster_id` int(11) NOT NULL,
	`revenue` float(11,2) NOT NULL DEFAULT '0.00',
	`type` varchar(20) CHARACTER SET utf8 NOT NULL,
	`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	KEY `referer_id` (`referer_id`)
	) ENGINE=MyISAM DEFAULT CHARSET=utf8";

	dbDelta($sql);
}

	$table_cpa = $wpdb->prefix . "aws_cpa_affiliates";
	if($wpdb->get_var("show tables like '$table_cpa'") != $table_cpa) {

	$sql2 = "CREATE TABLE $table_cpa (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`referer_id` int(11) NOT NULL,
	`post_id` int(11) NOT NULL,
	`order_id` int(11) NOT NULL,
	`poster_id` int(11) NOT NULL,
	`seller_id` int(11) NOT NULL,
	`revenue` float(11,2) NOT NULL DEFAULT '0.00',
	`type` varchar(20) CHARACTER SET utf8 NOT NULL,
	`updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	KEY `referer_id` (`referer_id`)
	) ENGINE=MyISAM DEFAULT CHARSET=utf8";

	dbDelta($sql2);
}


	//register the initial settings
	if( $latest < '1.0' ) {
	$settings = array(
            'AWS_classipress_revenue_percent'           => '20',
            'AWS_classipress_minimum_withdraw'          => '10',
            'AWS_classipress_active_two_level'          => 'no',
            'AWS_classipress_parent_revenue_percent'    => '10',
            'AWS_cpauction_revenue_percent'          	=> '20',
            'AWS_cpauction_minimum_withdraw'          	=> '10',
            'AWS_cpauction_active_two_level'          	=> 'no',
            'AWS_cpauction_parent_revenue_percent'    	=> '10'
	);

	foreach ($settings as $key => $value) {
        update_option($key, $value);
	}
	}

	update_option('aws_affiliate_db_version', $aws_affiliate_db_version);
}

?>