<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function aws_affiliate_load_ajax_scripts() {
	// load our jquery file that sends the $.post request
	wp_enqueue_script( 'affiliate-ajax', aws_affiliate_plugin_url() . '/js/jquery-affiliate.js', array( 'jquery' ), '1.0.0' );
	wp_localize_script( 'affiliate-ajax', 'translatable_aws_string', aws_affiliate_localize_vars() );
 
	// make the ajaxurl var available to the above script
	wp_localize_script( 'affiliate-ajax', 'aws_affiliate_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );	
}
add_action('wp_print_scripts', 'aws_affiliate_load_ajax_scripts');