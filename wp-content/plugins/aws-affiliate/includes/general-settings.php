<?php

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| GENERAL OPTIONS
|--------------------------------------------------------------------------
*/

add_action( 'admin_init', 'AWS_general_settings' );

function AWS_general_options() {
    global $wpdb, $cp_options, $aws_affiliate_db_version, $aws_affiliate_db_date;
    ?>
    <div class="wrap">
    	<div class="icon32" id="icon-edit-pages"><br/></div>
        <h3><?php _e('General Settings','awsAffiliate'); ?></h3>

<?php if( get_option('cp_auction_installed') == "yes" ) { ?>

	<p><?php _e('Before you start configuring AWS Affiliate plugin so you must','awsAffiliate'); ?> <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=pages"><?php _e('create a new page','awsAffiliate'); ?></a> <?php _e('that will serve as affiliate statistics and configuration page for your users.<br />Once the page is created, you need to link to this via the','awsAffiliate'); ?> <a style="text-decoration: none;" href="../wp-admin/nav-menus.php"><?php _e('user options menu.','awsAffiliate'); ?></a></p>

<?php } else { ?>

<p><?php _e('Before you start configuring AWS Affiliate plugin so you must..','awsAffiliate'); ?></a></p>

<?php } ?>

        <form method="post" action="options.php">
            <?php wp_nonce_field( 'update-options' ); ?>
            <?php settings_fields( 'AWS_general_group' ); ?>
            <?php do_settings_sections( 'AWS_general_group' ); ?>
            <table class="form-table">

		<tr valign="top">
        	    <th scope="row"><strong>AWS Affiliate Version:</strong><br><strong>Database Version:</strong></th>
    		    <td><strong><?php echo AWS_AFFILIATE_VERSION; ?></strong> - <?php echo AWS_AFFILIATE_LATEST_RELEASE; ?><br><strong><?php echo $aws_affiliate_db_version; ?></strong> - <?php echo $aws_affiliate_db_date; ?></td>
    		</tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Referred By','awsAffiliate'); ?>:</th>
                    <td>
                        <select name="AWS_referred_by">
                        <option value="no" <?php if(get_option('AWS_referred_by') == 'no') echo 'selected="selected"'; ?>><?php _e('No', 'awsAffiliate'); ?></option>
                        <option value="yes" <?php if(get_option('AWS_referred_by') == 'yes') echo 'selected="selected"'; ?>><?php _e('Yes', 'awsAffiliate'); ?></option>
			</select>
			<span class="description">
                            <?php _e('This option lets you store new users IP address and display a <strong>Referred By:</strong> field in the registration page.','awsAffiliate'); ?>
                        </span>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('HTML or TEXT Emails', 'awsAffiliate'); ?>:</th>
		    <td>
		        <select name="AWS_affiliate_html" id="AWS_affiliate_html" style="min-width: 100px;">
                        <option value="html" <?php if(get_option('AWS_affiliate_html') == "html") echo 'selected="selected"'; ?>><?php _e('HTML', 'awsAffiliate'); ?></option>
                        <option value="plain" <?php if(get_option('AWS_affiliate_html') == "plain") echo 'selected="selected"'; ?>><?php _e('TEXT', 'awsAffiliate'); ?></option>
                        </select>
                        <span class="description">
                            <?php _e('Enable the built in html email template.', 'awsAffiliate'); ?>
                        </span>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Ad Image URL', 'awsAffiliate'); ?>:</th>
                    <td>
                        <input type="text" name="AWS_affiliate_banner_url" value="<?php echo get_option('AWS_affiliate_banner_url'); ?>" size="50" />
                        <span class="description">
                            <?php _e('468x60 banner image URL.', 'awsAffiliate'); ?>
                        </span><br /><small><?php _e('Enter the image url to a 468x60 banner image here, this banner ad will be included with all outgoing html emails.', 'awsAffiliate'); ?></small>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Ad Destination URL', 'awsAffiliate'); ?>:</th>
                    <td>
                        <input type="text" name="AWS_affiliate_banner_link" value="<?php echo get_option('AWS_affiliate_banner_link'); ?>" size="50" />
                        <span class="description">
                            <?php _e('Banner ad destination URL.', 'awsAffiliate'); ?>
                        </span><br /><small><?php _e('Enter the destination URL of your banner ad here (i.e. http://www.yoursite.com/landing-page.html).', 'awsAffiliate'); ?></small>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Tell a Friend', 'awsAffiliate'); ?>:</th>
		    <td>
		        <select name="AWS_tell_friends" id="AWS_tell_friends" style="min-width: 100px;">
                        <option value="no" <?php if(get_option('AWS_tell_friends') == "no") echo 'selected="selected"'; ?>><?php _e('No', 'awsAffiliate'); ?></option>
                        <option value="yes" <?php if(get_option('AWS_tell_friends') == "yes") echo 'selected="selected"'; ?>><?php _e('Yes', 'awsAffiliate'); ?></option>
                        </select>
                        <span class="description">
                            <?php _e('This option lets you enable an tell a friend / send email invitations to friends feature.', 'awsAffiliate'); ?>
                        </span>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Number of Emails', 'awsAffiliate'); ?>:</th>
		    <td>
		        <select name="AWS_number_emails" id="AWS_number_emails" style="min-width: 100px;">
                        <option value="1" <?php if(get_option('AWS_number_emails') == "1") echo 'selected="selected"'; ?>><?php _e('1 Emails', 'awsAffiliate'); ?></option>
                        <option value="3" <?php if(get_option('AWS_number_emails') == "3") echo 'selected="selected"'; ?>><?php _e('3 Emails', 'awsAffiliate'); ?></option>
                        <option value="5" <?php if(get_option('AWS_number_emails') == "5") echo 'selected="selected"'; ?>><?php _e('5 Emails', 'awsAffiliate'); ?></option>
                        <option value="7" <?php if(get_option('AWS_number_emails') == "7") echo 'selected="selected"'; ?>><?php _e('7 Emails', 'awsAffiliate'); ?></option>
                        <option value="10" <?php if(get_option('AWS_number_emails') == "10") echo 'selected="selected"'; ?>><?php _e('10 Emails', 'awsAffiliate'); ?></option>
                        </select>
                        <span class="description">
                            <?php _e('This option allows you to limit the number of emails user can send per day.', 'awsAffiliate'); ?>
                        </span>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('From Email Address', 'awsAffiliate'); ?>:</th>
                    <td>
                        <input type="text" name="AWS_from_email" value="<?php echo get_option('AWS_from_email'); ?>" size="50" />
                        <span class="description">
                            <?php _e('Leave blank to use the user`s email.', 'awsAffiliate'); ?>
                        </span><br /><small><?php _e('Be aware that some webhosts do not accept the use of sender/from email address if it belongs to a different domain than your website, so if you leave this field blank and it is reported that the sent emails do not reach out to the receiver, try to use an email address on the same domain as your website, eg. noreply@your-domene.com.', 'awsAffiliate'); ?></small>
                    </td>
                </tr>

                <tr valign="top">
                    <th scope="row"><?php _e('Exclude Users', 'awsAffiliate'); ?>:</th>
                    <td>
                        <input type="text" name="AWS_users_excluded" value="" size="10" />
                        <span class="description">
                            <?php _e('Enter the user ID of the user to exclude / enable.', 'awsAffiliate'); ?>
                        </span><br /><small><?php _e('This option allows you to exclude / enable users from using the Tell a Friend feature. The ID can be found in users table or at the end of users affiliate link.', 'awsAffiliate'); ?></small>
                    </td>
                </tr>

            </table>


            <?php
            $opts = "AWS_referred_by, AWS_affiliate_html, AWS_affiliate_banner_url, AWS_affiliate_banner_link, AWS_tell_friends, AWS_from_email, AWS_number_emails, AWS_users_excluded";
            ?>

            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="<?php echo $opts; ?>" />

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e( 'Save Changes','awsAffiliate' ); ?>" />
            </p>

        </form>
      </div><!-- /wrap -->
<?php
}


/*
|--------------------------------------------------------------------------
| REGISTER OPTIONS VARIABLE
|--------------------------------------------------------------------------
*/

function AWS_general_settings() {
    register_setting( 'AWS_general_group', 'AWS_referred_by' );
    register_setting( 'AWS_general_group', 'AWS_affiliate_html' );
    register_setting( 'AWS_general_group', 'AWS_affiliate_banner_url' );
    register_setting( 'AWS_general_group', 'AWS_affiliate_banner_link' );
    register_setting( 'AWS_general_group', 'AWS_tell_friends' );
    register_setting( 'AWS_general_group', 'AWS_from_email' );
    register_setting( 'AWS_general_group', 'AWS_number_emails' );
    register_setting( 'AWS_general_group', 'AWS_users_excluded', 'AWS_users_excluded_action' );
}