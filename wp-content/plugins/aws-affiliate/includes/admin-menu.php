<?php

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| ADMIN TABBED MENU
|--------------------------------------------------------------------------
*/

function aws_affiliate_settings( $current = '' ) {

	$current = isset($_GET['tab']) ? $_GET['tab'] : 'general';
	if(isset($_GET['tab'])) $current = $_GET['tab']; ?>

	<div class="wrap">

		<h2><?php _e( 'AWS Affiliate Management', 'awsAffiliate' ); ?></h2>

<?php
    $tabs = array( 'general' => __('General', 'awsAffiliate'), 'classipress' => __('New Ads Affiliate', 'awsAffiliate'), 'cpauction' => __('Product Sales Affiliate', 'awsAffiliate') );
    $links = array();
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) :
            $links[] = "<a class='nav-tab nav-tab-active' href='?page=aws-affiliate&tab=$tab'>$name</a>";
        else :
            $links[] = "<a class='nav-tab' href='?page=aws-affiliate&tab=$tab'>$name</a>";
        endif;
    endforeach;
    echo '<h2 class="nav-tab-wrapper">';
    foreach ( $links as $link )
        echo $link;
    echo '</h2>';

    if ( isset ( $_GET['tab'] ) ) : $tab = $_GET['tab'];
    else:
        $tab = 'general';
    endif;
    switch ( $tab ) :
   	case 'general' :
            AWS_general_options();
            break;
        case 'classipress' :
            AWS_classipress_options();
            break;
        case 'cpauction' :
            AWS_cpauction_options();
            break;
    endswitch;
}