<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| APPROVE USERS WITHDRAW REQUEST - PRODUCT SALES
|--------------------------------------------------------------------------
*/

function AWS_cpauction_withdraw_approve( $ids = '' ) {

	if ( $ids > 0 ) $_POST["post_var"] = $ids;
	
	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "aws_cpa_affiliates";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	if( $res ) $referer_id = $res->referer_id;

	$where_array = array('id' => $id, 'type' => 'request');
	$wpdb->update($table_name, array('type' => 'withdraw'), $where_array);

	if ( empty( $ids ) ) {
	aws_affiliate_withdraw_approved_mail( $referer_id );
	exit;
	}
	}
}
add_action('wp_ajax_AWS_cpauction_approve_withraw', 'AWS_cpauction_withdraw_approve');


/*
|--------------------------------------------------------------------------
| DENY USERS WITHDRAW REQUEST - PRODUCT SALES
|--------------------------------------------------------------------------
*/

function AWS_cpauction_withdraw_deny( $ids = '' ) {

	if ( $ids > 0 ) $_POST["post_var"] = $ids;

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "aws_cpa_affiliates";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	if( $res ) $referer_id = $res->referer_id;

	$where_array = array('id' => $id, 'type' => 'request');
	$wpdb->update($table_name, array('type' => 'cancelled'), $where_array);

	if ( empty( $ids ) ) {
	aws_affiliate_withdraw_rejected_mail( $referer_id );
	exit;
	}
	}
}
add_action('wp_ajax_AWS_cpauction_deny_withraw', 'AWS_cpauction_withdraw_deny');


/*
|--------------------------------------------------------------------------
| APPROVE USERS WITHDRAW REQUEST - NEW ADS POSTED
|--------------------------------------------------------------------------
*/

function AWS_classipress_withdraw_approve( $ids = '' ) {

	if ( $ids > 0 ) $_POST["post_var"] = $ids;
	
	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "aws_clp_affiliates";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	if( $res ) $referer_id = $res->referer_id;

	$where_array = array('id' => $id, 'type' => 'request');
	$wpdb->update($table_name, array('type' => 'withdraw'), $where_array);

	if ( empty( $ids ) ) {
	aws_affiliate_withdraw_approved_mail( $referer_id );
	exit;
	}
	}
}
add_action('wp_ajax_AWS_classipress_approve_withraw', 'AWS_classipress_withdraw_approve');


/*
|--------------------------------------------------------------------------
| DENY USERS WITHDRAW REQUEST - NEW ADS POSTED
|--------------------------------------------------------------------------
*/

function AWS_classipress_withdraw_deny( $ids = '' ) {

	if ( $ids > 0 ) $_POST["post_var"] = $ids;

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "aws_clp_affiliates";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	if( $res ) $referer_id = $res->referer_id;

	$where_array = array('id' => $id, 'type' => 'request');
	$wpdb->update($table_name, array('type' => 'cancelled'), $where_array);

	if ( empty( $ids ) ) {
	aws_affiliate_withdraw_rejected_mail( $referer_id );
	exit;
	}
	}
}
add_action('wp_ajax_AWS_classipress_deny_withraw', 'AWS_classipress_withdraw_deny');