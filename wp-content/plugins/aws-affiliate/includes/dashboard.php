<?php

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes, $class ){
    	return array( 'page', 'page-template', 'page-template-cpauction', 'logged-in' );
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $cp_auction, $cpurl, $cp_options;
    	get_currentuserinfo();
	$uid = $current_user->ID;
	$role = cp_auction_get_user_role($uid);
	$ct = $cp_auction['child_theme'];
	$ip_address = cp_auction_users_ip();
	$mc_currency = get_user_meta( $uid, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;
	$cs = get_user_meta($uid, 'cp_currency', true );
	$cs = false;
	if( empty( $cs ) ) $cs = $cp_options->curr_symbol;
	$cc = $cp_options->curr_symbol;
	$ip_registered = get_user_meta( $uid, 'ip_address', true );
	//if( empty($ip_registered) )
	//update_user_meta( $uid, 'ip_address', $ip_address );
	$confirm = ''.__('You agree that you have read our policies!','awsAffiliate').'';

	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$msg = false; $error = false;

if(isset($_POST['save-settings'])) {

	$ok = 1;
	$enable = isset( $_POST['enable_affiliate'] ) ? esc_attr( $_POST['enable_affiliate'] ) : '';
	$revenue = isset( $_POST['affiliate_revenue'] ) ? esc_attr( $_POST['affiliate_revenue'] ) : '';
	$withdraw = isset( $_POST['minimum_withdraw'] ) ? esc_attr( $_POST['minimum_withdraw'] ) : '';
	$second = isset( $_POST['second_level'] ) ? esc_attr( $_POST['second_level'] ) : '';
	$parent = isset( $_POST['parent_revenue'] ) ? esc_attr( $_POST['parent_revenue'] ) : '';
	$banners = isset( $_POST['affiliate_banners'] ) ? esc_attr( $_POST['affiliate_banners'] ) : '';

	if( $revenue && !is_numeric($revenue) ) $ok = 2;
	if( $withdraw && !is_numeric($withdraw) ) $ok = 3;
	if( $parent && !is_numeric($parent) ) $ok = 4;

	if( $ok == 1 ) {

	if( $enable == "yes" ) {
	update_user_meta( $uid, 'AWS_cpauction_affiliate', $enable );
	} else {
	delete_user_meta( $uid, 'AWS_cpauction_affiliate' );
	}
	if( $revenue ) {
	update_user_meta( $uid, 'AWS_cpauction_revenue_percent', $revenue );
	} else {
	delete_user_meta( $uid, 'AWS_cpauction_revenue_percent' );
	}
	if( $withdraw ) {
	update_user_meta( $uid, 'AWS_cpauction_minimum_withdraw', $withdraw );
	} else {
	delete_user_meta( $uid, 'AWS_cpauction_minimum_withdraw' );
	}
	if( $second == "yes" ) {
	update_user_meta( $uid, 'AWS_cpauction_active_two_level', $second );
	} else {
	delete_user_meta( $uid, 'AWS_cpauction_active_two_level' );
	}
	if( $parent ) {
	update_user_meta( $uid, 'AWS_cpauction_parent_revenue_percent', $parent );
	} else {
	delete_user_meta( $uid, 'AWS_cpauction_parent_revenue_percent' );
	}
	if( $banners ) {
	update_user_meta( $uid, 'AWS_cpauction_banner', $banners );
	} else {
	delete_user_meta( $uid, 'AWS_cpauction_banner' );
	}

	$msg = ''.__('The settings was saved!', 'awsAffiliate').'';
	}
	if( $ok == 2 ) $error = ''.__('Please enter a numeric affiliate revenue!', 'awsAffiliate').'';
	if( $ok == 3 ) $error = ''.__('Please enter a numeric minimum withdraw!', 'awsAffiliate').'';
	if( $ok == 4 ) $error = ''.__('Please enter a numeric parent revenue!', 'awsAffiliate').'';
}

if(isset($_POST['send-message'])) {

	$ok = 1;
	$allowed = get_option('AWS_number_emails');
	$from = get_option('AWS_from_email');
	$name = isset( $_POST['sender_name'] ) ? esc_attr( $_POST['sender_name'] ) : '';
	$sender = isset( $_POST['sender_email'] ) ? esc_attr( $_POST['sender_email'] ) : '';
	$receivers = isset( $_POST['receiver_emails'] ) ? esc_attr( $_POST['receiver_emails'] ) : '';
	$message = isset( $_POST['sender_message'] ) ? esc_attr( $_POST['sender_message'] ) : '';

	$timeNow = time();
	$timeInFuture = time() + (60 * 60 * 24);
	$used = get_user_meta( $uid, 'AWS_used_emails', true );
	$next_emails = get_user_meta( $uid, 'AWS_next_emails', true );

	if( empty( $name ) ) $ok = 2;
	else if( !cp_auction_check_email_address($sender)) $ok = 3;
	else if( empty( $message ) ) $ok = 4;
	else if( $used >= $allowed ) {
	update_user_meta( $uid, 'AWS_used_emails', 0 );
	update_user_meta( $uid, 'AWS_next_emails', $timeInFuture );
	$ok = 5;
	}
	else if( $timeNow < $next_emails ) $ok = 5;

	if( ! empty( $from ) ) $sender = get_option('AWS_from_email');

	// get the submitted math answer
    	$rand_post_total = (int)$_POST['rand_total'];

    	// compare the submitted answer to the real answer
    	$rand_total = (int)$_POST['rand_num'] + (int)$_POST['rand_num2'];

    	// if it's a match then send the email
    	if ($rand_total != $rand_post_total) $ok = 6;

	if( $ok == 1 && $receivers ) {
        $emails = explode(PHP_EOL, $receivers);

        if( is_array( $emails ) ) {

            $i = 0;
            foreach($emails as $key => $val) {
                $i++;
                $emails[$key] = trim($val);
                $used = get_user_meta( $uid, 'AWS_used_emails', true );

                if( filter_var($emails[$key], FILTER_VALIDATE_EMAIL) && $used < $allowed ) {
                aws_affiliate_tell_friends_email($uid, trim($name), trim($sender), $emails[$key], nl2br(trim(strip_tags($message))), $i);
                $used = get_user_meta( $uid, 'AWS_used_emails', true );
                update_user_meta( $uid, 'AWS_used_emails', $used + 1 );
                }
		else {
			update_user_meta( $uid, 'AWS_used_emails', 0 );
			update_user_meta( $uid, 'AWS_next_emails', $timeInFuture );
			$ok = 5;
			break;
	      }
         }
     }
	$msg = ''.__('Your message was sent!', 'awsAffiliate').'';
}
	if( $ok == 2 ) $error = ''.__('Please enter your first and last name!', 'awsAffiliate').'';
	if( $ok == 3 ) $error = ''.__('Please enter an valid email address!', 'awsAffiliate').'';
	if( $ok == 4 ) $error = ''.__('Please enter a message!', 'awsAffiliate').'';
	if( $ok == 5 ) $error = ''.__('You have reached the daily limit of', 'awsAffiliate').' '.$allowed.' '.__('emails', 'awsAffiliate').'';
	if( $ok == 6 ) $error = ''.__('Incorrect captcha answer!', 'awsAffiliate').'';
}

	$allowed = true;
	$option = get_option( 'AWS_users_excluded' );
	$list = explode(',', $option);
	if( in_array( $uid, $list ) ) {
	$allowed = false;
	}

	cp_auction_header(); ?>

<div class="shadowblock_out" id="noise">

	<div class="shadowblock">

            	<h2 class="dotted"><?php echo the_title(); ?></h2>

		<div class="aws-box">

	<?php if( $msg ) { ?>
	<div class="notice success"><?php echo $msg; ?></div><br />
	<?php } ?>

	<?php if( $error ) { ?>
	<div class="notice error"><?php echo $error; ?></div><br />
	<?php } ?>

	<p><?php _e('Looking to make some extra money?', 'awsAffiliate'); ?></p>

	<p><?php _e('We’re happy to reward those who promote our website & Marketplace items. It’s easy to get started and we provide all the banners and text links for your web site.', 'awsAffiliate'); ?></p>

			</div><!--/aws-box-->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({ 
    activate: function (e, ui) { 
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' }); 
    },
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
    active: $.cookie('homeTab')             
});
});
</script>

<?php if ( $ct == "classipress-mydentity" ) { ?>
<style type="text/css">
.tblwide tbody tr td:nth-child(2) {
	width: 0;
}
</style>
<?php } ?>

<?php if( $ct == "eclassify" || $ct == "classipress-mydentity" ) { ?>

	<div class="tabcontrol">

		<ul class="tabnavig">
<?php } else { ?>

	<div class="tabhome">

		<ul class="tabmenu">
<?php } ?>

    <li><a href="#settings"><span class="big"><?php _e('Settings', 'awsAffiliate'); ?></span></a></li>
    <?php if(get_option('AWS_classipress_affiliate') == "yes") { ?>
    <li><a href="#ads"><span class="big"><?php _e('New Ads Revenue', 'awsAffiliate'); ?></span></a></li>
    <?php } if(get_option('AWS_cpauction_affiliate') == "yes") { ?>
    <li><a href="#products"><span class="big"><?php _e('Product Sales Revenue', 'awsAffiliate'); ?></span></a></li>
    <?php } if( get_option('AWS_tell_friends') == "yes" && $allowed ) { ?>
    <li><a href="#friends"><span class="big"><?php _e('Tell a Friend', 'awsAffiliate'); ?></span></a></li>
    <?php } ?>

	</ul>

    <div id="settings">

	<div class="clr"></div>

<div class="tabunder"><div class="tabs_title"><strong><span class="colour"><?php _e('Affiliate Settings & Details', 'awsAffiliate'); ?></span></strong></div></div>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="my-affiliate" class="aws-box">

	<?php echo '<h3 class="dotted">'.__('Affiliate Details','awsAffiliate').'</h3>'; ?>
	<div class="clear15"></div>
	<?php echo '<strong>'.__('Your Unique Affiliate ID:','awsAffiliate').'</strong> <a href="' . get_option('home') . '/?type=affiliate&id=' . $uid . '">' . $uid . '</a>'; ?>
	<div class="clear5"></div>
	<?php _e('Add your id', 'awsAffiliate'); ?> <strong>?type=affiliate&id=<?php echo $uid; ?></strong> <?php _e('to the end of any url on our site and start earning commissions!', 'awsAffiliate'); ?>
	<div class="clear15"></div>
	<?php echo '<strong>'.__('Example Link:','awsAffiliate').'</strong> <a href="' . get_option('home') . '/?type=affiliate&id=' . $uid . '">' . get_option('home') . '/?type=affiliate&id=' . $uid . '</a><div class="clear5"></div>'; ?>

	<?php if( get_option('AWS_cpauction_affiliate') == "yes" && get_option('AWS_classipress_affiliate') == "yes" ) { ?>
	<?php _e('By using the link above to promote our website so you can immediately start earning commission from sales of ads and products.', 'awsAffiliate'); ?>
	<?php } else if( get_option('AWS_cpauction_affiliate') == "yes" && get_option('AWS_classipress_affiliate') != "yes" ) { ?>
	<?php _e('By using the link above to promote our website so you can immediately start earning commission from sales of products.', 'awsAffiliate'); ?>
	<?php } else if( get_option('AWS_cpauction_affiliate') != "yes" && get_option('AWS_classipress_affiliate') == "yes" ) { ?>
	<?php _e('By using the link above to promote our website so you can immediately start earning a', 'awsAffiliate'); ?> <?php echo get_option( 'AWS_classipress_revenue_percent' ); ?>% <?php _e('commission from sales of ads.', 'awsAffiliate'); ?>
	<?php } ?>

	<div class="clear30"></div>

	<?php if( ( get_option('AWS_cpauction_affiliate') == "yes" ) && ( $cp_auction['admins_only'] != "yes" || current_user_can( 'manage_options' ) ) ) { ?>

	<?php echo '<h3 class="dotted">'.__('Affiliate Program','awsAffiliate').'</h3>'; ?>
	<div class="clear15"></div>

	<?php _e('Fill in the requested information below IF you intend to use the affiliate program for your ads. Be aware that you as the author have the full responsibility for that the affiliate revenue being paid when the withdrawal request is available from your affiliates.', 'awsAffiliate'); ?>

	<div class="clear15"></div>

	<form class="cp-ecommerce" name="mainform" action="" method="post" id="mainform">

<p>
	<label for="enable_affiliate"><a title="<?php _e('This option lets you activate the affiliate system on product sales.', 'awsAffiliate'); ?>" href="#" tip="<?php _e('This option lets you activate the affiliate system on product sales.', 'awsAffiliate'); ?>" tabindex="1"><span class="helpico"></span></a><?php _e('Enable on Product Sales', 'awsAffiliate'); ?>:</label>
	<select class="<?php echo $new_class; ?>" name="enable_affiliate" id="enable_affiliate" class="required valid"><option value="no" <?php if(get_user_meta( $uid, 'AWS_cpauction_affiliate', true ) == "no") echo 'selected="selected"'; ?>><?php _e('No', 'awsAffiliate'); ?></option><option value="yes" <?php if(get_user_meta( $uid, 'AWS_cpauction_affiliate', true ) == "yes") echo 'selected="selected"'; ?>><?php _e('Yes', 'awsAffiliate'); ?></option></select>
</p>

<p>
	<label for="affiliate_revenue"><a title="<?php _e('Enter the first level affiliate revenue, eg. 10 or 10.00', 'awsAffiliate'); ?>" href="#" tip="<?php _e('Enter the first level affiliate revenue, eg. 10 or 10.00', 'awsAffiliate'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Affiliate Revenue (%)', 'awsAffiliate'); ?>:</label>
	<input name="affiliate_revenue" id="affiliate_revenue" value="<?php echo get_user_meta( $uid, 'AWS_cpauction_revenue_percent', true ); ?>" class="required" minlength="3" type="text">
</p>

<p>
	<label for="minimum_withdraw"><a title="<?php _e('Enter the minimum withdraw amount, eg. 10 or 10.00', 'awsAffiliate'); ?>" href="#" tip="<?php _e('Enter the minimum withdraw amount, eg. 10 or 10.00', 'awsAffiliate'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Min. Withdraw', 'awsAffiliate'); ?> (<?php echo $mc_currency; ?>):</label>
	<input name="minimum_withdraw" id="minimum_withdraw" value="<?php echo get_user_meta( $uid, 'AWS_cpauction_minimum_withdraw', true ); ?>" class="required" minlength="3" type="text">
</p>

<p>
	<label for="second_level"><a title="<?php _e('This option lets you activate the second level. With this feature, if B is referred by A and C is referred B, then A gets some revenue.', 'awsAffiliate'); ?>" href="#" tip="<?php _e('This option lets you activate the second level. With this feature, if B is referred by A and C is referred B, then A gets some revenue.', 'awsAffiliate'); ?>" tabindex="4"><span class="helpico"></span></a><?php _e('Activate Second Level', 'awsAffiliate'); ?>:</label>
	<select class="<?php echo $new_class; ?>" name="second_level" id="second_level" class="required valid"><option value="no" <?php if(get_user_meta( $uid, 'AWS_cpauction_active_two_level', true ) == "no") echo 'selected="selected"'; ?>><?php _e('No', 'awsAffiliate'); ?></option><option value="yes" <?php if(get_user_meta( $uid, 'AWS_cpauction_active_two_level', true ) == "yes") echo 'selected="selected"'; ?>><?php _e('Yes', 'awsAffiliate'); ?></option></select>
</p>

<p>
	<label for="parent_revenue"><a title="<?php _e('Percentage of the top most referrer. It`s like A from example.', 'awsAffiliate'); ?>" href="#" tip="<?php _e('Percentage of the top most referrer. It`s like A from example.', 'awsAffiliate'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Parent Affiliate Revenue (%)', 'awsAffiliate'); ?>:</label>
	<input name="parent_revenue" id="parent_revenue" value="<?php echo get_user_meta( $uid, 'AWS_cpauction_parent_revenue_percent', true ); ?>" class="required" minlength="3" type="text">
</p>

<p>
	<label for="affiliate_banners"><a title="<?php _e('Your affiliate banner images. Enter ONE url per line.', 'awsAffiliate'); ?>" href="#" tip="<?php _e('Your affiliate banner images. Enter ONE url per line.', 'awsAffiliate'); ?>" tabindex="6"><span class="helpico"></span></a><?php _e('Affiliate Banners', 'awsAffiliate'); ?>:</label>
	<textarea name="affiliate_banners" id="affiliate_banners" class="required" minlength="5"><?php echo stripcslashes( get_user_meta( $uid, 'AWS_cpauction_banner', true ) ); ?></textarea><div class="clr"></div><div class="info-textarea"><small><p><?php _e('Your 468 x 60 affiliate banner images. Enter ONE url per line, eg. http://www.your-domain.com/images/banners.gif', 'awsAffiliate'); ?></p></small></div>
</p>

	<div class="clear10"></div>

	<p class="submit"><input class="btn_orange" name="save-settings" value="<?php _e('Save Settings', 'awsAffiliate'); ?>" type="submit"></p>

	</form>

	<div class="clear30"></div>

	<?php } ?><!-- /admins_only -->

<?php
        // display image banners for new ads
        $aws_banner = get_option('AWS_classipress_banner');
        $images = explode(PHP_EOL, $aws_banner);

        $banners = array();
        $uid = get_current_user_id();
        $aff_url = get_option('home') . '/?type=affiliate&id=' . $uid . '" />';

        if( $aws_banner && is_array($images) ) {

            echo '<h3 class="dotted">'.__('Website Banners','awsAffiliate').'</h3><div class="clear15"></div>';

            foreach($images as $key => $val) {
                $images[$key] = trim($val);

                if(filter_var($images[$key], FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED)) {

                    echo '<center><p><img src="'.$images[$key].'" alt="'.__('Affiliate Banner','awsAffiliate').'" /> <br>';
                    echo '<textarea cols="60" rows="3" style="border: 1px solid #ccc;">';
                    echo '<a href="'.$aff_url.'"><img src="'.$images[$key].'" alt="'.__('Affiliate Banner','awsAffiliate').'" /></a>';
                    echo '</textarea></p></center>';
           }
      }
}

	echo '<div class="clear20"></div>';

        // display image banners for products
        $referer = get_user_meta( $uid, 'AWS_referer', true );
        $aws_banner = get_user_meta( $referer, 'AWS_cpauction_banner', true );
        $images = explode(PHP_EOL, $aws_banner);

        $banners = array();
        $uid = get_current_user_id();
        $aff_url = get_option('home') . '/?type=affiliate&id=' . $uid . '" />';

        if( $aws_banner && is_array($images) ) {

            echo '<h3 class="dotted">'.__('Product Banners','awsAffiliate').'</h3><div class="clear15"></div>';

            foreach($images as $key => $val) {
                $images[$key] = trim($val);

                if(filter_var($images[$key], FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED)) {

                    echo '<center><p><img src="'.$images[$key].'" alt="'.__('Affiliate Banner','awsAffiliate').'" /> <br>';
                    echo '<textarea cols="60" rows="3" style="border: 1px solid #ccc;">';
                    echo '<a href="'.$aff_url.'"><img src="'.$images[$key].'" alt="'.__('Affiliate Banner','awsAffiliate').'" /></a>';
                    echo '</textarea></p></center>';
           }
      }
}
?>

		</div><!-- /my-affiliates aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

	</div><!-- /settings - tabcontent -->

    <?php if(get_option('AWS_classipress_affiliate') == "yes") { ?>

    <div id="ads">

	<div class="clr"></div>

<div class="tabunder"><div class="tabs_title"><strong><span class="colour"><?php _e('Your Revenue According to the New Ads Sales', 'awsAffiliate'); ?></span></strong></div></div>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="my-affiliate" class="aws-box">

        <?php if( get_option( 'AWS_classipress_active_two_level' ) == "yes" ) { ?>
        <p><?php _e('Our two-tiered affiliate program pays out an industry best of', 'awsAffiliate'); ?> <?php echo get_option( 'AWS_classipress_revenue_percent' ); ?>% <?php _e('and', 'awsAffiliate'); ?> <?php echo get_option( 'AWS_classipress_parent_revenue_percent' ); ?>%</p>
        <?php } else { ?>
        <p><?php _e('Our affiliate program pays out an industry best of', 'awsAffiliate'); ?> <?php echo get_option( 'AWS_classipress_revenue_percent' ); ?>%</p>
        <?php } ?>

<style type="text/css">

.AWS_details {
	margin:auto;
	overflow:hidden;
	padding-top:8px;
	padding-bottom:10px;
	width:100%;
}
.AWS_details1 {
	float:left;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	font-weight:bold;
	width:30%;
}
.AWS_details2 {
	float:left;
	text-align: right;
	width: 10%;
}
.AWS_details3 {
	margin-left: 5px;
	float:left;
	text-align: left;
	width: 40%;
}

</style>

<?php
    global $wpdb;

    $uid = get_current_user_id();
    $sql = "SELECT user_id
                FROM $wpdb->usermeta
                WHERE `meta_key` = 'AWS_referer'
                AND `meta_value` = '$uid'";
    $result = $wpdb->get_results($sql);
    $rowCount = $wpdb->num_rows;

    echo '<strong>'.__('Users invited by you:','awsAffiliate').'</strong> '.$rowCount.' '.__('users.','awsAffiliate').'';

    // if the user has any invited user, display them, else nothing to display
    if ($result) {
        //echo "<ul>";
        //foreach ($result as $value) {
            //$ref = get_userdata($value->user_id);
            //echo "<li>$ref->display_name</li>";
        //}
        //echo "</ul>";

    $sql2 = "SELECT *
                FROM {$wpdb->prefix}aws_clp_affiliates
                WHERE `referer_id` = $uid 
                AND `post_id` > '0'";
    $result = $wpdb->get_results($sql2);
    $rowCounts = $wpdb->num_rows;

        $total_earning = AWS_classipress_get_total_earning( $uid );
        if( empty( $total_earning ) ) $total_earning = "0.00";
        $total_withdraw = AWS_classipress_get_total_withdraw( $uid );
        if( empty( $total_withdraw ) ) $total_withdraw = "0.00";
        $balance = $total_earning - $total_withdraw;
        $minimum_withdraw = get_option( 'AWS_classipress_minimum_withdraw' );

        echo '<div class="AWS_details">';
        echo '<div class="AWS_details1">'.__('Earnings:','awsAffiliate').' </div><div class="AWS_details2">' . cp_display_price( $total_earning, $cc, false ) . '</div><div class="AWS_details3">'.__('in total from','awsAffiliate').' '.$rowCounts.' '.__('sales.','awsAffiliate').'</div><br>';
        echo '<div class="AWS_details1">'.__('Payouts:','awsAffiliate').' </div><div class="AWS_details2">' . cp_display_price( $total_withdraw, $cc, false ) . '</div><br>';
        echo '<div class="AWS_details1">'.__('Balance:','awsAffiliate').' </div><div class="AWS_details2">' . cp_display_price( $balance, $cc, false ) . '</div><br>';
        echo '<div class="AWS_details1">'.__('Withdraw Limit:','awsAffiliate').' </div><div class="AWS_details2">' . cp_display_price( $minimum_withdraw, $cc, false ) . '</div><br>';
	echo "</div>";

        // if the user is eligible for making a withdraw
        if ($balance >= $minimum_withdraw) {
        $available = false;
            // if the user submit's a request
            if (isset($_POST['AWS_classipress_submit'])) {

                $nonce = $_REQUEST['_wpnonce'];
                if (wp_verify_nonce($nonce, 'AWS_classipress_money_withdraw')) {

                    // if the user entered a valid withdraw requst
                    $withdraw_req_money = filter_var($_POST['AWS_classipress_amount'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

                    $total_earn = AWS_classipress_get_total_balance(AWS_classipress_get_total_earning($uid), AWS_classipress_get_total_withdraw($uid));

                    // if requested money is greater than earn, submit a request
                    if (($withdraw_req_money <= $total_earn) && ($withdraw_req_money > 0)) {
                        echo '<div class="notice success">'.__('Your withdraw request is being processed. You`ll be notified when it`s approved.','awsAffiliate').'</div><div class="clear15"></div>';

                        // make a withdraw request
                        AWS_classipress_submit_withdraw_req($uid, $withdraw_req_money);

                        //send a notification mail to the admin
                        aws_affiliate_admin_withdraw_request_mail();
                    }
                    else {
                        echo '<font color="red">'.__('Invalid Request','awsAffiliate').'</font>';
                    }
                }
            }// end isset submit

            // if it's a cancel request
            if ( isset( $_GET['action'] ) == 'cancel') {
                $nonce = $_REQUEST['_wpnonce'];
                if (wp_verify_nonce($nonce, 'AWS_classipress_cancel_withdraw')) {
                    $req_id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
                    AWS_classipress_withdraw_deny($req_id);
                }
            }

            // if the user has previous withdraw's, show them
            $withdraw_list = AWS_classipress_get_withdraw_req_list($uid, '');
            if ($withdraw_list) {
            echo '<strong>'.__('Active Withdraw Request:','awsAffiliate').'</strong><div class="clr"></div>';
            $i = 0;
            foreach ($withdraw_list as $wl) {
            $i++;
                echo '#'.$i.' - ' . cp_display_price( $wl->revenue, $cc, false ) . ' '.__('on','awsAffiliate').' ' . date( 'd M, Y', strtotime( $wl->updated ) ) . ' [<a href="' . wp_nonce_url( '?&action=cancel&id=' . $wl->id, 'AWS_classipress_cancel_withdraw' ) . '" >'.__('cancel','awsAffiliate').'</a>]<div class="clr"></div>';
            }
            echo '<div class="clear20"></div>';
            }
            else {
            $available = true;
?>
            <p><?php _e('Make sure you have entered your PayPal email in the <strong>Email</strong> field of your profile before you attempt to send any withdraw requests.','awsAffiliate'); ?></p>

<?php
	}
}
?>

        <h3 class="dotted"><?php _e( 'Order Overview','awsAffiliate' ); ?></h3>
        <div class="clear5"></div>
        <table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column"><div style="text-align: left;"><?php _e( 'Seller','awsAffiliate' ); ?></div></th>
		<th data-hide="phone"><?php _e( 'Earnings','awsAffiliate' ); ?></th>
		<th data-hide="phone"><?php _e( 'Payouts','awsAffiliate' ); ?></th>
		<th data-hide="phone"><?php _e( 'Balance','awsAffiliate' ); ?></th>
		<th class="footable-last-column" data-hide="phone"><div style="text-align: center;"><?php _e( 'Action','awsAffiliate' ); ?></div></th>
		</tr>
		</thead>
		<tbody>

            <?php
            $sql = "SELECT sum(revenue) as earning, id, referer_id
                            FROM {$wpdb->prefix}aws_clp_affiliates
                            WHERE `type`='earn' AND `referer_id`='$uid'
                            GROUP BY referer_id";
            $query = $wpdb->get_results( $sql, OBJECT );
            $num_rows = $wpdb->num_rows;

            $rowclass = false;
            if ( $num_rows > 0 ) {
                foreach ($query as $row) {
                $rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

                    $total_earning = AWS_classipress_get_total_earning( $row->referer_id );
                    if( empty( $total_earning ) ) $total_earning = "0.00";
                    $total_withdraw = AWS_classipress_get_total_withdraw( $row->referer_id );
                    if( empty( $total_withdraw ) ) $total_withdraw = "0.00";
                    $balance = $total_earning - $total_withdraw;
                    ?>
                    <tr id="<?php echo $row->id; ?>" class="<?php echo $rowclass; ?>" valign="top">
                        <td><?php _e('admin','awsAffiliate'); ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $total_earning, $cc, false ); ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $total_withdraw, $cc, false ); ?></td>
                        <td style="text-align: center;"><?php if ( $balance >= $minimum_withdraw && $available == true ) { ?><form name="AWS_classipress_withdraw" method="post" action=""><?php wp_nonce_field( 'AWS_classipress_money_withdraw' ) ?><input type="text" id="AWS_classipress_amount" name="AWS_classipress_amount" size="10" value="<?php echo cp_display_price( $balance, $cc, false ); ?>" readonly="readonly" /><?php } else { ?><?php echo cp_display_price( $balance, $cc, false ); ?><?php } ?></td>
                        <td style="text-align: center;"><?php if ( $balance >= $minimum_withdraw && $available == true ) { ?><input type="submit" name="AWS_classipress_submit" value="<?php _e('Send Withdraw Request','awsAffiliate'); ?>" />
                        </form>
                        <?php } else {?><?php _e('n/a','awsAffiliate'); ?><?php } ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            else {
                    echo '<tr valign="top">
                        <td colspan="5">'.__( 'There was no transactions found.','awsAffiliate' ).'</td>
                    </tr>';
            }
            ?>
        </tbody>
        </table>

        <div class="clear20"></div>

<?php }
    // the user has not any invited user
    else {
        echo '<ul>';
        echo '<li><i>'.__('There are no users invited by you.','awsAffiliate').'</i></li>';
        echo '</ul>';
    }

    // if the user has any transaction history, show them
    if ($history = AWS_classipress_get_transaction_history($uid)) {
        echo '<div class="clear20"></div>';
        echo '<h3 class="dotted">'.__( 'Transaction History','awsAffiliate' ).'</h3><div class="clear5"></div>';
        echo '<select class="'.$new_class.'" name="transaction_history" id="AWS_trans">';
        foreach ($history as $s) {
            echo '<option value="">'. cp_display_price( $s->revenue, $cc, false ) . ' '.$s->type.' '.__('on','awsAffiliate').' '.$s->updated.'</option>';
        }
        echo '</select>';
    }

?>

		</div><!-- /my-affiliates aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

	</div><!-- /ads - tabcontent -->

    <?php } if(get_option('AWS_cpauction_affiliate') == "yes") { ?>

    <div id="products">

	<div class="clr"></div>

<div class="tabunder"><div class="tabs_title"><strong><span class="colour"><?php _e('Your Revenue According to the Product Sales', 'awsAffiliate'); ?></span></strong></div></div>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="my-affiliate" class="aws-box">

        <p><?php _e('You can earn commissions on product sales if your referred user completes the purchase of a seller participating in the affiliate program dedicated to our sellers. Revenues may vary depending on the percentage determined by the current seller.', 'awsAffiliate'); ?></p>

<style type="text/css">

.AWS_details {
	margin:auto;
	overflow:hidden;
	padding-top:8px;
	padding-bottom:10px;
	width:100%;
}
.AWS_details1 {
	float:left;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	font-weight:bold;
	width:30%;
}
.AWS_details2 {
	float:left;
	text-align: right;
	width: 10%;
}
.AWS_details3 {
	margin-left: 5px;
	float:left;
	text-align: left;
	width: 40%;
}

</style>

<?php
    global $wpdb;

    $referer_id = get_user_meta( $uid, 'AWS_referer', true );
    $sql = "SELECT user_id
                FROM $wpdb->usermeta
                WHERE `meta_key` = 'AWS_referer'
                AND `meta_value` = '$uid'";
    $result = $wpdb->get_results($sql);
    $rowCount = $wpdb->num_rows;

    echo '<strong>'.__('Users invited by you:','awsAffiliate').'</strong> '.$rowCount.' '.__('users.','awsAffiliate').'';

    // if the user has any invited user, display them, else nothing to display
    if ($result) {

    $sql2 = "SELECT *
                FROM {$wpdb->prefix}aws_cpa_affiliates
                WHERE `referer_id` = $uid 
                AND `order_id` > '0'";
    $result = $wpdb->get_results($sql2);
    $rowCounts = $wpdb->num_rows;

        $total_earning = AWS_cpauction_get_total_earning( $uid );
        if( empty( $total_earning ) ) $total_earning = "0.00";
        $total_withdraw = AWS_cpauction_get_total_withdraw( $uid );
        if( empty( $total_withdraw ) ) $total_withdraw = "0.00";
        $balance = $total_earning - $total_withdraw;
        $minimum_withdraw = get_user_meta( $referer_id, 'AWS_cpauction_minimum_withdraw', true );
        if( empty( $minimum_withdraw ) ) $minimum_withdraw = '0.00';

        echo '<div class="AWS_details">';
        echo '<div class="AWS_details1">'.__('Earnings:','awsAffiliate').' </div><div class="AWS_details2">' . cp_display_price( $total_earning, $cc, false ) . '</div><div class="AWS_details3">'.__('in total from','awsAffiliate').' '.$rowCounts.' '.__('sales.','awsAffiliate').'</div><br>';
        echo '<div class="AWS_details1">'.__('Payouts:','awsAffiliate').' </div><div class="AWS_details2">' . cp_display_price( $total_withdraw, $cc, false ) . '</div><br>';
        echo '<div class="AWS_details1">'.__('Balance:','awsAffiliate').' </div><div class="AWS_details2">' . cp_display_price( $balance, $cc, false ) . '</div><br>';
        echo '<div class="AWS_details1">'.__('Withdraw Limit:','awsAffiliate').' </div><div class="AWS_details2">' . cp_display_price( $minimum_withdraw, $cc, false ) . '</div><br>';
	echo "</div>";

        // if the user is eligible for making a withdraw
        if ($balance >= $minimum_withdraw) {
        $available = false;
            // if the user submit's a request
            if (isset($_POST['AWS_cpauction_submit'])) {

                $seller_id = false; if( isset( $_POST['AWS_Seller'] ) ) $seller_id = $_POST['AWS_Seller'];
                $nonce = $_REQUEST['_wpnonce'];
                if (wp_verify_nonce($nonce, 'AWS_cpauction_money_withdraw')) {

                    // if the user entered a valid withdraw requst
                    $withdraw_req_money = filter_var($_POST['AWS_cpauction_amount'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

                    $total_earn = AWS_cpauction_get_total_balance(AWS_cpauction_get_total_earning($uid), AWS_cpauction_get_total_withdraw($uid));

                    // if requested money is greater than earn, submit a request
                    if (($withdraw_req_money <= $total_earn) && ($withdraw_req_money > 0)) {
                        echo '<div class="notice success">'.__('Your withdraw request is being processed. You`ll be notified when it`s approved.','awsAffiliate').'</div><div class="clear15"></div>';

                        // make a withdraw request
                        AWS_cpauction_submit_withdraw_req($uid, $withdraw_req_money, $seller_id);

                        //send a notification mail to the seller
                        aws_affiliate_seller_withdraw_request_mail($seller_id);
                    }
                    else {
                        echo '<font color="red">'.__('Invalid Request','awsAffiliate').'</font>';
                    }
                }
            }// end isset submit

            // if it's a cancel request
            if ( isset( $_GET['action'] ) == 'cancel') {
                $nonce = $_REQUEST['_wpnonce'];
                if (wp_verify_nonce($nonce, 'AWS_cpauction_cancel_withdraw')) {
                    $req_id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
                    AWS_cpauction_withdraw_deny($req_id);
                }
            }

            // if the user has previous withdraw's, show them
            $withdraw_list = AWS_cpauction_get_withdraw_req_list($uid, '');
            if ($withdraw_list) {
            echo '<strong>'.__('Active Withdraw Request:','awsAffiliate').'</strong><div class="clr"></div>';
            $i = 0;
            foreach ($withdraw_list as $wl) {
            $i++;
            $user = get_userdata( $wl->seller_id );
                echo '#'.$i.' - ' . cp_display_price( $wl->revenue, $cc, false ) . ' '.__('from','awsAffiliate').' <strong>'.$user->user_nicename.'</strong> '.__('on','awsAffiliate').' ' . date( 'd M, Y', strtotime( $wl->updated ) ) . ' [<a href="' . wp_nonce_url( '?&action=cancel&id=' . $wl->id, 'AWS_cpauction_cancel_withdraw' ) . '" >'.__('cancel','awsAffiliate').'</a>]<div class="clr"></div>';
            }
            echo '<div class="clear20"></div>';
            }
            else {
            $available = true;
?>
            <p><?php _e('Make sure you have entered your PayPal email in the <strong>Email</strong> field of your profile before you attempt to send any withdraw requests.','awsAffiliate'); ?></p>

<?php
	}
}
?>

        <h3 class="dotted"><?php _e( 'Order Overview','awsAffiliate' ); ?></h3>
        <div class="clear5"></div>
        <table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column"><div style="text-align: left;"><?php _e( 'Seller','awsAffiliate' ); ?></div></th>
		<th data-hide="phone"><?php _e( 'Earnings','awsAffiliate' ); ?></th>
		<th data-hide="phone"><?php _e( 'Payouts','awsAffiliate' ); ?></th>
		<th data-hide="phone"><?php _e( 'Balance','awsAffiliate' ); ?></th>
		<th class="footable-last-column" data-hide="phone"><div style="text-align: center;"><?php _e( 'Action','awsAffiliate' ); ?></div></th>
		</tr>
		</thead>
		<tbody>

            <?php
            $sql = "SELECT sum(revenue) as earning, id, referer_id, seller_id
                            FROM {$wpdb->prefix}aws_cpa_affiliates
                            WHERE `type`='earn' AND `referer_id`='$uid'
                            GROUP BY seller_id";
            $query = $wpdb->get_results( $sql, OBJECT );
            $num_rows = $wpdb->num_rows;

            $rowclass = false;
            if ( $num_rows > 0 ) {
                foreach ($query as $row) {
                $rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

                    $user = get_userdata( $row->seller_id );

                    $total_earning = AWS_cpauction_get_total_earning( $row->referer_id, $row->seller_id );
                    if( empty( $total_earning ) ) $total_earning = "0.00";
                    $total_withdraw = AWS_cpauction_get_total_withdraw( $row->referer_id, $row->seller_id );
                    if( empty( $total_withdraw ) ) $total_withdraw = "0.00";
                    $balance = $total_earning - $total_withdraw;
                    ?>
                    <tr id="<?php echo $row->id; ?>" class="<?php echo $rowclass; ?>" valign="top">
                        <td><?php echo $user->user_nicename ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $total_earning, $cc, false ); ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $total_withdraw, $cc, false ); ?></td>
                        <td style="text-align: center;"><?php if ( $balance >= $minimum_withdraw && $available == true ) { ?><form name="AWS_cpauction_withdraw" method="post" action=""><?php wp_nonce_field( 'AWS_cpauction_money_withdraw' ) ?><input type="hidden" name="AWS_Seller" value="<?php echo $row->seller_id; ?>"><input type="text" id="AWS_cpauction_amount" name="AWS_cpauction_amount" size="10" value="<?php echo cp_display_price( $balance, $cc, false ); ?>" readonly="readonly" /><?php } else { ?><?php echo cp_display_price( $balance, $cc, false ); ?><?php } ?></td>
                        <td style="text-align: center;"><?php if ( $balance >= $minimum_withdraw && $available == true ) { ?><input type="submit" name="AWS_cpauction_submit" value="<?php _e('Send Withdraw Request','awsAffiliate'); ?>" />
                        </form>
                        <?php } else {?><?php _e('n/a','awsAffiliate'); ?><?php } ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            else {
                    echo '<tr valign="top">
                        <td colspan="5">'.__( 'There was no transactions found.','awsAffiliate' ).'</td>
                    </tr>';
            }
            ?>
        </tbody>
        </table>

        <div class="clear20"></div>

<?php }
    // the user has not any invited user
    else {
        echo '<ul>';
        echo '<li><i>'.__('There are no users invited by you.','awsAffiliate').'</i></li>';
        echo '</ul>';
    }

    // if the user has any transaction history, show them
    if ($history = AWS_cpauction_get_transaction_history($uid)) {
        echo '<div class="clear20"></div>';
        echo '<h3 class="dotted">'.__( 'Transaction History','awsAffiliate' ).'</h3><div class="clear5"></div>';
        echo '<select class="'.$new_class.'" name="transaction_history" id="AWS_trans">';
        foreach ($history as $s) {
            echo '<option value="">'. cp_display_price( $s->revenue, $cc, false ) . ' '.$s->type.' '.__('on','awsAffiliate').' '.$s->updated.'</option>';
        }
        echo '</select>';
    }

        echo '<div class="clear20"></div>';

        $withdraw_list = AWS_cpauction_get_withdraw_req_list('', $uid);
        if ( $withdraw_list ) {
            ?>
            <h3 class="dotted"><?php _e( 'Withdraw Request','awsAffiliate' ); ?></h3>
            <div class="clear5"></div>
            <table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column"><div style="text-align: left;"><?php _e( 'User','awsAffiliate' ); ?></div></th>
		<th data-hide="phone"><?php _e( 'Amount','awsAffiliate' ); ?></th>
		<th data-hide="phone"><?php _e( 'Balance','awsAffiliate' ); ?></th>
		<th data-hide="phone"><?php _e( 'Date','awsAffiliate' ); ?></th>
		<th class="footable-last-column" data-hide="phone"><div style="text-align: center;"><?php _e( 'Action','awsAffiliate' ); ?></div></th>
		</tr>
		</thead>
		<tbody>

		<?php $rowclass = false; ?>
                <?php foreach ($withdraw_list as $withdraw_reqs): ?>
                <?php $rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even'; ?>
                    <?php $user = get_userdata( $withdraw_reqs->referer_id ); ?>

                    <tr id="<?php echo $withdraw_reqs->id; ?>" class="<?php echo $rowclass; ?>" valign="top">
                        <td><?php echo $user->user_nicename; ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $withdraw_reqs->revenue, $cc, false) ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( AWS_cpauction_get_total_balance( AWS_cpauction_get_total_earning( $user->ID, $uid ), AWS_cpauction_get_total_withdraw( $user->ID, $uid ) ), $cc, false ) ?></td>
                        <td style="text-align: center;"><?php echo $withdraw_reqs->updated ?></td>
                        <td style="text-align: center;"><a href="https://www.paypal.com/cgi-bin/webscr?on0=<?php _e( 'Receiver','awsAffiliate' ); ?>&os0=<?php echo $user->user_nicename; ?>&on1=Reference&os1=<?php _e( 'Withdraw','awsAffiliate' ); ?>&amount=<?php echo $withdraw_reqs->revenue; ?>&item_name=<?php _e( 'Withdraw','awsAffiliate' ); ?>&cmd=_xclick&business=<?php echo $user->user_email; ?>&no_shipping=1&currency_code=<?php echo $mc_currency; ?>&lc=EN" target="_blank"><?php _e('Pay', 'awsAffiliate'); ?></a> | <a href="#" class="aws-cpauction-approve-withdraw" title="<?php _e('Approve users withdraw request.', 'awsAffiliate'); ?>" id="<?php echo $withdraw_reqs->id; ?>"><?php _e( 'Approve','awsAffiliate' ); ?></a> | <a href="#" class="aws-cpauction-deny-withdraw" title="<?php _e('Reject users withdraw request.', 'awsAffiliate'); ?>" id="<?php echo $withdraw_reqs->id; ?>"><?php _e( 'Reject','awsAffiliate' ); ?></a></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            </table>
            <font color="red"><strong>*</strong></font> <?php _e( 'Make payment <strong>before</strong> you approve the request.','awsAffiliate' ); ?>
            <br /><br />
            <?php
        }
        echo '<div class="clear20"></div>';
        ?>

        <h3 class="dotted"><?php _e( 'All User`s Status','awsAffiliate' ); ?></h3>
        <div class="clear5"></div>
        <table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column"><div style="text-align: left;"><?php _e( 'User','awsAffiliate' ); ?></div></th>
		<th data-hide="phone"><?php _e( 'Earnings','awsAffiliate' ); ?></th>
		<th data-hide="phone"><?php _e( 'Withdraw','awsAffiliate' ); ?></th>
		<th data-hide="phone"><?php _e( 'Balance','awsAffiliate' ); ?></th>
		<th class="footable-last-column" data-hide="phone"><div style="text-align: right;"><?php _e( 'Affiliate ID','awsAffiliate' ); ?></div></th>
		</tr>
		</thead>
		<tbody>
            <?php
            $sql = "SELECT sum(revenue) as earning, id, referer_id
                            FROM {$wpdb->prefix}aws_cpa_affiliates
                            WHERE `type`!='request' AND `seller_id`='$uid'
                            GROUP BY referer_id";
            $query = $wpdb->get_results( $sql, OBJECT );
            $num_rows = $wpdb->num_rows;

            $rowclass = false;
            if ( $num_rows > 0 ) {
                foreach ($query as $row) {
                $rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

                    $user = get_userdata( $row->referer_id );

                    $total_earning = AWS_cpauction_get_total_earning( $row->referer_id, $uid );
                    if( empty( $total_earning ) ) $total_earning = "0.00";
                    $total_withdraw = AWS_cpauction_get_total_withdraw( $row->referer_id, $uid );
                    if( empty( $total_withdraw ) ) $total_withdraw = "0.00";
                    $balance = $total_earning - $total_withdraw;
                    ?>
                    <tr id="<?php echo $row->id; ?>" class="<?php echo $rowclass; ?>" valign="top">
                        <td><?php echo $user->user_nicename; ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $total_earning, $cc, false ); ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $total_withdraw, $cc, false ); ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $balance, $cc, false ); ?></td>
                        <td style="text-align: right;"><?php echo $row->referer_id; ?></td>
                    </tr>
                    <?php
                }
            }
            else {
                    echo '<tr valign="top">
                        <td colspan="5">'.__( 'There was no active affiliates found.','awsAffiliate' ).'</td>
                    </tr>';
            }
            ?>
        </tbody>
        </table>

		</div><!-- /my-affiliates aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

	</div><!-- /products - tabcontent -->

    <?php } ?><!-- /if enabled -->

    <?php if( get_option('AWS_tell_friends') == "yes" && $allowed ) { ?>

    <div id="friends">

	<div class="clr"></div>

<div class="tabunder"><div class="tabs_title"><strong><span class="colour"><?php _e('Tell Your Friends About Us', 'awsAffiliate'); ?></span></strong></div></div>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="my-affiliate" class="aws-box">

	<p><?php _e('Send an email including your affiliate link to your friends, let them know about our services and products. You can send up to', 'awsAffiliate'); ?> <strong><?php echo get_option('AWS_number_emails'); ?></strong> <?php _e('emails per day. Do not send spam as it will result in your account being closed and any available commission will be withheld.', 'awsAffiliate'); ?></p>

	<div class="clear15"></div>

	<form class="cp-ecommerce" name="mainform" action="" method="post" id="mainform">

<p>
	<label for="sender_name"><a title="<?php _e('Enter your full name.', 'awsAffiliate'); ?>" href="#" tip="<?php _e('Enter your full name.', 'awsAffiliate'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Your First & Last Name', 'awsAffiliate'); ?>:</label>
	<input name="sender_name" id="sender_name" value="" class="required" minlength="3" type="text">
</p>

<p>
	<label for="sender_email"><a title="<?php _e('Enter your email address.', 'awsAffiliate'); ?>" href="#" tip="<?php _e('Enter your email address.', 'awsAffiliate'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Your Email Address', 'awsAffiliate'); ?>:</label>
	<input name="sender_email" id="sender_email" value="" class="required" minlength="3" type="text">
</p>

<p>
	<label for="receiver_emails"><a title="<?php _e('Enter your friends email addresses, ONE address per line.', 'awsAffiliate'); ?>" href="#" tip="<?php _e('Enter your friends email addresses, ONE address per line.', 'awsAffiliate'); ?>" tabindex="6"><span class="helpico"></span></a><?php _e('Friends Email Addresses', 'awsAffiliate'); ?>:</label>
	<textarea name="receiver_emails" id="receiver_emails" class="required" minlength="5"></textarea><div class="clr"></div><div class="info-textarea"><small><p><?php _e('Enter your friends email addresses, only ONE address per line.', 'awsAffiliate'); ?></p></small></div>
</p>

<p>
	<label for="sender_message"><a title="<?php _e('Enter a message for your friends.', 'awsAffiliate'); ?>" href="#" tip="<?php _e('Enter a message for your friends.', 'awsAffiliate'); ?>" tabindex="6"><span class="helpico"></span></a><?php _e('Message', 'awsAffiliate'); ?>:</label>
	<textarea name="sender_message" id="sender_message" class="required" minlength="5"></textarea><div class="clr"></div><div class="info-textarea"><small><p><?php _e('Enter some information to your friends, your affiliate ID linked to a product etc. HTML can be used.', 'awsAffiliate'); ?></p></small></div>
</p>

<?php
	// create a random set of numbers for spam prevention
	$randomNum = '';
	$randomNum2 = '';
	$randomNumTotal = '';

	$rand_num = rand(0,9);
	$rand_num2 = rand(0,9);
	$randomNumTotal = $randomNum + $randomNum2;
?>

<p>
	<label for="rand_total"><?php _e('Sum of', 'awsAffiliate'); ?> <?php echo $rand_num; ?> + <?php echo $rand_num2; ?> =</label>
	<input tabindex="5" id="rand_total" name="rand_total" minlength="1" value="" type="text" />
</p>

	<input type="hidden" name="rand_num" value="<?php echo $rand_num; ?>" />
	<input type="hidden" name="rand_num2" value="<?php echo $rand_num2; ?>" />

<p class="submit">
	<input tabindex="6" type="submit" name="send-message" onclick="return confirm('<?php echo $confirm; ?>')" value="<?php _e('Send Message', 'awsAffiliate'); ?>" class="btn_orange" />
</p>

	</form>

	<div class="clear10"></div>

		</div><!-- /my-affiliates aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

	</div><!-- /tell friends - tabcontent -->

    <?php } ?><!-- /if enabled -->

</div><!-- /tabcontrol -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>