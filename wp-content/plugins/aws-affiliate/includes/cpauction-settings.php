<?php

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| CP AUCTION OPTIONS
|--------------------------------------------------------------------------
*/

add_action( 'admin_init', 'AWS_cpauction_settings' );

function AWS_cpauction_options() {
    global $wpdb, $cp_options;
    $cc = $cp_options->curr_symbol;
    $mc_currency = $cp_options->currency_code; ?>

    <div class="wrap">
    	<div class="icon32" id="icon-edit-pages"><br/></div>
        <h3><?php _e('Product Revenue Settings','awsAffiliate'); ?></h3>
        <p><?php _e('This option works for your sellers that have ads on your site, they can through their frontend dashboard put the same settings that you can do for your new ads affiliates. You as the administrator will have full control from the tables below whether sellers actually performs the payouts as requested by their affiliates.','awsAffiliate'); ?></p>

<?php if( get_option('cp_auction_installed') != "yes" ) { ?>

	<p><?php _e('Sorry, this feature is only available for users of CP Auction, our','awsAffiliate'); ?> <a style="text-decoration: none;" href="http://www.arctic-websolutions.com/" target="_blank">ClassiPress Auction & eCommerce</a> <?php _e('Plugin.','awsAffiliate'); ?></p>

	</div>
<?php } else { ?>

        <form method="post" action="options.php">
            <?php wp_nonce_field( 'update-options' ); ?>
            <?php settings_fields( 'AWS_cpauction_group' ); ?>
            <?php do_settings_sections( 'AWS_cpauction_group' ); ?>
            <table class="form-table">

                <tr valign="top">
                    <th scope="row"><?php _e('Enable on Product Sales','awsAffiliate'); ?>:</th>
                    <td>
                        <select name="AWS_cpauction_affiliate">
                        <option value="no" <?php if(get_option('AWS_cpauction_affiliate') == 'no') echo 'selected="selected"'; ?>><?php _e('No', 'awsAffiliate'); ?></option>
                        <option value="yes" <?php if(get_option('AWS_cpauction_affiliate') == 'yes') echo 'selected="selected"'; ?>><?php _e('Yes', 'awsAffiliate'); ?></option>
			</select>
			<span class="description">
                            <?php _e('This option lets you activate the affiliate system on product sales.','awsAffiliate'); ?>
                        </span>
                    </td>
                </tr>

            </table>
            <?php
            $opts = "AWS_cpauction_affiliate";
            //$opts .= "AWS_cpauction_parent_revenue_percent, AWS_cpauction_banner";
            ?>

            <input type="hidden" name="action" value="update" />
            <input type="hidden" name="page_options" value="<?php echo $opts; ?>" />

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e( 'Save Changes','awsAffiliate' ); ?>" />
            </p>

        </form>

        <?php
        $rowclass = false;
        $withdraw_list = AWS_cpauction_get_withdraw_req_list();
        if ( $withdraw_list ) {
            ?>
            <h3><?php _e( 'Withdraw Request','awsAffiliate' ); ?></h3>
            <table class="widefat">
                <thead>
                    <tr valign="top">
                        <th scope="col"><?php _e( 'User ID','awsAffiliate' ); ?></th>
                        <th scope="col"><?php _e( 'Username','awsAffiliate' ); ?></th>
                        <th scope="col"><?php _e( 'E-Mail','awsAffiliate' ); ?></th>
                        <th scope="col" style="text-align: center;"><?php _e( 'Withdraw Amount','awsAffiliate' ); ?></th>
                        <th scope="col" style="text-align: center;"><?php _e( 'Total Balance','awsAffiliate' ); ?></th>
                        <th scope="col" style="text-align: center;"><?php _e( 'Time','awsAffiliate' ); ?></th>
                        <th scope="col" style="text-align: center;"><?php _e( 'Action','awsAffiliate' ); ?></th>
                    </tr>
                </thead>
                <?php foreach ($withdraw_list as $withdraw_reqs): ?>
                <?php $rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even'; ?>
                    <?php $user = get_userdata( $withdraw_reqs->referer_id ); ?>

                    <tr class="<?php echo $rowclass; ?>">
                        <td><?php echo $user->ID ?></td>
                        <td><?php echo $user->user_nicename ?></td>
                        <td><?php echo $user->user_email ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( $withdraw_reqs->revenue, $cc, false) ?></td>
                        <td style="text-align: right;"><?php echo cp_display_price( AWS_cpauction_get_total_balance( AWS_cpauction_get_total_earning( $user->ID ), AWS_cpauction_get_total_withdraw( $user->ID ) ), $cc, false ) ?></td>
                        <td style="text-align: center;"><?php echo $withdraw_reqs->updated ?></td>
                        <td style="text-align: center;"><a href="https://www.paypal.com/cgi-bin/webscr?on0=<?php _e( 'Receiver','awsAffiliate' ); ?>&os0=<?php echo $user->user_nicename; ?>&on1=Reference&os1=<?php _e( 'Withdraw','awsAffiliate' ); ?>&amount=<?php echo $withdraw_reqs->revenue; ?>&item_name=<?php _e( 'Withdraw','awsAffiliate' ); ?>&cmd=_xclick&business=<?php echo $user->user_email; ?>&no_shipping=1&currency_code=<?php echo $mc_currency; ?>&lc=EN" target="_blank"><?php _e('Pay', 'awsAffiliate'); ?></a> | <a href="#" class="aws-cpauction-approve-withdraw" title="<?php _e('Approve users withdraw request.', 'awsAffiliate'); ?>" id="<?php echo $withdraw_reqs->id; ?>"><?php _e( 'Approve','awsAffiliate' ); ?></a> | <a href="#" class="aws-cpauction-deny-withdraw" title="<?php _e('Reject users withdraw request.', 'awsAffiliate'); ?>" id="<?php echo $withdraw_reqs->id; ?>"><?php _e( 'Reject','awsAffiliate' ); ?></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <font color="red"><strong>*</strong></font> <?php _e( 'Make payment <strong>before</strong> you approve the request.','awsAffiliate' ); ?>
            <br /><br />
            <?php
        }
        ?>
        <h3><?php _e( 'All User`s Status','awsAffiliate' ); ?></h3>
        <table class="widefat">
            <thead>
                <tr valign="top">
                    <th scope="col"><?php _e( 'User ID','awsAffiliate' ); ?></th>
                    <th scope="col"><?php _e( 'Username','awsAffiliate' ); ?></th>
                    <th scope="col"><?php _e( 'E-Mail','awsAffiliate' ); ?></th>
                    <th scope="col" style="text-align: center;"><?php _e( 'Earnings','awsAffiliate' ); ?></th>
                    <th scope="col" style="text-align: center;"><?php _e( 'Payouts','awsAffiliate' ); ?></th>
                    <th scope="col" style="text-align: center;"><?php _e( 'Balance','awsAffiliate' ); ?></th>
                    <th scope="col"><?php _e( 'Affiliate URL','awsAffiliate' ); ?></th>
                </tr>
            </thead>

<?php
	$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

	$limit = 20; // number of rows in page
	$offset = ( $pagenum - 1 ) * $limit;
	$sql = "SELECT sum(revenue) as earning, referer_id FROM {$wpdb->prefix}aws_cpa_affiliates WHERE `type`!='request' GROUP BY referer_id";
	$query = $wpdb->get_results( $sql, OBJECT );
	$total = $wpdb->num_rows;
	$num_of_pages = ceil( $total / $limit );

	$result = $wpdb->get_results("SELECT sum(revenue) as earning, referer_id
                            FROM {$wpdb->prefix}aws_cpa_affiliates
                            WHERE `type` != 'request' GROUP BY referer_id LIMIT $offset, $limit");
	$rowclass = false;
	$data_html = false;

	if ( $total > 0 ) {
	foreach( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$user = get_userdata( $row->referer_id );
	$total_earning = AWS_cpauction_get_total_earning( $row->referer_id );
	if( empty( $total_earning ) ) $total_earning = "0.00";
	$total_withdraw = AWS_cpauction_get_total_withdraw( $row->referer_id );
	if( empty( $total_withdraw ) ) $total_withdraw = "0.00";
	$balance = $total_earning - $total_withdraw;

	$total_earning = cp_display_price( $total_earning, $cc, false );
	$total_withdraw = cp_display_price( $total_withdraw, $cc, false );
	$balance = cp_display_price( $balance, $cc, false );
	$ref_url = ''.home_url().'/?type=affiliate&id='.$row->referer_id.'';

?>
	<?php $html  = "<tr valign=\"top\" class=\"". $rowclass ."\">";?>
	<?php $html .= "<td><div class=\"row-title\">". $row->referer_id ."</div></td>";?>
	<?php $html .= "<td>". $user->user_nicename ."</td>";?>
	<?php $html .= "<td>". $user->user_email ."</td>";?>
	<?php $html .= "<td style=\"text-align: right;\">". $total_earning ."</td>";?>
	<?php $html .= "<td style=\"text-align: right;\">". $total_withdraw ."</td>";?>
	<?php $html .= "<td style=\"text-align: right;\">". $balance ."</td>";?>
	<?php $html .= "<td>". $ref_url ."</td>";?>
	<?php $html .= "</tr>"?>
	<?php
	$data_html .=$html; 
	}
}
	echo $data_html;
	echo '</table>'; ?>

<style>
.tablenav-pages span.current {
	font-size: 14px;
	font-weight: 400;
	background: none repeat scroll 0% 0% #FFF;
	padding: 0px 10px 3px;
}
.tablenav .tablenav-pages a {
	font-size: 14px;
	font-weight: 400;
}
</style>

<?php
$page_links = paginate_links( array(
	'base' => add_query_arg( 'pagenum', '%#%' ),
	'format' => '',
	'prev_text' => __( '&laquo;', 'awsAffiliate' ),
	'next_text' => __( '&raquo;', 'awsAffiliate' ),
	'total' => $num_of_pages,
	'current' => $pagenum
    ) );

if ( $page_links ) {
	echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . 
	$page_links . '</div></div>';
}
?>

    </div>
    <?php
}
}


/*
|--------------------------------------------------------------------------
| REGISTER OPTIONS VARIABLE
|--------------------------------------------------------------------------
*/

function AWS_cpauction_settings() {
    register_setting( 'AWS_cpauction_group', 'AWS_cpauction_affiliate' );
}