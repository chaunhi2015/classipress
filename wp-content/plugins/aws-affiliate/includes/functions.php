<?php

/*
Copyright 2014 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| TRANSLATE .JS CONFIRM TEXT
|--------------------------------------------------------------------------
*/

function aws_affiliate_localize_vars() {
    return array(
    	'ConfirmApprove' => __('Are you sure that you want to approve this withdraw request?', 'awsAffiliate'),
        'ConfirmDeny' => __('Are you sure that you want to reject this withdraw request?', 'awsAffiliate')
    );
} //End localize_vars


/*
|--------------------------------------------------------------------------
| THE MAIN FUNCTION THAT TRACKS AFFILIATE CLICKS
|--------------------------------------------------------------------------
*/

function AWS_track_click() {
    global $wpdb;

    if ( isset( $_GET['type'] ) == 'affiliate' ) {
        if ( !empty($_GET['id']) ) {
            $id = isset($_GET['id']) ? intval($_GET['id']) : 0;

            // if user exists, set a cookie and redirect to homepage
            if (get_userdata($id)) {
                setcookie('ref_id', $id, time() + 60 * 60 * 24 * 7);
                wp_redirect(get_option('siteurl'));
            }
        }
    }
}


/*
|--------------------------------------------------------------------------
| ADDS A HIDDEN INPUT TO THE REGISTRATION FORM
|--------------------------------------------------------------------------
*/

function AWS_add_extra_reg_field() {
    $id = isset( $_COOKIE['ref_id'] ) ? intval( $_COOKIE['ref_id'] ) : 1;
    $user = get_userdata($id);
    $ip_address = cp_auction_users_ip();
    if( get_option('AWS_referred_by') == "yes" ) {
    echo '<p><label>'.__('Referred By:','awsAffiliate').' </label><input type="hidden" name="ip_address" value="' . $ip_address . '" /><input type="hidden" name="ref" value="' . $id . '" /><input class="text" type="text" name="" value="' . $user->user_login . '" disabled="disabled" /></p>';
    }
    else {
    echo '<input type="hidden" name="ref" value="' . $id . '" />';
    }
}


/*
|--------------------------------------------------------------------------
| ADDS THE REFERER TO THE REGISTERED USER
|--------------------------------------------------------------------------
*/

function AWS_add_ref_id($user_id) {
    update_user_meta($user_id, 'AWS_referer', filter_var($_POST['ref'], FILTER_SANITIZE_NUMBER_INT));

    //delete cookie
    setcookie("ref_id", "", time() - 3600);
}


/*
|--------------------------------------------------------------------------
| ADDS REVENUE TO THE POSTERS REFERER AFTER PUBLISHING THE POST
|--------------------------------------------------------------------------
*/

if( get_option('AWS_classipress_affiliate') == "yes" ) {
function AWS_after_publish_post($post_id) {
	$post = get_post($post_id);

	global $wpdb;

	$post_id = $post->ID;
	$author_id = $post->post_author;

	$ad_price = get_post_meta($post->ID, 'cp_sys_total_ad_cost', true);
	$percent = (int) get_option('AWS_classipress_revenue_percent');
	$revenue = (float) ($ad_price * $percent) / 100;

	$author_detail = get_userdata($author_id);
	$referer = $author_detail->AWS_referer;

	//check if second level referring is active
	if( get_option('AWS_classipress_active_two_level') == 'yes' ) {
        AWS_classipress_second_level_rev($referer, $post_id, $author_id);
	}

	//if the user has any referer, insert earning to the db
	if ($referer) {
        $table_aff = $wpdb->prefix . "aws_clp_affiliates";
        $query = "INSERT INTO {$table_aff} (referer_id, post_id, poster_id, revenue, type) VALUES (%d, %d, %d, %f, %s)"; 
    	$wpdb->query($wpdb->prepare($query, $referer, $post_id, $author_id, $revenue, 'earn'));
	}
}
}


/*
|--------------------------------------------------------------------------
| ADDS SECOND LEVEL REVENUE TO THE POSTERS REFERER AFTER PUBLISHING THE POST
|--------------------------------------------------------------------------
*/

function AWS_classipress_second_level_rev($first_ref_id, $post_id, $author_id) {
	global $wpdb;

	$ad_price = get_post_meta($post_id, 'cp_sys_total_ad_cost', true);
	$percent = (int) get_option('AWS_classipress_parent_revenue_percent');
	$revenue = (float) ($ad_price * $percent) / 100;

	$first_ref_detail = get_userdata($first_ref_id); //getting details of first referer user
	$second_ref_id = $first_ref_detail->AWS_referer; //getting referer of first referer

	//if the user has any referer, insert earning to the db
	if ($second_ref_id) {
        $table_aff = $wpdb->prefix . "aws_clp_affiliates";
        $query = "INSERT INTO {$table_aff} (referer_id, post_id, poster_id, revenue, type) VALUES (%d, %d, %d, %f, %s)"; 
    	$wpdb->query($wpdb->prepare($query, $second_ref_id, $post_id, $author_id, $revenue, 'earn'));
	}
}


/*
|--------------------------------------------------------------------------
| ADDS REVENUE TO THE PRODUCT REFERER AFTER PRODUCT SALE
|--------------------------------------------------------------------------
*/

function AWS_after_product_sale($order_id) {
	global $current_user, $wpdb;

	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$order_id'");
	$seller_id = $res->post_author;

	if( get_user_meta( $seller_id, 'AWS_cpauction_affiliate', true ) == "yes" ) {
	$item_price = $res->mc_gross;
	$percent = (int) get_user_meta( $seller_id, 'AWS_cpauction_revenue_percent', true );
	$revenue = (float) ($item_price * $percent) / 100;

	$referer = get_user_meta( $res->uid, 'AWS_referer', true );
	$seller_IP = get_user_meta( $seller_id, 'ip_address', true );
	$referer_IP = get_user_meta( $referer, 'ip_address', true );

	$table_aff = $wpdb->prefix . "aws_cpa_affiliates";
	$aff = $wpdb->get_row("SELECT * FROM {$table_aff} WHERE referer_id = '$referer' AND order_id = '$order_id'");
	if( $aff ) return;

	//check if second level referring is active
	if( get_user_meta( $seller_id, 'AWS_cpauction_active_two_level', true ) == "yes" ) {
        AWS_product_second_level_rev($referer, $order_id, $res->uid, $item_price, $seller_id);
	}

	//if the user has any referer, insert earning to the db
	if ( $referer && $referer != $seller_id && $referer_IP != $seller_IP ) {
        $table_aff = $wpdb->prefix . "aws_cpa_affiliates";
        $query = "INSERT INTO {$table_aff} (referer_id, order_id, poster_id, seller_id, revenue, type) VALUES (%d, %d, %d, %d, %f, %s)"; 
    	$wpdb->query($wpdb->prepare($query, $referer, $order_id, $res->uid, $seller_id, $revenue, 'earn'));
	}
	}
}


/*
|--------------------------------------------------------------------------
| ADDS SECOND LEVEL REVENUE TO THE PRODUCT REFERER AFTER PRODUCT SALE
|--------------------------------------------------------------------------
*/

function AWS_product_second_level_rev($first_ref_id, $order_id, $user_id, $item_price, $seller_id) {
	global $wpdb;

	$seller_IP = get_user_meta( $seller_id, 'ip_address', true );
	$referer_IP = get_user_meta( $first_ref_id, 'ip_address', true );

	$percent = (int) get_user_meta( $seller_id, 'AWS_cpauction_parent_revenue_percent', true );
	$revenue = (float) ($item_price * $percent) / 100;

	$first_ref_detail = get_userdata($first_ref_id); //getting details of first referer user
	if( isset( $first_ref_detail->AWS_referer ) )
	$second_ref_id = $first_ref_detail->AWS_referer; //getting referer of first referer
	else $second_ref_id = false;

	//if the user has any referer, insert earning to the db
	if ( $second_ref_id && $first_ref_id != $seller_id && $referer_IP != $seller_IP ) {
        $table_affiliates = $wpdb->prefix . "aws_cpa_affiliates";
        $query = "INSERT INTO {$table_affiliates} (referer_id, order_id, poster_id, seller_id, revenue, type) VALUES (%d, %d, %d, %d, %f, %s)"; 
    	$wpdb->query($wpdb->prepare($query, $second_ref_id, $order_id, $user_id, $seller_id, $revenue, 'earn'));
	}
}


/*
|--------------------------------------------------------------------------
| GET POST ID BY META KEY AND VALUE
|--------------------------------------------------------------------------
*/

function aws_affiliate_get_post_id($key, $value) {
	global $wpdb;
	$key = esc_sql( $key );
	$value = esc_sql( $value );
	$meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='".$key."' AND meta_value='".$value."'");
	if (is_array($meta) && !empty($meta) && isset($meta[0])) {
	$meta = $meta[0];
	}	
	if (is_object($meta)) {
	return $meta->post_id;
	}
	else {
	return false;
	}
}


/*
|--------------------------------------------------------------------------
| ADD EXCLUDED USERS TO THE USERS EXCLUDED OPTION
|--------------------------------------------------------------------------
*/

function AWS_users_excluded_action($input) {

	$option = get_option( 'AWS_users_excluded' );
	$list = explode(',', $option);
	if( in_array( $input, $list ) ) {
	unset($list[array_search($input, $list)]);
	$upd_list = implode(",", $list);
	$ids = trim($upd_list, ',');

		return $ids;
	}
	else {
		if( empty( $option ) ) return $input;
		else return ''.$option.','.$input.'';
	}

}


/*
|--------------------------------------------------------------------------
| GET USERS WITHDRAW REQUEST LIST
|--------------------------------------------------------------------------
*/

function AWS_classipress_get_withdraw_req_list( $user_id = '' ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_clp_affiliates";
	if ( $user_id ) {
	$result = $wpdb->get_results("SELECT id, revenue, updated FROM {$table_name} WHERE `type` = 'request' AND referer_id = '$user_id'");
	} else {
	$result = $wpdb->get_results("SELECT id, referer_id, revenue, updated FROM {$table_name} WHERE `type` = 'request'");
	}

	return $result;
}


/*
|--------------------------------------------------------------------------
| SUBMIT A WITHDRAW REQUEST FROM USER
|--------------------------------------------------------------------------
*/

function AWS_classipress_submit_withdraw_req( $user_id, $amount ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_clp_affiliates";
	$query = "INSERT INTO {$table_name} (referer_id, revenue, type) VALUES (%d, %f, %s)";
	$wpdb->query($wpdb->prepare($query, $user_id, $amount, 'request'));

}


/*
|--------------------------------------------------------------------------
| GET USERS EARNINGS
|--------------------------------------------------------------------------
*/

function AWS_classipress_get_total_earning( $user_id ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_clp_affiliates";
	$result = $wpdb->get_var("SELECT sum(revenue) FROM {$table_name} WHERE `type` = 'earn' AND `referer_id` = '$user_id'");

	return $result;
}


/*
|--------------------------------------------------------------------------
| GET USERS TOTAL WITHDRAWS AS ARRAY
|--------------------------------------------------------------------------
*/

function AWS_classipress_get_total_withdraw( $user_id ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_clp_affiliates";
	$result = $wpdb->get_var("SELECT sum(revenue) FROM {$table_name} WHERE `type` = 'withdraw' AND `referer_id` = '$user_id'");

	return $result;
}


/*
|--------------------------------------------------------------------------
| GET USERS TRANSACTION HISTORY
|--------------------------------------------------------------------------
*/

function AWS_classipress_get_transaction_history( $user_id ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_clp_affiliates";
	$result = $wpdb->get_results("SELECT id, revenue, type, updated FROM {$table_name} WHERE `type` != 'earn' AND referer_id = '$user_id' ORDER BY updated DESC");

	return $result;
}


/*
|--------------------------------------------------------------------------
| CALCULATE THE BALANCE
|--------------------------------------------------------------------------
*/

function AWS_classipress_get_total_balance( $total_earn, $total_withdraw ) {
    $balance = ($total_earn - $total_withdraw);

    return $balance;
}


/*
|--------------------------------------------------------------------------
| GET USERS WITHDRAW REQUEST LIST
|--------------------------------------------------------------------------
*/

function AWS_cpauction_get_withdraw_req_list( $user_id = '', $seller_id = '' ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_cpa_affiliates";
	if ( $user_id ) {
	$result = $wpdb->get_results("SELECT id, seller_id, revenue, updated FROM {$table_name} WHERE `type` = 'request' AND referer_id = '$user_id'");
	} else if ( $seller_id ) {
	$result = $wpdb->get_results("SELECT id, referer_id, revenue, updated FROM {$table_name} WHERE `type` = 'request' AND seller_id = '$seller_id'");
	} else {
	$result = $wpdb->get_results("SELECT id, referer_id, revenue, updated FROM {$table_name} WHERE `type` = 'request'");
	}

	return $result;
}


/*
|--------------------------------------------------------------------------
| SUBMIT A WITHDRAW REQUEST FROM USER
|--------------------------------------------------------------------------
*/

function AWS_cpauction_submit_withdraw_req( $user_id, $amount, $seller_id = '' ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_cpa_affiliates";
	if( $seller_id ) {
	$query = "INSERT INTO {$table_name} (referer_id, seller_id, revenue, type) VALUES (%d, %d, %f, %s)";
	$wpdb->query($wpdb->prepare($query, $user_id, $seller_id, $amount, 'request'));
	} else {
	$query = "INSERT INTO {$table_name} (referer_id, revenue, type) VALUES (%d, %f, %s)";
	$wpdb->query($wpdb->prepare($query, $user_id, $amount, 'request'));
	}

}


/*
|--------------------------------------------------------------------------
| GET USERS EARNINGS
|--------------------------------------------------------------------------
*/

function AWS_cpauction_get_total_earning( $user_id, $seller_id = '' ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_cpa_affiliates";
	if( $seller_id ) {
	$result = $wpdb->get_var("SELECT sum(revenue) FROM {$table_name} WHERE `type` = 'earn' AND `referer_id` = '$user_id' AND `seller_id` = '$seller_id'");
	} else {
	$result = $wpdb->get_var("SELECT sum(revenue) FROM {$table_name} WHERE `type` = 'earn' AND `referer_id` = '$user_id'");
	}

	return $result;
}


/*
|--------------------------------------------------------------------------
| GET USERS TOTAL WITHDRAWS AS ARRAY
|--------------------------------------------------------------------------
*/

function AWS_cpauction_get_total_withdraw( $user_id, $seller_id = '' ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_cpa_affiliates";
	if( $seller_id ) {
	$result = $wpdb->get_var("SELECT sum(revenue) FROM {$table_name} WHERE `type` = 'withdraw' AND `referer_id` = '$user_id' AND `seller_id` = '$seller_id'");
	} else {
	$result = $wpdb->get_var("SELECT sum(revenue) FROM {$table_name} WHERE `type`='withdraw' AND `referer_id` = '$user_id'");
	}

	return $result;
}


/*
|--------------------------------------------------------------------------
| GET USERS TRANSACTION HISTORY
|--------------------------------------------------------------------------
*/

function AWS_cpauction_get_transaction_history( $user_id ) {
	global $wpdb;

	$table_name = $wpdb->prefix . "aws_cpa_affiliates";
	$result = $wpdb->get_results("SELECT id, seller_id, revenue, type, updated FROM {$table_name} WHERE `type` != 'earn' AND referer_id = '$user_id' ORDER BY updated DESC");

	return $result;
}


/*
|--------------------------------------------------------------------------
| CALCULATE THE BALANCE
|--------------------------------------------------------------------------
*/

function AWS_cpauction_get_total_balance( $total_earn, $total_withdraw ) {
    $balance = ($total_earn - $total_withdraw);

    return $balance;
}