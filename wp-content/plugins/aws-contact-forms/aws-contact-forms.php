<?php
/*
Plugin Name: AWS Contact Forms
Plugin URI: http://www.arctic-websolutions.com/
Version: 1.0.1
Description: The very best and most convenient way to publish contact forms to your WordPress install.
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


define( 'AWS_CF_VERSION', '1.0.1' );
define( 'CUSTOM_EMAIL_SEP', '[^-*-^]' );
define( 'AWS_CF_PLUGIN_URI', plugin_dir_url( __FILE__ ) );
define( 'AWS_CF_PLUGIN_INCL_URI', AWS_CF_PLUGIN_URI . '/includes' );
define( 'AWS_CF_PLUGIN_CSS', AWS_CF_PLUGIN_URI . '/styles' );
define( 'AWS_CF_PLUGIN_JS', AWS_CF_PLUGIN_URI . '/js' );

require_once ( dirname( __FILE__ ) . '/widget.php' );
require_once ( dirname( __FILE__ ) . '/shortcodes.php' );
require_once ( dirname( __FILE__ ) . '/admin/settings.php' );

// Plugin localization
function aws_cf_load_language() {
	load_plugin_textdomain( 'AwsCF', false, AWS_CF_PLUGIN_URI . '/languages/' );
}

// Add Contact Forms settings menu item
function aws_cf_admin_menu() {

	$page = add_menu_page( __( 'AWS Contact Forms Management', 'AwsCF' ), __( 'AWS Contact', 'AwsCF' ), 'manage_options', 'aws-contact-settings', 'aws_contact_settings' );

	add_action( 'admin_print_styles-'.$page, 'aws_cf_admin_styles' );

}
add_action( 'admin_menu', 'aws_cf_admin_menu' );

// Add plugin Settings link
$plugin = plugin_basename( __FILE__ );

function aws_cf_settings_link( $links ) {

	$x = str_replace( basename( __FILE__ ),"", plugin_basename( __FILE__ ) );
	$settings_link = '<a href="/wp-admin/admin.php?page=aws-contact-settings">' . __( 'Settings', 'AwsCF' ) . '</a>';
	array_unshift( $links, $settings_link );

	return $links;

}
add_filter( 'plugin_action_links_' . $plugin, 'aws_cf_settings_link' );

// Enqueue the admin css files
function aws_cf_admin_styles() {

	wp_enqueue_style( 'aws-cf-admin', plugins_url( '/admin/styles/style.css', __FILE__ ), array(), AWS_CF_VERSION );

}

// Enqueue plugin scripts
function aws_cf_add_script() {

	wp_register_script( 'jquery-validator', AWS_CF_PLUGIN_JS .'/jquery.tools.min.js', array( 'jquery' ), '1.2.5', true );
	wp_enqueue_script( 'jquery-validator' );

	wp_register_script( 'aws-cf-scripts', AWS_CF_PLUGIN_JS .'/contact-form.js', array( 'jquery', 'jquery-validator' ), AWS_CF_VERSION, true );
	wp_localize_script( 'aws-cf-scripts', 'aws_cf_lang', aws_cf_localize_vars() );
	wp_enqueue_script( 'aws-cf-scripts' );

	// Make the ajaxurl var available to the above script
	wp_localize_script( 'aws-cf-scripts', 'AwsCF', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

}

// Enqueue plugin styles
function aws_cf_add_style()  {
	wp_enqueue_style( 'aws-cf-style', AWS_CF_PLUGIN_CSS . '/style.css', false, AWS_CF_VERSION, "screen" );
}

// Add thickbox script for the popup
add_action('template_redirect', 'aws_cf_add_thickbox');

function aws_cf_add_thickbox() {

	$array = get_option( 'aws_cf_page_array' );
	$page_array = explode(',', $array);
	if ( is_page( $page_array ) ) {
		add_thickbox(); 
	}

}

// Localize vars contact-form.js
function aws_cf_localize_vars() {

return array(
        'PleaseWait' => __( 'Please wait..', 'AwsCF' ),
        'SendMessage' => __( 'Send Message', 'AwsCF' )
    );

} // End localize_vars

// Plugin actions
add_action( 'widgets_init', create_function( '', 'register_widget( "AWS_CF_Widget" );' ) );

add_action( 'plugins_loaded', 'aws_cf_load_language' );
add_action( 'wp_print_scripts', 'aws_cf_add_script' );
add_action( 'wp_print_styles', 'aws_cf_add_style' );

// Standard and popup shortcodes
add_shortcode('aws-contact-form', 'aws_cf_shortcode_handler');
add_shortcode('aws-popup-contact-form', 'aws_cf_popup_shortcode_handler');

// Setup template redirect for the send email processing function
add_action( 'template_redirect', 'aws_cf_template_redirect', 1 );

function aws_cf_template_redirect() {

	if(isset($_GET['process_mail']))
	{
		header("HTTP/1.1 200 OK");
		include(ABSPATH . 'wp-content/plugins/aws-contact-forms/includes/wp-mailer.php');
		exit;
	}

}

?>