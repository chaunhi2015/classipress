<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


function aws_contact_settings() {
?>

<div class="wrap">

<h2><?php _e( 'AWS Contact Forms Management', 'AwsCF' ); ?></h2>

<?php if( isset( $_POST['action'] ) && $_POST['action'] == 'update' && $_POST['option_page'] == 'aws-contact-settings' ) {

	$options['html_or_text_emails'] = isset( $_POST['html_or_text_emails'] ) ? esc_attr( $_POST['html_or_text_emails'] ) : '';
	$options['aws_cf_page_array'] = isset( $_POST['aws_cf_page_array'] ) ? esc_attr( $_POST['aws_cf_page_array'] ) : '';
	$options['aws_cf_button_align'] = isset( $_POST['aws_cf_button_align'] ) ? esc_attr( $_POST['aws_cf_button_align'] ) : '';
	$options['aws_cf_button_text'] = isset( $_POST['aws_cf_button_text'] ) ? esc_attr( $_POST['aws_cf_button_text'] ) : '';
	$options['aws_cf_button_width'] = isset( $_POST['aws_cf_button_width'] ) ? esc_attr( $_POST['aws_cf_button_width'] ) : '';
	$options['aws_cf_popup_title'] = isset( $_POST['aws_cf_popup_title'] ) ? esc_attr( $_POST['aws_cf_popup_title'] ) : '';

	// Add values of $options to options table
	foreach ( $options as $key => $value ) { // Cycle through the $options array!
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		update_option( $key, $value );
		if ( empty( $value ) ) delete_option( $key ); // Delete if empty
	}

	echo '<div class="updated"><p><strong>' . __( 'Settings was updated..','AwsCF' ) . '</strong></p></div>';
}
?>

<div class="metabox-holder">

        <div class="postbox">

                <div class="inside">

<form method="post" action="">

	<?php settings_fields( 'aws-contact-settings'); ?>

<table class="form-table intro-text">
    <tbody>

        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( 'Once installed and activated you will find a Contact Form Widget under Appearance -> Widgets. The widget can be used in any available sidebar. To add a contact form in any page or post, just copy and paste the shortcode <strong>[aws-contact-form]</strong> into the html editor (content of your post or page).', 'AwsCF' ); ?></td>
        </tr>

        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( 'You can also use the contact form in any page or post as a <strong>popup</strong>, just copy and paste the shortcode <strong>[aws-popup-contact-form]</strong> into the html editor (content of your post or page), and enter the page/post ID of the actual page/post into the <strong>PopUp on Page ID</strong> text field below. You will then get a Contact Us button attached to the actual post/page(s)', 'AwsCF' ); ?></td>
        </tr>

    </tbody>
</table>

<div class="clear30"></div>

<div class="dotted"><h2><?php _e( 'Settings', 'AwsCF' ); ?></h2></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e( 'HTML or TEXT Emails:', 'AwsCF' ); ?></th>
	<td><select name="html_or_text_emails" id="html_or_text_emails" style="min-width: 100px;">
	<option value="text/html" <?php if( get_option( 'html_or_text_emails' ) == "text/html" ) echo 'selected="selected"'; ?>><?php _e( 'HTML', 'AwsCF' ); ?></option>
	<option value="text/plain" <?php if( get_option( 'html_or_text_emails' ) == "text/plain" ) echo 'selected="selected"'; ?>><?php _e( 'TEXT', 'AwsCF' ); ?></option>
	</select> <?php _e( 'Enable TEXT emails if you are using WP Better Emails Plugin.', 'AwsCF' ); ?></td>
	</tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'PopUp Title:', 'AwsCF' ); ?></th>
        <td><input type="text" name="aws_cf_popup_title" value="<?php echo get_option( 'aws_cf_popup_title' ); ?>" size="20"/> <?php _e( 'Enter a title for the popup box, ie. Contact Us.', 'AwsCF' ); ?></td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'PopUp Button Position:', 'AwsCF' ); ?></th>
	<td><select name="aws_cf_button_align" id="aws_cf_button_align" style="min-width: 100px;">
	<option value="left" <?php if( get_option( 'aws_cf_button_align' ) == "left" ) echo 'selected="selected"'; ?>><?php _e( 'Left', 'AwsCF' ); ?></option>
	<option value="center" <?php if( get_option( 'aws_cf_button_align' ) == "center" ) echo 'selected="selected"'; ?>><?php _e( 'Center', 'AwsCF' ); ?></option>
	<option value="right" <?php if( get_option( 'aws_cf_button_align' ) == "right" ) echo 'selected="selected"'; ?>><?php _e( 'Right', 'AwsCF' ); ?></option>
	</select> <?php _e( 'Select a floating position for the popup button.', 'AwsCF' ); ?></td>
	</tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'PopUp Button Text:', 'AwsCF' ); ?></th>
        <td><input type="text" name="aws_cf_button_text" value="<?php echo get_option( 'aws_cf_button_text' ); ?>" size="20"/> <?php _e( 'Enter a text/title for the popup button, ie. Contact Us.', 'AwsCF' ); ?></td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'PopUp Button Width:', 'AwsCF' ); ?></th>
        <td><input type="text" name="aws_cf_button_width" value="<?php echo get_option( 'aws_cf_button_width' ); ?>" size="20"/> <?php _e( 'Enter the preferred with of the button, ie. 90', 'AwsCF' ); ?></td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'PopUp on Page ID:', 'AwsCF' ); ?></th>
        <td><input type="text" name="aws_cf_page_array" value="<?php echo get_option( 'aws_cf_page_array' ); ?>" size="20"/> <?php _e( 'Enter all page or post IDs where you have added the popup shortcode.', 'AwsCF' ); ?><br /><small><?php _e( '<strong>NOTE:</strong> If more than one page/post is used enter IDs as a comma separated list, ie. 10,20,30', 'AwsCF' ); ?></small></td>
        </tr>

    </tbody>
</table>

<table class="form-table intro-text">
    <tbody>

        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( '<strong>Here is how you can find the Page / Post IDs</strong> - After you have logged into your WordPress dashboard, Go to Manage > Pages or Posts and from the list of Pages / Posts hover over the Page / Post title you want to find the ID of. Every time you hover over, your browsers status bar will show you a URL ending with a number like this http://www.your-domain.com/wp-admin/post.php?post=3648&action=edit. The number with which is in the URL is the Page / Post ID (3648 in this case).', 'AwsCF' ); ?></td>
        </tr>

	<tr valign="top">
        <td colspan="2" class="introtext"><?php _e( 'If you dont see anything when you hover over, click on the Page link to open edit Page screen. Now, look in your browsers address bar and you will find the URL with a number. and that number is your Page / Post ID.', 'AwsCF' ); ?></td>
        </tr>

    </tbody>
</table>

   <div align="center"><?php submit_button(); ?></div>

</form>

                </div>
        </div>
</div>

</div>
<?php }