<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


require_once( ABSPATH . '/wp-load.php' );

	$siteurl =  get_bloginfo( 'wpurl');
	$html_text = get_option( 'html_or_text_emails' );
	
	$to = isset( $_POST['to'] ) ? trim( $_POST['to'] ): '';
	$name = isset( $_POST['name'] ) ? trim( $_POST['name'] ) : '';
	$email = isset( $_POST['email'] ) ? trim( $_POST['email'] ) : '';
	$subject = isset( $_POST['subject'] ) ? trim( $_POST['subject'] ) : '';
	
	$to = base64_decode( $to );
	$to = str_replace( CUSTOM_EMAIL_SEP, '@', $to );
	
	$content = isset( $_POST['content'] ) ? trim( $_POST['content'] ) : '';
	
	$error = false;
	if( $to === '' || $email === '' || $content === '' || $name === '' || $subject === '' ) {
		$error = true;
	}
	if( !preg_match( '/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/', $email ) ) {
		$error = true;
	}
	if( !preg_match( '/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/', $to ) ) {
		$error = true;
	}
	
	if( $error == false ) {

		$headers   = array();
		$headers[] = 'MIME-Version: 1.0' . "\r\n";
		$headers[] = 'Content-type: '.$html_text.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
		$headers[] = 'From: '.get_bloginfo( 'name' ).'<'.get_bloginfo( 'admin_email' ).'>' . "\r\n";
		$headers[] = 'Reply-To: '.$name.' <'.$email.'>';

		$body  = '' . __( 'From:', 'AwsCF' ) . ' '.$name.'<br/>';
		$body .= '' . __( 'Email:', 'AwsCF' ) . ' <a style="text-decoration: none;" href="mailto:'.$email.'">'.$email.'</a><br/>';
		$body .= '' . __( 'Subject:', 'AwsCF' ) . ' '.$subject.'<br/><br/>';
		if ( $html_text == "text/html" ) {
		$body .= '' . __( '<strong>Message</strong>:', 'AwsCF' ) . '<br/>'.nl2br( stripslashes( $content ) ).'<br/></br><br/>';
		} else {
		$body .= '' . __( '<strong>Message</strong>:', 'AwsCF' ) . '<br/>'.$content.'<br/></br><br/>';
		}
		$body .= '' . sprintf( __( 'This mail was sent via contact form on %s. To reply to this message, just click the reply button in your mail program.', 'AwsCF' ), get_bloginfo( 'name' ) ) . '<br/><br/>';
		$body .= '' . __( 'Thanks,', 'AwsCF' ) . '<br/><strong>' . get_bloginfo( 'name' ) . '</strong><br/><a style="text-decoration: none;" href="' . get_bloginfo( 'wpurl' ) . '">' . get_bloginfo( 'wpurl' ) . '</a><br/><br/>';

		if( wp_mail( $to, $subject, $body, $headers ) ) {
			echo 'success';
		}
	}
?>