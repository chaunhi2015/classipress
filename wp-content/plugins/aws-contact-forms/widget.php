<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


class AWS_CF_Widget extends WP_Widget {

	function __construct() {
        	$widget_ops = array( false, 'description' => __( 'Add a contact form to your sidebars as a widget.', 'AwsCF' ) );
        	parent::__construct( false, 'Contact Form Widget', $widget_ops );

		if ( is_active_widget( false, false, $this->id_base ) ) {
			add_action( 'wp_print_scripts', array(&$this, 'add_script') );
			add_action( 'wp_print_styles', array(&$this, 'add_style') );
		}

	}

	function add_script() {

		wp_register_script( 'jquery-validator', AWS_CF_PLUGIN_JS .'/jquery.tools.min.js', array( 'jquery' ), '1.2.5', true );
		wp_enqueue_script( 'jquery-validator' );

		wp_register_script( 'aws-cf-scripts', AWS_CF_PLUGIN_JS .'/contact-form.js', array( 'jquery', 'jquery-validator' ), AWS_CF_VERSION, true );
		wp_enqueue_script( 'aws-cf-scripts' );
	}

	function add_style() {
		wp_enqueue_style( 'aws-cf-style', AWS_CF_PLUGIN_CSS . '/style.css', false, AWS_CF_VERSION, "screen" );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Contact Us', 'AwsCF' ) : $instance['title'], $instance, $this->id_base );
		$email = $instance['email'];
		$email = str_replace( '@', CUSTOM_EMAIL_SEP, $email );
		$email = base64_encode( $email );

		if( empty( $success ) ){
			$success = '<font color="green">'.__( 'Your message was successfully sent.<br /><strong>Thank You!</strong>', 'AwsCF' ).'</font>';
		}

		echo $before_widget;
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		$id = md5(time().' '.rand()); 
?>

<p style="display:none;"><font color="green"><?php _e( 'Your message was successfully sent.<br /><strong>Thank You!</strong>', 'AwsCF' );?></font></p>
<form class="contactform" action="<?php echo get_bloginfo( 'wpurl' ); ?>/?process_mail=1" method="post" novalidate="novalidate">
<p><input type="text" required="required" id="contact_<?php echo $id; ?>_name" name="contact_<?php echo $id; ?>_name" class="text_input" value="" size="33" tabindex="20" />
<label for="contact_<?php echo $id; ?>_name"><?php _e( 'Name', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><input type="email" required="required" id="contact_<?php echo $id; ?>_email" name="contact_<?php echo $id;?>_email" class="text_input" value="" size="33" tabindex="21" />
<label for="contact_<?php echo $id; ?>_email"><?php _e( 'Email', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><input type="text" required="required" id="contact_<?php echo $id; ?>_subject" name="contact_<?php echo $id;?>_subject" class="text_input" value="" size="33" tabindex="22" />
<label for="contact_<?php echo $id; ?>_subject"><?php _e( 'Subject', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><textarea required="required" name="contact_<?php echo $id;?>_content" class="textarea" cols="33" rows="5" tabindex="23"></textarea></p>

<div style="clear:both"></div>

<p><input id="btn_<?php echo $id;?>" type="submit" value="<?php _e( 'Send Message', 'AwsCF' ); ?>" /></p>
<input type="hidden" value="<?php echo $id; ?>" name="unique_widget_id"/>
<input type="hidden" value="<?php echo $email; ?>" name="contact_<?php echo $id; ?>_to"/>
</form>	

<?php
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {

		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['email'] = strip_tags( $new_instance['email'] );

		return $instance;
	}

	function form( $instance ) {

		$title = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$email = isset($instance['email']) ? esc_attr($instance['email']) : get_option('admin_email');
?>

	<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:', 'AwsCF' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

	<p><label for="<?php echo $this->get_field_id('email'); ?>"><?php _e( 'Your Email:', 'AwsCF' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" /></p>

<?php
	}

}