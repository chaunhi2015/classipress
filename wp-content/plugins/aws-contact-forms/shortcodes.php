<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


function aws_cf_shortcode_handler( $atts, $content = null ) {

	ob_start();

	extract( shortcode_atts( array(
		'email' => get_option( 'admin_email' )
	), $atts ) );

	if ( !isset( $email ) || $email == "" ) {
		$email = get_option( 'admin_email' );
	}

	$content = trim( $content );
	if( !empty( $content ) ) {
		$success = do_shortcode( $content );
	}

	if( empty( $success ) ) {
		$success = '<font color="green">'.__( 'Your message was successfully sent. <strong>Thank You!</strong>', 'AwsCF' ).'</font>';
	}

	$id = md5(time().' '.rand()); 
	$email = str_replace( '@', CUSTOM_EMAIL_SEP, $email );
	$email = base64_encode( $email );

?>

<p style="display:none;"><?php echo $success; ?></p>
<form class="contactform" action="<?php echo get_bloginfo( 'wpurl' ); ?>/?process_mail=1" method="post" novalidate="novalidate">
<p><input type="text" required="required" id="contact_<?php echo $id; ?>_name" name="contact_<?php echo $id; ?>_name" class="text_input" value="" size="33" tabindex="20" />
<label for="contact_<?php echo $id; ?>_name"><?php _e( 'Name', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><input type="email" required="required" id="contact_<?php echo $id; ?>_email" name="contact_<?php echo $id; ?>_email" class="text_input" value="" size="33" tabindex="21" />
<label for="contact_<?php echo $id; ?>_email"><?php _e( 'Email', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><input type="text" required="required" id="contact_<?php echo $id; ?>_subject" name="contact_<?php echo $id;?>_subject" class="text_input" value="" size="33" tabindex="22" />
<label for="contact_<?php echo $id; ?>_subject"><?php _e( 'Subject', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><textarea style="width: 98.5% !important;" required="required" name="contact_<?php echo $id; ?>_content" class="textarea" cols="33" rows="5" tabindex="23"></textarea></p>

<div style="clear:both"></div>

<p><input id="btn_<?php echo $id; ?>" type="submit" value="<?php _e( 'Send Message', 'AwsCF' ); ?>" /></p>
<input type="hidden" value="<?php echo $id; ?>" name="unique_widget_id"/>
<input type="hidden" value="<?php echo $email; ?>" name="contact_<?php echo $id; ?>_to"/>
</form>

<?php
	return ob_get_clean();
}


function aws_cf_popup_shortcode_handler( $atts, $content = null ) {

	ob_start();
?>

	<div id="AWSCFPopup" style="display:none">

<?php

	extract( shortcode_atts( array(
		'email' => get_option( 'admin_email' )
	), $atts ) );

	if ( !isset( $email ) || $email == "" ) {
		$email = get_option( 'admin_email' );
	}

	$content = trim( $content );
	if( !empty( $content ) ) {
		$success = do_shortcode( $content );
	}

	if( empty( $success ) ) {
		$success = '<br /><font color="green">'.__( 'Your message was successfully sent. <strong>Thank You!</strong>', 'AwsCF' ).'</font>';
	}

	$id = md5(time().' '.rand()); 
	$email = str_replace( '@', CUSTOM_EMAIL_SEP, $email );
	$email = base64_encode( $email );

	$align = get_option( 'aws_cf_button_align' );
	if ( empty( $align ) ) $align = "left";

	$width = get_option( 'aws_cf_button_width' );
	if ( empty( $width ) ) $width = "80";

	$text = get_option( 'aws_cf_button_text' );
	if ( empty( $text ) ) $text = __( 'Contact Us', 'AwsCF' );

	$ptitle = get_option( 'aws_cf_popup_title' );
	if ( empty( $ptitle ) ) $ptitle = __( 'Contact Us', 'AwsCF' );
?>

<style>
.AswSubmit {
	text-align: <?php echo $align; ?>;
}
.AswSubmit input {
	width: <?php echo $width; ?>px;
}
</style>

<p style="display:none;"><?php echo $success; ?></p>
<form class="contactform" action="<?php echo get_bloginfo( 'wpurl' ); ?>/?process_mail=1" method="post" novalidate="novalidate">
<p><input type="text" required="required" id="contact_<?php echo $id; ?>_name" name="contact_<?php echo $id; ?>_name" class="text_input" value="" size="33" tabindex="20" />
<label for="contact_<?php echo $id; ?>_name"><?php _e( 'Name', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><input type="email" required="required" id="contact_<?php echo $id; ?>_email" name="contact_<?php echo $id; ?>_email" class="text_input" value="" size="33" tabindex="21" />
<label for="contact_<?php echo $id; ?>_email"><?php _e( 'Email', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><input type="text" required="required" id="contact_<?php echo $id; ?>_subject" name="contact_<?php echo $id;?>_subject" class="text_input" value="" size="33" tabindex="22" />
<label for="contact_<?php echo $id; ?>_subject"><?php _e( 'Subject', 'AwsCF' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

<p><textarea required="required" name="contact_<?php echo $id; ?>_content" class="textarea" cols="33" rows="5" tabindex="23"></textarea></p>

<div style="clear:both"></div>

<p><input id="btn_<?php echo $id; ?>" type="submit" value="<?php _e( 'Send Message', 'AwsCF' ); ?>" /></p>
<input type="hidden" value="<?php echo $id; ?>" name="unique_widget_id"/>
<input type="hidden" value="<?php echo $email; ?>" name="contact_<?php echo $id; ?>_to"/>
</form>

	</div>

	<div class="AswSubmit">
	<input alt="#TB_inline?height=250&amp;width=400&amp;inlineId=AWSCFPopup" title="<?php echo $ptitle; ?>" class="thickbox button obtn btn_orange btn" value="<?php echo $text; ?>" />
	</div>

<?php
	return ob_get_clean();
}
?>