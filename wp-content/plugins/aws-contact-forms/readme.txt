=== AWS Contact Forms ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: contact, contact forms, ajax contact, widget, widget contact form, email contact, shortcode, shortcode contact form
Requires at least: 3.9.2
Tested up to: 4.3.0
Stable tag: 1.0.1
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

---------------------------------------------------------------------------------------------------

=== General Information ===

Once installed and activated you will find a Contact Form Widget under Appearance -> Widgets. The widget can be used in any available sidebar. You can also use the contact form in any page or post, just copy and paste the shortcode [aws-contact-form] into the html editor (content of your post or page).

You can also use the contact form in any page or post as a popup, just copy and paste the shortcode [aws-popup-contact-form] into the html editor (content of your post or page), and enter the page/post ID of the actual page/post into the PopUp on Page ID text field below. You will then get a Contact Us button attached to the actual post/page(s)

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://project.wp-build.com) - Testdrive every aspect of the plugin.
* [Manuals & Support](http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Some Admin Features ===

* Add contactform widget to any available sidebar.
* Add standard shortcode to get a standard contactform in pages or posts.
* Add the popup shortcode to get a popup contact form in pages or posts.
* Set HTML or Text emails.
* Set some css and float for the popup contact us button.

=== Installation ===

* Download AWS Contact Forms.
* Extract the downloaded .zip file. (Not necessary if filename dosen’t say so, ie. UNZIP-first)
* Login to your websites FTP and navigate to the “wp-contents/plugins/” folder.
* Upload the resulting “aws-contact-forms.zip” or “aws-contact-forms-X.X.X.zip” folder to the websites plugin folder.
* Go to your websites Dashboard, access the “plugins” section on the left hand menu.
* Locate “AWS Contact Forms” in the plugin list and click “activate”.
* Make sure to backup your translations before any upgrade.

Visit your AWS Contact -> Settings, configure any options as desired. That’s it!

=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.