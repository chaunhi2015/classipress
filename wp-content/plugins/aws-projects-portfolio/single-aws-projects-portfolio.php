<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	global $post, $current_user;
	get_currentuserinfo();

$container_width = 'width: '.get_option( 'aws_single_width_css' ).'px;';
$image_width = get_option('aws_single_image_width');
if ( empty( $image_width ) ) $image_width = "414";
$image_height = get_option('aws_single_image_height');
if ( empty( $image_height ) ) $image_height = "420";
$custom_css = get_option( 'aws_single_custom_css' );
$execution = get_option( 'aws_single_execution_text' );
if ( empty( $execution ) ) $execution = __( 'Project Execution', 'AwsPortfolio' );
$problem = get_option('aws_single_problem_text');
if ( empty( $problem ) ) $problem = __( 'Problem', 'AwsPortfolio' );
$solution = get_option('aws_single_solution_text');
if ( empty( $solution ) ) $solution = __( 'Solutions', 'AwsPortfolio' );
$testimonial = get_option('aws_single_testimonial_text');
if ( empty( $testimonial ) ) $testimonial = __( 'Client Testimonial', 'AwsPortfolio' );
$edit_url = false;

get_header();

if ( get_option( 'aws_single_width_css' ) || $custom_css || $image_width || $image_height ) {

	$inst = false;
	$args1 = array( '', '<style type="text/css">', '#project-image {', 'width: '.$image_width.'px;', 'height: '.$image_height.'px;', '}', $custom_css );
	$args2 = array();
	if ( get_option( 'aws_single_width_css' ) ) {
	$args2 = array( '.main-container {', $container_width, '}' );
	}
	$args3 = array( '</style>', '' );
	$args = array_merge( $args1, $args2, $args3 );
	foreach( $args as $arg ) {
	$inst .= $arg . "\r\n";
	}
	$css_custom = $inst;

	echo $css_custom;

}
?>

<?php echo stripslashes( html_entity_decode( get_option( 'aws_single_container_div' ) ) ); ?>

<!--/ main container -->

	<div class="main-container">

<?php if ( $post->post_author == $current_user->ID ) {
	$edit_url = ' (<a href="'.awsprojects_get_page_url_by_shortcode( '[aws-projects-registration]' ).'?pid='.$post->ID.'">'.__( 'Edit Project', 'AwsPortfolio' ).'</a>)';
} ?>

		<h1 class="entry-title"><?php _e( 'Project Details', 'AwsPortfolio' ); ?><?php echo $edit_url; ?></h1>

			<div class="project-detail">

<?php

	if ( have_posts() ) : while ( have_posts() ) : the_post();

		global $post;

		$description = '';
		$description = get_the_content();
		$description = apply_filters( 'the_content', $description );
		$type = get_post_meta( $post->ID, "_awsprojects_portfolio_cat", true );
		$portfolio_type = get_term_by( 'slug', $type, 'portfolio_type' );
		if ( isset( $portfolio_type->name ) ) {
			$type = $portfolio_type->name;
		} else {
			$type = "";
		}
		$datecreate =  get_post_meta( $post->ID, "_createdate", true );
		$client = get_post_meta( $post->ID, "_clientname", true );
		$technical_details = get_post_meta( $post->ID, "_technical_details", true );
		$siteurl = get_post_meta( $post->ID, "_siteurl", true );
		$project_imgurl = get_post_meta( $post->ID, "_imageurl", true );
		$project_problem = get_post_meta( $post->ID, "_projectproblem", true );
		$project_solution = get_post_meta( $post->ID, "_projectsolution", true );
		$password = get_post_meta( $post->ID, '_wpp_passcode', true );
		$asw_status = get_post_meta( $post->ID, '_wpp_crstatus', true ); 
		$cl_photo = get_post_meta( $post->ID, '_clientphoto', true ); 
		$client_test = get_post_meta( $post->ID, '_clientrecommendation', true );
		$post_id = $post->ID;
?>

<div class="detail-left">

<div id="AWSContactPopup" style="display:none">

	<?php
	$email = get_post_meta( $post->ID, 'aws_project_author_email', true );
	$email = str_replace( '@', CUSTOM_EMAIL_SEPS, $email );
	$email = base64_encode( $email );
	$id = md5(time().' '.rand());
	?>
		
	<p style="display:none;"><br /><?php _e( 'Your message was successfully sent.<br /><strong>Thank You!</strong>', 'AwsPortfolio' ); ?></p>
	<form class="contactform" action="<?php echo get_bloginfo('wpurl'); ?>/?process_mail=1" method="post" novalidate="novalidate">
	<p><input type="text" required="required" id="contact_<?php echo $id;?>_name" name="contact_<?php echo $id; ?>_name" class="text_input" value="" size="33" tabindex="20"/>
	<label for="contact_<?php echo $id; ?>_name"><?php _e( 'Name', 'AwsPortfolio' ); ?>&nbsp;<span style="color: red;">*</span></label></p>
			
	<p><input type="email" required="required" id="contact_<?php echo $id; ?>_email" name="contact_<?php echo $id;?>_email" class="text_input" value="" size="33" tabindex="21"/>
	<label for="contact_<?php echo $id; ?>_email"><?php _e( 'Email', 'AwsPortfolio' ); ?>&nbsp;<span style="color: red;">*</span></label></p>
			
	<p><textarea required="required" name="contact_<?php echo $id; ?>_content" class="textarea" cols="33" rows="5" tabindex="22"></textarea></p>
			
	<p><input id="btn_<?php echo $id; ?>" type="submit" value="<?php _e( 'Send Message', 'AwsPortfolio' ); ?>" tabindex="23" /></p>
	<input type="hidden" value="<?php echo $id; ?>" name="unique_widget_id"/>
	<input type="hidden" value="<?php echo $email; ?>" name="contact_<?php echo $id; ?>_to"/>
	</form>

</div>

		<?php							
		if ( ( !empty( $project_imgurl ) ) ) {
		$original_image = str_replace( '-235x150.jpg', '.jpg', $project_imgurl );
	
		$imageurl = '<a title="'.$post->post_title.'" class="thickbox" href="'.$original_image.'" target="_blank"><img id="project-image" src="'.$original_image.'" border="0" alt="" width="'.$image_width.'" height="'.$image_height.'"></a>';
		} else {
		
		$imageurl = '<img id="project-image" src="' .plugins_url( 'images/no-image.jpg' , __FILE__ ). '" width="'.$image_width.'" height="'.$image_height.'"/> ';
		}
		?>
		<?php echo $imageurl; ?>
		
		<?php if ( !empty( $siteurl ) ) { ?>
		<a href="<?php echo $siteurl; ?>" target="_blank"><?php echo $siteurl; ?></a>
		<?php } ?>

</div><!--/ detail-left -->

<div class="detail-right">

	<h2 class="border"><?php echo $post->post_title; ?></h2>

<?php
	if ( !empty( $description ) ) {

	$description_value = $description;

	} else {

	$description_value = '......';

}
?>

	<?php echo $description_value; ?>

	<?php if ( !empty( $siteurl ) ) { ?>
	<div class="detail-submit">
	<a class="button obtn btn_orange btn" href="<?php echo $siteurl; ?>" target="_blank"><?php _e( 'View the Project', 'AwsPortfolio' ); ?></a>
	<?php if ( get_post_meta( $post->ID, 'aws_project_author_email', true ) ) { ?>
	<input alt="#TB_inline?height=250&amp;width=400&amp;inlineId=AWSContactPopup" title="<?php _e( 'Send Me a Message', 'AwsPortfolio' ); ?>" class="thickbox button obtn btn_orange btn" value="<?php _e( 'Contact Me', 'AwsPortfolio' ); ?>" />
	<?php } ?>
	<div class="clear"></div>
	</div><!--/ detail-submit -->
	<?php } ?>

	<div class="clear30"></div>

	<?php if ( get_post_meta( $post->ID, '_itemid', true ) ) { ?>
	<div class="dotted"></div>
	<?php echo do_shortcode('[buy-now-button pid="'.get_post_meta( $post->ID, '_itemid', true ).'"]'); ?>

	<?php } ?>

</div><!--/ detail-right -->
                    
	<div class="clear"></div>

<div class="project-prob">

<?php if ( !empty( $project_problem ) || !empty( $project_solution ) ) : ?>
	<h3><?php echo $execution; ?></h3>
	<?php endif; ?>

	<div class="prob">

<ul>

<?php if ( !empty( $project_problem ) ) { ?>

<li>
	<div class="prob-left">
	<img id="prob-image" src="<?=plugins_url( 'images/problem.jpg' , __FILE__ );?>" width="207" height="26" alt="">
	<p id="prob-left-text"><?php echo $problem; ?></p>
	</div><!--/ prob-left -->
	<div class="prob-right">
	<p class="tm-alright">
	<?php echo $project_problem; ?>
	</p>
	</div><!--/ prob-right -->
</li>

	<div class="clear"></div>

	<?php } ?>

<?php if ( !empty( $project_solution ) ) { ?>

<li>
	<div class="solution-left">
	<img id="sol-image" src="<?=plugins_url( 'images/solution.jpg' , __FILE__ );?>" width="207" height="26" alt="">
	<p id="sol-left-text"><?php echo $solution; ?></p>
	</div><!--/ prob-left -->
	<div class="solution-right">
	<p class="tm-alright">
	<?php echo nl2br( $project_solution ); ?>
	</p>
	</div><!--/ solution-right -->
</li>

	<div class="clear"></div>

<?php } if( !empty( $client_test ) ) {

	if( $asw_status == "on" ):
?>

<!--/ Client Testimonal -->

<li>
	<div class="client-testimonal">
	<div class="test-left">
	<img id="test-image" src="<?=plugins_url( 'images/client-testimonial.jpg' , __FILE__ );?>" width="207" height="26" alt="">
	<p id="test-left-text"><?php echo $testimonial; ?></p>
	</div><!--/ prob-left -->

	<div class="clear"></div>

	<div class="client-container">
	<div class="client-img">
	<img id="client-image" src="<?php  if( !empty( $cl_photo ) ): echo $cl_photo; else: echo plugins_url( 'images/aws-avatar.png' , __FILE__ ); endif;?>" width="124" height="123" alt="">
	</div><!--/ client-img -->
	<div class="client-cont">
	<p class="test-alright">
	<i><?php echo $client_test; ?></i>
	<span><strong><?php if ( !empty( $client ) ) { echo $client; } ?></strong><br><?php if ( !empty( $technical_details ) ) { echo "  ".$technical_details; } ?></span>
	</p>
	</div>

	</div>
	</div>

	<div class="clear"></div>

</li>

<!--/ end Client Testimonal -->

<?php endif;

} else {

if( !empty( $password ) ) { ?>

<!--/ testimonal -->

<div class="testimonal">
	<a href="#clt-testimonial" id="rlickbutton" onclick="showpasscode( 'crFormbox', this )" class="hide-button"><?php _e( 'Submit your Testimonial', 'AwsPortfolio' ); ?></a>
</div>

<div class="clear"></div>

<!--/ end testimonal -->

<?php } } ?>

<!--/ passcode -->

<li>
	<div id="crFormbox" class="passcode">
	<form id="crForm" method="post" name="crForm">
	<div align="center" class="pc-label" id="pc-label"></div>
	<div id="crForm-inner" class="dp_contact_name-passcode">
	<?php $cr_nonce = wp_create_nonce( 'crvalidate_nonce' ); ?>
	<strong>&nbsp;<?php _e( 'Your Password:', 'AwsPortfolio' ); ?></strong> <input id="wppassc" name="wppassc" class="input" value= "" type="password" size="15"/>
	<input name="wpppid" type="hidden" value="<?php echo $post_id; ?>" />
	<input id="crnonce" type="hidden" value="<?php echo $cr_nonce; ?>"/>
	<input name="action" type="hidden" value="crpcode_nonce" />&nbsp;
	<input name="action" type="hidden" value="crpcode_nonce" />&nbsp;</div>
	<div class="passcode-readmore">
	<a href="#clt-testimonial" class="button obtn btn_orange btn" align="right" onClick="submit_crdata(document.crForm.wppassc.value,document.crForm.wpppid.value);"><?php _e( 'Verify', 'AwsPortfolio' ); ?></a>
	</div>
	</form>
	</div>

<!--/ end passcode -->

	<div class="clear"></div>

<div id="crresp" align="center"></div>

	<div id="crboxcontainer">
	<div align="center" class="recom-label" id="recom-label"><?php _e( 'Please enter your recommendation here', 'AwsPortfolio' ); ?></div>
	<form name="crecommend" id="crecommend" action="" method="post">
	<?php $crbox_nonce = wp_create_nonce( 'crboxcont_nonce' ); ?>
	<input id="crboxnonce" name="crboxnonce" type="hidden" value="<?php echo $crbox_nonce; ?>"/>
	<input name="crpid" type="hidden" value="<?php echo $post_id; ?>" />
	<input name="action" type="hidden" value="crcontent_nonce" />&nbsp;
	<textarea id="crboxcont" name="crboxcont" rows="5" cols="25" class="crbox" style="height:120px; width:380px;"></textarea><br>
	<div align="center">
	<input type="button" name="csubmit" id="csubmit" value="<?php _e( 'Submit', 'AwsPortfolio' ); ?>" class="crboxsubmit" onClick="submit_recommendation(crboxcont.value,crpid.value);">
	</div><!--/ center -->
	</form>
	</div><!--/ crboxcontainer -->
</li>
</ul>

</div><!--/ prob -->

</div><!--/ project-prob -->

<?php
	endwhile; 
	endif;
?>

	<div class="clear"></div>

	</div><!--/ end project-detail -->

	</div><!--/ end main-container -->

	<?php echo stripslashes( html_entity_decode( get_option( 'aws_single_container_div_end' ) ) ); ?>

<?php get_footer(); ?>