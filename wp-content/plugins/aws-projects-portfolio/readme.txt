=== AWS Projects Portfolio ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: projects, portfolio, testimonials, references, recommendations, reviews, client quotes
Requires at least: 3.9.2
Tested up to: 4.3.0
Stable tag: 1.0.4
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

---------------------------------------------------------------------------------------------------

=== General Information ===

* Find complete setup instructions in plugin settings page.

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://project.wp-build.com) - Testdrive every aspect of the plugin.
* [Manuals & Support](http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Some Admin Features ===

* Change the page slug.
* Enter cusom CSS and/or opening/closing container DIVs in addition to the standard ones, so if your single project page looks weird then you can try to add your themes DIVs.
* Change the project image sizes.
* Change the title text for Project Execution, Problem, Solution and Client Testimonial.
* Change the pagination colors.
* Set HTML or TEXT emails.
* Edit the outgoing email messages.
* Request client testimonials.
* Approve client testimonials.
* Easy translation with Poedit or CodeStyling Localization Plugin.

=== Some Client Features ===

* Clients can post their testimonials.

=== Installation ===

* Download AWS Projects Portfolio.
* Extract the downloaded .zip file. (Not necessary if filename dosen’t say so, ie. UNZIP-first)
* Login to your websites FTP and navigate to the “wp-contents/plugins/” folder.
* Upload the resulting “aws-projects-portfolio.zip” or “aws-projects-portfolio-X.X.X.zip” folder to the websites plugin folder.
* Go to your websites Dashboard, access the “plugins” section on the left hand menu.
* Locate “AWS Projects Portfolio” in the plugin list and click “activate”.
* Make sure to backup your translations before any upgrade.

Visit your Projects -> Settings, configure any options as desired. That’s it!

=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.

