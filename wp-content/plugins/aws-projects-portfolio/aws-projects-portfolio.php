<?php
/*
Plugin Name: AWS Projects Portfolio
Plugin URI: http://www.arctic-websolutions.com/
Version: 1.0.4
Description: The very best and most convenient way to publish your Projects and Portfolio listings. Includes password protected Client Testimonials submitted by the actual clients.
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


define ( 'AWS_PROJECTS_VERSION', '1.0.4' );
define ( 'CUSTOM_EMAIL_SEPS', '[^-*-^]' );
define ( 'AWS_PROJECTS_WP_PAGE', basename( $_SERVER['PHP_SELF'] ) );
define ( 'AWS_PROJECTS_SLUG', basename( dirname( __FILE__ ) ) );
define ( 'AWS_PROJECTS_URI', plugin_dir_url( __FILE__ ) );
define ( 'AWS_PROJECTS_TEXTDOMAIN', 'AwsPortfolio' );

include_once( 'functions.php' );

// Register the AWS Projects Portfolio  custom post type and shortcode
add_action( 'init', 'awsprojects_portfolio_post_type_init' );

// Register the AWS Projects Portfolio Cat taxonomy
add_action( 'init', 'generate_awsprojects_portfolio_type_taxonomy', 1 );

add_filter( 'post_updated_messages', 'awsprojects_portfolio_updated_messages' );

add_action( 'plugins_loaded', 'aws_portfolio_load_language' );

function aws_portfolio_load_language() {
	load_plugin_textdomain( 'AwsPortfolio', false, AWS_PROJECTS_SLUG . '/languages/' );
}

if ( is_admin() ) {

	add_action( 'init', 'awsprojects_portfolio_session_start', 1 );
	add_action( 'wp_logout', 'awsprojects_portfolio_session_end' );
	add_action( 'wp_login', 'awsprojects_portfolio_session_end' );

	$plugin = plugin_basename(__FILE__);
	$file = WP_PLUGIN_URL.'/'.str_replace( basename( __FILE__ ),"", plugin_basename( __FILE__ ) ) . 'css/admin.css';

	register_activation_hook( __FILE__, 'awsprojects_portfolio_install' );

	add_action( 'admin_menu', 'awsprojects_portolio_admin_menu' );
	add_filter( 'plugin_action_links_' . $plugin, 'add_awsprojects_portfolio_plugin_settings_link' );
	add_action( 'admin_print_scripts', 'awsprojects_portfolio_admin_scripts' );
	add_action( 'admin_print_styles', 'awsprojects_portfolio_admin_styles' );
	add_action( 'admin_notices', 'awsprojects_portfolio_display_update_alert' );
	add_filter( 'manage_edit-awsprojects_columns', 'add_new_awsprojects_portfolio_columns' );
	add_action( 'manage_posts_custom_column', 'manage_awsprojects_portfolio_columns', 10, 2 );
	add_action( 'admin_head-edit.php', 'awsprojects_portfolio_quickedit' );
	add_filter( 'post_row_actions', 'awsprojects_portfolio_custom_edit',10,2 );

	// Add the Save Metabox Data
	add_action( 'save_post', 'save_awsprojects_portfolio_meta', 1 ); // Save the custom fields

	register_deactivation_hook( __FILE__, 'awsprojects_portfolio_remove' );

	add_action( 'admin_enqueue_scripts', 'awsprojects_portfolio_set_admin_css' );

} else {
	add_shortcode( 'aws-projects-portfolio', 'awsprojects_portfolio_loop' );
	add_action ( 'wp','awsprojects_shortcode' );
	add_filter( 'query_vars', 'awsprojects_portfolio_queryvars' );
	add_filter( 'posts_join', 'awsprojects_portfolio_search_join', 10, 2 );
	add_filter( 'posts_where', 'awsprojects_portfolio_search_where', 10, 2 );

	include_once( 'frontend/create-project.php' );

}
	include_once( 'widget.php' );

// Setup template redirect for the frontend project image uploader
add_action( 'template_redirect', 'aws_projects_portfolio_template_redirect', 1 );

function aws_projects_portfolio_template_redirect() {

	if(isset($_GET['process_upload']))
	{
		header("HTTP/1.1 200 OK");
		include(ABSPATH . 'wp-content/plugins/aws-projects-portfolio/frontend/upload.php');
		exit;
	}
	if(isset($_GET['process_mail']))
	{
		header("HTTP/1.1 200 OK");
		include(ABSPATH . 'wp-content/plugins/aws-projects-portfolio/frontend/wp-mailer.php');
		exit;
	}

}
?>