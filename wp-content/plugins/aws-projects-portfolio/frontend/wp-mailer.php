<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


require_once( ABSPATH . '/wp-load.php' );

	$siteurl =  get_bloginfo( 'wpurl');
	$html_text = get_option( 'html_or_text_emails' );
	
	$to = isset( $_POST['to'] ) ? trim( $_POST['to'] ): '';
	$name = isset( $_POST['name'] ) ? trim( $_POST['name'] ) : '';
	$email = isset( $_POST['email'] ) ? trim( $_POST['email'] ) : '';
	
	$to = base64_decode( $to );
	$to = str_replace( CUSTOM_EMAIL_SEPS, '@', $to );
	
	$content = isset( $_POST['content'] ) ? trim( $_POST['content'] ) : '';
	
	$error = false;
	if( $to === '' || $email === '' || $content === '' || $name === '' ) {
		$error = true;
	}
	if( !preg_match( '/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/', $email ) ) {
		$error = true;
	}
	if( !preg_match( '/^[^@]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$/', $to ) ) {
		$error = true;
	}
	
	if( $error == false ) {

		$subject = ''.__( 'Message from', 'AwsPortfolio' ).' '.$name.'';

		$headers   = array();
		$headers[] = 'MIME-Version: 1.0' . "\r\n";
		$headers[] = 'Content-type: '.$html_text.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
		$headers[] = 'From: '.get_bloginfo( 'name' ).'<'.get_bloginfo( 'admin_email' ).'>' . "\r\n";
		$headers[] = 'Reply-To: '.$name.' <'.$email.'>';

		$body  = '' . __( 'Hello,', 'AwsPortfolio' ) . '<br/><br/>';
		$body .= ''.$name.' ' . __( 'has sent you the following message via your listing at', 'AwsPortfolio' ) . ' ' . get_bloginfo( 'name' ) . '.<br/></br>';
		if ( $html_text == "text/html" ) {
		$body .= '' . __( '<strong>Message</strong>:', 'AwsPortfolio' ) . '<br/>'.nl2br( stripslashes( $content ) ).'<br/></br>';
		} else {
		$body .= '' . __( '<strong>Message</strong>:', 'AwsPortfolio' ) . '<br/>'.$content.'<br/></br>';
		}
		$body .= '' . __( 'To reply to this message, just click the reply button in your mail program.', 'AwsPortfolio' ) . '<br/><br/><br/>';
		$body .= '' . __( 'Thanks,', 'AwsPortfolio' ) . '<br/><strong>' . get_bloginfo( 'name' ) . '</strong><br/><a style="text-decoration: none;" href="' . get_bloginfo( 'wpurl' ) . '">' . get_bloginfo( 'wpurl' ) . '</a><br/><br/>';

		if( wp_mail( $to, $subject, $body, $headers ) ) {
			echo 'success';
		}
	}
?>