<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


require_once( ABSPATH . '/wp-load.php' );
require_once( ABSPATH . '/wp-admin/includes/file.php' );
require_once( ABSPATH . '/wp-admin/includes/image.php' );

// required for wp_handle_upload() to upload the file
$upload_overrides = array( 'test_form' => FALSE );

if ( isset( $_POST['update'] ) && $_POST['update'] > 0 ) {
	awsprojects_delete_attachment( $_POST['post_id'] );
	delete_post_meta( $_POST['post_id'], "_imageurl" );
	delete_post_meta( $_POST['post_id'], "_clientphoto" );
}

global $current_user;
get_currentuserinfo();
$logged_in_user = $current_user->ID;

// count how many files were uploaded
$count_files = count( $_FILES['files'] );

// load up a variable with the upload direcotry
$wp_upload_dir = wp_upload_dir();
$uploads = ( $wp_upload_dir['url'] );

// foreach file uploaded do the upload
foreach ( range( 0, $count_files ) as $i ) {

        // create an array of the $_FILES for each file
        $file_array = array(
                'name'          => @$_FILES['files']['name'][$i],
                'type'          => @$_FILES['files']['type'][$i],
                'tmp_name'      => @$_FILES['files']['tmp_name'][$i],
                'error'         => @$_FILES['files']['error'][$i],
                'size'          => @$_FILES['files']['size'][$i],
        );

        // check to see if the file name is not empty
        if ( !empty( $file_array['name'] ) ) {

            // upload the file to the server
            $uploaded_file = wp_handle_upload( $file_array, $upload_overrides );

            // checks the file type and stores it in a variable
            $wp_filetype = wp_check_filetype( basename( $uploaded_file['file'] ), null );
            $filename = @basename( $uploaded_file['file'] );

            // set up the array of arguments for "wp_insert_post();"
            $attachment = array(
                'post_mime_type' => $wp_filetype['type'],
                'post_title' => preg_replace('/.[^.]+$/', '', basename( $uploaded_file['file'] ) ),
                'post_content' => '',
                'post_author' => $logged_in_user,
                'post_status' => 'inherit',
                'post_type' => 'attachment',
                'post_parent' => $_POST['post_id'],
                'guid' => $uploads . '/' . $filename
            );

	    $url = '' . $uploads . '/' . $filename . '';
	    if ( $i == '0' ) update_post_meta( $_POST['post_id'], "_imageurl", $url );
	    if ( $i == '1' ) update_post_meta( $_POST['post_id'], "_clientphoto", $url );

            // insert the attachment post type and get the ID
            $attachment_id = wp_insert_post( $attachment );

                // generate the attachment metadata
                $attach_data = wp_generate_attachment_metadata( $attachment_id, $uploaded_file['file'] );

                // update the attachment metadata
                wp_update_attachment_metadata( $attachment_id,  $attach_data );

		// update the '_wp_attached_file' post meta
		update_attached_file( $attachment_id, $uploaded_file['file'] );

        }
}
	if ( isset( $_POST['update'] ) && $_POST['update'] > 0 ) {
	header("Location: " . $_SERVER['HTTP_REFERER'] . "/?complete=".$_POST['update']."");
	} else {
	header("Location: " . $_SERVER['HTTP_REFERER'] . "/?success=".$_POST['post_id']."");
	}
?>