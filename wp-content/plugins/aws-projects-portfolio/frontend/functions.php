<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


function simple_aws($content = null) {
	global $post, $error_array;

	// We're outputing a lot of html and the easiest way 
	// to do it is with output buffering from php.
	ob_start();

	$problem = get_option('aws_single_problem_text');
	if ( empty( $problem ) ) $problem = __( 'Problem', 'AwsPortfolio' );
	$solution = get_option('aws_single_solution_text');
	if ( empty( $solution ) ) $solution = __( 'Solutions', 'AwsPortfolio' );
	$testimonial = get_option('aws_single_testimonial_text');
	if ( empty( $testimonial ) ) $testimonial = __( 'Client Testimonial', 'AwsPortfolio' );

	$pid = false; if ( isset( $_GET['pid'] ) ) $pid = $_GET['pid'];
	if ( $pid > '0' ) $post = get_post( $pid );
	else $post = false;

	$status	= get_option( 'aws_approve_project' );
	if ( empty( $status ) ) $status = "pending";

?>

<div id="simple-aws-postbox" class="<?php if(is_user_logged_in()) echo 'closed'; else echo 'loggedout'?>">

	<?php do_action( 'simple-aws-notice' ); ?>

	<?php do_action( 'awsolutions-upload-images' ); ?>

	<?php if ( isset( $_GET['success'] ) && $_GET['success'] > '0' ) {
		if ( $status == "pending" ) {
		echo '<p class="simple-aws-notice">' . __( 'Your project was successfully posted, and pending approval!', 'AwsPortfolio' ) . '</p>';
		} else {
		$view = '<a href="'.get_permalink( $_GET['success'] ).'">'.__( 'View project', 'AwsPortfolio' ).'</a>.';
		echo '<p class="simple-aws-notice">' . sprintf(__( 'Your project was successfully posted! %s', 'AwsPortfolio' ), $view) . '</p>';
		}
	} ?>

	<?php if ( isset( $_GET['complete'] ) && $_GET['complete'] > '0' ) {
		if ( $status == "pending" ) {
		echo '<p class="simple-aws-notice">' . __( 'Your project was successfully edited, and pending approval!', 'AwsPortfolio' ) . '</p>';
		} else {
		$view = '<a href="'.get_permalink( $_GET['complete'] ).'">'.__( 'View project', 'AwsPortfolio' ).'</a>.';
		echo '<p class="simple-aws-notice">' . sprintf(__( 'Your project was successfully edited! %s', 'AwsPortfolio' ), $view) . '</p>';
		}
	} ?>
	

<?php if ( empty( $_POST['action'] ) || ! empty( $error_array ) ) { ?>

<div class="simple-aws-inputarea">

<?php if(is_user_logged_in()) { ?>

<?php $args = array(
	'hide_empty' => 0,
	'show_option_none' => __( 'Select Category', 'AwsPortfolio' ),
	'taxonomy' => 'awsprojects_portfolio_cat',
); ?>

	<form id="aws-new-post" name="new_post" method="post" action="<?php echo awsprojects_get_page_url_by_shortcode( '[aws-projects-registration]' ); ?>">
		<p><label><?php _e( 'Select Category', 'AwsPortfolio' ); ?> *</label><?php wp_dropdown_categories( $args ); ?><br />
		<span class="small-text"><?php _e( 'Select the category where you want to publish the project.', 'AwsPortfolio' ); ?></span></p>

		<p><label><?php _e( 'Project Title', 'AwsPortfolio' ); ?> *</label><input type="text" id="aws-post-title" name="post-title" value="<?php if ( $post ) echo $post->post_title; ?>" tabindex="1" /><br /><span class="small-text"><?php _e( 'Enter a title for the project.', 'AwsPortfolio' ); ?></span></p>

		<p><label><?php _e( 'Description', 'AwsPortfolio' ); ?> *</label><textarea class="aws-content" name="posttext" id="aws-post-text" tabindex="2" rows="4" cols="60"><?php if ( $post ) echo $post->post_content; ?></textarea><br /><span class="small-text"><?php _e( 'Enter a description of the project itself.', 'AwsPortfolio' ); ?></span></p>

		<p><label><?php _e( 'Project URL', 'AwsPortfolio' ); ?> *</label><input type="text" id="aws-siteurl" name="_siteurl" value="<?php if ( $post ) echo get_post_meta( $post->ID, '_siteurl', true ); ?>" tabindex="3" /><br /><span class="small-text"><?php _e( 'Enter a URL to the finished project, demosite etc.', 'AwsPortfolio' ); ?></span></p>

		<p><label><?php echo $problem; ?></label><textarea class="aws-projectproblem" name="_projectproblem" id="_projectproblem" tabindex="4" rows="4" cols="60"><?php if ( $post ) echo get_post_meta( $post->ID, '_projectproblem', true ); ?></textarea><br /><span class="small-text"><?php _e( 'Describe the project, problems were rectified etc. (optional).', 'AwsPortfolio' ); ?></span></p>

		<p><label><?php echo $solution; ?></label><textarea class="aws-projectsolution" name="_projectsolution" id="_projectsolution" tabindex="5" rows="4" cols="60"><?php if ( $post ) echo get_post_meta( $post->ID, '_projectsolution', true ); ?></textarea><br /><span class="small-text"><?php _e( 'According to the problems, what was the solution? (optional).', 'AwsPortfolio' ); ?></span></p>

		<?php if ( get_option( 'cp_auction_installed' ) == "yes" ) { ?>
		<p><label><?php _e( 'Connected to AD', 'AwsPortfolio' ); ?></label><input type="text" id="aws-itemid" name="_itemid" value="<?php if ( $post ) echo get_permalink( get_post_meta( $post->ID, '_itemid', true ) ); ?>" tabindex="6" /><br /><span class="small-text"><?php _e( 'To enable a buy now button enter the URL to the AD this project is based on.', 'AwsPortfolio' ); ?></span></p>
		<?php } ?>

		<p><label><?php _e( 'Client Name', 'AwsPortfolio' ); ?></label><input type="text" id="aws-clientname" name="_clientname" value="<?php if ( $post ) echo get_post_meta( $post->ID, '_clientname', true ); ?>" tabindex="7" /></p>

		<p><label><?php _e( 'Client Email', 'AwsPortfolio' ); ?></label><input type="text" id="aws-clientemail" name="_clientemail" value="<?php if ( $post ) echo get_post_meta( $post->ID, '_clientemail', true ); ?>" tabindex="8" /></p>

		<p><label><?php _e( 'Client Password', 'AwsPortfolio' ); ?></label><input type="password" id="aws-wpp_passcode" name="_wpp_passcode" value="<?php if ( $post ) echo get_post_meta( $post->ID, '_wpp_passcode', true ); ?>" tabindex="9" /><br /><span class="small-text"><?php _e( 'Enter a password for your client to use when posting a testimonial (optional).', 'AwsPortfolio' ); ?></span></p>

		<p><label><?php _e( 'Additional Details', 'AwsPortfolio' ); ?></label><input type="text" id="aws-technical_details" name="_technical_details" value="<?php if ( $post ) echo get_post_meta( $post->ID, '_technical_details', true ); ?>" tabindex="10" /><br /><span class="small-text"><?php _e( 'Enter client address eg. Address, City, Country (optional).', 'AwsPortfolio' ); ?></span></p>

	<p><label><?php echo $testimonial; ?></label><textarea class="aws-clientrecommendation" name="_clientrecommendation" id="_clientrecommendation" tabindex="11" rows="4" cols="60"><?php if ( $post ) echo get_post_meta( $post->ID, '_clientrecommendation', true ); ?></textarea><br /><span class="small-text"><?php _e( 'If you already have a testimonial enter it here.', 'AwsPortfolio' ); ?></span></p>

		<input id="submit" type="submit" tabindex="12" value="<?php esc_attr_e( 'Continue »', 'AwsPortfolio' ); ?>" />					
		<input type="hidden" name="action" value="post" />
		<input type="hidden" name="update" value="<?php if ( $post ) echo $post->ID; ?>" />
		<?php wp_nonce_field( 'new-post' ); ?>
	</form>

	<?php } else { ?>		
		<h4><?php _e( 'You must be logged in to post or edit your project.', 'AwsPortfolio' ); ?></h4>
	<?php } ?>

</div>
 
</div> <!-- #simple-aws-postbox -->
<?php
	// Output the content.
	$output = ob_get_contents();
	ob_end_clean();
 
	// return only if we're inside a page. This won't list anything on a post or archive page. 
	if ( is_page() ) return $output;
}
	echo '</div>';
}
 
// Add the shortcode to WordPress. 
add_shortcode('aws-projects-registration', 'simple_aws');
 
 
function simple_aws_errors(){

	global $error_array;
	$url = '<a href="javascript: history.go(-1)">'.__( 'Go back »', 'AwsPortfolio' ).'</a>';
	foreach( $error_array as $error ) {
		echo '<p class="simple-aws-error">' . $error . ' '.$url.'</p>';
	}
}
 
function simple_aws_notices(){

	global $notice_array;

	if ( empty( $notice_array[1] ) ) {

	foreach( $notice_array as $notice ) {
		echo '<p>' . $notice . '</p>';
	}

	} else {
		$url = '<a href="'.awsprojects_get_page_url_by_shortcode( '[aws-projects-registration]' ).'?complete='.$notice_array[1].'"><strong>'.__( 'Click here', 'AwsPortfolio' ).'</strong></a>';
		echo '<p>'.sprintf(__( '%s to finish your editing, or upload new images below.', 'AwsPortfolio' ), $url).'</p><br />';
	}
}

function awsolutions_upload_images() {

	global $project_array;
?>
	<form id="aws-new-post" action="<?php echo get_bloginfo( 'wpurl' ).'/?process_upload=1' ?>" method="POST" enctype="multipart/form-data">
        <p><label><?php _e( 'Project Image', 'AwsPortfolio' ); ?> *</label><input type="file" name="files[]" accept="image/gif, image/jpeg"><br />
	<span class="small-text"><?php _e( 'Upload a project image, .gif, .jpg or .jpeg', 'AwsPortfolio' ); ?></span></p>
        <p><label><?php _e( 'Client Photo', 'AwsPortfolio' ); ?></label><input type="file" name="files[]" accept="image/gif, image/jpeg"><br />
	<span class="small-text"><?php _e( 'Upload a client profile photo, .gif, .jpg or .jpeg (optional).', 'AwsPortfolio' ); ?></span></p>

        <input type="hidden" name="post_id" value="<?php echo $project_array[0]; ?>">
        <input type="hidden" name="update" value="<?php echo $project_array[1]; ?>">
        <?php if ( $project_array[1] > '0' ) { ?>
      <p><?php _e( '<strong>Note:</strong> Please note that if you choose to update an image then ALL previously uploaded images will be deleted, which means that you need to upload both images again. Never upload client photo alone as this then will be used as project image.', 'AwsPortfolio' ); ?></p>
        <?php } ?>
        <p><input type="submit" class="btn" value="<?php _e( 'Start Upload »', 'AwsPortfolio' ); ?>"></p>
	</form>
<?php
}

function simple_aws_add_post() {

	if ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) && $_POST['action'] == 'post' ) {

	if ( !is_user_logged_in() )
		return;

		global $current_user;
 
		$user_id	= $current_user->ID;
		$category	= $_POST['cat'];
		$post_title     = $_POST['post-title'];
		$post_content	= $_POST['posttext'];
		$site_url	= $_POST['_siteurl'];
		$status		= get_option( 'aws_approve_project' );
		if ( empty( $status ) ) $status = "pending";
 
		global $error_array;
		$error_array = array();
 
		if ( $category == '-1' ) $error_array[] = __( 'Please select a category!', 'AwsPortfolio' );
		if ( empty( $post_title ) ) $error_array[] = __( 'Please enter a project title!', 'AwsPortfolio' );
		if ( empty( $post_content ) ) $error_array[] = __( 'Please enter a project description!', 'AwsPortfolio' );
		if ( empty( $site_url ) ) $error_array[] = __( 'Please enter project URL!', 'AwsPortfolio' );
 
		if ( count( $error_array ) == 0 ) {

			if ( empty( $_POST['update'] ) ) {

			$post_id = wp_insert_post( array(
				'post_author'	=> $user_id,
				'post_title'	=> $post_title,
				'post_type'     => 'awsprojects',
				'post_content'	=> $post_content,
				'post_status'	=> $status
				) );

			} else {

			$update_post = wp_update_post( array(
				'ID'		=> $_POST['update'],
				'post_title'	=> $post_title,
				'post_content'	=> $post_content,
				'post_status'	=> $status
				) );

			$post_id = $_POST['update'];
			wp_set_object_terms( $post_id, null, 'awsprojects_portfolio_cat' );

			}

		$term = get_term( $category, 'awsprojects_portfolio_cat' );
		wp_set_object_terms( $post_id, array( $term->slug ), 'awsprojects_portfolio_cat' );

		$project_meta['_siteurl'] = $_POST['_siteurl'];
		$project_meta['_projectproblem'] = $_POST['_projectproblem'];
		$project_meta['_projectsolution'] = $_POST['_projectsolution'];
		$project_meta['_itemid'] = url_to_postid( $_POST['_itemid'] );
		$project_meta['_clientname'] = $_POST['_clientname'];
		$project_meta['_clientemail'] = $_POST['_clientemail'];
		$project_meta['_wpp_passcode'] = $_POST['_wpp_passcode'];
		$project_meta['_technical_details'] = $_POST['_technical_details'];
		$project_meta['_clientrecommendation'] = $_POST['_clientrecommendation'];

		if ( !empty($_POST['_sortorder'] ) ) {
			$project_meta['_sortorder'] = $_POST['_sortorder'];
		} else {
			$project_meta['_sortorder'] = -1*( $post_id );
		}

		// Add values of $project_meta as custom fields
		foreach ( $project_meta as $key => $value ) { // Cycle through the $project_meta array!
			$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
			if ( get_post_meta( $post_id, $key, false ) ) { // If the custom field already has a value
				update_post_meta( $post_id, $key, $value );
			} else { // If the custom field doesn't have a value
				add_post_meta( $post_id, $key, $value );
			}
			if ( !$value ) delete_post_meta( $post_id, $key ); // Delete if blank
		}

		// Now send an notification email to admin
		$html_text = get_option( 'html_or_text_emails' );
		$post_title = get_the_title( $post_id );
		$editlink = admin_url( 'post.php?post='.$post_id.'&action=edit' );
		$awsplink = get_permalink( $post_id );
		$adminemail = get_bloginfo( 'admin_email' );

		if ( isset( $_POST['update'] ) ) {
		$subject = __( 'Project where updated', 'AwsPortfolio' );
		} else {
		$subject = __( 'New Project where posted', 'AwsPortfolio' );
		}

		$headers   = array();
		$headers[] = 'MIME-Version: 1.0' . "\r\n";
		$headers[] = 'Content-type: '.$html_text.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
		$headers[] = 'From: '.get_bloginfo( 'name' ).'<'.get_bloginfo( 'admin_email' ).'>' . "\r\n";
		$headers[] = 'Reply-To: '.get_bloginfo( 'name' ).' <'.get_bloginfo( 'admin_email' ).'>';

		$content  = '' . __( 'Hello Admin,', 'AwsPortfolio' ) . '<br/><br/>';
		if ( isset( $_POST['update'] ) ) {
		$content .= '' . __( 'Project where updated,', 'AwsPortfolio' ) . ' <strong>'.$post_title.'</strong> ' . __( 'project.', 'AwsPortfolio' ) . '<br/>'.$awsplink.'<br/><br/>';
		} else {
		$content .= '' . __( 'New Project where posted,', 'AwsPortfolio' ) . ' <strong>'.$post_title.'</strong> ' . __( 'project.', 'AwsPortfolio' ) . '<br/>'.$awsplink.'<br/><br/>';
		}
		$content .= '' . __( 'Please click on the following link to approve this project', 'AwsPortfolio' ) . '</br>'.$editlink.'<br/><br/><br/>';
		$content .= '' . __( 'Thanks,', 'AwsPortfolio' ) . '<br/><strong>' . get_bloginfo( 'name' ) . '</strong><br/>' . get_bloginfo( 'wpurl' ) . '<br/><br/>';

		wp_mail( $adminemail, $subject, $content, $headers );

			global $notice_array;
			$notice_array = array();
			$notice_array[] = __( 'Upload a project image and an optionally picture of your client.', 'AwsPortfolio' );
			if ( ! empty( $_POST['update'] ) ) $notice_array[] = $_POST['update'];
			add_action('simple-aws-notice', 'simple_aws_notices');

			global $project_array;
			$project_array = array();
			$project_array[] = $post_id;
			if ( ! empty( $_POST['update'] ) ) $project_array[] = $_POST['update'];
			add_action('awsolutions-upload-images', 'awsolutions_upload_images');
		} else {
			add_action('simple-aws-notice', 'simple_aws_errors');
		}
	}
}
add_action('init','simple_aws_add_post');