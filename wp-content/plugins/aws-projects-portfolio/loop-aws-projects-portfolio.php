<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


global $wp_query;
global $portfolio_types;
global $portfolio_output;
global $num_per_page;
global $limit_portfolios_returned;
global $currpageurl;
global $port;
global $display_the_credit;
global $options;


$odd_class = "";
$ul_close = "";
$li_close = "";

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$portfolio_open_empty = "";
$ul_open_empty = "";
$li_open_odd_empty = "";
$post_class = "portfolio_entry ".$odd_class;

if ( !isset( $currpageurl ) ) {
	$currpageurl = get_permalink();
	$port = 0;
	$portnum = "";
} else {
	$port ++;
	$portnum = "-" . $port;
}

$wp_query_holder = $wp_query;
$wp_query = null;

// If the user has set a hard value for the number of portfolios to return in the shortcode
if ( is_numeric( $limit_portfolios_returned ) ) {
	if ( $limit_portfolios_returned > 0 ) {
		$paged = 0;
		$num_per_page = $limit_portfolios_returned;
	}
}

// If the portfolio shortcode had no portfolio types defined
if ( empty( $portfolio_types ) ) {
	$wp_query = new WP_Query();
	$wp_query->query( array( 'post_type' => 'awsprojects', 'posts_per_page' => $num_per_page, 'orderby' => 'meta_value' . $options['sort_numerically'], 'meta_key' => '_sortorder', 'order' => 'ASC', 'paged'=> $paged ) );
} else {
	$wp_query = new WP_Query();
	$wp_query->query( array( 'post_type' => 'awsprojects', 'awsprojects_portfolio_cat' => $portfolio_types, 'post_status'=>'publish','posts_per_page' => $num_per_page, 'orderby' => 'meta_value' . $options['sort_numerically'], 'meta_key' => '_sortorder', 'order' => 'ASC', 'paged'=> $paged ) );
}

$portfolio_open	= '';
if ( $wp_query->have_posts() ) {

	$custom_css = get_option('aws_page_custom_css');
	$image_width = get_option('aws_page_image_width');
	if ( empty( $image_width ) ) $image_width = "235";
	$image_height = get_option('aws_page_image_height');
	if ( empty( $image_height ) ) $image_height = "150";
	$pg_border = get_option( 'aws_pagination_border' );
	//if ( empty( $pg_border ) ) $pg_border = "#A9C5E1";
	$pg_background = get_option( 'aws_pagination_background' );
	//if ( empty( $pg_background ) ) $pg_background = "#F0F0F6";
	$pg_hover = get_option( 'aws_pagination_hover' );
	//if ( empty( $pg_hover ) ) $pg_hover = "#bcd2e8";
	$pg_selected = get_option( 'aws_pagination_selected' );
	//if ( empty( $pg_selected ) ) $pg_selected = "#A9C5E1";

	$portfolio_output .= $portfolio_open;

	if ( $custom_css || $image_width || $image_height || $pg_border || $pg_background || $pg_hover || $pg_selected ) {

	$inst = false;
	$args1 = array( '', '<style type="text/css">', '#portfolio-image {', 'width: '.$image_width.'px;', 'height: '.$image_height.'px;', '}', $custom_css );
	$args2 = array();
	$args3 = array();
	if ( $pg_border ) {
	$args2 = array( '.pagination {', 'border: 1px solid '.$pg_border.';', '}' );
	$args3 = array( '.pagination ul.aws-projects-nav li {', 'border-right: 1px solid '.$pg_border.';', '}' );
	}
	$args4 = array();
	if ( $pg_background ) {
	$args4 = array( '.pagination {', 'background: '.$pg_background.';', '}' );
	}
	$args5 = array();
	if ( $pg_hover ) {
	$args5 = array( '.pagination ul.aws-projects-nav li a:hover {', 'background: '.$pg_hover.';', '}' );
	}
	$args6 = array();
	if ( $pg_selected ) {
	$args6 = array( '.pagination ul.aws-projects-nav li.selected {', 'background: '.$pg_selected.';', '}' );
	}
	$args7 = array( '</style>', '' );
	$args = array_merge( $args1, $args2, $args3, $args4, $args5, $args6, $args7 );
	foreach( $args as $arg ) {
	$inst .= $arg . "\r\n";
	}
	$css_custom = $inst;

	$portfolio_output .= $css_custom;

	}

	$portfolio_output .= '<div class="project-container">';
	$portfolio_output .= '<ul>';

	while ( $wp_query->have_posts() ) {
		$wp_query->the_post();

		$description = '';
		$description = get_the_content();

		$type = get_post_meta( get_the_ID(), "_awsprojects_portfolio_cat", true );
		$awsprojects_portfolio_cat = get_term_by( 'slug', $type, 'awsprojects_portfolio_cat' );
		if ( isset( $awsprojects_portfolio_cat->name ) ) {
			$type = $awsprojects_portfolio_cat->name;
		} else {
			$type = "";
		}
		$datecreate = get_post_meta( get_the_ID(), "_createdate", true );
		$client = get_post_meta( get_the_ID(), "_clientname", true );
		$technical_details = get_post_meta( get_the_ID(), "_technical_details", true );
		$siteurl = get_post_meta( get_the_ID(), "_siteurl", true );
		if( strpos( $siteurl, 'http://' ) !== 0 ) {

			$siteurl = 'http://' . $siteurl;

		}

		$imageurl = get_post_meta( get_the_ID(), "_imageurl", true );
		if ( ( empty( $imageurl ) ) ) {
		$imageurl = plugins_url( 'images/no-image.jpg' , __FILE__ );
		}
		$sortorder = get_post_meta( get_the_ID(), "_sortorder", true );

		if ( !empty( $description ) ) {
			$description = apply_filters( 'the_content', $description );
			$description = strip_tags( $description );
			if( strlen( $description ) >= 200 ):
			$description_val = substr( $description, 0, 200 )."...";
			else:
			$description_val = $description;
			endif;
		}
		$portfolio_titles = get_the_title();
		if( strlen( $portfolio_titles ) >= 37 ):
		$portfolio_title = substr( get_the_title(), 0, 37 )."..." ;
		else:
		$portfolio_title = $portfolio_titles;
		endif;

                      $portfolio_output .= '
                        <li>
                            <h2>'.$portfolio_title.'</h2> 
                            <div class="project-img">
                                <a href="'.get_permalink().'"><img id="portfolio-image" src="'.$imageurl.'" width="'.$image_width.'" height="'.$image_height.'" alt=""/></a>
                                <a href="'.$siteurl.'" target="_blank">'.$siteurl.'</a>
                            </div>
                            <div class="project-cont">
                                <p>'.$description_val.'</p>
                            </div>
                            <div class="project-details">
                                <a class="button obtn btn_orange btn" href="'.get_permalink().'">' .__( 'View Details', 'AwsPortfolio' ). '</a>
                            </div>
                        </li>';

	} // endwhile;
	$portfolio_output .= '</ul>';
	$portfolio_output .= '<div class="clear"></div>';
	$portfolio_output .= '</div><!-- #portfolios -->';
	$portfolio_output .= '<div class="clear"></div>';
	awsprojects_nav_pages( $wp_query, $currpageurl, "awsprojects_nav_bottom" );

} else {

	$portfolio_output .= $portfolio_open_empty;
	$portfolio_output .= $ul_open_empty;
	$portfolio_output .= $li_open_odd_empty;
	$portfolio_output .= '<div class="' . implode( " ", get_post_class( $post_class ) ) . ' empty">';
	$portfolio_output .= '<div class="portfolio_page_img">' .__( 'No listings found...', 'AwsPortfolio' ). '</div>';
	$portfolio_output .= '</div>';
	$portfolio_output .= $li_close;
	$portfolio_output .= $ul_close;

}

$wp_query = $wp_query_holder;
?>