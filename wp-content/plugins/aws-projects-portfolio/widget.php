<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


class AWS_Contact_Form_Widget extends WP_Widget {

	function __construct() {
        	$widget_ops = array( false, 'description' => __( 'Add a contact form to your sidebar as a widget.', 'AwsPortfolio' ) );
        	parent::__construct( false, 'Contact Form Widget', $widget_ops );

		if ( is_active_widget( false, false, $this->id_base ) ) {
			add_action( 'wp_print_scripts', array( &$this, 'add_script' ) );
		}

	}

	function add_script() {

	wp_register_script( 'jquery-validator', WP_PLUGIN_URL.'/aws-projects-portfolio/scripts/jquery.tools.min.js', array( 'jquery' ), '1.2.5', true );
	wp_enqueue_script( 'jquery-validator' );

	wp_register_script( 'aws-contact-form', WP_PLUGIN_URL.'/aws-projects-portfolio/scripts/contact-form.js', array( 'jquery', 'jquery-validator' ), AWS_PROJECTS_VERSION, true );
	wp_enqueue_script( 'aws-contact-form' );

	}

	function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Contact Me', 'AwsPortfolio' ) : $instance['title'], $instance, $this->id_base );
		$email = $instance['email'];
		$email = str_replace( '@', CUSTOM_EMAIL_SEPS, $email );
		$email = base64_encode( $email );

		if( empty( $success ) ) {
			$success = __( 'Your message was successfully sent.<br /><strong>Thank You!</strong>', 'AwsPortfolio' );
		}
		
		echo $before_widget;
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		$id = md5(time().' '.rand()); 
		?>

	<p style="display:none;"><?php _e( 'Your message was successfully sent.<br /><strong>Thank You!</strong>', 'AwsPortfolio' ); ?></p>
	<form class="contactform" action="<?php echo get_bloginfo('wpurl'); ?>/?process_mail=1" method="post" novalidate="novalidate">
	<p><input type="text" required="required" id="contact_<?php echo $id;?>_name" name="contact_<?php echo $id; ?>_name" class="text_input" value="" size="33" tabindex="20"/>
	<label for="contact_<?php echo $id; ?>_name"><?php _e( 'Name', 'AwsPortfolio' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

	<p><input type="email" required="required" id="contact_<?php echo $id; ?>_email" name="contact_<?php echo $id;?>_email" class="text_input" value="" size="33" tabindex="21"/>
	<label for="contact_<?php echo $id; ?>_email"><?php _e( 'Email', 'AwsPortfolio' ); ?>&nbsp;<span style="color: red;">*</span></label></p>

	<p><textarea required="required" name="contact_<?php echo $id; ?>_content" class="textarea" cols="33" rows="5" tabindex="22"></textarea></p>

	<div style="clear:both"></div>

	<p><input id="btn_<?php echo $id; ?>" type="submit" value="<?php _e( 'Send Message', 'AwsPortfolio' ); ?>" tabindex="23" /></p>
	<input type="hidden" value="<?php echo $id; ?>" name="unique_widget_id"/>
	<input type="hidden" value="<?php echo $email; ?>" name="contact_<?php echo $id; ?>_to"/>
	</form>	

		<?php echo $after_widget;

	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['email'] = strip_tags( $new_instance['email'] );

		return $instance;
	}

	function form( $instance ) {

		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$email = isset( $instance['email'] ) ? esc_attr( $instance['email'] ) : get_option( 'admin_email' );
	?>
	<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Title:', 'AwsPortfolio' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>

	<p><label for="<?php echo $this->get_field_id('email'); ?>"><?php _e( 'Your Email:', 'AwsPortfolio' ); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" /></p>
		
<?php
	}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "AWS_Contact_Form_Widget" );' ) );