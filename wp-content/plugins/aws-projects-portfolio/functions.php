<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


// Define and register the AWS Project Portfolio custom post type
function awsprojects_portfolio_post_type_init() {

$rewrite_slug = get_option( 'aws_rewrite_slug' );
if ( empty( $rewrite_slug ) ) $rewrite_slug = "project";

	$labels = array(
		'name' => _x( 'Projects', 'post type general name', 'AwsPortfolio' ),
		'singular_name' => _x( 'Project', 'post type singular name', 'AwsPortfolio' ),
		'add_new' => __( 'Add Project', 'AwsPortfolio' ),
		'add_new_item' => __( 'Add Project', 'AwsPortfolio' ),
		'edit_item' => __( 'Edit Project', 'AwsPortfolio' ),
		'new_item' => __( 'New Project', 'AwsPortfolio' ),
		'view_item' => __( 'View Project', 'AwsPortfolio' ),
		'search_items' => __( 'Search Projects', 'AwsPortfolio' ),
		'not_found' =>  __( 'No Projects found', 'AwsPortfolio' ),
		'not_found_in_trash' => __( 'No Portfolios found in Trash', 'AwsPortfolio' ), 
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_in_menu' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => ''.$rewrite_slug.'/%awsprojects_portfolio_cat%', 'with_front' => false ),
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array( 'title', 'editor', 'author' ),
		'register_meta_box_cb' => 'add_awsprojects_portfolio_metaboxes',
		'taxonomies' => array( 'awsprojects_portfolio_cat', 'awsprojects_portfolio_tag' )
	); 

	register_post_type( 'awsprojects', $args );

	flush_rewrite_rules();

}

// Define a custom Portfolio Type taxonomy and populate it
function generate_awsprojects_portfolio_type_taxonomy() {

	if (!taxonomy_exists('awsprojects_portfolio_cat')) {

	$labels = array(
		'name'              => _x( 'Project Categories', 'taxonomy general name', 'AwsPortfolio' ),
		'singular_name'     => _x( 'Project Category', 'taxonomy singular name', 'AwsPortfolio' ),
		'search_items'      => __( 'Search Project Category', 'AwsPortfolio' ),
		'popular_items'     => __( 'Popular Project Categories', 'AwsPortfolio' ),
		'all_items'         => __( 'All Project Categories', 'AwsPortfolio' ),
		'parent_item'       => __( 'Parent Project Category', 'AwsPortfolio' ),
		'parent_item_colon' => __( 'Parent Project Category:', 'AwsPortfolio' ),
		'edit_item'         => __( 'Edit Project Category', 'AwsPortfolio' ),
		'update_item'       => __( 'Update Project Category', 'AwsPortfolio' ),
		'add_new_item'      => __( 'Add New Project Category', 'AwsPortfolio' ),
		'new_item_name'     => __( 'New Project Category', 'AwsPortfolio' ),
		'menu_name'         => __( 'Project Categories', 'AwsPortfolio' )
	);

	register_taxonomy( 'awsprojects_portfolio_cat', 'awsprojects',
			array( 'hierarchical' => true, 
					'labels' => $labels,
					'show_tagcloud' => true,
					'public' => true,
					'show_in_nav_menus' => true,
					'show_ui' => true,
					'query_var' => 'awsprojects_portfolio_cat',
					'rewrite' => array( 'slug' => 'awsprojects_portfolio_cat' ) )
				);

	// If there are no AWS Projects Portfolio Type terms, add a default term
	if ( count( get_terms( 'awsprojects_portfolio_cat', 'hide_empty=0' ) ) == 0 ) {
		wp_insert_term( 'Default', 'awsprojects_portfolio_cat' );
		}
	}

	if (!taxonomy_exists('awsprojects_portfolio_tag')) {

	$labels = array(
		'name'              => _x( 'Project Tags', 'taxonomy general name', 'AwsPortfolio' ),
		'singular_name'     => _x( 'Project Tag', 'taxonomy singular name', 'AwsPortfolio' ),
		'search_items'      => __( 'Search Project Tags', 'AwsPortfolio' ),
		'popular_items'     => __( 'Popular Project Tags', 'AwsPortfolio' ),
		'all_items'         => __( 'All Project Tags', 'AwsPortfolio' ),
		'parent_item'       => __( 'Parent Project Tag', 'AwsPortfolio' ),
		'parent_item_colon' => __( 'Parent Project Tag:', 'AwsPortfolio' ),
		'edit_item'         => __( 'Edit Project Tag', 'AwsPortfolio' ),
		'update_item'       => __( 'Update Project Tag', 'AwsPortfolio' ),
		'add_new_item'      => __( 'Add New Project Tag', 'AwsPortfolio' ),
		'new_item_name'     => __( 'New Project Tag Name', 'AwsPortfolio' ),
		'menu_name'         => __( 'Project Tags', 'AwsPortfolio' )
	);

		register_taxonomy( 'awsprojects_portfolio_tag', 'awsprojects',
				array( 'hierarchical' => false, 
						'labels' => $labels,
						'show_tagcloud' => true,
						'public' => true,
						'show_in_nav_menus' => true,
						'show_ui' => true,
						'query_var' => 'awsprojects_portfolio_tag',
						'rewrite' => array( 'slug' => 'awsprojects_portfolio_tag' ) )
					);

	}
}

add_filter( 'post_link', 'awsprojects_portfolio_permalink', 10, 3 );
add_filter( 'post_type_link', 'awsprojects_portfolio_permalink', 10, 3 );

function awsprojects_portfolio_permalink( $permalink, $post_id, $leavename ) {

	if ( strpos( $permalink, '%awsprojects_portfolio_cat%' ) === FALSE ) return $permalink;
        // Get post
        $post = get_post( $post_id );
        if ( !$post ) return $permalink;

        // Get taxonomy terms
        $terms = wp_get_object_terms( $post->ID, 'awsprojects_portfolio_cat' );  
        if ( !is_wp_error( $terms ) && !empty( $terms ) && is_object( $terms[0] ) ) $taxonomy_slug = $terms[0]->slug;
        else $taxonomy_slug = get_option( 'aws_rewrite_slug' );
	return str_replace( '%awsprojects_portfolio_cat%', $taxonomy_slug, $permalink );

}

// Define the AWS Projects Portfolio custom post type update messages
function awsprojects_portfolio_updated_messages( $messages ) {

	global $post, $post_ID;

	$messages['awsprojects'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Project details updated.', 'AwsPortfolio' ),
		2 => __( 'Custom field updated.', 'AwsPortfolio' ),
		3 => __( 'Custom field deleted.', 'AwsPortfolio' ),
		4 => __( 'Project updated.', 'AwsPortfolio' ),
		5 => isset( $_GET['revision'] ) ? sprintf( __( 'Project restored to revision from %s', 'AwsPortfolio' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => __( 'Published.', 'AwsPortfolio' ),
		7 => __( 'Project details saved.', 'AwsPortfolio' ),
		8 => __( 'Project details submitted.', 'AwsPortfolio' ),
		9 => sprintf( __( 'Project scheduled for: <strong>%1$s</strong>.', 'AwsPortfolio' ), date_i18n( __( 'M j, Y @ G:i', 'AwsPortfolio' ), strtotime( $post->post_date ) ) ),
		10 => __( 'Project draft updated.', 'AwsPortfolio' ),
	);

	return $messages;
}

function awsprojects_portfolio_session_start() {

	if ( ! session_id() ) {
		session_start();
	}

}

function awsprojects_portfolio_session_end() {

	session_destroy();

}

/* Define AWS Projects Portfolio Plugin Activation process */
function awsprojects_portfolio_install() {

	awsprojects_portfolio_post_type_init();

	flush_rewrite_rules();

}

// Add Portfolio Options menu item
function awsprojects_portolio_admin_menu() {

	$page = add_submenu_page( 'edit.php?post_type=awsprojects', __( 'AWS Projects Portfolio Settings', 'AwsPortfolio' ), __( 'Settings', 'AwsPortfolio' ), 'manage_options', 'aws-projects-portfolio', 'aws_settings_page' );

	remove_submenu_page( 'edit.php?post_type=awsprojects', 'edit-tags.php?taxonomy=post_tag&amp;post_type=awsprojects' );

}

// Add plugin Settings link
function add_awsprojects_portfolio_plugin_settings_link( $links ) {

	$x = str_replace( basename( __FILE__ ),"", plugin_basename( __FILE__ ) );
	$settings_link = '<a href="edit.php?post_type=awsprojects&page=' . $x .'">' . __( 'Settings', 'AwsPortfolio' ) . '</a>';
	array_unshift( $links, $settings_link );

	return $links;

}

// Add a colorpicker to the settings page
add_action( 'admin_enqueue_scripts', 'aws_add_color_picker' );
function aws_add_color_picker( $hook ) {

	if( is_admin() ) {

	// Add the color picker css file
	wp_enqueue_style( 'wp-color-picker' );

	// Include our custom jQuery file with WordPress Color Picker dependency
	wp_enqueue_script( 'custom-script-handle', plugins_url( 'scripts/custom-script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );

	}

}

// Admin settings page
function aws_settings_page() {
?>

<div class="wrap">

<h2><?php _e( 'AWS Projects Portfolio Settings', 'AwsPortfolio' ); ?></h2>

<?php if( isset( $_POST['action'] ) && $_POST['action'] == 'update' && $_POST['option_page'] == 'aws-settings-tab' ) {

	$project_option['aws_rewrite_slug'] = isset( $_POST['aws_rewrite_slug'] ) ? esc_attr( $_POST['aws_rewrite_slug'] ) : '';
	$project_option['aws_single_width_css'] = isset( $_POST['aws_single_width_css'] ) ? esc_attr( $_POST['aws_single_width_css'] ) : '';
	$project_option['aws_single_image_width'] = isset( $_POST['aws_single_image_width'] ) ? esc_attr( $_POST['aws_single_image_width'] ) : '';
	$project_option['aws_single_image_height'] = isset( $_POST['aws_single_image_height'] ) ? esc_attr( $_POST['aws_single_image_height'] ) : '';	
	$project_option['aws_single_custom_css'] = isset( $_POST['aws_single_custom_css'] ) ? esc_attr( $_POST['aws_single_custom_css'] ) : '';
		$container_div = isset( $_POST['aws_single_container_div'] ) ? esc_attr( $_POST['aws_single_container_div'] ) : '';
		$container_div_end = isset( $_POST['aws_single_container_div_end'] ) ? esc_attr( $_POST['aws_single_container_div_end'] ) : '';
	$project_option['aws_single_execution_text'] = isset( $_POST['aws_single_execution_text'] ) ? esc_attr( $_POST['aws_single_execution_text'] ) : '';
	$project_option['aws_single_problem_text'] = isset( $_POST['aws_single_problem_text'] ) ? esc_attr( $_POST['aws_single_problem_text'] ) : '';
	$project_option['aws_single_solution_text'] = isset( $_POST['aws_single_solution_text'] ) ? esc_attr( $_POST['aws_single_solution_text'] ) : '';
	$project_option['aws_single_testimonial_text'] = isset( $_POST['aws_single_testimonial_text'] ) ? esc_attr( $_POST['aws_single_testimonial_text'] ) : '';
	$project_option['aws_page_image_width'] = isset( $_POST['aws_page_image_width'] ) ? esc_attr( $_POST['aws_page_image_width'] ) : '';
	$project_option['aws_page_image_height'] = isset( $_POST['aws_page_image_height'] ) ? esc_attr( $_POST['aws_page_image_height'] ) : '';
	$project_option['aws_page_custom_css'] = isset( $_POST['aws_page_custom_css'] ) ? esc_attr( $_POST['aws_page_custom_css'] ) : '';
	$project_option['aws_pagination_border'] = isset( $_POST['aws_pagination_border'] ) ? esc_attr( $_POST['aws_pagination_border'] ) : '';
	$project_option['aws_pagination_background'] = isset( $_POST['aws_pagination_background'] ) ? esc_attr( $_POST['aws_pagination_background'] ) : '';
	$project_option['aws_pagination_hover'] = isset( $_POST['aws_pagination_hover'] ) ? esc_attr( $_POST['aws_pagination_hover'] ) : '';
	$project_option['aws_pagination_selected'] = isset( $_POST['aws_pagination_selected'] ) ? esc_attr( $_POST['aws_pagination_selected'] ) : '';
	$project_option['html_or_text_emails'] = isset( $_POST['html_or_text_emails'] ) ? esc_attr( $_POST['html_or_text_emails'] ) : '';
	$project_option['aws_request_message'] = isset( $_POST['aws_request_message'] ) ? esc_attr( $_POST['aws_request_message'] ) : '';
	$project_option['aws_received_message'] = isset( $_POST['aws_received_message'] ) ? esc_attr( $_POST['aws_received_message'] ) : '';
	$project_option['aws_approve_project'] = isset( $_POST['aws_approve_project'] ) ? esc_attr( $_POST['aws_approve_project'] ) : '';
	$project_option['aws_project_information'] = isset( $_POST['aws_project_information'] ) ? esc_attr( $_POST['aws_project_information'] ) : '';
	$project_option['aws_project_payment_enabled'] = isset( $_POST['aws_project_payment_enabled'] ) ? esc_attr( $_POST['aws_project_payment_enabled'] ) : '';
	$project_option['aws_project_product_name'] = isset( $_POST['aws_project_product_name'] ) ? esc_attr( $_POST['aws_project_product_name'] ) : '';
	$project_option['aws_project_payment_amount'] = isset( $_POST['aws_project_payment_amount'] ) ? esc_attr( $_POST['aws_project_payment_amount'] ) : '';
	$project_option['aws_project_title'] = isset( $_POST['aws_project_title'] ) ? esc_attr( $_POST['aws_project_title'] ) : '';
	$project_option['aws_project_description'] = isset( $_POST['aws_project_description'] ) ? esc_attr( $_POST['aws_project_description'] ) : '';
	$project_option['aws_project_url_enabled'] = isset( $_POST['aws_project_url_enabled'] ) ? esc_attr( $_POST['aws_project_url_enabled'] ) : '';
	$project_option['aws_project_url'] = isset( $_POST['aws_project_url'] ) ? esc_attr( $_POST['aws_project_url'] ) : '';
	$project_option['aws_project_problem_enabled'] = isset( $_POST['aws_project_problem_enabled'] ) ? esc_attr( $_POST['aws_project_problem_enabled'] ) : '';
	$project_option['aws_project_problem'] = isset( $_POST['aws_project_problem'] ) ? esc_attr( $_POST['aws_project_problem'] ) : '';
	$project_option['aws_project_solution_enabled'] = isset( $_POST['aws_project_solution_enabled'] ) ? esc_attr( $_POST['aws_project_solution_enabled'] ) : '';
	$project_option['aws_project_solution'] = isset( $_POST['aws_project_solution'] ) ? esc_attr( $_POST['aws_project_solution'] ) : '';
	$project_option['aws_project_connected_enabled'] = isset( $_POST['aws_project_connected_enabled'] ) ? esc_attr( $_POST['aws_project_connected_enabled'] ) : '';
	$project_option['aws_project_connected'] = isset( $_POST['aws_project_connected'] ) ? esc_attr( $_POST['aws_project_connected'] ) : '';
	$project_option['aws_project_author_email_enabled'] = isset( $_POST['aws_project_author_email_enabled'] ) ? esc_attr( $_POST['aws_project_author_email_enabled'] ) : '';
	$project_option['aws_project_author_email'] = isset( $_POST['aws_project_author_email'] ) ? esc_attr( $_POST['aws_project_author_email'] ) : '';
	$project_option['aws_project_disable_client_option'] = isset( $_POST['aws_project_disable_client_option'] ) ? esc_attr( $_POST['aws_project_disable_client_option'] ) : '';
	$project_option['aws_project_clientname_enabled'] = isset( $_POST['aws_project_clientname_enabled'] ) ? esc_attr( $_POST['aws_project_clientname_enabled'] ) : '';
	$project_option['aws_project_clientname'] = isset( $_POST['aws_project_clientname'] ) ? esc_attr( $_POST['aws_project_clientname'] ) : '';
	$project_option['aws_project_clientemail_enabled'] = isset( $_POST['aws_project_clientemail_enabled'] ) ? esc_attr( $_POST['aws_project_clientemail_enabled'] ) : '';
	$project_option['aws_project_clientemail'] = isset( $_POST['aws_project_clientemail'] ) ? esc_attr( $_POST['aws_project_clientemail'] ) : '';
	$project_option['aws_project_clientpassword_enabled'] = isset( $_POST['aws_project_clientpassword_enabled'] ) ? esc_attr( $_POST['aws_project_clientpassword_enabled'] ) : '';
	$project_option['aws_project_clientpassword'] = isset( $_POST['aws_project_clientpassword'] ) ? esc_attr( $_POST['aws_project_clientpassword'] ) : '';
	$project_option['aws_project_clientdetails_enabled'] = isset( $_POST['aws_project_clientdetails_enabled'] ) ? esc_attr( $_POST['aws_project_clientdetails_enabled'] ) : '';
	$project_option['aws_project_clientdetails'] = isset( $_POST['aws_project_clientdetails'] ) ? esc_attr( $_POST['aws_project_clientdetails'] ) : '';
	$project_option['aws_project_testimonial_enabled'] = isset( $_POST['aws_project_testimonial_enabled'] ) ? esc_attr( $_POST['aws_project_testimonial_enabled'] ) : '';
	$project_option['aws_project_testimonial'] = isset( $_POST['aws_project_testimonial'] ) ? esc_attr( $_POST['aws_project_testimonial'] ) : '';


	// Add values of $project_option in options table
	foreach ( $project_option as $key => $value ) { // Cycle through the $project_option array!
		$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
		update_option( $key, $value );
		if ( empty( $value ) ) delete_option( $key ); // Delete if empty
	}
		update_option( 'aws_single_container_div', stripslashes( html_entity_decode( $container_div ) ) );
		update_option( 'aws_single_container_div_end', stripslashes( html_entity_decode( $container_div_end ) ) );

	echo '<div class="updated"><p><strong>' . __( 'Settings was updated!','AwsPortfolio' ) . '</strong></p></div>';
}
?>

<div class="metabox-holder">

        <div class="postbox">

                <div class="inside">

<form method="post" action="">

	<?php settings_fields( 'aws-settings-tab'); ?>

<table class="form-table intro-text">
    <tbody>

        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( "Create a <a style='text-decoration: none;' href='/wp-admin/post-new.php?post_type=page'><strong>Portfolio</strong></a> page and enter the short code [aws-projects-portfolio]. Display category based projects using, [aws-projects-portfolio per_page=9 portfolio_type='Category'].", "AwsPortfolio" ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( "<a style='text-decoration: none;' href='/wp-admin/post-new.php?post_type=awsprojects'>Add Projects</a> with the form provided as you would normally do for a post.", "AwsPortfolio" ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( "From the <a style='text-decoration: none;' href='/wp-admin/edit.php?post_type=awsprojects'>Projects Page</a>, there will be an option to Request Client Recommendation and once you click on it, an email with a password along with the link to submit the recommendation will be sent.", "AwsPortfolio" ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( "The client upon receiving the email, he will be redirected to the Project page where he will be prompted to enter the password that is mailed to him. Upon entering the correct password, he will be prompted with a form to submit his recommendations. Once the recommendation is submitted, the admin will receive an email alert along with the link to approve it.", "AwsPortfolio" ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( "The admin can also view the Recommendations received by clicking the menu Reviews Received and the Projects with new Recommendations will be displayed. Edit the Project page and at the bottom under the Recommendation field, select the Display in the website: ON. Now the client recommendation for that particular project will be displayed under the Project details page in the frontend. You can also switch off, if you choose not to display the recommendation.", "AwsPortfolio" ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( "If you have <a style='text-decoration: none;' href='http://www.arctic-websolutions.com/ads/cp-auction-social-sharing/' target='_blank'>CP Auction plugin</a> installed then you can connect each project to any Classified Ad that has the Buy Now option enabled. Enter the Ad`s ID in CP Auction Item ID field during the creation of the project and your customers can purchase this item directly from your project detail page.", "AwsPortfolio" ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( "<strong>Frontend posting</strong> - You can allow your users to post their projects from frontend, just create a <a style='text-decoration: none;' href='/wp-admin/post-new.php?post_type=page'><strong>Post a Project</strong></a> page and enter the short code [aws-projects-registration], then link to this page from users frontend dashboard.", "AwsPortfolio" ); ?></td>
        </tr>

    </tbody>
</table>

<div class="clear30"></div>

<div class="dotted"><h2><?php _e( 'General Settings', 'AwsPortfolio' ); ?></h2></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e( 'Rewrite Slug:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_rewrite_slug" value="<?php echo get_option('aws_rewrite_slug'); ?>" size="10"/> <?php _e( 'Custom rewrite slug settings for project details page. Leave blank to use the default slug.', 'AwsPortfolio' ); ?> </td>
        </tr>

    </tbody>
</table>

   <div align="center"><?php submit_button(); ?></div>

<div class="clear30"></div>

<div class="dotted"><h2><?php _e( 'Single Project Page', 'AwsPortfolio' ); ?></h2></div>

<table class="form-table">
    <tbody>

	<tr valign="top">
        <th scope="row"><?php _e( 'Single Project Width:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_single_width_css" value="<?php echo get_option('aws_single_width_css'); ?>" size="10"/> <?php _e( 'If necessary you can set the desired container width of the single project page. Leave blank to use the default width.', 'AwsPortfolio' ); ?> </td>
        </tr>

<tr valign="top">
        <th scope="row"><?php _e( 'Project Image Width:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_single_image_width" value="<?php echo get_option('aws_single_image_width'); ?>" size="10"/> <?php _e( 'If necessary you can set the desired image width of the project image. Leave blank to use the default width.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Project Image Height:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_single_image_height" value="<?php echo get_option('aws_single_image_height'); ?>" size="10"/> <?php _e( 'If necessary you can set the desired image height of the project image. Leave blank to use the default height.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Single Project Custom CSS:', 'AwsPortfolio' ); ?></th>
        <td><textarea name="aws_single_custom_css" rows="6" cols="80"><?php echo get_option('aws_single_custom_css'); ?></textarea><br/>
        <p class="padmin"><?php _e( 'Enter a custom CSS if it is necessary to improve the adaptation of the project view in your theme. Otherwise you can edit the file, /wp-content/plugins/aws-projects-portfolio/<strong>single-aws-projects-portfolio.php</strong>', 'AwsPortfolio' ); ?></p></td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Opening DIVs:', 'AwsPortfolio' ); ?></th>
        <td><textarea name="aws_single_container_div" rows="6" cols="80"><?php echo get_option('aws_single_container_div'); ?></textarea><br/>
        <p class="padmin"><?php _e( 'Your theme might use its own container DIVs in addition to the standard ones, so if your single project page looks weird then you can try to add your themes DIVs here. It could look like, <strong>&lt;div class="content"&gt;</strong> and you can enter as many as necessary.', 'AwsPortfolio' ); ?></p></td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Closing DIVs:', 'AwsPortfolio' ); ?></th>
        <td><textarea name="aws_single_container_div_end" rows="6" cols="80"><?php echo get_option('aws_single_container_div_end'); ?></textarea><br/>
        <p class="padmin"><?php _e( 'If you have entered any opening DIVs in the Opening DIVs field you will also need to close these. Enter the same number of closing DIVs as you have entered opening DIVs above. It could look like, <strong>&lt;/div&gt;&lt;!-- /content --&gt;</strong>', 'AwsPortfolio' ); ?></p></td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Project Execution Title:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_single_execution_text" value="<?php echo get_option('aws_single_execution_text'); ?>" size="20"/> <?php _e( 'Enter any title of your choice.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Problem Title:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_single_problem_text" value="<?php echo get_option('aws_single_problem_text'); ?>" size="20"/> <?php _e( 'Enter any title of your choice.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Solution Title:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_single_solution_text" value="<?php echo get_option('aws_single_solution_text'); ?>" size="20"/> <?php _e( 'Enter any title of your choice.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Testimonial Title:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_single_testimonial_text" value="<?php echo get_option('aws_single_testimonial_text'); ?>" size="20"/> <?php _e( 'Enter any title of your choice.', 'AwsPortfolio' ); ?> </td>
        </tr>

    </tbody>
</table>

   <div align="center"><?php submit_button(); ?></div>

<div class="clear30"></div>

<div class="dotted"><h2><?php _e( 'Portfolio Page', 'AwsPortfolio' ); ?></h2></div>

<table class="form-table">
    <tbody>

	<tr valign="top">
        <th scope="row"><?php _e( 'Project Image Width:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_page_image_width" value="<?php echo get_option('aws_page_image_width'); ?>" size="10"/> <?php _e( 'If necessary you can set the desired image width of the project image. Leave blank to use the default width.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Project Image Height:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_page_image_height" value="<?php echo get_option('aws_page_image_height'); ?>" size="10"/> <?php _e( 'If necessary you can set the desired image height of the project image. Leave blank to use the default height.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Page Custom CSS:', 'AwsPortfolio' ); ?></th>
        <td><textarea name="aws_page_custom_css" rows="6" cols="80"><?php echo get_option('aws_page_custom_css'); ?></textarea><br/>
        <p class="padmin"><?php _e( 'Enter a custom CSS if it is necessary to improve the adaptation of the projects listing page. Otherwise you can edit the file, /wp-content/plugins/aws-projects-portfolio/<strong>loop-aws-projects-portfolio.php</strong>', 'AwsPortfolio' ); ?></p></td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Pagination Border Color:', 'AwsPortfolio' ); ?></th>
        <td><input class="color-field" type="text" name="aws_pagination_border" value="<?php echo get_option('aws_pagination_border'); ?>" size="10"/> <?php _e( 'Change the border color for the pagination.', 'AwsPortfolio' ); ?> </td>
        </tr>

<tr valign="top">
        <th scope="row"><?php _e( 'Pagination Background Color:', 'AwsPortfolio' ); ?></th>
        <td><input class="color-field" type="text" name="aws_pagination_background" value="<?php echo get_option('aws_pagination_background'); ?>" size="10"/> <?php _e( 'Change the background color for the pagination.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Pagination Hover Color:', 'AwsPortfolio' ); ?></th>
        <td><input class="color-field" type="text" name="aws_pagination_hover" value="<?php echo get_option('aws_pagination_hover'); ?>" size="10"/> <?php _e( 'Change the mouse over / hover color for the pagination.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Pagination Selected Color:', 'AwsPortfolio' ); ?></th>
        <td><input class="color-field" type="text" name="aws_pagination_selected" value="<?php echo get_option('aws_pagination_selected'); ?>" size="10"/> <?php _e( 'Change the page selected color for the pagination.', 'AwsPortfolio' ); ?> </td>
        </tr>

    </tbody>
</table>

   <div align="center"><?php submit_button(); ?></div>

<div class="clear30"></div>

<div class="dotted"><h2><?php _e( 'Email Settings', 'AwsPortfolio' ); ?></h2></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e( 'HTML or TEXT Emails:', 'AwsPortfolio' ); ?></th>
	<td><select name="html_or_text_emails" id="html_or_text_emails" style="min-width: 100px;">
	<option value="text/html" <?php if(get_option('html_or_text_emails') == "text/html") echo 'selected="selected"'; ?>><?php _e( 'HTML', 'AwsPortfolio' ); ?></option>
	<option value="text/plain" <?php if(get_option('html_or_text_emails') == "text/plain") echo 'selected="selected"'; ?>><?php _e( 'TEXT', 'AwsPortfolio' ); ?></option>
	</select> <?php _e( 'Enable TEXT emails if you are using WP Better Emails Plugin.', 'AwsPortfolio' ); ?></td>
	</tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Request Recommendation:', 'AwsPortfolio' ); ?></th>
        <td><textarea name="aws_request_message" rows="6" cols="80"><?php echo get_option('aws_request_message'); ?></textarea><br/>
        <p class="padmin"><?php _e( 'Enter the message you want to send to your client when you want to make an inquiry regarding getting a recommendation written on your site. Leave blank to use the default message.', 'AwsPortfolio' ); ?></p></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Recommendation Received:', 'AwsPortfolio' ); ?></th>
        <td><textarea name="aws_received_message" rows="6" cols="80"><?php echo get_option('aws_received_message'); ?></textarea><br/>
        <p class="padmin"><?php _e( 'Enter the message you want to receive when a recommendation has been written on your site. Leave blank to use the default message.', 'AwsPortfolio' ); ?></p></td>
        </tr>

    </tbody>
</table>

   <div align="center"><?php submit_button(); ?></div>

<div class="clear30"></div>

<div class="dotted"><h2><?php _e( 'Frontend Posting Settings', 'AwsPortfolio' ); ?></h2></div>

<table class="form-table">
    <tbody>
	<?php $purl = '<a style="text-decoration: none;" href="https://wordpress.org/plugins/wp-ecommerce-paypal/" target="_blank">'.__( 'Easy PayPal Buy Now', 'AwsPortfolio' ).'</a>'; ?>
	<tr valign="top">
       <td colspan="2" class="introtext"><?php echo sprintf(__('If you want to charge for listings, you need to install the %s plugin and create a Cancel and Thank You page. AWS Projects Portfolio is completely configured to process payments via this plugin. Just set up the settings below.', 'AwsPortfolio' ), $purl); ?></td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Charge for Listing:', 'AwsPortfolio' ); ?></th>
	<td><select name="aws_project_payment_enabled" id="aws_project_payment_enabled" style="min-width: 100px;">
	<option value="" <?php if(get_option('aws_project_payment_enabled') == "") echo 'selected="selected"'; ?>><?php _e( 'Disabled', 'AwsPortfolio' ); ?></option>
	<option value="yes" <?php if(get_option('aws_project_payment_enabled') == "yes") echo 'selected="selected"'; ?>><?php _e( 'Enabled', 'AwsPortfolio' ); ?></option>
	</select> <?php echo sprintf(__( 'Enable if you want to charge for listing. This require the free %s plugin.', 'AwsPortfolio' ), $purl); ?></td>
	</tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Product Name:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_project_product_name" value="<?php echo get_option('aws_project_product_name'); ?>" size="20"/> <?php _e( 'Enter a product name as it should appear on the sales receipt, ie. Project Listing.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Payment Amount:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_project_payment_amount" value="<?php echo get_option('aws_project_payment_amount'); ?>" size="10"/> <?php _e( 'Enter the amount you wish to charge for this purchase, ie. 5 or 5.00', 'AwsPortfolio' ); ?> </td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Approve Listing:', 'AwsPortfolio' ); ?></th>
	<td><select name="aws_approve_project" id="aws_approve_project" style="min-width: 100px;">
	<option value="publish" <?php if(get_option('aws_approve_project') == "publish") echo 'selected="selected"'; ?>><?php _e( 'No', 'AwsPortfolio' ); ?></option>
	<option value="pending" <?php if(get_option('aws_approve_project') == "pending") echo 'selected="selected"'; ?>><?php _e( 'Yes', 'AwsPortfolio' ); ?></option>
	</select> <?php _e( 'Select Yes if you want to approve new or edited projects before they go publish.', 'AwsPortfolio' ); ?></td>
	</tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Post a Project Information:', 'AwsPortfolio' ); ?></th>
        <td><textarea name="aws_project_information" rows="6" cols="80"><?php echo get_option('aws_project_information'); ?></textarea><br/>
        <?php
        $check = awsprojects_get_page_url_by_shortcode( '[aws-projects-registration]' );
        $url = '<a style="text-decoration: none;" href="'.awsprojects_get_page_url_by_shortcode( '[aws-projects-registration]' ).'" target="_blank">'.__( 'Post a Project', 'AwsPortfolio' ).'</a>';
        if ( $check ) { ?>
        <p class="padmin"><?php echo sprintf(__( 'Enter any information concerning the post project process, price, terms etc. This information will appear on the %s page. HTML can be used.', 'AwsPortfolio' ), $url); ?></p>
        <?php } else { ?>
        <p class="padmin"><?php _e( 'Enter any information concerning the post project process, price, terms etc. This information will appear on the Post a Project page. HTML can be used.', 'AwsPortfolio' ); ?></p>
        <?php } ?></td>
        </tr>

	<tr valign="top">
       <td colspan="2" class="introtext"><?php _e('<strong>Form Fields</strong> - Enable / disable any of the fields used by the Post a Project form for your customers. Use the label fields for localization or if you just want to change the understanding of the fields. Enter a label in your language for any of the form fields listed below.', 'AwsPortfolio' ); ?></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Project Title:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_project_title" value="<?php echo get_option('aws_project_title'); ?>" size="20"/> <?php _e( 'Enter a label for the Project Title field (required field).', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Description:', 'AwsPortfolio' ); ?></th>
        <td><input type="text" name="aws_project_description" value="<?php echo get_option('aws_project_description'); ?>" size="20"/> <?php _e( 'Enter a label for the Description field (required field).', 'AwsPortfolio' ); ?> </td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Project URL:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_url_enabled" value="yes" <?php if(get_option('aws_project_url_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_url" value="<?php echo get_option('aws_project_url'); ?>" size="20"/> <?php _e( 'Enter a label for the Project URL field.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Problem:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_problem_enabled" value="yes" <?php if(get_option('aws_project_problem_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_problem" value="<?php echo get_option('aws_project_problem'); ?>" size="20"/> <?php _e( 'Enter a label for the Problem field.', 'AwsPortfolio' ); ?> </td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Solution:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_solution_enabled" value="yes" <?php if(get_option('aws_project_solution_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_solution" value="<?php echo get_option('aws_project_solution'); ?>" size="20"/> <?php _e( 'Enter a label for the Solution field.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<?php if ( get_option( 'cp_auction_installed' ) == "yes" ) { ?>
	<tr valign="top">
        <th scope="row"><?php _e( 'Connected to AD:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_connected_enabled" value="yes" <?php if(get_option('aws_project_connected_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_connected" value="<?php echo get_option('aws_project_connected'); ?>" size="20"/> <?php _e( 'Enter a label for the Connected to AD field.', 'AwsPortfolio' ); ?> </td>
        </tr>
	<?php } ?>

        <tr valign="top">
        <th scope="row"><?php _e( 'Author Email:', 'AwsPortfolio' ); ?></th>
       <td><input type="checkbox" name="aws_project_author_email_enabled" value="yes" <?php if(get_option('aws_project_author_email_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_author_email" value="<?php echo get_option('aws_project_author_email'); ?>" size="20"/> <?php _e( 'Enter a label for the Author Email field.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
       <td colspan="2" class="introtext"><?php _e('The following fields are only used for projects that are connected to a client / customer, ie. If you have carried out a project for another person or company then the following fields are used to receive testimonials or feedbacks from the client / customer you did the project for.', 'AwsPortfolio' ); ?></td>
        </tr>

<tr valign="top">
        <th scope="row"><?php _e( 'Disable Client Option:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_disable_client_option" value="yes" <?php if(get_option('aws_project_disable_client_option') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to disable ALL the below fields, or leave unticked to make your selections below.', 'AwsPortfolio' ); ?> </td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Client Name:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_clientname_enabled" value="yes" <?php if(get_option('aws_project_clientname_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_clientname" value="<?php echo get_option('aws_project_clientname'); ?>" size="20"/> <?php _e( 'Enter a label for the Client Name field.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Client Email:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_clientemail_enabled" value="yes" <?php if(get_option('aws_project_clientemail_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_clientemail" value="<?php echo get_option('aws_project_clientemail'); ?>" size="20"/> <?php _e( 'Enter a label for the Client Email field.', 'AwsPortfolio' ); ?> </td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Client Password:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_clientpassword_enabled" value="yes" <?php if(get_option('aws_project_clientpassword_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_clientpassword" value="<?php echo get_option('aws_project_clientpassword'); ?>" size="20"/> <?php _e( 'Enter a label for the Client Password field.', 'AwsPortfolio' ); ?> </td>
        </tr>

	<tr valign="top">
        <th scope="row"><?php _e( 'Additional Details:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_clientdetails_enabled" value="yes" <?php if(get_option('aws_project_clientdetails_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_clientdetails" value="<?php echo get_option('aws_project_clientdetails'); ?>" size="20"/> <?php _e( 'Enter a label for the Additional Details field.', 'AwsPortfolio' ); ?> </td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e( 'Client Testimonial:', 'AwsPortfolio' ); ?></th>
        <td><input type="checkbox" name="aws_project_testimonial_enabled" value="yes" <?php if(get_option('aws_project_testimonial_enabled') == 'yes') echo 'checked="checked"'; ?>> <?php _e( 'Tick to enable this field.', 'AwsPortfolio' ); ?><br /><input type="text" name="aws_project_testimonial" value="<?php echo get_option('aws_project_testimonial'); ?>" size="20"/> <?php _e( 'Enter a label for the Client Testimonial field.', 'AwsPortfolio' ); ?> </td>
        </tr>

    </tbody>
</table>

   <div align="center"><?php submit_button(); ?></div>

</form>

                </div>
        </div>
</div>
</div>
<?php }

// Make certain the scripts and css necessary to support the file upload button are active
function awsprojects_portfolio_admin_scripts() {

	global $post;

	$continue = "False";

	// Don't include the media upload script if we are not on a portfolio edit page
	if ( !empty( $post ) ) {
		if ( strtolower( $post->post_type ) == "awsprojects" ) {
			$continue = "True";
		}
	}
	if ( $continue == "True" ) {
		$script = plugins_url( 'scripts/file-uploader.js', __FILE__ );
		$script = awsprojects_portfolio_clear( $script );

		$editor_fix_script = plugins_url( 'scripts/aws-editor.js', __FILE__ );
		$editor_fix_script_val = awsprojects_portfolio_clear( $editor_fix_script );

		wp_enqueue_script( 'media-upload' );
		wp_enqueue_script( 'thickbox' );
		wp_register_script( 'projects-image-upload', $script, array('jquery', 'media-upload', 'thickbox' ) );
		wp_enqueue_script( 'projects-image-upload' );
		wp_register_script( 'aws-editorfix', $editor_fix_script_val );
		wp_enqueue_script( 'aws-editorfix' );
	}
}

function awsprojects_portfolio_admin_styles() {

	global $post;

	$continue = "False";

	// Don't include the media upload script if we are not on a portfolio edit page,
	if ( !empty( $post ) ) {
		if ( strtolower( $post->post_type ) == "awsprojects" ) {
			$continue = "True";
		}
	}

}

// Check and display any plugin messages
function awsprojects_portfolio_display_update_alert() {

	if ( ( ! empty( $message ) ) && ( $message != 'empty' ) ) {

		echo '<div class="awsprojects_portfolio_message">';
		echo '<div class="errrror"><p>' . $message . '</p></div>';
		echo '</div>';

		// now that we've displayed the alert, clear it out

	}
}

/* Register the AWS Projects Portfolio columns to display in the Portfolio Admin listing */
function add_new_awsprojects_portfolio_columns($columns) {

	// Note: columns in the listing are ordered in line with where they are created below
	unset( $columns['author'] );
	unset( $columns['date'] );
	$columns['title'] = _x( 'Project', 'column name', 'AwsPortfolio' );
	$columns['_imageurl'] = _x( 'Screenshot', 'column name', 'AwsPortfolio' );
	$columns['_wpp_passcode'] = _x( 'Status', 'column name', 'AwsPortfolio' );
	$columns['_clientname'] = _x( 'Client Name', 'column name', 'AwsPortfolio' );
	$columns['_clientemail'] = _x( 'Client Email', 'column name', 'AwsPortfolio' );
	$columns['_awsprojects_portfolio_cat'] = _x( 'Category', 'column name', 'AwsPortfolio' );
	$columns['date'] = _x( 'Date', 'column name', 'AwsPortfolio' );

	return $columns;

}

/* Define the data retrieval arguments for the Portfolio list columns */
function manage_awsprojects_portfolio_columns( $column_name, $id ) {

	global $wpdb;
	$strcont = '';

	switch ( $column_name ) {
	case '_sortorder':
		echo get_post_meta( $id , '_sortorder' , true );
		break;
	case '_imageurl':
		$aws_imgurl = get_post_meta( $id , '_imageurl' , true );
		if(!empty($aws_imgurl)):
			$strcont ='<img src="'.$aws_imgurl.'" border="0" width="100" height="90"></br>';
		endif;
		echo $strcont .= '<a href="'.get_post_meta( $id , '_siteurl' , true ).'" target="_blank">'.get_post_meta( $id , '_siteurl' , true).'</a>';

		break;

	case '_clientname':
		// Get the name of the client for whom the development was performed
		echo get_post_meta( $id , '_clientname' , true );
		break;

	case '_clientemail':
		// Get the name of the client for whom the development was performed
		echo get_post_meta( $id , '_clientemail' , true );
		break;

	case '_technical_details':
		// Get the technical details
		echo get_post_meta( $id , '_technical_details' , true );
		break;

	case '_wpp_passcode':
	// Get the URL to the actual website
	$aws_passwordval = get_post_meta( $id , '_wpp_passcode' , true );
	$awsval = get_post_meta( $id , '_clientrecommendation' , true );

	if( $aws_passwordval !='' ):
	$awscont_strcont = '<span class="psuccess-msg">' . __( 'Request Sent', 'AwsPortfolio' ) . '</span>';
	else:
	$awscont_strcont = '<span class="perror-msg">' . __( 'No Request', 'AwsPortfolio' ) . '</span>';
	endif;

	if( !empty( $aws_passwordval ) && !empty( $awsval ) ):
	$awsdisp_sts = get_post_meta( $id , '_wpp_crstatus' , true );
	$awscont_strcont = '<span class="ptest-msg">' . __( 'Review Received', 'AwsPortfolio' ) . '<br>';
	$awscont_strcont .= "<span class='";
	if( $awsdisp_sts == 'on' ) {
	$awscont_strcont .= "prsent";
	} else {
	$awscont_strcont .= "perror-msg";
	}
	$awscont_strcont .= "' align='center' style='margin:0px 0px 0px 45px;'>";
	if( $awsdisp_sts == 'on' ) {
	$awscont_strcont .= __( '(ON)', 'AwsPortfolio' );
	} else {
	$awscont_strcont .= __( '(OFF)', 'AwsPortfolio' );
	}
	$awscont_strcont .= "</span></span>";
	endif;
	echo $awscont_strcont;

	break;

	case '_awsprojects_portfolio_cat':
		$aws_terms = wp_get_object_terms( $id, 'awsprojects_portfolio_cat' );
		if( !empty( $aws_terms ) ){
		if( !is_wp_error( $aws_terms ) ) {
		$terms_data = '';
		foreach( $aws_terms as $term ) {
		$terms_data .= '<strong>'.$term->name.'</strong> ,'; 

		}
		echo $aws_termdata = substr( $terms_data, 0, strlen( $terms_data )-1 );
		}
		}

		break;

	default:
		break;
	} // End switch

}

// Hide the Post Tags and Portfolio Types Quick Edit fields on the AWS Projects Portfolio listing
function awsprojects_portfolio_quickedit() {

	global $post;

	if ( is_object( $post ) ) {
	    if ( $post->post_type == 'awsprojects' ) {
			echo '<style type="text/css">';
			echo '	.inline-edit-tags {display: none !important;}';
			echo '</style>';
		}
	}

}

function awsprojects_portfolio_custom_edit( $actions, $post ) {
	$actions['customedit'] = '';
	$clientrec_val = get_post_meta( $post->ID, '_clientrecommendation', true );

	if ( $post->post_type == 'awsprojects' ) {
		if( $clientrec_val == '' ):
		//Adding a custom link and passing the post id with it
		$actions['customedit'] .= '<a title="' . __( 'Send the client a recommendation request', 'AwsPortfolio' ) . '" href=\''.admin_url( 'edit.php?post_type=awsprojects&post='.$post->ID ).'&wppaction=1\' class="crecomm">' . __( 'Request Client Recommendation', 'AwsPortfolio' ) . '</a>';
		endif;
	}
	return $actions;
}

// Portfolio edit screen code start
function save_awsprojects_portfolio_meta( $post_id ) {

	$postid = wp_is_post_revision( $post_id );

	if ( $postid == false ) {

		// If the save was initiated by an autosave or a quick edit, exit out as the Portfolio fields being updated here may get over written or hang the save
		if ( !isset( $_POST['autosave_quickedit_check'] ) ) {
			return $post_id;
		}

		// Verify this call is the result of a POST
		if ( empty( $_POST ) ) {
			return $post_id;
		}

		// If the user isn't saving a portfolio
		if ( strtolower( $_POST['post_type'] ) != "awsprojects" ) {
			return $post_id;
		}

		// Verify this came from our screen and with proper authorization, because save_post can be triggered at other times
		if ( !check_admin_referer( 'awsprojects_portfolio_edit', 'wppportfoliometanonce' ) ) {
			return $post_id;
		}

		// Is the user allowed to edit the post or page?
		if ( !current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}

		// OK, we're authenticated, now we need to find and save the data
		$portfolio_meta['_siteurl'] = $_POST['_siteurl'];
		$portfolio_meta['_imageurl'] = $_POST['_imageurl'];
		$portfolio_meta['_projectproblem'] = $_POST['_projectproblem'];
		$portfolio_meta['_projectsolution'] = $_POST['_projectsolution'];
		$portfolio_meta['_itemid'] = $_POST['_itemid'];
		$portfolio_meta['aws_project_author_email'] = $_POST['aws_project_author_email'];
		$portfolio_meta['_clientname'] = $_POST['_clientname'];
		$portfolio_meta['_clientemail'] = $_POST['_clientemail'];
		$portfolio_meta['_clientphoto'] = $_POST['_clientphoto'];
		$portfolio_meta['_wpp_passcode'] = $_POST['_wpp_passcode'];
		$portfolio_meta['_technical_details'] = $_POST['_technical_details'];
		$portfolio_meta['_clientrecommendation'] = $_POST['_clientrecommendation'];
		if ( !empty($_POST['_sortorder'] ) ) {
			$portfolio_meta['_sortorder'] = $_POST['_sortorder'];
		} else {
			$portfolio_meta['_sortorder'] = -1*( $post_id );
		}
	 
		// Add values of $portfolio_meta as custom fields
		foreach ( $portfolio_meta as $key => $value ) { // Cycle through the $portfolio_meta array!
			$value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
			if ( get_post_meta( $post_id, $key, false ) ) { // If the custom field already has a value
				update_post_meta( $post_id, $key, $value );
			} else { // If the custom field doesn't have a value
				add_post_meta( $post_id, $key, $value );
			}
			if ( !$value ) delete_post_meta( $post_id, $key ); // Delete if blank
		}
	}
}

// Define the Portfolio edit form custom fields
function awsprojects_portfolio_edit_init() {

	global $post;

	// Noncename needed to verify where the data originated
	wp_nonce_field( 'awsprojects_portfolio_edit', 'wppportfoliometanonce' );

	// Gather any existing custom data for the Portfolio
	$datecreate = get_post_meta( $post->ID, '_createdate', true );
	$siteurl = get_post_meta( $post->ID, '_siteurl', true );
	$imageurl = get_post_meta( $post->ID, '_imageurl', true );
	$projectproblem = get_post_meta( $post->ID, '_projectproblem', true );
	$projectsolution = get_post_meta( $post->ID, '_projectsolution', true );
	$item_id = get_post_meta( $post->ID, '_itemid', true );
	$author_email = get_post_meta( $post->ID, 'aws_project_author_email', true );
	$client_name = get_post_meta( $post->ID, '_clientname', true );
	$clientemail = get_post_meta( $post->ID, '_clientemail', true );
	$client_photo = get_post_meta( $post->ID, '_clientphoto', true );
	$client_passcode = get_post_meta( $post->ID , "_wpp_passcode", true );
	$technical_details = get_post_meta( $post->ID, '_technical_details', true );
	$clientrecommendation = get_post_meta( $post->ID, '_clientrecommendation', true );
	$wpp_portfolio_type = get_post_meta( $post->ID, '_awsprojects_portfolio_cat', true );
	$sortorder = get_post_meta( $post->ID, '_sortorder', true );
	if ( $sortorder == "" ) $sortorder = "-" . $post->ID;

	// Gather the list of AWS Projects Portfolio Types
	$portfolio_type_list = get_terms( 'awsprojects_portfolio_cat', 'hide_empty=0' ); 

 // Build out the form fields
	echo '<p><label for="_siteurl">' . __( 'Project URL: ', 'AwsPortfolio' ) . '</label>';
	echo '<input type="text" id="_siteurl" name="_siteurl" value="' . $siteurl . '" class="widefat" /></p>';

	echo '<p><label for="_imageurl">' . __( 'Project Screenshot URL: ', 'AwsPortfolio' ) . '</label>';
	echo '<input id="upload_portfolio_image_button" class="upload_image_button" type="button" value="' . __( 'Upload Image', 'AwsPortfolio' ) . '" /><br />';
	echo '<input type="text" id="_imageurl" name="_imageurl" value="' . $imageurl . '" class="widefat shortbottom" /><br />';

	if( !empty( $imageurl ) ):

	echo '<p><a href="'.$imageurl.'" rel="" target="_blank"><img src="'.$imageurl.'" border="0" width="120" height="120"></a></p>';

	endif;

	echo '<p><label for="_portfolio_project_problem"><b>&nbsp;' . __( 'Problem:', 'AwsPortfolio' ) . '</b></label></br>';

	echo '<textarea id="_projectproblem" name="_projectproblem" rows="5" cols="90">'.$projectproblem.'</textarea>';

	echo '<p><label for="_portfolio_project_problem"><b>&nbsp;' . __( 'Solution:', 'AwsPortfolio' ) . '</b></label></br>';

	echo '<textarea id="_projectsolution" name="_projectsolution" rows="5" cols="90">'.$projectsolution.'</textarea>';

	if ( get_option( 'cp_auction_installed' ) == "yes" ):

	echo '<p><label for="_itemid"><strong>' . __( 'CP Auction Item ID: ', 'AwsPortfolio' ) . '</strong> ( ' . __( 'Classified Ad ID - Enable a buy now button on project details page.', 'AwsPortfolio' ) . ' )</label>';
	echo '<input type="text" id="_itemid" name="_itemid" value="' . $item_id . '" class="widefat" /></p>';

	endif;

	echo '<p><label for="aws_project_author_email">' . __( 'Author Email: ', 'AwsPortfolio' ) . '</label>';
	echo '<input type="text" id="aws_project_author_email" name="aws_project_author_email" value="' . $author_email . '" class="widefat" /></p>';

	echo '<p><label for="_clientname">' . __( 'Client Name: ', 'AwsPortfolio' ) . '</label>';
	echo '<input type="text" id="_clientname" name="_clientname" value="' . $client_name . '" class="widefat" /></p>';

	echo '<p><label for="_clientname">' . __( 'Client Email: ', 'AwsPortfolio' ) . '</label>';
	echo '<input type="text" id="_clientemail" name="_clientemail" value="' . $clientemail . '" class="widefat" /></p>';

	echo '<p><label for="_imageurl">' . __( 'Client Photo: ', 'AwsPortfolio' ) . '</label>';
	echo '<input id="upload_cphoto_image_button" class="upload_image_button" type="button" value="' . __( 'Upload Image', 'AwsPortfolio' ) . '" /><br />';
	echo '<input type="text" id="_clientphoto" name="_clientphoto" value="' . $client_photo . '" class="widefat shortbottom" /><br />';

	if( !empty( $client_photo ) ):
	echo '<p><a href="'.$client_photo.'" rel="" target="_blank"><img src="'.$client_photo.'" border="0" width="48" height="48"></a></p>';
	endif;

	echo '<p><label for="_sortorder">' . __( 'Client Password: ', 'AwsPortfolio' ) . '</label>';
	echo '<input type="text" id="_wpp_passcode" name="_wpp_passcode" value="' . $client_passcode . '" class="code" />';

	echo '<p><label for="_technical_details">' . __( 'Additional Details ( Phone & address ): ', 'AwsPortfolio' ) . '</label>';
	echo '<input type="text" id="_technical_details" name="_technical_details" value="' . $technical_details . '" class="widefat" /></p>';

	echo '<p><label for="_portfolio_client_recommendation"><b>&nbsp;' . __( 'Recommendation: ', 'AwsPortfolio' ) . '</b></label></br>';

	echo '<textarea id="_clientrecommendation" name="_clientrecommendation" rows="5" cols="90">'.$clientrecommendation.'</textarea>';

	echo '<p><label for="_portfolio_client_recommendation"><b>&nbsp;' . __( 'Display in the website', 'AwsPortfolio' ) . '</b></label>';

	echo '<div class="switch-ajax" id="'.$post->ID.'"></div>
	<div id="crdisp"></div>
	<div class="clear"></div>';

  	$crsts_val = get_post_meta( $post->ID, "_wpp_crstatus", true );
	if( $crsts_val ):
		$switch_status = $crsts_val;
	else:
		$switch_status = "off";
	endif;

	echo '<script type="text/javascript">
	jQuery( "#'.$post->ID.'" ).iphoneSwitch( "'.$switch_status.'", 
	function() {
	var postidval = jQuery( "#'.$post->ID.'" ).attr( "id" );
	clientrec_disp_status(postidval,"on" );
	},
	function() {
	var postidval = jQuery( "#'.$post->ID.'" ).attr( "id" );
	clientrec_disp_status( postidval,"off" );
	},
	{
	switch_on_container_path: "'.plugins_url( 'images/switch_container_off.png' , __FILE__ ).'"
	},"'.plugin_dir_url( __FILE__ ).'" );
	</script>';

	echo '<p><label for="_sortorder">' . __( 'Sort Order: ', 'AwsPortfolio' ) . '</label>';
	echo '<input type="text" id="_sortorder" name="_sortorder" value="" class="code" />';
	echo '<input type="hidden" name="autosave_quickedit_check" value="true" /></p>';

}

/* Add the Portfolio custom fields (called as an argument of the custom post type registration) */
function add_awsprojects_portfolio_metaboxes() {
	add_meta_box( 'awsprojects_portfolio_edit_init', __( 'Project Information', 'AwsPortfolio' ), 'awsprojects_portfolio_edit_init', 'awsprojects', 'normal', 'high' );
}

// Manage Portfolio Types taxonomy counts
function awsprojects_portfolio_cat_taxonomy_count_rec( $post_id ) {

	global $wpdb;

	$postid = wp_is_post_revision( $post_id );

	if ( $postid == false ) {
		$postid = $post_id;
	}

	$wpdb->query( "DELETE FROM $wpdb->term_relationships WHERE object_id = '".$postid."' AND EXISTS ( SELECT 1 FROM	$wpdb->term_taxonomy stt WHERE stt.term_taxonomy_id = $wpdb->term_relationships.term_taxonomy_id AND stt.taxonomy = 'awsprojects_portfolio_cat' )" );

	$wpdb->query( "INSERT INTO $wpdb->term_relationships ( object_id, term_taxonomy_id, term_order ) SELECT	sp.id 'object_id', ( SELECT ssstt.term_taxonomy_id FROM	$wpdb->postmeta spm INNER JOIN $wpdb->terms ssst ON spm.meta_value = ssst.slug INNER JOIN $wpdb->term_taxonomy ssstt ON ssst.term_id = ssstt.term_id AND ssstt.taxonomy = 'awsprojects_portfolio_cat' WHERE spm.meta_key = 'awsprojects_portfolio_cat' AND	spm.post_id = sp.id) 'term_taxonomy_id', 0 'term_order' FROM $wpdb->posts sp WHERE sp.id = '".$postid."' AND sp.post_type = 'awsprojects' AND EXISTS ( SELECT 1 FROM $wpdb->postmeta sspm INNER JOIN $wpdb->terms sssst ON sspm.meta_value = sssst.slug INNER JOIN $wpdb->term_taxonomy sssstt ON sssst.term_id = sssstt.term_id AND sssstt.taxonomy = 'awsprojects_portfolio_cat' WHERE sspm.meta_key = '_awsprojects_portfolio_cat' AND sspm.post_id = sp.id ) AND NOT EXISTS ( SELECT 1 FROM $wpdb->term_relationships str INNER JOIN $wpdb->term_taxonomy stt ON str.term_taxonomy_id = stt.term_taxonomy_id AND stt.taxonomy = 'awsprojects_portfolio_cat' INNER JOIN $wpdb->terms st ON stt.term_id = st.term_id WHERE str.object_id = sp.id )" );

	// Update the AWS Projects Portfolio (Post) counts on the Portfolio Types
	$wpdb->query( "UPDATE $wpdb->term_taxonomy SET count = ( SELECT count( ssp.id ) FROM $wpdb->posts ssp INNER JOIN $wpdb->term_relationships str ON ssp.id = str.object_id 
	WHERE ssp.post_type = 'awsprojects' AND ssp.post_status = 'publish' AND str.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id ) WHERE taxonomy = 'awsprojects_portfolio_cat'" );

}

// Define Portfolio Plugin de-activation process
function awsprojects_portfolio_remove() {

	$deletedata = "false";
	// If the delete data option is set to delete, then delete the Portfolio records and Portfolio Type taxonomy records
	if ( $deletedata == "true" ) {

		// Gather the Portfolios
		$portfolios_to_delete = new WP_Query( array( 'post_type' => 'awsprojects', 'post_status' => 'any', 'orderby' => 'ID', 'order' => 'DESC' ) );

		// Loop through and delete the Portfolios
		if ( $portfolios_to_delete->have_posts() ) {
			while ( $portfolios_to_delete->have_posts() ) : $portfolios_to_delete->the_post();
				wp_delete_post( get_the_id(), true );
			endwhile;
		}

		// Gather the list of Portfolio Types
		$portfolio_type_list = get_terms( 'awsprojects_portfolio_cat', 'hide_empty=0' );

		// Loop thru the types and delete each one, the last will clear the taxonomy
		foreach ( $portfolio_type_list as $portfolio_item ) {
			wp_delete_term( $portfolio_item->term_id, 'awsprojects_portfolio_cat' );
		}

		// Gather the list of Portfolio Tags
		$portfolio_type_list = get_terms( 'awsprojects_portfolio_tag', 'hide_empty=0' );

		// Loop thru the tags and delete each one
		foreach ( $portfolio_type_list as $portfolio_item ) {
			wp_delete_term( $portfolio_item->term_id, 'awsprojects_portfolio_tag' );
		}

	}

}

function awsprojects_portfolio_set_admin_css() {

	$file = plugins_url( 'css/admin.css', __FILE__ );
	wp_register_style( 'aws-project-admin', $file );
	wp_enqueue_style( 'aws-project-admin' );

}

if( !function_exists( 'awsprojects_create_portfolio' ) ) {
function awsprojects_create_portfolio() {

	awsprojects_portfolio_googleapis_jquery();
	add_awsprojects_portfolio_css();
	deregister_wpprojec_plugin_styles();
	add_action( 'wp_print_scripts', 'deregister_wpprojec_plugin_scripts' );

	}
}

function add_awsprojects_portfolio_css() {

	$css = plugins_url( 'css/main-style.css', __FILE__ );
	wp_register_style( 'aws-project-main', $css );
	wp_enqueue_style( 'aws-project-main' );

}

function deregister_wpprojec_plugin_styles() {

	wp_deregister_style( 'thickbox' );

}

function deregister_wpprojec_plugin_scripts() {

	wp_deregister_script( 'thickbox' );

}

if(!function_exists( 'awsprojects_create_single_portfolio' ) ) {
function awsprojects_create_single_portfolio() {

	$js = plugins_url( 'css/single-style.css', __FILE__ );
	$js = awsprojects_portfolio_clear( $js );
	wp_register_style( 'aws-projects-single', $js );
	wp_enqueue_style( 'aws-projects-single' );
	}

}

// Define the Portfolio ShortCode and set defaults for available arguments
function awsprojects_portfolio_loop( $atts, $content = null ) {

	if ( is_admin() ) { return null; }

	global $for;
	global $portfolio_types;
	global $portfolio_output;
	global $num_per_page;
	global $limit_portfolios_returned;
	global $display_the_credit;

	awsprojects_clear_global_entries();

	$max_nav_spread = '';
	$portfolio_type = '';

	extract( shortcode_atts( array(
	'max_nav_spread' => 5,
	'portfolio_type' => '',
	'thickbox' => '',
	'id' => '',
	'per_page' => '',
	'limit' => '',
	'credit' =>''), $atts ) );

	$for = $max_nav_spread;
	$portfolio_types = $portfolio_type;

	if ( !empty( $per_page ) && is_numeric( $per_page ) ) {
		$num_per_page = $per_page;
	}

	if ( !empty( $id ) ) {
		$portfolio_output = '<div id="' . $id . '">';
	}

	if ( !empty( $content ) ) {
		$portfolio_output .= '<div class="awsprojects_portfolio_page_content">' . $content . '</div>';
	}

	if ( !empty( $limit ) && is_numeric( $limit ) ) {
		$limit_portfolios_returned = $limit;
	}

	include ( 'loop-aws-projects-portfolio.php' );

	if ( !empty( $id ) ) {
		$portfolio_output .= '</div>';
	}

	return $portfolio_output;

}

// Clear out the shortcode values otherwise they get re-used if more than one shortcode is used per page
function awsprojects_clear_global_entries() {

	global $wp_query;
	global $for;
	global $portfolio_types;
	global $click_behavior;
	global $portfolio_output;
	global $num_per_page;
	global $limit_portfolios_returned;
	global $display_the_credit;

	$wp_query->query_vars['portfoliotype'] = '';
	$for = '';
	$portfolio_types = '';
	$click_behavior = '';
	$portfolio_output = '';
	$num_per_page = '';
	$limit_portfolios_returned = '';
	$display_the_credit = '';

}

// Password validate FUNCTION
function aws_validate_password() {

	$aws_password = trim( $_POST['wpppcode'] );
	$aws_postid = $_POST['wpppid'];
	if( !empty( $aws_password ) && !empty( $aws_postid ) ):
	$db_password = get_post_meta( $aws_postid , "_wpp_passcode", true );
	$db_passvalue = ( $db_password == "" ) ? 0 : $db_password;
	if( $aws_password  == $db_passvalue ) {
	$message = "spcode";
	} else {
	$message = "fpcode";
	}
	else:
	$message = "empcode";
	endif;
	echo $message;

	die();

}

function aws_command_script_enqueuer() {

	wp_register_script( 'aws-command-script', WP_PLUGIN_URL.'/aws-projects-portfolio/scripts/aws-script.js', array( 'jquery' ) );
	wp_localize_script( 'aws-command-script', 'aws_projects_lang', aws_projects_portfolio_localize_vars() );
	wp_enqueue_script( 'aws-command-script' );
	// Make the ajaxurl var available to the above script
	wp_localize_script( 'aws-command-script', 'AwsAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	wp_register_script( 'onoff-script', WP_PLUGIN_URL.'/aws-projects-portfolio/scripts/on-off-script.js', array( 'jquery' ) );
	wp_enqueue_script( 'onoff-script' );

}

add_action( 'init', 'aws_command_script_enqueuer' );

add_action( 'wp_ajax_crpcode_nonce', 'aws_validate_password' );
add_action( 'wp_ajax_nopriv_crpcode_nonce', 'aws_validate_password' );

// Enqueue the necessary scripts for the contact form
function aws_add_contact_form_script() {

	if (is_single() && awsprojects_is_post_type( 'awsprojects' ) ) {
	wp_register_script( 'jquery-validator', WP_PLUGIN_URL.'/aws-projects-portfolio/scripts/jquery.tools.min.js', array( 'jquery' ), '1.2.5', true );
	wp_enqueue_script( 'jquery-validator' );

	wp_register_script( 'aws-contact-form', WP_PLUGIN_URL.'/aws-projects-portfolio/scripts/contact-form.js', array( 'jquery', 'jquery-validator' ), AWS_PROJECTS_VERSION, true );
	wp_enqueue_script( 'aws-contact-form' );
	}
	else {
		return false;
	}

}
add_action( 'wp_print_scripts', 'aws_add_contact_form_script');

// Localize vars aws-script.js
function aws_projects_portfolio_localize_vars() {

return array(
        'PleaseWait' => __( 'Please wait....', 'AwsPortfolio' ),
        'PasscodeVerified' => __( 'Password was verified', 'AwsPortfolio' ),
	'InvalidPasscode' => __( 'Invalid password! Please try again.', 'AwsPortfolio' ),
	'EnterPasscode' => __( 'Please enter your password.', 'AwsPortfolio' ),
	'Success' => __( 'Successfully added! Thank you.', 'AwsPortfolio' ),
	'Error' => __( 'ERROR: Please try again.', 'AwsPortfolio' ),
	'PleaseFillIn' => __( 'Please fill in the recommendation field.', 'AwsPortfolio' )
    );

} // End localize_vars

function aws_client_submit_mail() {

	$aws_crcontent = trim( $_POST['crboxcontent'] );
	$crpostid = $_POST['crpid'];
	$html_text = get_option( 'html_or_text_emails' );
	$client_name = get_post_meta( $crpostid, "_clientname", true );
	$client_email = get_post_meta( $crpostid, "_clientemail", true );
	$post_title = get_the_title( $crpostid );
	$editlink = admin_url( 'post.php?post='.$crpostid.'&action=edit' );
	$awsplink = get_permalink( $crpostid );
	$adminemail = get_bloginfo( 'admin_email' );

	$subject = '' . __( 'New Client`s Recommendation Received From', 'AwsPortfolio' ) . ' - '.$client_name.'';

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: '.$html_text.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.get_bloginfo( 'name' ).'<'.get_bloginfo( 'admin_email' ).'>' . "\r\n";
	$headers[] = 'Reply-To: '.$client_name.' <'.$client_email.'>';

	$aws_received_message  = get_option( 'aws_received_message' );

	if( $aws_received_message != '' ) {

	$content  = '' . __( 'Hello Admin,', 'AwsPortfolio' ) . '<br/><br/>';
	$content  = get_option('aws_received_message').'<br/><br/>';
	$content .= '' . __( 'Project Title:', 'AwsPortfolio' ) . ' '.$post_title.'<br/></br>';
	$content .= '' . __( '<strong>Recommendation</strong>:', 'AwsPortfolio' ) . '<br/>'.stripslashes( $aws_crcontent ).'<br/></br>';
	$content .= '' . __( 'Please click on the following link to approve this recommendation', 'AwsPortfolio' ) . ' </br><a style="text-decoration: none;" href="'.$editlink.'">'.$editlink.'</a><br/><br/><br/>';
	$content .= '' . __( 'Thanks,', 'AwsPortfolio' ) . '<br/><strong>' . get_bloginfo( 'name' ) . '</strong><br/><a style="text-decoration: none;" href="' . get_bloginfo( 'wpurl' ) . '">' . get_bloginfo( 'wpurl' ) . '</a><br/><br/>';

	} else {
	$content  = '' . __( 'Hello Admin,', 'AwsPortfolio' ) . '<br/><br/>';
	$content .= '' . __( 'New recommendation received for', 'AwsPortfolio' ) . ' <strong>'.$post_title.'</strong> ' . __( 'project.', 'AwsPortfolio' ) . '<br/><a href="'.$awsplink.'">'.$awsplink.'</a><br/><br/>';
	$content .= '<strong>' . __( 'Recommendation:', 'AwsPortfolio' ) . '</strong><br/>'.stripslashes( $aws_crcontent ).'<br/><br/>';
	$content .= '' . __( 'Please click on the following link to approve this recommendation', 'AwsPortfolio' ) . '</br><a style="text-decoration: none;" href="'.$editlink.'">'.$editlink.'</a><br/><br/><br/>';
	$content .= '' . __( 'Thanks,', 'AwsPortfolio' ) . '<br/><strong>' . get_bloginfo( 'name' ) . '</strong><br/><a style="text-decoration: none;" href="' . get_bloginfo( 'wpurl' ) . '">' . get_bloginfo( 'wpurl' ) . '</a><br/><br/>';

	}

	if( !empty( $aws_crcontent ) && !empty( $crpostid ) ):
	$dbcontent = update_post_meta( $crpostid , "_clientrecommendation", $aws_crcontent );
	if( $dbcontent ) {
	wp_mail( $adminemail, $subject, $content, $headers );
	$message = "scrcont";
	} else {
	$message = "fcrcont";
	}
	else:
	$message = "empcrcont";
	endif;
	echo $message;

	die();

}

// Password generator
function aws_password_generator( $length=10 ) {

	$password = '';
	list( $usec, $sec ) = explode( ' ', microtime() );
	mt_srand( (float) $sec + ( (float) $usec * 100000 ) );

   	$inputs = array_merge( range( 'z', 'a' ), range( 0, 9 ), range( 'A', 'Z' ) );

   	for( $i=0; $i<$length; $i++ ) {
		$password .= $inputs{mt_rand( 0, 61 )};
	}
		return $password;

}

function aws_client_testimonial_sent() {

	global $current_screen;

	$ptrashed = "";
	$puntrashed = "";

	if( isset( $_REQUEST['trashed'] ) ):
		$ptrashed = $_REQUEST['trashed'];
	endif;

	if( isset( $_REQUEST['untrashed'] ) ):
		$puntrashed = $_REQUEST['untrashed'];
	endif;

	if ( 'awsprojects' == $current_screen->post_type && !$ptrashed && !$puntrashed ) {
	_e( "<div class='updated'><p><strong>Client recommendation request has been sent!</strong></p></div>", "AwsPortfolio" );
	}

}

// Testimonial action starts
if( isset( $_GET['wppaction'] ) && isset( $_GET['post'] ) ) {

	$currentaws_id = $_REQUEST['post'];

	if( $_REQUEST['wppaction'] == 1 && !empty( $_GET['post'] ) ):

	$aws_pword = aws_password_generator(); // Random passcode value
	$prv_pword = get_post_meta( $currentaws_id, '_wpp_passcode', true ); // Existing passcode value

	if ( $prv_pword != '' ) { // If the custom field already has a value
	$ext_password = get_post_meta( $currentaws_id, '_wpp_passcode', true );
	update_post_meta( $currentaws_id, '_wpp_passcode', $ext_password );
	} else { // If the custom field doesn't have a value
	add_post_meta( $currentaws_id, '_wpp_passcode', $aws_pword );
	$send_password = $aws_pword;
	}

	add_action( 'plugins_loaded', 'aws_client_message', 11 );
	add_action( 'admin_notices', 'aws_client_testimonial_sent' );

	endif;

}

function aws_client_message() {
	global $wpdb;

	$currentaws_id = $_REQUEST['post'];

	$rewrite_slug = get_option( 'aws_rewrite_slug' );

	$ctermsql = "SELECT st.slug FROM $wpdb->posts sp, $wpdb->term_relationships str INNER JOIN $wpdb->term_taxonomy stt ON str.term_taxonomy_id = stt.term_taxonomy_id AND stt.taxonomy = 'awsprojects_portfolio_cat' INNER JOIN $wpdb->terms st ON stt.term_id = st.term_id WHERE str.object_id = sp.id AND sp.ID ='".$_REQUEST['post']."' limit 0,1";

	$cterm_data = $wpdb->get_var( $ctermsql );

	if( $cterm_data != '' ):

	$catslug = $cterm_data;

	else:

	$catslug = get_option( 'aws_rewrite_slug' );

	endif;

	$html_text = get_option( 'html_or_text_emails' );
	$clientname = get_post_meta( $currentaws_id, '_clientname', true );
	$clientemail = get_post_meta( $currentaws_id, '_clientemail', true );
	$send_password = get_post_meta( $currentaws_id, '_wpp_passcode', true );
	$postlink = site_url( '/'.$rewrite_slug.'/'.$catslug.'/'.basename( get_permalink( $currentaws_id ) ).'' );

	$subject = '' . __( 'Recommendation Request', 'AwsPortfolio' ) . ' - '.get_bloginfo( 'name' ).'';

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: '.$html_text.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.get_bloginfo( 'name' ).' <'.get_bloginfo( 'admin_email' ).'>' . "\r\n";
	$headers[] = 'Reply-To: '.get_bloginfo( 'name' ).' <'.get_bloginfo( 'admin_email' ).'>';

	$aws_request_message = get_option( 'aws_request_message' );

	if( $aws_request_message != '' ) {
	$content  = '' . __( 'Hello', 'AwsPortfolio' ) . ' '.$clientname.',<br/><br/>';
	$content .= get_option( 'aws_request_message' ).'<br/><br/>';
	$content .= '' . __( 'Please click on the following link to submit your testimonial', 'AwsPortfolio' ) . '<br/><a style="text-decoration: none;" href="'.$postlink.'">'.$postlink.'</a></br></br>';
	$content .= '' . __( 'Use the Password:', 'AwsPortfolio' ) . ' <strong style="color:#009900;">'.$send_password .'</strong><br/><br/><br/>';
	$content .= '' . __( 'Thanks,', 'AwsPortfolio' ) . '<br/><strong>' . get_bloginfo( 'name' ) . '</strong><br/><a style="text-decoration: none;" href="' . get_bloginfo( 'wpurl' ) . '">' . get_bloginfo( 'wpurl' ) . '</a><br/><br/>';

	} else {

	$content  = '' . __( 'Hello', 'AwsPortfolio' ) . ' '.$clientname.',<br/><br/>';
	$content .= '' . __( 'Thanks for offering me the project, it was nice working with you on this project as per your request.', 'AwsPortfolio' ) . '<br/><br/>';
	$content .= '' . __( 'So I would request you to provide us with a testimonial on our portfolio page for the same by clicking on the link below', 'AwsPortfolio' ) . '<br/><a style="text-decoration: none;" href="'.$postlink.'">'.$postlink.'</a><br/><br/>';
	$content .= '' . __( 'Use the Password:', 'AwsPortfolio' ) . ' <strong style="color:#009900;">'.$send_password .' </strong> ' . __( 'to provide the testimonial.', 'AwsPortfolio' ) . '<br/><br/><br/>';
	$content .= '' . __( 'Thanks,', 'AwsPortfolio' ) . '<br/><strong>' . get_bloginfo( 'name' ) . '</strong><br/><a style="text-decoration: none;" href="' . get_bloginfo( 'wpurl' ) . '">' . get_bloginfo( 'wpurl' ) . '</a><br/><br/>';
	}

	wp_mail( $clientemail, $subject, $content, $headers );

}

add_action( 'wp_ajax_crcontent_nonce', 'aws_client_submit_mail' );
add_action( 'wp_ajax_nopriv_crcontent_nonce', 'aws_client_submit_mail' );

// Test for whether a hook should be applied or not
function awsprojects_portfolio_apply_hook( $query, $hook ) {
	return (
		// We have query vars
		property_exists( $query, 'query_vars' ) &&
		( array_key_exists( 'post_type', $query->query_vars ) && $query->query_vars['post_type'] == 'awsprojects' )
	);
}

// Add "portfoliotype" into the recognized set of query variables
function awsprojects_portfolio_queryvars( $qvars ) {
	$qvars[] = 'portfoliotype';
	return $qvars;
}

// Status mode - FUNCTION
function aws_non_dstatus() {

	$crstatus = $_POST['crsts'];
	$cr_postid = $_POST['crpid'];
	if( !empty( $crstatus ) && !empty( $cr_postid ) ):
	$crsts_update = update_post_meta( $cr_postid, "_wpp_crstatus", $crstatus );
	if( $crsts_update ) {
	$message = "crstss";
	} else {
	$message = "crstsf";
	}
	endif;
	echo $message;

	die();

}

// Argument the JOIN if a Portfolio Type is part of the search
function awsprojects_portfolio_search_join( $join, $query ) {

	global $wpdb, $wp_query;

	// If the portfolio type has been defined in the search vars
	if ( awsprojects_portfolio_apply_hook( $query, 'join' ) ) {

		// Add the join to the wp_postmeta table for meta records that are of a Portfolio Type
		$join .=  " LEFT OUTER JOIN " . $wpdb->prefix . "postmeta AS port ON ( " . $wpdb->posts . ".ID = port.post_id AND port.meta_key = '_awsprojects_portfolio_cat' ) ";

	}

	return $join;
}

// Argument the WHERE clause if a Portfolio Type is part of the search
function awsprojects_portfolio_search_where( $where, $query ) {

	global $wp_query;

	if ( is_admin() ) { return $where; }

	// If the portfolio type has been defined in the search vars
	if ( awsprojects_portfolio_apply_hook( $query, 'where' ) ) {

		// Clear out our portfolio type buckets
		$IN = "";
		$OUT = "";

		$types = get_query_var( 'portfoliotype' );

		// Place the portfolio types into an array so that it is easier to process them
		$ptypes = explode( ",",$types );

		// Loop through the portfolio array
		foreach ( $ptypes as $value ) {

			// If the portfolio type is not lead by a minus sign then add it to the IN bucket
			if ( substr( $value, 0, 1 ) != '-' ) {
				if ( !empty( $IN ) ) $IN .= ",";
				$IN .= $value;
			} else { // Otherwise, add it to the OUT bucket
				if ( !empty( $OUT ) ) $OUT .= ",";
				$OUT .= substr( $value, 1 );
			}
		}

		// If some of the portfolio types were flagged for inclusion then add an IN() clause
		if ( !empty( $IN ) ) {
			if ( !empty( $where ) ) $where .= " AND ";
			$where .= " port.meta_value IN ( '" . str_replace( ',', "','", $IN ) . "' )";
		}

		// If some of the portfolio types were flagged for exclusion then add a NOT IN() clause
		if ( !empty( $OUT ) ) {
			if ( !empty( $where ) ) $where .= " AND ";
			$where .= " port.meta_value NOT IN ( '" . str_replace( ',', "','", $OUT ) . "' )";
		}

	}

	return $where;
}

add_action( 'wp_ajax_crsts_nonce', 'aws_non_dstatus' );
add_action( 'wp_ajax_nopriv_crsts_nonce', 'aws_non_dstatus' );

// Extend standard WordPress tag cloud to include Portfolio tags
function awsprojects_portfolio_tag_cloud_inc( $args = array() ) {

	$include = "False";

	if ( $include == 'True' ) {

		if ( is_array( $args['taxonomy'] ) ) {
			array_push( $args['taxonomy'], 'awsprojects_portfolio_tag' );
		} else {
			$args['taxonomy'] = array( $args['taxonomy'], 'awsprojects_portfolio_tag' );
		}

	}

	return $args;

}

if ( ! is_admin() ) {
	add_filter( 'widget_tag_cloud_args', 'awsprojects_portfolio_tag_cloud_inc', 90 );
}

function wp_admin_bar_totalcount_item() {

	global $wpdb, $wp_admin_bar;

	if ( ! is_super_admin() || ! is_admin_bar_showing() )
	  return;

	$cr_count = $wpdb->get_var( "SELECT COUNT( * ) AS count FROM {$wpdb->postmeta} pm LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id WHERE ( pm.meta_key = '_clientrecommendation' AND pm.meta_value != '' ) AND p.post_status = 'publish' AND p.post_type = 'awsprojects' ");

	$wp_admin_bar->add_menu( array(
			'id' => 'wp-admin-bar-new-item',
			'title' => '' . __( 'Reviews Received', 'AwsPortfolio' ) . ' (<span style="padding:2px 2px 2px 2px;">'.$cr_count.'</span>)',
			'href' => admin_url().'edit.php?post_type="awsprojects&sfname=_clientrecommendation"',
			'meta' => array(
			'class' => 'menupop', 
			'title' => 'The total number of client recommendations received.' ) )
			);


}
add_action( 'wp_before_admin_bar_render', 'wp_admin_bar_totalcount_item' );

// If we are on a post or a page with the aws-projects-portfolio shortcode in the content then carry off certain actions
function awsprojects_shortcode() {

	$cont = "";

	global $post;

	if ( is_single() || is_page() ) {
		$cont = get_aws_projects_page_pontent( $post->ID );
	}

	// If the aws-projects-portfolio shortcode is within the content take the actions indicated
	if ( strpos( $cont, 'aws-projects-portfolio' ) > 0 ) {
		add_action( 'wp_print_styles', 'awsprojects_create_portfolio' );
	} else {
		if ( $_SERVER["REMOTE_ADDR"] == '127.0.0.1' ) { // Asterisk - when running locally this was needed to avert a non-ending re-direct
			remove_filter( 'template_redirect', 'redirect_canonical' );
		}
		add_filter( 'template_include', 'awsprojects_portfolio_template_include' );
		add_action( 'wp_print_styles', 'awsprojects_create_single_portfolio' );
	}

}

function awsprojects_portfolio_template_include( $inc_file ) {

	if ( get_post_type() == 'awsprojects' ) {
		$inc_file = awsprojects_portfolio_post_templatefile_include( $inc_file );
	}

	return $inc_file;

}

if( !function_exists( 'awsprojects_portfolio_post_templatefile_include' ) ) {
function awsprojects_portfolio_post_templatefile_include( $inc_file ) {

	global $wp_query;

	if ( is_single() ) {
		add_action( 'wp_print_styles', 'awsprojects_create_single_portfolio' );
		$file = get_stylesheet_directory() . '/single-aws-projects-portfolio.php';
		if ( ! file_exists( $file ) ) {
			$file = plugin_dir_path( __FILE__ ) . 'single-aws-projects-portfolio.php';
		}
		if ( file_exists( $file ) ) {
			$inc_file = $file;
		}
	} else {
		$wp_query->is_404 = true;
	}

	return $inc_file;
}
}

if( !function_exists( 'get_aws_projects_page_pontent' ) ) {
function get_aws_projects_page_pontent( $pageId ) {

	if( !is_numeric( $pageId ) ) {
		return;
	}

	global $wpdb;

	$sql_query = 'SELECT DISTINCT * FROM ' . $wpdb->posts . ' WHERE ' . $wpdb->posts . '.ID=' . $pageId;

	$posts = $wpdb->get_results( $sql_query );

	if( !empty( $posts ) ) {

		foreach( $posts as $post ) {

			return nl2br( $post->post_content );

		}
	}

}
}

// Google jquery inclusion
function awsprojects_portfolio_googleapis_jquery() {

	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js' );
	wp_enqueue_script( 'jquery' );

}

// Clear the passed in path up to wp-content as some code and hosting providers don't play nicely with arguments containing http://www
if ( ! function_exists( 'awsprojects_portfolio_clear' ) ) :

function awsprojects_portfolio_clear( $url ) {

	$return = $url;

	$use_full_path = 'True';

	if ( $use_full_path != 'True' ) {

		$pos = strpos( $return, 'wp-content' );

		if ( ! empty( $pos ) ) {
			$return = str_replace( substr( $return, 0, strpos( $return, 'wp-content' ) - 1 ), "", $return );
		}

	}

	return $return;
}

endif;

// Enter property description here, label filter
function customs_tinyMCE_label( $content ) {

	if( $content == '' ) $content = __( 'Enter project requirements or job description here...', 'AwsPortfolio' );

	return $content;

}

// Add custom tinyMCE settings filter
function customs_tinyMCE( $settings ) {

	$settings['setup'] = 'tinyEvent';

	return $settings;

}

// Add custom tinyMCE script and custom tinyMCE initialize content
$awsuri = $_SERVER['REQUEST_URI'];

if ( strstr( $awsuri, 'post_type=awsprojects' ) ) { 

	add_filter( 'tiny_mce_before_init', 'customs_tinyMCE' );
	add_filter( 'the_editor_content', 'customs_tinyMCE_label' );

}

// Build out the navigation elements for paging through the AWS Projects Portfolio pages
function awsprojects_nav_pages( $qryloop, $pageurl, $class ) {

	global $for;
	global $portfolio_output;
	global $navcontrol;
	global $limit_portfolios_returned;

	// Get total number of pages in the query results
	$pages = $qryloop->max_num_pages;
	$legacy = '';
	$top = "";
	$bottom = "";
	if ( $legacy == 'True' ) {
		$top = " top";
		$bottom = " bottom";
	}

	// If the user has set a hard value for the number of portfolios to return in the shortcode
	if ( is_numeric( $limit_portfolios_returned ) ) {
		if ( $limit_portfolios_returned > 0 ) {
			$pages = 1;
		}
	}

	// If there is more than one page of Portfolio query results
	if ( $pages > 1 ) {

		if ( ( $class == "awsprojects_nav_bottom" ) && ( !empty( $navcontrol ) ) ) {
			$portfolio_output .= '<div class="pagination' . $bottom . ' ' . $class . '">' . $navcontrol . '</div>';
			$navcontrol = array();
			return $portfolio_output;
		}

		$paged_1 = $pageurl;

		if ( strpos( $pageurl, "?page_id=" ) > 0 ) {
			$paged = $pageurl . "&paged=";
			$paged_end = "";
		} else {
			$paged = $pageurl . "/page/";
			$paged_end = "/";
		}		

		// Get current page number
		intval( get_query_var( 'paged' ) ) == 0 ? $curpage = 1 : $curpage = intval( get_query_var( 'paged' ) );

		// Determine the starting page number of the nav control

		// Figure out where to start and end the nav control numbering as well as what arrow elements we need on each end, if any
		$start = $curpage - round( ( $for/2 ), 0 ) + 1;
		if ( ( $start + $for ) > $pages ) { $start = $pages - $for + 1; }
		if ( $start < 1 ) { $start = 1; }
		if ( ( $start + $for ) > $pages ) { $for = $pages - $start + 1; }
		$before = 0;
		if ( $start > 2 ) {
			$before = 2;
		} elseif ( $start > 1 ) {
			$before = 1;
		}
		$after = $pages - ( $start + $for - 1 );
		if ( $after > 2 ) {
			$after = 2;
		} elseif ( $after < 0 ) {
			$after = 0;
		}

		// Now build out the navigation page control elements
		$nav = '<ul class="aws-projects-nav">';
		if ( $before == 1 ) {
			$nav .= '<li><a href="' . $paged . ( $start - 1 ) . $paged_end . '">&lt;</a></li>';
		} elseif ( $before == 2 ) {
			$nav .= '<li><a href="' . $paged_1 . '">&laquo;</a></li>';
			$nav .= '<li><a href="' . $paged . ( $start - 1 ) . $paged_end . '">&lt;</a></li>';
		}
		for ( $i=$start;$i<=( $start+$for-1 );$i++ ) {
			if ( $i == 1 ) {
				$pagenav = $paged_1;
			} else {
				$pagenav = $paged . $i . $paged_end;
			}
			if ( $curpage!=$i ) {
				$nav .= '<li><a href="' . $pagenav . '"';
			} else {
				$nav .= '<li class="selected"><a href="' . $pagenav . '" class="selected"';
			}
			$nav .= '>' . $i . '</a></li>';
		}
		if ( $after == 1 ) {
			$nav .= '<li><a href="' . $paged . ( $start + $for ) . $paged_end . '">&gt;</a></li>';
		} elseif ( $after == 2 ) {
			$nav .= '<li><a href="' . $paged . ( $start + $for ) . $paged_end . '">&gt;</a></li>';
			$nav .= '<li><a href="' . $paged . $pages . $paged_end . '">&raquo;</a></li>';
		}
		$nav .= '</ul>';

		$portfolio_output .= '<div class="pagination' . $top . ' ' . $class . '">' . $nav . '</div>';
		$portfolio_output .= '<div class="clear"></div>';

		if ( $class == "awsprojects_nav_top" ) {
			$navcontrol = $nav;
		}

	}

	return $portfolio_output;
}

add_filter( 'parse_query', 'awsprojects_portfolio_search_filter' );
add_action( 'restrict_manage_posts', 'awsprojects_portfolio_search_filter_downlist' );

function awsprojects_portfolio_search_filter( $query ) {

	global $pagenow;
	if ( is_admin() && $pagenow == 'edit.php' && isset( $_GET['sfname'] ) && $_GET['sfname'] != '' ) {
        $query->set( 'meta_key', $_GET['sfname'] );
	if ( isset( $_GET['sfvalue'] ) && $_GET['sfvalue'] != '' )
	$query->set( 'meta_value', $_GET['sfvalue'] );
	$query->set( 'meta_compare', 'LIKE' );
	}

}

function awsprojects_portfolio_search_filter_downlist() {	
	$ptype = isset( $_REQUEST['post_type'] )? $_REQUEST['post_type'] : '';
	if( $ptype == 'awsprojects' ):

	$drop_down_array = array( 
			array( '_clientname' => __( 'Client Name', 'AwsPortfolio' ) ), 
			array( '_clientemail' => __( 'Client Email', 'AwsPortfolio' ) ), 
			array( '_siteurl' => __( 'Website URL', 'AwsPortfolio' ) ), 
			array( '_clientrecommendation' => __( 'Recommendations', 'AwsPortfolio' ) ) 
			);	
?>

<select name="sfname">
<option value=""><?php _e( 'Filter by Client information', 'AwsPortfolio' ); ?></option>

<?php
	$current_item = isset( $_GET['sfname'] )? $_GET['sfname'] : '';
	$current_txtval = isset( $_GET['sfvalue'] )? $_GET['sfvalue'] : '';

	for ( $dfrow = 0; $dfrow < count( $drop_down_array ); $dfrow++ ) {
		foreach ( $drop_down_array[$dfrow] as $dpdown_key => $dpdown_val ) {

		printf( '<option value="%s"%s>%s</option>', $dpdown_key, $dpdown_key == $current_item ? ' selected="selected"' : '', $dpdown_val );

		}

	} // endfor

?>
</select> <?php _e( 'Value:', 'AwsPortfolio' ); ?><input type="text" name="sfvalue" value="<?php echo $current_txtval; ?>" />

<?php
endif;

}

function awsprojects_is_post_type( $type ) {
	global $wp_query;
	if( $type == get_post_type( $wp_query->post->ID ) ) return true;
	return false;
}

add_action('template_redirect', 'awsprojects_add_scripts');
 
function awsprojects_add_scripts() {
	if (is_single() && awsprojects_is_post_type( 'awsprojects' ) ) {
		add_thickbox(); 
	}
}

// Get page url based on shortcode
function awsprojects_get_page_url_by_shortcode( $shortcode ) {
	global $wpdb;

	$url = '';

	$sql = 'SELECT ID
		FROM ' . $wpdb->posts . '
		WHERE
			post_type = "page"
			AND post_status = "publish"
			AND post_content LIKE "%' . $shortcode . '%"';

	if ( $id = $wpdb->get_var( $sql ) ) {
		$url = get_permalink( $id );
	}

	return $url;
}

// Delete all attachments when project is edited from frontend
function awsprojects_delete_attachment( $post_id ) {

// Don't do unless post type is 'awsprojects'.
	$post_type = get_post_type( $post_id );
	if ( $post_type != 'awsprojects' )
		return;

	if( !isset( $post_id ) )
		return;

	elseif( $post_id == 0 )
		return;

	elseif( is_array( $post_id ) )
		return;

	else {

        $attachments = get_posts( array(
            'post_type'      => 'attachment',
            'posts_per_page' => -1,
            'post_status'    => 'any',
            'post_parent'    => $post_id
        ) );

        foreach ( $attachments as $attachment ) {
            $file_path = get_attached_file( $attachment->ID );
            if ( $file_path ) unlink( $file_path );
            if ( false === wp_delete_attachment( $attachment->ID ) ) {
                // Log failure to delete attachment.
            }
        }
    }

}