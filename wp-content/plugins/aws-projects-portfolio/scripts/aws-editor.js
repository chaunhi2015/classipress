function tinyEvent(ed){
	ed.onClick.add(function(ed, e){
		var content = tinyMCE.activeEditor.getContent();
		if(content.match(/Enter project requirements or job description here.../i)){
			ed.setContent('');
			ed.selection.collapse();
		} 
    });
}