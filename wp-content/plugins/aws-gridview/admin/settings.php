<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


function gridview_settings() {
?>

<div class="wrap">

<h2><?php _e( 'AWS GridView Settings', 'AwsGridview' ); ?></h2>

<?php if( isset( $_POST['action'] ) && $_POST['action'] == 'update' && $_POST['option_page'] == 'gridview-settings' ) {

	$layout_option = isset( $_POST['aws_gridview_layout_option'] ) ? esc_attr( $_POST['aws_gridview_layout_option'] ) : '';
	$featured_ribbon = isset( $_POST['aws_gridview_featured_ribbon'] ) ? esc_attr( $_POST['aws_gridview_featured_ribbon'] ) : '';
	$sold_ribbon = isset( $_POST['aws_gridview_sold_ribbon'] ) ? esc_attr( $_POST['aws_gridview_sold_ribbon'] ) : '';

	update_option( 'aws_gridview_layout_option', $layout_option );
	update_option( 'aws_gridview_featured_ribbon', $featured_ribbon );
	update_option( 'aws_gridview_sold_ribbon', $sold_ribbon );

	echo '<div class="updated"><p><strong>' . __( 'Gridview settings was updated..','AwsGridview' ) . '</strong></p></div>';
}
?>

<form method="post" action="">

	<?php settings_fields( 'gridview-settings'); ?>

<table class="form-table intro-text">
    <tbody>

        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( 'Welcome to the release version of AWS GridView. If you can you read this text then the plugin has installed correctly, and it is now time to set the few settings below.', 'AwsGridview' ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( 'For our next release we will have in place options to, change colors and set some custom style/CSS, add more tabs, and option to add additional text/information boxes to the frontpage.', 'AwsGridview' ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( '<strong><a style="text-decoration: none;" href="http://www.arctic-websolutions.com/ads/cp-auction-social-sharing/" target="_blank">CP Auction</a></strong> - If you have our CP Auction plugin installed and active, you need to visit the <a style="text-decoration: none;" href="/wp-admin/admin.php?page=cp-auction&tab=frontpage">settings page</a> and set up the different functions you want in your new custom frontpage. Activate the different menu tabs and any other features you want to activate. You can now forget everything that you previously had to perform in the sense of copying and moving files, your new frontpage will be created automatically.', 'AwsGridview' ); ?></td>
        </tr>
        <tr valign="top">
        <td colspan="2" class="introtext"><?php _e( '<strong>Note:</strong> As this plugin is based on our AWS Grandchild Plugin you will have the same great options available, this means that you can easily edit your child themes according to these FAQs, <strong><a style="text-decoration: none;" href="http://www.arctic-websolutions.com/grandchild-plugin-faq/" target="_blank">read more</a></strong>, the possibilities are limitless.', 'AwsGridview' ); ?></td>
        </tr>

    </tbody>
</table>

<div class="dotted"><h3><?php _e( 'Settings', 'AwsGridview' ); ?></h3></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Grid or Listview:', 'AwsGridview'); ?></th>
    	<td><select name="aws_gridview_layout_option" id="aws_gridview_layout_option" style="min-width: 100px;">
	<option value="" <?php if(get_option('aws_gridview_layout_option') == "") echo 'selected="selected"'; ?>><?php _e('Listview', 'AwsGridview'); ?></option>
	<option value="gridview" <?php if(get_option('aws_gridview_layout_option') == "gridview") echo 'selected="selected"'; ?>><?php _e('Gridview', 'AwsGridview'); ?></option>
	</select> <?php _e('Enable the default view, grid or listview.', 'AwsGridview'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Featured Ad Ribbon:', 'AwsGridview'); ?></th>
    	<td><select name="aws_gridview_featured_ribbon" id="aws_gridview_featured_ribbon" style="min-width: 100px;">
	<option value="" <?php if(get_option('aws_gridview_featured_ribbon') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'AwsGridview'); ?></option>
	<option value="yes" <?php if(get_option('aws_gridview_featured_ribbon') == "yes") echo 'selected="selected"'; ?>><?php _e('Enabled', 'AwsGridview'); ?></option>
	</select> <?php _e('Enable the featured ad ribbon.', 'AwsGridview'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Sold Ad Ribbon:', 'AwsGridview'); ?></th>
    	<td><select name="aws_gridview_sold_ribbon" id="aws_gridview_sold_ribbon" style="min-width: 100px;">
	<option value="" <?php if(get_option('aws_gridview_sold_ribbon') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'AwsGridview'); ?></option>
	<option value="yes" <?php if(get_option('aws_gridview_sold_ribbon') == "yes") echo 'selected="selected"'; ?>><?php _e('Enabled', 'AwsGridview'); ?></option>
	</select> <?php _e('Enable the sold ad ribbon.', 'AwsGridview'); ?></td>
    </tr>

    </tbody>
</table>

   <div align="center"><?php submit_button(); ?></div>

</form>
</div>
<?php }