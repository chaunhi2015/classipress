Plugin Name: AWS GridView
Description: The very best and most convenient way to add gridview into ClassiPress theme.
Plugin URI: http://www.arctic-websolutions.com
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com
Version: 1.0.1

---------------------------------------------------------------------------------------------------

=== Installation ===
 - Download AWS GridView.
 - Extract the downloaded .zip file. (Not necessary if filename dosen`t say so, ie. UNZIP)
 - Login to your websites FTP and navigate to the "wp-contents/plugins/" folder.
 - Upload the resulting "aws-gridview.zip" or "aws-gridview-X.X.X.zip" folder to the websites plugin folder.
 - Go to your websites Dashboard, access the "plugins" section on the left hand menu.
 - Locate "AWS GridView" in the plugin list and click "activate".

OR you can use WP plugin installer, just extract the downloaded .zip file and upload the "aws-gridview-X.X.X.zip".

That’s it!

=== How to use the gridview plugin ===

Visit your ClassiPress -> Grid View, configure any options as desired. That’s it!

If you have our CP Auction plugin installed and active, you need to visit the settings page and set up the different functions
you want in your new custom frontpage. Activate the different menu tabs and any other features you want to activate.

You can now forget everything that you previously had to perform in the sense of copying and moving files, your new frontpage
will be created automatically.

Note: As this plugin is based on our AWS Grandchild Plugin you will have the same great options available, this means that you
can easily edit your child themes according to these FAQs, read more, the possibilities are limitless.