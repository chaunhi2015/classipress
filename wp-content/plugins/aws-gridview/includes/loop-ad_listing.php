<?php
/**
 * Main loop for displaying ads
 *
 * @package ClassiPress
 * @author AppThemes
 *
 */
	global $post, $cp_options;
?>

<?php appthemes_before_loop(); ?>

	<div id="adlisting">

	<?php if ( have_posts() ) : ?>

	<?php  $counter = 0; ?>
	<?php while ( have_posts() ) : the_post(); ?>

	<?php appthemes_before_post(); ?>

	<?php if ( $counter % 3 == 2 ) { ?>
	<div class="post-block-out pbo <?php echo 'thirditem'; ?> <?php cp_display_style( 'featured' ); ?>">
	<?php } else { ?>
	<div class="post-block-out pbo <?php cp_display_style( 'featured' ); ?>">
	<?php } ?>

			<?php if ( is_sticky() && get_option( 'aws_gridview_featured_ribbon' ) == "yes" ) { ?>
			<span class="featured-aws"></span>
			<?php } ?>

			<?php if ( get_post_meta( $post->ID, 'cp_ad_sold', true ) == "yes" && get_option( 'aws_gridview_sold_ribbon' ) == "yes" ) { ?>
			<span class="sold-aws"></span>
			<?php } ?>

	<?php awsolutions_gridview_adtype_hook(); ?>

		<div class="post-block">

		<div class="post-left">

		<?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?>

                </div>

                <div class="<?php cp_display_style( array( 'ad_images', 'ad_class' ) ); ?>">

		<?php appthemes_before_post_title(); ?>

	<h3><a title="<?php echo get_the_title(); ?>" href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= 30 ) echo mb_substr( get_the_title(), 0, 30 ) . '...'; else the_title(); ?></a></h3>

		<div class="clr"></div>

		<?php appthemes_after_post_title(); ?>

		<div class="clr"></div>

		<?php appthemes_before_post_content(); ?>

		<?php if ( get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ) == "normal" ) { ?>
		<p class="post-desc"><?php echo substr( cp_get_content_preview( 130 ), 0, 130 ); ?></p>
		<?php } else { ?>
		<p class="post-desc"><?php echo substr( cp_get_content_preview( 210 ), 0, 210 ); ?></p>
		<?php } ?>

		<?php if ( get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ) == "normal" ) { ?>
		<p class="grid-desc"><?php echo substr( cp_get_content_preview( 60 ), 0, 60 ); ?></p>
		<?php } else { ?>
		<p class="grid-desc"><?php echo substr( cp_get_content_preview( 110 ), 0, 110 ); ?></p>
		<?php } ?>

		<?php appthemes_after_post_content(); ?>

		<div class="clr"></div>

		</div>
        
		<div class="clr"></div>

		</div><!-- /post-block -->

	</div><!-- /post-block-out -->

	<?php appthemes_after_post(); ?>

	<?php $counter++;  ?>

	<?php endwhile; ?>

	</div>

	<div class="clr"></div>

	<?php appthemes_after_endwhile(); ?>

	<?php else: ?>

	</div>

	<?php appthemes_loop_else(); ?>

	<?php endif; ?>

<?php appthemes_after_loop(); ?>

<?php wp_reset_query(); ?>