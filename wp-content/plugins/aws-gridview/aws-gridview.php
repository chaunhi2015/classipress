<?php
/*
Plugin Name: AWS GridView
Description: The very best and most convenient way to add gridview into ClassiPress theme.
Plugin URI: https://www.arctic-websolutions.com
Author: Rune Kristoffersen
Author URI: https://www.arctic-websolutions.com
Version: 1.0.1
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


// Define the root directory path of this plugin
if ( ! defined( 'AWS_GV_PLUGIN_DIR' ) ) {
	define( 'AWS_GV_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}
if ( ! defined( 'AWS_GV_TEXTDOMAIN' ) ) {
	define( 'AWS_GV_TEXTDOMAIN', 'AwsGridview' );
}

	// Load the custom functions and admin settings
	include_once( AWS_GV_PLUGIN_DIR .'/includes/functions.php' );
	include_once( AWS_GV_PLUGIN_DIR .'/admin/settings.php' );

function awsolutions_gridview_activate() {

	update_option( 'aws_gridview_installed', 'yes' );

}
register_activation_hook( __FILE__, 'awsolutions_gridview_activate' );

function awsolutions_gridview_deactivate() {

	update_option( 'aws_gridview_installed', 'no' );

}
register_deactivation_hook( __FILE__, 'awsolutions_gridview_deactivate' );

function awsolutions_gridview_load_language() {
	load_plugin_textdomain( 'AwsGridview', false, AWS_GV_PLUGIN_DIR . '/languages/' );
}
add_action( 'plugins_loaded', 'awsolutions_gridview_load_language' );

// Add GridView settings menu item
function awsolutions_gridview_admin_menu() {

	$page = add_submenu_page( 'app-dashboard', __( 'AWS GridView Management', 'AwsGridview' ), __( 'Grid View', 'AwsGridview' ), 'manage_options', 'gridview-settings', 'gridview_settings' );
	
	add_action( 'admin_print_styles-'.$page, 'awsolutions_admin_styles' );

}
add_action( 'appthemes_add_submenu_page', 'awsolutions_gridview_admin_menu' );

// Add plugin Settings link
$plugin = plugin_basename( __FILE__ );

function awsolutions_gridview_settings_link( $links ) {

	$x = str_replace( basename( __FILE__ ),"", plugin_basename( __FILE__ ) );
	$settings_link = '<a href="/wp-admin/admin.php?page=gridview-settings">' . __( 'Settings', 'AwsGridview' ) . '</a>';
	array_unshift( $links, $settings_link );

	return $links;

}
add_filter( 'plugin_action_links_' . $plugin, 'awsolutions_gridview_settings_link' );

// Hook for the ad type ribbons
function awsolutions_gridview_adtype_hook() {
	do_action( 'awsolutions_gridview_adtype_hook' );
}

// Processes the entire ad thumbnail logic within the loop
if ( !function_exists('cp_ad_loop_thumbnail') ) :
	function cp_ad_loop_thumbnail() {
		global $post, $cp_options;

		// go see if any images are associated with the ad
		$image_id = cp_get_featured_image_id($post->ID);

		// set the class based on if the hover preview option is set to "yes"
		$prevclass = ( $cp_options->ad_image_preview ) ? 'preview' : 'nopreview';

		if ( $image_id > 0 ) {

			// get 155x105 v3.0.5+ image size
			$adthumbarray = wp_get_attachment_image( $image_id, 'featured-thumbnail' );

			// grab the large image for onhover preview
			$adlargearray = wp_get_attachment_image_src( $image_id, 'large' );
			$img_large_url_raw = $adlargearray[0];

			// must be a v3.0.5+ created ad
			if ( $adthumbarray ) {
				echo '<a href="'. get_permalink() .'" title="'. the_title_attribute('echo=0') .'" class="'.$prevclass.'" data-rel="'.$img_large_url_raw.'">'.$adthumbarray.'</a>';

			// maybe a v3.0 legacy ad
			} else {
				$adthumblegarray = wp_get_attachment_image_src($image_id, 'thumbnail');
				$img_thumbleg_url_raw = $adthumblegarray[0];
				echo '<a href="'. get_permalink() .'" title="'. the_title_attribute('echo=0') .'" class="'.$prevclass.'" data-rel="'.$img_large_url_raw.'">'.$adthumblegarray.'</a>';
			}

		// no image so return the placeholder thumbnail
		} else {
			$no_image = plugins_url( '/styles/images/no-thumb-140.png', __FILE__ );
			echo '<a href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '"><img class="attachment-medium" alt="" title="" src="'.$no_image.'" /></a>';
		}

	}
endif;

function awsolutions_gridview_image_size() {
	add_image_size( 'featured-thumbnail', 155, 118, true );
	add_image_size( 'ad-thumb', 75, 75, true );
	add_image_size( 'ad-small', 100, 100, true );
	add_image_size( 'ad-medium', 250, 250, true );
}
add_action( 'init', 'awsolutions_gridview_image_size' );

// Enqueue the custom css files
function awsolutions_gridview_styles() {

	wp_enqueue_style( 'aws-gridview-style', plugins_url( '/styles/styles.css', __FILE__ ), array(), '1.0' );

}
add_action( 'wp_enqueue_scripts', 'awsolutions_gridview_styles', 11 );

// Enqueue the admin css files
function awsolutions_admin_styles() {

	wp_enqueue_style( 'aws-grid-admin', plugins_url( '/admin/styles/style.css', __FILE__ ), array(), '1.0' );

}

// Enqueue the custom script files
function awsolutions_gridview_scripts() {

	//wp_enqueue_script( 'aws-gridview', plugins_url( '/js/aws-gridview.js', __FILE__ ), array( 'jquery' ), '1.0' );
	wp_enqueue_script( 'sticky', plugins_url( '/js/jquery.sticky.js', __FILE__ ), array( 'jquery','jquery-ui-tabs','jquery-effects-slide','jquery-effects-blind' ), '3.3' );
	if ( get_option('aws_gridview_layout_option') === 'gridview' ) {
	wp_enqueue_script( 'aws-gridview-script', plugins_url( '/js/gridview.js', __FILE__ ), array( 'jquery' ), '1.0' );
	} else {
	wp_enqueue_script( 'aws-listview-script', plugins_url( '/js/listview.js', __FILE__ ), array( 'jquery' ), '1.0' );
	}

}
add_action( 'wp_enqueue_scripts', 'awsolutions_gridview_scripts', 99 );


// If custom templates exists load them from 'templates' dir.
function awsolutions_gridview_template_loader( $template ) {

	$new_template = basename( $template );
	if ( get_option('cp_auction_installed' ) == "yes" && $new_template == 'tpl-ads-home.php' ) {
	$new_template = 'tpl-ads-home-auction.php';
	}

	if ( file_exists( untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . $new_template ) )
		$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . $new_template;

	return $template;
}
add_filter( 'template_include', 'awsolutions_gridview_template_loader', 11 );

// AWSolutions Get Template Class.
class AWS_GridView_Templater {

	// A Unique Identifier
	protected $plugin_slug;

	// A reference to an instance of this class.
        private static $instance;

	// The array of templates that this plugin tracks.
        protected $templates;

	// Returns an instance of this class.
        public static function get_instance() {

                if( null == self::$instance ) {
                        self::$instance = new AWS_GridView_Templater();
                } 
                	return self::$instance;
        }

	// Initializes the plugin by setting filters and administration functions.
        private function __construct() {

                $this->templates = array();

                // Add a filter to the attributes metabox to inject template into the cache.
                add_filter( 'page_attributes_dropdown_pages_args', array( $this, 'register_project_templates' ) );

                // Add a filter to the save post to inject out template into the page cache
                add_filter( 'wp_insert_post_data', array( $this, 'register_project_templates' ) );

                // Add a filter to the template include to determine if the page has our template assigned and return it's path
                add_filter( 'template_include', array( $this, 'view_project_template') );

                // Add your templates to this array.
                $this->templates = array(
                	'loop-ad_listing.php' => 'Your Template Name',
                	'tpl-your-template2.php' => 'Your Template2 Name'
                );
        } 


	// Adds our template to the pages cache in order to trick WordPress into thinking the template file exists where it doens't really exist.
        public function register_project_templates( $atts ) {

                // Create the key used for the themes cache
                $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

                // Retrieve the cache list. If it doesn't exist, or it's empty prepare an array
                $templates = wp_get_theme()->get_page_templates();
                if ( empty( $templates ) ) {
                        $templates = array();
                }

                // New cache, therefore remove the old one
                wp_cache_delete( $cache_key , 'themes');

                // Now add our template to the list of templates by merging our templates with the existing templates array from the cache.
                $templates = array_merge( $templates, $this->templates );

                // Add the modified cache to allow WordPress to pick it up for listing available templates
                wp_cache_add( $cache_key, $templates, 'themes', 1800 );

                return $atts;

        }

	// Checks if the template is assigned to the page
        public function view_project_template( $template ) {

                global $post;

                if ( $post && !isset( $this->templates[get_post_meta( $post->ID, '_wp_page_template', true )] ) ) {

                        return $template;

                }

                if ( $post ) $file = plugin_dir_path(__FILE__). '/templates/'. get_post_meta( $post->ID, '_wp_page_template', true );

                // Just to be safe, we check if the file exist first
                if( $post && file_exists( $file ) ) {
                        return $file;
                }

                return $template;

        } 

}
add_action( 'plugins_loaded', array( 'AWS_GridView_Templater', 'get_instance' ) );