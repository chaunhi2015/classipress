jQuery(document).ready(function ($) {

    jQuery('.paging').find('a.moretag').attr('href', '');

    var cc = $.cookie('list_grid');
    if (cc == 'g') {
        $('#adlisting,#popular').removeClass('list').addClass('grid');
        $('#grid-views').addClass('grid-active');
    } else if (cc == 'l') {
        $('#adlisting,#popular').removeClass('grid').addClass('list');
        $('#list-views').addClass('list-active');
    }  else {
	$('#adlisting,#popular').addClass('list');
        $('#list-views').addClass('list-active');
	$.cookie('list_grid', 'l', {path: '/', expires: 365 });
    }
    
    jQuery('#grid-views, .grid-views1, .grid-views2, .grid-views3, .grid-views4, .grid-views5, .grid-views6, .grid-views7, .grid-views8, .grid-views9, .grid-views10, .grid-views11, .grid-views12, .grid-views13, .grid-views14').click(function() {
        $(this).siblings().removeClass('list-active');
        $(this).addClass('grid-active');
        $('#adlisting,#popular').fadeOut(300, function() {
            $(this).addClass('grid').fadeIn(300);
            $(this).removeClass('list').fadeIn(300);
            $.cookie('list_grid', 'g', {path: '/', expires: 365 });
        });
        return false;
    });

    jQuery('#list-views, .list-views1, .list-views2, .list-views3, .list-views4, .list-views5, .list-views6, .list-views7, .list-views8, .list-views9, .list-views10, .list-views11, .list-views12, .list-views13, .list-views14').click(function() {
        $(this).siblings().removeClass('grid-active');
        $(this).addClass('list-active');
        $('#adlisting,#popular').fadeOut(300, function() {
            $(this).removeClass('grid').fadeIn(300);
            $(this).addClass('list').fadeIn(300);
            $.cookie('list_grid', 'l', {path: '/', expires: 365 });
        });
        return false;
    });

});