<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


class APP_SimplePay4u extends APP_Boomerang {

	protected $options;


	public function __construct() {
		parent::__construct( 'simplepay4u', array(
			'admin' => __( 'SimplePay4u', AWS_SU ),
			'dropdown' => __( 'SimplePay4u', AWS_SU ),
		) );
	}


	public function create_form( $order, $options ) {

		if ( function_exists('awsolutions_check_receiver') ) {
		$simplepay4u_email = awsolutions_check_receiver( $order, 'simplepay4u' );
		$nocards = awsolutions_check_receiver( $order, 'nocards' );
		}
		if ( empty( $simplepay4u_email ) || $simplepay4u_email == false ) $simplepay4u_email = $options['email'];
		if ( empty( $nocards ) || $nocards == false ) $nocards = $options['nocards'];

		$fields = array(
			APP_SimplePay4u_Fields::MEMBER => $simplepay4u_email,
			APP_SimplePay4u_Fields::ESCROW => 'N',
			APP_SimplePay4u_Fields::ACTION => 'payment',
			APP_SimplePay4u_Fields::PRODUCT => $order->get_description(),
			APP_SimplePay4u_Fields::PRICE => $order->get_total(),

			APP_SimplePay4u_Fields::QUANTITY => '1',
			APP_SimplePay4u_Fields::URETURN => $this->get_return_url( $order ),
			APP_SimplePay4u_Fields::UNOTIFY => $this->get_ipn_url( $order ),
			APP_SimplePay4u_Fields::UCANCEL => site_url( '/' ),

			APP_SimplePay4u_Fields::COMMENTS => '',
			APP_SimplePay4u_Fields::CUSTOMID => $order->get_id().'|'.$options['sandbox'],
			APP_SimplePay4u_Fields::FREECLIENT => 'Y',

			APP_SimplePay4u_Fields::NOCARDS => $nocards,
			APP_SimplePay4u_Fields::GIFTCARDS => 'Y',
			APP_SimplePay4u_Fields::CHARGEFORCARD => 'Y',

			APP_SimplePay4u_Fields::SITE_LOGO => ''

		);

		$form = array(
			'accept-charset' => 'utf-8',
			'action' => $options['sandbox'] == true ? APP_SimplePay4u_Fields::TEST_URL : APP_SimplePay4u_Fields::POST_URL,
			'id' => 'create-listing',
			'method' => 'POST',
			'name' => 'simplepay4u_payform',
		);
		$this->redirect( $form, $fields, __( 'You are now being redirected to SimplePay4u.', AWS_SU ) );

	}


	public function form() {

		$fields = array (
			array(
				'title' => __( 'Email Address:', AWS_SU ),
				'desc' => __( 'Use <strong>touser</strong> as your merchant ID in test environment.', AWS_SU ),
				'tip' => __( 'Enter your SimplePay4u email address.', AWS_SU ),
				'name' => 'email',
				'type' => 'text',
			),
			array(
				'title' => __( 'Allow Card Payments:', AWS_SU ),
				'desc' => __( 'For nocards set to <strong>No</strong> users will only use SimplePay account as payment method to check out.', AWS_SU ),
				'tip' => __( 'For nocards set to <strong>No</strong> users will only use SimplePay account as payment method to check out. If the value is set to <strong>Yes</strong>, there will be card and also SimplePay TM account payment method at check out.', AWS_SU ),
				'type' => 'select',
				'name' => 'nocards',
				'choices' => array( 'Y' => __( 'No', AWS_SU ), 'N' => __( 'Yes', AWS_SU ) )
			),
			array(
				'title' => __( 'Test Mode:', AWS_SU ),
				'desc' => __( 'Use <strong>touser</strong> as your merchant ID in test environment.', AWS_SU ),
				'tip' => __( 'By default SimplePay4u is set to live mode. If you would like to test and see if payments are being processed correctly, check this box to switch to sandbox mode. Once touser is used as your merchant ID, you can use any email and password to make payment. To simulate a successful transaction, use any email and any password to pay for the transaction. You may use your real email in your notification page to get detail of transaction.', AWS_SU ),
				'type' => 'select',
				'name' => 'sandbox',
				'choices' => array( '' => __( 'No', AWS_SU ), 'yes' => __( 'Yes', AWS_SU ) )
			)
		);

		return array(
			array(
				'title' => __( 'SimplePay4u', AWS_SU ),
				'fields' => $fields,
			)
		);

	}


	public function process( $order, $options ) {

		if ( ! $this->is_returning() ) {
			$this->create_form( $order, $options );
		} else {
			//get_template_part( 'order-summary' );
		}

	}


	public function handle_ipn() {

		if ( self::is_valid_ipn_request() ) {
			self::process_ipn();
		}

	}


	protected function is_valid_ipn_request() {

		$referer = $_POST["referer"];
		$transaction_id = $_POST["transaction_id"];
		$val = @json_encode( $_POST );
		// For Logging $val can be used

		if( $referer != "https://simplepay4u.com" || $referer != "https://www.simplepay4u.com" ) { //1st level checking
			return false;
		}

		if( empty( $transaction_id ) ) { //2nd level checking
			return false;
		}

			return true;

	}


	protected function process_ipn() {

		$customid = $_POST["customid"];
		$cust = explode( "|", $customid );
		$order_id = $cust['0'];

		$order = appthemes_get_order( $order_id );
		if ( ! $order ) {
			return false;
		}

		/*** Server side verification. Your server is communicating with Simplepay Server Internally**/
		$simplepay_url = $sandbox ? "http://sandbox.simplepay4u.com/processverify.php" : "https://simplepay4u.com/processverify.php";
		$curldata["cmd"] = "_notify-validate";
		foreach ( $_REQUEST as $key =>  $value ) {
			if ( $key != 'view' && $key != 'layout' ) {
			$value = urlencode ( $value );
			$curldata[$key] = $value;
			}
		}
		$handle = curl_init();
		curl_setopt( $handle, CURLOPT_URL, $simplepay_url );
		curl_setopt( $handle, CURLOPT_POST, 1 );
		curl_setopt( $handle, CURLOPT_POSTFIELDS, $curldata );
		curl_setopt( $handle, CURLOPT_SSL_VERIFYPEER, 0 );
		curl_setopt( $handle, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $handle, CURLOPT_TIMEOUT, 90 );
		$result = curl_exec( $handle );
		curl_close( $handle );

		if( 'VERIFIED' == trim( $result ) ) {
			$order->complete();
		}
		else {
			if ( method_exists( $order, 'pending' ) ) {
				$order->pending();
			}
			else {
				if ( method_exists( $order, 'failed' ) ) {
				$order->failed();
				}
			}
		}

		exit();
	}


	protected function get_ipn_url( $order ) {

		return add_query_arg( array( 'action' => 'simplepay4u_ipn' ), admin_url( 'admin-ajax.php' ) );
	}

}