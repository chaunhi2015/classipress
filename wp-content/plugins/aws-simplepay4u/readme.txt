=== AWS Simplepay4u ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: gateway, simplepay4u, classipress
Requires at least: 3.9.2
Tested up to: 4.0
Stable tag: 1.0.2
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

---------------------------------------------------------------------------------------------------

Allows to use Simplepay4u as payment processor in AppThemes products. 


=== Description ===

This plugin extends AppThemes products with support for Simplepay4u payments. Plugin supports IPN - instant payment notification, which means that gateway communicate with Simplepay4u by using hidden channel without interaction of customer like, ex. click back to website button.


=== Supported languages ===

* English

If you have translated plugin to your native language, please send it to me, and I will attach it to next release.
Email address you will find on [contact page](http://www.arctic-websolutions.com/contact-us/).



=== Installation ===

1. Upload the folder 'simplepay4u.zip' to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Go to "Payments->Settings" menu, activate the gateway and provide configuration data.


=== Changelog ===

== 1.0.2 - 24 October 2014 ==
* Updated IPN listener.
* Added sanbox url to the listener.

== 1.0.1 - 24 October 2014 ==
* Added test mode (sandbox).
* Updated frontend email field to also accept the testuser "touser".

== 1.0.0 - 24 October 2014 ==
* Initial release


=== Upgrade Notice ===


=== Frequently Asked Questions ===

== Plugin doesn't work for me, what to do? ==

Report it with details in our forum, http://www.arctic-websolutions.com/contact-us/