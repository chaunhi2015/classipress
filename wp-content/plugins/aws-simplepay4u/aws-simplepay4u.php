<?php

/**
 * Plugin Name: AWS SimplePay4u
 * Plugin URI: http://www.arctic-websolutions.com/
 * Version: 1.0.2
 * Description: Allows to use SimplePay4u as payment processor in AppThemes products.
 * Author: Rune Kristoffersen
 * Author URI: http://www.arctic-websolutions.com
 * License: GPLv3
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AWS_SU' ) )
	define( 'AWS_SU', 'simplepay4u' );


add_action( 'init', 'simplepay4u_setup' );

// Load localization text-domain
load_plugin_textdomain( AWS_SU, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );


// Check for existence of AppThemes compatible product and register gateway
function simplepay4u_setup() {

	// Check for the right version of Payments Module
	if ( ! class_exists( 'APP_Boomerang' ) || ! current_theme_supports( 'app-payments' ) ) {
	add_action( 'admin_notices', 'simplepay4u_display_version_warning' );
		return;
	}

	require_once( dirname( __FILE__ ) . '/simplepay4u-gateway.php' );
	appthemes_register_gateway( 'APP_SimplePay4u' );

	// Listener IPN channel
	add_action( 'wp_ajax_nopriv_simplepay4u_ipn', array( 'APP_SimplePay4u', 'handle_ipn' ) );
	add_action( 'wp_ajax_simplepay4u_ipn', array( 'APP_SimplePay4u', 'handle_ipn' ) );

}


// Display warning and disable gateway if AppThemes compatible product is not installed
function simplepay4u_display_version_warning() {

	$message = __( 'AppThemes SimplePay4u Payment Gateway could not run.', AWS_SU );

	if ( ! class_exists( 'APP_Boomerang' ) ) {
		$message = __( 'AppThemes SimplePay4u Payment Gateway does not support the current theme. Please update your AppThemes Product to latest version.', AWS_SU );
	}

	if ( ! current_theme_supports( 'app-payments' ) ) {
		$message = __( 'AppThemes SimplePay4u Payment Gateway does not support the current theme. Please use a compatible AppThemes Product.', AWS_SU );
	}

	echo '<div class="error fade"><p>' . $message . '</p></div>';
	deactivate_plugins( plugin_basename( __FILE__ ) );
}


// Checks if the user is returning via a nonce'd url
function simplepay4u_is_returning() {
	return isset( $_GET['_wpnonce'] ) && wp_verify_nonce( $_GET['_wpnonce'], 'simplepay4u' );
}


// Displays summary page when user is returning from payment
function simplepay4u_template_include( $template ) {
	if ( simplepay4u_is_returning() ) {
		return locate_template( 'order-summary.php' );
	}

	return $template;
}
add_filter( 'template_include', 'simplepay4u_template_include', 11, 1 );


// Class with gateway fields constants
class APP_SimplePay4u_Fields {

	const POST_URL = 'https://simplepay4u.com/process.php';
	const TEST_URL = 'http://sandbox.simplepay4u.com/process.php';

	const MEMBER = 'member';
	const ESCROW = 'escrow';
	const ACTION = 'action';

	const PRODUCT = 'product';
	const PRICE = 'price';
	const QUANTITY = 'quantity';

	const URETURN = 'ureturn';
	const UNOTIFY = 'unotify';
	const UCANCEL = 'ucancel';

	const COMMENTS = 'comments';
	const CUSTOMID = 'customid';
	const FREECLIENT = 'freeclient';

	const NOCARDS = 'nocards';
	const GIFTCARDS = 'giftcards';
	const CHARGEFORCARD = 'chargeforcards';

	const SITE_LOGO = 'site_logo';

}