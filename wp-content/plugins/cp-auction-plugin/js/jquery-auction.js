/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/


jQuery(document).ready(function($) {

	jQuery('#list_cp_special_delivery').hide();

   	var special = jQuery('#cp_special_delivery').val();
   	if( special > 0 ) {
   	jQuery('#list_cp_special_delivery').show();
   	}

	jQuery("input:radio[name=cp_shipping_options]").click(function() {
	var value = jQuery(this).val();
	var text = translatable_text_string.ShippingOptions;
	if( value == text ) {
		jQuery('#list_cp_special_delivery').slideToggle("fast");
	} else {
		jQuery('#list_cp_special_delivery').hide();
	}
	});
});

jQuery(document).ready(function($) {

    jQuery(".show-bt").click( function() {
	var id = jQuery(this).attr("id");
        jQuery('.bt-payment-' + id + '').slideToggle("fast");
    });
});

jQuery(document).ready(function($) {
	jQuery("#cp_shipping_country").change( function() {

	var e = document.getElementById("cp_shipping_country");
	var strName = e.options[e.selectedIndex].value;
	var strUser = e.options[e.selectedIndex].text;

	if( strName === '' ) {
   		return false;
   	}
	else {
   		jQuery(".cp_shipping_country").append('<br /><label for="shipto[' + strName + ']"><a title="' + translatable_text_string.FreightCost + '" href="#" tip="' + translatable_text_string.FreightCost + '" tabindex="99"><span class="helpico"></span></a>' + strUser + ':</label> <input tabindex="99" id="shipto[' + strName + ']" name="shipto[' + strName + ']" value="" style="width: 100px;" type="text">');
	}
	});
});

jQuery(document).ready(function($) {

    jQuery(".delete-country").click( function() {
	var id = jQuery(this).attr("id");
        jQuery("#" + id + "").remove();
    });
});

jQuery(document).ready( function($) {
	jQuery(".add-favorites").click( function() {

	jQuery.ajax({
		url : cp_auction_ajax_script.ajaxurl,
		beforeSend: function() {
		jQuery(".notice").remove();
		jQuery(".content_left").prepend("<div class='notice success'><div>" + translatable_text_string.UpdateList + "</div></div>");
		},
		type : "post",
		dataType : "json",
		data : {
			action: 'cp_add_favorites',
                        post_var: jQuery(this).attr("id"),
			},
			success: function(response) {
			if(response.type == "success") {

			jQuery(".notice").remove();
			jQuery(".content_left").removeClass('error');
			jQuery(".content_left").prepend(response.notices);

	 		} else {

			jQuery(".notice").remove();
			jQuery(".content_left").removeClass('success');
			jQuery(".content_left").prepend(response.notices);

			}
		}
	
		})

	})
});

jQuery(document).ready( function($) {
	jQuery(".update-credit-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'update_credit_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".update-escrow-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'update_escrow_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".set-escrow-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'set_escrow_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".update-verification-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'update_verification_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".admin-bump-ads").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'admin_bump_ads',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".update-bumped-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'update_bumped_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".update-featured-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'update_featured_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".delete-complete-purchase").click( function() {
		var retVal = confirm(translatable_text_string.DeleteComplPurchase);
   		if( retVal == true ) {
		var data = {
			action: 'delete_complete_purchase',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location = window.location.href.split("#")[0];});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".trans-delete-pid").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'trans_delete_pid',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".trans-delete-credits").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'trans_delete_credits',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".trans-delete-withdraw").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'trans_delete_withdraw',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".verification-delete-pid").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'verification_delete_pid',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".bumped-delete-pid").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'bumped_delete_pid',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".featured-delete-pid").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'featured_delete_pid',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".update-withdraw-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'update_withdraw_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".add-watchlist").click( function() {

	jQuery.ajax({
		url : cp_auction_ajax_script.ajaxurl,
		beforeSend: function() {
		jQuery(".notice").remove();
		jQuery(".content_left").prepend("<div class='notice success'><div>" + translatable_text_string.UpdateList + "</div></div>");
		},
		type : "post",
		dataType : "json",
		data : {
			action: 'cp_add_watchlist',
                        post_var: jQuery(this).attr("id"),
			},
			success: function(response) {
			if(response.type == "success") {

			jQuery(".notice").remove();
			jQuery(".content_left").removeClass('error');
			jQuery(".content_left").prepend(response.notices);

	 		} else {

			jQuery(".notice").remove();
			jQuery(".content_left").removeClass('success');
			jQuery(".content_left").prepend(response.notices);

			}
		}
	
		})

	})
});

jQuery(document).ready( function($) {
	jQuery(".delete-favorites").click( function() {
		var retVal = confirm(translatable_text_string.DeleteFavorite);
		var post_id = jQuery(this).attr("id");
   		if( retVal == true ) {
	jQuery.ajax({
		url : cp_auction_ajax_script.ajaxurl,
		beforeSend: function() {
		jQuery(".notice").remove();
		jQuery(".content_left").prepend("<div class='notice success'><div>" + translatable_text_string.UpdateList + "</div></div>");
		},
		type : "post",
		dataType : "json",
		data : {
			action: 'cp_del_favorites',
                        post_var: jQuery(this).attr("id"),
			},
			success: function(response) {
			if(response.type == "success") {

			jQuery(".notice").remove();
			jQuery("#" + post_id + "").remove();
			jQuery(".content_left").removeClass('error');
			jQuery(".content_left").prepend(response.notices);

	 		} else {

			jQuery(".notice").remove();
			jQuery(".content_left").removeClass('success');
			jQuery(".content_left").prepend(response.notices);

			}
		}
	
		})
	 	} else {
			return false;
		}
	})
});

jQuery(document).ready( function($) {
	jQuery(".delete-auctions").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'delete_auctions',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".upd-auction-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'upd_auction_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".update-direct-status").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'update_direct_status',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".delete-watchlist").click( function() {
		var retVal = confirm(translatable_text_string.DeleteWatchlist);
		var post_id = jQuery(this).attr("id");
   		if( retVal == true ) {
	jQuery.ajax({
		url : cp_auction_ajax_script.ajaxurl,
		beforeSend: function() {
		jQuery(".notice").remove();
		jQuery(".content_left").prepend("<div class='notice success'><div>" + translatable_text_string.UpdateList + "</div></div>");
		},
		type : "post",
		dataType : "json",
		data : {
			action: 'cp_del_watchlist',
                        post_var: jQuery(this).attr("id"),
			},
			success: function(response) {
			if(response.type == "success") {

			jQuery(".notice").remove();
			jQuery("#" + post_id + "").remove();
			jQuery(".content_left").removeClass('error');
			jQuery(".content_left").prepend(response.notices);

	 		} else {

			jQuery(".notice").remove();
			jQuery(".content_left").removeClass('success');
			jQuery(".content_left").prepend(response.notices);

			}
		}
	
		})
	 	} else {
			return false;
		}
	})
});

jQuery(document).ready( function($) {
	jQuery(".delete-bid").click( function() {
		var retVal = confirm(translatable_text_string.DeleteBid);
		var post_id = jQuery(this).attr("id");
   		if( retVal == true ) {
	jQuery.ajax({
		url : cp_auction_ajax_script.ajaxurl,
		beforeSend: function() {
		jQuery(".notice").remove();
		jQuery(".content_left").prepend("<div class='notice success'><div>" + translatable_text_string.DeletingBids + "</div></div>");
		},
		type : "post",
		dataType : "json",
		data : {
			action: 'cp_bid_del_pid',
                        post_var: jQuery(this).attr("id"),
			},
			success: function(response) {
			if(response.type == "success") {

			jQuery(".notice").remove();
			jQuery("#" + post_id + "").remove();
			jQuery(".content_left").removeClass('error');
			jQuery(".content_left").prepend(response.notices);

	 		} else {

			jQuery(".notice").remove();
			jQuery(".content_left").removeClass('success');
			jQuery(".content_left").prepend(response.notices);

			}
		}
	
		})
	 	} else {
			return false;
		}
	})
});

jQuery(document).ready( function($) {
	jQuery(".delete-wanted-offer").click( function() {
		var retVal = confirm(translatable_text_string.DeleteWantedOffer);
   		if( retVal == true ) {
		var data = {
			action: 'cp_delete_wanted_offer',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".delete-coupon").click( function() {
		var retVal = confirm(translatable_text_string.DeleteCoupon);
   		if( retVal == true ) {
		var data = {
			action: 'cp_del_coupon',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".delete-uploads").click( function() {
		var retVal = confirm(translatable_text_string.DeleteUploads);
		var post_id = jQuery(this).attr("id");
   		if( retVal == true ) {
	jQuery.ajax({
		url : cp_auction_ajax_script.ajaxurl,
		beforeSend: function() {
		jQuery(".notice").remove();
		jQuery(".content_left").prepend("<div class='notice success'><div>" + translatable_text_string.DeletingUploads + "</div></div>");
		},
		type : "post",
		dataType : "json",
		data : {
			action: 'cp_del_uploads',
                        post_var: jQuery(this).attr("id"),
			},
			success: function(response) {
			if(response.type == "success") {

			jQuery(document).ajaxStop(function(){window.location.reload();});

	 		} else {

			jQuery(".notice").remove();
			jQuery(".content_left").removeClass('success');
			jQuery(".content_left").prepend(response.notices);

			}
		}

		})
	 	} else {
			return false;
		}
	})
});

jQuery(document).ready( function($) {
	jQuery(".delete-log").click( function() {
		var retVal = confirm(translatable_text_string.ConfirmText);
   		if( retVal == true ) {
		var data = {
			action: 'cp_del_log',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".mark-paid").click( function() {
		var retVal = confirm(translatable_text_string.MarkAsPaid);
   		if( retVal == true ) {
		var data = {
			action: 'cp_mark_as_paid',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	jQuery(".deactivate-purchase").click( function() {
		var retVal = confirm(translatable_text_string.DeactivatePurchase);
   		if( retVal == true ) {
		var data = {
			action: 'cp_deactivate_purchase',
                        post_var: jQuery(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	jQuery.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		jQuery(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});