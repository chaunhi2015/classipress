/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

jQuery(document).ready(function($) {

	/* initialize tabs control of auction pages */
	tabControlAuction();

});

/* Tab Control Auction */
function tabControlAuction() {
	var tabs = [];
	var tabContainers = [];
	jQuery('ul.tab_navig a').each(function() {
		if ( window.location.pathname.match(this.pathname) ) {
			tabs.push(this);
			tabContainers.push( jQuery(this.hash).get(0) );
		}
	});

	//hide all contrainers except execpt for the one from the URL hash or the first container
	if ( window.location.hash !== "" ) {
		jQuery(tabContainers).hide().filter(window.location.hash).show();
		//detecting <a> tab using its "href" which should always equal the hash
		jQuery(tabs).filter( function(index) {
			return ( jQuery(this).attr('href') === window.location.hash );
		}).addClass('selected');
		jQuery('html').scrollTop( jQuery('.tab_navig').position().top );
	} else {
		jQuery(tabContainers).hide().filter(':first').show();
		jQuery(tabs).filter(':first').addClass('selected');
	}

	jQuery(tabs).click(function() {
		// hide all tabs
		jQuery(tabContainers).hide().filter(this.hash).fadeIn(500);
		jQuery(tabs).removeClass('selected');
		jQuery(this).addClass('selected');
		return false;
	});
}