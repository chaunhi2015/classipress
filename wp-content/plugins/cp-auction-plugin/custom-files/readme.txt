=== CP Auction ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: auction, classipress, auction plugin, classipress theme, cpauction
License: GPL
License URI: http://www.gnu.org/licenses/gpl.html

---------------------------------------------------------------------------------------------------

=== Setup Instructions ===
* Locate the sidebar-ad.php file in the classipress directory, wp-content/themes/classipress/sidebar-ad.php
* OR if you use a childtheme you might locate the sidebar-ad.php file in the childtheme directory, wp-content/themes/CHILDTHEME/sidebar-ad.php
* Put your site into maintenace mode and rename the old standard file to, original_sidebar.php
* Copy the file sidebar-ad.php from wp-content/plugins/cp-auction-plugin/sidebar-ad.php/ into the correct directory classipress OR childtheme.

Note: If you rename the old/original file to original_sidebar.php then you can switch between the orginal and the custom sidebar
whenever you need without having to upload or rename any files again, just enable/disable the custom sidebar with the settings found
under the Misc tab of CP Auction settings. 

That`s it...

If you have questions then please open a ticket in our support forum, http://www.arctic-websolutions.com/forums/