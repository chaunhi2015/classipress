<?php

if ( get_option('cp_auction_enable_custom_sidebar') == "yes" ) {

	global $current_user, $gmap_active, $cp_options, $post, $cpurl;
	get_currentuserinfo();
	$uid = $current_user->ID;

	// make sure google maps has a valid address field before showing tab
	$gmap_active = false;
	$location_fields = get_post_custom();
	$_fields = array( 'cp_zipcode', 'cp_country', 'cp_state', 'cp_street', 'cp_city' );
	foreach ( $_fields as $i ) {
	if ( ! empty( $location_fields[$i] ) && ! empty( $location_fields[$i][0] ) ) {
		$gmap_active = true;
		break;
	}
}

if ( get_option('remove_maps') == "yes" ) {
	$gmap_active = false; 
}
	$ct = get_option('stylesheet');
	$pull_fields = get_option('cp_auction_pull_fields');
	$expire_date = strtotime(get_post_meta( $post->ID, 'cp_sys_expire_date', true ));
	$todayDate = strtotime(current_time('mysql'));
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$allowdeals = get_post_meta( $post->ID, 'cp_allow_deals', true );
	if ( function_exists( 'get_localized_field_values' ) ) $allow_deals = get_localized_field_values('cp_allow_deals', $allowdeals);
	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	$apage = get_option( 'cp_auction_author_page' );
	if( $post->post_status == "publish" && $closed != "yes" ) $display = true;
	else $display = false;
	$pms_opt = get_user_meta( $post->post_author, 'cartpaujPM_uOptions', true );
	if( $pms_opt ) $pms = $pms_opt['allow_messages'];
	else $pms = false;

	global $wpdb;
	if (class_exists("cartpaujPM")) {
	$pageID =  $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE post_content LIKE '%[cartpauj-pm]%' AND post_status = 'publish' AND post_type = 'page'");
	$link = get_permalink($pageID);
	}
	$allow_pms = get_user_meta( $post->post_author, 'cp_auction_allow_pms', true );

	if( $apage == "apage" ) $view_all_url = get_author_posts_url($post->post_author);
	else $view_all_url = cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.'');

	$display_info = true;
	if( ( get_option('cp_auction_hide_author_info') == "yes" ) && ( $closed == "yes" || $post->post_status == "pending" || $expire_date < $todayDate ) )
	$display_info = false;

	$login_url = '<a href="'.wp_login_url( get_permalink() ).'" title="'.__( 'Login to inquire about this ad.', 'auctionPlugin' ).'">'.__( 'logged in', 'auctionPlugin' ).'</a>';

	$msg = false;
?>

<script type="text/javascript">
function CPcontact() {
	el = document.getElementById("CPcontact");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>

<div id="CPcontact">
	<div class="pop-up custom">

	<?php if ( ( $cp_options->ad_inquiry_form && is_user_logged_in() ) || ! $cp_options->ad_inquiry_form ) {

// if contact form has been submitted, send the email
if ( isset( $_POST['submit'] ) && $_POST['send_email'] == 'yes' ) {

	$result = cp_auction_ad_owner_email( $post->ID );

	if ( $result->get_error_code() ) {
		$error_html = '';
		foreach ( $result->errors as $error )
			$error_html .= $error[0] . '<br />';
		$msg = '<p class="red center"><strong>' . $error_html . '</strong></p>';
	} else {
		$msg = '<p class="green center"><strong>' . __( 'Your message has been sent!', 'auctionPlugin' ) . '</strong></p>';
	}

}

?>

<form name="mainform" id="mainform" class="form_contact" action="" method="post" enctype="multipart/form-data">

	<p class="contact_msg"><?php _e( 'To inquire about this ad listing, complete the form below to send a message to the ad poster.', 'auctionPlugin' ); ?></p>

	<ol>
		<li>
			<label><?php _e( 'Name:', 'auctionPlugin' ); ?></label>
			<input name="from_name" id="from_name" type="text" minlength="2" value="<?php if(isset($_POST['from_name'])) echo esc_attr( stripslashes($_POST['from_name']) ); ?>" class="text required" />
		<div class="clr"></div>
		</li>

		<li>
			<label><?php _e( 'Email:', 'auctionPlugin' ); ?></label>
			<input name="from_email" id="from_email" type="text" minlength="5" value="<?php if(isset($_POST['from_email'])) echo esc_attr( stripslashes($_POST['from_email']) ); ?>" class="text required email" />
		<div class="clr"></div>
		</li>

		<li>
			<label><?php _e( 'Subject:', 'auctionPlugin' ); ?></label>
			<input name="subject" id="subject" type="text" minlength="2" value="<?php _e( 'Re:', 'auctionPlugin' ); ?> <?php the_title();?>" class="text required" />
		<div class="clr"></div>
		</li>

		<li>
			<label><?php _e( 'Message:', 'auctionPlugin' ); ?></label>
			<textarea name="message" id="message" rows="" cols="" class="text required"><?php if(isset($_POST['message'])) echo esc_attr( stripslashes($_POST['message']) ); ?></textarea>
		<div class="clr"></div>
		</li>

		<li>
		<?php
			// create a random set of numbers for spam prevention
			$randomNum = '';
			$randomNum2 = '';
			$randomNumTotal = '';

			$rand_num = rand(0,9);
			$rand_num2 = rand(0,9);
			$randomNumTotal = $randomNum + $randomNum2;
		?>
			<label><?php _e( 'Sum of', 'auctionPlugin' ); ?> <?php echo $rand_num; ?> + <?php echo $rand_num2; ?> =</label>
			<input name="rand_total" id="rand_total" type="text" minlength="1" value="" class="text required number" />
		<div class="clr"></div>
		</li>

	</ol>

	<div class="clr"></div>

	<input name="submit" type="submit" id="submit_inquiry" class="btn_orange" value="<?php _e( 'Send Inquiry', 'auctionPlugin' ); ?>" /> &nbsp; &nbsp; <a class="mbtn btn_orange" href='#' onclick='CPcontact()'><?php _e('Cancel', 'auctionPlugin'); ?></a>

	<input type="hidden" name="rand_num" value="<?php echo $rand_num; ?>" />
	<input type="hidden" name="rand_num2" value="<?php echo $rand_num2; ?>" />
	<input type="hidden" name="send_email" value="yes" />

</form>

<?php

			} else {
			?>
				<div class="pad25"></div>
				<p class="contact_msg center"><strong><?php _e( 'You must be logged in to inquire about this ad.', 'auctionPlugin' ); ?></strong></p>
				<div class="pad100"></div>
			<?php } ?>

	</div>
</div>

<!-- right sidebar -->
<div class="content_right custom">

<?php if( $ct == "jibo" ) { ?>
	<div class="tabsidebar">
	<?php } else { ?>
	<div class="tabprice">
	<?php } ?>

<?php if ( $gmap_active && $ct == "citrus-night" || $ct == "eldorado" || $ct == "jibo" || $ct == "multiport" || $ct == "simply-responsive-cp" || $ct == "phoenix" || $ct == "rondell_370" || $ct == "twinpress_classifieds" ) { ?>

			<!-- tab 1 -->
			<div id="priceblock1">

				<div class="clr"></div>

			</div>

<?php } ?>

			<div class="clr"></div>

			<div class="postertab">

				<div class="priceblocksmall dotted">

				<?php echo $msg; ?>

<?php if( $display_info ) { ?>

					<h2 class="dotted"><?php _e( 'Information about the ad poster', 'auctionPlugin' ); ?></h2>

			<?php if( get_option('cp_auction_remove_avatar') != "yes" ) { ?>

			<div id="userphoto">
				<p class='image-thumb'><?php appthemes_get_profile_pic( get_the_author_meta('ID'), get_the_author_meta('user_email'), 64 ); ?></p>
			</div>

			<?php } else { ?>

		<style>
		ul.member {
			margin-left: 0 !important;
			padding: 0px 5px;
		}
		</style>

		<?php } ?>

					<ul class="member">

						<li><span><?php _e( 'Listed by:', 'auctionPlugin' ); ?></span>
							<a href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php the_author_meta('display_name'); ?></a>
						</li>

						<li><span><?php _e( 'Active Since:', 'auctionPlugin' ); ?></span> <?php echo appthemes_display_date( get_the_author_meta('user_registered'), 'date', true ); ?></li>
						<?php if( get_the_author_meta('cp_private_business') ) { ?>
						<li><span><?php _e( 'Account Type:', 'auctionPlugin' ); ?></span> <?php echo get_the_author_meta('cp_private_business'); ?></li>
						<?php } ?>

					</ul>

					<?php cp_author_info( 'sidebar-ad' ); ?>

					<?php if( ! function_exists('cprate_install_plugin') ) { ?>
					<div class="pad10"></div>
					<div class="clr"></div>

					<?php } if( ! get_the_author_meta('cp_private_business') ) { ?>
					<div class="pad10"></div>
					<div class="clr"></div>
					<?php } ?>

					<?php if( get_option('cp_auction_about_me') && get_the_author_meta('user_description') ) { ?>
					<?php echo nl2br( get_the_author_meta('user_description') ); ?>
					<div class="pad10"></div>
					<div class="clr"></div>
					<?php } ?>

<?php } ?>

<?php if( $closed == "yes" ) { ?>
	<p class="red center"><strong><?php _e( 'This item has been sold!', 'auctionPlugin' ); ?></strong></p>
<?php } else if( $post->post_status == "pending" ) { ?>
	<p class="red center"><strong><?php _e( 'This ad has status as pending!', 'auctionPlugin' ); ?></strong></p>
<?php } else if( $expire_date < $todayDate ) { ?>
	<p class="red center"><strong><?php _e( 'This ad has expired!', 'auctionPlugin' ); ?></strong></p>
<?php } else {

	$option = get_option('cp_auction_fields');
	if( isset( $option['cp_contact_options'] ) )
	$list = explode(',', $option['cp_contact_options']);
	else $list = false;

	if( $list && in_array( $my_type, $list ) ) { ?>

<script type="text/javascript" language="javascript">
// <![CDATA[
function ShowHideOpt<?php echo $post->ID; ?>() {
    var ele = document.getElementById("ShowHidePhone<?php echo $post->ID; ?>");
    if(ele.style.display == "block") {
            ele.style.display = "none";
      }
    else {
        ele.style.display = "block";
    }
    var but = document.getElementById("ShowHideLink<?php echo $post->ID; ?>");
    if(but.style.display == "none") {
            but.style.display = "block";
      }
    else {
        but.style.display = "none";
    }
}
// ]]>
</script>

<script type="text/javascript" language="javascript">
// <![CDATA[
function ShowHideOpts<?php echo $post->ID; ?>() {
    var ele = document.getElementById("ShowHideEmail<?php echo $post->ID; ?>");
    if(ele.style.display == "block") {
            ele.style.display = "none";
      }
    else {
        ele.style.display = "block";
    }
    var but = document.getElementById("ShowHideLinks<?php echo $post->ID; ?>");
    if(but.style.display == "none") {
            but.style.display = "block";
      }
    else {
        but.style.display = "none";
    }
}
// ]]>
</script>

	<?php if( ( $pull_fields == "" || $pull_fields == "profile" && get_the_author_meta( 'cp_phone' ) > 0 ) || 
	( $pull_fields == "custom" && get_post_meta( $post->ID, 'cp_contact_phone', true ) ) ) { ?>
	<p class="member-title"><?php _e( 'Contact ad poster:', 'auctionPlugin' ); ?></p>
	<div class="pad5"></div>
	<?php } ?>

<div id="ShowHidePhone<?php echo $post->ID; ?>" style="display:none;">

	<?php if( $pull_fields == "" || $pull_fields == "profile" ) $phone = esc_attr( get_the_author_meta( 'cp_phone' ) );
	else $phone = esc_attr( get_post_meta( $post->ID, 'cp_contact_phone', true ) ); ?>
	<form name="mainform" id="mainform">
	<div id="call-us"><?php if ( ( $cp_options->ad_inquiry_form && is_user_logged_in() ) || ! $cp_options->ad_inquiry_form ) { ?><?php echo $phone; ?><?php } else { ?><a class="std-text" href="<?php echo wp_login_url( get_permalink() ); ?>" title="<?php _e( 'Login to view phone number', 'auctionPlugin' ); ?>"><?php _e( 'View phone number', 'auctionPlugin' ); ?></a>
	<a title="<?php _e( 'Login to view phone number', 'auctionPlugin' ); ?>" href="#" tip="<?php _e( 'You must click the link and log in to see the phone number. This makes it difficult to extract many phone numbers and then misusing these.', 'auctionPlugin' ); ?>"><span class="helpico"></span></a><?php } ?></div>

	<div class="pad10"></div>

	<div class="clr"></div>

	</form>

</div><!-- /ShowHidePhone -->

<?php if ( ( $cp_options->ad_inquiry_form && is_user_logged_in() ) || ! $cp_options->ad_inquiry_form ) { ?>
	<?php if( ( $pull_fields == "" || $pull_fields == "profile" && get_the_author_meta( 'cp_phone' ) > 0 ) || 
	( $pull_fields == "custom" && get_post_meta( $post->ID, 'cp_contact_phone', true ) ) ) { ?>
	<div id="ShowHideLink<?php echo $post->ID; ?>">
	<form name="mainform" id="mainform">
	<div id="call-us"><a class="std-text" href="#" onClick="return ShowHideOpt<?php echo $post->ID; ?>();"><?php _e( 'View phone number', 'auctionPlugin' ); ?></a>
	<a title="<?php _e( 'Click the link to see the phone number', 'auctionPlugin' ); ?>" href="#" tip="<?php _e( 'You must click the link to see the phone number. This makes it difficult to extract many phone numbers and then misusing these.', 'auctionPlugin' ); ?>"><span class="helpico"></span></a></div>
	<div class="pad10"></div>
	<div class="clr"></div>
	</form>
	</div><!-- /ShowHideLink -->
	<?php } ?>
<?php } else { ?>
	<?php if( ( $pull_fields == "" || $pull_fields == "profile" && get_the_author_meta( 'cp_phone' ) > 0 ) || 
	( $pull_fields == "custom" && get_post_meta( $post->ID, 'cp_contact_phone', true ) ) ) { ?>
	<form name="mainform" id="mainform">
	<div id="call-us"><a class="std-text" href="<?php echo wp_login_url( get_permalink() ); ?>" title="<?php _e( 'Login to view phone number', 'auctionPlugin' ); ?>"><?php _e( 'View phone number', 'auctionPlugin' ); ?></a><a title="<?php _e( 'Login to view phone number', 'auctionPlugin' ); ?>" href="#" tip="<?php _e( 'You must click the link and log in to see the phone number. This makes it difficult to extract many phone numbers and then misusing these.', 'auctionPlugin' ); ?>"><span class="helpico"></span></a></div>
	<div class="pad10"></div>
	<div class="clr"></div>
	</form>
	<?php } ?>
<?php } ?>

<div class="pad5"></div>

<div id="ShowHideEmail<?php echo $post->ID; ?>" style="display:none;">

	<?php if( $pull_fields == "" || $pull_fields == "profile" ) $email = esc_attr( get_the_author_meta( 'user_email' ) );
	else $email = esc_attr( get_post_meta( $post->ID, 'cp_contact_email', true ) ); ?>
	<form name="mainform" id="mainform">
	<div id="email-us"><?php if ( ( $cp_options->ad_inquiry_form && is_user_logged_in() ) || ! $cp_options->ad_inquiry_form ) { ?><?php echo $email; ?><?php } else { ?><a class="std-text" href="<?php echo wp_login_url( get_permalink() ); ?>" title="<?php _e( 'Login to view email address', 'auctionPlugin' ); ?>"><?php _e( 'View email address', 'auctionPlugin' ); ?></a><a title="<?php _e( 'Login to view email address', 'auctionPlugin' ); ?>" href="#" tip="<?php _e( 'You must click the link and log in to see the email address. This makes it difficult to extract email addresses and then misusing these.', 'auctionPlugin' ); ?>"><span class="helpico"></span></a><?php } ?></div>

	<div class="pad10"></div>

	<div class="clr"></div>

	</form>

</div><!-- /ShowHideEmail -->

<?php if ( ( $cp_options->ad_inquiry_form && is_user_logged_in() ) || ! $cp_options->ad_inquiry_form ) { ?>
	<?php if( ( $pull_fields == "" || $pull_fields == "profile" && get_the_author_meta( 'user_email' ) ) || 
	( $pull_fields == "custom" && get_post_meta( $post->ID, 'cp_contact_email', true ) ) ) { ?>
	<div id="ShowHideLinks<?php echo $post->ID; ?>">
	<form name="mainform" id="mainform">
	<div id="email-us"><a class="std-text" href="#" onClick="return ShowHideOpts<?php echo $post->ID; ?>();"><?php _e( 'View email address', 'auctionPlugin' ); ?></a><a title="<?php _e( 'Click the link to see the email address', 'auctionPlugin' ); ?>" href="#" tip="<?php _e( 'You must click the link to see the email address. This makes it difficult to extract email addresses and then misusing these.', 'auctionPlugin' ); ?>"><span class="helpico"></span></a></div>
	<div class="pad10"></div>
	<div class="clr"></div>
	</form>
	</div><!-- /ShowHideLinks -->
	<?php } ?>
<?php } else { ?>
	<?php if( ( $pull_fields == "" || $pull_fields == "profile" && get_the_author_meta( 'user_email' ) ) || 
	( $pull_fields == "custom" && get_post_meta( $post->ID, 'cp_contact_email', true ) ) ) { ?>
	<form name="mainform" id="mainform">
	<div id="email-us"><a class="std-text" href="<?php echo wp_login_url( get_permalink() ); ?>" title="<?php _e( 'Login to view email address', 'auctionPlugin' ); ?>"><?php _e( 'View email address', 'auctionPlugin' ); ?></a><a title="<?php _e( 'Login to view email address', 'auctionPlugin' ); ?>" href="#" tip="<?php _e( 'You must click the link and log in to see the email address. This makes it difficult to extract email addresses and then misusing these.', 'auctionPlugin' ); ?>"><span class="helpico"></span></a></div>
	<div class="pad10"></div>
	<div class="clr"></div>
	</form>
	<?php } ?>
<?php } ?>

	<?php if( get_post_meta( $post->ID, 'cp_contact_notes', true ) ) { ?>
	<div class="pad10"></div>
	<p class="member-title"><?php _e( 'Notes:', 'auctionPlugin' ); ?></p>
	<?php echo get_post_meta( $post->ID, 'cp_contact_notes', true ) ?>
	<div class="pad10"></div>
	<div class="clr"></div>
	<?php } ?>

	<?php if ( ( $cp_options->ad_inquiry_form && is_user_logged_in() ) || ! $cp_options->ad_inquiry_form ) { ?>

			<div class="pad5"></div>

			<a class="mbtn btn_orange" href="#" title="<?php _e('Click here to contact seller by e-mail', 'auctionPlugin'); ?>" onclick="CPcontact()"><?php _e('Contact seller by e-mail &raquo;', 'auctionPlugin'); ?></a>

			<div class="pad10"></div>

			<?php } else {
			?>
				<div class="pad10"></div>
				<p class="contact_msg center"><strong><?php echo sprintf( __( 'You must be %s to inquire about this ad.', 'auctionPlugin' ), $login_url); ?></strong></p>
				<div class="pad5"></div>
			<?php } ?>

	<?php } else { ?>

	<?php if( get_post_meta( $post->ID, 'cp_contact_notes', true ) ) { ?>
	<p class="member-title"><?php _e( 'Notes:', 'auctionPlugin' ); ?></p>
	<?php echo get_post_meta( $post->ID, 'cp_contact_notes', true ) ?>
	<div class="pad10"></div>
	<div class="clr"></div>
	<?php } ?>

<?php }
} ?>

		</div>

	<?php if ( $gmap_active ) { ?>
	<div class="pad10"></div>
	<?php } ?>

<?php if(( get_option('cp_auction_enable_bid_widget') == "yes" && $display ) && ( $my_type == "normal" || $my_type == "reverse" )) { ?>
<script type="text/javascript" src="<?php echo cp_auction_plugin_url(); ?>/js/countdown-bidwidget.js" defer="defer"></script>
<?php cp_auction_bidder_box_widget(); ?>
<?php if ( $gmap_active ) { ?>
<div class="dotted"></div>
<div class="pad10"></div>
<?php } } elseif(( get_option('cp_auction_enable_wanted_widget') == "yes" && $allow_deals == "Yes" && $display ) && ( empty( $my_type ) || $my_type == 'wanted' || $my_type == 'classified' )) { ?>
<?php cp_auction_wanted_box_widget(); ?>
<div class="pad5"></div>
<?php if ( $gmap_active ) { ?>
<div class="dotted"></div>
<div class="pad10"></div>
<?php } } elseif(( get_option('aws_bo_use_widget') == "yes" && $bestoffer == true && $display ) && ( empty( $my_type ) || $my_type == 'classified' )) { ?>
<?php if( function_exists('aws_best_offer_box') ) aws_best_offer_box(); ?>
<div class="pad10"></div>
<?php if ( $gmap_active ) { ?>
<div class="dotted"></div>
<div class="pad10"></div>
<?php } } ?>

<?php if( ( get_option('cp_auction_move_menu_bar') == "yes" ) && ( $my_type == 'normal' || $my_type == 'reverse' ) ) { ?>

<div class="cp-sidebar-links"><?php if( current_user_can( 'manage_options' ) ) { ?><div id="bump_icon"><a href="#" class="admin-bump-ads" title="<?php _e('Click here to bump up this ad.', 'auctionPlugin'); ?>" id='<?php echo $post->ID; ?>'><?php _e('Bump Up', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?><?php if( get_option('cp_auction_plugin_if_watchlist') == "yes" && !cp_auction_check_watchlist($uid, $post->ID) ) { ?><div id="watchlist_icon">
<a href="#" class="add-watchlist" title="<?php _e('Add this auction to your watchlist.', 'auctionPlugin'); ?>" id='<?php echo $uid; ?>|<?php echo $post->ID; ?>'><?php _e('Add to Watchlist', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?><?php if( get_option('cp_auction_plugin_if_favorites') == "yes" && !cp_auction_check_favoritelist($uid, $post->post_author) ) { ?><div class="favourite_icon"><a href="#" class="add-favorites" title="<?php _e('Add this author / shop to your favourites.', 'auctionPlugin'); ?>" id='<?php echo $uid; ?>|<?php echo $post->post_author; ?>'><?php _e('Add to Favourites', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?><div id="viewall_icon"><a href="<?php echo $view_all_url; ?>"><?php _e('View all Ads', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php if ( class_exists("cartpaujPM") && $pms == "true" && $allow_pms == "yes" ) { ?><div id="pm_icon"><a href="<?php echo $link; ?>?pmaction=newmessage&to=<?php echo $post->post_author; ?>"><?php _e('Send PM', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?><?php if ( get_option('cp_auction_enable_report_ad') == "yes" ) { ?><div id="report_icon"><a href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$post->post_author.'&pid='.$post->ID.''); ?>"><?php _e('Report Ad', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?>

	<div class="pad10"></div>
	<div class="dotted"></div>
	<div class="pad5"></div>
</div>

<?php } elseif ( ( get_option('cp_auction_move_menu_bar') == "yes" ) && ( empty( $my_type ) || $my_type == 'classified' || $my_type == 'wanted' ) ) { ?>

<div class="cp-sidebar-links"><?php if( current_user_can( 'manage_options' ) ) { ?><div id="bump_icon"><a href="#" class="admin-bump-ads" title="<?php _e('Click here to bump up this ad.', 'auctionPlugin'); ?>" id='<?php echo $post->ID; ?>'><?php _e('Bump Up', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?><?php if(( get_option('cp_auction_plugin_if_favorites') == "yes" ) && ( !cp_auction_check_favoritelist($uid, $post->post_author) )) { ?><div class="favourite_icon"><a href="#" class="add-favorites" title="<?php _e('Add this author / shop to your favourites.', 'auctionPlugin'); ?>" id='<?php echo $uid; ?>|<?php echo $post->post_author; ?>'><?php _e('Add to Favourites', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?><div id="viewall_icon"><a href="<?php echo $view_all_url; ?>"><?php _e('View all Ads', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php if ( class_exists("cartpaujPM") && $pms == "true" && $allow_pms == "yes" ) { ?><div id="pm_icon"><a href="<?php echo $link; ?>?pmaction=newmessage&to=<?php echo $post->post_author; ?>"><?php _e('Send PM', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?><?php if ( get_option('cp_auction_enable_report_ad') == "yes" ) { ?><div id="report_icon"><a href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$post->post_author.'&pid='.$post->ID.''); ?>"><?php _e('Report Ad', 'auctionPlugin'); ?></a></div>
	<div class="pad10"></div>
	<div class="clear5"></div>
<?php } ?>

	<div class="pad10"></div>
	<div class="dotted"></div>
	<div class="pad5"></div>
</div>

<?php } ?>
</div>

<?php if( $ct != "citrus-night" && $ct != "eldorado" && $ct != "multiport" && $ct != "simply-responsive-cp" && $ct != "phoenix" && $ct != "rondell_370" && $ct != "twinpress_classifieds" ) { ?>

		<?php if ( $gmap_active ) { ?>

		<div class="clear15"></div>

			<div id="priceblock1">

				<div class="clr"></div>

				<div class="singletab">

					<?php get_template_part( 'includes/sidebar', 'gmap' ); ?>

				</div><!-- /singletab -->

			</div>

		<?php } else {
		if ( function_exists('cp_auction_custom_sidebar') ) cp_auction_custom_sidebar();
		} ?>

<?php } ?>

	</div><!-- /postertab -->

	<?php appthemes_before_sidebar_widgets( 'ad' ); ?>

	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar_listing') ) : else : ?>

	<!-- no dynamic sidebar so don`t do anything -->

	<?php endif; ?>

	<?php appthemes_after_sidebar_widgets( 'ad' ); ?>

</div><!-- /content_right -->

<?php } else {

	$file = get_stylesheet_directory() . '/original_sidebar.php';

	if ( file_exists( $file ) ) {
		require_once( get_stylesheet_directory() . '/original_sidebar.php');
}
else { ?>

<!-- right sidebar -->
<div class="content_right custom">
	<div class="tabprice">
		<div id="priceblock1">

			<div class="clr"></div>

			<div class="singletab">

				<?php _e('Please read the <a href="/wp-admin/admin.php?page=cp-auction&tab=misc">setup instructions</a> for how to use the custom sidebar, or you can open a ticket in our <a href="http://www.arctic-websolutions.com/forums/" target="_blank">support forum</a>.', 'auctionAdmin'); ?>

			</div><!-- /singletab -->

		</div>
	</div>
</div>

<?php
}
}
?>