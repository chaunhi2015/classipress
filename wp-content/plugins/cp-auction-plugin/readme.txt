=== CP Auction Plugin ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: auction, classipress, auction plugin, classipress theme, cpauction
Requires at least: 4.5.0
Tested up to: 4.6.1
ClassiPress: 3.5.7
Stable tag: 6.0.4
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

---------------------------------------------------------------------------------------------------

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://mixed.wp-build.com) - Testdrive every aspect of the plugin.
* [Manuals & Support](http://www.arctic-websolutions.com/cpauction-plugin/) - Setup instructions and support.

=== Some Admin Features ===

* Super easy management of auctions, based on ClassiPress theme.
* Get email notifications on new auctions.
* Add / edit custom fields.
* Countdown timer on all active auctions.
* Latest auctions widget (unlimited).
* Featured auctions widget (unlimited).
* Userpanel menu widget (unlimited).
* Edit users reviews/ratings.
* Select imageuploader, choose between ajax/flash or simple php uploader.
* Set max image size.
* Set max images allowed for upload.
* Set auction type, normal, reverse or free bid.
* Enable or disable google maps.
* Set currencies.
* Add / edit auctions.
* Check out http://www.arctic-websolutions.com for all options.

=== Some User Features ===

* Users can post their own auctions.
* Review and rate seller and/or buyer.
* Contact seller.
* Set start price and/or buy now price.
* Set auction end date.
* Report fraud / fraudulent users.
* Pay for won auctions/items.
* View users auction profile and comments posted by other users.
* Check out http://www.arctic-websolutions.com for all options.

=== Installation ===

* [Instructions] (http://www.arctic-websolutions.com/cpauction-plugin/) - Setup instructions and support.

=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.