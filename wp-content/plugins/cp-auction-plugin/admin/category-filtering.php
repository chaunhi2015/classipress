<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_plugin_filter_cats_settings() {


	if( isset( $_POST['save_settings'] ) ) {

	$cats = false; 
	$ad_type = isset( $_POST['ad_type'] ) ? esc_attr( $_POST['ad_type'] ) : '';
	if( isset( $_POST['post_category'] ) ) $cats = join(",", $_POST['post_category']);

	if( $cats && $ad_type ) {
	update_option( 'cp_auction_'.$ad_type.'_cats', $cats );
	}
	else if ( ! $cats ) {
	delete_option( 'cp_auction_'.$ad_type.'_cats' );
	}

	echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Settings was saved!','auctionAdmin').'</strong></p></div>';

	}

	$checked = false;
	$classified = false;
	$wanted = false;
	$normal = false;
	if ( isset( $_GET['ad_type'] ) && $_GET['ad_type'] == "classified" ) {
	$option = get_option('cp_auction_classified_cats');
	$checked = explode(',', $option);
	$classified = "checked";
	} else if ( isset( $_GET['ad_type'] ) && $_GET['ad_type'] == "wanted" ) {
	$option = get_option('cp_auction_wanted_cats');
	$checked = explode(',', $option);
	$wanted = "checked";
	} else if ( isset( $_GET['ad_type'] ) && $_GET['ad_type'] == "normal" ) {
	$option = get_option('cp_auction_normal_cats');
	$checked = explode(',', $option);
	$normal = "checked";
	}
?>

<script type='text/javascript'>
jQuery(document).ready( function($) {
	jQuery(".selectUrl").click(function() {
	window.location.href = 'admin.php?page=cp-auction&tab=cats&ad_type='+$(this).val();
	})
});
</script>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Category Filtering by Ad Type', 'auctionAdmin'); ?></h3>
            <div class="inside">

	<div class="admin_text"><?php _e('Category filtering allows you to assign categories to an specified ad type. Leave this option be untouched if you do not want to assign categories.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('<strong>NOTE</strong>: Check and assign any parent category only, OR check and assign the parent category with ALL it`s child categories. You can <strong>NOT</strong> check and assign child categories alone, their parents must always be checked, and then also <strong>ALL</strong> it`s child categories. <strong>Always do a live test of your settings</strong>.', 'auctionAdmin'); ?></div>

<form method="post" action="">

<table class="form-table">
    <tbody>

	<tr id="post_category[]_row" valign="top">
	<th scope="row"><?php _e('Available Categories:', 'auctionAdmin'); ?></th>
	<td class="forminp"><div id="form-categorydiv">
		<div class="tabs-panel" id="categories-all" style="">
			<ul class="list:category categorychecklist form-no-clear" id="categorychecklist">

			<?php _e('Select the type of ad you want to assign categories:', 'auctionAdmin'); ?>

			<div class="clear10"></div>

			<input <?php echo $classified; ?> class="selectUrl" value="classified" name="ad_type" type="radio"><?php _e('Classified Ads.', 'auctionAdmin'); ?> <input <?php echo $wanted; ?> class="selectUrl" value="wanted" name="ad_type" type="radio"><?php _e('Wanted Ads.', 'auctionAdmin'); ?> <input <?php echo $normal; ?> class="selectUrl" value="normal" name="ad_type" type="radio"><?php _e('Normal Auctions.', 'auctionAdmin'); ?>

			<div class="clear15"></div>

			<?php echo cp_auction_category_checklist( $checked ); ?> 

			</ul>
		</div>
			<a href="#" class="checkall"><?php _e('check all', 'auctionAdmin'); ?></a>
		</div>
	</td>
	</tr>

    </tbody>
</table>

	<p class="submit">
	<input type="submit" name="save_settings" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" />
	</p>

</form>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}