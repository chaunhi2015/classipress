<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

    	$del_options = get_option('cp_auction_plugin_uninstall');

function uninstall_cp_auction() {

	global $wpdb;

	if (class_exists("CP_Auction_Avatars")) {
	$cp_auction_avatars = new CP_Auction_Avatars;
	$args = array(
	'blog_id' => $GLOBALS['blog_id']
	);
	$users = get_users($args);

	foreach ( $users as $user ) {
		$cp_auction_avatars->avatar_delete( $user->user_id );
	}
	delete_option('cp_auction_avatars_enable');
	}

	$option = get_option( 'cp_auction_page_ids' );
	foreach ( $option as $key => $value ) {
	$page_id = $value;
	$force_delete = true;
	wp_delete_post( $page_id, $force_delete );
	}

	$table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
	$results = $wpdb->get_results("SELECT * FROM {$table_fields} ORDER BY field_id ASC");
	if( $results ) {
	foreach($results as $res) {
	$table_ad_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_ad_fields} WHERE field_name = '$res->field_name'");
	if( $row ) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_ad_fields} WHERE field_name = '%s'", $res->field_name));
	}
	}
}

	//----------------------------------------------------------------------------

	$bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$wpdb->query("DROP TABLE IF EXISTS $bids");

	$fields = $wpdb->prefix . "cp_auction_plugin_fields";
	$wpdb->query("DROP TABLE IF EXISTS $fields");

	$tabs = $wpdb->prefix . "cp_auction_plugin_tabs";
	$wpdb->query("DROP TABLE IF EXISTS $tabs");

	$transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$wpdb->query("DROP TABLE IF EXISTS $transactions");

	$coupons = $wpdb->prefix . "cp_auction_plugin_coupons";
	$wpdb->query("DROP TABLE IF EXISTS $coupons");

	$purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$wpdb->query("DROP TABLE IF EXISTS $purchases");

	$favorites = $wpdb->prefix . "cp_auction_plugin_favorites";
	$wpdb->query("DROP TABLE IF EXISTS $favorites");

	$watchlist = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$wpdb->query("DROP TABLE IF EXISTS $watchlist");

	$uploads = $wpdb->prefix . "cp_auction_plugin_uploads";
	$wpdb->query("DROP TABLE IF EXISTS $uploads");

	//----------------------------------------------------------------------------

	$cp_prefix = "cp_auction";
	$sql = "DELETE FROM ". $wpdb->options
         ." WHERE option_name LIKE '".$cp_prefix."%'";
	$wpdb->query($sql);

	//----------------------------------------------------------------------------

	delete_option('widget_cp_auctions_wanted_widget');
	delete_option('widget_cp_auctions_latest_auctions');
	delete_option('widget_cp_auctions_featured_auctions');
	delete_option('widget_cp_auctions_bidder_widget');
	delete_option('FeaturedAuctions_widget');
	delete_option('LatestAuctions_widget');
	delete_option('LastAuctions_widget');
	delete_option('WantedBox_widget');
	delete_option('BidderBox_widget');
	delete_option("cp_add_this_activate");
	delete_option("cp_add_this_activate_classifieds");
	delete_option("cp_add_this_activate_header");
	delete_option("cp_add_this_code");
	delete_option("cp_add_this_code_header");
	delete_option("cp_fb_like_activate");
	delete_option("cp_fb_like_activate_classifieds");
	delete_option("cp_fb_like_activate_header");
	delete_option("cp_fb_like_top");
	delete_option("cp_fb_like_middle");
	delete_option("cp_fb_like_bottom");
	delete_option("cp_fb_like_header");
	delete_option("cp_fb_like_layout");
	delete_option("cp_fb_like_action");
	delete_option("cp_fb_like_font");
	delete_option("cp_fb_like_colorscheme");
	delete_option("cp_fb_like_width");
	delete_option("cp_fb_like_height");
	delete_option("cp_fb_like_send");

	//----------------------------------------------------------------------------

	wp_reset_postdata();
	flush_rewrite_rules();
}

	if ( $del_options == "yes" ) {
		uninstall_cp_auction();
	}

?>