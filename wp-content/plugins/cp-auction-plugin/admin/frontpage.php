<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_plugin_setup_frontpage() {
	global $current_user, $wpdb;

	$msg = "";
	$ok = "";

	if(isset($_POST['enable_frontpage'])) {
		$enable = isset( $_POST['cp_auction_enable_frontpage'] ) ? esc_attr( $_POST['cp_auction_enable_frontpage'] ) : '';
		$enables = isset( $_POST['cp_auction_enable_authorpage'] ) ? esc_attr( $_POST['cp_auction_enable_authorpage'] ) : '';
		$slide_in = isset( $_POST['cp_auction_slide_in_ads'] ) ? esc_attr( $_POST['cp_auction_slide_in_ads'] ) : '';
		$aslug = isset( $_POST['cp_auction_author_page_slug'] ) ? esc_attr( $_POST['cp_auction_author_page_slug'] ) : '';
		if( $enable == "yes" ) {
		update_option( 'cp_auction_enable_frontpage', $enable );
		} else {
		delete_option( 'cp_auction_enable_frontpage' );
		}
		if( $enables == "yes" ) {
		update_option( 'cp_auction_enable_authorpage', $enables );
		} else {
		delete_option( 'cp_auction_enable_authorpage' );
		}
		if( $slide_in == "yes" ) {
		update_option( 'cp_auction_slide_in_ads', $slide_in );
		} else {
		delete_option( 'cp_auction_slide_in_ads' );
		}
		if( $aslug ) {
		update_option('cp_auction_author_page_slug',$aslug);
		} else {
		delete_option('cp_auction_author_page_slug');
		}
		$msg = ''.__( 'Settings was saved!', 'auctionAdmin' ).'';
		}
	if(isset($_POST['save_toggle'])) {
		$toggle = isset( $_POST['toggle_cats'] ) ? esc_attr( $_POST['toggle_cats'] ) : '';
		if( $toggle == "yes" ) {
		update_option('cp_auction_toggle_cats',$toggle);
		} else {
		delete_option('cp_auction_toggle_cats');
		}
		$msg = ''.__('Settings was saved','auctionAdmin').'';
		}
	if(isset($_POST['save_misc'])) {
		$rondell_banner = isset( $_POST['rondell_banner'] ) ? esc_attr( $_POST['rondell_banner'] ) : '';
		$rondell_banner_bottom = isset( $_POST['rondell_banner_bottom'] ) ? esc_attr( $_POST['rondell_banner_bottom'] ) : '';
		$rondell_searchbar = isset( $_POST['rondell_searchbar'] ) ? esc_attr( $_POST['rondell_searchbar'] ) : '';
		if( $rondell_banner == "yes" ) {
		update_option('cp_auction_rondell_banner',$rondell_banner);
		} else {
		delete_option('cp_auction_rondell_banner');
		}
		if( $rondell_banner_bottom == "yes" ) {
		update_option('cp_auction_rondell_banner_bottom',$rondell_banner_bottom);
		} else {
		delete_option('cp_auction_rondell_banner_bottom');
		}
		if( $rondell_searchbar == "yes" ) {
		update_option('cp_auction_rondell_searchbar',$rondell_searchbar);
		} else {
		delete_option('cp_auction_rondell_searchbar');
		}
		$msg = ''.__('Settings was saved','auctionAdmin').'';
		}
	if(isset($_POST['save_tabs'])) {
		$jibo = isset( $_POST['jibo'] ) ? esc_attr( $_POST['jibo'] ) : '';
		$jibo_order = isset( $_POST['jibo_order'] ) ? esc_attr( $_POST['jibo_order'] ) : '';
		$jibo_tab_where = isset( $_POST['cp_auction_jibo_tab_where'] ) ? esc_attr( $_POST['cp_auction_jibo_tab_where'] ) : '';
		$listed = isset( $_POST['listed'] ) ? esc_attr( $_POST['listed'] ) : '';
		$listed_order = isset( $_POST['listed_order'] ) ? esc_attr( $_POST['listed_order'] ) : '';
		$listed_tab_where = isset( $_POST['cp_auction_listed_tab_where'] ) ? esc_attr( $_POST['cp_auction_listed_tab_where'] ) : '';
		$featured = isset( $_POST['featured'] ) ? esc_attr( $_POST['featured'] ) : '';
		$featured_order = isset( $_POST['featured_order'] ) ? esc_attr( $_POST['featured_order'] ) : '';
		$featured_tab_where = isset( $_POST['cp_auction_featured_tab_where'] ) ? esc_attr( $_POST['cp_auction_featured_tab_where'] ) : '';
		$popular = isset( $_POST['popular'] ) ? esc_attr( $_POST['popular'] ) : '';
		$popular_order = isset( $_POST['popular_order'] ) ? esc_attr( $_POST['popular_order'] ) : '';
		$popular_tab_where = isset( $_POST['cp_auction_popular_tab_where'] ) ? esc_attr( $_POST['cp_auction_popular_tab_where'] ) : '';
		$classified = isset( $_POST['classified'] ) ? esc_attr( $_POST['classified'] ) : '';
		$classified_order = isset( $_POST['classified_order'] ) ? esc_attr( $_POST['classified_order'] ) : '';
		$classified_tab_where = isset( $_POST['cp_auction_classified_tab_where'] ) ? esc_attr( $_POST['cp_auction_classified_tab_where'] ) : '';
		$wanted = isset( $_POST['wanted'] ) ? esc_attr( $_POST['wanted'] ) : '';
		$wanted_order = isset( $_POST['wanted_order'] ) ? esc_attr( $_POST['wanted_order'] ) : '';
		$wanted_tab_where = isset( $_POST['cp_auction_wanted_tab_where'] ) ? esc_attr( $_POST['cp_auction_wanted_tab_where'] ) : '';
		$auction = isset( $_POST['auction'] ) ? esc_attr( $_POST['auction'] ) : '';
		$auction_order = isset( $_POST['auction_order'] ) ? esc_attr( $_POST['auction_order'] ) : '';
		$auction_tab_where = isset( $_POST['cp_auction_auction_tab_where'] ) ? esc_attr( $_POST['cp_auction_auction_tab_where'] ) : '';
		$sold_ads = isset( $_POST['sold_ads'] ) ? esc_attr( $_POST['sold_ads'] ) : '';
		$sold_ads_order = isset( $_POST['sold_ads_order'] ) ? esc_attr( $_POST['sold_ads_order'] ) : '';
		$sold_ads_tab_where = isset( $_POST['cp_auction_sold_ads_tab_where'] ) ? esc_attr( $_POST['cp_auction_sold_ads_tab_where'] ) : '';
		$categories = isset( $_POST['categories'] ) ? esc_attr( $_POST['categories'] ) : '';
		$categories_order = isset( $_POST['categories_order'] ) ? esc_attr( $_POST['categories_order'] ) : '';
		$categories_tab_where = isset( $_POST['cp_auction_categories_tab_where'] ) ? esc_attr( $_POST['cp_auction_categories_tab_where'] ) : '';
		$blog = isset( $_POST['blog'] ) ? esc_attr( $_POST['blog'] ) : '';
		$blog_order = isset( $_POST['blog_order'] ) ? esc_attr( $_POST['blog_order'] ) : '';
		$blog_tab_where = isset( $_POST['cp_auction_blog_tab_where'] ) ? esc_attr( $_POST['cp_auction_blog_tab_where'] ) : '';
		$category = isset( $_POST['category'] ) ? esc_attr( $_POST['category'] ) : '';
		$category_order = isset( $_POST['category_order'] ) ? esc_attr( $_POST['category_order'] ) : '';
		$category_tab_where = isset( $_POST['cp_auction_category_tab_where'] ) ? esc_attr( $_POST['cp_auction_category_tab_where'] ) : '';
		$users_type = isset( $_POST['users_type'] ) ? esc_attr( $_POST['users_type'] ) : '';
		$userstype_order = isset( $_POST['userstype_order'] ) ? esc_attr( $_POST['userstype_order'] ) : '';
		$userstype_tab_where = isset( $_POST['cp_auction_intention_tab_where'] ) ? esc_attr( $_POST['cp_auction_intention_tab_where'] ) : '';
		$buynow_classifieds = isset( $_POST['buynow_classifieds'] ) ? esc_attr( $_POST['buynow_classifieds'] ) : '';
		$buynow_classifieds_order = isset( $_POST['buynow_classifieds_order'] ) ? esc_attr( $_POST['buynow_classifieds_order'] ) : '';
		$buynow_classifieds_tab_where = isset( $_POST['cp_auction_buynow_classifieds_tab_where'] ) ? esc_attr( $_POST['cp_auction_buynow_classifieds_tab_where'] ) : '';
		$buynow = isset( $_POST['buynow'] ) ? esc_attr( $_POST['buynow'] ) : '';
		$buynow_order = isset( $_POST['buynow_order'] ) ? esc_attr( $_POST['buynow_order'] ) : '';
		$buynow_tab_where = isset( $_POST['cp_auction_buynow_tab_where'] ) ? esc_attr( $_POST['cp_auction_buynow_tab_where'] ) : '';
		$closing = isset( $_POST['closing'] ) ? esc_attr( $_POST['closing'] ) : '';
		$closing_order = isset( $_POST['closing_order'] ) ? esc_attr( $_POST['closing_order'] ) : '';
		$closing_tab_where = isset( $_POST['cp_auction_closing_tab_where'] ) ? esc_attr( $_POST['cp_auction_closing_tab_where'] ) : '';
		$contact = isset( $_POST['contact'] ) ? esc_attr( $_POST['contact'] ) : '';
		$contact_order = isset( $_POST['contact_order'] ) ? esc_attr( $_POST['contact_order'] ) : '';
		$contact_tab_where = isset( $_POST['cp_auction_contact_tab_where'] ) ? esc_attr( $_POST['cp_auction_contact_tab_where'] ) : '';

		$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";

		if( $jibo && $jibo_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block0'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $jibo_tab_where, 'title' => $jibo, 'sequence' => $jibo_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $jibo_tab_where, 'block0', $jibo, $jibo_order));
		}
		update_option('cp_auction_jibo_tab',$jibo);
		update_option('cp_auction_jibo_tab_order',$jibo_order);
		update_option('cp_auction_jibo_tab_where',$jibo_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block0'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block0' ) );
		}
		delete_option('cp_auction_jibo_tab');
		delete_option('cp_auction_jibo_tab_order');
		delete_option('cp_auction_jibo_tab_where');
		}
		if( $listed && $listed_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block1'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $listed_tab_where, 'title' => $listed, 'sequence' => $listed_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $listed_tab_where, 'block1', $listed, $listed_order));
		}
		update_option('cp_auction_listed_tab',$listed);
		update_option('cp_auction_listed_tab_order',$listed_order);
		update_option('cp_auction_listed_tab_where',$listed_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block1'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block1' ) );
		}
		delete_option('cp_auction_listed_tab');
		delete_option('cp_auction_listed_tab_order');
		delete_option('cp_auction_listed_tab_where');
		}
		if( $featured && $featured_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block2'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $featured_tab_where, 'title' => $featured, 'sequence' => $featured_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $featured_tab_where, 'block2', $featured, $featured_order));
		}
		update_option('cp_auction_featured_tab',$featured);
		update_option('cp_auction_featured_tab_order',$featured_order);
		update_option('cp_auction_featured_tab_where',$featured_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block2'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block2' ) );
		}
		delete_option('cp_auction_featured_tab');
		delete_option('cp_auction_featured_tab_order');
		delete_option('cp_auction_featured_tab_where');
		}
		if( $popular && $popular_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block3'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $popular_tab_where, 'title' => $popular, 'sequence' => $popular_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $popular_tab_where, 'block3', $popular, $popular_order));
		}
		update_option('cp_auction_popular_tab',$popular);
		update_option('cp_auction_popular_tab_order',$popular_order);
		update_option('cp_auction_popular_tab_where',$popular_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block3'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block3' ) );
		}
		delete_option('cp_auction_popular_tab');
		delete_option('cp_auction_popular_tab_order');
		delete_option('cp_auction_popular_tab_where');
		}
		if( $classified && $classified_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block4'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $classified_tab_where, 'title' => $classified, 'sequence' => $classified_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $classified_tab_where, 'block4', $classified, $classified_order));
		}
		update_option('cp_auction_classified_tab',$classified);
		update_option('cp_auction_classified_tab_order',$classified_order);
		update_option('cp_auction_classified_tab_where',$classified_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block4'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block4' ) );
		}
		delete_option('cp_auction_classified_tab');
		delete_option('cp_auction_classified_tab_order');
		delete_option('cp_auction_classified_tab_where');
		}
		if( $wanted && $wanted_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block7'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $wanted_tab_where, 'title' => $wanted, 'sequence' => $wanted_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $wanted_tab_where, 'block7', $wanted, $wanted_order));
		}
		update_option('cp_auction_wanted_tab',$wanted);
		update_option('cp_auction_wanted_tab_order',$wanted_order);
		update_option('cp_auction_wanted_tab_where',$wanted_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block7'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block7' ) );
		}
		delete_option('cp_auction_wanted_tab');
		delete_option('cp_auction_wanted_tab_order');
		delete_option('cp_auction_wanted_tab_where');
		}
		if( $auction && $auction_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block6'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $auction_tab_where, 'title' => $auction, 'sequence' => $auction_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $auction_tab_where, 'block6', $auction, $auction_order));
		}
		update_option('cp_auction_auction_tab',$auction);
		update_option('cp_auction_auction_tab_order',$auction_order);
		update_option('cp_auction_auction_tab_where',$auction_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block6'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block6' ) );
		}
		delete_option('cp_auction_auction_tab');
		delete_option('cp_auction_auction_tab_order');
		delete_option('cp_auction_auction_tab_where');
		}
		if( $sold_ads && $sold_ads_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block19'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $sold_ads_tab_where, 'title' => $sold_ads, 'sequence' => $sold_ads_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $sold_ads_tab_where, 'block19', $sold_ads, $sold_ads_order));
		}
		update_option('cp_auction_sold_ads_tab',$sold_ads);
		update_option('cp_auction_sold_ads_tab_order',$sold_ads_order);
		update_option('cp_auction_sold_ads_tab_where',$sold_ads_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block19'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block19' ) );
		}
		delete_option('cp_auction_sold_ads_tab');
		delete_option('cp_auction_sold_ads_tab_order');
		delete_option('cp_auction_sold_ads_tab_where');
		}
		if( $categories && $categories_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block11'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $categories_tab_where, 'title' => $categories, 'sequence' => $categories_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $categories_tab_where, 'block11', $categories, $categories_order));
		}
		update_option('cp_auction_categories_tab',$categories);
		update_option('cp_auction_categories_tab_order',$categories_order);
		update_option('cp_auction_categories_tab_where',$categories_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block11'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block11' ) );
		}
		delete_option('cp_auction_categories_tab');
		delete_option('cp_auction_categories_tab_order');
		delete_option('cp_auction_categories_tab_where');
		}
		if( $blog && $blog_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block12'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $blog_tab_where, 'title' => $blog, 'sequence' => $blog_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $blog_tab_where, 'block12', $blog, $blog_order));
		}
		update_option('cp_auction_blog_tab',$blog);
		update_option('cp_auction_blog_tab_order',$blog_order);
		update_option('cp_auction_blog_tab_where',$blog_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block12'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block12' ) );
		}
		delete_option('cp_auction_blog_tab');
		delete_option('cp_auction_blog_tab_order');
		delete_option('cp_auction_blog_tab_where');
		}
		if( $category && $category_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block14'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $category_tab_where, 'title' => $category, 'sequence' => $category_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $category_tab_where, 'block14', $category, $category_order));
		}
		update_option('cp_auction_category_tab',$category);
		update_option('cp_auction_category_tab_order',$category_order);
		update_option('cp_auction_category_tab_where',$category_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block14'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block14' ) );
		}
		delete_option('cp_auction_category_tab');
		delete_option('cp_auction_category_tab_order');
		delete_option('cp_auction_category_tab_where');
		}
		if( $users_type && $userstype_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block17'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $userstype_tab_where, 'title' => $users_type, 'sequence' => $userstype_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $userstype_tab_where, 'block17', $users_type, $userstype_order));
		}
		update_option('cp_auction_intention_tab',$users_type);
		update_option('cp_auction_intention_tab_order',$userstype_order);
		update_option('cp_auction_intention_tab_where',$userstype_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block17'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block17' ) );
		}
		delete_option('cp_auction_intention_tab');
		delete_option('cp_auction_intention_tab_order');
		delete_option('cp_auction_intention_tab_where');
		}
		if( $buynow_classifieds && $buynow_classifieds_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block18'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $buynow_classifieds_tab_where, 'title' => $buynow_classifieds, 'sequence' => $buynow_classifieds_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $buynow_classifieds_tab_where, 'block18', $buynow_classifieds, $buynow_classifieds_order));
		}
		update_option('cp_auction_buynow_classifieds_tab',$buynow_classifieds);
		update_option('cp_auction_buynow_classifieds_tab_order',$buynow_classifieds_order);
		update_option('cp_auction_buynow_classifieds_tab_where',$buynow_classifieds_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block18'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block18' ) );
		}
		delete_option('cp_auction_buynow_classifieds_tab');
		delete_option('cp_auction_buynow_classifieds_tab_order');
		delete_option('cp_auction_buynow_classifieds_tab_where');
		}
		if( $buynow && $buynow_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block15'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $buynow_tab_where, 'title' => $buynow, 'sequence' => $buynow_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $buynow_tab_where,'block15', $buynow, $buynow_order));
		}
		update_option('cp_auction_buynow_tab',$buynow);
		update_option('cp_auction_buynow_tab_order',$buynow_order);
		update_option('cp_auction_buynow_tab_where',$buynow_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block15'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block15' ) );
		}
		delete_option('cp_auction_buynow_tab');
		delete_option('cp_auction_buynow_tab_order');
		delete_option('cp_auction_buynow_tab_where');
		}
		if( $closing && $closing_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block16'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $closing_tab_where, 'title' => $closing, 'sequence' => $closing_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $closing_tab_where, 'block16', $closing, $closing_order));
		}
		update_option('cp_auction_closing_tab',$closing);
		update_option('cp_auction_closing_tab_order',$closing_order);
		update_option('cp_auction_closing_tab_where',$closing_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block16'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block16' ) );
		}
		delete_option('cp_auction_closing_tab');
		delete_option('cp_auction_closing_tab_order');
		delete_option('cp_auction_closing_tab_where');
		}
		if( $contact && $contact_tab_where ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'contact'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => $contact_tab_where, 'title' => $contact, 'sequence' => $contact_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, $contact_tab_where, 'contact', $contact, $contact_order));
		}
		update_option('cp_auction_contact_tab',$contact);
		update_option('cp_auction_contact_tab_order',$contact_order);
		update_option('cp_auction_contact_tab_where',$contact_tab_where);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'contact'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'contact' ) );
		}
		delete_option('cp_auction_contact_tab');
		delete_option('cp_auction_contact_tab_order');
		delete_option('cp_auction_contact_tab_where');
		}
		$msg = ''.__('Tab settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_tab_one'])) {
		$tab_one = isset( $_POST['tab_one'] ) ? esc_attr( $_POST['tab_one'] ) : '';
		$tab_one_order = isset( $_POST['tab_one_order'] ) ? esc_attr( $_POST['tab_one_order'] ) : '';
		$cats_one = ""; if( isset( $_POST['post_category'] ) ) $cats_one = join(",", $_POST['post_category']);

		$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";

		if( $tab_one ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block8'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => 'front', 'title' => $tab_one, 'sequence' => $tab_one_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, 'front', 'block8', $tab_one, $tab_one_order));
		}
		update_option('cp_auction_menu_tab_one',$tab_one);
		update_option('cp_auction_tab_one_order',$tab_one_order);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block8'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block8' ) );
		}
		delete_option('cp_auction_menu_tab_one');
		delete_option('cp_auction_tab_one_order');
		}
		if( $cats_one ) {
		update_option('cp_auction_menu_tab_cats_one',$cats_one);
		} else {
		delete_option('cp_auction_menu_tab_cats_one');
		}
		$msg = ''.__('Settings tab #1 was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_tab_two'])) {
		$tab_two = isset( $_POST['tab_two'] ) ? esc_attr( $_POST['tab_two'] ) : '';
		$tab_two_order = isset( $_POST['tab_two_order'] ) ? esc_attr( $_POST['tab_two_order'] ) : '';
		$cats_two = ""; if( isset( $_POST['post_category'] ) ) $cats_two = join(",", $_POST['post_category']);

		$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";

		if( $tab_two ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block9'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => 'front', 'title' => $tab_two, 'sequence' => $tab_two_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, 'front', 'block9', $tab_two, $tab_two_order));
		}
		update_option('cp_auction_menu_tab_two',$tab_two);
		update_option('cp_auction_tab_two_order',$tab_two_order);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block9'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block9' ) );
		}
		delete_option('cp_auction_menu_tab_two');
		delete_option('cp_auction_tab_two_order');
		}
		if( $cats_two ) {
		update_option('cp_auction_menu_tab_cats_two',$cats_two);
		} else {
		delete_option('cp_auction_menu_tab_cats_two');
		}
		$msg = ''.__('Settings tab #2 was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_tab_three'])) {
		$tab_three = isset( $_POST['tab_three'] ) ? esc_attr( $_POST['tab_three'] ) : '';
		$tab_three_order = isset( $_POST['tab_three_order'] ) ? esc_attr( $_POST['tab_three_order'] ) : '';
		$cats_three = ""; if( isset( $_POST['post_category'] ) ) $cats_three = join(",", $_POST['post_category']);

		$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";

		if( $tab_three ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block10'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => 'front', 'title' => $tab_three, 'sequence' => $tab_three_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, 'front', 'block10', $tab_three, $tab_three_order));
		}
		update_option('cp_auction_menu_tab_three',$tab_three);
		update_option('cp_auction_tab_three_order',$tab_three_order);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block10'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block10' ) );
		}
		delete_option('cp_auction_menu_tab_three');
		delete_option('cp_auction_tab_three_order');
		}
		if( $cats_three ) {
		update_option('cp_auction_menu_tab_cats_three',$cats_three);
		} else {
		delete_option('cp_auction_menu_tab_cats_three');
		}
		$msg = ''.__('Settings tab #3 was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_tab_textbox'])) {
		$tab_textbox = isset( $_POST['tab_textbox'] ) ? esc_attr( $_POST['tab_textbox'] ) : '';
		$tab_textbox_order = isset( $_POST['tab_textbox_order'] ) ? esc_attr( $_POST['tab_textbox_order'] ) : '';
		$title = isset( $_POST['textbox_title'] ) ? esc_attr( $_POST['textbox_title'] ) : '';
		$text = isset( $_POST['textbox_text'] ) ? esc_attr( $_POST['textbox_text'] ) : '';

		$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";

		if( $tab_textbox ) {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block13'");
		if( $block ) {
		$where_array = array('id' => $block->id);
		$wpdb->update($table_name, array('position' => 'front', 'title' => $tab_textbox, 'sequence' => $tab_textbox_order), $where_array);
		} else {
		$query = "INSERT INTO {$table_name} (position, block, title, sequence) VALUES (%s, %s, %s, %d)";
		$wpdb->query($wpdb->prepare($query, 'front', 'block13', $tab_textbox, $tab_textbox_order));
		}
		update_option('cp_auction_menu_tab_textbox',$tab_textbox);
		update_option('cp_auction_tab_textbox_order',$tab_textbox_order);
		} else {
		$block = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block13'");
		if( $block ) {
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE block = '%s'", 'block13' ) );
		}
		delete_option('cp_auction_menu_tab_textbox');
		delete_option('cp_auction_tab_textbox_order');
		}
		if( $title ) {
		update_option('cp_auction_textbox_title',stripslashes(html_entity_decode($title)));
		} else {
		delete_option('cp_auction_textbox_title');
		}
		if( $text ) {
		update_option('cp_auction_textbox_text',stripslashes(html_entity_decode($text)));
		} else {
		delete_option('cp_auction_textbox_text');
		}
		$msg = ''.__('Textbox settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_frontpage_info'])) {
		$title = isset( $_POST['cp_auction_frontpage_title'] ) ? esc_attr( $_POST['cp_auction_frontpage_title'] ) : '';
		$text = isset( $_POST['cp_auction_frontpage_text'] ) ? esc_attr( $_POST['cp_auction_frontpage_text'] ) : '';
		$pos = isset( $_POST['pos'] ) ? esc_attr( $_POST['pos'] ) : '';
		if( $title ) {
		update_option('cp_auction_frontpage_title',stripslashes(html_entity_decode($title)));
		} else {
		delete_option('cp_auction_frontpage_title');
		}
		if( $text ) {
		update_option('cp_auction_frontpage_text',stripslashes(html_entity_decode($text)));
		} else {
		delete_option('cp_auction_frontpage_text');
		}
		if( $pos ) {
		update_option('cp_auction_frontpage_pos',$pos);
		} else {
		delete_option('cp_auction_frontpage_pos');
		}
		$msg = ''.__('Frontpage title & text was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_front_info'])) {
		$title = isset( $_POST['cp_auction_front_title'] ) ? esc_attr( $_POST['cp_auction_front_title'] ) : '';
		$text = isset( $_POST['cp_auction_front_text'] ) ? esc_attr( $_POST['cp_auction_front_text'] ) : '';
		if( $title ) {
		update_option('cp_auction_front_title',stripslashes(html_entity_decode($title)));
		} else {
		delete_option('cp_auction_front_title');
		}
		if( $text ) {
		update_option('cp_auction_front_text',stripslashes(html_entity_decode($text)));
		} else {
		delete_option('cp_auction_front_text');
		}
		$msg = ''.__('Frontpage title & text was saved!','auctionAdmin').'';
		}
		$list_one = get_option('cp_auction_menu_tab_cats_one');
		$cats_one = explode( ',', $list_one );
		$list_two = get_option('cp_auction_menu_tab_cats_two');
		$cats_two = explode( ',', $list_two );
		$list_three = get_option('cp_auction_menu_tab_cats_three');
		$cats_three = explode( ',', $list_three );
?>

	<?php if($msg) echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$msg.'</strong></p></div>'; ?>

<style>
.form-table td {
	vertical-align:top;
}
</style>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Customized Front & Author Page Settings', 'auctionAdmin'); ?></h3>
            <div class="inside">

	<div class="clear10"></div>

	<?php _e('To help you include classified, wanted and auction ads in your home/frontpage & authorpage we have included a modified "tpl-ads-home.php", "author.php" and "archive-ad_listing.php" file for the ClassiPress theme and most of our supported child themes. Just set the below settings and your new frontpage and/or author will be created automatically.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<?php _e('You can also do some modifications to the single ad page and it`s sidebar, just go to the Misc tab or, <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=misc">click here</a>.', 'auctionAdmin'); ?>

	<div class="clear20"></div>

<form method="post" action="">

			<table class="form-table">
				<tbody>
				        <tr valign="top">
        					<th scope="row"><?php _e('Enable Custom Frontpage:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_enable_frontpage"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_frontpage')); ?></select> <?php _e('Automatically replace the standard frontpage with the custom frontpage.', 'auctionAdmin'); ?><br /><small><?php _e('<strong>Note</strong>: Please set the tab settings below before you enable the custom frontpage, otherwise you will get a blank frontpage with no menu tabs.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
        					<th scope="row"><?php _e('Enable Custom Author Page:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_enable_authorpage"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_authorpage')); ?></select> <?php _e('Automatically replace the standard author page with the custom authorpage.', 'auctionAdmin'); ?><br /><small><?php _e('<strong>Note</strong>: Please set the tab settings below before you enable the custom authorpage, otherwise you will get a blank authorpage with no menu tabs.', 'auctionAdmin'); ?></small></td>
					</tr>

<?php if( get_option('stylesheet') != "jibo" ) { ?>

					<tr valign="top">
        					<th scope="row"><?php _e('Slide In Effect:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_slide_in_ads"><?php echo cp_auction_yes_no(get_option('cp_auction_slide_in_ads')); ?></select> <?php _e('Slide in effect on tab content.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option to have the tab content slide in/out to the right.', 'auctionAdmin'); ?></small></td>
					</tr>

<?php } ?>

			        	<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Author Permalink Base:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_author_page_slug" value="<?php echo get_option('cp_auction_author_page_slug'); ?>"> <?php _e('Change the value of the author permalink base.', 'auctionAdmin'); ?><br /><small><?php _e('This option is used to change the value of the author permalink base to whatever you want here.', 'auctionAdmin'); ?><br /><strong><?php _e('Your URL', 'auctionAdmin'); ?></strong>: <?php echo get_author_posts_url($current_user->ID); ?> (<?php _e('you may need to refresh this page to see the changes', 'auctionAdmin'); ?>)<br /><br /><?php _e('<strong>NOTE:</strong> After you update the permalink base, be sure to visit your <a style="text-decoration: none;" href="../wp-admin/options-permalink.php">permalink settings</a> and click "save" to flush your rewrite rules.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="enable_frontpage" id="submit" class="button-primary" value="<?php _e('Save Changes', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

<form method="post" action="">

			<table class="form-table">
				<tbody>
				        <tr valign="top">
        					<th scope="row"><?php _e('Toggle Categories:', 'auctionAdmin'); ?></th>
						<td><select name="toggle_cats"><?php echo cp_auction_yes_no(get_option('cp_auction_toggle_cats')); ?></select> <?php _e('Showw / hide the category list on your frontpage.', 'auctionAdmin'); ?><br /><small><?php _e('If you have set the ClassiPress layout to Directory Style then you can enable this option to display a toggle button for your users to toggle the category listing. Useful if you have many categories enabled.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="save_toggle" id="submit" class="button-primary" value="<?php _e('Save Toggle Settings', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

<?php if( get_option('stylesheet') == "redrondell" || get_option('stylesheet') == "rondell_370" || get_option('stylesheet') == "rondell" ) { ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>
				        <tr valign="top">
        					<th scope="row"><?php _e('Remove Banner #1:', 'auctionAdmin'); ?></th>
						<td><select name="rondell_banner"><?php echo cp_auction_yes_no(get_option('cp_auction_rondell_banner')); ?></select><br /><small><?php _e('Remove the top banner block in Rondell`s frontpage.', 'auctionAdmin'); ?></small></td>
					</tr>
				        <tr valign="top">
        					<th scope="row"><?php _e('Remove Banner #2:', 'auctionAdmin'); ?></th>
						<td><select name="rondell_banner_bottom"><?php echo cp_auction_yes_no(get_option('cp_auction_rondell_banner_bottom')); ?></select><br /><small><?php _e('Remove the bottom banner block in Rondell`s frontpage.', 'auctionAdmin'); ?></small></td>
					</tr>
					<tr valign="top">
        					<th scope="row"><?php _e('Remove Searchbar:', 'auctionAdmin'); ?></th>
						<td><select name="rondell_searchbar"><?php echo cp_auction_yes_no(get_option('cp_auction_rondell_searchbar')); ?></select><br /><small><?php _e('Remove the searchbar block in Rondell`s frontpage.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="save_misc" id="submit" class="button-primary" value="<?php _e('Save Misc Settings', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

<?php } ?>

	<div class="admin_header"><?php _e('Select which menu tabs to display', 'auctionAdmin'); ?></div>
	<?php _e('Select position and enter a title for activating the appropriate ad types as menu tabs on the custom frontpage and/or authorpage. Leave blank for no tabs. <strong>Tab types marked with * has separate queries against the database, do not overload your website by activating too many of these.</strong>', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>

<?php if( get_option('stylesheet') == "jibo" ) { ?>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Title of JIBO Welcome:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_jibo_tab_where">
						<option value="" <?php if(get_option('cp_auction_jibo_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_jibo_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_jibo_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_jibo_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="jibo" value="<?php echo get_option( 'cp_auction_jibo_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Welcome, <strong>ONLY Jibo child theme</strong>.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="jibo_order" value="<?php echo get_option( 'cp_auction_jibo_tab_order' ); ?>" size="5"> <strong><?php _e('Set the order in which tabs to appear.', 'auctionAdmin'); ?></strong></td>
					</tr>
					</tr>

<?php } ?>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Just Listed Ads Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_listed_tab_where">
						<option value="" <?php if(get_option('cp_auction_listed_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_listed_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_listed_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_listed_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="listed" value="<?php echo get_option( 'cp_auction_listed_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Just Listed', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="listed_order" value="<?php echo get_option( 'cp_auction_listed_tab_order' ); ?>" size="5"> <?php if( get_option('stylesheet') != "jibo" ) { ?><strong><?php _e('Set the order in which tabs to appear.', 'auctionAdmin'); ?></strong><?php } ?></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Featured Ads Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_featured_tab_where">
						<option value="" <?php if(get_option('cp_auction_featured_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_featured_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_featured_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_featured_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="featured" value="<?php echo get_option( 'cp_auction_featured_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Featured', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="featured_order" value="<?php echo get_option( 'cp_auction_featured_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Popular Ads Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_popular_tab_where">
						<option value="" <?php if(get_option('cp_auction_popular_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_popular_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_popular_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_popular_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="popular" value="<?php echo get_option( 'cp_auction_popular_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Popular', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="popular_order" value="<?php echo get_option( 'cp_auction_popular_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Classified Ads Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_classified_tab_where">
						<option value="" <?php if(get_option('cp_auction_classified_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_classified_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_classified_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_classified_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="classified" value="<?php echo get_option( 'cp_auction_classified_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Classified', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="classified_order" value="<?php echo get_option( 'cp_auction_classified_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Wanted Ads Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_wanted_tab_where">
						<option value="" <?php if(get_option('cp_auction_wanted_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_wanted_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_wanted_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_wanted_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="wanted" value="<?php echo get_option( 'cp_auction_wanted_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Wanted', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="wanted_order" value="<?php echo get_option( 'cp_auction_wanted_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Auction Ads Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_auction_tab_where">
						<option value="" <?php if(get_option('cp_auction_auction_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_auction_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_auction_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_auction_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="auction" value="<?php echo get_option( 'cp_auction_auction_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Auctions', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="auction_order" value="<?php echo get_option( 'cp_auction_auction_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Sold Ads Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_sold_ads_tab_where">
						<option value="" <?php if(get_option('cp_auction_sold_ads_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_sold_ads_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_sold_ads_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_sold_ads_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="sold_ads" value="<?php echo get_option( 'cp_auction_sold_ads_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Sold Ads. Display all ads with status as sold.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="sold_ads_order" value="<?php echo get_option( 'cp_auction_sold_ads_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Categories Title:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_categories_tab_where">
						<option value="" <?php if(get_option('cp_auction_categories_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_categories_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_categories_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_categories_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="categories" value="<?php echo get_option( 'cp_auction_categories_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Categories. Display all categories.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="categories_order" value="<?php echo get_option( 'cp_auction_categories_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Blog Posts Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_blog_tab_where">
						<option value="" <?php if(get_option('cp_auction_blog_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_blog_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_blog_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_blog_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="blog" value="<?php echo get_option( 'cp_auction_blog_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Blog. Display blog posts.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="blog_order" value="<?php echo get_option( 'cp_auction_blog_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('List By Category Title:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_category_tab_where">
						<option value="" <?php if(get_option('cp_auction_category_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_category_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_category_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_category_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="category" value="<?php echo get_option( 'cp_auction_category_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Categorized. Allow users to display a specific ad type by category.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="category_order" value="<?php echo get_option( 'cp_auction_category_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Users Ad Type Title:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_intention_tab_where">
						<option value="" <?php if(get_option('cp_auction_intention_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_intention_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_intention_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_intention_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="users_type" value="<?php echo get_option( 'cp_auction_intention_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Search by Ad Type. Allow users to display a specific users set ad type, by category.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="userstype_order" value="<?php echo get_option( 'cp_auction_intention_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Buy Now Classifieds Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_buynow_classifieds_tab_where">
						<option value="" <?php if(get_option('cp_auction_buynow_classifieds_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_buynow_classifieds_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_buynow_classifieds_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_buynow_classifieds_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="buynow_classifieds" value="<?php echo get_option( 'cp_auction_buynow_classifieds_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Buy it Now Ads. Display auctions with buy now price.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="buynow_classifieds_order" value="<?php echo get_option( 'cp_auction_buynow_classifieds_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Buy Now Auctions Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_buynow_tab_where">
						<option value="" <?php if(get_option('cp_auction_buynow_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_buynow_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_buynow_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_buynow_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="buynow" value="<?php echo get_option( 'cp_auction_buynow_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Buy Now Auctions. Display auctions with buy now price.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="buynow_order" value="<?php echo get_option( 'cp_auction_buynow_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Closing Soon Title *:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_closing_tab_where">
						<option value="" <?php if(get_option('cp_auction_closing_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="front" <?php if(get_option('cp_auction_closing_tab_where') == "front") echo 'selected="selected"'; ?>><?php _e('Frontpage', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_closing_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						<option value="both" <?php if(get_option('cp_auction_closing_tab_where') == 'both') echo 'selected="selected"'; ?>><?php _e('Both Pages', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="closing" value="<?php echo get_option( 'cp_auction_closing_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Closing Soon. Display soon to close auctions.', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="closing_order" value="<?php echo get_option( 'cp_auction_closing_tab_order' ); ?>" size="5"></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Contact Author Title:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_contact_tab_where">
						<option value="" <?php if(get_option('cp_auction_contact_tab_where') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="author" <?php if(get_option('cp_auction_contact_tab_where') == "author") echo 'selected="selected"'; ?>><?php _e('Author Page', 'auctionAdmin'); ?></option>
						</select></td>
						<td width="200px"><input type="text" name="contact" value="<?php echo get_option( 'cp_auction_contact_tab' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Contact. Allow visitors to contact author from the author page (only custom author page).', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="contact_order" value="<?php echo get_option( 'cp_auction_contact_tab_order' ); ?>" size="5"></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="save_tabs" id="submit" class="button-primary" value="<?php _e('Save Tab Settings', 'auctionAdmin'); ?>"  /></p>

		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Display custom menu tab(s) on the front page and display ads from the selected categories', 'auctionAdmin'); ?></div>
	<?php _e('This function will create additional menu tab(s) on the site`s front page, you choose from which categories you want the ads / content to be presented. If you want to only show ads with eg. musical content then enter the title eg. Music, and select the categories that are appropriate for advertising music.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Title of Tab #1:', 'auctionAdmin'); ?></th>
						<td width="200px"><input type="text" name="tab_one" value="<?php echo get_option( 'cp_auction_menu_tab_one' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Music', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="tab_one_order" value="<?php echo get_option( 'cp_auction_tab_one_order' ); ?>" size="5"> <strong><?php _e('Set the order in which tabs to appear.', 'auctionAdmin'); ?></strong></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Select Categories:', 'auctionAdmin'); ?></th>
						<td colspan="2">
						<div id="form-categorydiv">
						<div class="tabs-panel" id="categories-all" style="">
						<ul class="list:category categorychecklist form-no-clear" id="categorychecklist">
						<?php echo cp_category_checklist($cats_one); ?>
						</ul>
						</div>
						</div></td>
					</tr>
				</tbody>
			</table>
			<p class="submit"><input type="submit" name="save_tab_one" id="submit" class="button-primary" value="<?php _e('Save Tab One', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="dotted"></div>

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Title of Tab #2:', 'auctionAdmin'); ?></th>
						<td width="200px"><input type="text" name="tab_two" value="<?php echo get_option( 'cp_auction_menu_tab_two' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Music', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="tab_two_order" value="<?php echo get_option( 'cp_auction_tab_two_order' ); ?>" size="5"> <strong><?php _e('Set the order in which tabs to appear.', 'auctionAdmin'); ?></strong></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Select Categories:', 'auctionAdmin'); ?></th>
						<td colspan="2">
						<div id="form-categorydiv">
						<div class="tabs-panel" id="categories-all" style="">
						<ul class="list:category categorychecklist form-no-clear" id="categorychecklist">
						<?php echo cp_category_checklist($cats_two); ?>
						</ul>
						</div>
						</div></td>
					</tr>
				</tbody>
			</table>
			<p class="submit"><input type="submit" name="save_tab_two" id="submit" class="button-primary" value="<?php _e('Save Tab Two', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="dotted"></div>

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Title of Tab #3:', 'auctionAdmin'); ?></th>
						<td width="200px"><input type="text" name="tab_three" value="<?php echo get_option( 'cp_auction_menu_tab_three' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. Music', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="tab_three_order" value="<?php echo get_option( 'cp_auction_tab_three_order' ); ?>" size="5"> <strong><?php _e('Set the order in which tabs to appear.', 'auctionAdmin'); ?></strong></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Select Categories:', 'auctionAdmin'); ?></th>
						<td colspan="2">
						<div id="form-categorydiv">
						<div class="tabs-panel" id="categories-all" style="">
						<ul class="list:category categorychecklist form-no-clear" id="categorychecklist">
						<?php echo cp_category_checklist($cats_three); ?>
						</ul>
						</div>
						</div></td>
					</tr>
				</tbody>
			</table>
			<p class="submit"><input type="submit" name="save_tab_three" id="submit" class="button-primary" value="<?php _e('Save Tab Three', 'auctionAdmin'); ?>"  /></p>
 
		</form>
	
	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Display a custom menu tab on the front page and add any text to display', 'auctionAdmin'); ?></div>
	<?php _e('This function will create an additional menu tab on the site`s front page. Enter a title and any text / information that you want to make visible in this tab on the front page of your website.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Title of Tab TextBox:', 'auctionAdmin'); ?></th>
						<td width="200px"><input type="text" name="tab_textbox" value="<?php echo get_option( 'cp_auction_menu_tab_textbox' ); ?>" size="40"><br /><small><?php _e('Set the title You want to display in the tab .i.e. News', 'auctionAdmin'); ?></small></td>
						<td><input type="text" name="tab_textbox_order" value="<?php echo get_option( 'cp_auction_tab_textbox_order' ); ?>" size="5"> <strong><?php _e('Set the order in which tabs to appear.', 'auctionAdmin'); ?></strong></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('TextBox Header:', 'auctionAdmin'); ?></th>
						<td colspan="2"><input type="text" name="textbox_title" value="<?php echo get_option( 'cp_auction_textbox_title' ); ?>" size="40"> <?php _e('(Optional)', 'auctionAdmin'); ?><br /><small><?php _e('Set the header You want to display in the textbox .i.e. Latest News (optional)', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Text / Information:', 'auctionAdmin'); ?></th>
						<td colspan="2"><textarea class="options" name="textbox_text" rows="5" cols="80"><?php echo get_option('cp_auction_textbox_text'); ?></textarea>
						<br /><small><?php _e('Enter the text / information you want to make visible in this tab on the front page of your website. HTML can be used.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>
			<p class="submit"><input type="submit" name="save_tab_textbox" id="submit" class="button-primary" value="<?php _e('Save Tab TextBox', 'auctionAdmin'); ?>"  /></p>

		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Box #1 - Informative texture / box that will appear on the front page.', 'auctionAdmin'); ?></div>
	<?php _e('Enter a title and any text / information that you want to make visible on the front page of your website.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Info Title:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_frontpage_title" value="<?php echo get_option('cp_auction_frontpage_title'); ?>" size="40">
						<br /><small><?php _e('Enter your choice of title for the info box to display on the front page of your website.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Text / Information:', 'auctionAdmin'); ?></th>
						<td><textarea class="options" name="cp_auction_frontpage_text" rows="5" cols="80"><?php echo get_option('cp_auction_frontpage_text'); ?></textarea>
						<br /><small><?php _e('Enter the text / information you want to make visible on the front page of your website. HTML can be used.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Position:', 'auctionAdmin'); ?></th>
						<td><input type="radio" name="pos" value="above_menu" <?php if(get_option('cp_auction_frontpage_pos') == 'above_menu') echo 'checked="checked"'; ?>> <?php _e('Just above the tabbed menu, below category box.', 'auctionAdmin'); ?><br />
						<input type="radio" name="pos" value="above_cats" <?php if(get_option('cp_auction_frontpage_pos') == 'above_cats') echo 'checked="checked"'; ?>> <?php _e('Just above the category box, below featured slider.', 'auctionAdmin'); ?><br />
						<?php if( get_option('stylesheet') != "flatpress" ) { ?>
						<input type="radio" name="pos" value="above_slider" <?php if(get_option('cp_auction_frontpage_pos') == 'above_slider') echo 'checked="checked"'; ?>> <?php _e('Fullwidth, above the featured slider.', 'auctionAdmin'); ?>
						<?php } ?>
						</td>
					</tr>
				</tbody>
			</table>
			<p class="submit"><input type="submit" name="save_frontpage_info" id="submit" class="button-primary" value="<?php _e('Save Frontpage Box #1', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Box #2 - Informative texture / box that will appear on the front page.', 'auctionAdmin'); ?></div>
	<?php _e('Enter a title and any text / information that you want to make visible just above the menu tabs on the front page of your website. Consider this as an option in addition to Box #1.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Info Title:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_front_title" value="<?php echo get_option('cp_auction_front_title'); ?>" size="40">
						<br /><small><?php _e('Enter your choice of title for the info box to display on the front page of your website.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Text / Information:', 'auctionAdmin'); ?></th>
						<td><textarea class="options" name="cp_auction_front_text" rows="5" cols="80"><?php echo get_option('cp_auction_front_text'); ?></textarea>
						<br /><small><?php _e('Enter the text / information you want to make visible just above the menu tabs on the front page of your website. HTML can be used.', 'auctionAdmin'); ?></small></td>
					</tr>

				</tbody>
			</table>

			<p class="submit"><input type="submit" name="save_front_info" id="submit" class="button-primary" value="<?php _e('Save Frontpage Box #2', 'auctionAdmin'); ?>"  /></p>

		</form>

	<div class="clear10"></div>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}