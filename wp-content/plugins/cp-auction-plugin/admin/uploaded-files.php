<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

include_once(ABSPATH.'/wp-content/plugins/cp-auction-plugin/scripts/pagination.class.php');

function cp_auction_plugin_upload_history() {

	ob_start();

	global $wpdb, $cpurl;
	$limit = "";
	$act = get_option('cp_auction_plugin_auctiontype');
	
	$fullpath = cp_auction_plugin_upload_dir();

if ( ! defined( 'BASE_DIR' ) ) {
	define( 'BASE_DIR', $fullpath );
}
?>

<!--
|--------------------------------------------------------------------------
| UPLOADED FILES
|--------------------------------------------------------------------------
-->

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
	if( isset( $_GET['uid'] ) ) {
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE uid = {$_GET['uid']} AND type = 'upload'" );
	} else {
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE type = 'upload'" );
	}

	if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-downloads");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
	if( isset( $_GET['uid'] ) ) {
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_author = {$_GET['uid']} AND type = 'upload' ORDER BY id DESC {$limit}");
	} else {
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE type = 'upload' ORDER BY id DESC {$limit}");
	}
	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Ad Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Author', 'auctionAdmin'); ?></th>
        <th><?php _e('Filename', 'auctionAdmin'); ?></th>
        <th><?php _e('Size', 'auctionAdmin'); ?></th>
        <th><?php _e('IP Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Ad Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Author', 'auctionAdmin'); ?></th>
        <th><?php _e('Filename', 'auctionAdmin'); ?></th>
        <th><?php _e('Size', 'auctionAdmin'); ?></th>
        <th><?php _e('IP Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
            $id        	= $row->id;
            $pid        = $row->pid;
            $uid	= get_userdata($row->post_author);
            $fname     	= $row->fname;
            $filename   = $row->filename;
            $fullpath	= $row->fullpath;
            $size  	= "".round($row->size / 1024)." KB";
            $IP      	= $row->ip;
            $date      	= date('d-M-Y',$row->datemade);
            
            $post = get_post($pid);
            $status = $post->post_status;
            $title = $post->post_title;

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td>#'.$id.'</td>';
            if( empty( $title )) {
            echo '<td>'.__('N/A', 'auctionAdmin').'</td>';
            } else {
            echo '<td><a href="'.get_permalink( $pid ).'" target="_blank">'.$title.'</a></td>';
            }
            if( empty( $uid->user_login )) {
            echo '<td>'.__('N/A', 'auctionAdmin').'</td>';
            } else {
            echo '<td><a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$row->post_author.'').'" target="_blank">'.$uid->user_login.'</a></td>';
            }
            echo '<td>'.$fname.'</td>';
            echo '<td>'.$size.'</td>';
            echo '<td>'.$IP.'</td>';
            echo '<td>'.$date.'</td>';
            echo '<td><a href="#" class="delete-uploads" id="'.$id.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="8"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">
<font color="red"><strong>*</strong></font> <small><?php _e('<strong>WARNING</strong>: Deletion of upload history will also delete the uploaded files.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
}
ob_end_flush();
}