<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_plugin_setup_instructions() {

?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Setup Instructions', 'auctionAdmin'); ?></h3>
            <div class="inside">
	    <div class="clear10"></div>

	<div class="admin_header"><?php _e('BASIC SETTINGS', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('I am working on some better instructions for the basics of the plugin.', 'auctionAdmin'); ?></div>

	<div class="admin_header"><?php _e('MENU SETTINGS', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('You should setup an custom menu in menu settings which you find by going to Apperance -> Menus. Create a new menu and name it eg. Dashboard, then go to Manage Locations and attach the new menu you just created to the Theme Dashboard and save. You now have a new customized User Options menu ready to set up with your user options links.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('If you now have set up all the necessary pages using the', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=pages"><strong><?php _e('page creator', 'auctionAdmin'); ?></strong></a> <?php _e('you will find these pages in the left box labeled <strong>Pages</strong>. Select the pages you want added to the menu and click the button <strong>Add to Menu</strong>.', 'auctionAdmin'); ?></div>

	<div class="clear20"></div>

	<div class="admin_header"><?php _e('REPLACE THE POST AN AD BUTTON', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('If you have chosen to use the redirect option in general settings or, if you want to use the standard Classified Ad Type ONLY then you do not need to do any changes to the button code.', 'auctionAdmin'); ?>', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('If you have chosen to not use the redirect option in general settings and want to use other ad types than the standard classified ad type then you have to replace the entire ClassiPress button/code line with the below code. Setup all the <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=pages"> <strong>new pages</strong></a> and replace the url code with your new <strong>Choose Ad Type</strong> page url, eg. <strong>http://www.mysite.com/post-new/</strong>, see the settings below.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('<strong>ClassiPress v3.3</strong> replace line 72 in classipress theme file <font color="navy">header.php</font> with the below code, header.php is to be found in themes/classipress.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('<strong>Classiclean</strong> if you are using classiclean child theme replace line 63 in classiclean child theme file <font color="navy">themes/classiclean/header.php</font> with the below code, header.php is to be found in <font color="navy">themes/classiclean/header.php</font>.  Also change the button class <font color="red">btn_orange</font> to <font color="red">btn_orange_classiclean</font>.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('<strong>Option #1.</strong> works with all ad types. (recommended unless redirect option in general settings has been enabled)', 'auctionAdmin'); ?></div>

	<div class="code_box">
&lt;a href="&lt;?php echo get_bloginfo('url'); ?&gt;/post-new/" class="obtn btn_orange"&gt;&lt;?php _e('Post an Ad', 'auctionAdmin') ?&gt;&lt;/a&gt;</div>

	<div class="clear20"></div>

	<div class="admin_text"><?php _e('<strong>Option #2.</strong> works with only one ad type. (disables the ad type selection)', 'auctionAdmin'); ?></div>

	<div class="admin_text"><strong><?php _e('If you want to use only one ad type you must add the following hash to the post an ad button url:', 'auctionAdmin'); ?></strong><br /><?php _e('Classified Ads:', 'auctionAdmin'); ?> <font color="navy"> ?type=classified</font><br />
  <?php _e('Wanted:', 'auctionAdmin'); ?> <font color="navy"> ?type=wanted</font><br />
  <?php _e('Normal Auctions:', 'auctionAdmin'); ?> <font color="navy"> ?type=normal</font><br />
  <?php _e('Reverse Auctions:', 'auctionAdmin'); ?> <font color="navy"> ?type=reverse</font><br /><br />
  <?php _e('The example below is setup for classified ads only. Replace the hash tag with your choice of ad type.', 'auctionAdmin'); ?></div>

	<div class="code_box">
&lt;a href="&lt;?php echo get_bloginfo('url'); ?&gt;/add-new/?type=classified" class="obtn btn_orange"&gt;&lt;?php _e('Post an Ad', 'auctionAdmin') ?&gt;&lt;/a&gt;</div>

	<div class="clear30"></div>

	<div class="admin_header"><?php _e('DISPLAY CLASSIFIEDS, WANTED AND AUCTION ADS IN HOME / FRONTPAGE', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('To help you include classified, wanted and auction ads in your home/frontpage we have included a modified "tpl-ads-home.php" and "archive-ad_listing.php" file for the ClassiPress theme and most of our supported child themes. Just set the <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=frontpage"><strong>Frontpage</strong></a> settings and your new frontpage will be created automatically.', 'auctionAdmin'); ?></div>

	<div class="admin_header"><?php _e('SETUP OUR CUSTOMIZED AUTHORS PAGE', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('Replace the standard author page with a customized page including a tabbed menu displaying all ads by actual author', 'auctionAdmin'); ?>, <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=misc"><strong><?php _e('check out', 'auctionAdmin'); ?></strong></a> <?php _e('the Misc tab for the required settings.', 'auctionAdmin'); ?></div>

	<div class="admin_header"><?php _e('SETUP OUR CUSTOMIZED AD SIDEBAR', 'auctionAdmin'); ?></div>
	<div class="admin_text"><a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=misc"><strong><?php _e('Check out', 'auctionAdmin'); ?></strong></a> <?php _e('the Misc tab for the required settings.', 'auctionAdmin'); ?></div>
	
	<div class="admin_header"><?php _e('CSS STYLE, HEADER AND FOOTER', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('The main .css file can be found at the following path/directory, <font color="navy">wp-content/plugins/cp-auction-plugin/css/style.css</font>, this can also be edited by using the plugin editor. There is separate css files for all the supported child themes, and also a <font color="navy">custom.css</font> for the non supported child themes.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('The header and footer is controlled by <font color="navy">wrapper.php</font> wich is located in <font color="navy">wp-content/plugins/cp-auction-plugin/scripts/wrapper.php</font>. CP Auction plugin has a built in custom call wich means you can create the following files, <font color="navy">cpauction-wrapper.php</font> and <font color="navy">cpauction.css</font> and upload these into the <font color="navy">wp-content/plugins/</font> directory, you can then use your own header, footer and .css without the need to backup your changes each time CP Auction plugin has an update.', 'auctionAdmin'); ?> <strong><?php _e('To activate your own header, footer and custom .css', 'auctionAdmin'); ?>, <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=misc"><?php _e('click here', 'auctionAdmin'); ?></a></strong>.</div>

	<div class="admin_header"><?php _e('AUCTION COUNTDOWN TIMER', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('Make sure that your date and timing is set in WP -> General Settings to follow <strong>UTC</strong>. Go to the WP general settings and set your timezone by UTC and NOT by City / location.', 'auctionAdmin'); ?></div>

	<div class="admin_header"><?php _e('PRIVATE MESSENGER - CARTPAUJ PM', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('There are a few things you need to think about before you create the page as described in Cartpauj settings, this MUST use the same page slug that you use for your CP Auctions userpanel, in other words, the Userpanel page which you have created using the page creator under the Pages tab in CP Auction settings. Paste the shortcode [cartpauj-pm] into the page content (under the HTML tab of the page editor) of the page you have created for the frontend userpanel.', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=pages"><?php _e('Click here', 'auctionAdmin'); ?></a>, <?php _e('to create the new pages.', 'auctionAdmin'); ?></div>

	<div class="admin_header"><?php _e('TRANSLATION / LOCALIZATION', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('Our plugins comes localisation ready out of the box – all that’s needed is your translation (if the plugin does not come bundled with one for your language).', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('There are several methods to create a translation, most of which are outlined in the WordPress Codex, however, we find the easiest method is to use a plugin called', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="http://wordpress.org/extend/plugins/codestyling-localization/" target="_blank"><?php _e('codestyling localisation', 'auctionAdmin'); ?></a>.</div>

	<div class="admin_text"><?php _e('CP Auction includes a language file (.po file) which contains all of the US / English text. You can find this language file inside the plugin folder in /cp-auction-plugin/languages/.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('Codestyling Localization is the method we recommend for most users as it is the simplest to setup. Install and activate the plugin and go to Tools > Localization (shown in your language), then follow the instructions on how to translate the plugin.', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="http://www.code-styling.de/english/development/wordpress-plugin-codestyling-localization-en" target="_blank"><?php _e('Click here', 'auctionAdmin'); ?></a>, <?php _e('to read the instructions.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('<strong>Important</strong>: You can ignore the message in codestyling which states “Loading problem: Author is using load_textdomain instead of load_plugin_textdomain.” We use both methods to ensure maximum compatibility.', 'auctionAdmin'); ?></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('MAKING YOUR LOCALIZATION UPGRADE SAFE', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('If you keep your custom translations in /cp-auction-plugin/languages/ they can be lost when upgrading. To make them upgrade safe, instead place them in wp-content/languages/cp-auction-plugin/', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('<strong>Note</strong>, once moved you cannot use Codestyling Localization plugin to edit your moved localizations - you need to move them back to make changes.', 'auctionAdmin'); ?></div>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}