<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

include_once(ABSPATH.'/wp-content/plugins/cp-auction-plugin/scripts/pagination.class.php');

function cp_auction_plugin_transactions() {

	global $siteinfo, $post, $cpurl, $wpdb, $cp_options;

	$limit = "";
	$act = get_option('cp_auction_plugin_auctiontype');
	$cc = $cp_options->currency_code;
	$class = ""; if( isset( $_GET['status'] ) ) $class = $_GET['status'];
	$ad_type = ""; if( isset( $_GET['type'] ) ) $ad_type = $_GET['type'];
	$lc = get_option('cp_auction_paypal_lc');

	$current = isset($_GET['tab']) ? $_GET['tab'] : 'purchased';
	if(isset($_GET['tab'])) $current = $_GET['tab']; ?>

	<div class="wrap">
	<div class="icon32" id="icon-edit-pages"><br/></div>
	<h2><?php _e('Transactions', 'auctionAdmin'); ?></h2>

<?php
    $tabs = array( 'purchased' => __('Credits with PayPal', 'auctionAdmin'), 'btpurchased' => __('Credits with Bank Transfer', 'auctionAdmin'), 'escrow' => __('Escrow Payments', 'auctionAdmin'), 'verification' => __('Verifications', 'auctionAdmin'), 'bump_ad' => __('Bumped Ads', 'auctionAdmin'), 'upgrade' => __('Featured Upgrades', 'auctionAdmin') );
    $links = array();
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) :
            $links[] = "<a class='nav-tab nav-tab-active' href='?page=cp-transactions&tab=$tab'>$name</a>";
        else :
            $links[] = "<a class='nav-tab' href='?page=cp-transactions&tab=$tab'>$name</a>";
        endif;
    endforeach;
    echo '<h2 class="nav-tab-wrapper">';
    foreach ( $links as $link )
        echo $link;
    echo '</h2>';

    if ( isset ( $_GET['tab'] ) ) : $tab = $_GET['tab'];
    else:
        $tab = 'purchased';
    endif;
?>


<!--
|--------------------------------------------------------------------------
| CREDITS PAID WITH PAYPAL
|--------------------------------------------------------------------------
-->

		<?php if($tab == 'purchased') { ?>
			<div id="poststuff">

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE payment_option = 'paypal' AND type = 'credits_purchase'" );

if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-transactions");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>
<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE payment_option = 'paypal' AND type = 'credits_purchase' ORDER BY id DESC {$limit}");
	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Username', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Type', 'auctionAdmin'); ?></th>
        <th><?php _e('Quantity', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Fees', 'auctionAdmin'); ?></th>
        <th><?php _e('Gross', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Available', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Username', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Type', 'auctionAdmin'); ?></th>
        <th><?php _e('Quantity', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Fees', 'auctionAdmin'); ?></th>
        <th><?php _e('Gross', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Available', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
            $id        = $row->id;
            $pid       = $row->pid;
            $uid       = $row->uid;
            $email     = $row->payer_email;
            $itemname  = $row->item_name;
            $currency  = $row->mc_currency;
            $quantity  = cp_auction_format_amount($row->moved_credits, 'process');
            $available = cp_auction_format_amount($row->available, 'process');
            $fees      = cp_auction_format_amount($row->mc_fee);
            $price     = cp_auction_format_amount($row->mc_gross);
            $transid   = $row->txn_id;
            $date      = date_i18n('Y-m-d H:i:s', $row->payment_date, true);
            $status    = $row->status;
            $user = get_userdata($uid);
            $fullname = ''.$user->first_name.' '.$user->last_name.'';

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td>#'.$id.'</td>';
            echo '<td><a title="'.__('Click here to contact', 'auctionAdmin').' '.$fullname.'" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.'').'" target="_blank">'.$user->user_login.'</a></td>';
            echo '<td><a href="mailto:'.$email.'">'.$email.'</td>';
            echo '<td>'.$itemname.'</td>';
            echo '<td>'.$quantity.' '.__('credits', 'auctionAdmin').'</td>';
            echo '<td><a href="#" class="update-credit-status" title="'.__('Update credit status.', 'auctionAdmin').'" id="'.$id.'">'.$status.'</a></td>';
            if( !$fees ) $fees = '0.00';
            echo '<td>'.$fees.'</td>';
            echo '<td>'.$price.'</td>';
            echo '<td>'.$transid.'</td>';
            echo '<td>'.$date.'</td>';
            echo '<td>'.$available.' '.__('credits', 'auctionAdmin').' | <a href="#" class="trans-delete-credits" title="'.__('Delete transaction and credits.', 'auctionAdmin').'" id="'.$id.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="10"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>

<div class="tablenav">
<font color="red"><strong>*</strong></font> <small><?php _e('Once payment is received, click on the status pending, the transaction will change the status to complete and purchased credits are automatically transferred to the user`s account.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
} ?>

<div class="clear20"></div>
      </div><!-- /purchased - tab_content -->

<!--
|--------------------------------------------------------------------------
| CREDITS PAID WITH BANK TRANSFER
|--------------------------------------------------------------------------
-->

		<?php } if($tab == 'btpurchased') { ?>
			<div id="poststuff">

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE payment_option = 'bank_transfer' AND type = 'credits_purchase'" );

if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-transactions&tab=btpurchased");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE payment_option = 'bank_transfer' AND type = 'credits_purchase' ORDER BY id DESC {$limit}");
	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Username', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Type', 'auctionAdmin'); ?></th>
        <th><?php _e('Quantity', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Fees', 'auctionAdmin'); ?></th>
        <th><?php _e('Gross', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Available', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Username', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Type', 'auctionAdmin'); ?></th>
        <th><?php _e('Quantity', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Fees', 'auctionAdmin'); ?></th>
        <th><?php _e('Gross', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Available', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
            $id        = $row->id;
            $pid       = $row->pid;
            $uid       = $row->uid;
            $email     = $row->payer_email;
            $itemname  = $row->item_name;
            $currency  = $row->mc_currency;
            $quantity  = cp_auction_format_amount($row->moved_credits, 'process');
            $available = cp_auction_format_amount($row->available, 'process');
            $fees      = cp_auction_format_amount($row->mc_fee);
            $price     = cp_auction_format_amount($row->mc_gross);
            $transid   = $row->txn_id;
            $date      = date_i18n('Y-m-d H:i:s', $row->payment_date, true);
            $status    = $row->status;
            $user = get_userdata($uid);
            $fullname = ''.$user->first_name.' '.$user->last_name.'';

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td>#'.$id.'</td>';
            echo '<td><a title="'.__('Click here to contact', 'auctionAdmin').' '.$fullname.'" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.'').'" target="_blank">'.$user->user_login.'</a></td>';
            if( empty($email)) {
            echo '<td>'.__('N/A', 'auctionAdmin').'</td>';
            } else {
            echo '<td><a href="mailto:'.$email.'">'.$email.'</td>';
            }
            echo '<td>'.$itemname.'</td>';
            echo '<td>'.$quantity.' '.__('credits', 'auctionAdmin').'</td>';
            echo '<td><a href="#" class="update-credit-status" title="'.__('Update credit status.', 'auctionAdmin').'" id="'.$id.'">'.$status.'</a></td>';
            if( !$fees ) $fees = '0.00';
            echo '<td>'.$fees.'</td>';
            echo '<td>'.$price.'</td>';
            echo '<td>'.$transid.'</td>';
            echo '<td>'.$date.'</td>';
            echo '<td>'.$available.' '.__('credits', 'auctionAdmin').' | <a href="#" class="trans-delete-credits" title="'.__('Delete transaction and credits.', 'auctionAdmin').'" id="'.$id.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="10"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>

<div class="tablenav">
<font color="red"><strong>*</strong></font> <small><?php _e('Once payment is received, click on the status pending, the transaction will change the status to complete and purchased credits are automatically transferred to the user`s account.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
} ?>

<div class="clear20"></div>

      </div><!-- /btpurchased - tab_content -->

<!--
|--------------------------------------------------------------------------
| ESCROW PAYMENTS
|--------------------------------------------------------------------------
-->

		<?php } if($tab == 'escrow') { ?>
			<div id="poststuff">

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE type = 'escrow'" );

if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-transactions&tab=escrow");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
         $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE type = 'escrow' ORDER BY id DESC {$limit}");
	?>

<SCRIPT TYPE="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, windowname, 'width=800,height=500,scrollbars=yes');
return false;
}
//-->
</SCRIPT>

<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Username', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Order ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Quantity', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Fees', 'auctionAdmin'); ?></th>
        <th><?php _e('Gross', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Username', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Order ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Quantity', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Fees', 'auctionAdmin'); ?></th>
        <th><?php _e('Gross', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
            $id        = $row->id;
            $order_id  = $row->order_id;
            $post_ids  = $row->post_ids;
            $uid       = $row->uid;
            $seller_id = $row->post_author;
            $email     = $row->payer_email;
            $currency  = $row->mc_currency;
            $quantity  = $row->quantity;
            $fee       = $row->processor_fee;
            $fees      = $row->mc_fee;
            $price     = $row->mc_gross;
            $transid   = $row->txn_id;
            $date      = date_i18n('Y-m-d H:i:s', $row->payment_date, true);
            $status    = $row->status;
            $escrow_status = $row->escrow_status;
            $item = ''.__('Order ID:','auctionAdmin').' '.$order_id.'';

            $receiver = get_user_meta( $post->post_author, 'paypal_email', true );
            $user = get_userdata( $uid );
            $fullname = ''.$user->first_name.' '.$user->last_name.'';
            if( empty($escrow_status) ) $escrow_status = ''.__('unpaid', 'auctionAdmin').'';

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td>#'.$id.'</td>';
            echo '<td><a title="'.__('Click here to contact', 'auctionAdmin').' '.$fullname.'" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.'').'" target="_blank">'.$user->user_login.'</a></td>';
            if( empty($email)) {
            echo '<td>'.__('N/A', 'auctionAdmin').'</td>';
            } else {
            echo '<td><a href="mailto:'.$email.'">'.$email.'</td>';
            }
            if ( $order_id > 0 ) {
            echo '<td>'.$order_id.' | <a href="'.get_bloginfo('wpurl').'/?get_invoice='.$order_id.'" onclick="return popup(this, \'notes\')">'.__('Invoice', 'auctionAdmin').'</a></td>';
            } else {
            echo '<td>'.__('N/A','auctionAdmin').'</td>';
            }
            echo '<td>'.$quantity.' '.__('items', 'auctionAdmin').'</td>';
            if( $status == "pending" ) {
            echo '<td><a href="#" class="update-escrow-status" title="'.__('Update escrow status.', 'auctionAdmin').'" id="'.$id.'">'.$status.'</a> | <a href="#" class="set-escrow-status" title="'.__('Set status as paid or unpaid.', 'auctionAdmin').'" id="'.$id.'">'.$escrow_status.'</a></td>';
            } else {
            echo '<td>'.$status.' | <a href="#" class="set-escrow-status" title="'.__('Set status as paid or unpaid.', 'auctionAdmin').'" id="'.$id.'">'.$escrow_status.'</a></td>';
            }

            $payout = ($price - $fees) - fees_covered($fee);
            $fees_total = $price - $payout;
            if( !$fees_total ) $fees_total = '0';
            echo '<td>'.cp_auction_format_amount($fees_total).'</td>';
            echo '<td>'.cp_auction_format_amount($price).'</td>';
            echo '<td>'.$transid.'</td>';
            echo '<td>'.$date.'</td>';
            echo '<td><a href="https://www.paypal.com/cgi-bin/webscr?on0=Buyer&os0='.$user->user_login.'&on1=Reference&os1='.$transid.'&amount='.$payout.'&item_name='.$item.'&cmd=_xclick&business='.$receiver.'&no_shipping=1&currency_code='.$cc.'&lc='.$lc.'" target="_blank">'.__('Pay', 'auctionAdmin').'</a> | <a href="#" class="trans-delete-pid" title="'.__('Delete transaction.', 'auctionAdmin').'" id="'.$id.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="11"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>

<div class="tablenav">
<font color="red"><strong>*</strong></font> <small><?php _e('Click on the status pending, the transaction will change the status to complete and an notification email is automatically sent to seller. Once you have made the payment to the seller click on unpaid and this will be changed to paid.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
} ?>

<div class="clear20"></div>

      </div><!-- /escrow - tab_content -->

<!--
|--------------------------------------------------------------------------
| VERIFICATION PAYMENTS
|--------------------------------------------------------------------------
-->

		<?php } if($tab == 'verification') { ?>
			<div id="poststuff">
<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE type = 'verification'" );

if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-transactions&tab=verification");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE type = 'verification' ORDER BY id DESC {$limit}");
	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Full Name (Username)', 'auctionAdmin'); ?></th>
        <th><?php _e('IP Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Amount', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Full Name (Username)', 'auctionAdmin'); ?></th>
        <th><?php _e('IP Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Amount', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
            $id      	= $row->id;
            $uid	= $row->uid;
            $fullname  	= "".$row->first_name." ".$row->last_name."";
            $email     	= $row->payer_email;
            $country  	= $row->address_country;
            $state  	= $row->address_state;
            $zip  	= $row->address_zip;
            $street  	= $row->address_street;
            $ip_address	= $row->ip_address;
            $fees      	= cp_auction_format_amount($row->mc_fee);
            $price     	= cp_auction_format_amount($row->mc_gross);
            $transid   	= $row->txn_id;
            $date      	= date_i18n('Y-m-d H:i:s', $row->payment_date, true);

	    $verified	= get_user_meta( $uid, 'cp_auction_account_verified', true );
	    $user = get_userdata($uid);
	    if( $verified == 1 ) $status = "approved";
	    if( $verified == 2 ) $status = "pending";
	    if( $verified == "" ) $status = "pending";

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td>#'.$id.'</td>';
            echo '<td>'.$fullname.' (<a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.'').'" target="_blank">'.$user->user_login.'</a>)</td>';
            echo '<td>'.$ip_address.'</td>';
            echo '<td><a href="mailto:'.$email.'">'.$email.'</td>';
            echo '<td>'.$zip.' '.$street.', '.$state.' '.$country.'</td>';
            echo '<td><a href="#" class="update-verification-status" title="'.__('Update verification status.', 'auctionAdmin').'" id="'.$id.'">'.$status.'</a></td>';
            echo '<td>'.$price.'</td>';
            echo '<td>'.$transid.'</td>';
            echo '<td>'.$date.'</td>';
            echo '<td><a href="#" class="verification-delete-pid" title="'.__('Delete transaction.', 'auctionAdmin').'" id="'.$id.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="10"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">

<font color="red"><strong>*</strong></font> <small><?php _e('Click on the status pending, the transaction will change the status to approved.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
} ?>

<div class="clear20"></div>

      </div><!-- /verification - tab_content -->

<!--
|--------------------------------------------------------------------------
| BUMPED ADS
|--------------------------------------------------------------------------
-->

		<?php } if($tab == 'bump_ad') { ?>
			<div id="poststuff">
<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE type = 'bump_ad'" );

if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-transactions&tab=bump_ad");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE type = 'bump_ad' ORDER BY id DESC {$limit}");
	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('AD Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Full Name (Username)', 'auctionAdmin'); ?></th>
        <th><?php _e('IP Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Amount', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('AD Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Full Name (Username)', 'auctionAdmin'); ?></th>
        <th><?php _e('IP Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Amount', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
            $id      	= $row->id;
            $pid	= $row->pid;
            $uid	= $row->uid;
            $fullname  	= "".$row->first_name." ".$row->last_name."";
            $email     	= $row->payer_email;
            $country  	= $row->address_country;
            $state  	= $row->address_state;
            $zip  	= $row->address_zip;
            $street  	= $row->address_street;
            $ip_address	= $row->ip_address;
            $fees      	= cp_auction_format_amount($row->mc_fee);
            $price     	= cp_auction_format_amount($row->mc_gross);
            $transid   	= $row->txn_id;
            $date      	= date_i18n('Y-m-d H:i:s', $row->payment_date, true);
            $status	= $row->status;

	    $verified	= get_user_meta( $uid, 'cp_auction_account_verified', true );
	    $user = get_userdata($uid);
	    global $post;
	    $post = get_post($pid);

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td><a href="'.get_permalink($pid).'" target="_blank">'.$post->post_title.'</a></td>';
            echo '<td>'.$fullname.' (<a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.'').'" target="_blank">'.$user->user_login.'</a>)</td>';
            echo '<td>'.$ip_address.'</td>';
            echo '<td><a href="mailto:'.$email.'">'.$email.'</td>';
            echo '<td>'.$zip.' '.$street.', '.$state.' '.$country.'</td>';
            echo '<td><a href="#" class="update-bumped-status" title="'.__('Update Bumped Ad status.', 'auctionAdmin').'" id="'.$id.'">'.$status.'</a></td>';
            echo '<td>'.$price.'</td>';
            echo '<td>'.$transid.'</td>';
            echo '<td>'.$date.'</td>';
            echo '<td><a href="#" class="bumped-delete-pid" title="'.__('Delete transaction.', 'auctionAdmin').'" id="'.$id.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="10"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">

<font color="red"><strong>*</strong></font> <small><?php _e('Click on the status pending, the transaction will change the status to complete, or conversely. If you have choosen to extend the duration of the ad during the bump up then these days will be added or removed depending on the status, pending or complete.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
} ?>

<div class="clear20"></div>

      </div><!-- /bump_ad - tab_content -->

<!--
|--------------------------------------------------------------------------
| FEATURED UPGRADES
|--------------------------------------------------------------------------
-->

		<?php } if($tab == 'upgrade') { ?>
			<div id="poststuff">
<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE type = 'upgrade'" );

if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-transactions&tab=upgrade");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE type = 'upgrade' ORDER BY id DESC {$limit}");
	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('AD Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Full Name (Username)', 'auctionAdmin'); ?></th>
        <th><?php _e('IP Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Amount', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('AD Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Full Name (Username)', 'auctionAdmin'); ?></th>
        <th><?php _e('IP Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Payer Email', 'auctionAdmin'); ?></th>
        <th><?php _e('Address', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Amount', 'auctionAdmin'); ?></th>
        <th><?php _e('TransID', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
            $id      	= $row->id;
            $pid	= $row->pid;
            $uid	= $row->uid;
            $fullname  	= "".$row->first_name." ".$row->last_name."";
            $email     	= $row->payer_email;
            $country  	= $row->address_country;
            $state  	= $row->address_state;
            $zip  	= $row->address_zip;
            $street  	= $row->address_street;
            $ip_address	= $row->ip_address;
            $fees      	= cp_auction_format_amount($row->mc_fee);
            $price     	= cp_auction_format_amount($row->mc_gross);
            $transid   	= $row->txn_id;
            $date      	= date_i18n('Y-m-d H:i:s', $row->payment_date, true);
            $status	= $row->status;

	    $verified	= get_user_meta( $uid, 'cp_auction_account_verified', true );
	    $user = get_userdata($uid);
	    global $post;
	    $post = get_post($pid);

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td><a href="'.get_permalink($pid).'" target="_blank">'.$post->post_title.'</a></td>';
            echo '<td>'.$fullname.' (<a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.'').'" target="_blank">'.$user->user_login.'</a>)</td>';
            echo '<td>'.$ip_address.'</td>';
            echo '<td><a href="mailto:'.$email.'">'.$email.'</td>';
            echo '<td>'.$zip.' '.$street.', '.$state.' '.$country.'</td>';
            echo '<td><a href="#" class="update-featured-status" title="'.__('Update Ad status.', 'auctionAdmin').'" id="'.$id.'">'.$status.'</a></td>';
            echo '<td>'.$price.'</td>';
            echo '<td>'.$transid.'</td>';
            echo '<td>'.$date.'</td>';
            echo '<td><a href="#" class="featured-delete-pid" title="'.__('Delete transaction.', 'auctionAdmin').'" id="'.$id.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="10"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">

<font color="red"><strong>*</strong></font> <small><?php _e('Click on the status pending, the transaction will change the status to complete, or conversely. If you have choosen to extend the duration of the ad during the upgrade then these days will be added or removed depending on the status, pending or complete.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
} ?>

<div class="clear20"></div>

      </div><!-- /upgrade - tab_content -->

<?php } ?>

      </div><!-- /wrap -->

<?php
}
?>