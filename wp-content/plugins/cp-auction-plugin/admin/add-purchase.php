<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

include_once(ABSPATH.'/wp-content/plugins/cp-auction-plugin/scripts/pagination.class.php');

function cp_auction_plugin_add_purchase() {

	global $post, $wpdb, $cp_options;

/*
|--------------------------------------------------------------------------
| ADD PURCHASE INCL. COMPLETE TRANSACTION HISTORY
|--------------------------------------------------------------------------
*/

$msg = "";

if(isset($_POST['add_complete_purchase'])) {

	$aws = ""; $total = ""; $credits = ""; $available = ""; $discount = 0;
	$pid = isset( $_POST['page_id'] ) ? esc_attr( $_POST['page_id'] ) : '';
	if ( $pid == "Verification" ) {
	$item = $pid;
	} else {
	$post = get_post($pid);
	$item = $post->post_title;
	}

	$uid = isset( $_POST['author'] ) ? esc_attr( $_POST['author'] ) : '';
	$user = get_userdata($uid);
	$payment = isset( $_POST['payment'] ) ? esc_attr( $_POST['payment'] ) : '';
	$quantity = isset( $_POST['quantity'] ) ? esc_attr( $_POST['quantity'] ) : '';
	$cbid = isset( $_POST['amount'] ) ? esc_attr( $_POST['amount'] ) : '';
	$fees = isset( $_POST['fees'] ) ? esc_attr( $_POST['fees'] ) : '';
	$trans_id = isset( $_POST['transaction_id'] ) ? esc_attr( $_POST['transaction_id'] ) : '';
	$type_payment = isset( $_POST['type_payment'] ) ? esc_attr( $_POST['type_payment'] ) : '';

	if ( $type_payment == "paypal" ) {
	$type = "purchase";
	$howto_pay = "paypal";
	$trans_type = "Direct by PayPal";
	} else if ( $type_payment == "item_credits" ) {
	$type = "purchase";
	$howto_pay = "item_credits";
	$aws = "aws-credits";
	$trans_type = "Direct by Credits";
	} else if ( $type_payment == "cih" ) {
	$type = "purchase";
	$howto_pay = "cih";
	$trans_type = "Cash in Hand";
	} else if ( $type_payment == "cod" ) {
	$type = "purchase";
	$howto_pay = "cod";
	$trans_type = "Cash on Delivery";
	} else if ( $type_payment == "cre_escrow" ) {
	$type = "escrow";
	$howto_pay = "cre_escrow";
	$aws = "aws-credits";
	$trans_type = "Escrow Payment";
	} else if ( $type_payment == "bt_escrow" ) {
	$type = "escrow";
	$howto_pay = "bt_escrow";
	$trans_type = "Escrow Payment";
	} else if ( $type_payment == "verification" ) {
	$type = "verification";
	$howto_pay = "paypal";
	$trans_type = "Verification";
	}

	$receipt = isset( $_POST['receipt'] ) ? esc_attr( $_POST['receipt'] ) : '';
	if( $payment == 1 ) {
	$nopaid = 0;
	$status = "paid";
	$payment_date = time();
	}
	else if( $payment == "" ) {
	$payment = 2;
	$nopaid = $quantity;
	$status = "unpaid";
	$payment_date = 0;
	}
	$howmany = get_post_meta($pid, 'cp_auction_howmany', true);
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );
	$cp_shipping = get_post_meta($pid, 'cp_shipping', true);
	if ( empty( $cp_shipping ) )
	$cp_shipping = get_user_meta( $post->post_author, 'cp_auction_shipping', true );
	$add_shipping = get_user_meta( $post->post_author, 'cp_auction_add_shipping', true );
	$max_shipping = get_user_meta( $post->post_author, 'cp_auction_max_shipping', true );

	if( !$cbid ) {
	if( empty( $my_type ) || $my_type == "classified" ) {
	$cbid = get_post_meta( $pid,'cp_buy_now',true );
	if( !$cbid ) $cbid = get_post_meta( $pid,'cp_price',true );
	} else if( $my_type == "reverse" ) {
        $cbid = get_post_meta( $pid,'winner_amount',true );
        if( !$cbid ) $cbid = cp_auction_plugin_get_latest_bid($pid);
	} else {
	$cbid = cp_auction_plugin_get_latest_bid($pid);
	}
	}

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$order = $wpdb->get_var( "SELECT order_id FROM {$table_purchases} WHERE uid = '$post->post_author' AND buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY order_id DESC" );
	if ( $order < 1 )
	$order = $wpdb->get_var( "SELECT order_id FROM {$table_purchases} ORDER BY order_id DESC" );
	else $order = $order - 1;
	$order_id = $order + 1;

	$tm			= time();
	$datemade 		= time();
	$txn_id			= $trans_id;
	$item_name 		= $item;
	$mc_currency 		= $cp_options->currency_code;
	$last_name 		= get_user_meta( $uid, 'last_name', true );
	$first_name 		= get_user_meta( $uid, 'first_name', true );
	$payer_email 		= get_user_meta( $uid, 'paypal_email', true );
	$address_country 	= get_user_meta( $uid, 'cp_country', true );
	$address_state 		= get_user_meta( $uid, 'cp_state', true );
	$address_zip 		= get_user_meta( $uid, 'cp_zipcode', true );
	$address_street 	= get_user_meta( $uid, 'cp_street', true );
	$mc_fee 		= cp_auction_sanitize_amount($fees);
	$mc_gross 		= cp_auction_sanitize_amount($total);

	$author = get_userdata($post->post_author);
	$receiver_email = get_user_meta( $post->post_author, 'paypal_email', true );
	if( empty($receiver_email) ) $receiver_email = $author->user_email;

	if( $quantity > 1 ) {
	$shipping = $cp_shipping + ($cp_shipping * $add_shipping / 100 * $adds);
	if( $shipping > $max_shipping ) $shipping = $max_shipping;
	} else {
	$shipping = $cp_shipping;
	}
	$total = $cbid * $quantity;
	$unpaid_total = ($cbid * $quantity) + $shipping;

	$approves = "complete";
	if ( $payment != 1 ) $approves = "pending";

	if ( $type_payment == "item_credits" || $type_payment == "cre_escrow" ) {
	$credits = $unpaid_total / get_option('cp_auction_plugin_credit_value');
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$available = cp_auction_sanitize_amount($available_credits - $credits);
	update_user_meta( $uid, 'cp_credits', $available );
	}

	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$query = "INSERT INTO {$table_transactions} (order_id, pid, post_ids, datemade, uid, post_author, ip_address, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, receiver_email, address_country, address_state, address_country_code, address_zip, address_street, moved_credits, available, quantity, payment_option, shipping, mc_fee, mc_gross, aws, type, status) VALUES (%d, %d, %s, %d, %d, %d, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %f, %f, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, $pid, $pid, $datemade, $uid, $post->post_author, 'N/A', $payment_date, $txn_id, $item_name, $trans_type, $mc_currency, $last_name, $first_name, $payer_email, $receiver_email, $address_country, $address_state, 'N/A', $address_zip, $address_street, $credits, $available, $quantity, $howto_pay, $shipping, $mc_fee, $unpaid_total, $aws, $type, $approves));
	$lastid = $wpdb->insert_id;

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$query = "INSERT INTO {$table_purchases} (order_id, trans_id, pid, uid, buyer, item_name, quantity, amount, discount, mc_fee, mc_gross, datemade, numbers, unpaid, net_total, unpaid_total, paid, paiddate, txn_id, type, status) VALUES (%d, %d, %d, %d, %d, %s, %d, %f, %f, %f, %f, %d, %d, %d, %f, %f, %d, %d, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, $lastid, $pid, $post->post_author, $uid, $item_name, $quantity, $cbid, $discount, $mc_fee, $unpaid_total, $tm, $quantity, $nopaid, $total, $unpaid_total, $payment, $payment_date, $txn_id, 'classified', $status));

	if( $my_type == "normal" || $my_type == "reverse" ) {
	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$query = "INSERT INTO {$table_bids} (pid, datemade, bid, uid, winner, type) VALUES (%d, %d, %f, %d, %d, %s)"; 
	$wpdb->query($wpdb->prepare($query, $pid, $tm, $cbid, $uid, '1', 'classified'));

	$my_post = array();
	$my_post['ID'] = $pid;
	$my_post['post_status'] = 'draft';
	wp_update_post( $my_post );

	update_post_meta( $pid,'cp_ad_sold','yes' );
	update_post_meta( $pid,'cp_ad_sold_date',$tm );
	}

	if( $howmany > 0 ) {
	if( $quantity > $howmany ) $quantity = $howmany;
	$how_many = $howmany - $quantity;
	update_post_meta( $pid,'cp_auction_howmany',$how_many );
	if( $how_many < 1 ) {
	$my_post = array();
	$my_post['ID'] = $pid;
	$my_post['post_status'] = 'draft';
	wp_update_post( $my_post );

	update_post_meta( $pid,'cp_ad_sold','yes' );
	update_post_meta( $pid,'cp_ad_sold_date',$tm );
	}
	}

	if( $receipt == "yes" && $payment == 1 ) {
	cp_auction_backend_payment_receipt( $pid, $item_name, $uid, $mc_fee, $unpaid_total, $txn_id );
	}
	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Purchase was added to','auctionAdmin').' '.$user->user_login.'`s '.__('account!','auctionAdmin').'</strong></p></div>';

}

?>

	<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

<script type="text/javascript">
function ValidateME2()
{    
if (document.getElementById("quantity2").value == "")
{
alert("<?php _e('Please enter quantity!', 'auctionAdmin'); ?>");  // Give alert to user
           document.getElementById("quantity2").focus(); // Set focus on textbox
 return false;	
}
}
</script>

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Add complete purchase to user`s account', 'auctionAdmin'); ?></h3>
            <div class="inside">

	<div class="clear10"></div>
	<?php _e('This feature creates a purchase and a full transaction history on the buyer, and gives the buyer the opportunity to download the product purchased.', 'auctionAdmin'); ?>

   	<form action="" method="post">
	<table width="100%">
	<tbody>
	<tr valign="top" height="20"><td colspan="2" valign="top"></td>
	</tr>
	<tr valign="top"><td style="width:100px"><?php _e('Select Item:', 'auctionAdmin'); ?></td><td><?php get_add_purchase_dropdown(); ?></td>
	</tr>
	<tr valign="top"><td><?php _e('Select User:', 'auctionAdmin'); ?></td><td><?php wp_dropdown_users(array('name' => 'author')); ?></td>
	</tr>
	<tr valign="top"><td><?php _e('Quantity:', 'auctionAdmin'); ?></td><td><input type="text" size="10" name="quantity" id="quantity2" /></td>
	</tr>
	<tr valign="top"><td><?php _e('Transaction ID:', 'auctionAdmin'); ?></td><td><input type="text" size="30" name="transaction_id" id="transaction_id" /> <?php _e('If you have a PayPal transaction id.', 'auctionAdmin'); ?></td>
	</tr>
	<tr valign="top"><td><?php _e('Item Price:', 'auctionAdmin'); ?></td><td><input type="text" size="10" name="amount" id="amount" /> <?php _e('Optional, calculated from the purchase type price unless stated.', 'auctionAdmin'); ?></td>
	</tr>
	<tr valign="top"><td><?php _e('Fees', 'auctionAdmin'); ?></td><td><input type="text" size="10" name="fees" id="fees" /> <?php _e('Optional, calculated from the purchase type price unless stated.', 'auctionAdmin'); ?></td>
	</tr>
	<tr valign="top"><td><?php _e('Paid / Unpaid', 'auctionAdmin'); ?></td><td><select name="payment" id="payment">
	<option value="1"><?php _e('Paid', 'auctionAdmin'); ?></option>
	<option value=""><?php _e('Unpaid', 'auctionAdmin'); ?></option></select></td>
	</tr>
	<tr valign="top"><td>Purchase Type:</td><td><select name="type_payment" id="type_payment">
	<option value="paypal"><?php _e('PayPal', 'auctionAdmin'); ?></option>
	<option value="item_credits"><?php _e('Credits', 'auctionAdmin'); ?></option>
	<option value="cih"><?php _e('Cash in Hand', 'auctionAdmin'); ?></option>
	<option value="cod"><?php _e('Cash on Delivery', 'auctionAdmin'); ?></option>
	<option value="cre_escrow"><?php _e('Credits Escrow', 'auctionAdmin'); ?></option>
	<option value="bt_escrow"><?php _e('Bank Transfer Escrow', 'auctionAdmin'); ?></option>
	<option value="verification"><?php _e('Verification', 'auctionAdmin'); ?></option>
	</select></td>
	</tr>
	<tr valign="top"><td><?php _e('Receipt:', 'auctionAdmin'); ?></td><td><select name="receipt" id="receipt"><?php echo cp_auction_yes_no(); ?></select> <?php _e('Select yes if you want to send a payment receipt to the buyer.', 'auctionAdmin'); ?></td>
	</tr>
	</tbody>
	</table>
	<div class="clear10"></div>
	<?php _e('Shipping costs are automatically calculated based on the seller`s shipping cost settings. Credit payments will automatically be charged to the user`s account regardless of whether you have selected paid or unpaid.', 'auctionAdmin'); ?>
	<p class="submit"><input type="submit" name="add_complete_purchase" id="submit" class="button-primary" value="<?php _e('Add Purchase', 'auctionAdmin'); ?>" onclick="return ValidateME2();" /></p>
   	</form>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}