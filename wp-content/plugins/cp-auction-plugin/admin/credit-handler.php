<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_plugin_credit_handler() {

	global $wpdb;
	$msg = "";

if(isset($_POST['add'])) {
	$uid = isset( $_POST['author'] ) ? esc_attr( $_POST['author'] ) : '';
	$tm = time();
	$user = get_userdata($uid);
	$credits = isset( $_POST['credits'] ) ? esc_attr( $_POST['credits'] ) : '';
	$value = cp_auction_sanitize_amount($credits * get_option('cp_auction_plugin_credit_value'));
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$add_credits = cp_auction_sanitize_amount($available_credits + $credits);
	update_user_meta( $uid, 'cp_credits', $add_credits );

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$query = "INSERT INTO {$table_name} (datemade, uid, trans_type, moved_credits, available, mc_gross, aws, type, status) VALUES (%d, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $tm, $uid, 'Credited by Admin', $credits, $add_credits, $value, 'aws-credits', 'purchase', 'complete'));

	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$credits.' '.__('credits was added to','auctionAdmin').' '.$user->user_login.'`s '.__('account!','auctionAdmin').'</strong></p></div>';
	}

if(isset($_POST['remove'])) {
	$uid = isset( $_POST['author'] ) ? esc_attr( $_POST['author'] ) : '';
	$tm = time();
	$user = get_userdata($uid);
	$credits = isset( $_POST['credits'] ) ? esc_attr( $_POST['credits'] ) : '';
	$value = cp_auction_sanitize_amount($credits * get_option('cp_auction_plugin_credit_value'));
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$remove_credits = cp_auction_sanitize_amount($available_credits - $credits);
	update_user_meta( $uid, 'cp_credits', $remove_credits );

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$query = "INSERT INTO {$table_name} (datemade, uid, trans_type, moved_credits, available, mc_gross, aws, type, status) VALUES (%d, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $tm, $uid, 'Charged by Admin', $credits, $remove_credits, $value, 'aws-credits', 'purchase', 'complete'));

	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$credits.' '.__('credits was removed from','auctionAdmin').' '.$user->user_login.'`s '.__('account!','auctionAdmin').'</strong></p></div>';

	}

if(isset($_POST['check'])) {
	$uid = isset( $_POST['author'] ) ? esc_attr( $_POST['author'] ) : '';
	$user = get_userdata($uid);
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	if(empty($available_credits)) $available_credits = 0;

	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$user->user_login.' '.__('has','auctionAdmin').' '.$available_credits.' '.__('credits available in their account.','auctionAdmin').'</strong></p></div>';

	}
?>

	<?php if($msg) echo $msg; ?>

	<?php if( function_exists('aws_cp_settings_transactions') ) { ?>
	<div class="metabox-holder">
	<?php } else { ?>
	<div class="metabox-holder has-right-sidebar">
	<?php } ?>

	<div id="post-body">
	<div id="post-body-content">

	<?php if( function_exists('aws_cp_settings_transactions') ) { aws_cp_settings_transactions(); ?>

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Credit Handler', 'auctionAdmin'); ?></h3>
            <div class="inside">

   	<form action="" method="post">
	<table width="300">
	<tbody>
	<tr valign="top" height="30"><td colspan="2" valign="top"><strong><?php _e('Add or remove credits on user`s account', 'auctionAdmin'); ?></strong></td>
	</tr>
	<tr valign="top"><td><?php _e('Select user:', 'auctionAdmin'); ?></td><td><?php wp_dropdown_users(array('show_option_none' => 'Select User', 'name' => 'author')); ?></td>
	</tr>
	<tr valign="top"><td><?php _e('Number of credits:', 'auctionAdmin'); ?></td><td><input name="credits" type="text" value="" size="8" /></td>
	</tr>
	</tbody>
	</table>
	<p class="submit"><input type="submit" name="check" id="submit" class="button-secondary" value="<?php _e('Check Credits Available', 'auctionAdmin'); ?>" /> <input type="submit" name="add" id="submit" class="button-secondary" value="<?php _e('Add Credits', 'auctionAdmin'); ?>" /> <input type="submit" name="remove" id="submit" class="button-secondary" value="<?php _e('Remove Credits', 'auctionAdmin'); ?>" /></p>
	</form>

	<?php } else { ?>

	<div class="postbox">
            <h3 style="cursor:default;"><?php _e('Credit Handler', 'auctionAdmin'); ?></h3>
            <div class="inside">

	<div class="clear10"></div>
	Buy our complete <a style="text-decoration: none;" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank"><strong>credit payment module</strong></a> and allow your customers to pay for their product purchases directly to the seller using their earned credits.
	<div class="clear20"></div>
	<?php } ?>

           </div>
         </div>

       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

<?php _e('This feature requires that the buy / sell credits option is enabled.', 'auctionAdmin'); ?>

<?php if( !function_exists('aws_cp_settings_transactions') ) { ?>
<div class="clear10"></div>
Buy our complete <a style="text-decoration: none;" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank"><strong>credit payment module</strong></a> and allow your customers to pay for their product purchases directly to the seller using their earned credits.
<div class="clear10"></div>
<a class="button-primary" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank">Buy Now</a></td>
<?php } ?>
            </div>
          </div>
        </div>
</div>

<?php
}