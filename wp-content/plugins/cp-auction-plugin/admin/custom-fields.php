<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_custom_fields() {

	global $wpdb;

	if( isset($_GET['action'])) $action = $_GET['action'];
	else $action = "";
	if( isset($_GET['id'])) $field_id = $_GET['id'];
	else $field_id = "";
	$msg = ""; $pri = ""; $ids = ""; $style = ""; $readonly = ""; $style2 = ""; $readonly2 = ""; $disabled = "";

	if( isset( $_POST['restore_fields'] ) ) {
	$table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
	$results = $wpdb->get_results("SELECT * FROM {$table_fields} ORDER BY field_id ASC");
	if( $results ) {
	foreach($results as $res) {
	$table_ad_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_ad_fields} WHERE field_name = '$res->field_name'");
	if( !$row ) {
	$query = "INSERT INTO {$table_ad_fields} (field_name, field_label, field_desc, field_type, field_values, field_tooltip, field_search, field_perm, field_core, field_req, field_owner, field_created, field_modified, field_min_length, field_validation) VALUES (%s, %s, %s, %s, %s, %s, %d, %d, %d, %d, %s, %s, %s, %d, %d)"; 
	$wpdb->query($wpdb->prepare($query, $res->field_name, $res->field_label, $res->field_desc, $res->field_type, $res->field_values, $res->field_tooltip, $res->field_search, $res->field_perm, $res->field_core, $res->field_req, $res->field_owner, $res->field_created, $res->field_modified, $res->field_min_length, $res->field_validation));
		}
		}
		}
	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Custom fields was re-created.','auctionAdmin').'</strong></p></div>';
	}

	if( isset( $_POST['create_field'] ) ) {

	$fieldname = isset( $_POST['field_label'] ) ? esc_attr( $_POST['field_label'] ) : '';
	$field_desc = isset( $_POST['field_desc'] ) ? esc_attr( $_POST['field_desc'] ) : '';
	$field_tooltip = isset( $_POST['field_tooltip'] ) ? esc_attr( $_POST['field_tooltip'] ) : '';
	$field_type = isset( $_POST['field_type'] ) ? esc_attr( $_POST['field_type'] ) : '';
	$field_values = isset( $_POST['field_values'] ) ? esc_attr( $_POST['field_values'] ) : '';
	$trans_values = isset( $_POST['trans_values'] ) ? esc_attr( $_POST['trans_values'] ) : '';
	$field_details = isset( $_POST['field_details'] ) ? esc_attr( $_POST['field_details'] ) : '';
	$price_field = isset( $_POST['price_field'] ) ? esc_attr( $_POST['price_field'] ) : '';
	$field_search = isset( $_POST['field_search'] ) ? esc_attr( $_POST['field_search'] ) : '';
	$field_core = isset( $_POST['field_core'] ) ? esc_attr( $_POST['field_core'] ) : '';
	$field_req = isset( $_POST['field_req'] ) ? esc_attr( $_POST['field_req'] ) : '';
	$field_min_length = isset( $_POST['field_min_length'] ) ? esc_attr( $_POST['field_min_length'] ) : '';
	$classified = isset( $_POST['classified'] ) ? esc_attr( $_POST['classified'] ) : '';
	$wanted = isset( $_POST['wanted'] ) ? esc_attr( $_POST['wanted'] ) : '';
	$normal = isset( $_POST['normal'] ) ? esc_attr( $_POST['normal'] ) : '';
	$reverse = isset( $_POST['reverse'] ) ? esc_attr( $_POST['reverse'] ) : '';

	$field_name = cp_make_custom_name( $fieldname, 'fields', $random = false );

	$option = get_option('cp_auction_fields');
	$options = get_option('cp_auction_details');
	$prices = get_option('cp_auction_pricefields');
	$list = ''.$classified.','.$wanted.','.$normal.','.$reverse.'';
	$array = explode( ',', $list );
	$new_list = implode(",", $array);
	$field_ads = trim($new_list, ',');

	if( isset( $_POST['field_id'] ) ) {
	$field_id = $_POST['field_id'];
	$field_modified = date('Y-m-d h:i:s');
	$table_ad_fields = $wpdb->prefix . "cp_ad_fields";
	$table_ad_meta = $wpdb->prefix . "cp_ad_meta";
	$row = $wpdb->get_row("SELECT * FROM {$table_ad_fields} WHERE field_id = '$field_id'");
	if( $row->field_core == 1 ) {
	if( $trans_values ) $field_values = $trans_values;
	$where_array = array('field_id' => $field_id);
	$wpdb->update($table_ad_fields, array('field_label' => $fieldname, 'field_values' => $field_values, 'field_tooltip' => stripslashes(html_entity_decode($field_tooltip)), 'field_search' => $field_search, 'field_core' => $field_core, 'field_req' => $field_req, 'field_modified' => $field_modified, 'field_min_length' => $field_min_length), $where_array);
	$fsearch = $wpdb->get_row("SELECT * FROM {$table_ad_meta} WHERE field_id = '$field_id'");
	if( $fsearch ) {
	$wpdb->update($table_ad_meta, array('field_search' => $field_search), $where_array);
	}
	$option[$row->field_name] = $field_ads;
	if ( ! empty( $option ) )
   	update_option('cp_auction_fields', $option);
   	$options[$row->field_name] = $field_details;
	if ( ! empty( $options ) )
   	update_option('cp_auction_details', $options);
   	$prices[$row->field_name] = $price_field;
	if ( ! empty( $prices ) )
   	update_option('cp_auction_pricefields', $prices);
	} else {
	if( $trans_values ) $field_values = $trans_values;
	$where_array = array('field_id' => $field_id);
	$wpdb->update($table_ad_fields, array('field_label' => $fieldname, 'field_desc' => $field_desc, 'field_type' => $field_type, 'field_values' => $field_values, 'field_tooltip' => stripslashes(html_entity_decode($field_tooltip)), 'field_core' => $field_core, 'field_req' => $field_req, 'field_modified' => $field_modified, 'field_min_length' => $field_min_length), $where_array);
	$option[$row->field_name] = $field_ads;
	if ( ! empty( $option ) )
   	update_option('cp_auction_fields', $option);
   	$options[$row->field_name] = $field_details;
	if ( ! empty( $options ) )
   	update_option('cp_auction_details', $options);
   	$prices[$row->field_name] = $price_field;
	if ( ! empty( $prices ) )
   	update_option('cp_auction_pricefields', $prices);
	}
	if( $trans_values ) {
	$table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
	$upd = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = '$row->field_name'");
	if( $upd->trans_values ) $upd->field_values = $upd->trans_values;
	$where_array = array('field_name' => $row->field_name);
	$wpdb->update($table_fields, array('field_values' => $trans_values, 'trans_values' => $upd->field_values), $where_array);
	} else {
	$table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
	$table_ad_fields = $wpdb->prefix . "cp_ad_fields";
	$upd = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = '$row->field_name'");
	if( isset( $upd->trans_values ) ) {
	$where_array = array('field_name' => $row->field_name);
	$wpdb->update($table_ad_fields, array('field_values' => $upd->trans_values), $where_array);
	$wpdb->update($table_fields, array('field_values' => $upd->trans_values, 'trans_values' => ''), $where_array);
	}
	}
	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Changes was saved.','auctionAdmin').'</strong></p></div>';
	}
	else {
	$field_created = date('Y-m-d h:i:s');
	$table_ad_fields = $wpdb->prefix . "cp_ad_fields";
	$query = "INSERT INTO {$table_ad_fields} (field_name, field_label, field_desc, field_type, field_values, field_tooltip, field_search, field_perm, field_core, field_req, field_owner, field_created, field_modified, field_min_length) VALUES (%s, %s, %s, %s, %s, %s, %d, %d, %d, %d, %s, %s, %s, %d)"; 
    	$wpdb->query($wpdb->prepare($query, $field_name, $fieldname, $field_desc, $field_type, $field_values, stripslashes(html_entity_decode($field_tooltip)), $field_search, '0', $field_core, $field_req, 'admin', $field_created, $field_created, $field_min_length));
    	$option[$field_name] = $field_ads;
	if ( ! empty( $option ) )
   	update_option('cp_auction_fields', $option);
   	$options[$field_name] = $field_details;
	if ( ! empty( $options ) )
   	update_option('cp_auction_details', $options);
   	$prices[$field_name] = $price_field;
	if ( ! empty( $prices ) )
   	update_option('cp_auction_pricefields', $prices);

    	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Custom field was created.','auctionAdmin').'</strong></p></div>';
    	}
}

	if( $action == "editfield" && $field_id > 0 ) {
	$option = get_option('cp_auction_fields');
	$options = get_option('cp_auction_details');
	$prices = get_option('cp_auction_pricefields');
	$table_ad_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_ad_fields} WHERE field_id = '$field_id'");
	$fname = $row->field_name;
	$list = explode(',', $option[$fname]);
	$ids = $options[$fname];
	$pri = $prices[$fname];
	$cids = $row->field_core;
	$rids = $row->field_req;
	$sids = $row->field_search;

	$table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
	$rows = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = '$fname'");

	if( isset( $rows->trans_values ) && ! empty( $rows->trans_values ) ) {
	$row->field_values = $rows->trans_values;
	} else {
	if ( !isset( $rows ) )
		$rows = new stdClass();

	$rows->field_values = false;
	}
}

	if( $action == "fdelete" && $field_id > 0 ) {
	$table_fields = $wpdb->prefix . "cp_ad_fields";
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_fields} WHERE field_id = '%d'", $field_id));

	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Custom field with id','auctionAdmin').' '.$field_id.' '.__('was deleted.','auctionAdmin').'</strong></p></div>';
}

	$items = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->cp_ad_fields" );

	if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(30); // Limit entries per page
        $p->target("admin.php?page=cp-auction&tab=fields");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>

	<?php if($msg) echo $msg; ?>

	<?php if( isset( $_POST['add_new'] ) || isset( $_POST['create_field'] ) || $action == "editfield" ) { ?>

	<div class="metabox-holder">
	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php if( $action == "editfield" ) echo ''.__('Edit Custom Field', 'auctionAdmin').''; else echo ''.__('Add New Custom Field', 'auctionAdmin').''; ?></h3>
            <div class="inside">

<script type="text/javascript">
jQuery(document).ready(function($) {
$('#field_type').change(function(){
if($('#field_type').val() === 'drop-down')
   {
   $('#field_values').show();
document.getElementById("field_values").disabled = false;
   }
else if($('#field_type').val() === 'radio')
   {
      $('#field_values').show();
document.getElementById("field_values").disabled = false;
   }
else if($('#field_type').val() === 'checkbox')
   {
      $('#field_values').show();
document.getElementById("field_values").disabled = false;
   }
else if($('#field_type').val() === 'text box')
   {
      $('#field_min_length').show();
document.getElementById("field_min_length").disabled = false;
   }
else if($('#field_type').val() === 'text area')
   {
      $('#field_min_length').show();
document.getElementById("field_min_length").disabled = false;
   }
else
   {
   $('#field_values').hide();
   $('#field_min_length').hide();
   }
});
});
</script>

	<form method="post" action="">

	<?php if( $action == "editfield" && $field_id > 0 ) {
	echo '<input type="hidden" name="field_id" value="'.$field_id.'">';
	if( $row->field_core == 1 ) {
	$readonly = " readonly=\"readonly\"";
	$disabled = " disabled=\"disabled\"";
	$style = " style=\"background-color: #EFEFEF;\"";
	}
	if( ( $row->field_name ) && ( $row->field_type == "drop-down" || $row->field_type == "radio" ) && ( $row->field_name == "cp_pausead" || $row->field_name == "cp_want_to" || $row->field_name == "cp_allow_deals" || $row->field_name == "cp_iwant_to" || $row->field_name == "cp_shipping_options" ) ) {
	$readonly2 = " readonly=\"readonly\"";
	$disabled2 = " disabled=\"disabled\"";
	$style2 = " style=\"background-color: #EFEFEF;\"";
	}
	$table_ad_meta = $wpdb->prefix . "cp_ad_meta";
	$fsearch = $wpdb->get_row("SELECT * FROM {$table_ad_meta} WHERE field_id = '$field_id'");
	if( isset( $fsearch->field_search ) == 1 ) $field_search = true;
	} ?>

    <table class="form-table">

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Create a field name that best describes what this field will be used for. (i.e. Color, Size, etc). It will be visible on your site.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Field Name:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="field_label" id="field_label" value="<?php if( $action == "editfield" ) echo $row->field_label; ?>" size="60" /></br /><small><?php _e('Create a field name that best describes what this field will be used for.', 'auctionAdmin'); ?></small></td>
    </tr>

<?php if( $action == "editfield" && $field_id > 0 ) { ?>
        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('This field is used by WordPress so you cannot modify it. Doing so could cause problems displaying the field on your ads.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Meta Name:', 'auctionAdmin'); ?></th>
    <td><input type="text" readonly="readonly" name="field_name" value="<?php if( $action == "editfield" ) echo $row->field_name; ?>" size="60" /></br /><small><?php _e('This field is used by WordPress so you cannot modify it.', 'auctionAdmin'); ?></small></td>
    </tr>
<?php } ?>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Enter a description of your new field. It will not be visible on your site.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Field Description:', 'auctionAdmin'); ?></th>
    <td><textarea<?php if( $action == "editfield" ) echo $style; ?> class="options"<?php if( $action == "editfield" ) echo $readonly; ?> name="field_desc" id="field_desc" rows="5" cols="80"><?php if( $action == "editfield" ) echo $row->field_desc; ?></textarea><br><small><?php _e('Enter a description of your new form layout. It will not be visible on your site.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('This will create a ? tooltip icon next to this field on the submit ad page.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Field Tooltip:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="field_tooltip" rows="5" cols="80"><?php if( $action == "editfield" ) echo $row->field_tooltip; ?></textarea><br><small><?php _e('This will create a ? tooltip icon next to this field on the submit ad page.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('This is the type of field you want to create.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Field Type:', 'auctionAdmin'); ?></th>
    <td><select<?php if( $action == "editfield" ) echo $style; ?> name="field_type"<?php if( $action == "editfield" ) echo $disabled; ?> id="field_type">
        <option value="">Select</option>
	<option value="text box" <?php if( $row->field_type && $row->field_type == "text box" ) echo 'selected="selected"'; ?>>text box</option>
	<option value="drop-down" <?php if( $row->field_type && $row->field_type == "drop-down" ) echo 'selected="selected"'; ?>>drop-down</option>
	<option value="text area" <?php if( $row->field_type && $row->field_type == "text area" ) echo 'selected="selected"'; ?>>text area</option>
	<option value="radio" <?php if( $row->field_type && $row->field_type == "radio" ) echo 'selected="selected"'; ?>>radio buttons</option>
	<option value="checkbox" <?php if( $row->field_type && $row->field_type == "checkbox" ) echo 'selected="selected"'; ?>>checkboxes</option>
	</select> <?php _e('This is the type of field you want to create.', 'auctionAdmin'); ?></td>
    </tr>

<?php if( $row->field_type && ( $row->field_type == "drop-down" || $row->field_type == "radio" || $row->field_type == "checkbox" ) ) { ?>
	<tr valign="top">
<?php } else { ?>
        <tr id="field_values" style="display: none;" valign="top">
<?php } ?>
        <th scope="row"><a href="#" title="<?php _e('Enter a comma separated list of values you want to appear with this field. (i.e. XXL,XL,L,M,S,XS). Do not separate values with the return key.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Field Values:', 'auctionAdmin'); ?></th>
    <td><textarea<?php if( $action == "editfield" ) echo $style2; ?> class="options"<?php if( $action == "editfield" ) echo $readonly2; ?> name="field_values" rows="5" cols="80"><?php if( $action == "editfield" ) echo $row->field_values; ?></textarea><br><small><?php _e('Enter a comma separated list of values you want to appear with this field.', 'auctionAdmin'); ?></small></td>
    </tr>

<?php if( ( $row->field_name ) && ( $row->field_type == "drop-down" || $row->field_type == "radio" ) && ( $row->field_name == "cp_pausead" || $row->field_name == "cp_want_to" || $row->field_name == "cp_allow_deals" || $row->field_name == "cp_iwant_to" || $row->field_name == "cp_shipping_options" ) ) { ?>
	<tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Enter the same comma separated list as above in your language. Do not separate values with the return key.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Translate Field Values:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="trans_values" rows="5" cols="80"><?php if( $action == "editfield" ) echo $rows->field_values; ?></textarea><br><small><?php _e('Enter the above comma separated list of values in your language.', 'auctionAdmin'); ?></small></td>
    </tr>
<?php } ?>

<?php if( $row->field_type && ( $row->field_type == "text box" || $row->field_type == "text area" ) ) { ?>
	<tr valign="top">
<?php } else { ?>
        <tr id="field_min_length" style="display: none;" valign="top">
<?php } ?>
        <th scope="row"><a href="#" title="<?php _e('Defines the minimum number of characters required for this field. Enter a number like 2 or enter 0 to make the field optional.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Minimum Length:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="field_min_length" id="field_min_length" value="<?php if( $action == "editfield" ) echo $row->field_min_length; ?>" size="8" /> <?php _e('Defines the minimum number of characters required for this field.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Set this option to yes if the field you are creating is a price field. The value will then be displayed in the ad listings ad details with your choosen currency symbol.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Price Field:', 'auctionAdmin'); ?></th>
    <td><select name="price_field">
    <option value="no" <?php if( $pri == "no" ) echo 'selected="selected"'; ?>><?php _e('No', 'auctionAdmin'); ?></option>
    <option value="yes" <?php if( $pri == "yes" ) echo 'selected="selected"'; ?>><?php _e('Yes', 'auctionAdmin'); ?></option>
    </select> <?php _e('Display the value in ad listings ad details with a currency symbol.', 'auctionAdmin'); ?></td>
    </tr>

	<tr valign="top">
	<th scope="row"><a href="#" title="<?php _e('Select the type of ad that this field should be registered against.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Ad Types:', 'auctionAdmin'); ?></th>
    
   <td><input type="checkbox" name="classified" value="classified" <?php if( $action == "editfield" && in_array("classified", $list)) echo 'checked="checked"'; ?>> <?php _e('Classified Ads', 'auctionAdmin'); ?><br/>
   <input type="checkbox" name="wanted" value="wanted" <?php if( $action == "editfield" && in_array("wanted", $list)) echo 'checked="checked"'; ?>> <?php _e('Wanted Ads', 'auctionAdmin'); ?><br/>
   <input type="checkbox" name="normal" value="normal" <?php if( $action == "editfield" && in_array("normal", $list)) echo 'checked="checked"'; ?>> <?php _e('Normal Auction Listings', 'auctionAdmin'); ?><br/>
   <input type="checkbox" name="reverse" value="reverse" <?php if( $action == "editfield" && in_array("reverse", $list)) echo 'checked="checked"'; ?>> <?php _e('Reverse Auction Listings', 'auctionAdmin'); ?>
        </td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Set this option to yes if you want to display the field and its value in single ad listings ad details.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Ad Details:', 'auctionAdmin'); ?></th>
    <td><select name="field_details">
    <option value="no" <?php if( $ids == "no" ) echo 'selected="selected"'; ?>><?php _e('No', 'auctionAdmin'); ?></option>
    <option value="yes" <?php if( $ids == "yes" ) echo 'selected="selected"'; ?>><?php _e('Yes', 'auctionAdmin'); ?></option>
    </select> <?php _e('Display the field and its value in single ad listings ad details.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Set this option to yes if you want to make the field required.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Required Field:', 'auctionAdmin'); ?></th>
    <td><select name="field_req">
    <option value="0" <?php if( $rids == "" || $rids == "0" ) echo 'selected="selected"'; ?>><?php _e('No', 'auctionAdmin'); ?></option>
    <option value="1" <?php if( $rids == '1' ) echo 'selected="selected"'; ?>><?php _e('Yes', 'auctionAdmin'); ?></option>
    </select> <?php _e('Make field required.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Set this option to yes if you want to make the field a core field.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Core Field:', 'auctionAdmin'); ?></th>
    <td><select name="field_core">
    <option value="0" <?php if( $cids == "" || $cids == "0" ) echo 'selected="selected"'; ?>><?php _e('No', 'auctionAdmin'); ?></option>
    <option value="1" <?php if( $cids == '1' ) echo 'selected="selected"'; ?>><?php _e('Yes', 'auctionAdmin'); ?></option>
    </select> <?php _e('Create as core field.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this custom field to be used with the standard form. You can change a core field to standard field and this will open for more editing possibilities BUT, if this is done then NEVER make any changes to Meta Name, Field Type or Field Values (use at your own risk).', 'auctionAdmin'); ?></small></td>
    </tr>

	<?php if( isset( $field_search ) && $action == "editfield" && $field_id > 0 ) { ?>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Set this option to yes if you want to make the field value searchable.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Searchable:', 'auctionAdmin'); ?></th>
    <td><select name="field_search">
    <option value="" <?php if( $sids == "" ) echo 'selected="selected"'; ?>><?php _e('No', 'auctionAdmin'); ?></option>
    <option value="1" <?php if( $sids == '1' ) echo 'selected="selected"'; ?>><?php _e('Yes', 'auctionAdmin'); ?></option>
    </select> <?php _e('Make field values searchable.', 'auctionAdmin'); ?></td>
    </tr>

	<?php } ?>

    <tr valign="top">
    <td colspan="4"><input type="submit" value="<?php if( $action == "editfield" ) echo ''.__('Save Changes', 'auctionAdmin').''; else echo ''.__('Create New Field', 'auctionAdmin').''; ?>" name="create_field" class="button-primary"/> &nbsp;&nbsp; <input type=button class="button-secondary" onClick="window.open('admin.php?page=cp-auction&tab=fields','_self')" value="<?php _e('Cancel', 'auctionAdmin'); ?>"></td>
    </tr>

    </table>

    </form>

      </div>
    </div>
  </div>
  </div>
  </div>

	<?php } else { ?>

	<p class="admin-msg"><?php _e('Custom fields allow you to customize your ad submission forms and collect more information. <strong>If custom form layouts is used then each custom field needs to be added to a form layout in order to be visible on your website</strong>. You can create unlimited custom fields and each one can be used across multiple form layouts. It is highly recommended to NOT delete a custom field once it is being used on your ads because it could cause ad editing problems for your customers.','auctionAdmin'); ?> <?php _e('Form layouts allow you to create your own custom ad submission forms. Each form is essentially a container for your fields and can be applied to one or all of your categories. If you do not create any form layouts, the default one will be used. To change the default form, create a new form layout and apply it to all categories.','auctionAdmin'); ?></p>

	<div class="clear10"></div>

	<p class="admin-msg"><?php _e('If you have deleted any of the CP Auction standard custom fields you can use the Restore Fields button to restore them.','auctionAdmin'); ?></p>

	<div class="clear10"></div>

	<p class="admin-msg"><?php _e('Make sure that the fields you want to use are set to work with the correct Ad Type, as you can see from the Title field this is set to work with all ad types. You can easily edit any of the custom fields, just click on the name.','auctionAdmin'); ?></p>

	<div class="clear10"></div>

<div class="tablenav">
    <div class="clear20"></div>
	<form method="post" action="">
	<input type="submit" name="add_new" class="button-primary" value="<?php _e('Add Custom Field', 'auctionAdmin'); ?>" /> &nbsp;&nbsp; <input type=button class="button-secondary" onClick="window.open('admin.php?page=layouts&action=addform','_self')" value="<?php _e('Add Form', 'auctionAdmin'); ?>"> &nbsp;&nbsp; <input type="submit" name="restore_fields" class="button-secondary" value="<?php _e('Restore Fields', 'auctionAdmin'); ?>" />
	</form>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php
	$table_name = $wpdb->prefix . "cp_ad_fields";
	$result = $wpdb->get_results("SELECT * FROM {$table_name} ORDER BY field_id ASC {$limit}");
	?>
<table class="widefat">
<thead>
    <tr>
        <th style="width:35px;"><?php _e('ID', 'auctionAdmin'); ?></th>
        <th style="width:150px;"><?php _e('Name', 'auctionAdmin'); ?></th>
        <th style="width:100px;"><?php _e('Type', 'auctionAdmin'); ?></th>
        <th><?php _e('Description', 'auctionAdmin'); ?></th>
        <th style="text-align:center;width:120px;"><?php _e('Ad Types', 'auctionAdmin'); ?></th>
        <th style="text-align:center;width:60px;"><?php _e('Price Field', 'auctionAdmin'); ?></th>
        <th style="text-align:center;width:60px;"><?php _e('Ad Details', 'auctionAdmin'); ?></th>
        <th style="width:120px;"><?php _e('Modified', 'auctionAdmin'); ?></th>
        <th style="text-align:center;width:80px;"><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th style="width:35px;"><?php _e('ID', 'auctionAdmin'); ?></th>
        <th style="width:150px;"><?php _e('Name', 'auctionAdmin'); ?></th>
        <th style="width:100px;"><?php _e('Type', 'auctionAdmin'); ?></th>
        <th><?php _e('Description', 'auctionAdmin'); ?></th>
        <th style="text-align:center;width:120px;"><?php _e('Ad Types', 'auctionAdmin'); ?></th>
        <th style="text-align:center;width:80px;"><?php _e('Price Field', 'auctionAdmin'); ?></th>
        <th style="text-align:center;width:80px;"><?php _e('Ad Details', 'auctionAdmin'); ?></th>
        <th style="width:120px;"><?php _e('Modified', 'auctionAdmin'); ?></th>
        <th style="text-align:center;width:80px;"><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	$option = get_option('cp_auction_fields');
	$options = get_option('cp_auction_details');
	$prices = get_option('cp_auction_pricefields');
	$rowclass = '';

	if ( $result ) {
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$id     = $row->field_id;
	$name	= $row->field_name;
	$label	= $row->field_label;
	$type	= $row->field_type;
	$desc   = $row->field_desc;
	$perm	= $row->field_perm;
	$mods	= $row->field_modified;

	    $flist = explode(',', $option[$name]);
	    if ( $options[$name] == "yes" ) $details = '<font color="green">'.__('Yes','auctionAdmin').'</font>';
	    else $details = ''.__('No','auctionAdmin').'';

	    if ( $prices[$name] == "yes" ) $pfield = '<font color="green">'.__('Yes','auctionAdmin').'</font>';
	    else $pfield = ''.__('No','auctionAdmin').'';

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td>#'.$id.'</td>';
            echo '<td><a href="?page=cp-auction&tab=fields&action=editfield&id='.$id.'"><strong>'.$label.'</strong></a></td>';
            echo '<td>'.$type.'</td>';
            echo '<td>'.$desc.'</td>';
            echo '<td>';
            if(in_array("classified", $flist)) echo '&nbsp;&nbsp;&nbsp;'.__('Classified Ads','auctionAdmin').'<br/>';
            if(in_array("wanted", $flist)) echo '&nbsp;&nbsp;&nbsp;'.__('Wanted Ads','auctionAdmin').'<br/>';
            if(in_array("normal", $flist)) echo '&nbsp;&nbsp;&nbsp;'.__('Normal Auction','auctionAdmin').'<br/>';
            if(in_array("reverse", $flist)) echo '&nbsp;&nbsp;&nbsp;'.__('Reverse Auction','auctionAdmin').'';
            echo '</td>';
            echo '<td style="text-align:center;">'.$pfield.'</td>';
            echo '<td style="text-align:center;">'.$details.'</td>';
            echo '<td>'.$mods.'</td>';
            echo '<td style="text-align:center;"><a href="?page=cp-auction&tab=fields&action=editfield&id='.$id.'"><img src="'.get_bloginfo('wpurl').'/wp-content/themes/classipress/images/edit.png" alt=""></a>&nbsp;&nbsp;&nbsp;'; if( $perm == '0' ) echo '<a href="?page=cp-auction&tab=fields&action=fdelete&id='.$id.'" onclick="javascript:return confirm(\''.__('WARNING: Deleting this field will prevent any existing ads currently using this field from displaying the field value. Deleting fields is NOT recommended unless you do not have any existing ads using this field. Are you sure you want to delete this field?? (This cannot be undone)','auctionAdmin').'\')"><img src="'.get_bloginfo('wpurl').'/wp-content/themes/classipress/images/cross.png" alt=""></a>'; else echo '<img src="'.get_bloginfo('wpurl').'/wp-content/themes/classipress/images/cross-grey.png" alt=""></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="8"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } ?>

<?php } else { ?>
	<div class="clear20"></div>
	<form method="post" action="">
	<input type="submit" name="add_new" class="button-primary" value="<?php _e('Add Custom Field', 'auctionAdmin'); ?>" />
	</form>
<?php
}
}