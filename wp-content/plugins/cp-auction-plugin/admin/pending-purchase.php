<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

include_once(ABSPATH.'/wp-content/plugins/cp-auction-plugin/scripts/pagination.class.php');

function cp_auction_plugin_pending_purchase() {

	global $post, $cpurl, $wpdb;

	$limit = "";
	$act = get_option('cp_auction_plugin_auctiontype');
?>

<script type="text/javascript">
	function update_pending(id)
	{
		 var quantity = jQuery("#quantity_"+id).val();
		 var unpaid = jQuery("#unpaid_"+id).val();

		 jQuery.ajax({
				method: 'get',
				url : '<?php echo cp_auction_plugin_url('url');?>/index.php/?_update_pending='+id+'&quantity='+quantity+'&unpaid='+unpaid,
				dataType : 'text',
				success: function() { window.location.reload(true); }
				});
}

</script>

<?php

/*
|--------------------------------------------------------------------------
| PENDING PAYMENTS
|--------------------------------------------------------------------------
*/

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$items = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} WHERE unpaid <> '0' OR paid = '' OR status = '' AND type = 'classified'" );

	if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-add-purchase&tab=pending");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

?>
<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$result = $wpdb->get_results("SELECT * FROM {$table_name} WHERE unpaid <> '0' OR paid = '' OR status = '' AND type = 'classified' ORDER BY id DESC {$limit}");
	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('PostID', 'auctionAdmin'); ?></th>
        <th><?php _e('AD Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Order ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Seller', 'auctionAdmin'); ?></th>
        <th><?php _e('Buyer', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Quantity', 'auctionAdmin'); ?></th>
        <th><?php _e('Unpaid', 'auctionAdmin'); ?></th>
        <th><?php _e('Payment', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('PostID', 'auctionAdmin'); ?></th>
        <th><?php _e('AD Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Order ID', 'auctionAdmin'); ?></th>
        <th><?php _e('Seller', 'auctionAdmin'); ?></th>
        <th><?php _e('Buyer', 'auctionAdmin'); ?></th>
        <th><?php _e('Date', 'auctionAdmin'); ?></th>
        <th><?php _e('Quantity', 'auctionAdmin'); ?></th>
        <th><?php _e('Unpaid', 'auctionAdmin'); ?></th>
        <th><?php _e('Payment', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $result ) {
	$rowclass = "";
    	foreach ( $result as $row ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
            $id      	= $row->id;
            $order_id	= $row->order_id;
            $pid        = $row->pid;
            $uid	= $row->uid;
            $buyerid  	= $row->buyer;
            $datemade  	= date('d-M-Y',$row->datemade);
            $quantity  	= $row->numbers;
            $unpaid  	= $row->unpaid;
            $paid	= $row->paid;
            $paiddate  	= date('d-M-Y',$row->paiddate);
            $type     	= $row->type;
            if( $paid == '' || $paid == 0 ) $payment = "failed";
            if( $paid == 1 ) $payment = "complete";
            if(( $paid == 1 ) && ( $unpaid > 0 )) $payment = "uncompleted";
            if( $paid == 2 ) $payment = "pending";
            if( $row->status == "unpaid" || $row->status == "paid" ) $status = ''.__('Deactivate','auctionAdmin').'';
	    if( $row->status == "deactivated" ) $status = '<font color="red">'.__('Deactivated','auctionAdmin').'</font>';

		global $post;
		$post = get_post($pid);
		$user = get_userdata($uid);
		$buyer = get_userdata($buyerid);

        echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
            echo '<td>'.$pid.'</td>';
            echo '<td>'.$post->post_title.'</td>';
            echo '<td>'.$order_id.'</td>';
            echo '<td><a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.'').'" target="_blank">'.$user->user_login.'</a></td>';
            echo '<td><a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$buyerid.'').'" target="_blank">'.$buyer->user_login.'</a></td>';
            echo '<td>'.$datemade.'</td>';
            echo '<td><input id="quantity_'.$id.'" type="text" name="quantity" value="'.$quantity.'" style="width:50px;" /></td>';
            echo '<td><input id="unpaid_'.$id.'" type="text" name="quantity" value="'.$unpaid.'" style="width:50px;" /></td>';
            echo '<td>'.$payment.'</td>';
            echo '<td><a href="javascript: void(0)" onclick="update_pending(\''.$id.'\')">'.__('Update', 'auctionAdmin').'</a> | <a href="#" class="deactivate-purchase" id="'.$id.'">'.$status.'</a> | <a href="#" class="delete-complete-purchase" title="'.__('Delete transaction.', 'auctionAdmin').'" id="'.$id.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';
}
} else { ?>
        <tr>
        <td colspan="10"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">

<font color="red"><strong>*</strong></font> <?php _e('If any payment has status as failed click on Update and status will change in accordance to the unpaid items, if unpaid is set to 1 payment status will change to pending. Set unpaid to 0 and payment status will change to complete and transaction will dissapear from the pending / failed list.', 'auctionAdmin'); ?>

    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
}
}