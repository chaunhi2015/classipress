<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_plugin_general_settings() {

		global $wpdb, $cp_auction_db_version, $cp_auction_db_date, $cp_options, $siteinfo;
		$msg = "";
		$act = get_option('cp_auction_plugin_auctiontype');

		if( isset($_POST['save_settings']) ) {

		$inst_folder = isset( $_POST['cp_auction_inst_folder'] ) ? esc_attr( $_POST['cp_auction_inst_folder'] ) : '';
		$auctiontype = isset( $_POST['cp_auction_plugin_auctiontype'] ) ? esc_attr( $_POST['cp_auction_plugin_auctiontype'] ) : '';
		$approves = isset( $_POST['cp_auction_plugin_approves'] ) ? esc_attr( $_POST['cp_auction_plugin_approves'] ) : '';
		$cla_approves = isset( $_POST['cp_auction_plugin_cla_approves'] ) ? esc_attr( $_POST['cp_auction_plugin_cla_approves'] ) : '';
		$wnt_approves = isset( $_POST['cp_auction_plugin_wnt_approves'] ) ? esc_attr( $_POST['cp_auction_plugin_wnt_approves'] ) : '';
		$max_size = isset( $_POST['cp_auction_plugin_max_size'] ) ? esc_attr( $_POST['cp_auction_plugin_max_size'] ) : '';
		$lastchance = isset( $_POST['cp_auction_plugin_lastchance'] ) ? esc_attr( $_POST['cp_auction_plugin_lastchance'] ) : '';
		$plain_html = isset( $_POST['cp_auction_plugin_plain_html'] ) ? esc_attr( $_POST['cp_auction_plugin_plain_html'] ) : '';
		$bid_anonymous = isset( $_POST['cp_auction_plugin_bid_anonymous'] ) ? esc_attr( $_POST['cp_auction_plugin_bid_anonymous'] ) : '';
		$cp_agreement = isset( $_POST['cp_auction_plugin_cp_agreement'] ) ? esc_attr( $_POST['cp_auction_plugin_cp_agreement'] ) : '';
		$deactivate_cp_agreement = isset( $_POST['cp_auction_deactivate_cp_agreement'] ) ? esc_attr( $_POST['cp_auction_deactivate_cp_agreement'] ) : '';
		$conf_popup = isset( $_POST['cp_auction_confirmation_popup'] ) ? esc_attr( $_POST['cp_auction_confirmation_popup'] ) : '';
		$deactivate_conf_popup = isset( $_POST['cp_auction_deactivate_confirmation_popup'] ) ? esc_attr( $_POST['cp_auction_deactivate_confirmation_popup'] ) : '';
		$deactivate_all_popups = isset( $_POST['cp_auction_deactivate_confirmation_popups'] ) ? esc_attr( $_POST['cp_auction_deactivate_confirmation_popups'] ) : '';
		$bidder_delete = isset( $_POST['cp_auction_plugin_bidder_delete'] ) ? esc_attr( $_POST['cp_auction_plugin_bidder_delete'] ) : '';
		$owner_delete = isset( $_POST['cp_auction_plugin_owner_delete'] ) ? esc_attr( $_POST['cp_auction_plugin_owner_delete'] ) : '';
		$price_type = isset( $_POST['cp_auction_plugin_price_type'] ) ? esc_attr( $_POST['cp_auction_plugin_price_type'] ) : '';
		$display_duration = isset( $_POST['cp_auction_display_max_duration'] ) ? esc_attr( $_POST['cp_auction_display_max_duration'] ) : '';
		$if_favorites = isset( $_POST['cp_auction_plugin_if_favorites'] ) ? esc_attr( $_POST['cp_auction_plugin_if_favorites'] ) : '';
		$if_watchlist = isset( $_POST['cp_auction_plugin_if_watchlist'] ) ? esc_attr( $_POST['cp_auction_plugin_if_watchlist'] ) : '';
		$verification = isset( $_POST['cp_auction_plugin_verification'] ) ? esc_attr( $_POST['cp_auction_plugin_verification'] ) : '';
		$verification_info = isset( $_POST['cp_auction_plugin_verification_info'] ) ? esc_attr( $_POST['cp_auction_plugin_verification_info'] ) : '';
		$upl_verification = isset( $_POST['cp_auction_plugin_upl_verification'] ) ? esc_attr( $_POST['cp_auction_plugin_upl_verification'] ) : '';
		$if_classified = isset( $_POST['classified'] ) ? esc_attr( $_POST['classified'] ) : '';
		$if_normal = isset( $_POST['auction'] ) ? esc_attr( $_POST['auction'] ) : '';
		$if_reverse = isset( $_POST['reverse'] ) ? esc_attr( $_POST['reverse'] ) : '';
		$if_wanted = isset( $_POST['wanted'] ) ? esc_attr( $_POST['wanted'] ) : '';
		$fee_classified = isset( $_POST['classified_fee'] ) ? esc_attr( $_POST['classified_fee'] ) : '';
		$fee_normal = isset( $_POST['auction_fee'] ) ? esc_attr( $_POST['auction_fee'] ) : '';
		$fee_reverse = isset( $_POST['reverse_fee'] ) ? esc_attr( $_POST['reverse_fee'] ) : '';
		$fee_wanted = isset( $_POST['wanted_fee'] ) ? esc_attr( $_POST['wanted_fee'] ) : '';
		$if_only_admins = isset( $_POST['only_admins'] ) ? esc_attr( $_POST['only_admins'] ) : '';
		$if_avatar = isset( $_POST['cp_auction_plugin_if_avatar'] ) ? esc_attr( $_POST['cp_auction_plugin_if_avatar'] ) : '';
		$admins_only = isset( $_POST['cp_auction_plugin_admins_only'] ) ? esc_attr( $_POST['cp_auction_plugin_admins_only'] ) : '';
		$banner_url = isset( $_POST['cp_auction_plugin_banner_url'] ) ? esc_attr( $_POST['cp_auction_plugin_banner_url'] ) : '';
		$banner_link = isset( $_POST['cp_auction_plugin_banner_link'] ) ? esc_attr( $_POST['cp_auction_plugin_banner_link'] ) : '';
		$widget_title = isset( $_POST['cp_auction_plugin_widget_title'] ) ? esc_attr( $_POST['cp_auction_plugin_widget_title'] ) : '';
		$start_timer = isset( $_POST['cp_auction_start_timer'] ) ? esc_attr( $_POST['cp_auction_start_timer'] ) : '';
		$classified_auction = isset( $_POST['cp_auction_enable_classified_auction'] ) ? esc_attr( $_POST['cp_auction_enable_classified_auction'] ) : '';
		$classified_exp = isset( $_POST['cp_auction_classified_expiration'] ) ? esc_attr( $_POST['cp_auction_classified_expiration'] ) : '';
		$period = ""; if( isset( $_POST['period'] ) ) $period = join(",", $_POST['period']);
		$classified_wanted = isset( $_POST['cp_auction_enable_classified_wanted'] ) ? esc_attr( $_POST['cp_auction_enable_classified_wanted'] ) : '';
		$wanted_info = isset( $_POST['cp_auction_plugin_wanted_info'] ) ? esc_attr( $_POST['cp_auction_plugin_wanted_info'] ) : '';
		$ad_enabled = isset( $_POST['cp_auction_buy_now_ad_enabled'] ) ? esc_attr( $_POST['cp_auction_buy_now_ad_enabled'] ) : '';
		$time_incart = isset( $_POST['cp_auction_hold_in_cart'] ) ? esc_attr( $_POST['cp_auction_hold_in_cart'] ) : '';
		$hide_button = isset( $_POST['cp_auction_hide_buy_now_button'] ) ? esc_attr( $_POST['cp_auction_hide_buy_now_button'] ) : '';
		$search_shopping = isset( $_POST['cp_auction_enable_search_shopping'] ) ? esc_attr( $_POST['cp_auction_enable_search_shopping'] ) : '';
		$loop_shopping = isset( $_POST['cp_auction_enable_loop_shopping'] ) ? esc_attr( $_POST['cp_auction_enable_loop_shopping'] ) : '';
		$important_info = isset( $_POST['cp_auction_plugin_important_info'] ) ? esc_attr( $_POST['cp_auction_plugin_important_info'] ) : '';
		$sold_banner = isset( $_POST['cp_auction_plugin_sold_banner'] ) ? esc_attr( $_POST['cp_auction_plugin_sold_banner'] ) : '';
		$sold_ribbon = isset( $_POST['cp_auction_sold_ribbon'] ) ? esc_attr( $_POST['cp_auction_sold_ribbon'] ) : '';
		$featured_ribbon = isset( $_POST['cp_auction_featured_ribbon'] ) ? esc_attr( $_POST['cp_auction_featured_ribbon'] ) : '';
		$force_upload = isset( $_POST['cp_auction_force_upload'] ) ? esc_attr( $_POST['cp_auction_force_upload'] ) : '';
		$mrp_force_upload = isset( $_POST['cp_auction_mrp_force_upload'] ) ? esc_attr( $_POST['cp_auction_mrp_force_upload'] ) : '';
		$auction_ribbon = isset( $_POST['cp_auction_auction_ribbon'] ) ? esc_attr( $_POST['cp_auction_auction_ribbon'] ) : '';
		$reverse_ribbon = isset( $_POST['cp_auction_reverse_ribbon'] ) ? esc_attr( $_POST['cp_auction_reverse_ribbon'] ) : '';
		$wanted_ribbon = isset( $_POST['cp_auction_wanted_ribbon'] ) ? esc_attr( $_POST['cp_auction_wanted_ribbon'] ) : '';
		$classified_ribbon = isset( $_POST['cp_auction_classified_ribbon'] ) ? esc_attr( $_POST['cp_auction_classified_ribbon'] ) : '';
		$users_ribbon = isset( $_POST['cp_auction_users_ribbon'] ) ? esc_attr( $_POST['cp_auction_users_ribbon'] ) : '';
		$enable_comments = isset( $_POST['cp_auction_enable_comments'] ) ? esc_attr( $_POST['cp_auction_enable_comments'] ) : '';
		$enable_freebids = isset( $_POST['cp_auction_enable_freebids'] ) ? esc_attr( $_POST['cp_auction_enable_freebids'] ) : '';
		$last_freebids = isset( $_POST['cp_auction_last_freebids'] ) ? esc_attr( $_POST['cp_auction_last_freebids'] ) : '';
		$enable_bid_widget = isset( $_POST['cp_auction_enable_bid_widget'] ) ? esc_attr( $_POST['cp_auction_enable_bid_widget'] ) : '';
		$remove_map_tab = isset( $_POST['cp_auction_remove_map_tab'] ) ? esc_attr( $_POST['cp_auction_remove_map_tab'] ) : '';
		$auction_buy_widget = isset( $_POST['cp_auction_enable_auction_buy_widget'] ) ? esc_attr( $_POST['cp_auction_enable_auction_buy_widget'] ) : '';
		$enable_wanted_widget = isset( $_POST['cp_auction_enable_wanted_widget'] ) ? esc_attr( $_POST['cp_auction_enable_wanted_widget'] ) : '';
		$enable_wanted_loop = isset( $_POST['cp_auction_enable_wanted_loop'] ) ? esc_attr( $_POST['cp_auction_enable_wanted_loop'] ) : '';
		$file_extensions = isset( $_POST['cp_auction_file_extensions'] ) ? esc_attr( $_POST['cp_auction_file_extensions'] ) : '';
		$use_hooks = isset( $_POST['cp_auction_use_hooks'] ) ? esc_attr( $_POST['cp_auction_use_hooks'] ) : '';
		$buy_hooks = isset( $_POST['cp_auction_use_buy_hooks'] ) ? esc_attr( $_POST['cp_auction_use_buy_hooks'] ) : '';
		$wanted_hooks = isset( $_POST['cp_auction_use_wanted_hooks'] ) ? esc_attr( $_POST['cp_auction_use_wanted_hooks'] ) : '';
		$breadcrumb = isset( $_POST['cp_auction_disable_breadcrumb'] ) ? esc_attr( $_POST['cp_auction_disable_breadcrumb'] ) : '';
		$negotiable = isset( $_POST['cp_auction_enable_negotiable'] ) ? esc_attr( $_POST['cp_auction_enable_negotiable'] ) : '';
		$redir_dash = isset( $_POST['cp_auction_redirect_dashboard'] ) ? esc_attr( $_POST['cp_auction_redirect_dashboard'] ) : '';
		$redir_new = isset( $_POST['cp_auction_redirect_post_new'] ) ? esc_attr( $_POST['cp_auction_redirect_post_new'] ) : '';
		$post_access = isset( $_POST['cp_auction_post_access'] ) ? esc_attr( $_POST['cp_auction_post_access'] ) : '';
		$pause_ad = isset( $_POST['cp_auction_allow_pause_ad'] ) ? esc_attr( $_POST['cp_auction_allow_pause_ad'] ) : '';
		$upload_classified = isset( $_POST['upload_classified'] ) ? esc_attr( $_POST['upload_classified'] ) : '';
		$upload_normal = isset( $_POST['upload_normal'] ) ? esc_attr( $_POST['upload_normal'] ) : '';
		$upload_reverse = isset( $_POST['upload_reverse'] ) ? esc_attr( $_POST['upload_reverse'] ) : '';
		$account_type = isset( $_POST['cp_auction_enable_account_type'] ) ? esc_attr( $_POST['cp_auction_enable_account_type'] ) : '';
		$disable_company = isset( $_POST['cp_auction_disable_company'] ) ? esc_attr( $_POST['cp_auction_disable_company'] ) : '';
		$disable_address = isset( $_POST['cp_auction_disable_address'] ) ? esc_attr( $_POST['cp_auction_disable_address'] ) : '';
		$disable_zipcode = isset( $_POST['cp_auction_disable_zipcode'] ) ? esc_attr( $_POST['cp_auction_disable_zipcode'] ) : '';
		$disable_city = isset( $_POST['cp_auction_disable_city'] ) ? esc_attr( $_POST['cp_auction_disable_city'] ) : '';
		$disable_state = isset( $_POST['cp_auction_disable_state'] ) ? esc_attr( $_POST['cp_auction_disable_state'] ) : '';
		$disable_country = isset( $_POST['cp_auction_disable_country'] ) ? esc_attr( $_POST['cp_auction_disable_country'] ) : '';
		$disable_phone = isset( $_POST['cp_auction_disable_phone'] ) ? esc_attr( $_POST['cp_auction_disable_phone'] ) : '';
		$disable_paypal = isset( $_POST['cp_auction_disable_paypal'] ) ? esc_attr( $_POST['cp_auction_disable_paypal'] ) : '';
		$report_ad = isset( $_POST['cp_auction_enable_report_ad'] ) ? esc_attr( $_POST['cp_auction_enable_report_ad'] ) : '';
		$pages_timer = isset( $_POST['cp_auction_pages_timer'] ) ? esc_attr( $_POST['cp_auction_pages_timer'] ) : '';
		$single_timer = isset( $_POST['cp_auction_single_timer'] ) ? esc_attr( $_POST['cp_auction_single_timer'] ) : '';
		$widgets_timer = isset( $_POST['cp_auction_widgets_timer'] ) ? esc_attr( $_POST['cp_auction_widgets_timer'] ) : '';
		$bids_timer = isset( $_POST['cp_auction_bids_timer'] ) ? esc_attr( $_POST['cp_auction_bids_timer'] ) : '';

		if( $inst_folder ) {
		update_option('cp_auction_inst_folder',$inst_folder);
		} else {
		delete_option('cp_auction_inst_folder');
		}
		if( $ad_enabled == "yes" ) {
		global $wpdb;
		$created = date('Y-m-d h:i:s');
		$option = get_option('cp_auction_fields');
		$options = get_option('cp_auction_details');
		$prices = get_option('cp_auction_pricefields');
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = 'cp_buy_now_enabled'");
		if( empty( $res ) ) {
		$query = "INSERT INTO {$table_name} (field_name, field_label, field_desc, field_type, field_values, field_perm, field_core, field_req, field_owner, field_created, field_modified) VALUES (%s, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s)";
		$wpdb->query($wpdb->prepare($query, 'cp_buy_now_enabled', ''.__('Enable Buy Now','auctionAdmin').'', ''.__('This is the Enable Buy Now on Ads field for all ad types. It is a core CP Auction field and should not be deleted unless this option is disabled in general settings.','auctionAdmin').'', 'checkbox', 'Tick to enable', '0', '1', '0', 'CP Auction', $created, $created));
		}
		$list = "classified,normal,reverse";
		$array = explode( ',', $list );
		$new_list = implode(",", $array);
		$field_ads = trim($new_list, ',');
		$option['cp_buy_now_enabled'] = $field_ads;
		if ( ! empty( $option ) )
   		update_option('cp_auction_fields', $option);
		update_option('cp_auction_buy_now_ad_enabled',$ad_enabled);
   		$options['cp_buy_now_enabled'] = 'no';
   		if ( ! empty( $options ) )
   		update_option('cp_auction_details', $options);
   		$prices['cp_buy_now_enabled'] = 'no';
   		if ( ! empty( $prices ) )
   		update_option('cp_auction_pricefields', $prices);
		} else {
		global $wpdb;
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE field_name = '%s'", 'cp_buy_now_enabled'));
		delete_option('cp_auction_buy_now_ad_enabled');
		}
		if( $time_incart ) {
		update_option( 'cp_auction_hold_in_cart', $time_incart );
		} else {
		delete_option( 'cp_auction_hold_in_cart' );
		}
		if( $hide_button == "yes" ) {
		update_option( 'cp_auction_hide_buy_now_button', $hide_button );
		} else {
		delete_option( 'cp_auction_hide_buy_now_button' );
		}
		if( $search_shopping == "yes" ) {
		update_option('cp_auction_enable_search_shopping',$search_shopping);
		} else {
		delete_option('cp_auction_enable_search_shopping');
		}
		if( $loop_shopping == "yes" ) {
		update_option('cp_auction_enable_loop_shopping',$loop_shopping);
		} else {
		delete_option('cp_auction_enable_loop_shopping');
		}
		if( $classified_auction == "yes" ) {
		global $wpdb;
		$created = date('Y-m-d h:i:s');
		$option = get_option('cp_auction_fields');
		$options = get_option('cp_auction_details');
		$prices = get_option('cp_auction_pricefields');
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = 'cp_auction'");
		if( empty( $res ) ) {
		$query = "INSERT INTO {$table_name} (field_name, field_label, field_desc, field_type, field_values, field_perm, field_core, field_req, field_owner, field_created, field_modified) VALUES (%s, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s)";
		$wpdb->query($wpdb->prepare($query, 'cp_auction', ''.__('Enable Auction','auctionAdmin').'', ''.__('This is the Auction on ClassiPress field for the classified ad. It is a core CP Auction field and should not be deleted unless Auction on Classifieds are disabled.','auctionAdmin').'', 'drop-down', 'No,Yes', '0', '1', '0', 'CP Auction', $created, $created));
		}
		$option['cp_auction'] = 'classified';
		if ( ! empty( $option ) )
   		update_option('cp_auction_fields', $option);
		update_option('cp_auction_enable_classified_auction',$classified_auction);
   		$options['cp_auction'] = 'no';
   		if ( ! empty( $options ) )
   		update_option('cp_auction_details', $options);
   		$prices['cp_auction'] = 'no';
   		if ( ! empty( $prices ) )
   		update_option('cp_auction_pricefields', $prices);
		} else {
		global $wpdb;
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE field_name = '%s'", 'cp_auction'));
		delete_option('cp_auction_enable_classified_auction');
		}
		if( $classified_exp ) {
		update_option('cp_auction_classified_expiration',$classified_exp);
		} else {
		delete_option('cp_auction_classified_expiration');
		}
		if( $period ) {
		update_option('cp_auction_period_type',$period);
		} else {
		delete_option('cp_auction_period_type');
		}
		if( $classified_wanted == "yes" ) {
		$option = get_option('cp_auction_fields');
		$option['cp_want_to'] = 'wanted,classified';
		$option['cp_max_price'] = 'wanted,classified';
		$option['cp_swap_with'] = 'wanted,classified';
		$option['cp_allow_deals'] = 'wanted,classified';
		if ( ! empty( $option ) )
   		update_option('cp_auction_fields', $option);
		update_option('cp_auction_enable_classified_wanted',$classified_wanted);
		} else {
		$option = get_option('cp_auction_fields');
		$option['cp_want_to'] = 'wanted';
		$option['cp_max_price'] = 'wanted';
		$option['cp_swap_with'] = 'wanted';
		$option['cp_allow_deals'] = 'wanted';
		if ( ! empty( $option ) )
   		update_option('cp_auction_fields', $option);
		delete_option('cp_auction_enable_classified_wanted');
		}
		if( $wanted_info ) {
		update_option('cp_auction_plugin_wanted_info',stripslashes(html_entity_decode($wanted_info)));
		} else {
		delete_option('cp_auction_plugin_wanted_info');
		}
		if( $widget_title ) {
		update_option('cp_auction_plugin_widget_title',$widget_title);
		} else {
		delete_option('cp_auction_plugin_widget_title');
		}
		if( $start_timer == "yes" ) {
		update_option('cp_auction_start_timer',$start_timer);
		} else {
		delete_option('cp_auction_start_timer');
		}
		if( $banner_link ) {
		update_option('cp_auction_plugin_banner_link',$banner_link);
		} else {
		delete_option('cp_auction_plugin_banner_link');
		}
		if( $banner_url ) {
		update_option('cp_auction_plugin_banner_url',$banner_url);
		} else {
		delete_option('cp_auction_plugin_banner_url');
		}
		if( $admins_only == "yes" ) {
		update_option('cp_auction_plugin_admins_only',$admins_only);
		} else {
		delete_option('cp_auction_plugin_admins_only');
		}
		if( $if_avatar == "yes" ) {
		update_option('cp_auction_plugin_if_avatar',$if_avatar);
		} else {
		delete_option('cp_auction_plugin_if_avatar');
		}
		if( $if_reverse ) {
		update_option('cp_auction_plugin_reverse',$if_reverse);
		} else {
		delete_option('cp_auction_plugin_reverse');
		}
		if( $if_normal ) {
		update_option('cp_auction_plugin_normal',$if_normal);
		} else {
		delete_option('cp_auction_plugin_normal');
		}
		if( $if_classified ) {
		update_option('cp_auction_plugin_classified',$if_classified);
		} else {
		delete_option('cp_auction_plugin_classified');
		}
		if( $if_wanted ) {
		update_option('cp_auction_plugin_wanted',$if_wanted);
		} else {
		delete_option('cp_auction_plugin_wanted');
		}
		if( $fee_reverse ) {
		update_option('cp_auction_plugin_reverse_fee',$fee_reverse);
		} else {
		delete_option('cp_auction_plugin_reverse_fee');
		}
		if( $fee_normal ) {
		update_option('cp_auction_plugin_normal_fee',$fee_normal);
		} else {
		delete_option('cp_auction_plugin_normal_fee');
		}
		if( $fee_classified ) {
		update_option('cp_auction_plugin_classified_fee',$fee_classified);
		} else {
		delete_option('cp_auction_plugin_classified_fee');
		}
		if( $fee_wanted ) {
		update_option('cp_auction_plugin_wanted_fee',$fee_wanted);
		} else {
		delete_option('cp_auction_plugin_wanted_fee');
		}
		if( $if_only_admins ) {
		update_option('cp_auction_only_admins_post_auctions',$if_only_admins);
		} else {
		delete_option('cp_auction_only_admins_post_auctions');
		}
		if( $upl_verification == "yes" ) {
		update_option('cp_auction_plugin_upl_verification',$upl_verification);
		} else {
		delete_option('cp_auction_plugin_upl_verification');
		}
		if( $verification_info ) {
		update_option('cp_auction_plugin_verification_info',stripslashes(html_entity_decode($verification_info)));
		} else {
		delete_option('cp_auction_plugin_verification_info');
		}
		if( $verification ) {
		update_option('cp_auction_plugin_verification',cp_auction_sanitize_amount($verification));
		} else {
		delete_option('cp_auction_plugin_verification');
		}
		if( $if_watchlist == "yes" ) {
		update_option('cp_auction_plugin_if_watchlist',$if_watchlist);
		} else {
		delete_option('cp_auction_plugin_if_watchlist');
		}
		if( $if_favorites == "yes" ) {
		update_option('cp_auction_plugin_if_favorites',$if_favorites);
		} else {
		delete_option('cp_auction_plugin_if_favorites');
		}
		if( $display_duration == "yes" ) {
		update_option('cp_auction_display_max_duration',$display_duration);
		} else {
		delete_option('cp_auction_display_max_duration');
		}
		if( $price_type ) {
		update_option('cp_auction_plugin_price_type',$price_type);
		} else {
		delete_option('cp_auction_plugin_price_type');
		}
		if( $bidder_delete == "yes" ) {
		update_option('cp_auction_plugin_bidder_delete',$bidder_delete);
		} else {
		delete_option('cp_auction_plugin_bidder_delete');
		}
		if( $owner_delete == "yes" ) {
		update_option('cp_auction_plugin_owner_delete',$owner_delete);
		} else {
		delete_option('cp_auction_plugin_owner_delete');
		}
		if( $cp_agreement ) {
		update_option('cp_auction_plugin_cp_agreement',stripslashes(html_entity_decode($cp_agreement)));
		} else {
		delete_option('cp_auction_plugin_cp_agreement');
		}
		if( $deactivate_cp_agreement == "yes" ) {
		update_option( 'cp_auction_deactivate_cp_agreement', $deactivate_cp_agreement );
		} else {
		delete_option( 'cp_auction_deactivate_cp_agreement' );
		}
		if( $conf_popup ) {
		update_option('cp_auction_confirmation_popup',stripslashes(html_entity_decode($conf_popup)));
		} else {
		delete_option('cp_auction_confirmation_popup');
		}
		if( $deactivate_conf_popup == "yes" ) {
		update_option( 'cp_auction_deactivate_confirmation_popup', $deactivate_conf_popup );
		} else {
		delete_option( 'cp_auction_deactivate_confirmation_popup' );
		}
		if( $deactivate_all_popups == "yes" ) {
		update_option( 'cp_auction_deactivate_confirmation_popups', $deactivate_all_popups );
		} else {
		delete_option( 'cp_auction_deactivate_confirmation_popups' );
		}
		if( $bid_anonymous == "yes" ) {
		update_option('cp_auction_plugin_bid_anonymous',$bid_anonymous);
		} else {
		delete_option('cp_auction_plugin_bid_anonymous');
		}
		if( $plain_html ) {
		update_option('cp_auction_plugin_plain_html',$plain_html);
		} else {
		delete_option('cp_auction_plugin_plain_html');
		}
		if( $lastchance ) {
		update_option('cp_auction_plugin_lastchance',$lastchance);
		} else {
		delete_option('cp_auction_plugin_lastchance');
		}
		if( $max_size ) {
		update_option('cp_auction_plugin_max_size',$max_size);
		} else {
		delete_option('cp_auction_plugin_max_size');
		}
		if( $approves == "yes" ) {
		update_option('cp_auction_plugin_approves',$approves);
		} else {
		delete_option('cp_auction_plugin_approves');
		}
		if( $cla_approves == "yes" ) {
		update_option('cp_auction_plugin_cla_approves',$cla_approves);
		} else {
		delete_option('cp_auction_plugin_cla_approves');
		}
		if( $wnt_approves == "yes" ) {
		update_option('cp_auction_plugin_wnt_approves',$wnt_approves);
		} else {
		delete_option('cp_auction_plugin_wnt_approves');
		}
		if( $auctiontype ) {
		update_option('cp_auction_plugin_auctiontype',$auctiontype);
		} else {
		delete_option('cp_auction_plugin_auctiontype');
		}
		if( $important_info ) {
		update_option('cp_auction_plugin_important_info',stripslashes(html_entity_decode($important_info)));
		} else {
		delete_option('cp_auction_plugin_important_info');
		}
		if( $sold_banner == "yes" ) {
		update_option('cp_auction_plugin_sold_banner',$sold_banner);
		} else {
		delete_option('cp_auction_plugin_sold_banner');
		}
		if( $sold_ribbon == "yes" ) {
		update_option('cp_auction_sold_ribbon',$sold_ribbon);
		} else {
		delete_option('cp_auction_sold_ribbon');
		}
		if( $featured_ribbon == "yes" ) {
		update_option('cp_auction_featured_ribbon',$featured_ribbon);
		} else {
		delete_option('cp_auction_featured_ribbon');
		}
		if( $force_upload == "yes" ) {
		update_option('cp_auction_force_upload',$force_upload);
		} else {
		delete_option('cp_auction_force_upload');
		}
		if( $mrp_force_upload == "yes" ) {
		update_option('cp_auction_mrp_force_upload',$mrp_force_upload);
		} else {
		delete_option('cp_auction_mrp_force_upload');
		}
		if( $auction_ribbon ) {
		update_option('cp_auction_auction_ribbon',$auction_ribbon);
		} else {
		delete_option('cp_auction_auction_ribbon');
		}
		if( $reverse_ribbon ) {
		update_option('cp_auction_reverse_ribbon',$reverse_ribbon);
		} else {
		delete_option('cp_auction_reverse_ribbon');
		}
		if( $wanted_ribbon ) {
		update_option('cp_auction_wanted_ribbon',$wanted_ribbon);
		} else {
		delete_option('cp_auction_wanted_ribbon');
		}
		if( $classified_ribbon ) {
		update_option('cp_auction_classified_ribbon',$classified_ribbon);
		} else {
		delete_option('cp_auction_classified_ribbon');
		}
		if( $users_ribbon ) {
		update_option('cp_auction_users_ribbon',$users_ribbon);
		} else {
		delete_option('cp_auction_users_ribbon');
		}
		if( $enable_comments == "yes" ) {
		update_option('cp_auction_enable_comments',$enable_comments);
		} else {
		delete_option('cp_auction_enable_comments');
		}
		if( $enable_bid_widget == "yes" ) {
		update_option('cp_auction_enable_bid_widget',$enable_bid_widget);
		} else {
		delete_option('cp_auction_enable_bid_widget');
		}
		if( $remove_map_tab == "yes" ) {
		update_option('cp_auction_remove_map_tab',$remove_map_tab);
		} else {
		delete_option('cp_auction_remove_map_tab');
		}
		if( $enable_freebids == "yes" ) {
		update_option('cp_auction_enable_freebids',$enable_freebids);
		} else {
		delete_option('cp_auction_enable_freebids');
		}
		if( $last_freebids == "yes" ) {
		update_option('cp_auction_last_freebids',$last_freebids);
		} else {
		delete_option('cp_auction_last_freebids');
		}
		if( $auction_buy_widget == "yes" ) {
		update_option('cp_auction_enable_auction_buy_widget',$auction_buy_widget);
		} else {
		delete_option('cp_auction_enable_auction_buy_widget');
		}
		if( $enable_wanted_widget == "yes" ) {
		update_option('cp_auction_enable_wanted_widget',$enable_wanted_widget);
		} else {
		delete_option('cp_auction_enable_wanted_widget');
		}
		if( $enable_wanted_loop == "yes" ) {
		update_option('cp_auction_enable_wanted_loop',$enable_wanted_loop);
		} else {
		delete_option('cp_auction_enable_wanted_loop');
		}
		if( $file_extensions ) {
		update_option('cp_auction_file_extensions',$file_extensions);
		} else {
		delete_option('cp_auction_file_extensions');
		}
		if( $use_hooks == "yes" ) {
		update_option('cp_auction_use_hooks',$use_hooks);
		} else {
		delete_option('cp_auction_use_hooks');
		}
		if( $buy_hooks == "yes" ) {
		update_option('cp_auction_use_buy_hooks',$buy_hooks);
		} else {
		delete_option('cp_auction_use_buy_hooks');
		}
		if( $wanted_hooks == "yes" ) {
		update_option('cp_auction_use_wanted_hooks',$wanted_hooks);
		} else {
		delete_option('cp_auction_use_wanted_hooks');
		}
		if( $breadcrumb == "yes" ) {
		update_option('cp_auction_disable_breadcrumb',$breadcrumb);
		} else {
		delete_option('cp_auction_disable_breadcrumb');
		}
		if( $negotiable == "yes" ) {
		update_option('cp_auction_enable_negotiable',$negotiable);
		} else {
		delete_option('cp_auction_enable_negotiable');
		}
		if( $redir_dash == "yes" ) {
		update_option('cp_auction_redirect_dashboard',$redir_dash);
		} else {
		delete_option('cp_auction_redirect_dashboard');
		}
		if( $redir_new == "yes" ) {
		update_option('cp_auction_redirect_post_new',$redir_new);
		} else {
		delete_option('cp_auction_redirect_post_new');
		}
		if( $post_access == "yes" ) {
		update_option('cp_auction_post_access',$post_access);
		} else {
		delete_option('cp_auction_post_access');
		}
		if( $pause_ad == "yes" ) {
		global $wpdb;
		$created = date('Y-m-d h:i:s');
		$option = get_option('cp_auction_fields');
		$options = get_option('cp_auction_details');
		$prices = get_option('cp_auction_pricefields');
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = 'cp_pausead'");
		if( empty( $res ) ) {
		$query = "INSERT INTO {$table_name} (field_name, field_label, field_desc, field_type, field_values, field_perm, field_core, field_req, field_owner, field_created, field_modified) VALUES (%s, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s)";
		$wpdb->query($wpdb->prepare($query, 'cp_pausead', ''.__('Set Ad Status','auctionAdmin').'', ''.__('This is the ad status field for all ad types. Author can through out the advertising process choose to pause their ad or make it private. It is a core CP Auction field and should not be deleted unless the Author set Ad Status settings are disabled.','auctionAdmin').'', 'drop-down', 'Public,Private,Paused', '0', '1', '0', 'CP Auction', $created, $created));
		}
		$option['cp_pausead'] = 'classified,wanted,normal,reverse';
		$options['cp_pausead'] = 'no';
		$prices['cp_pausead'] = 'no';
		if ( ! empty( $option ) )
   		update_option('cp_auction_fields', $option);
   		if ( ! empty( $options ) )
   		update_option('cp_auction_details', $options);
   		if ( ! empty( $prices ) )
   		update_option('cp_auction_pricefields', $prices);
		update_option('cp_auction_allow_pause_ad',$pause_ad);
		} else {
		global $wpdb;
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE field_name = '%s'", 'cp_pausead'));
		delete_option('cp_auction_allow_pause_ad');
		}
		if( $account_type == "yes" ) {
		update_option('cp_auction_enable_account_type',$account_type);
		} else {
		delete_option('cp_auction_enable_account_type');
		}
		if( $disable_company != "no" ) {
		update_option('cp_auction_disable_company',$disable_company);
		} else {
		delete_option('cp_auction_disable_company');
		}
		if( $disable_address != "no" ) {
		update_option('cp_auction_disable_address',$disable_address);
		} else {
		delete_option('cp_auction_disable_address');
		}
		if( $disable_zipcode != "no" ) {
		update_option('cp_auction_disable_zipcode',$disable_zipcode);
		} else {
		delete_option('cp_auction_disable_zipcode');
		}
		if( $disable_city != "no" ) {
		update_option('cp_auction_disable_city',$disable_city);
		} else {
		delete_option('cp_auction_disable_city');
		}
		if( $disable_state != "no" ) {
		update_option('cp_auction_disable_state',$disable_state);
		} else {
		delete_option('cp_auction_disable_state');
		}
		if( $disable_country != "no" ) {
		update_option('cp_auction_disable_country',$disable_country);
		} else {
		delete_option('cp_auction_disable_country');
		}
		if( $disable_phone != "no" ) {
		update_option('cp_auction_disable_phone',$disable_phone);
		} else {
		delete_option('cp_auction_disable_phone');
		}
		if( $disable_paypal != "no" ) {
		update_option('cp_auction_disable_paypal',$disable_paypal);
		} else {
		delete_option('cp_auction_disable_paypal');
		}
		if( $report_ad == "yes" ) {
		update_option('cp_auction_enable_report_ad',$report_ad);
		} else {
		delete_option('cp_auction_enable_report_ad');
		}

		$timer_option = get_option('cp_auction_timer');
		$timer_list = ''.$pages_timer.','.$single_timer.','.$widgets_timer.','.$bids_timer.'';
		$timer_array = explode( ',', $timer_list );
		$timer_new_list = implode(",", $timer_array);
		$positions = trim($timer_new_list, ',');

		if( ! $timer_option ) $timer_option = array();
		$timer_option['cp_disable_timer'] = $positions;
		if ( ! empty( $timer_option ) )
   		update_option('cp_auction_timer', $timer_option);

		$available = get_option('cp_auction_uploads');
		$upl_list = ''.$upload_classified.','.$upload_normal.','.$upload_reverse.'';
		$upl_array = explode( ',', $upl_list );
		$new_ulist = implode(",", $upl_array);
		$uploader = trim($new_ulist, ',');

		$available['cp_ad_types'] = $uploader;
		if ( ! empty( $available ) ) {
   		update_option('cp_auction_uploads', $available);

		$avail = get_option('cp_auction_uploads');
		$upl = explode(',', $avail['cp_ad_types']);
		$table_posts = $wpdb->prefix . "posts";
   		$result = $wpdb->get_results("SELECT * FROM {$table_posts} WHERE post_type = '".APP_POST_TYPE."' AND post_status = 'publish' OR post_status = 'pending' ORDER BY ID ASC");
   		if( $result ) {
   		foreach($result as $res) {
   		$pid = $res->ID;
   		$uid = $res->post_author;
   		$ad_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );
   		if( in_array($ad_type, $upl) ) {
   		$table_upl = $wpdb->prefix . "cp_auction_plugin_uploads";
   		$uploads = $wpdb->get_row("SELECT * FROM {$table_upl} WHERE pid = '$pid' ORDER BY id DESC");
   		if( !$uploads ) {
   		$query = "INSERT INTO {$table_upl} (pid, post_author) VALUES (%d, %d)";
   		$wpdb->query($wpdb->prepare($query, $pid, $uid));
   		}
   		}
   		else {
   		$table_upl = $wpdb->prefix . "cp_auction_plugin_uploads";
   		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_upl} WHERE pid = '%d'", $pid));
   		}
   		}
		}
	}

	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Settings was saved!','auctionAdmin').'</strong></p></div>';
}
	$available = get_option('cp_auction_uploads');
	$upl = explode(',', $available['cp_ad_types']);
	$pos = get_option('cp_auction_timer');
	$list = explode(',', $pos['cp_disable_timer']);

	$cdn_url = '<a style="text-decoration: none;" href="'.get_bloginfo('wpurl').'/wp-admin/admin.php?page=app-settings&tab=advanced">Google CDN jQuery</a>';
	$trans_url = '<a style="text-decoration: none;" href="'.get_bloginfo('wpurl').'/wp-admin/admin.php?page=cp-transactions">'.__('transactions', 'auctionAdmin').'</a>';

	$option = get_option('cp_auction_period_type');
	$period = explode(',', $option);

	$farray = array();
	$farray[] = get_option('cp_auction_plugin_classified_fee');
	$farray[] = get_option('cp_auction_plugin_wanted_fee');
	$farray[] = get_option('cp_auction_plugin_normal_fee');
	$farray[] = get_option('cp_auction_plugin_reverse_fee');

	if ( ( get_option( 'cp_auction_override_payment_file' ) != "yes" ) && ( in_array( "classified", $farray ) || in_array( "wanted", $farray ) || in_array( "normal", $farray ) || in_array( "reverse", $farray ) ) ) {
	include_once( CP_AUCTION_PLUGIN_DIR .'/scripts/copyus.php' );
	cp_auction_override_payments();
	update_option( 'cp_auction_override_payment_file', "yes" );
	}

?>

	<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('General Settings', 'auctionAdmin'); ?></h3>
            <div class="inside">

<form method="post" action="">

<div class="admin_header"><?php _e('The Sky`s The Limit!', 'auctionAdmin'); ?></div>
	<div class="admin_text"><?php _e('Welcome to v', 'auctionAdmin'); ?><?php echo CP_AUCTION_PLUGIN_VERSION ?> <?php _e('of our essential auction & ecommerce plugin. I recommend that you make it a habit that every time you update to the latest version then take a review of all the settings and re-save if necessary.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('Depending on how you choose to set up your online store it will be of importance that you create the <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=pages">necessary pages</a> and use the post an ad page that follows CP Auction plugin, either by <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=setup">replacing the post an ad button</a> in the header, or by using the redirect option (recommended) in the below general settings. Have a thorough review of the <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=fields">custom fields</a> settings and confirm that all custom fields have the correct association with the post an ad form. Be aware that some fields are required to make all functions work properly.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><?php _e('If you want to change the ad listing period for auctions that can be done via <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=fields">custom field</a> Ad Listing Period. The default period is set to 1,3,5,7,10 days and are selectable for author via Ad Listing Period dropdown box during the post an ad process.', 'auctionAdmin'); ?></div>

	<div class="admin_text"><strong><?php _e('Please carefully read through the contents under <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=fields">Custom Fields</a>, <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=misc">Misc</a> and <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=setup">Setup</a> as this content can have a major impact on how CP Auction works.', 'auctionAdmin'); ?></strong></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><strong><?php _e('CP Auction Version:', 'auctionAdmin'); ?></strong><br /><strong><?php _e('Database Version:', 'auctionAdmin'); ?></strong><br /><strong><?php _e('ClassiPress Version:', 'auctionAdmin'); ?></strong></th>
    <td><strong><?php echo CP_AUCTION_PLUGIN_VERSION; ?></strong> - <?php echo CP_AUCTION_PLUGIN_LATEST_RELEASE; ?><br /><strong><?php echo $cp_auction_db_version; ?></strong> - <?php echo $cp_auction_db_date; ?><br /><strong><?php echo CP_AUCTION_CLASSIPRESS_SUPPORTED; ?></td>
    </tr>

    </tbody>
</table>

<div class="dotted"><h3>SUPPORTED CHILD THEMES</h3></div><a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/flannel/?aid=20153" target="_blank">Flannel</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/eldorado/?aid=20153" target="_blank">Eldorado</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/grid/?aid=20153" target="_blank">Grid</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/phoenix/?aid=20153" target="_blank">Phoenix</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classiclean/?aid=20153" target="_blank">ClassiClean</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/citrus-night/?aid=20153" target="_blank">Citrus Night</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/multiport/?aid=20153" target="_blank">Multiport</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/ultra-classifieds/?aid=20153" target="_blank">Ultra Classifieds</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/twinpress-classifieds/?aid=20153" target="_blank">TwinPress</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/simply-responsive/?aid=20153" target="_blank">Simply Responsive</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/jibo/?aid=20153" target="_blank">Jibo</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/eclassify/?aid=20153" target="_blank">eClassify</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/rondell/?aid=20153" target="_blank">Rondell</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/flatpress/?aid=20153" target="_blank">FlatPress</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/flatron/?aid=20153" target="_blank">Flatron</a> | <a style="text-decoration: none" href="http://marketplace.appthemes.com/child-themes/classipress/skye/?aid=20153" target="_blank">Skye</a> | <a style="text-decoration: none" href="https://marketplace.appthemes.com/child-themes/classipress/classiestate/?aid=20153" target="_blank">ClassiEstate</a> |

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('GENERAL SETTINGS', 'auctionAdmin'); ?></h3></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Install Folder:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_inst_folder" value="<?php echo get_option('cp_auction_inst_folder'); ?>" size="15" /> <?php _e('Only if installed outside the root directory, ie. <font color="navy">http://www.your-domain.com/shop/</font>', 'auctionAdmin'); ?><br /><small><?php _e('Enter the folder/directory name ONLY if installed outside root directory, ie. <font color="navy">http://www.your-domain.com/shop/</font> then enter <strong>shop</strong> in the textbox above. <strong>Leave blank if installed in the root directory</strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Admins Only:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_admins_only"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_admins_only')); ?></select><br /><small><?php _e('Setup your website as your own marketplace, only admins are allowed to post ads.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Redirect Dasboard:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_redirect_dashboard"><?php echo cp_auction_yes_no(get_option('cp_auction_redirect_dashboard')); ?></select> <?php _e('Enable this option to redirect ClassiPress dashboard to the new dashboard.', 'auctionAdmin'); ?><br /><small><?php _e('This option redirect your old dashboard to the new and improved dashboard setup by CP Auction.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Redirect Post an Ad:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_redirect_post_new"><?php echo cp_auction_yes_no(get_option('cp_auction_redirect_post_new')); ?></select> <?php _e('Enable this option to redirect ClassiPress "add-new" page to the new "post an ad" page.', 'auctionAdmin'); ?><br /><small><?php _e('This option redirect your old post an ad page to the new post an ad page setup by CP Auction', 'auctionAdmin'); ?>, <strong>required option</strong>, <?php _e('unless you have replaced the post an ad button in header with a button pointing against the new post an ad page setup by using the <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=pages">page creator</a> option. <strong>OR if you want to use the standard Classified Ad Type ONLY, then leave this as disabled (No), and set Ad Listing Type below to Classified</strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Disable Redirect:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_post_access"><?php echo cp_auction_yes_no(get_option('cp_auction_post_access')); ?></select> <?php _e('Set this option to Yes if you want to disable the login/register redirect on the select ad type page.', 'auctionAdmin'); ?><br /><small><?php _e('Disable the login/register redirect if you are using the','auctionAdmin'); ?> <a style="text-decoration: none" href="http://marketplace.appthemes.com/plugins/anonymous-posting-for-classipress/?aid=20153" target="_blank"><strong>Anonymous Posting for ClassiPress</strong></a> <?php _e('plugin.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Ad Listing Type:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_auctiontype">
    <option value="classified" <?php if(get_option('cp_auction_plugin_auctiontype') == 'classified') echo 'selected="selected"'; ?>><?php _e('Classified', 'auctionAdmin'); ?></option>  
    <option value="wanted" <?php if(get_option('cp_auction_plugin_auctiontype') == 'wanted') echo 'selected="selected"'; ?>><?php _e('Wanted', 'auctionAdmin'); ?></option>
    <option value="normal" <?php if(get_option('cp_auction_plugin_auctiontype') == 'normal') echo 'selected="selected"'; ?>><?php _e('Normal', 'auctionAdmin'); ?></option>
    <option value="reverse" <?php if(get_option('cp_auction_plugin_auctiontype') == 'reverse') echo 'selected="selected"'; ?>><?php _e('Reverse', 'auctionAdmin'); ?></option>
    <option value="mixed" <?php if(get_option('cp_auction_plugin_auctiontype') == 'mixed') echo 'selected="selected"'; ?>><?php _e('Mixed', 'auctionAdmin'); ?></option>
    </select><br /><small><?php _e('Select the ad type that you want to use on your website. <strong>Normal</strong> means that users can bid normally with a increase of the last recorded bid. <strong>Reverse</strong> means that users only can register their bids with the decrease of the last recorded bid. <strong>Classified & Wanted</strong> will remove all bidding functions and make your website into a webshop / marketplace. If Classified is enabled you should also enable buy now options below. <strong>Mixed</strong> (recommended) give your users the option to choose between posting classified ads, wanted ads, normal auction and/or reverse auctions (<strong>depending on the ad types you choose below</strong>). Never change this setting after your site goes live as that can cause listings to show up as wrong ad type. <strong>Select Classified if you want to use the standard Classified Ad Type only, and leave the below Selectable Ad Types empty</strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Selectable Ad Types:', 'auctionAdmin'); ?></th>
    <td><input type="checkbox" name="classified" value="yes" <?php if(get_option('cp_auction_plugin_classified') == 'yes') echo 'checked="checked"'; ?>> <?php _e('Classified Ads.', 'auctionAdmin'); ?> &nbsp;&nbsp; <input type="checkbox" name="wanted" value="yes" <?php if(get_option('cp_auction_plugin_wanted') == 'yes') echo 'checked="checked"'; ?>> <?php _e('Wanted Ads.', 'auctionAdmin'); ?> &nbsp;&nbsp; <input type="checkbox" name="auction" value="yes" <?php if(get_option('cp_auction_plugin_normal') == 'yes') echo 'checked="checked"'; ?>> <?php _e('Normal Auctions.', 'auctionAdmin'); ?> &nbsp;&nbsp; <input type="checkbox" name="reverse" value="yes" <?php if(get_option('cp_auction_plugin_reverse') == 'yes') echo 'checked="checked"'; ?>> <?php _e('Reverse Auctions.', 'auctionAdmin'); ?><br /><small><?php _e('If Ad listing Type above is set to Mixed (recommended) you get the selection you make here selectable from a drop down menu in the new Step 1 Post an Ad page. <strong>Check out the information in the setup tab if you want to use only one type of ad, other than the standard classified ad type</strong>.', 'auctionAdmin'); ?></small><br /><br /><input type="checkbox" name="only_admins" value="yes" <?php if(get_option('cp_auction_only_admins_post_auctions') == 'yes') echo 'checked="checked"'; ?>> <?php _e('Auctions by admins only.', 'auctionAdmin'); ?><br /><small><?php _e('Tick to enable auctions posted by admins only. Do NOT tick the auctions ad types above.', 'auctionAdmin'); ?></small></td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Charge Ad Listing Fee:', 'auctionAdmin'); ?></th>
    <td><input type="checkbox" name="classified_fee" value="classified" <?php if(get_option('cp_auction_plugin_classified_fee') == 'classified') echo 'checked="checked"'; ?>> <?php _e('Classified Ads.', 'auctionAdmin'); ?> &nbsp;&nbsp; <input type="checkbox" name="wanted_fee" value="wanted" <?php if(get_option('cp_auction_plugin_wanted_fee') == 'wanted') echo 'checked="checked"'; ?>> <?php _e('Wanted Ads.', 'auctionAdmin'); ?> &nbsp;&nbsp; <input type="checkbox" name="auction_fee" value="normal" <?php if(get_option('cp_auction_plugin_normal_fee') == 'normal') echo 'checked="checked"'; ?>> <?php _e('Normal Auctions.', 'auctionAdmin'); ?> &nbsp;&nbsp; <input type="checkbox" name="reverse_fee" value="reverse" <?php if(get_option('cp_auction_plugin_reverse_fee') == 'reverse') echo 'checked="checked"'; ?>> <?php _e('Reverse Auctions.', 'auctionAdmin'); ?><br /><small><?php _e('If you in ClassiPress settings has enabled the <strong>Charge for Listing Ads</strong>, ALL ad types will be charged the same fee. You can here specify any ad type which you do not want to charge the ad listing fee. Users will then be able to post those type of ads for free.', 'auctionAdmin'); ?><br /><br /><strong><?php _e('Please note that this function will overwrite the file classipress/includes/payments.php, you should therefore not upgrade your ClassiPress theme before you have confirmed that CP Auction supports the update/new version of ClassiPress. See the top of this page, and do NOT enable this option if you use an older version.', 'auctionAdmin'); ?></strong></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Display in Dashboard:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_display_max_duration"><?php echo cp_auction_yes_no(get_option('cp_auction_display_max_duration')); ?></select> <?php _e('Display the Ad Listing Period in users dashboard.', 'auctionAdmin'); ?><br /><small><?php _e('If you use ad packages that offers a different ad listings periods then set this option to No.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Enable Buy Now:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_buy_now_ad_enabled"><?php echo cp_auction_yes_no(get_option('cp_auction_buy_now_ad_enabled')); ?></select> <?php _e('Add an option / checkobox to the post an ad form.', 'auctionAdmin'); ?><br /><small><?php _e('This option will activate a checkbox in the post an ad form so that the authors can easily decide whether they wants a buy now feature in the ad or not. The payment option is not available in the ad unless the user has entered his PayPal email address in their profile, or enabled another payment option in their frontend dashboard, settings.', 'auctionAdmin'); ?> <strong><?php _e('If you want to enable / disable the buy now feature on the different ad types then that can be done by going to', 'auctionAdmin'); ?> <a style="text-decoration: none" href="admin.php?page=cp-auction&tab=fields"><?php _e('custom field', 'auctionAdmin'); ?></a> <?php _e('settings.', 'auctionAdmin'); ?></strong></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Shopping Cart Expiration:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_hold_in_cart">
    <option value="" <?php if(get_option('cp_auction_hold_in_cart') == '') echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
    <option value="15 minutes" <?php if(get_option('cp_auction_hold_in_cart') == '15 minutes') echo 'selected="selected"'; ?>><?php _e('15 minutes', 'auctionAdmin'); ?></option>
    <option value="30 minutes" <?php if(get_option('cp_auction_hold_in_cart') == '30 minutes') echo 'selected="selected"'; ?>><?php _e('30 minutes', 'auctionAdmin'); ?></option>
    <option value="45 minutes" <?php if(get_option('cp_auction_hold_in_cart') == '45 minutes') echo 'selected="selected"'; ?>><?php _e('45 minutes', 'auctionAdmin'); ?></option>
    <option value="1 hours" <?php if(get_option('cp_auction_hold_in_cart') == '1 hours') echo 'selected="selected"'; ?>><?php _e('1 hour', 'auctionAdmin'); ?></option>
    <option value="2 hours" <?php if(get_option('cp_auction_hold_in_cart') == '2 hours') echo 'selected="selected"'; ?>><?php _e('2 hours', 'auctionAdmin'); ?></option>
    </select> <?php _e('Set the shopping cart expiration time.', 'auctionAdmin'); ?><br /><small><?php _e('Set the time for how long a product can be held in the cart before it is removed automatically. If no time is selected the default 48 hours will be used.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Hide Buy Now Button:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_hide_buy_now_button"><?php echo cp_auction_yes_no(get_option('cp_auction_hide_buy_now_button')); ?></select> <?php _e('Hide the buy now button for the ad author.', 'auctionAdmin'); ?><br /><small><?php _e('This option will hide the buy now button when ad author view their ads.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Buy Now in Search Results:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_search_shopping"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_search_shopping')); ?></select> <?php _e('Use the buy now / shopping cart feature on the site`s search results.', 'auctionAdmin'); ?><br><small><?php _e('Enable this option if you want to use the buy now / shopping cart option on the site`s search results. If you want to use the user to user direct payment so this must be activated via the payment settings.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Buy Now in the Loop:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_loop_shopping"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_loop_shopping')); ?></select> <?php _e('Use the buy now / shopping cart feature on the site`s ad listings, frontpage etc.', 'auctionAdmin'); ?><br><small><?php _e('Enable this option if you want to use the buy now / shopping cart option on the site`s ad listings, frontpage etc. If you want to use the user to user direct payment so this must be activated via the payment settings.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Enable Buy Now Hooks:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_use_buy_hooks"><?php echo cp_auction_yes_no(get_option('cp_auction_use_buy_hooks')); ?></select> <?php _e('Use the hooks provided.', 'auctionAdmin'); ?><br /><small><?php _e('Some child themes have placed the google maps just below the ad description, this may mean that the buy now button is placed at the very bottom of the ad. To make use of another placement put the below hooks where you want the the buy now button to appear in the single ad listing.', 'auctionAdmin'); ?></small>

	<div class="code_box">
	&lt;?php if ( function_exists('cp_auction_buy_now_button') ) cp_auction_buy_now_button(); ?&gt;
	</div>

    </td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Author set Ad Status:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_allow_pause_ad"><?php echo cp_auction_yes_no(get_option('cp_auction_allow_pause_ad')); ?></select> <?php _e('Author can through out the advertising process choose to pause their ad or make it private.', 'auctionAdmin'); ?><br /><small><?php _e('Author can through out the advertising process choose to pause their ad or make it private. Works with all ad types, classifieds, wanted and auction ads.', 'auctionAdmin'); ?> <strong><?php _e('Do not enable this feature if you set the ads to be approved by the admin.', 'auctionAdmin'); ?></strong></small></td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('Length of Widget Title:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_widget_title" value="<?php echo get_option('cp_auction_plugin_widget_title'); ?>" size="8" /> <?php _e('Recommended 25 characters, depending on your choice of theme.', 'auctionAdmin'); ?></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Disable Breadcrumb:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_breadcrumb"><?php echo cp_auction_yes_no(get_option('cp_auction_disable_breadcrumb')); ?></select> <?php _e('Disables the breadcrumb linkbar on all CP Auction pages.', 'auctionAdmin'); ?><br /><small><?php _e('If your theme does not support breadcrumbs then you should set this option to Yes.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Enable Negotiable Price:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_negotiable"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_negotiable')); ?></select> <?php _e('Display an image on the price tag.', 'auctionAdmin'); ?><br /><small><?php _e('This option displays an image on the price tag with the text Price is negotiable. You should also enable the <strong>Negotiable Price</strong> custom field. <strong>Note that this option is only tested with the main theme, ClassiPress</strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Report Ad:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_report_ad"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_report_ad')); ?></select> <?php _e('Allow users to report ads with inappropriate content.', 'auctionAdmin'); ?></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Sold Ad Banner:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_sold_banner"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_sold_banner')); ?></select> <?php _e('Display a sold ad/item banner under the ad title on single ad page when the ad/item is sold.', 'auctionAdmin'); ?></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Sold Ad Ribbon:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_sold_ribbon"><?php echo cp_auction_yes_no(get_option('cp_auction_sold_ribbon')); ?></select> <?php _e('Display a sold ad/item ribbon on the ad listing image when the ad/item is sold.', 'auctionAdmin'); ?></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Featured Ad Ribbon:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_featured_ribbon"><?php echo cp_auction_yes_no(get_option('cp_auction_featured_ribbon')); ?></select> <?php _e('Display a featured ad/item ribbon on the featured ad image.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Allow Comments:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_comments"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_comments')); ?></select> <?php _e('Allow users to post comments on classifieds, wanted and auction ads.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Enable Verification:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_verification" value="<?php echo get_option('cp_auction_plugin_verification'); ?>" size="8" /> <?php echo $cp_options->currency_code; ?> - <?php _e('To avoid unnecessary click on the buy-now button. Leave blank or set to 0 to disable verification.', 'auctionAdmin'); ?><br /><small><?php _e('If you want better control with the upload file option, you can specify an amount in that users must pay to your paypal account to verify their profile information. This will help ensure that fewer people click on the buy-now button, just for fun. Specify an amount in that users must pay to your paypal account.', 'auctionAdmin'); ?></small></td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('Enable Upload Verification:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_upl_verification"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_upl_verification')); ?></select> <?php _e('Verification must be enabled.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to enable verification on upload file function only. It will then NOT ask for verification on buy now purchases.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Information to buyer:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="cp_auction_plugin_verification_info" rows="5" cols="80"><?php echo get_option('cp_auction_plugin_verification_info'); ?></textarea><br><small><?php _e('Enter some information to the buyer about profile verification, this information will appear below the escrow button. HTML can be used.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Terms & Conditions:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="cp_auction_plugin_cp_agreement" rows="10" cols="80"><?php echo get_option('cp_auction_plugin_cp_agreement'); ?></textarea><br><small><?php _e('Enter your specific terms & conditions here. HTML can be used. <strong>Leave blank to use ClassiPress settings</strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('Deaktivate Terms Pop Up:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_deactivate_cp_agreement"><?php echo cp_auction_yes_no(get_option('cp_auction_deactivate_cp_agreement')); ?></select> <?php _e('Deaktivate the Terms & Conditions pop up.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to deaktivate the Terms & Conditions pop up.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Confirmation Pop Up:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_confirmation_popup" value="<?php echo get_option('cp_auction_confirmation_popup'); ?>" size="60" /><br /><small><?php _e('Add / edit the information text of the buy now confirmation pop up.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Deaktivate Pop Up:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_deactivate_confirmation_popup"><?php echo cp_auction_yes_no(get_option('cp_auction_deactivate_confirmation_popup')); ?></select> <?php _e('Deaktivate the buy now confirmation pop up.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to deaktivate the buy now confirmation pop up.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Deaktivate ALL Pop Ups:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_deactivate_confirmation_popups"><?php echo cp_auction_yes_no(get_option('cp_auction_deactivate_confirmation_popups')); ?></select> <?php _e('Deaktivate ALL confirmation pop ups.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to deaktivate ALL confirmation pop ups.', 'auctionAdmin'); ?></small></td>
    </tr>

    </tbody>
</table>

<p class="submit">
    <input type="submit" name="save_settings" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" />
    </p>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('NOTIFICATION EMAILS', 'auctionAdmin'); ?></h3></div>

<div class="admin_text"><?php _e('CP Auction has integrated a HTML notification mail system that supports <a style="text-decoration: none" href="http://wordpress.org/extend/plugins/wp-better-emails/">WP Better Emails</a>. To take full advantage of this, we recommend that you install <a style="text-decoration: none" href="http://wordpress.org/extend/plugins/wp-better-emails/">WP Better Emails</a> so that also your ClassiPress notification emails get a professional look.', 'auctionAdmin'); ?></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('HTML or TEXT Emails:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_plain_html" id="cp_auction_plugin_plain_html" style="min-width: 100px;">
	<option value="html" <?php if(get_option('cp_auction_plugin_plain_html') == "html") echo 'selected="selected"'; ?>><?php _e('HTML', 'auctionAdmin'); ?></option>
	<option value="plain" <?php if(get_option('cp_auction_plugin_plain_html') == "plain") echo 'selected="selected"'; ?>><?php _e('TEXT', 'auctionAdmin'); ?></option>
	</select> <?php _e('Enable the built in HTML email template.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Ad Image URL:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_banner_url" value="<?php echo get_option('cp_auction_plugin_banner_url'); ?>" size="50" /> <?php _e('468x60 banner image URL.', 'auctionAdmin'); ?><br /><small><?php _e('Paste the image url to a 468x60 banner image here, this banner ad will be included with all outgoing html emails.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Ad Destination URL:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_banner_link" value="<?php echo get_option('cp_auction_plugin_banner_link'); ?>" size="50" /> <?php _e('Banner ad destination URL.', 'auctionAdmin'); ?><br /><small><?php _e('Paste the destination URL of your banner ad here (i.e. http://www.yoursite.com/landing-page.html).', 'auctionAdmin'); ?></small></td>
    </tr>

    </tbody>
</table>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('PRODUCT FILES UPLOAD & DOWNLOAD', 'auctionAdmin'); ?></h3></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Important Information:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="cp_auction_plugin_important_info" rows="5" cols="80"><?php echo get_option('cp_auction_plugin_important_info'); ?></textarea><br /><small><?php _e('If admins only is enabled then you can enter some important information on users download page. HTML can be used.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Max Allowed File Size:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_max_size" id="cp_auction_plugin_max_size" style="min-width: 100px;">
        <option value="" <?php if(get_option('cp_auction_plugin_max_size') == '') echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
	<option value="100" <?php if(get_option('cp_auction_plugin_max_size') == '100') echo 'selected="selected"'; ?>>100KB</option>
	<option value="250" <?php if(get_option('cp_auction_plugin_max_size') == '250') echo 'selected="selected"'; ?>>250KB</option>
	<option value="500" <?php if(get_option('cp_auction_plugin_max_size') == '500') echo 'selected="selected"'; ?>>500KB</option>
	<option value="1024" <?php if(get_option('cp_auction_plugin_max_size') == '1024') echo 'selected="selected"'; ?>>1MB</option>
	<option value="2048" <?php if(get_option('cp_auction_plugin_max_size') == '2048') echo 'selected="selected"'; ?>>2MB</option>
	<option value="5120" <?php if(get_option('cp_auction_plugin_max_size') == '5120') echo 'selected="selected"'; ?>>5MB</option>
	<option value="0" <?php if(get_option('cp_auction_plugin_max_size') == '0') echo 'selected="selected"'; ?>>Unlimited</option>
	</select> <?php _e('Select file size to enable uploader.', 'auctionAdmin'); ?><br /><small><?php _e('Determine the maximum allowable size per file upload.', 'auctionAdmin'); ?></small></td>
    </tr>


	<tr valign="top">
	<th scope="row"><?php _e('Uploader availability:', 'auctionAdmin'); ?></th>
    <td><input type="checkbox" name="upload_classified" value="classified" <?php if( in_array('classified', $upl) ) echo 'checked="checked"'; ?>> <?php _e('Classified Ads.', 'auctionAdmin'); ?><br /><input type="checkbox" name="upload_normal" value="normal" <?php if( in_array('normal', $upl) ) echo 'checked="checked"'; ?>> <?php _e('Normal Auctions.', 'auctionAdmin'); ?><br /><input type="checkbox" name="upload_reverse" value="reverse" <?php if( in_array('reverse', $upl) ) echo 'checked="checked"'; ?>> <?php _e('Reverse Auctions.', 'auctionAdmin'); ?><br /><small><?php _e('Choose what types of ads will have the ability to upload files.', 'auctionAdmin'); ?> <strong><?php _e('NOTE: If you later uncheck any of the selected type of ads all associated uploads will be deleted from database.', 'auctionAdmin'); ?></strong></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('File Extensions:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_file_extensions" value="<?php echo get_option('cp_auction_file_extensions'); ?>" size="50" /> <?php _e('Comma separated list, eg. zip,png,doc,pdf.', 'auctionAdmin'); ?><br/><small><?php _e('Leave blank if you are not sure about this, and a standard list will be used.', 'auctionAdmin'); ?></small></td>
    </tr>

    </tbody>
</table>

<p class="submit">
    <input type="submit" name="save_settings" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" />
    </p>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('CLASSIFIED AD SETTINGS', 'auctionAdmin'); ?></h3></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Classifieds File Upload', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_mrp_force_upload"><?php echo cp_auction_yes_no(get_option('cp_auction_mrp_force_upload')); ?></select> <?php _e('Force author to upload their product files.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to force author to upload their product files after their classified ad has been posted. The ad will not go publish before all files are fully uploaded.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Approve Classified Ads:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_cla_approves"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_cla_approves')); ?></select> <?php _e('Manually approve Classified ads regardless of payment.', 'auctionAdmin'); ?><br><small><?php _e('If you want to approve transfers & payments go to the tab payment.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Classified Ribbon:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_classified_ribbon" value="<?php echo get_option('cp_auction_classified_ribbon'); ?>" size="16" /> <?php _e('Add a title to display an Classified ribbon on the left side of the ad title. Leave empty to disable.', 'auctionAdmin'); ?><br /><small><?php _e(' Make it easier for visitors to distinguish the different ad types.', 'auctionAdmin'); ?></small></td>
    </tr>

    </tbody>
</table>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('WANTED AD SETTINGS', 'auctionAdmin'); ?></h3></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Wanted Ads Information:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="cp_auction_plugin_wanted_info" rows="5" cols="80"><?php echo get_option('cp_auction_plugin_wanted_info'); ?></textarea><br><small><?php _e('Enter some Wanted Ads Offers information for the visitor here, this information will appear above the wanted ads offer option box.<br />HTML can be used.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Approve Wanted Ads:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_wnt_approves"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_wnt_approves')); ?></select> <?php _e('Manually approve Wanted ads regardless of payment.', 'auctionAdmin'); ?><br><small><?php _e('If you want to approve transfers & payments go to the tab payment.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Enable Wanted Widget:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_wanted_widget"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_wanted_widget')); ?></select> <?php _e('Display an place offer box in the sidebar.', 'auctionAdmin'); ?><br /><small><?php _e('Go to the widget settings and enable the CP Auction Wanted Option widget, <strong>OR</strong> leave the widget as is (disabled) to automatically apply the widget to the on top tabbed sidebar widget. <strong>For other settings that affect on the single ad page functionality visit the <a style="text-decoration: none;" href="/wp-admin/admin.php?page=cp-auction&tab=misc">Misc settings</a></strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Enable Wanted in Frontpage:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_wanted_loop"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_wanted_loop')); ?></select> <?php _e('Display an place offer box in the frontpage ad loop (not recommended).', 'auctionAdmin'); ?><br /><small><?php _e('Make sure the sidebar widget option above is disabled before you enable this option. When enabled you will get a bidder option box into each wanted ad in the frontpage and single ad page.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Wanted Ribbon:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_wanted_ribbon" value="<?php echo get_option('cp_auction_wanted_ribbon'); ?>" size="16" /> <?php _e('Add a title to display an Wanted ribbon on the left side of the ad title. Leave empty to disable.', 'auctionAdmin'); ?><br /><small><?php _e(' Make it easier for visitors to distinguish the different ad types.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Wanted on Classified Ads:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_classified_wanted"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_classified_wanted')); ?></select> <?php _e('Use the Wanted ads option on the site`s classified ads.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option if you want to allow users to convert their classified ad to wanted ad.', 'auctionAdmin'); ?> <strong><?php _e('Author can choose to ignore this, if enabled by admin.', 'auctionAdmin'); ?></strong></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Enable Wanted Hooks:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_use_wanted_hooks"><?php echo cp_auction_yes_no(get_option('cp_auction_use_wanted_hooks')); ?></select> <?php _e('Use the hooks provided.', 'auctionAdmin'); ?><br /><small><?php _e('Some child themes have placed the google maps just below the ad description, this may mean that the wanted place offer box is placed at the very bottom of the ad. To make use of another placement put the below hooks where you want the the place offer option to appear in the single ad listing.', 'auctionAdmin'); ?></small>

	<div class="code_box">
	&lt;?php if ( function_exists('cp_auction_wanted_box') ) cp_auction_wanted_box(); ?&gt;
	</div>

    </td>
    </tr>

    </tbody>
</table>

<p class="submit">
    <input type="submit" name="save_settings" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" />
    </p>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('AUCTION SETTINGS', 'auctionAdmin'); ?></h3></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Start Timer on First Bid:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_start_timer"><?php echo cp_auction_yes_no(get_option('cp_auction_start_timer')); ?></select> <?php _e('Enable, and countdown timer starts on the first bid.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to start the auction countdown timer on the first bid and not immediately after auction get online, as is standard. This will set a new expiration date in accordance to the ad listing period and time of first bid posted.', 'auctionAdmin'); ?></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Price to Show in Price Tag:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_price_type">
    <option value=""><?php _e('Select..', 'auctionAdmin'); ?></option>
    <option value="current" <?php if(get_option('cp_auction_plugin_price_type') == "current") echo 'selected="selected"'; ?>><?php _e('Latest Bid', 'auctionAdmin'); ?></option>
    <option value="highest" <?php if(get_option('cp_auction_plugin_price_type') == "highest") echo 'selected="selected"'; ?>><?php _e('Highest Bid', 'auctionAdmin'); ?></option>
    <option value="start" <?php if(get_option('cp_auction_plugin_price_type') == "start") echo 'selected="selected"'; ?>><?php _e('Start Price', 'auctionAdmin'); ?></option>
    </select> <?php _e('Set the price type you want to display in the price tag for auction listings.', 'auctionAdmin'); ?><br /><small><?php _e('Classifieds & Wanted ads will use the standard price regardless of this setting.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Auction on Classified Ads:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_classified_auction"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_classified_auction')); ?></select> <?php _e('Run a simple auction model on the site`s classified ads.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option if you want to run a simple auction model on the site`s classified ads, and include a simple version of the below shopping cart option.', 'auctionAdmin'); ?> <strong><?php _e('Author can choose to ignore this, if enabled by admin.', 'auctionAdmin'); ?></strong></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Classified Auctions Listing Period:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_classified_expiration" value="<?php echo get_option('cp_auction_classified_expiration'); ?>" size="8" /> <?php _e('Days', 'auctionAdmin'); ?> - <?php _e('Enter the number of days for the (classified ads) auction to end, eg. 10', 'auctionAdmin'); ?><br /><small><?php _e('This setting is ONLY for the classified ads auction. If enabled in Auction on Classified Ads above.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Auction Ad Listing Period:', 'auctionAdmin'); ?></th>
    <td><input type="radio" name="period[]" id="minutes" value="minutes" <?php if( in_array("minutes", $period) ) echo 'checked="checked"'; ?>> <label for="minutes"><?php _e('Minutes.', 'auctionAdmin'); ?></label><br />
        <input type="radio" name="period[]" id="hours" value="hours" <?php if( in_array("hours", $period) ) echo 'checked="checked"'; ?>> <label for="hours"><?php _e('Hours.', 'auctionAdmin'); ?></label><br />
        <input type="radio" name="period[]" id="days" value="days" <?php if( in_array("days", $period) ) echo 'checked="checked"'; ?>> <label for="days"><?php _e('Days.', 'auctionAdmin'); ?></label><br />
	<small><?php _e('Auctions end date (expiration) can be set by minutes, hours or days, standard is days. If you want to change the ad listing period for auctions to reflect your settings here that can be done via editing the values of the custom field Ad Listing Period. The default period is set to 1,3,5,7,10 days and are selectable for author via Ad Listing Period dropdown box during the post an ad process. If you rather want to use minutes or hours then just change your settings above.<br/><br/>To make the ad expiration more accurately you should setup a cronjob in your server environment, you might have to contact your hosting provider for accurate how to do this for your site. <strong>Cronjob URL:</strong> <font color="navy">'.get_bloginfo('wpurl').'/?_process_cronjob=1</font>', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Approve Auction Ads:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_approves"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_approves')); ?></select> <?php _e('Manually approve auction ads regardless of payment.', 'auctionAdmin'); ?><br><small><?php _e('If you want to approve transfers & payments go to the tab payment.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Auction Ribbon:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_auction_ribbon" value="<?php echo get_option('cp_auction_auction_ribbon'); ?>" size="16" /> <?php _e('Add a title to display an Auction ribbon on the left side of the ad title. Leave empty to disable.', 'auctionAdmin'); ?><br /><small><?php _e(' Make it easier for visitors to distinguish the different ad types.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Reverse Auction Ribbon:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_reverse_ribbon" value="<?php echo get_option('cp_auction_reverse_ribbon'); ?>" size="16" /> <?php _e('Add a title to display an Reverse Auction ribbon on the left side of the ad title. Leave empty to disable.', 'auctionAdmin'); ?><br /><small><?php _e(' Make it easier for visitors to distinguish the different ad types.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Allow bidding Anonymous:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_bid_anonymous"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_bid_anonymous')); ?></select> <?php _e('Auctions & Wanted Ads', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to allow users to register their bids anonymous. Author can disable this option from their userpanel.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Owner delete fraud bids:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_owner_delete"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_owner_delete')); ?></select> <?php _e('Auctions & Wanted Ads', 'auctionAdmin'); ?><br /><small><?php _e('Allow auctions & wanted ad owners to delete fraudelent bids.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Bidder delete faulty bids:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_bidder_delete"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_bidder_delete')); ?></select> <?php _e('Auctions & Wanted Ads', 'auctionAdmin'); ?><br /><small><?php _e('Allow the bidder to delete their own bids.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Soon To End Auctions Widget:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_lastchance" id="cp_auction_plugin_lastchance" style="min-width: 100px;">
	<option value="1 hour" <?php if(get_option('cp_auction_plugin_lastchance') == '1 hour') echo 'selected="selected"'; ?>>1 <?php _e('Hour', 'auctionAdmin'); ?></option>
	<option value="2 hours" <?php if(get_option('cp_auction_plugin_lastchance') == '2 hours') echo 'selected="selected"'; ?>>2 <?php _e('Hours', 'auctionAdmin'); ?></option>
	<option value="3 hours" <?php if(get_option('cp_auction_plugin_lastchance') == '3 hours') echo 'selected="selected"'; ?>>3 <?php _e('Hours', 'auctionAdmin'); ?></option>
	<option value="4 hours" <?php if(get_option('cp_auction_plugin_lastchance') == '4 hours') echo 'selected="selected"'; ?>>4 <?php _e('Hours', 'auctionAdmin'); ?></option>
	<option value="5 hours" <?php if(get_option('cp_auction_plugin_lastchance') == '5 hours') echo 'selected="selected"'; ?>>5 <?php _e('Hours', 'auctionAdmin'); ?></option>
	<option value="6 hours" <?php if(get_option('cp_auction_plugin_lastchance') == '6 hours') echo 'selected="selected"'; ?>>6 <?php _e('Hours', 'auctionAdmin'); ?></option>
	</select><br /><small><?php _e('Set how many hours it should be left of an online auction listing before it is displayed in the Soon To End Auctions widget.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Add to Favorites:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_if_favorites"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_if_favorites')); ?></select><br /><small><?php _e('Allow your customers to add authors of interest to their favorite list.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Add to Watchlist:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_if_watchlist"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_if_watchlist')); ?></select> <?php _e('Auctions & Wanted Ads', 'auctionAdmin'); ?><br /><small><?php _e('Allow your customers to add auctions of interest to their watchlist list.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Enable Freebids:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_freebids"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_freebids')); ?></select> <?php _e('Allow bids lower than set start price.', 'auctionAdmin'); ?></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Allow Lower Freebids:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_last_freebids"><?php echo cp_auction_yes_no(get_option('cp_auction_last_freebids')); ?></select> <?php _e('Allow bids lower than the latest bid.', 'auctionAdmin'); ?><br /><small><?php _e('Only effective if the freebids option above are enabled.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Enable Auction Hooks:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_use_hooks"><?php echo cp_auction_yes_no(get_option('cp_auction_use_hooks')); ?></select> <?php _e('Use the hooks provided.', 'auctionAdmin'); ?><br /><small><?php _e('Some child themes have placed the google maps just below the ad description, this may mean that the auction bid box is placed at the very bottom of the ad. To make use of another placement put the below hooks where you want the the auction bid box to appear in the single ad listing.', 'auctionAdmin'); ?></small>

	<div class="code_box">
	&lt;?php if ( function_exists('cp_auction_bidder_box') ) cp_auction_bidder_box(); ?&gt;
	</div>

    </td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Enable Bid Widget:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_bid_widget"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_bid_widget')); ?></select> <?php _e('Display an bid option box in the sidebar.', 'auctionAdmin'); ?><br /><small><?php _e('Go to the widget settings and enable the CP Auction Bid Option widget, <strong>OR</strong> leave the widget as is (disabled) to automatically apply the widget to the on top tabbed sidebar widget. <strong>For other settings that affect on the single ad page functionality visit the <a style="text-decoration: none;" href="/wp-admin/admin.php?page=cp-auction&tab=misc">Misc settings</a></strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Buy Now Button in Widget:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_auction_buy_widget"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_auction_buy_widget')); ?></select> <?php _e('Include the buy now button in the auction bid option widget.', 'auctionAdmin'); ?><br /><small><?php _e('The payment option is not available in the ad unless the user has entered his PayPal email address in their profile, or enabled another payment option in their frontend dashboard, settings.', 'auctionAdmin'); ?></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Remove the Map:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_remove_map_tab"><?php echo cp_auction_yes_no(get_option('cp_auction_remove_map_tab')); ?></select> <?php _e('Enable to remove the map tab.', 'auctionAdmin'); ?><br /><small><?php _e('Removes the Map tab that appear in some childthemes when bid widget is enabled.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Auction File Upload', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_force_upload"><?php echo cp_auction_yes_no(get_option('cp_auction_force_upload')); ?></select> <?php _e('Force author to upload their product files.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to force author to upload their product files after their auction ad has been posted. Auction will not go publish before all files are fully uploaded.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
	<th scope="row"><?php _e('Disable Countdown Timer:', 'auctionAdmin'); ?></th>

	<td><input type="checkbox" name="cp_auction_pages_timer" value="pages" <?php if( in_array("pages", $list)) echo 'checked="checked"'; ?>> <?php _e('Pages, frontpage, overview page and authors listings.', 'auctionAdmin'); ?><br />
	<input type="checkbox" name="cp_auction_single_timer" value="single" <?php if( in_array("single", $list)) echo 'checked="checked"'; ?>> <?php _e('Single Ads Page, bidder systems.', 'auctionAdmin'); ?><br />
	<input type="checkbox" name="cp_auction_widgets_timer" value="widgets" <?php if( in_array("widgets", $list)) echo 'checked="checked"'; ?>> <?php _e('Sidebar Widgets, featured ads, latest ads and soon to close auctions.', 'auctionAdmin'); ?><br />
	<input type="checkbox" name="cp_auction_bids_timer" value="bids" <?php if( in_array("bids", $list)) echo 'checked="checked"'; ?>> <?php _e('Sidebar Widgets, bidder systems.', 'auctionAdmin'); ?><br /><small><?php _e('Check the checkbox(es) above for the area where you don`t want to display the timer.', 'auctionAdmin'); ?></small>

	</td>
    </tr>

    </tbody>
</table>

<p class="submit">
    <input type="submit" name="save_settings" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" />
    </p>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('ADDITIONAL AD TYPE SETTINGS', 'auctionAdmin'); ?></h3></div>

<div class="admin_text"><?php _e('Some additional options to the above ad type settings.', 'auctionAdmin'); ?></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Users Ribbon:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_users_ribbon"><?php echo cp_auction_yes_no(get_option('cp_auction_users_ribbon')); ?></select> <?php _e('Overwrite the above given ribbons with users own ribbons titles.', 'auctionAdmin'); ?><br /><small><?php _e('As the author can set the intention of their ads to <strong>I Want To</strong>, eg. Sell, Buy, Swap or Give Away you can with this option make their selection to overwrite the above given ribbons titles to display, eg. For Sale, Want to Buy, Want to Swap or To Give Away. See also the possibility to sort the ads in the custom frontpage according to the users intention for their ad,', 'auctionAdmin'); ?> <a style="text-decoration: none" href="get_bloginfo('wpurl').'/wp-admin/admin.php?page=cp-auction&tab=frontpage"><?php _e('click here', 'auctionAdmin'); ?></a>.</small>

	</td>
    </tr>

    </tbody>
</table>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('REGISTRATION FIELD SETTINGS', 'auctionAdmin'); ?></h3></div>

<div class="admin_text"><?php _e('Some additional fields for the registration process. At the moment the below option/fields has no practical means for your website, but we work on possibilities to separate private and business users as free and paid accounts, which should be based on ClassiPress membership packages.', 'auctionAdmin'); ?></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Private or Business:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_account_type"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_account_type')); ?></select> <?php _e('Enable field for selecting the account type.', 'auctionAdmin'); ?><br /><small><?php _e('New users must during the registration process choose their preferred account type, Private or Business.', 'auctionAdmin'); ?></a></small>

	</td>
    </tr>

    </tbody>
</table>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('PROFILE FIELDS SETTINGS', 'auctionAdmin'); ?></h3></div>

<div class="admin_text"><?php _e('A deactivation of the below profile fields will only hide them from the user`s profile, they will still be operational in the shopping cart`s billing address and invoice system. <strong>Set the option to Required if you want the actual field as required for users to post an ad</strong>.', 'auctionAdmin'); ?></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Company Name Field:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_company">
        <option value="no" <?php if(get_option('cp_auction_disable_company') == "no") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionAdmin'); ?></option>
        <option value="required" <?php if(get_option('cp_auction_disable_company') == "required") echo 'selected="selected"'; ?>><?php _e('Required', 'auctionAdmin'); ?></option>
        <option value="yes" <?php if(get_option('cp_auction_disable_company') == "yes") echo 'selected="selected"'; ?>><?php _e('Hide / Disable', 'auctionAdmin'); ?></option>
        </select> <?php _e('Impact on the usermeta <strong>company_name</strong>', 'auctionAdmin'); ?><br /><small><?php _e('Hide from users profile, or set as Required for users to post an ad.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Address Field:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_address">
        <option value="no" <?php if(get_option('cp_auction_disable_address') == "no") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionAdmin'); ?></option>
        <option value="required" <?php if(get_option('cp_auction_disable_address') == "required") echo 'selected="selected"'; ?>><?php _e('Required', 'auctionAdmin'); ?></option>
        <option value="yes" <?php if(get_option('cp_auction_disable_address') == "yes") echo 'selected="selected"'; ?>><?php _e('Hide / Disable', 'auctionAdmin'); ?></option>
        </select> <?php _e('Impact on the usermeta <strong>cp_street</strong>', 'auctionAdmin'); ?><br /><small><?php _e('Hide from users profile, or set as Required for users to post an ad.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('ZIPcode Field:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_zipcode">
        <option value="no" <?php if(get_option('cp_auction_disable_zipcode') == "no") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionAdmin'); ?></option>
        <option value="required" <?php if(get_option('cp_auction_disable_zipcode') == "required") echo 'selected="selected"'; ?>><?php _e('Required', 'auctionAdmin'); ?></option>
        <option value="yes" <?php if(get_option('cp_auction_disable_zipcode') == "yes") echo 'selected="selected"'; ?>><?php _e('Hide / Disable', 'auctionAdmin'); ?></option>
        </select> <?php _e('Impact on the usermeta <strong>cp_zipcode</strong>', 'auctionAdmin'); ?><br /><small><?php _e('Hide from users profile, or set as Required for users to post an ad.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('City Field:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_city">
        <option value="no" <?php if(get_option('cp_auction_disable_city') == "no") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionAdmin'); ?></option>
        <option value="required" <?php if(get_option('cp_auction_disable_city') == "required") echo 'selected="selected"'; ?>><?php _e('Required', 'auctionAdmin'); ?></option>
        <option value="yes" <?php if(get_option('cp_auction_disable_city') == "yes") echo 'selected="selected"'; ?>><?php _e('Hide / Disable', 'auctionAdmin'); ?></option>
        </select> <?php _e('Impact on the usermeta <strong>cp_city</strong>', 'auctionAdmin'); ?><br /><small><?php _e('Hide from users profile, or set as Required for users to post an ad.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('State Field:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_state">
        <option value="no" <?php if(get_option('cp_auction_disable_state') == "no") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionAdmin'); ?></option>
        <option value="required" <?php if(get_option('cp_auction_disable_state') == "required") echo 'selected="selected"'; ?>><?php _e('Required', 'auctionAdmin'); ?></option>
        <option value="yes" <?php if(get_option('cp_auction_disable_state') == "yes") echo 'selected="selected"'; ?>><?php _e('Hide / Disable', 'auctionAdmin'); ?></option>
        </select> <?php _e('Impact on the usermeta <strong>cp_state</strong>', 'auctionAdmin'); ?><br /><small><?php _e('Hide from users profile, or set as Required for users to post an ad.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Country Field:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_country">
        <option value="no" <?php if(get_option('cp_auction_disable_country') == "no") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionAdmin'); ?></option>
        <option value="required" <?php if(get_option('cp_auction_disable_country') == "required") echo 'selected="selected"'; ?>><?php _e('Required', 'auctionAdmin'); ?></option>
        <option value="yes" <?php if(get_option('cp_auction_disable_country') == "yes") echo 'selected="selected"'; ?>><?php _e('Hide / Disable', 'auctionAdmin'); ?></option>
        </select> <?php _e('Impact on the usermeta <strong>cp_country</strong>', 'auctionAdmin'); ?><br /><small><?php _e('Hide from users profile, or set as Required for users to post an ad.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Phone Field:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_phone">
        <option value="no" <?php if(get_option('cp_auction_disable_phone') == "no") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionAdmin'); ?></option>
        <option value="required" <?php if(get_option('cp_auction_disable_phone') == "required") echo 'selected="selected"'; ?>><?php _e('Required', 'auctionAdmin'); ?></option>
        <option value="yes" <?php if(get_option('cp_auction_disable_phone') == "yes") echo 'selected="selected"'; ?>><?php _e('Hide / Disable', 'auctionAdmin'); ?></option>
        </select> <?php _e('Impact on the usermeta <strong>cp_phone</strong>', 'auctionAdmin'); ?><br /><small><?php _e('Hide from users profile, or set as Required for users to post an ad.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('PayPal Email Field:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_paypal">
        <option value="no" <?php if(get_option('cp_auction_disable_paypal') == "no") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionAdmin'); ?></option>
        <option value="required" <?php if(get_option('cp_auction_disable_paypal') == "required") echo 'selected="selected"'; ?>><?php _e('Required', 'auctionAdmin'); ?></option>
        <option value="yes" <?php if(get_option('cp_auction_disable_paypal') == "yes") echo 'selected="selected"'; ?>><?php _e('Hide / Disable', 'auctionAdmin'); ?></option>
        </select> <?php _e('Impact on the usermeta <strong>paypal_email</strong>', 'auctionAdmin'); ?><br /><small><?php _e('Hide from users profile, or set as Required for users to post an ad.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Upload Users Avatar:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_if_avatar"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_if_avatar')); ?></select><br /><small><?php _e('Set this option to Yes if you want to allow users to upload avatars through their frontend profile page. This will also add the avatar upload function to WP`s main profile page in wp backend / admin. You can set user capabilities in WP Settings --> Discussions.', 'auctionAdmin'); ?></small></td>
    </tr>

    </tbody>
</table>

    <p class="submit">
    <input type="submit" name="save_settings" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" />
    </p>
    </form>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}