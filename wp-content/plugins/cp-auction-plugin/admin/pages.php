<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_plugin_page_settings() {

	global $cpurl;

	$msg = "";
	$ok = "";

	if(isset($_POST['save_settings'])) {

		$download_log = isset( $_POST['download_log'] ) ? esc_attr( $_POST['download_log'] ) : '';
		if( empty( $download_log ) ) {
		$download_log2 = isset( $_POST['download_log2'] ) ? esc_attr( $_POST['download_log2'] ) : '';
		$download_log = get_the_title($download_log2);
		}

		$my_coupons = isset( $_POST['my_coupons'] ) ? esc_attr( $_POST['my_coupons'] ) : '';
		if( empty( $my_coupons ) ) {
		$my_coupons2 = isset( $_POST['my_coupons2'] ) ? esc_attr( $_POST['my_coupons2'] ) : '';
		$my_coupons = get_the_title($my_coupons2);
		}

		$my_downloads = isset( $_POST['my_downloads'] ) ? esc_attr( $_POST['my_downloads'] ) : '';
		if( empty( $my_downloads ) ) {
		$my_downloads2 = isset( $_POST['my_downloads2'] ) ? esc_attr( $_POST['my_downloads2'] ) : '';
		$my_downloads = get_the_title($my_downloads2);
		}

		$file_upload = isset( $_POST['file_upload'] ) ? esc_attr( $_POST['file_upload'] ) : '';
		if( empty( $file_upload ) ) {
		$file_upload2 = isset( $_POST['file_upload2'] ) ? esc_attr( $_POST['file_upload2'] ) : '';
		$file_upload = get_the_title($file_upload2);
		}

		$post_new = isset( $_POST['post_new'] ) ? esc_attr( $_POST['post_new'] ) : '';
		if( empty( $post_new ) ) {
		$post_new2 = isset( $_POST['post_new2'] ) ? esc_attr( $_POST['post_new2'] ) : '';
		$post_new = get_the_title($post_new2);
		}

		$choose_winner = isset( $_POST['choose_winner'] ) ? esc_attr( $_POST['choose_winner'] ) : '';
		if( empty( $choose_winner ) ) {
		$choose_winner2 = isset( $_POST['choose_winner2'] ) ? esc_attr( $_POST['choose_winner2'] ) : '';
		$choose_winner = get_the_title($choose_winner2);
		}

		$authors_listings = isset( $_POST['authors_listings'] ) ? esc_attr( $_POST['authors_listings'] ) : '';
		if( empty( $authors_listings ) ) {
		$authors_listings2 = isset( $_POST['authors_listings2'] ) ? esc_attr( $_POST['authors_listings2'] ) : '';
		$authors_listings = get_the_title($authors_listings2);
		}

		$dashboard = isset( $_POST['dashboard'] ) ? esc_attr( $_POST['dashboard'] ) : '';
		if( empty( $dashboard ) ) {
		$dashboard2 = isset( $_POST['dashboard2'] ) ? esc_attr( $_POST['dashboard2'] ) : '';
		$dashboard = get_the_title($dashboard2);
		}

		$my_ads = isset( $_POST['my_ads'] ) ? esc_attr( $_POST['my_ads'] ) : '';
		if( empty( $my_ads ) ) {
		$my_ads2 = isset( $_POST['my_ads2'] ) ? esc_attr( $_POST['my_ads2'] ) : '';
		$my_ads = get_the_title($my_ads2);
		}

		$my_favorites = isset( $_POST['my_favorites'] ) ? esc_attr( $_POST['my_favorites'] ) : '';
		if( empty( $my_favorites ) ) {
		$my_favorites2 = isset( $_POST['my_favorites2'] ) ? esc_attr( $_POST['my_favorites2'] ) : '';
		$my_favorites = get_the_title($my_favorites2);
		}

		$my_watchlist = isset( $_POST['my_watchlist'] ) ? esc_attr( $_POST['my_watchlist'] ) : '';
		if( empty( $my_watchlist ) ) {
		$my_watchlist2 = isset( $_POST['my_watchlist2'] ) ? esc_attr( $_POST['my_watchlist2'] ) : '';
		$my_watchlist = get_the_title($my_watchlist2);
		}

		$contact_user = isset( $_POST['contact_user'] ) ? esc_attr( $_POST['contact_user'] ) : '';
		if( empty( $contact_user ) ) {
		$contact_user2 = isset( $_POST['contact_user2'] ) ? esc_attr( $_POST['contact_user2'] ) : '';
		$contact_user = get_the_title($contact_user2);
		}

		$shopping_cart = isset( $_POST['shopping_cart'] ) ? esc_attr( $_POST['shopping_cart'] ) : '';
		if( empty( $shopping_cart ) ) {
		$shopping_cart2 = isset( $_POST['shopping_cart2'] ) ? esc_attr( $_POST['shopping_cart2'] ) : '';
		$shopping_cart = get_the_title($shopping_cart2);
		}

		$overview = isset( $_POST['overview'] ) ? esc_attr( $_POST['overview'] ) : '';
		if( empty( $overview ) ) {
		$overview2 = isset( $_POST['overview2'] ) ? esc_attr( $_POST['overview2'] ) : '';
		$overview = get_the_title($overview2);
		}

		$affiliate = isset( $_POST['affiliate'] ) ? esc_attr( $_POST['affiliate'] ) : '';
		if( empty( $affiliate ) ) {
		$affiliate2 = isset( $_POST['affiliate2'] ) ? esc_attr( $_POST['affiliate2'] ) : '';
		$affiliate = get_the_title($affiliate2);
		}

		$option = get_option( 'cp_auction_page_titles' );
		if( empty( $option ) )
		$option = array();

		$option['title_log'] = $download_log;
		$option['title_coupons'] = $my_coupons;
		$option['title_downloads'] = $my_downloads;
		$option['title_upload'] = $file_upload;
		$option['title_new'] = $post_new;
		$option['title_winner'] = $choose_winner;
		$option['title_authors'] = $authors_listings;
		$option['title_userpanel'] = $dashboard;
		$option['title_myads'] = $my_ads;
		$option['title_favorites'] = $my_favorites;
		$option['title_watchlist'] = $my_watchlist;
		$option['title_contact'] = $contact_user;
		$option['title_cart'] = $shopping_cart;
		$option['title_overview'] = $overview;
		$option['title_affiliate'] = $affiliate;

		if ( ! empty( $option ) )
   		update_option( 'cp_auction_page_titles', $option );

		$option = get_option( 'cp_auction_page_titles' );

		foreach ( $option as $key => $value ) {
		$explode = explode('_', $key);
		$the_slug = end($explode);
		$the_page_title = $value;
		$the_page = get_page_by_title( $the_page_title );

		if ( ! $the_page && $the_page_title ) {

		// Create post object
		$_p = array();
		$_p['post_title'] = $the_page_title;
		$_p['post_status'] = 'publish';
		$_p['post_type'] = 'page';
		$_p['comment_status'] = 'closed';
		$_p['ping_status'] = 'closed';

		// Insert the post into the database
		$the_page_id = wp_insert_post( $_p );
		$the_page_slug = cp_get_the_slug( $the_page_id );

		$options = get_option( 'cp_auction_page_slugs' );
		$options['enable'] = "yes";
		$options[$the_slug] = $the_page_slug;
		if ( ! empty( $options ) )
   		update_option( 'cp_auction_page_slugs', $options );

		$pids = get_option( 'cp_auction_page_ids' );
		$pids[$the_page_title] = $the_page_id;
		if ( ! empty( $pids ) )
   		update_option( 'cp_auction_page_ids', $pids );

		}
		else {

		// the plugin may have been previously active and the page may just be trashed...
		if( isset( $the_page->ID ) ) {
		$the_page_id = $the_page->ID;
		$the_page_slug = cp_get_the_slug( $the_page_id );
		
		$options = get_option( 'cp_auction_page_slugs' );
		$options['enable'] = "yes";
		$options[$the_slug] = $the_page_slug;
		if ( ! empty( $options ) )
   		update_option( 'cp_auction_page_slugs', $options );

		$pids = get_option( 'cp_auction_page_ids' );
		$pids[$the_page_title] = $the_page_id;
		if ( ! empty( $pids ) )
   		update_option( 'cp_auction_page_ids', $pids );

		//make sure the page is not trashed...
		$the_page->post_status = 'publish';
		$the_page_id = wp_update_post( $the_page );
		}

		}
		}

		global $wp_rewrite;	
		$wp_rewrite->flush_rules();

		$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Pages was created!','auctionAdmin').'</strong></p></div>';
		}

		$option = get_option( 'cp_auction_page_titles' );
		$options = get_option( 'cp_auction_page_slugs' );

?>

<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Create Pages', 'auctionAdmin'); ?></h3>
            <div class="inside">

	<div class="clear10"></div>

	<?php _e('Create the pages that are necessary for the CP Auction plugin to work satisfactorily. Enter a title and save. All pages will be created automatically and can subsequently be found under the menu Pages to the left.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<?php _e('You should setup an custom menu in menu settings which you find by going to Apperance -> Menus. Create a new menu and name it eg. Dashboard, then go to Manage Locations and attach the new menu you just created to the Theme Dashboard and save. You now have a new customized User Options menu ready to set up with your user options links.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<?php _e('Only pages marked with <font color="green"><strong>*</strong></font> should be added to the user options menu, this depends of course on what services you want to make available to your users.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<?php _e('<strong>Enter the page titles below, and save. OR, if you have already created the page then you can select it from the drop down menu</strong>.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

<form method="post" action="">

    <table class="form-table">
    <tbody>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('Download Log:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="download_log" value="<?php echo $option['title_log']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['log']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>] <small><?php _e('<strong>The settings must be saved first</strong>.', 'auctionAdmin'); ?><br />
    <?php $args = array( 'name' => 'download_log2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br />
    <?php if( isset( $options['log'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['log'].'/'; ?></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Listings Overview:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="overview" value="<?php echo $option['title_overview']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['overview']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'overview2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['overview'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['overview'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Contact User:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="contact_user" value="<?php echo $option['title_contact']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['contact']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'contact_user2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['contact'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['contact'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('Shopping Cart:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="shopping_cart" value="<?php echo $option['title_cart']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['cart']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'shopping_cart2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['cart'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['cart'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('My Coupons:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="my_coupons" value="<?php echo $option['title_coupons']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['coupons']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'my_coupons2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['coupons'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['coupons'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('My Downloads:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="my_downloads" value="<?php echo $option['title_downloads']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['downloads']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'my_downloads2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['downloads'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['downloads'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('File Upload:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="file_upload" value="<?php echo $option['title_upload']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['upload']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'file_upload2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['upload'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['upload'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Choose Ad Type:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="post_new" value="<?php echo $option['title_new']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['new']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'post_new2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><?php if( isset( $options['new'] ) ) echo '<small>'.get_bloginfo('wpurl').'/'.$options['new'].'/</small><br />'; ?><small><?php _e('This is the NEW first step post an ad page where authors can choose which ad types to post.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Choose a Winner:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="choose_winner" value="<?php echo $option['title_winner']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['winner']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'choose_winner2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['winner'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['winner'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('Authors Listings:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="authors_listings" value="<?php echo $option['title_authors']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['authors']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'authors_listings2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['authors'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['authors'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('Userpanel:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="dashboard" value="<?php echo $option['title_userpanel']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['userpanel']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'dashboard2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><?php if( isset( $options['userpanel'] ) ) echo '<small>'.get_bloginfo('wpurl').'/'.$options['userpanel'].'/</small><br />'; ?><small><?php _e('Paste the shortcode [cartpauj-pm] into the page content (under the HTML tab of the page editor), if Cartpauj PM is installed.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('My Ads:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="my_ads" value="<?php echo $option['title_myads']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['myads']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'my_ads2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['myads'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['myads'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('My Favorites:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="my_favorites" value="<?php echo $option['title_favorites']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $options['favorites']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'my_favorites2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['favorites'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['favorites'].'/'; ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('My Watchlist:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="my_watchlist" value="<?php echo $option['title_watchlist']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $cpurl['watchlist']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'my_watchlist2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['watchlist'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['watchlist'].'/'; ?></small></td>
    </tr>

<?php if( get_option('aws_affiliate_installed') == "yes" ) { ?>
<tr valign="top">
        <th scope="row"><font color="green"><strong>*</strong></font> <?php _e('My Affiliate Settings:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="affiliate" value="<?php echo $option['title_affiliate']; ?>"> [<a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/<?php echo $cpurl['affiliate']; ?>" target="_blank"><?php _e('test', 'auctionAdmin'); ?></a>]<br />
    <?php $args = array( 'name' => 'affiliate2', 'show_option_none' => 'Select Page' ); wp_dropdown_pages( $args ); ?><br /><small><?php if( isset( $options['affiliate'] ) ) echo ''.get_bloginfo('wpurl').'/'.$options['affiliate'].'/'; ?></small></td>
    </tr>
<?php } ?>

    </tbody>
    </table>

	<p class="submit">
    <input type="submit" name="save_settings" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" />
    </p>
    </form>

           </div>
         </div>
       </div>
     </div>

	    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}