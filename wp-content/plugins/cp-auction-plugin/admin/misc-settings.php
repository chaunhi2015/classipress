<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


function cp_auction_plugin_misc_settings() {
	global $current_user, $cp_options;

	$msg = "";
	$ok = "";

	if(isset($_POST['save_access_info'])) {
		$single_ad = isset( $_POST['cp_auction_plugin_access_single'] ) ? esc_attr( $_POST['cp_auction_plugin_access_single'] ) : '';
		$page_ids = isset( $_POST['cp_auction_plugin_access_pages'] ) ? esc_attr( $_POST['cp_auction_plugin_access_pages'] ) : '';
		$disable_alogin = isset( $_POST['cp_auction_plugin_disable_alogin'] ) ? esc_attr( $_POST['cp_auction_plugin_disable_alogin'] ) : '';
		$enable_ahooks = isset( $_POST['cp_auction_plugin_enable_ahooks'] ) ? esc_attr( $_POST['cp_auction_plugin_enable_ahooks'] ) : '';
		$disable_ilogin = isset( $_POST['cp_auction_plugin_disable_ilogin'] ) ? esc_attr( $_POST['cp_auction_plugin_disable_ilogin'] ) : '';
		$enable_ihooks = isset( $_POST['cp_auction_plugin_enable_ihooks'] ) ? esc_attr( $_POST['cp_auction_plugin_enable_ihooks'] ) : '';
		$access_title = isset( $_POST['cp_auction_plugin_access_title'] ) ? esc_attr( $_POST['cp_auction_plugin_access_title'] ) : '';
		$access_info = isset( $_POST['cp_auction_plugin_access_info'] ) ? esc_attr( $_POST['cp_auction_plugin_access_info'] ) : '';
		$blocked_roles = isset( $_POST['cp_auction_blocked_roles'] ) ? esc_attr( $_POST['cp_auction_blocked_roles'] ) : '';
		$access_posts = isset( $_POST['cp_auction_plugin_access_posts'] ) ? esc_attr( $_POST['cp_auction_plugin_access_posts'] ) : '';
		$roles_title = isset( $_POST['cp_auction_plugin_roles_title'] ) ? esc_attr( $_POST['cp_auction_plugin_roles_title'] ) : '';
		$roles_info = isset( $_POST['cp_auction_plugin_roles_info'] ) ? esc_attr( $_POST['cp_auction_plugin_roles_info'] ) : '';
		$blocked_ips = isset( $_POST['cp_auction_blocked_ips'] ) ? esc_attr( $_POST['cp_auction_blocked_ips'] ) : '';
		$redirect_url = isset( $_POST['cp_auction_blocked_redirect_url'] ) ? esc_attr( $_POST['cp_auction_blocked_redirect_url'] ) : '';

		if( $single_ad == "yes" ) {
		update_option('cp_auction_plugin_access_single',$single_ad);
		} else {
		delete_option('cp_auction_plugin_access_single');
		}
		if( $page_ids ) {
		update_option('cp_auction_plugin_access_pages',$page_ids);
		} else {
		delete_option('cp_auction_plugin_access_pages');
		}
		if( $disable_alogin == "yes" ) {
		update_option('cp_auction_plugin_disable_alogin',$disable_alogin);
		} else {
		delete_option('cp_auction_plugin_disable_alogin');
		}
		if( $disable_ilogin == "yes" ) {
		update_option('cp_auction_plugin_disable_ilogin',$disable_ilogin);
		} else {
		delete_option('cp_auction_plugin_disable_ilogin');
		}
		if( $enable_ahooks == "yes" ) {
		update_option('cp_auction_plugin_enable_ahooks',$enable_ahooks);
		} else {
		delete_option('cp_auction_plugin_enable_ahooks');
		}
		if( $enable_ihooks == "yes" ) {
		update_option('cp_auction_plugin_enable_ihooks',$enable_ihooks);
		} else {
		delete_option('cp_auction_plugin_enable_ihooks');
		}
		if( $access_title ) {
		update_option('cp_auction_plugin_access_title',$access_title);
		} else {
		delete_option('cp_auction_plugin_access_title');
		}
		if( $access_info ) {
		update_option('cp_auction_plugin_access_info',stripslashes(html_entity_decode($access_info)));
		} else {
		delete_option('cp_auction_plugin_access_info');
		}
		if( $blocked_roles ) {
		update_option('cp_auction_blocked_roles',$blocked_roles);
		} else {
		delete_option('cp_auction_blocked_roles');
		}
		if( $access_posts ) {
		update_option('cp_auction_plugin_access_posts',$access_posts);
		} else {
		delete_option('cp_auction_plugin_access_posts');
		}
		if( $roles_title ) {
		update_option('cp_auction_plugin_roles_title',$roles_title);
		} else {
		delete_option('cp_auction_plugin_roles_title');
		}
		if( $roles_info ) {
		update_option('cp_auction_plugin_roles_info',$roles_info);
		} else {
		delete_option('cp_auction_plugin_roles_info');
		}
		if( $blocked_ips ) {
		update_option('cp_auction_blocked_ips',$blocked_ips);
		} else {
		delete_option('cp_auction_blocked_ips');
		}
		if( $redirect_url ) {
		update_option('cp_auction_blocked_redirect_url',$redirect_url);
		} else {
		delete_option('cp_auction_blocked_redirect_url');
		}
		$msg = ''.__('Access settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_form_settings'])) {
		$select = isset( $_POST['cp_auction_dropdown_select_title'] ) ? esc_attr( $_POST['cp_auction_dropdown_select_title'] ) : '';
		$classified = isset( $_POST['cp_auction_dropdown_classified_title'] ) ? esc_attr( $_POST['cp_auction_dropdown_classified_title'] ) : '';
		$wanted = isset( $_POST['cp_auction_dropdown_wanted_title'] ) ? esc_attr( $_POST['cp_auction_dropdown_wanted_title'] ) : '';
		$auction = isset( $_POST['cp_auction_dropdown_auction_title'] ) ? esc_attr( $_POST['cp_auction_dropdown_auction_title'] ) : '';
		$reverse = isset( $_POST['cp_auction_dropdown_reverse_title'] ) ? esc_attr( $_POST['cp_auction_dropdown_reverse_title'] ) : '';
		$buy_classified = isset( $_POST['cp_auction_buynow_on_classifieds'] ) ? esc_attr( $_POST['cp_auction_buynow_on_classifieds'] ) : '';
		$buy_auction = isset( $_POST['cp_auction_buynow_on_auctions'] ) ? esc_attr( $_POST['cp_auction_buynow_on_auctions'] ) : '';
		$wanto = isset( $_POST['cp_auction_want_to_defaults'] ) ? esc_attr( $_POST['cp_auction_want_to_defaults'] ) : '';
		$deals = isset( $_POST['cp_auction_allow_deals_auto'] ) ? esc_attr( $_POST['cp_auction_allow_deals_auto'] ) : '';

		if( $select ) {
		update_option('cp_auction_dropdown_select_title',$select);
		} else {
		delete_option('cp_auction_dropdown_select_title');
		}
		if( $classified ) {
		update_option('cp_auction_dropdown_classified_title',$classified);
		} else {
		delete_option('cp_auction_dropdown_classified_title');
		}
		if( $wanted ) {
		update_option('cp_auction_dropdown_wanted_title',$wanted);
		} else {
		delete_option('cp_auction_dropdown_wanted_title');
		}
		if( $auction ) {
		update_option('cp_auction_dropdown_auction_title',$auction);
		} else {
		delete_option('cp_auction_dropdown_auction_title');
		}
		if( $reverse ) {
		update_option('cp_auction_dropdown_reverse_title',$reverse);
		} else {
		delete_option('cp_auction_dropdown_reverse_title');
		}
		if( $buy_classified == "yes" ) {
		update_option('cp_auction_buynow_on_classifieds',$buy_classified);
		} else {
		delete_option('cp_auction_buynow_on_classifieds');
		}
		if( $buy_auction == "yes" ) {
		update_option('cp_auction_buynow_on_auctions',$buy_auction);
		} else {
		delete_option('cp_auction_buynow_on_auctions');
		}
		if( $wanto ) {
		update_option('cp_auction_want_to_defaults',$wanto);
		} else {
		delete_option('cp_auction_want_to_defaults');
		}
		if( $deals == "yes" ) {
		update_option('cp_auction_allow_deals_auto',$deals);
		} else {
		delete_option('cp_auction_allow_deals_auto');
		}
		$msg = ''.__('Submit form settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_custom'])) {
		$wrapper = isset( $_POST['cp_auction_custom_wrapper'] ) ? esc_attr( $_POST['cp_auction_custom_wrapper'] ) : '';
		$style = isset( $_POST['cp_auction_custom_style'] ) ? esc_attr( $_POST['cp_auction_custom_style'] ) : '';
		if( $wrapper == "yes" ) {
		update_option('cp_auction_custom_wrapper',$wrapper);
		} else {
		delete_option('cp_auction_custom_wrapper');
		}
		if( $style == "yes" ) {
		update_option('cp_auction_custom_style',$style);
		} else {
		delete_option('cp_auction_custom_style');
		}
		$msg = ''.__('Settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_author_page'])) {
		$apage = isset( $_POST['cp_auction_author_page'] ) ? esc_attr( $_POST['cp_auction_author_page'] ) : '';
		if( $apage ) {
		update_option('cp_auction_author_page',$apage);
		} else {
		delete_option('cp_auction_author_page');
		}
		$msg = ''.__('Link settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_currency'])) {
		$enable = isset( $_POST['cp_auction_enable_conversion'] ) ? esc_attr( $_POST['cp_auction_enable_conversion'] ) : '';
		$convert = isset( $_POST['cp_auction_convert_to'] ) ? esc_attr( $_POST['cp_auction_convert_to'] ) : '';
		if( $enable == "yes" ) {
		update_option('cp_auction_enable_conversion',$enable);
		} else {
		delete_option('cp_auction_enable_conversion');
		}
		if( $convert ) {
		update_option('cp_auction_convert_to',$convert);
		} else {
		delete_option('cp_auction_convert_to');
		}
		$msg = ''.__('The conversion settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_button_pos'])) {
		$pos = isset( $_POST['cp_auction_buynow_button_float'] ) ? esc_attr( $_POST['cp_auction_buynow_button_float'] ) : '';
		$poss = isset( $_POST['cp_auction_buynow_button_float_short'] ) ? esc_attr( $_POST['cp_auction_buynow_button_float_short'] ) : '';
		if( $pos ) {
		update_option('cp_auction_buynow_button_float',$pos);
		} else {
		delete_option('cp_auction_buynow_button_float');
		}
		if( $poss ) {
		update_option('cp_auction_buynow_button_float_short',$poss);
		} else {
		delete_option('cp_auction_buynow_button_float_short');
		}
		$msg = ''.__('Button position was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_uninstall'])) {
		$uninstall = isset( $_POST['cp_auction_plugin_uninstall'] ) ? esc_attr( $_POST['cp_auction_plugin_uninstall'] ) : '';
		if( $uninstall == "yes" ) {
		update_option('cp_auction_plugin_uninstall',$uninstall);
		} else {
		delete_option('cp_auction_plugin_uninstall');
		}
		$msg = ''.__('Settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_adm_info'])) {
		$adm_link = isset( $_POST['cp_auction_plugin_adm_link'] ) ? esc_attr( $_POST['cp_auction_plugin_adm_link'] ) : '';
		$adm_info = isset( $_POST['cp_auction_plugin_adm_info'] ) ? esc_attr( $_POST['cp_auction_plugin_adm_info'] ) : '';
		if( $adm_link ) {
		update_option('cp_auction_plugin_adm_link',$adm_link);
		} else {
		delete_option('cp_auction_plugin_adm_link');
		}
		if( $adm_info ) {
		update_option('cp_auction_plugin_adm_info',$adm_info);
		} else {
		delete_option('cp_auction_plugin_adm_info');
		}
		$msg = ''.__('Link and text was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_page_info'])) {
		$page_info = isset( $_POST['cp_auction_plugin_page_info'] ) ? esc_attr( $_POST['cp_auction_plugin_page_info'] ) : '';
		if( $page_info ) {
		update_option('cp_auction_plugin_page_info',stripslashes(html_entity_decode($page_info)));
		} else {
		delete_option('cp_auction_plugin_page_info');
		}
		$msg = ''.__('Text information was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_upload_page_info'])) {
		$upload_page_info = isset( $_POST['cp_auction_upload_page_info'] ) ? esc_attr( $_POST['cp_auction_upload_page_info'] ) : '';
		if( $upload_page_info ) {
		update_option('cp_auction_upload_page_info',stripslashes(html_entity_decode($upload_page_info)));
		} else {
		delete_option('cp_auction_upload_page_info');
		}
		$msg = ''.__('Upload page information was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_dashboard_page_info'])) {
		$dashboard_page_info = isset( $_POST['cp_auction_dashboard_page_info'] ) ? esc_attr( $_POST['cp_auction_dashboard_page_info'] ) : '';
		if( $dashboard_page_info ) {
		update_option('cp_auction_dashboard_page_info',stripslashes(html_entity_decode($dashboard_page_info)));
		} else {
		delete_option('cp_auction_dashboard_page_info');
		}
		$msg = ''.__('Dashboard page information was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_terms_link'])) {
		$terms_link = isset( $_POST['cp_auction_plugin_terms_link'] ) ? esc_attr( $_POST['cp_auction_plugin_terms_link'] ) : '';
		if( $terms_link ) {
		update_option('cp_auction_plugin_terms_link',stripslashes(html_entity_decode($terms_link)));
		} else {
		delete_option('cp_auction_plugin_terms_link');
		}
		$msg = ''.__('Link to terms page was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_bump_ad'])) {
		$enable = isset( $_POST['cp_auction_bump_ad_enabled'] ) ? esc_attr( $_POST['cp_auction_bump_ad_enabled'] ) : '';
		$bump_price = isset( $_POST['cp_auction_bump_ad_price'] ) ? esc_attr( $_POST['cp_auction_bump_ad_price'] ) : '';
		$bump_exp = isset( $_POST['cp_auction_bump_extend_exp'] ) ? esc_attr( $_POST['cp_auction_bump_extend_exp'] ) : '';
		$upgrade = isset( $_POST['cp_auction_upgrade_ad_enabled'] ) ? esc_attr( $_POST['cp_auction_upgrade_ad_enabled'] ) : '';
		$upg_price = isset( $_POST['cp_auction_upgrade_ad_price'] ) ? esc_attr( $_POST['cp_auction_upgrade_ad_price'] ) : '';
		$upgrade_exp = isset( $_POST['cp_auction_upgrade_extend_exp'] ) ? esc_attr( $_POST['cp_auction_upgrade_extend_exp'] ) : '';
		$link = isset( $_POST['cp_auction_bump_ad_link'] ) ? esc_attr( $_POST['cp_auction_bump_ad_link'] ) : '';
		if( $enable == "yes" ) {
		update_option('cp_auction_bump_ad_enabled',$enable);
		} else {
		delete_option('cp_auction_bump_ad_enabled');
		}
		if( $bump_price ) {
		update_option('cp_auction_bump_ad_price',$bump_price);
		} else {
		delete_option('cp_auction_bump_ad_price');
		}
		if( $bump_exp ) {
		update_option('cp_auction_bump_extend_exp',$bump_exp);
		} else {
		delete_option('cp_auction_bump_extend_exp');
		}
		if( $upgrade == "yes" ) {
		update_option('cp_auction_upgrade_ad_enabled',$upgrade);
		} else {
		delete_option('cp_auction_upgrade_ad_enabled');
		}
		if( $upg_price ) {
		update_option('cp_auction_upgrade_ad_price',$upg_price);
		} else {
		delete_option('cp_auction_upgrade_ad_price');
		}
		if( $upgrade_exp ) {
		update_option('cp_auction_upgrade_extend_exp',$upgrade_exp);
		} else {
		delete_option('cp_auction_upgrade_extend_exp');
		}
		if( $link ) {
		update_option('cp_auction_bump_ad_link',$link);
		} else {
		delete_option('cp_auction_bump_ad_link');
		}
		$msg = ''.__('Bump Up settings was saved!','auctionAdmin').'';
		}
	if(isset($_POST['save_ad_page'])) {
		$classified = isset( $_POST['classified'] ) ? esc_attr( $_POST['classified'] ) : '';
		$auctions = isset( $_POST['normal'] ) ? esc_attr( $_POST['normal'] ) : '';
		$wanted = isset( $_POST['wanted'] ) ? esc_attr( $_POST['wanted'] ) : '';
		$replace = isset( $_POST['cp_auction_do_not_replace_single'] ) ? esc_attr( $_POST['cp_auction_do_not_replace_single'] ) : '';
		$view_ads = isset( $_POST['cp_auction_view_all_ads'] ) ? esc_attr( $_POST['cp_auction_view_all_ads'] ) : '';
		$numbers = isset( $_POST['cp_auction_number_of_ads'] ) ? esc_attr( $_POST['cp_auction_number_of_ads'] ) : '';
		$remove_avatar = isset( $_POST['cp_auction_remove_avatar'] ) ? esc_attr( $_POST['cp_auction_remove_avatar'] ) : '';
		$hide_info = isset( $_POST['cp_auction_hide_author_info'] ) ? esc_attr( $_POST['cp_auction_hide_author_info'] ) : '';
		$move_bar = isset( $_POST['cp_auction_move_menu_bar'] ) ? esc_attr( $_POST['cp_auction_move_menu_bar'] ) : '';
		$pull_fields = isset( $_POST['cp_auction_pull_fields'] ) ? esc_attr( $_POST['cp_auction_pull_fields'] ) : '';
		$about = isset( $_POST['cp_auction_about_me'] ) ? esc_attr( $_POST['cp_auction_about_me'] ) : '';
		$maps = isset( $_POST['remove_maps'] ) ? esc_attr( $_POST['remove_maps'] ) : '';
		$enable = isset( $_POST['cp_auction_enable_custom_sidebar'] ) ? esc_attr( $_POST['cp_auction_enable_custom_sidebar'] ) : '';

		$list = ''.$classified.','.$auctions.','.$wanted.'';
		$array = explode( ',', $list );
		$new_list = implode(",", $array);
		$field_ads = trim($new_list, ',');

		$option = get_option('cp_auction_fields');
		$option['cp_contact_options'] = $field_ads;
		if ( ! empty( $option ) )
   		update_option('cp_auction_fields', $option);

		if( $replace == "yes" ) {
		update_option('cp_auction_do_not_replace_single',$replace);
		} else {
		delete_option('cp_auction_do_not_replace_single');
		}
		if( $view_ads ) {
		update_option('cp_auction_view_all_ads',$view_ads);
		} else {
		delete_option('cp_auction_view_all_ads');
		}
		if( $numbers ) {
		update_option('cp_auction_number_of_ads',$numbers);
		} else {
		delete_option('cp_auction_number_of_ads');
		}
		if( $remove_avatar == "yes" ) {
		update_option('cp_auction_remove_avatar',$remove_avatar);
		} else {
		delete_option('cp_auction_remove_avatar');
		}
		if( $hide_info == "yes" ) {
		update_option('cp_auction_hide_author_info',$hide_info);
		} else {
		delete_option('cp_auction_hide_author_info');
		}
		if( $move_bar == "yes" ) {
		update_option('cp_auction_move_menu_bar',$move_bar);
		} else {
		delete_option('cp_auction_move_menu_bar');
		}
		if( $pull_fields ) {
		update_option('cp_auction_pull_fields',$pull_fields);
		} else {
		delete_option('cp_auction_pull_fields');
		}
		if( $about == "yes" ) {
		update_option('cp_auction_about_me',$about);
		} else {
		delete_option('cp_auction_about_me');
		}
		if( $maps == "yes" ) {
		update_option('remove_maps',$maps);
		} else {
		delete_option('remove_maps');
		}
		if( $enable == "yes" ) {
		update_option('cp_auction_enable_custom_sidebar',$enable);
		} else {
		delete_option('cp_auction_enable_custom_sidebar');
		delete_option('cp_auction_move_menu_bar');
		}
		$msg = ''.__('Custom settings was saved!','auctionAdmin').'';
		}

	if(isset($_POST['empty_tables'])) {
		$ok = cp_auction_delete_bids();
		if( $ok == 2 ) $msg = '- '.__('Bids table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the bid table!','auctionAdmin').'<br />';
		$ok1 = cp_auction_delete_purchases();
		if( $ok1 == 2 ) $msg .= '- '.__('Purchase table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the purchase table!','auctionAdmin').'<br />';
		$ok2 = cp_auction_delete_transactions();
		if( $ok2 == 2 ) $msg .= '- '.__('Transaction table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the transaction table!','auctionAdmin').'<br />';
		$ok3 = cp_auction_delete_watchlist();
		if( $ok3 == 2 ) $msg .= '- '.__('Watchlist table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the watchlist table!','auctionAdmin').'<br />';
		$ok4 = cp_auction_delete_coupons();
		if( $ok4 == 2 ) $msg .= '- '.__('Coupons table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the coupons table!','auctionAdmin').'<br />';
		$ok5 = cp_auction_delete_uploads();
		if( $ok5 == 2 ) $msg .= '- '.__('Uploads table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the uploads table!','auctionAdmin').'<br />';
		$ok6 = cp_auction_delete_favourites();
		if( $ok6 == 2 ) $msg .= '- '.__('Favourite table was emptied and ok.','auctionAdmin').'';
		else $msg = '- '.__('Something went wrong during the emptying of the favourite table!','auctionAdmin').'';
		}

	if(isset($_POST['total_empty_tables'])) {
		$ok = cp_auction_delete_bids('empty');
		if( $ok == 2 ) $msg = '- '.__('Bids table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the bid table!','auctionAdmin').'<br />';
		$ok1 = cp_auction_delete_purchases('empty');
		if( $ok1 == 2 ) $msg .= '- '.__('Purchase table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the purchase table!','auctionAdmin').'<br />';
		$ok2 = cp_auction_delete_transactions('empty');
		if( $ok2 == 2 ) $msg .= '- '.__('Transaction table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the transaction table!','auctionAdmin').'<br />';
		$ok3 = cp_auction_delete_watchlist('empty');
		if( $ok3 == 2 ) $msg .= '- '.__('Watchlist table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the watchlist table!','auctionAdmin').'<br />';
		$ok4 = cp_auction_delete_coupons('empty');
		if( $ok4 == 2 ) $msg .= '- '.__('Coupons table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the coupons table!','auctionAdmin').'<br />';
		$ok5 = cp_auction_delete_uploads('empty');
		if( $ok5 == 2 ) $msg .= '- '.__('Uploads table was emptied and ok.','auctionAdmin').'<br />';
		else $msg = '- '.__('Something went wrong during the emptying of the uploads table!','auctionAdmin').'<br />';
		$ok6 = cp_auction_delete_favourites('empty');
		if( $ok6 == 2 ) $msg .= '- '.__('Favourite table was emptied and ok.','auctionAdmin').'';
		else $msg = '- '.__('Something went wrong during the emptying of the favourite table!','auctionAdmin').'';
		}

	$option = get_option('cp_auction_fields');
	if( isset( $option['cp_contact_options'] ) )
	$list = explode(',', $option['cp_contact_options']);
	else $list = false;

?>

	<?php if($msg) echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$msg.'</strong></p></div>'; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Other Settings', 'auctionAdmin'); ?></h3>
            <div class="inside">

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Custom header & footer', 'auctionAdmin'); ?></div>

	<?php _e('The header and footer is controlled by <font color="navy">wrapper.php</font> wich is located in <font color="navy">wp-content/plugins/cp-auction-plugin/scripts/wrapper.php</font>. CP Auction plugin has a built in custom call wich means if you create the following files, <font color="navy">cpauction-wrapper.php</font> and <font color="navy">cpauction.css</font>, then copy the content from <font color="navy">wp-content/plugins/cp-auction-plugin/scripts/<strong>wrapper.php</strong></font> and <font color="navy">wp-content/plugins/cp-auction-plugin/css/<strong>custom.css</strong></font> into the respective files wich you just created and upload both these files into the <font color="navy">wp-content/plugins/</font> directory, edit them as to your own needs, you then have your own header, footer and .css without the need to backup your changes each time CP Auction plugin has an update.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<?php _e('<strong>IMPORTANT</strong>: If you use any of our supported child themes you must copy the content from the actual child themes .css file located in <font color="navy">wp-content/plugins/cp-auction-plugin/css/</font>.', 'auctionAdmin'); ?>

	<div class="clear10"></div>	

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable Custom Wrapper:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_custom_wrapper"><?php echo cp_auction_yes_no(get_option('cp_auction_custom_wrapper')); ?></select> <?php _e('Enable only if you have uploaded <font color="navy">cpauction-wrapper.php</font> to plugins directory.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to enable the use of your own header and footer.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable Custom Style:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_custom_style"><?php echo cp_auction_yes_no(get_option('cp_auction_custom_style')); ?></select> <?php _e('Enable only if you have uploaded <font color="navy">cpauction.css</font> to plugins directory.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to enable the use of your own custom style.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>

	<div class="clear10"></div>

			<p class="submit"><input type="submit" name="save_custom" id="submit" class="button-primary" value="<?php _e('Save Changes', 'auctionAdmin'); ?>"  /></p>

		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Currency Conversion', 'auctionAdmin'); ?></div>
	<?php _e('An useful tool if you want to use PayPal with another currency than your website standards. This option requires the <strong><a style="text-decoration: none;" href="https://wordpress.org/plugins/currencyr/" target="_blank">Currencyr</a></strong> plugin. This means you can display your ads in standard stated currency and charge via PayPal in another currency.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<?php _e('Install the <strong><a style="text-decoration: none;" href="https://wordpress.org/plugins/currencyr/" target="_blank">Currencyr</a></strong> plugin and enter the settings similar to your ClassiPress payment settings. It is <strong>important</strong> to select the same currency ( <a style="text-decoration: none;" href="/wp-admin/admin.php?page=currencyr">Base Currency</a> ) as you use as your website standards. <strong>Does not work with new ads payment but would convert all PayPal payments pursued by CP Auction</strong>.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable Currency Conversion:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_enable_conversion"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_conversion')); ?></select> <?php _e('Enable only if you have installed the <strong><a style="text-decoration: none;" href="https://wordpress.org/plugins/currencyr/" target="_blank">Currencyr</a></strong> plugin.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option if you want to use PayPal with another currency than your website standards.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Convert', 'auctionAdmin'); ?> <?php echo $cp_options->currency_code; ?> <?php _e('to:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_convert_to"><?php echo cp_auction_currencyr_dropdown(get_option('cp_auction_convert_to')); ?></select> <?php _e('Currency supported by PayPal.', 'auctionAdmin'); ?><br /><small><?php _e('Select the currency that you want to use with PayPal payments.', 'auctionAdmin'); ?></small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

			<p class="submit"><input type="submit" name="save_currency" id="submit" class="button-primary" value="<?php _e('Save Currency Conversion', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Submit Form Custom Settings', 'auctionAdmin'); ?></div>
	<?php _e('Customize the post an ad submit form by changing / editing the below settings.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Select Ad Type:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_dropdown_select_title" value="<?php echo get_option('cp_auction_dropdown_select_title'); ?>"> <?php _e('Select Ad Type Title.', 'auctionAdmin'); ?><br /><small><?php _e('Enter your preferred title for the select ad type.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Classified Ad:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_dropdown_classified_title" value="<?php echo get_option('cp_auction_dropdown_classified_title'); ?>"> <?php _e('Classified Ad Type.', 'auctionAdmin'); ?><br /><small><?php _e('Enter your preferred title for the classified ad type.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Wanted:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_dropdown_wanted_title" value="<?php echo get_option('cp_auction_dropdown_wanted_title'); ?>"> <?php _e('Wanted Ad Type.', 'auctionAdmin'); ?><br /><small><?php _e('Enter your preferred title for the wanted ad type.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Normal Auction:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_dropdown_auction_title" value="<?php echo get_option('cp_auction_dropdown_auction_title'); ?>"> <?php _e('Normal Auction Ad Type.', 'auctionAdmin'); ?><br /><small><?php _e('Enter your preferred title for the normal auction ad type.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Reverse Auction:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_dropdown_reverse_title" value="<?php echo get_option('cp_auction_dropdown_reverse_title'); ?>"> <?php _e('Reverse Auction Ad Type.', 'auctionAdmin'); ?><br /><small><?php _e('Enter your preferred title for the reverse auction ad type.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Buy Now on Classifieds:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_buynow_on_classifieds"><?php echo cp_auction_yes_no(get_option('cp_auction_buynow_on_classifieds')); ?></select> <?php _e('Enable Buy Now automatically.', 'auctionAdmin'); ?><br /><small><?php _e('Enable if you want to hide the <strong>Enable Buy Now</strong> checkbox and rather activate buy now on classified ads automatically.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Buy Now on Auctions:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_buynow_on_auctions"><?php echo cp_auction_yes_no(get_option('cp_auction_buynow_on_auctions')); ?></select> <?php _e('Enable Buy Now automatically if buy now price is set.', 'auctionAdmin'); ?><br /><small><?php _e('Enable if you want to hide the <strong>Enable Buy Now</strong> checkbox and rather activate buy now on auction ads automatically if the buy now price is set.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Wanted Ad Defaults:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_want_to_defaults">
						<option value="" <?php if(get_option('cp_auction_want_to_defaults') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="Buy" <?php if(get_option('cp_auction_want_to_defaults') == "Buy") echo 'selected="selected"'; ?>><?php _e('Buy', 'auctionAdmin'); ?></option>
						<option value="Swap" <?php if(get_option('cp_auction_want_to_defaults') == 'Swap') echo 'selected="selected"'; ?>><?php _e('Swap', 'auctionAdmin'); ?></option>
						<option value="Give Away" <?php if(get_option('cp_auction_want_to_defaults') == 'Give Away') echo 'selected="selected"'; ?>><?php _e('Give Away', 'auctionAdmin'); ?></option>
						</select> <?php _e('Set a default value for the <strong>I Want To</strong> selectbox.', 'auctionAdmin'); ?><br /><small><?php _e('If a default value is set the <strong>I Want To</strong> selectbox will be hidden from the submit form and the default set value will be added to the wanted ad automatically. <strong>Note</strong>: The <strong>Swap With</strong> field can be deactivated from the custom field settings.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Allow Deals as Default:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_allow_deals_auto"><?php echo cp_auction_yes_no(get_option('cp_auction_allow_deals_auto')); ?></select> <?php _e('For Wanted Ads.', 'auctionAdmin'); ?><br /><small><?php _e('Enable if you want to hide the <strong>Allow Deals</strong> selectbox and rather activate deals on wanted ads automatically.', 'auctionAdmin'); ?></small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

			<p class="submit"><input type="submit" name="save_form_settings" id="submit" class="button-primary" value="<?php _e('Save Changes', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Bump Ad & Featured Upgrade Settings', 'auctionAdmin'); ?></div>
	<?php _e('Allow author to bump ads, or upgrade their ads to featured from within the my ads page.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable Bump Ad:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_bump_ad_enabled"><?php echo cp_auction_yes_no(get_option('cp_auction_bump_ad_enabled')); ?></select> <?php _e('Enable the Bump Ad feature.', 'auctionAdmin'); ?><br /><small><?php _e('Bumping up Ads will reset the post date of users Ad to the time of the bump, thus moving the Ad from its current position in the listings back up to the top of the first page.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Bump Ad Price:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_bump_ad_price" value="<?php echo get_option('cp_auction_bump_ad_price'); ?>"> <?php _e('Set the Bump Ad price.', 'auctionAdmin'); ?><br /><small><?php _e('Enter what it would cost the user to Bump their Ad, eg. 10 or 10.00', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Extend Ad Duration:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_bump_extend_exp" value="<?php echo get_option('cp_auction_bump_extend_exp'); ?>"> <?php _e('Enter the number of days, eg. 5 (works not with auctions).', 'auctionAdmin'); ?><br /><small><?php _e('Enter the number of days you wish to extend the duration of the ad when bumped up. Leave blank for no extension of the ad duration.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable Featured Upgrade:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_upgrade_ad_enabled"><?php echo cp_auction_yes_no(get_option('cp_auction_upgrade_ad_enabled')); ?></select> <?php _e('Enable the Featured Upgrade feature.', 'auctionAdmin'); ?><br /><small><?php _e('Allow users to upgrade their active ads to featured ads. Featured Ads are randomly selected to appear on rotation at the top in the main category they have been posted in.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Featured Ad Price:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_upgrade_ad_price" value="<?php echo get_option('cp_auction_upgrade_ad_price'); ?>"> <?php _e('Set the upgrade to Featured Ad price.', 'auctionAdmin'); ?><br /><small><?php _e('Enter what it would cost the user to Upgrade their Ad to Featured, eg. 10 or 10.00', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Extend Ad Duration:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_upgrade_extend_exp" value="<?php echo get_option('cp_auction_upgrade_extend_exp'); ?>"> <?php _e('Enter the number of days, eg. 5 (works not with auctions).', 'auctionAdmin'); ?><br /><small><?php _e('Enter the number of days you wish to extend the duration of the ad when upgrade to featured. Leave blank for no extension of the ad duration.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row" valign="top"><?php _e('Link Placement:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_bump_ad_link">
						<option value="" <?php if(get_option('cp_auction_bump_ad_link') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="block" <?php if(get_option('cp_auction_bump_ad_link') == "block") echo 'selected="selected"'; ?>><?php _e('Block', 'auctionAdmin'); ?></option>
						<option value="table" <?php if(get_option('cp_auction_bump_ad_link') == 'table') echo 'selected="selected"'; ?>><?php _e('Table', 'auctionAdmin'); ?></option>
						</select> <?php _e('Position the Bump Ad and Featured Upgrade link.', 'auctionAdmin'); ?><br /><small><?php _e('Select Table to position the Bump Ad and Featured Upgrade link in a new table field between Views and Status in the My Ads page, or select Block to position the Bump Ad and Featured Upgrade link in between the option links in the My Ads page.', 'auctionAdmin'); ?></small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

	<p class="submit"><input type="submit" name="save_bump_ad" id="submit" class="button-primary" value="<?php _e('Save Bump Ad Settings', 'auctionAdmin'); ?>"  /></p>

		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('View All Ads link should point to', 'auctionAdmin'); ?></div>
	<?php _e('Enable the option below to indicate which page the <strong>View All Ads</strong> link on the single ads listing should point to, if you choose the Author Page so you must make sure that you have replaced the standard author.php file with our modified file located in the folder cp-auction-plugin/custom-files.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('View All Ads link:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_author_page">
						<option value="" <?php if(get_option('cp_auction_author_page') == '') echo 'selected="selected"'; ?>><?php _e('Authors Listings', 'auctionAdmin'); ?></option>
						<option value="apage" <?php if(get_option('cp_auction_author_page') == 'apage') echo 'selected="selected"'; ?>><?php _e('Authors Page', 'auctionAdmin'); ?></option>
						</select><br /><small><?php _e('if you choose the Author Page so you must make sure that you have replaced the standard author.php file with our modified file located in the folder cp-auction-plugin/custom-files.', 'auctionAdmin'); ?></small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

	<p class="submit"><input type="submit" name="save_author_page" id="submit" class="button-primary" value="<?php _e('Save Link Settings', 'auctionAdmin'); ?>"  /></p>

		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Single Ad Page Customizations', 'auctionAdmin'); ?></div>
	<?php _e('The single ad sidebar customization use three new custom fields, <strong>cp_contact_phone</strong>, <strong>cp_contact_email</strong> and <strong>cp_contact_notes</strong>, these fields should be activated through the CP Auction <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=fields">custom field settings</a>.', 'auctionAdmin'); ?>

<form method="post" action="">

<div class="note-frame">

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Single Ad Page File:', 'auctionAdmin'); ?></th>
    						<td><select name="cp_auction_do_not_replace_single">
						<option value="" <?php if(get_option('cp_auction_do_not_replace_single') == '') echo 'selected="selected"'; ?>><?php _e('Replace File', 'auctionAdmin'); ?></option>
						<option value="yes" <?php if(get_option('cp_auction_do_not_replace_single') == 'yes') echo 'selected="selected"'; ?>><?php _e('Do NOT Replace File', 'auctionAdmin'); ?></option>
						</select> <?php _e('Automatically replace the <strong>single-ad_listing.php</strong> file.', 'auctionAdmin'); ?><br /><small><?php _e('CP Auction will automatically replace the standard <strong>single-ad_listing.php</strong> to make the different option widgets automatically added to the sidebar, you can choose to disable this behavior by selecting the "Do NOT Replace File" option above. <strong>NOTE</strong>: If you choose to disable the auto replacement you will have to manually copy/paste the following codes/files to enable ie. custom sidebar, bid widget, best offer widget and wanted ads widget (the standard widgets will still work). Use the below hook to enable the different options into the on top tabbed sidebar widget, just replace the tabs code in the file <strong>sidebar-ad.php</strong> with the below code. The sidebar-ad.php file is located in "<strong>wp-content/themes/classipress</strong>".', 'auctionAdmin'); ?></small>

	<div class="clear10"></div>

<small><?php _e('<strong>NOTE</strong>: If you use a childtheme you might have to edit the <strong>sidebar-ad.php</strong> file within your childtheme directory.', 'auctionAdmin'); ?></small>

<div class="code_box">
	&lt;?php if ( function_exists('cp_auction_bidder_box_tabbed') ) cp_auction_bidder_box_tabbed(); ?&gt;
	</div>

	<div class="clear10"></div>

	<small><strong><?php _e('When done code should look something like below', 'auctionAdmin'); ?></strong>:</small>

	<div class="code_box">
	&lt;div class="tabprice"&gt;
	<br />
	&lt;?php if ( function_exists('cp_auction_bidder_box_tabbed') ) cp_auction_bidder_box_tabbed(); ?&gt;
	<br />
	&lt;?php if ( $gmap_active ) { ?&gt;
	</div>

	<div class="clear20"></div>

<small><strong><?php _e('The following is only valid IF you choose to NOT automatically replace the "single-ad_listing.php" file', 'auctionAdmin'); ?></strong>:</small><br />
<small><?php _e('We have included a modified <strong>sidebar-ad.php</strong> file for the ClassiPress theme and most of our supported child themes. This new file can be found at the following location, wp-content/plugins/cp-auction-plugin/custom-files/. Just rename the old <strong>sidebar-ad.php</strong> file to <strong>original_sidebar.php</strong> (please read the below note), then upload the new sidebar-ad.php file into the same location to replace the old, the classipress files to be replaced can be found here, wp-content/themes/classipress/. <strong>If you use any child theme then it is strongly recommended to just upload the custom file into the child theme directory</strong>. This customization also use three new custom fields, <strong>cp_contact_phone</strong>, <strong>cp_contact_email</strong> and <strong>cp_contact_notes</strong>, these fields should be activated through the CP Auction <a style="text-decoration: none;" href="../wp-admin/admin.php?page=cp-auction&tab=fields">custom field settings</a>.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

<?php _e('<strong>Note:</strong> If you rename the old/original file to <strong>original_sidebar.php</strong> then you can switch between the orginal and the custom sidebar whenever you need without having to upload or rename any files again, just enable/disable the custom sidebar with the below option Enable Custom Sidebar.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

<?php _e('<strong>Where to edit the replaced file:</strong> If you want to use the automatically replaced "single-ad_listing.php" file and also want to customize it then keep the above option set to <strong>Replace File</strong>, and locate the "single-ad_listing.php" file for customizations here, <strong>wp-content/plugins/cp-auction-plugin/templates/single-ad_listing_childtheme.php</strong>. Remember to backup your customizations before you later on update CP Auction plugin.', 'auctionAdmin'); ?></small></td>
    </tr>

				</tbody>
			</table>

	<p class="submit"><input type="submit" name="save_ad_page" id="submit" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>"  /></p>

</div>

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Other items listed by:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_view_all_ads">
						<option value="" <?php if(get_option('cp_auction_view_all_ads') == '') echo 'selected="selected"'; ?>><?php _e('Disabled', 'auctionAdmin'); ?></option>
						<option value="below" <?php if(get_option('cp_auction_view_all_ads') == 'below') echo 'selected="selected"'; ?>><?php _e('Below Ad', 'auctionAdmin'); ?></option>
						<option value="content" <?php if(get_option('cp_auction_view_all_ads') == 'content') echo 'selected="selected"'; ?>><?php _e('Below Content', 'auctionAdmin'); ?></option>
						</select> <?php _e('Enable the, Other items listed by.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option if you want to display other items listed by the actual author just below the single ad listing.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Number of Ads to Display:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_number_of_ads" value="<?php echo get_option('cp_auction_number_of_ads'); ?>"> <?php _e('The number of ads to display.', 'auctionAdmin'); ?><br /><small><?php _e('Set the number of ads you want to display just below the single ad listing.', 'auctionAdmin'); ?>)</small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable Custom Sidebar:', 'auctionAdmin'); ?></th>
    						<td><select name="cp_auction_enable_custom_sidebar"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_custom_sidebar')); ?></select> <?php _e('Enable the custom sidebar.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to enable the custom sidebar, this ONLY if you have followed the instructions as given above.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Remove Users Avatar:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_remove_avatar"><?php echo cp_auction_yes_no(get_option('cp_auction_remove_avatar')); ?></select> <?php _e('Remove avatar on custom single ad sidebar.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to remove the avatar / profile picture from the custom single ad sidebar.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Remove Map:', 'auctionAdmin'); ?></th>
    <td><select name="remove_maps"><?php echo cp_auction_yes_no(get_option('remove_maps')); ?></select> <?php _e('Hide the sidebar google map on custom single ad sidebar.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to hide the google sidebar map.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('View About Me:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_about_me"><?php echo cp_auction_yes_no(get_option('cp_auction_about_me')); ?></select> <?php _e('View authors about me info in custom single ad sidebar.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to view authors about me information in the sidebar.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Hide Author Info:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_hide_author_info"><?php echo cp_auction_yes_no(get_option('cp_auction_hide_author_info')); ?></select> <?php _e('Hide author info if ad is not active (custom sidebar only).', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to hide the author info when ad has status as pending, sold or expired.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Move Menu Bar:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_move_menu_bar"><?php echo cp_auction_yes_no(get_option('cp_auction_move_menu_bar')); ?></select> <?php _e('View this in the sidebar (custom sidebar only).', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to move the Report Ad, View All, Favourites, Watchlist and Send PM menu bar from the above ad content to the custom sidebar.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">
						<th scope="row"><?php _e('Enable Contact Options:', 'auctionAdmin'); ?></th>
   						<td><input type="checkbox" name="classified" value="classified" <?php if( $list ) { if( in_array("classified", $list )) echo 'checked="checked"'; } ?>> <?php _e('Classified Ads.', 'auctionAdmin'); ?><br/>
   						<input type="checkbox" name="wanted" value="wanted" <?php if( $list ) { if( in_array("wanted", $list )) echo 'checked="checked"'; } ?>> <?php _e('Wanted Ads.', 'auctionAdmin'); ?><br/>
   						<input type="checkbox" name="normal" value="normal" <?php if( $list ) { if( in_array("normal", $list )) echo 'checked="checked"'; } ?>> <?php _e('Auction Ads.', 'auctionAdmin'); ?><br/><small><?php _e('Select the type of ads that should have the contact options available.', 'auctionAdmin'); ?></small></td>
    					</tr>


					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Contact Info:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_pull_fields"><option value="profile" <?php if(get_option('cp_auction_pull_fields') == 'profile') echo 'selected="selected"'; ?>><?php _e('Profile Fields', 'auctionAdmin'); ?></option>
					      <option value="custom" <?php if(get_option('cp_auction_pull_fields') == 'custom') echo 'selected="selected"'; ?>><?php _e('Custom Fields', 'auctionAdmin'); ?></option></select> <?php _e('Custom fields or profile fields (custom sidebar only).', 'auctionAdmin'); ?><br /><small><?php _e('Set from where the contact information such as email and phone should be pulled from, if custom fields is selected author will have the option to enter their email and phone during the post an ad process, otherwise it is pulled from users profile settings.', 'auctionAdmin'); ?></small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

	<p class="submit"><input type="submit" name="save_ad_page" id="submit" class="button-primary" value="<?php _e('Save Single Ad Settings', 'auctionAdmin'); ?>"  /></p>

		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Set the position / float for the buy now button', 'auctionAdmin'); ?></div>
	<?php _e('Use the options below to set the position / float, right, center or left for the buy now button.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Buy Now Button Std.:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_buynow_button_float">
						<option value="" <?php if(get_option('cp_auction_buynow_button_float') == '') echo 'selected="selected"'; ?>><?php _e('Right', 'auctionAdmin'); ?></option>
						<option value="left" <?php if(get_option('cp_auction_buynow_button_float') == 'left') echo 'selected="selected"'; ?>><?php _e('Left', 'auctionAdmin'); ?></option>
						<option value="center" <?php if(get_option('cp_auction_buynow_button_float') == 'center') echo 'selected="selected"'; ?>><?php _e('Center', 'auctionAdmin'); ?></option>
						</select> <?php _e('Buy now button, float center, left or right.', 'auctionAdmin'); ?></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Buy Now Button Shortcode:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_buynow_button_float_short">
						<option value="" <?php if(get_option('cp_auction_buynow_button_float_short') == '') echo 'selected="selected"'; ?>><?php _e('Right', 'auctionAdmin'); ?></option>
						<option value="left" <?php if(get_option('cp_auction_buynow_button_float_short') == 'left') echo 'selected="selected"'; ?>><?php _e('Left', 'auctionAdmin'); ?></option>
						<option value="center" <?php if(get_option('cp_auction_buynow_button_float_short') == 'center') echo 'selected="selected"'; ?>><?php _e('Center', 'auctionAdmin'); ?></option>
						</select> <?php _e('Buy now button shortcode, float center, left or right.', 'auctionAdmin'); ?></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

	<p class="submit"><input type="submit" name="save_button_pos" id="submit" class="button-primary" value="<?php _e('Save Placement', 'auctionAdmin'); ?>"  /></p>

		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Deny Access to Pages of Your Choice', 'auctionAdmin'); ?></div>
	<?php _e('Use the options below to deny access to pages of your choice on your website if the visitor is not logged in.', 'auctionAdmin'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Single Ad Page:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_plugin_access_single"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_access_single')); ?></select> <?php _e('Deny access to the single ad page if the guest are not logged in.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to deny access to the single ad page if the guest are not logged in. This option will deny access to all posts.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Pages / Posts by ID:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_plugin_access_pages" value="<?php echo get_option('cp_auction_plugin_access_pages'); ?>"> <?php _e('Enter the Page/Post IDs as comma separated list, eg. 9,10,14', 'auctionAdmin'); ?><br /><small><?php _e('This option is used to deny access to any standard Page or Post, by Page/Post ID.', 'auctionAdmin'); ?> <?php _e('Enter any page/post id wich you want to deny access to if the visitor is not logged in.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Page Title:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_plugin_access_title" value="<?php echo get_option('cp_auction_plugin_access_title'); ?>" size="40"> <?php _e('Enter a title for the page which visitors are redirected to.', 'auctionAdmin'); ?><br /><small><?php _e('Enter your choice of title for the page which visitors are redirected to if not logged in.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Page Information:', 'auctionAdmin'); ?></th>
						<td><textarea class="options" name="cp_auction_plugin_access_info" rows="5" cols="80"><?php echo get_option('cp_auction_plugin_access_info'); ?></textarea><br /><small><?php _e('Enter an explanatory text that will be visible on the login page. HTML can be used.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Remove Login Form:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_plugin_disable_alogin"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_disable_alogin')); ?></select> <?php _e('Remove the login / register form on the no access page.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to remove the login / register form from the no access page.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable Hooks:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_plugin_enable_ahooks"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_enable_ahooks')); ?></select> <?php _e('To make use of blocking on pages without standard hooks.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to use the below hook, put the hook just below the <strong>content_res</strong> class within the page / template code.', 'auctionAdmin'); ?></small>

	<div class="clear10"></div>

<small><strong><?php _e('Use this code with sidebar pages', 'auctionAdmin'); ?></strong>:</small>

<div class="code_box">
	&lt;?php if ( function_exists('cp_auction_block_access') ) cp_auction_block_access(); ?&gt;
	</div>

	<div class="clear20"></div>

<small><strong><?php _e('Use this code with fullwidth pages', 'auctionAdmin'); ?></strong>:</small>

<div class="code_box">
	&lt;?php if ( get_option('cp_auction_plugin_enable_ahooks') == "yes" && function_exists('cp_auction_block_access') ) { ?&gt;<br />
		&lt;div class="content_left"&gt;<br />
		&lt;?php cp_auction_block_access(); } ?&gt;
	</div>

	<div class="clear10"></div>

						</td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

	<strong><?php _e('Here is how you can find the Page / Post IDs', 'auctionAdmin'); ?></strong><br />
	<?php _e('After you have logged into your WordPress dashboard, Go to Manage > Pages or Ads and from the list of Pages / Ads hover over the Page / Ad title you want to find the ID of. Every time you hover over, your browsers status bar will show you a URL ending with a number like this http://www.your-domain.com/wp-admin/post.php?post=3648&action=edit. The number with which is in the URL is the Page / Post ID (3648 in this case).', 'auctionAdmin'); ?>
<br /><br />
	<?php _e('If you dont see anything when you hover over, click on the Page link to open edit Page screen. Now, look in your browsers address bar and you will find the URL with a number. and that number is your Page / Post ID.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<p class="submit"><input type="submit" name="save_access_info" id="submit" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>"  /></p>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

<div class="admin_header"><?php _e('Deny Access by User Roles', 'auctionAdmin'); ?></div>
<?php _e('Use the options below to deny access to the different pages on your site by user roles.', 'auctionAdmin'); ?>

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('User Roles:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_blocked_roles"><option value=""><?php _e('Select Role', 'auctionAdmin'); ?></option><?php wp_dropdown_roles(get_option('cp_auction_blocked_roles')); ?></select> <?php _e('Select the user role that you want to deny access.', 'auctionAdmin'); ?><br /><small><?php _e('Select the user role that you want to deny access.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Pages / Posts by ID:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_plugin_access_posts" value="<?php echo get_option('cp_auction_plugin_access_posts'); ?>"> <?php _e('Enter the Page/Post IDs as comma separated list, eg. 9,10,14', 'auctionAdmin'); ?><br /><small><?php _e('This option is used to deny access to any standard Page or Post, by Page/Post ID.', 'auctionAdmin'); ?> <?php _e('Enter any page/post id wich you want to deny access to on your site. Block users by user roles.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Page Title:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_plugin_roles_title" value="<?php echo get_option('cp_auction_plugin_roles_title'); ?>" size="40"> <?php _e('Enter a title for the page which users are redirected to.', 'auctionAdmin'); ?><br /><small><?php _e('Enter your choice of title for the page which users are redirected to if trying to access a given page or post.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Page Information:', 'auctionAdmin'); ?></th>
						<td><textarea class="options" name="cp_auction_plugin_roles_info" rows="5" cols="80"><?php echo get_option('cp_auction_plugin_roles_info'); ?></textarea><br><small><?php _e('Enter an explanatory text that will be visible on the login page. HTML can be used.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Remove Login Form:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_plugin_disable_ilogin"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_disable_ilogin')); ?></select> <?php _e('Remove the login / register form on the no access page.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to remove the login / register form from the no access page.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable Hooks:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_plugin_enable_ihooks"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_enable_ihooks')); ?></select> <?php _e('To make use of blocking on pages without standard hooks.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to use the below hook, put the hook just below the <strong>content_res</strong> class within the page / template code.', 'auctionAdmin'); ?></small>

	<div class="clear10"></div>

<small><strong><?php _e('Use this code with sidebar pages', 'auctionAdmin'); ?></strong>:</small>

<div class="code_box">
	&lt;?php if ( function_exists('cp_auction_block_access') ) cp_auction_block_access(); ?&gt;
	</div>

	<div class="clear20"></div>

<small><strong><?php _e('Use this code with fullwidth pages', 'auctionAdmin'); ?></strong>:</small>

<div class="code_box">
	&lt;?php if ( get_option('cp_auction_plugin_enable_ihooks') == "yes" && function_exists('cp_auction_block_access') ) { ?&gt;<br />
		&lt;div class="content_left"&gt;<br />
		&lt;?php cp_auction_block_access(); } ?&gt;
	</div>

	<div class="clear10"></div>

						</td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

	<strong><?php _e('Here is how you can find the Page / Post IDs', 'auctionAdmin'); ?></strong><br />
	<?php _e('After you have logged into your WordPress dashboard, Go to Manage > Pages or Ads and from the list of Pages / Ads hover over the Page / Ad title you want to find the ID of. Every time you hover over, your browsers status bar will show you a URL ending with a number like this http://www.your-domain.com/wp-admin/post.php?post=3648&action=edit. The number with which is in the URL is the Page / Post ID (3648 in this case).', 'auctionAdmin'); ?>
<br /><br />
	<?php _e('If you dont see anything when you hover over, click on the Page link to open edit Page screen. Now, look in your browsers address bar and you will find the URL with a number. and that number is your Page / Post ID.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<p class="submit"><input type="submit" name="save_access_info" id="submit" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>"  /></p>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

<div class="admin_header"><?php _e('Block IP Addresses from Reaching Your Site', 'auctionAdmin'); ?></div>
<?php _e('Use the options below to block any IP address of your choice.', 'auctionAdmin'); ?>

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Block IP Addresses:', 'auctionAdmin'); ?></th>
						<td><textarea class="options" name="cp_auction_blocked_ips" rows="5" cols="80"><?php echo get_option('cp_auction_blocked_ips'); ?></textarea><br /><small><?php _e('Enter the IP addresses in a line separated with comma, eg. 111.111.111,222.222.222,333.333.333', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('URL Forwarding:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_blocked_redirect_url" value="<?php echo get_option('cp_auction_blocked_redirect_url'); ?>" size="40"> <?php _e('Full URL to which you want to forward blocked visits.', 'auctionAdmin'); ?><br /><small><?php _e('Enter a complete URL to which you want to forward those who have had their IP address blocked', 'auctionAdmin'); ?>, eg. http://www.google.com/ (<?php _e('<strong>IMPORTANT</strong> Do NOT use any URL within your ClassiPress website.', 'auctionAdmin'); ?>)</small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

	<p class="submit"><input type="submit" name="save_access_info" id="submit" class="button-primary" value="<?php _e('Save Settings', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Add / edit information on the Select Ad Type page', 'auctionAdmin'); ?></div>
	<?php _e('Use the below textarea to add any information to the new select ad type page (Post an Ad page, Step 1).', 'auctionAdmin'); ?>

	<div class="clear10"></div>	

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Information Text:', 'auctionAdmin'); ?></th>
						<td><textarea class="options" name="cp_auction_plugin_page_info" rows="5" cols="80"><?php echo get_option('cp_auction_plugin_page_info'); ?></textarea><br /><small><?php _e('Enter some information for the author here. HTML can be used. Leave blank to disable.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="save_page_info" id="submit" class="button-primary" value="<?php _e('Save Info', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Add / edit information on Upload Files page', 'auctionAdmin'); ?></div>
	<?php _e('Use the below textarea to add any information to the Upload Files page.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Information Text:', 'auctionAdmin'); ?></th>
						<td><textarea class="options" name="cp_auction_upload_page_info" rows="5" cols="80"><?php echo get_option('cp_auction_upload_page_info'); ?></textarea><br /><small><?php _e('Enter some information for the author here. HTML can be used. Leave blank to disable.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="save_upload_page_info" id="submit" class="button-primary" value="<?php _e('Save Info', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Add / edit information on the Dashboard page', 'auctionAdmin'); ?></div>
	<?php _e('Use the below textarea to add any information to the Dashboard page.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Information Text:', 'auctionAdmin'); ?></th>
						<td><textarea class="options" name="cp_auction_dashboard_page_info" rows="5" cols="80"><?php echo get_option('cp_auction_dashboard_page_info'); ?></textarea><br /><small><?php _e('Enter some information for the author here. HTML can be used. Leave blank to disable.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="save_dashboard_page_info" id="submit" class="button-primary" value="<?php _e('Save Info', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Display a link to eg. any post an ad information', 'auctionAdmin'); ?></div>
	<?php _e('If you have an information page that you wish to link to from the "Post an Ad" page then you can do this by typing the complete URL and any text to follow the link, in the textfields below.', 'auctionAdmin'); ?>

	<div class="clear10"></div>

	<?php _e('The link will be placed at the top of the page and look like this: <a style="text-decoration: none;" href="#">Click here</a> your info text goes here.', 'auctionAdmin'); ?>

	<div class="clear10"></div>	

<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('The Complete URL:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_plugin_adm_link" value="<?php echo get_option( 'cp_auction_plugin_adm_link' ); ?>" size="50"><br /><small><?php _e('Enter the complete URL including http://. Leave blank to deactivate.', 'auctionAdmin'); ?></small></td>
					</tr>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Following Text:', 'auctionAdmin'); ?></th>
						<td><input type="text" name="cp_auction_plugin_adm_info" value="<?php echo get_option( 'cp_auction_plugin_adm_info' ); ?>" size="50"> <?php _e('eg. and learn how to do an auction winner.', 'auctionAdmin'); ?><br /><small><?php _e('Enter a short text to follow the clik here link. Leave blank to deactivate.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="save_adm_info" id="submit" class="button-primary" value="<?php _e('Save Info', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Delete plugin data upon plugin deletion', 'auctionAdmin'); ?></div>
	<?php _e('DISCLAIMER: If for any reason your data is lost, damaged or otherwise becomes unusable in any way or by any means in whichever way I will not take responsibility. You should always have a backup of your database. Use with caution and make a backup first!', 'auctionAdmin'); ?>
		
		<div class="clear10"></div>

		<form method="post" action="">

			<table class="form-table">
				<tbody>
					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Delete all plugin data:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_plugin_uninstall"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_uninstall')); ?></select><br /><small><?php _e('Enable to totally delete plugin, database tables, pages, and plugin settings upon plugin deletion.', 'auctionAdmin'); ?></small></td>
					</tr>
				</tbody>
			</table>	
			<p class="submit"><input type="submit" name="save_uninstall" id="submit" class="button-primary" value="<?php _e('Save Changes', 'auctionAdmin'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Reset the database tables before going online with your site', 'auctionAdmin'); ?></div>
	<?php _e('Click the <strong>Reset Auction Tables</strong> button below if you have done eg. test transactions and deleted ads. This operation will clear the database table(s) that may include transactions, purchases, reviews, coupons, and/or bids related to the deleted ads. <strong>Will only delete table rows without relation to an ad.</strong>', 'auctionAdmin'); ?>

    <form method="post" action="">
    <p class="submit"><input type="submit" value="<?php _e('Reset CP Auction Tables', 'auctionAdmin'); ?>" name="empty_tables" class="button-primary"/></p>
    </form>

	<div class="clear10"></div>

	<div class="dotted"></div>

	<div class="clear10"></div>

	<div class="admin_header"><?php _e('Totally empty the database tables before going online with your site', 'auctionAdmin'); ?></div>
	<?php _e('Click the <strong>Empty Auction Tables</strong> button below to TOTALLY empty all auction plugin tables. <strong>Never use this option on a production site unless you know what you are doing</strong>.', 'auctionAdmin'); ?>

    <form method="post" action="">
    <p class="submit"><input type="submit" value="<?php _e('Empty CP Auction Tables', 'auctionAdmin'); ?>" name="total_empty_tables" class="button-primary"/></p>
    </form>

	<div class="clear10"></div>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}