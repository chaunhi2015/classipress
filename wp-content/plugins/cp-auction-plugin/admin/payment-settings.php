<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_plugin_payment_settings() {
	global $cp_options;

		$msg = "";

		if( isset($_POST['save']) ) {

		$disable_settings = isset( $_POST['cp_auction_disable_settings'] ) ? esc_attr( $_POST['cp_auction_disable_settings'] ) : '';
		$pay_mode = isset( $_POST['cp_auction_payment_mode'] ) ? esc_attr( $_POST['cp_auction_payment_mode'] ) : '';
		$users_mode = isset( $_POST['cp_auction_users_payment_mode'] ) ? esc_attr( $_POST['cp_auction_users_payment_mode'] ) : '';
		$simple_settings = ""; if( isset( $_POST['opt'] ) ) $simple_settings = join(",", $_POST['opt']);
		$cih = isset( $_POST['cp_auction_plugin_cih'] ) ? esc_attr( $_POST['cp_auction_plugin_cih'] ) : '';
		$cod = isset( $_POST['cp_auction_plugin_cod'] ) ? esc_attr( $_POST['cp_auction_plugin_cod'] ) : '';
		$cod_fee_type = isset( $_POST['cp_auction_cod_fee_type'] ) ? esc_attr( $_POST['cp_auction_cod_fee_type'] ) : '';
		$cod_fee = isset( $_POST['cp_auction_cod_fee'] ) ? esc_attr( $_POST['cp_auction_cod_fee'] ) : '';
		$paypalemail = isset( $_POST['cp_auction_plugin_paypalemail'] ) ? esc_attr( $_POST['cp_auction_plugin_paypalemail'] ) : '';
		$currency = ""; if( isset( $_POST['currency'] ) ) $currency = join(",", $_POST['currency']);
		$language = ""; if( isset( $_POST['lang'] ) ) $language = join(",", $_POST['lang']);
		$bank_transfer = isset( $_POST['cp_auction_plugin_bank_transfer'] ) ? esc_attr( $_POST['cp_auction_plugin_bank_transfer'] ) : '';
		$bank_info = isset( $_POST['cp_auction_plugin_bank_info'] ) ? esc_attr( $_POST['cp_auction_plugin_bank_info'] ) : '';
		$sandbox = isset( $_POST['cp_auction_plugin_sandbox'] ) ? esc_attr( $_POST['cp_auction_plugin_sandbox'] ) : '';
		$credit_value = isset( $_POST['cp_auction_plugin_credit_value'] ) ? esc_attr( $_POST['cp_auction_plugin_credit_value'] ) : '';
		$credits = isset( $_POST['cp_auction_plugin_credits'] ) ? esc_attr( $_POST['cp_auction_plugin_credits'] ) : '';
		$credits_fee = isset( $_POST['cp_auction_plugin_credits_fee'] ) ? esc_attr( $_POST['cp_auction_plugin_credits_fee'] ) : '';
		$paypal = isset( $_POST['cp_auction_plugin_paypal'] ) ? esc_attr( $_POST['cp_auction_plugin_paypal'] ) : '';
		$add_post_credits = isset( $_POST['cp_auction_plugin_credits_on_posting'] ) ? esc_attr( $_POST['cp_auction_plugin_credits_on_posting'] ) : '';
		$pay_admin = isset( $_POST['cp_auction_plugin_pay_admin'] ) ? esc_attr( $_POST['cp_auction_plugin_pay_admin'] ) : '';
		$pay_user = isset( $_POST['cp_auction_plugin_pay_user'] ) ? esc_attr( $_POST['cp_auction_plugin_pay_user'] ) : '';
		$escrow_info = isset( $_POST['cp_auction_plugin_escrow_info'] ) ? esc_attr( $_POST['cp_auction_plugin_escrow_info'] ) : '';
		$bt_fee = isset( $_POST['cp_auction_plugin_bt_fee'] ) ? esc_attr( $_POST['cp_auction_plugin_bt_fee'] ) : '';
		$credits_escrow = isset( $_POST['cp_auction_plugin_credits_escrow'] ) ? esc_attr( $_POST['cp_auction_plugin_credits_escrow'] ) : '';
		$esc_credits_fee = isset( $_POST['cp_auction_plugin_esc_credits_fee'] ) ? esc_attr( $_POST['cp_auction_plugin_esc_credits_fee'] ) : '';
		$pay_escrow_credits = isset( $_POST['cp_auction_plugin_pay_escrow_credits'] ) ? esc_attr( $_POST['cp_auction_plugin_pay_escrow_credits'] ) : '';
		$esc_bt_fee = isset( $_POST['cp_auction_plugin_esc_bt_fee'] ) ? esc_attr( $_POST['cp_auction_plugin_esc_bt_fee'] ) : '';
		$pay_bt_escrow = isset( $_POST['cp_auction_plugin_pay_bt_escrow'] ) ? esc_attr( $_POST['cp_auction_plugin_pay_bt_escrow'] ) : '';
		$esc_bank_transfer = isset( $_POST['cp_auction_plugin_esc_bank_transfer'] ) ? esc_attr( $_POST['cp_auction_plugin_esc_bank_transfer'] ) : '';
		$pp_pdt = isset( $_POST['cp_auction_plugin_pp_pdt'] ) ? esc_attr( $_POST['cp_auction_plugin_pp_pdt'] ) : '';
		$cre_fee_type = isset( $_POST['cp_auction_credits_fee_type'] ) ? esc_attr( $_POST['cp_auction_credits_fee_type'] ) : '';
		$bt_fee_type = isset( $_POST['cp_auction_banktransfer_fee_type'] ) ? esc_attr( $_POST['cp_auction_banktransfer_fee_type'] ) : '';
		$cre_escrow_fee_type = isset( $_POST['cp_auction_cre_escrow_fee_type'] ) ? esc_attr( $_POST['cp_auction_cre_escrow_fee_type'] ) : '';
		$bt_escrow_fee_type = isset( $_POST['cp_auction_bt_escrow_fee_type'] ) ? esc_attr( $_POST['cp_auction_bt_escrow_fee_type'] ) : '';
		$identity_token = isset( $_POST['cp_auction_identity_token'] ) ? esc_attr( $_POST['cp_auction_identity_token'] ) : '';
		$button	= isset( $_POST['cp_auction_plugin_button'] ) ? esc_attr( $_POST['cp_auction_plugin_button'] ) : '';
		$button_text = isset( $_POST['cp_auction_button_text'] ) ? esc_attr( $_POST['cp_auction_button_text'] ) : '';
		$paypal_direct = isset( $_POST['cp_auction_enable_paypal_direct'] ) ? esc_attr( $_POST['cp_auction_enable_paypal_direct'] ) : '';
		$min_withdraw = isset( $_POST['cp_auction_minimum_withdraw'] ) ? esc_attr( $_POST['cp_auction_minimum_withdraw'] ) : '';
		$pp_withdraw_credits = isset( $_POST['cp_auction_paypal_withdraw_credits'] ) ? esc_attr( $_POST['cp_auction_paypal_withdraw_credits'] ) : '';
		$withdraw_fee_paypal = isset( $_POST['cp_auction_withdraw_fee_paypal'] ) ? esc_attr( $_POST['cp_auction_withdraw_fee_paypal'] ) : '';
		$bt_withdraw_credits = isset( $_POST['cp_auction_bt_withdraw_credits'] ) ? esc_attr( $_POST['cp_auction_bt_withdraw_credits'] ) : '';
		$withdraw_fee_bt = isset( $_POST['cp_auction_withdraw_fee_bt'] ) ? esc_attr( $_POST['cp_auction_withdraw_fee_bt'] ) : '';
		$payout_info = isset( $_POST['cp_auction_payout_info'] ) ? esc_attr( $_POST['cp_auction_payout_info'] ) : '';
		$paypal_fee_type = isset( $_POST['cp_auction_paypal_fee_type'] ) ? esc_attr( $_POST['cp_auction_paypal_fee_type'] ) : '';
		$paypal_fee = isset( $_POST['cp_auction_paypal_fee'] ) ? esc_attr( $_POST['cp_auction_paypal_fee'] ) : '';
		$paypal_release = isset( $_POST['cp_auction_paypal_release'] ) ? esc_attr( $_POST['cp_auction_paypal_release'] ) : '';
		$paypal_approves = isset( $_POST['cp_auction_paypal_approves'] ) ? esc_attr( $_POST['cp_auction_paypal_approves'] ) : '';
		$credit_approves = isset( $_POST['cp_auction_credit_approves'] ) ? esc_attr( $_POST['cp_auction_credit_approves'] ) : '';
		$paypal_lc = isset( $_POST['cp_auction_paypal_lc'] ) ? esc_attr( $_POST['cp_auction_paypal_lc'] ) : '';
		$paypal_debugger = isset( $_POST['cp_auction_paypal_debugger'] ) ? esc_attr( $_POST['cp_auction_paypal_debugger'] ) : '';
		$actfee = isset( $_POST['cp_auction_plugin_actfee'] ) ? esc_attr( $_POST['cp_auction_plugin_actfee'] ) : '';

		if( $disable_settings == "yes" ) {
		update_option('cp_auction_disable_settings',$disable_settings);
		} else {
		delete_option('cp_auction_disable_settings');
		}
		if( $pay_mode ) {
		update_option('cp_auction_payment_mode',$pay_mode);
		} else {
		delete_option('cp_auction_payment_mode');
		}
		if( $users_mode == "yes" ) {
		update_option('cp_auction_users_payment_mode',$users_mode);
		} else {
		delete_option('cp_auction_users_payment_mode');
		}
		if( $simple_settings ) {
		update_option('cp_auction_simple_settings',$simple_settings);
		} else {
		delete_option('cp_auction_simple_settings');
		}
		if( $cih == "yes" ) {
		update_option('cp_auction_plugin_cih',$cih);
		} else {
		delete_option('cp_auction_plugin_cih');
		}
		if( $cod == "yes" ) {
		update_option('cp_auction_plugin_cod',$cod);
		} else {
		delete_option('cp_auction_plugin_cod');
		}
		if( $cod_fee_type ) {
		update_option('cp_auction_cod_fee_type',$cod_fee_type);
		} else {
		delete_option('cp_auction_cod_fee_type');
		}
		if( $cod_fee ) {
		update_option('cp_auction_cod_fee',$cod_fee);
		} else {
		delete_option('cp_auction_cod_fee');
		}
		if( $button ) {
		update_option('cp_auction_plugin_button',$button);
		} else {
		delete_option('cp_auction_plugin_button');
		}
		if( $button_text ) {
		update_option( 'cp_auction_button_text', $button_text );
		} else {
		delete_option( 'cp_auction_button_text' );
		}
		if( $identity_token ) {
		update_option('cp_auction_identity_token',$identity_token);
		} else {
		delete_option('cp_auction_identity_token');
		}
		if( $cre_fee_type ) {
		update_option('cp_auction_credits_fee_type',$cre_fee_type);
		} else {
		delete_option('cp_auction_credits_fee_type');
		}
		if( $bt_fee_type ) {
		update_option('cp_auction_banktransfer_fee_type',$bt_fee_type);
		} else {
		delete_option('cp_auction_banktransfer_fee_type');
		}
		if( $cre_escrow_fee_type ) {
		update_option('cp_auction_cre_escrow_fee_type',$cre_escrow_fee_type);
		} else {
		delete_option('cp_auction_cre_escrow_fee_type');
		}
		if( $bt_escrow_fee_type ) {
		update_option('cp_auction_bt_escrow_fee_type',$bt_escrow_fee_type);
		} else {
		delete_option('cp_auction_bt_escrow_fee_type');
		}
		if( $pp_pdt == "yes" ) {
		update_option('cp_auction_plugin_pp_pdt',$pp_pdt);
		} else {
		delete_option('cp_auction_plugin_pp_pdt');
		}
		if( $esc_bt_fee ) {
		update_option('cp_auction_plugin_esc_bt_fee',cp_auction_sanitize_amount($esc_bt_fee));
		} else {
		delete_option('cp_auction_plugin_esc_bt_fee');
		}
		if( $pay_bt_escrow ) {
		update_option('cp_auction_plugin_pay_bt_escrow',$pay_bt_escrow);
		} else {
		delete_option('cp_auction_plugin_pay_bt_escrow');
		}
		if( $esc_bank_transfer == "yes" ) {
		update_option('cp_auction_plugin_esc_bank_transfer',$esc_bank_transfer);
		} else {
		delete_option('cp_auction_plugin_esc_bank_transfer');
		}
		if( $pay_escrow_credits ) {
		update_option('cp_auction_plugin_pay_escrow_credits',$pay_escrow_credits);
		} else {
		delete_option('cp_auction_plugin_pay_escrow_credits');
		}
		if( $credits_fee ) {
		update_option('cp_auction_plugin_credits_fee',cp_auction_sanitize_amount($credits_fee));
		} else {
		delete_option('cp_auction_plugin_credits_fee');
		}
		if( $esc_credits_fee ) {
		update_option('cp_auction_plugin_esc_credits_fee',cp_auction_sanitize_amount($esc_credits_fee));
		} else {
		delete_option('cp_auction_plugin_esc_credits_fee');
		}
		if( $credits_escrow == "yes" ) {
		update_option('cp_auction_plugin_credits_escrow',$credits_escrow);
		} else {
		delete_option('cp_auction_plugin_credits_escrow');
		}
		if( $bt_fee ) {
		update_option('cp_auction_plugin_bt_fee',cp_auction_sanitize_amount($bt_fee));
		} else {
		delete_option('cp_auction_plugin_bt_fee');
		}
		if( $escrow_info ) {
		update_option('cp_auction_plugin_escrow_info',html_entity_decode($escrow_info));
		} else {
		delete_option('cp_auction_plugin_escrow_info');
		}
		if( $pay_user == "yes" ) {
		update_option('cp_auction_plugin_pay_user',$pay_user);
		} else if( $pay_user == "no" ) {
		update_option('cp_auction_plugin_pay_user',$pay_user);
		} else {
		delete_option('cp_auction_plugin_pay_user');
		}
		if( $pay_admin == "yes" ) {
		update_option('cp_auction_plugin_pay_admin',$pay_admin);
		} else {
		delete_option('cp_auction_plugin_pay_admin');
		}
		if( $add_post_credits ) {
		update_option('cp_auction_plugin_credits_on_posting',cp_auction_sanitize_amount($add_post_credits));
		} else {
		delete_option('cp_auction_plugin_credits_on_posting');
		}
		if( $paypal == "yes" ) {
		update_option('cp_auction_plugin_paypal',$paypal);
		} else {
		delete_option('cp_auction_plugin_paypal');
		}
		if( $credit_value ) {
		update_option('cp_auction_plugin_credit_value',cp_auction_sanitize_amount($credit_value));
		} else {
		delete_option('cp_auction_plugin_credit_value');
		}
		if( $credits == "yes" ) {
		update_option('cp_auction_plugin_credits',$credits);
		} else {
		delete_option('cp_auction_plugin_credits');
		}
		if( $sandbox == "yes" ) {
		update_option('cp_auction_plugin_sandbox',$sandbox);
		} else {
		delete_option('cp_auction_plugin_sandbox');
		}
		if( $bank_info ) {
		update_option('cp_auction_plugin_bank_info',html_entity_decode($bank_info));
		} else {
		delete_option('cp_auction_plugin_bank_info');
		}
		if( $bank_transfer == "yes" ) {
		update_option('cp_auction_plugin_bank_transfer',$bank_transfer);
		} else {
		delete_option('cp_auction_plugin_bank_transfer');
		}
		if( $paypalemail ) {
		update_option('cp_auction_plugin_paypalemail',$paypalemail);
		} else {
		delete_option('cp_auction_plugin_paypalemail');
		}
		if( $currency ) {
		update_option('cp_auction_users_currency',$currency);
		} else {
		delete_option('cp_auction_users_currency');
		}
		if( $language ) {
		update_option('cp_auction_users_paypal_lang',$language);
		} else {
		delete_option('cp_auction_users_paypal_lang');
		}
		if( $paypal_direct == "yes" ) {
		update_option('cp_auction_enable_paypal_direct',$paypal_direct);
		} else {
		delete_option('cp_auction_enable_paypal_direct');
		}
		if( $min_withdraw ) {
		update_option('cp_auction_minimum_withdraw',$min_withdraw);
		} else {
		delete_option('cp_auction_minimum_withdraw');
		}
		if( $pp_withdraw_credits ) {
		update_option('cp_auction_paypal_withdraw_credits',$pp_withdraw_credits);
		} else {
		delete_option('cp_auction_paypal_withdraw_credits');
		}
		if( $withdraw_fee_paypal ) {
		update_option('cp_auction_withdraw_fee_paypal',$withdraw_fee_paypal);
		} else {
		delete_option('cp_auction_withdraw_fee_paypal');
		}
		if( $bt_withdraw_credits ) {
		update_option('cp_auction_bt_withdraw_credits',$bt_withdraw_credits);
		} else {
		delete_option('cp_auction_bt_withdraw_credits');
		}
		if( $withdraw_fee_bt ) {
		update_option('cp_auction_withdraw_fee_bt',$withdraw_fee_bt);
		} else {
		delete_option('cp_auction_withdraw_fee_bt');
		}
		if( $payout_info ) {
		update_option('cp_auction_payout_info',html_entity_decode($payout_info));
		} else {
		delete_option('cp_auction_payout_info');
		}
		if( $paypal_fee_type ) {
		update_option('cp_auction_paypal_fee_type',$paypal_fee_type);
		} else {
		delete_option('cp_auction_paypal_fee_type');
		}
		if( $paypal_fee ) {
		update_option('cp_auction_paypal_fee',$paypal_fee);
		} else {
		delete_option('cp_auction_paypal_fee');
		}
		if( $paypal_release == "yes" ) {
		update_option('cp_auction_paypal_release',$paypal_release);
		} else {
		delete_option('cp_auction_paypal_release');
		}
		if( $paypal_approves == "yes" ) {
		update_option('cp_auction_paypal_approves',$paypal_approves);
		} else {
		delete_option('cp_auction_paypal_approves');
		}
		if( $credit_approves == "yes" ) {
		update_option('cp_auction_credit_approves',$credit_approves);
		} else {
		delete_option('cp_auction_credit_approves');
		}
		if( $paypal_lc ) {
		update_option('cp_auction_paypal_lc',$paypal_lc);
		} else {
		delete_option('cp_auction_paypal_lc');
		}
		if( $paypal_debugger == "yes" ) {
		update_option('cp_auction_paypal_debugger',$paypal_debugger);
		} else {
		delete_option('cp_auction_paypal_debugger');
		}
		if( $actfee ) {
		update_option('cp_auction_plugin_actfee',cp_auction_sanitize_amount($actfee));
		} else {
		delete_option('cp_auction_plugin_actfee');
		}

	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Settings was saved!','auctionAdmin').'</strong></p></div>';
}

		if( isset($_POST['save_adaptive']) ) {

		$sand_email	= isset( $_POST['cp_auction_plugin_sand_email'] ) ? esc_attr( $_POST['cp_auction_plugin_sand_email'] ) : '';
		$admin_sand_email	= isset( $_POST['cp_auction_plugin_admin_sand_email'] ) ? esc_attr( $_POST['cp_auction_plugin_admin_sand_email'] ) : '';
		$seller_sand_email	= isset( $_POST['cp_auction_plugin_seller_sand_email'] ) ? esc_attr( $_POST['cp_auction_plugin_seller_sand_email'] ) : '';
		$buyer_sand_email	= isset( $_POST['cp_auction_plugin_buyer_sand_email'] ) ? esc_attr( $_POST['cp_auction_plugin_buyer_sand_email'] ) : '';
		$sand_signature = isset( $_POST['cp_auction_plugin_sand_signature'] ) ? esc_attr( $_POST['cp_auction_plugin_sand_signature'] ) : '';
		$sand_apipass	= isset( $_POST['cp_auction_plugin_sand_apipass'] ) ? esc_attr( $_POST['cp_auction_plugin_sand_apipass'] ) : '';
		$sand_apiuser	= isset( $_POST['cp_auction_plugin_sand_apiuser'] ) ? esc_attr( $_POST['cp_auction_plugin_sand_apiuser'] ) : '';
		$sand_appid	= isset( $_POST['cp_auction_plugin_sand_appid'] ) ? esc_attr( $_POST['cp_auction_plugin_sand_appid'] ) : '';
		$signature	= isset( $_POST['cp_auction_plugin_signature'] ) ? esc_attr( $_POST['cp_auction_plugin_signature'] ) : '';
		$apipass	= isset( $_POST['cp_auction_plugin_apipass'] ) ? esc_attr( $_POST['cp_auction_plugin_apipass'] ) : '';
		$apiuser	= isset( $_POST['cp_auction_plugin_apiuser'] ) ? esc_attr( $_POST['cp_auction_plugin_apiuser'] ) : '';
		$appid		= isset( $_POST['cp_auction_plugin_appid'] ) ? esc_attr( $_POST['cp_auction_plugin_appid'] ) : '';
		$debug		= isset( $_POST['cp_auction_plugin_sand_debug'] ) ? esc_attr( $_POST['cp_auction_plugin_sand_debug'] ) : '';
		$echeck		= isset( $_POST['cp_auction_plugin_echeck'] ) ? esc_attr( $_POST['cp_auction_plugin_echeck'] ) : '';
		$balance	= isset( $_POST['cp_auction_plugin_balance'] ) ? esc_attr( $_POST['cp_auction_plugin_balance'] ) : '';
		$creditcard	= isset( $_POST['cp_auction_plugin_creditcard'] ) ? esc_attr( $_POST['cp_auction_plugin_creditcard'] ) : '';
		$echeck_sand	= isset( $_POST['cp_auction_plugin_echeck_sand'] ) ? esc_attr( $_POST['cp_auction_plugin_echeck_sand'] ) : '';
		$balance_sand	= isset( $_POST['cp_auction_plugin_balance_sand'] ) ? esc_attr( $_POST['cp_auction_plugin_balance_sand'] ) : '';
		$creditcard_sand	= isset( $_POST['cp_auction_plugin_creditcard_sand'] ) ? esc_attr( $_POST['cp_auction_plugin_creditcard_sand'] ) : '';

		if( $echeck ) {
		update_option('cp_auction_plugin_echeck',$echeck);
		} else {
		delete_option('cp_auction_plugin_echeck');
		}
		if( $balance ) {
		update_option('cp_auction_plugin_balance',$balance);
		} else {
		delete_option('cp_auction_plugin_balance');
		}
		if( $creditcard ) {
		update_option('cp_auction_plugin_creditcard',$creditcard);
		} else {
		delete_option('cp_auction_plugin_creditcard');
		}
		if( $echeck_sand ) {
		update_option('cp_auction_plugin_echeck_sand',$echeck_sand);
		} else {
		delete_option('cp_auction_plugin_echeck_sand');
		}
		if( $balance_sand ) {
		update_option('cp_auction_plugin_balance_sand',$balance_sand);
		} else {
		delete_option('cp_auction_plugin_balance_sand');
		}
		if( $creditcard_sand ) {
		update_option('cp_auction_plugin_creditcard_sand',$creditcard_sand);
		} else {
		delete_option('cp_auction_plugin_creditcard_sand');
		}
		if( $debug == "yes" ) {
		update_option('cp_auction_plugin_sand_debug', $debug);
		} else {
		delete_option('cp_auction_plugin_sand_debug');
		}
		if( $admin_sand_email ) {
		update_option('cp_auction_plugin_admin_sand_email', $admin_sand_email);
		} else {
		delete_option('cp_auction_plugin_admin_sand_email');
		}
		if( $seller_sand_email ) {
		update_option('cp_auction_plugin_seller_sand_email', $seller_sand_email);
		} else {
		delete_option('cp_auction_plugin_seller_sand_email');
		}
		if( $buyer_sand_email ) {
		update_option('cp_auction_plugin_buyer_sand_email', $buyer_sand_email);
		} else {
		delete_option('cp_auction_plugin_buyer_sand_email');
		}
		if( $sand_appid ) {
		update_option('cp_auction_plugin_sand_appid', $sand_appid);
		} else {
		delete_option('cp_auction_plugin_sand_appid');
		}
		if( $sand_apiuser ) {
		update_option('cp_auction_plugin_sand_apiuser', $sand_apiuser);
		} else {
		delete_option('cp_auction_plugin_sand_apiuser');
		}
		if( $sand_apipass ) {
		update_option('cp_auction_plugin_sand_apipass', $sand_apipass);
		} else {
		delete_option('cp_auction_plugin_sand_apipass');
		}
		if( $sand_signature ) {
		update_option('cp_auction_plugin_sand_signature', $sand_signature);
		} else {
		delete_option('cp_auction_plugin_sand_signature');
		}
		if( $sand_email ) {
		update_option('cp_auction_plugin_sand_email', $sand_email);
		} else {
		delete_option('cp_auction_plugin_sand_email');
		}
		if( $signature ) {
		update_option('cp_auction_plugin_signature', $signature);
		} else {
		delete_option('cp_auction_plugin_signature');
		}
		if( $apipass ) {
		update_option('cp_auction_plugin_apipass', $apipass);
		} else {
		delete_option('cp_auction_plugin_apipass');
		}
		if( $apiuser ) {
		update_option('cp_auction_plugin_apiuser', $apiuser);
		} else {
		delete_option('cp_auction_plugin_apiuser');
		}
		if( $appid ) {
		update_option('cp_auction_plugin_appid', $appid);
		} else {
		delete_option('cp_auction_plugin_appid');
		}

	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Adaptive settings was saved!','auctionAdmin').'</strong></p></div>';
}

	$option = get_option('cp_auction_users_currency');
	$list = explode(',', $option);

	$options = get_option('cp_auction_users_paypal_lang');
	$res = explode(',', $options);

	$optionss = get_option('cp_auction_simple_settings');
	$opt = explode(',', $optionss);

	$payment_mode = get_option( 'cp_auction_payment_mode' );

?>

	<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Payment Settings', 'auctionAdmin'); ?></h3>
            <div class="inside">

	<p><?php _e('Most settings you`ll find on this page are basics for buying and selling products on your website, the sale of new ads, featured ads etc. is controlled by ClassiPress settings. <strong>Note that you can not use credits, escrow or adaptive payments if you use multiple currencies.</strong>', 'auctionAdmin'); ?></p>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('USERPANEL / DASHBOARD - SETTINGS FIELDS', 'auctionAdmin'); ?></h3></div>

<div class="admin_text"><?php _e('Use the options below if you are not running under admins only and want to disable / enable some of the setting fields in the users dashboard. <strong>To view any changes to the settings, you need to log in to the frontend dashboard as a normal user, with no admin rights</strong>.', 'auctionAdmin'); ?></div>

	<form method="post" action="">

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Disable Settings Fields:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_disable_settings"><?php echo cp_auction_yes_no(get_option('cp_auction_disable_settings')); ?></select> <?php _e('Disable frontend dashboard settings.', 'auctionAdmin'); ?><br /><small><?php _e('This will disable <strong>ALL</strong> settings that directly affect users product sales options, eg. paypal email, tax / vat and PDT settings. <strong>Do NOT disable settings if multiple currencies is allowed. Please see Settings Mode below  for how you can disable these options one by one.</strong>', 'auctionAdmin'); ?></small></td>
    </tr>

    </tbody>
</table>

<div class="admin_header"><?php _e('Settings Mode', 'auctionAdmin'); ?></div>
	<p><?php _e('Select the following settings for how you want your customers to display and use the payment settings as found in their frontend dashboard. If you select Simple Mode so users will have available the only options which you makes available from the below settings <strong>Selectable by Users</strong>. Selecting Advanced Mode so users will get the full range of settings available which again could cause some confusion by users not comfortable with these advanced settings. You can also give users the ability to choose between Simple Mode and the Advanced mode.', 'auctionAdmin'); ?></p>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Set Settings Mode:', 'auctionAdmin'); ?></th>
    <td><select tabindex="1" id="cp_auction_payment_mode" name="cp_auction_payment_mode">
        <option value="" <?php if( $payment_mode == "" ) echo 'selected="selected"'; ?>><?php _e('Select Mode', 'auctionAdmin'); ?></option>
	<option value="simple" <?php if( $payment_mode == "simple" ) echo 'selected="selected"'; ?>><?php _e('Simple Mode', 'auctionAdmin'); ?></option>
	<option value="pro" <?php if( $payment_mode == "pro" ) echo 'selected="selected"'; ?>><?php _e('Advanced Mode', 'auctionAdmin'); ?></option>
	</select><br /><small><?php _e('Set this to Simple Mode (<strong>recommended</strong>) if you want to offer a simple settings solution for your users, <strong>OR</strong> set the below option to give users the ability to choose between Simple Mode and the Advanced mode.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Selectable by Users:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_users_payment_mode"><?php echo cp_auction_yes_no(get_option('cp_auction_users_payment_mode')); ?></select><br /><small><?php _e('Give users the ability to choose between Simple Mode and the Advanced Mode. <strong>Recommended, unless you have disabled the Payment Data Transfer (PDT) option</strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

    </tbody>
</table>

<div class="clear15"></div>

<div class="admin_header"><?php _e('If Simple Mode Enabled:', 'auctionAdmin'); ?></div>
	<p><?php _e('If simple mode is selected above then you can here tick the checkboxes for the setting options you want to make available for your customers, in the frontend dashboard. All settings will still be available for admin and users with admin rights. <strong>The shipping cost module</strong> is based on shipping cost by country and is considered a beta version. Feel free to send us an email if you have ideas for improvements of this module. Settings in relation to the shipping module is to be found under the <strong><a style="text-decoration: none;" href="/wp-admin/admin.php?page=cp-auction&tab=shipping">Shipping</a></strong> tab.', 'auctionAdmin'); ?></p>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"></th>
    <td><input type="checkbox" name="opt[]" id="accept_cih" value="accept_cih" <?php if( in_array("accept_cih", $opt) ) echo 'checked="checked"'; ?>> <label for="accept_cih"><?php _e('Accept Cash in Hand (CIH).', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="accept_cod" value="accept_cod" <?php if( in_array("accept_cod", $opt) ) echo 'checked="checked"'; ?>> <label for="accept_cod"><?php _e('Accept Cash on Delivery (COD).', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="cod_fees" value="cod_fees" <?php if( in_array("cod_fees", $opt) ) echo 'checked="checked"'; ?>> <label for="cod_fees"><?php _e('COD Extra Fees.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="accept_bt" value="accept_bt" <?php if( in_array("accept_bt", $opt) ) echo 'checked="checked"'; ?>> <label for="accept_bt"><?php _e('Accept Bank Transfer.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="bt_fees" value="bt_fees" <?php if( in_array("bt_fees", $opt) ) echo 'checked="checked"'; ?>> <label for="bt_fees"><?php _e('Bank Transfer Extra Fees.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="paypal_email" value="paypal_email" <?php if( in_array("paypal_email", $opt) ) echo 'checked="checked"'; ?>> <label for="paypal_email"><?php _e('Accept PayPal (PayPal Email Address).', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="currencies" value="currencies" <?php if( in_array("currencies", $opt) ) echo 'checked="checked"'; ?>> <label for="currencies"><?php _e('Select Currency. (Do <strong>NOT</strong> disable this option if multiple currencies is allowed)', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="symbol" value="symbol" <?php if( in_array("symbol", $opt) ) echo 'checked="checked"'; ?>> <label for="symbol"><?php _e('Set Currency Symbol.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="language" value="language" <?php if( in_array("language", $opt) ) echo 'checked="checked"'; ?>> <label for="language"><?php _e('Select PayPal Language.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="pp_fees" value="pp_fees" <?php if( in_array("pp_fees", $opt) ) echo 'checked="checked"'; ?>> <label for="pp_fees"><?php _e('Set PayPal Extra Fees', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="tax_vat" value="tax_vat" <?php if( in_array("tax_vat", $opt) ) echo 'checked="checked"'; ?>> <label for="tax_vat"><?php _e('Tax / Vat Settings.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="shipping" value="shipping" <?php if( in_array("shipping", $opt) ) echo 'checked="checked"'; ?>> <label for="shipping"><?php _e('Shipping Cost (Standard - Enable ONLY one shipping cost option).', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="shipping_module" value="shipping_module" <?php if( in_array("shipping_module", $opt) ) echo 'checked="checked"'; ?>> <label for="shipping_module"><?php _e('Shipping Cost Module (Based on Country - Enable ONLY one shipping cost option).', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="author_approves" value="author_approves" <?php if( in_array("author_approves", $opt) ) echo 'checked="checked"'; ?>> <label for="author_approves"><?php _e('Author Approve Payments.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="anonymous" value="anonymous" <?php if( in_array("anonymous", $opt) ) echo 'checked="checked"'; ?>> <label for="anonymous"><?php _e('Allow Anonymous Bids.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="safe_pay" value="safe_pay" <?php if( in_array("safe_pay", $opt) ) echo 'checked="checked"'; ?>> <label for="safe_pay"><?php _e('Accept Safe Payments (escrow).', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="infotext" value="infotext" <?php if( in_array("infotext", $opt) ) echo 'checked="checked"'; ?>> <label for="infotext"><?php _e('Info Text - Top & Bottom (in users shop / ad listings).', 'auctionAdmin'); ?></label><br />
        <?php if( function_exists('aws_cp_settings_transactions') ) { ?>
        <input type="checkbox" name="opt[]" id="accept_credits" value="accept_credits" <?php if( in_array("accept_credits", $opt) ) echo 'checked="checked"'; ?>> <label for="accept_credits"><?php _e('Accept Credit Payments.', 'auctionAdmin'); ?></label><br />
        <input type="checkbox" name="opt[]" id="credit_fees" value="credit_fees" <?php if( in_array("credit_fees", $opt) ) echo 'checked="checked"'; ?>> <label for="credit_fees"><?php _e('Credit Extra Fees.', 'auctionAdmin'); ?></label><br />
        <?php } if ( class_exists("cartpaujPM") ) { ?>
        <input type="checkbox" name="opt[]" id="allow_pms" value="allow_pms" <?php if( in_array("allow_pms", $opt) ) echo 'checked="checked"'; ?>> <label for="allow_pms"><?php _e('Allow PMs (cartpaujPM).', 'auctionAdmin'); ?></label><br />
        <?php } ?>
        <input type="checkbox" name="opt[]" id="pdt" value="pdt" <?php if( in_array("pdt", $opt) ) echo 'checked="checked"'; ?>> <label for="pdt"><?php _e('Payment Data Transfer (PDT).', 'auctionAdmin'); ?></label><br />
	<small><?php _e('Select the settings above which you want to display in users frontend settings if Simple Mode is enabled.', 'auctionAdmin'); ?></small></td>
    </tr>
</table>


	<div class="clear30"></div>

	<div class="admin_header"><?php _e('Users Currency Dropdown', 'auctionAdmin'); ?></div>
	<p><?php _e('Select below the currencies that you want to be available as an option for users. The selection will appear in the drop down menu <strong>Currency</strong> in users frontend dashboard / settings and is <strong>required</strong> for PayPal payments if multiple currencies is allowed.', 'auctionAdmin'); ?></p>

    <table class="form-table">

        <tr valign="top">
        <th scope="row"></th>
        <td colspan="2">
	<div id="form-categorydiv">
	<div class="tabs-panel" id="categories-all" style="padding:15px;">

	<input type="checkbox" name="currency[]" id="usd" value="USD" <?php if( in_array("USD", $list) ) echo 'checked="checked"'; ?>> <label for="usd"><?php _e('&#36; US Dollars', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="eur" value="EUR" <?php if( in_array("EUR", $list) ) echo 'checked="checked"'; ?>> <label for="eur"><?php _e('&euro; Euros', 'auctionAdmin'); ?></label></label><br />
    	<input type="checkbox" name="currency[]" id="gbp" value="GBP" <?php if( in_array("GBP", $list) ) echo 'checked="checked"'; ?>> <label for="gbp"><?php _e('&pound; British Pounds', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="aud" value="AUD" <?php if( in_array("AUD", $list) ) echo 'checked="checked"'; ?>> <label for="aud"><?php _e('&#36; Australian Dollars', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="cad" value="CAD" <?php if( in_array("CAD", $list) ) echo 'checked="checked"'; ?>> <label for="cad"><?php _e('&#36; Canadian Dollars', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="nok" value="NOK" <?php if( in_array("NOK", $list) ) echo 'checked="checked"'; ?>> <label for="nok"><?php _e('kr Norwegian Krone', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="brl" value="BRL" <?php if( in_array("BRL", $list) ) echo 'checked="checked"'; ?>> <label for="brl"><?php _e('R&#36; Brazilian Real', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="myr" value="MYR" <?php if( in_array("MYR", $list) ) echo 'checked="checked"'; ?>> <label for="myr"><?php _e('RM Malaysian Rinngits', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="ngn" value="NGN" <?php if( in_array("NGN", $list) ) echo 'checked="checked"'; ?>> <label for="ngn"><?php _e('₦ Nigerian Naira', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="jpy" value="JPY" <?php if( in_array("JPY", $list) ) echo 'checked="checked"'; ?>> <label for="jpy"><?php _e('&yen; Yen', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="nzd" value="NZD" <?php if( in_array("NZD", $list) ) echo 'checked="checked"'; ?>> <label for="nzd"><?php _e('&#36; New Zealand Dollar', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="rub" value="RUB" <?php if( in_array("RUB", $list) ) echo 'checked="checked"'; ?>> <label for="nzd"><?php _e('&#1088;&#1091;&#1073; Russian Ruble', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="chf" value="CHF" <?php if( in_array("CHF", $list) ) echo 'checked="checked"'; ?>> <label for="chf"><?php _e('Fr Swiss Franc', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="hkd" value="HKD" <?php if( in_array("HKD", $list) ) echo 'checked="checked"'; ?>> <label for="hkd"><?php _e('&#36; Hong Kong Dollar', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="sgd" value="SGD" <?php if( in_array("SGD", $list) ) echo 'checked="checked"'; ?>> <label for="sgd"><?php _e('&#36; Singapore Dollar', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="sek" value="SEK" <?php if( in_array("SEK", $list) ) echo 'checked="checked"'; ?>> <label for="sek"><?php _e('kr Swedish Krona', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="dkk" value="DKK" <?php if( in_array("DKK", $list) ) echo 'checked="checked"'; ?>> <label for="dkk"><?php _e('kr Danish Krone', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="mxn" value="MXN" <?php if( in_array("MXN", $list) ) echo 'checked="checked"'; ?>> <label for="mxn"><?php _e('&#36; Mexican Peso', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="twd" value="TWD" <?php if( in_array("TWD", $list) ) echo 'checked="checked"'; ?>> <label for="twd"><?php _e('&#36; Taiwan New Dollar', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="php" value="PHP" <?php if( in_array("PHP", $list) ) echo 'checked="checked"'; ?>> <label for="php"><?php _e('P Philippine Peso', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="try" value="TRY" <?php if( in_array("TRY", $list) ) echo 'checked="checked"'; ?>> <label for="try"><?php _e('&#8356; Turkish Lira', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="thb" value="THB" <?php if( in_array("THB", $list) ) echo 'checked="checked"'; ?>> <label for="thb"><?php _e('&#3647; Thai Baht', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="pln" value="PLN" <?php if( in_array("PLN", $list) ) echo 'checked="checked"'; ?>> <label for="pln"><?php _e('z&#322; Polish Zloty', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="huf" value="HUF" <?php if( in_array("HUF", $list) ) echo 'checked="checked"'; ?>> <label for="huf"><?php _e('Ft Hungarian Forint', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="vuv" value="VUV" <?php if( in_array("VUV", $list) ) echo 'checked="checked"'; ?>> <label for="vuv"><?php _e('VT Vanuatu Vatu', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="czk" value="CZK" <?php if( in_array("CZK", $list) ) echo 'checked="checked"'; ?>> <label for="czk"><?php _e('K&#269; Czech Koruna', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="currency[]" id="ils" value="ILS" <?php if( in_array("ILS", $list) ) echo 'checked="checked"'; ?>> <label for="ils"><?php _e('&#8362; Israeli Shekel', 'auctionAdmin'); ?></label><div class="clear10"></div>
	<input type="hidden" name="currency[]" value="-1">

	</div>
	</div></td>
    </tr>
</table>

	<div class="clear20"></div>

	<div class="admin_header"><?php _e('PayPal Checkout Language', 'auctionAdmin'); ?></div>
	<p><?php _e('Select below the languages that you want to be available as an option for users. The selection will appear in the drop down menu <strong>PayPal Language</strong> in users frontend dashboard / settings.', 'auctionAdmin'); ?></p>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"></th>
        <td colspan="2">
	<div id="form-categorydiv">
	<div class="tabs-panel" id="categories-all" style="padding:15px;">

	<input type="checkbox" name="lang[]" id="au" value="AU" <?php if( in_array("AU", $res) ) echo 'checked="checked"'; ?>> <label for="au"><?php _e('Australia', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="at" value="AT" <?php if( in_array("AT", $res) ) echo 'checked="checked"'; ?>> <label for="at"><?php _e('Austria', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="be" value="BE" <?php if( in_array("BE", $res) ) echo 'checked="checked"'; ?>> <label for="be"><?php _e('Belgium', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="br" value="BR" <?php if( in_array("BR", $res) ) echo 'checked="checked"'; ?>> <label for="br"><?php _e('Brazil', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="ca" value="CA" <?php if( in_array("CA", $res) ) echo 'checked="checked"'; ?>> <label for="ca"><?php _e('Canada', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="ch" value="CH" <?php if( in_array("CH", $res) ) echo 'checked="checked"'; ?>> <label for="ch"><?php _e('Switzerland', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="cn" value="CN" <?php if( in_array("CN", $res) ) echo 'checked="checked"'; ?>> <label for="cn"><?php _e('China', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="de" value="DE" <?php if( in_array("DE", $res) ) echo 'checked="checked"'; ?>> <label for="de"><?php _e('Germany', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="es" value="ES" <?php if( in_array("ES", $res) ) echo 'checked="checked"'; ?>> <label for="es"><?php _e('Spain', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="gb" value="GB" <?php if( in_array("GB", $res) ) echo 'checked="checked"'; ?>> <label for="gb"><?php _e('United Kingdom', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="fr" value="FR" <?php if( in_array("FR", $res) ) echo 'checked="checked"'; ?>> <label for="fr"><?php _e('France', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="it" value="IT" <?php if( in_array("IT", $res) ) echo 'checked="checked"'; ?>> <label for="it"><?php _e('Italy', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="nl" value="NL" <?php if( in_array("NL", $res) ) echo 'checked="checked"'; ?>> <label for="nl"><?php _e('Netherlands', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="pl" value="PL" <?php if( in_array("PL", $res) ) echo 'checked="checked"'; ?>> <label for="pl"><?php _e('Poland', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="pt" value="PT" <?php if( in_array("PT", $res) ) echo 'checked="checked"'; ?>> <label for="pt"><?php _e('Portugal', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="ru" value="RU" <?php if( in_array("RU", $res) ) echo 'checked="checked"'; ?>> <label for="ru"><?php _e('Russia', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="us" value="US" <?php if( in_array("US", $res) ) echo 'checked="checked"'; ?>> <label for="us"><?php _e('United States', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="dk" value="DK" <?php if( in_array("DK", $res) ) echo 'checked="checked"'; ?>> <label for="dk"><?php _e('Danish', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="il" value="IL" <?php if( in_array("IL", $res) ) echo 'checked="checked"'; ?>> <label for="il"><?php _e('Hebrew', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="id" value="ID" <?php if( in_array("ID", $res) ) echo 'checked="checked"'; ?>> <label for="id"><?php _e('Indonesian', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="jp" value="JP" <?php if( in_array("JP", $res) ) echo 'checked="checked"'; ?>> <label for="jp"><?php _e('Japanese', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="no" value="NO" <?php if( in_array("NO", $res) ) echo 'checked="checked"'; ?>> <label for="no"><?php _e('Norwegian', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="br" value="BR" <?php if( in_array("BR", $res) ) echo 'checked="checked"'; ?>> <label for="br"><?php _e('Brazilian Portuguese', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="lt" value="LT" <?php if( in_array("LT", $res) ) echo 'checked="checked"'; ?>> <label for="lt"><?php _e('Lithuania', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="lv" value="LV" <?php if( in_array("LV", $res) ) echo 'checked="checked"'; ?>> <label for="lv"><?php _e('Latvia', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="ua" value="UA" <?php if( in_array("UA", $res) ) echo 'checked="checked"'; ?>> <label for="ua"><?php _e('Ukraine', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="se" value="SE" <?php if( in_array("SE", $res) ) echo 'checked="checked"'; ?>> <label for="se"><?php _e('Swedish', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="th" value="TH" <?php if( in_array("TH", $res) ) echo 'checked="checked"'; ?>> <label for="th"><?php _e('Thai', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="tr" value="TR" <?php if( in_array("TR", $res) ) echo 'checked="checked"'; ?>> <label for="tr"><?php _e('Turkish', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="zn" value="ZN" <?php if( in_array("ZN", $res) ) echo 'checked="checked"'; ?>> <label for="zn"><?php _e('Chinese (For domestic Chinese bank transactions only)', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="c2" value="C2" <?php if( in_array("C2", $res) ) echo 'checked="checked"'; ?>> <label for="c2"><?php _e('Chinese (For CUP, bank card and cross-border transactions)', 'auctionAdmin'); ?></label><br />
    	<input type="checkbox" name="lang[]" id="tw" value="TW" <?php if( in_array("TW", $res) ) echo 'checked="checked"'; ?>> <label for="tw"><?php _e('Taiwan (Province of China)', 'auctionAdmin'); ?></label><div class="clear10"></div>
    	<input type="checkbox" name="lang[]" id="hk" value="HK" <?php if( in_array("HK", $res) ) echo 'checked="checked"'; ?>> <label for="hk"><?php _e('Hong Kong', 'auctionAdmin'); ?></label><br />
    	<input type="hidden" name="lang[]" value="-1">

	</div>
	</div></td>
    </tr>

        <tr valign="top">
    <td colspan="2"><input type="submit" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" name="save" class="button-primary"/></td>
    </tr>
</table>

	<div class="clear30"></div>

	<div class="dotted"><h3><?php _e('CASH IN HAND (CIH) SETTINGS', 'auctionAdmin'); ?></h3></div>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Enable CIH:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_cih"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_cih')); ?></select><br /><small><?php _e('Set this to yes if you want to offer cash in hand on pick up as a payment option on your site. No payment buttons will appear, and no shipping will be calculated.', 'auctionAdmin'); ?></small></td>
        </tr>
</table>

	<div class="clear30"></div>

	<div class="dotted"><h3><?php _e('CASH ON DELIVERY (COD) SETTINGS', 'auctionAdmin'); ?></h3></div>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Enable COD:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_cod"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_cod')); ?></select><br /><small><?php _e('Set this to yes if you want to offer cash on delivery as a payment option on your site. No shipping will be calculated', 'auctionAdmin'); ?></small></td>
        </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Price Modifier:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_cod_fee_type" id="cp_auction_cod_fee_type" style="min-width:100px;">
	<option value="static_on" <?php if(get_option('cp_auction_cod_fee_type') == 'static_on') echo 'selected="selected"'; ?>><?php _e('Amount added to the sales price', 'auctionAdmin'); ?></option>
	<option value="percentage_on" <?php if(get_option('cp_auction_cod_fee_type') == 'percentage_on') echo 'selected="selected"'; ?>><?php _e('Percentage added to the sales price', 'auctionAdmin'); ?></option>
	</select> <?php _e('Select how the price should be affected by the COD fees.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('COD Fee:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_cod_fee" value="<?php echo get_option('cp_auction_cod_fee'); ?>" size="8" /> <?php _e('Enter #.## for currency (i.e. 2.25 for $2.25), ### for percentage (i.e. 50 for 50%).', 'auctionAdmin'); ?><br /><small><?php _e('Set the default fee per COD delivery. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?></small></td>
    </tr>
</table>

	<div class="clear30"></div>

	<div class="dotted"><h3><?php _e('PAYPAL & PAY BUTTON SETTINGS', 'auctionAdmin'); ?></h3></div>

<p><?php _e('If you want to use PayPal with another currency than your website standards, which means you can display your ads in standard stated currency and charge via PayPal in another currency, <strong><a style="text-decoration: none;" href="/wp-admin/admin.php?page=cp-auction&tab=misc">check out</a></strong> the currency converter settings.', 'auctionAdmin'); ?></p>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Enable PayPal:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_paypal"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_paypal')); ?></select><br /><small><?php _e('Set this to yes if you want to offer cash payments via PayPal as a payment option on your site.', 'auctionAdmin'); ?></small></td>
    </tr>

<tr valign="top">
        <th scope="row"><?php _e('PayPal Language:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_paypal_lc"><?php echo cp_auction_language_dropdown(get_option('cp_auction_paypal_lc')); ?></select><br /><small><?php _e('Select the language you want to use on PayPal checkout page. Set up any currency settings in ClassiPress -> Payments settings.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Mark Orders as Paid:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_paypal_release"><?php echo cp_auction_yes_no(get_option('cp_auction_paypal_release')); ?></select> <?php _e('Mark order as paid even if PayPal fails to return after payment.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option if you want to mark orders as paid even if PayPal fails to return after payment, ie. buyer click out of payment process after payment completed. <strong>Not recommended with use of digital goods and direct downloads</strong>.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Approve PayPal Payments:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_paypal_approves"><?php echo cp_auction_yes_no(get_option('cp_auction_paypal_approves')); ?></select> <?php _e('Manually approve all PayPal payments regardless of other settings.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('PayPal Direct Payment:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_enable_paypal_direct"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_paypal_direct')); ?></select> <?php _e('This will disable the shopping cart (not on auctions) and any purchases will be paid directly to the author.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option if you want to use PayPal direct payment and not the shopping cart feature. With direct payments means payments made directly to the seller/author.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Price Modifier:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_paypal_fee_type" id="cp_auction_paypal_fee_type" style="min-width:100px;">
	<option value="static_on" <?php if(get_option('cp_auction_paypal_fee_type') == 'static_on') echo 'selected="selected"'; ?>><?php _e('Amount added to the sales price', 'auctionAdmin'); ?></option>
	<option value="percentage_on" <?php if(get_option('cp_auction_paypal_fee_type') == 'percentage_on') echo 'selected="selected"'; ?>><?php _e('Percentage added to the sales price', 'auctionAdmin'); ?></option>
	</select> <?php _e('Select how the price should be affected by the fees.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('PayPal Fee:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_paypal_fee" value="<?php echo get_option('cp_auction_paypal_fee'); ?>" size="8" /> <?php _e('Enter #.## for currency (i.e. 2.25 for $2.25), ### for percentage (i.e. 50 for 50%).', 'auctionAdmin'); ?><br /><small><?php _e('Set the default fee per PayPal payment. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?></small></td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('Select PayPal Button:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_button">
    <option value="" <?php if(get_option('cp_auction_plugin_button') == '') echo 'selected="selected"'; ?>><?php _e('Standard', 'auctionAdmin'); ?></option>
    <option value="1" <?php if(get_option('cp_auction_plugin_button') == '1') echo 'selected="selected"'; ?>><?php _e('Button #1', 'auctionAdmin'); ?></option>
    <option value="2" <?php if(get_option('cp_auction_plugin_button') == '2') echo 'selected="selected"'; ?>><?php _e('Button #2', 'auctionAdmin'); ?></option>
    </select></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Pay Button Text:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_button_text" value="<?php echo get_option('cp_auction_button_text'); ?>" size="15" /> <?php _e('Enter any text for the standard button. Leave blank to use the standard text.', 'auctionAdmin'); ?><br /><small><?php _e('If you have chosen to use the standard payment button you can here enter an optional text for the button, ie. Buy It Now.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Enable PDT:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_pp_pdt"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_pp_pdt')); ?></select> <?php _e('Payment Data Transfer (PDT), automatically approve payments,', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="https://www.x.com/developers/paypal/products/payment-data-transfer" target="_blank"><?php _e('see x.com`s tutorial.', 'auctionAdmin'); ?></a><br /><small><?php _e('Set this to yes if you want to enable Payment Data Transfer (PDT). Payment Data Transfer (PDT) allows you to receive notification of successful payments as they are made. The use of Payment Data Transfer depends on your system configuration and your Return URL. Please note that in order to use Payment Data Transfer, you must turn on Auto Return URL, the URL that will be used to redirect your customers upon payment completion. It does not matter what URL you choose to use because the plugin will overwrite the selected url anyway.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('PDT Identity Token:', 'auctionAdmin'); ?></th>
        <?php $tutorial = "http://docs.appthemes.com/tutorials/enable-paypal-pdt-payment-data-transfer/"; ?>
    <td><input type="text" name="cp_auction_identity_token" value="<?php echo get_option('cp_auction_identity_token'); ?>" size="50" /> <?php echo sprintf(__('See ClassiPress <a style="text-decoration: none;" href="%s" target="_blank">tutorial</a> on enabling PDT.','auctionAdmin'), $tutorial); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('PayPal Email:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_paypalemail" value="<?php echo get_option('cp_auction_plugin_paypalemail'); ?>" size="25" /></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('PayPal Sandbox:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_sandbox"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_sandbox')); ?></select> <?php _e('Enable also if adaptive payments shall be tested,', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="https://www.x.com/developers/paypal/documentation-tools/adaptive-payments/gs_AdaptivePayments" target="_blank"><?php _e('read more.', 'auctionAdmin'); ?></a><br /><small><?php _e('Set this option to Yes if you want to test std. payments and/or adaptive payments using PayPal sandbox. Remember to set this option to No before you go live online with your website.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('PayPal Debugger:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_paypal_debugger"><?php echo cp_auction_yes_no(get_option('cp_auction_paypal_debugger')); ?></select> <?php _e('Enable and you will get a report on screen.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to debug std. PayPal payments. Remember to set this option to No before you go live online with your website.', 'auctionAdmin'); ?></small></td>
    </tr>
        <tr valign="top">
    <td colspan="2"><input type="submit" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" name="save" class="button-primary"/></td>
    </tr>
</table>

	<p><?php _e('If you want to manually approve all ads and payments, you can do so by enabling the Approve Ads & Payments in General settings.', 'auctionAdmin'); ?></p>

<div class="clear30"></div>

<div class="dotted"><h3><?php _e('TAKE % OF SALE PRICE', 'auctionAdmin'); ?></h3></div>

<table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Take % of sale price:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_actfee" value="<?php echo get_option('cp_auction_plugin_actfee'); ?>" size="8" /> % - <?php _e('Leave blank if your paypal does not support adaptive payments. <strong>IMPORTANT!!</strong>', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="https://www.x.com/developers/paypal/documentation-tools/adaptive-payments/gs_AdaptivePayments" target="_blank"><?php _e('Documentation.', 'auctionAdmin'); ?></a><br /><small><?php _e('Enter the percentage to deduct from the purchase price when the buyer pays for the product he just bought. eg. 20, which means 80% goes to the seller and 20% goes to the website admin.', 'auctionAdmin'); ?></small></td>
    </tr>

    </tbody>
</table>

<div class="clear30"></div>

	<div class="dotted"><h3><?php _e('CREDIT MODULE', 'auctionAdmin'); ?></h3></div>

	<p><?php _e('<strong>Note that you can not use credits, escrow or adaptive payments if you use multiple currencies.</strong>', 'auctionAdmin'); ?></p>

    <table class="form-table">

<?php if( function_exists('aws_cp_settings_transactions') ) { ?>

        <tr valign="top">
        <th scope="row"><?php _e('Enable Buy/Sell Credits:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_credits"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_credits')); ?></select> <?php _e('Offer the use of credits as an option to pay author/seller for any purchase.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to yes if you want to offer the use of credits as an option to pay author/seller for any purchase done by other users. Author/seller must enable this option in their account settings.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Approve Direct Payments:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_credit_approves"><?php echo cp_auction_yes_no(get_option('cp_auction_credit_approves')); ?></select> <?php _e('Manually approve all Credit Direct Payments regardless of other settings.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to Yes if you want to manually approve all Credit direct payments regardless of other settings. With direct payments means payments made directly to the seller/author.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Credit Value:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_credit_value" value="<?php echo get_option('cp_auction_plugin_credit_value'); ?>" size="8" /> <?php echo $cp_options->currency_code; ?><br /><small><?php _e('Set the default price per credit, ie. 0.10. Users can then choose the number of credits they wanna purchase for later use on your website.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Reward users with credits:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_credits_on_posting" value="<?php echo get_option('cp_auction_plugin_credits_on_posting'); ?>" size="8" /> <?php _e('Credits', 'auctionAdmin'); ?><br /><small><?php _e('Add credits to users account when they are posting an paid ad. Enter the number of credits you want to assign.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Price Modifier:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_credits_fee_type" id="cp_auction_credits_fee_type" style="min-width:100px;">
	<option value="static_off" <?php if(get_option('cp_auction_credits_fee_type') == 'static_off') echo 'selected="selected"'; ?>><?php _e('Amount of sales price (paid by seller)', 'auctionAdmin'); ?></option>
	<option value="static_on" <?php if(get_option('cp_auction_credits_fee_type') == 'static_on') echo 'selected="selected"'; ?>><?php _e('Amount added to the sales price (paid by buyer)', 'auctionAdmin'); ?></option>
	<option value="percentage_off" <?php if(get_option('cp_auction_credits_fee_type') == 'percentage_off') echo 'selected="selected"'; ?>><?php _e('Percentage of sales price (paid by seller)', 'auctionAdmin'); ?></option>
	<option value="percentage_on" <?php if(get_option('cp_auction_credits_fee_type') == 'percentage_on') echo 'selected="selected"'; ?>><?php _e('Percentage added to the sales price (paid by buyer)', 'auctionAdmin'); ?></option>
	</select> <?php _e('Select how the item price should be affected by the fees.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Fee for Credit Payment:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_credits_fee" value="<?php echo get_option('cp_auction_plugin_credits_fee'); ?>" size="8" /> <?php _e('Enter #.## for currency (i.e. 2.25 for $2.25), ### for percentage (i.e. 50 for 50%).', 'auctionAdmin'); ?><br /><small><?php _e('Set the default fee per credit payment if admins only is enabled. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Minimum Withdraw:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_minimum_withdraw" value="<?php echo get_option('cp_auction_minimum_withdraw'); ?>" size="8" /> <?php _e('Enter the minimum number of credits.', 'auctionAdmin'); ?><br /><small><?php _e('Enter the minimum number of credits users must have in their account before they can get those paid. Leave blank or set to 0 for no minimum.', 'auctionAdmin'); ?></small></td>
    </tr>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Enable PayPal:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_paypal_withdraw_credits"><?php echo cp_auction_yes_no(get_option('cp_auction_paypal_withdraw_credits')); ?></select><br /><small><?php _e('Set this to yes if you want to offer withdraw via PayPal as an exchange option for credits.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('PayPal Withdraw Fee:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_withdraw_fee_paypal" value="<?php echo get_option('cp_auction_withdraw_fee_paypal'); ?>" size="8" /> <?php _e('Enter #.## for currency (i.e. 2.25 for $2.25), ### for percentage (i.e. 50 for 50%).', 'auctionAdmin'); ?><br /><small><?php _e('Set the default fee per withdraw to PayPal. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?></small></td>
    </tr>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Enable Bank Transfer:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_bt_withdraw_credits"><?php echo cp_auction_yes_no(get_option('cp_auction_bt_withdraw_credits')); ?></select><br /><small><?php _e('Set this to yes if you want to offer withdraw via bank transfer as an exchange option for credits.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Set the default fee per withdraw via bank transfer. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Bank Transfer Withdraw Fee:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_withdraw_fee_bt" value="<?php echo get_option('cp_auction_withdraw_fee_bt'); ?>" size="8" /> <?php _e('Enter #.## for currency (i.e. 2.25 for $2.25), ### for percentage (i.e. 50 for 50%).', 'auctionAdmin'); ?><br /><small><?php _e('Set the default fee per withdraw via bank transfer. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Payout Information:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="cp_auction_payout_info" rows="5" cols="80"><?php echo get_option('cp_auction_payout_info'); ?></textarea><br /><small><?php _e('Enter the fields for the information you need in regards to the payout option user has selected.', 'auctionAdmin'); ?> <?php _e('eg. PayPal Email:, Bank Name:, Account No:, one field pr. line. HTML can be used.', 'auctionAdmin'); ?></small></td>
    </tr>
</table>
<?php } else { ?>
</table>

	<?php delete_option('cp_auction_plugin_credits'); ?>
	<p><?php _e('Buy our complete <a style="text-decoration: none;" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank"><strong>credit payment module</strong></a> and allow your customers to pay for their product purchases directly to the seller using their earned credits.', 'auctionAdmin'); ?></p>
<?php } ?>
</table>

	<div class="clear20"></div>

	<div class="dotted"><h3><?php _e('BANK TRANSFER SETTINGS', 'auctionAdmin'); ?></h3></div>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Enable Bank Transfer:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_bank_transfer"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_bank_transfer')); ?></select><br /><small><?php _e('Set this to yes if you want to offer cash payments via bank transfer as a payment option on your site.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Price Modifier:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_banktransfer_fee_type" id="cp_auction_banktransfer_fee_type" style="min-width:100px;">
	<option value="static_on" <?php if(get_option('cp_auction_banktransfer_fee_type') == 'static_on') echo 'selected="selected"'; ?>><?php _e('Amount added to the sales price', 'auctionAdmin'); ?></option>
	<option value="percentage_on" <?php if(get_option('cp_auction_banktransfer_fee_type') == 'percentage_on') echo 'selected="selected"'; ?>><?php _e('Percentage added to the sales price', 'auctionAdmin'); ?></option>
	</select> <?php _e('Select how the price should be affected by the fees.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Fee for Bank Transfer:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_bt_fee" value="<?php echo get_option('cp_auction_plugin_bt_fee'); ?>" size="8" /> <?php _e('Enter #.## for currency (i.e. 2.25 for $2.25), ### for percentage (i.e. 50 for 50%).', 'auctionAdmin'); ?><br /><small><?php _e('Set the default fee per bank transfer. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Bank Transfer Instructions:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="cp_auction_plugin_bank_info" rows="10" cols="80"><?php echo get_option('cp_auction_plugin_bank_info'); ?></textarea><br><small><strong><?php _e('Enter your specific bank wire instructions here. HTML can be used.', 'auctionAdmin'); ?></strong> <?php _e('This will be shown on the payment page after a purchase has been submitted. You will then need to verify the money has been transfered and then manually approve the purchase. Include your bank account name, number, routing number, IBAN, or whatever information is necessary to transfer money into your account. <strong>Leave blank to use ClassiPress Bank Transfer Instructions</strong>.', 'auctionAdmin'); ?></small></td>
    </tr>
        <tr valign="top">
    <td colspan="2"><input type="submit" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" name="save" class="button-primary"/></td>
    </tr>
</table>

	<div class="clear20"></div>

	<div class="dotted"><h3><?php _e('ESCROW - SAFE PAYMENTS', 'auctionAdmin'); ?></h3></div>

	<p><?php _e('<strong>Note that you can not use credits, escrow or adaptive payments if you use multiple currencies.</strong>', 'auctionAdmin'); ?></p>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Enable Escrow Payment:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_pay_admin"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_pay_admin')); ?></select><br /><small><?php _e('Set this option to yes if you want to offer the use of safe/escrow payments. Buyer have to pay for the product/item purchased, to admin.', 'auctionAdmin'); ?></small></td>
    </tr>

<?php if( function_exists('aws_cp_settings_transactions') ) { ?>

    <tr valign="top">
    <td colspan="2"><div class="dotted"><strong><?php _e('Credits Escrow', 'auctionAdmin'); ?></strong></div></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Credits Escrow Payment:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_credits_escrow"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_credits_escrow')); ?></select> <?php _e('Safe / Escrow Payment have to be enabled.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to yes if you want to offer the use of credits payment on safe/escrow payments. A separate paybutton will appear in the pay for auction / item page, and buyer can pay/escrow with credits for the product/item purchased, to admin.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Credits Escrow Button:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_pay_escrow_credits" value="<?php echo get_option('cp_auction_plugin_pay_escrow_credits'); ?>" size="25" /><br /><small><?php _e('If you choose to use the safe/escrow payment with credits then you can change the text on the credit deduct/payment button, change the text to ie. Escrow by Credits.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Price Modifier:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_cre_escrow_fee_type" id="cp_auction_cre_escrow_fee_type" style="min-width:100px;">
	<option value="static_off" <?php if(get_option('cp_auction_cre_escrow_fee_type') == 'static_off') echo 'selected="selected"'; ?>><?php _e('Amount of sales price (paid by seller)', 'auctionAdmin'); ?></option>
	<option value="static_on" <?php if(get_option('cp_auction_cre_escrow_fee_type') == 'static_on') echo 'selected="selected"'; ?>><?php _e('Amount added to the sales price (paid by buyer)', 'auctionAdmin'); ?></option>
	<option value="percentage_off" <?php if(get_option('cp_auction_cre_escrow_fee_type') == 'percentage_off') echo 'selected="selected"'; ?>><?php _e('Percentage of sales price (paid by seller)', 'auctionAdmin'); ?></option>
	<option value="percentage_on" <?php if(get_option('cp_auction_cre_escrow_fee_type') == 'percentage_on') echo 'selected="selected"'; ?>><?php _e('Percentage added to the sales price (paid by buyer)', 'auctionAdmin'); ?></option>
	</select> <?php _e('Select how the price should be affected by the fees.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Set the default fee per credit escrow. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?>"><div class="helpico"></div></a><?php _e('Escrow Credit Fee:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_esc_credits_fee" value="<?php echo get_option('cp_auction_plugin_esc_credits_fee'); ?>" size="8" /> <?php _e('Enter #.## for currency (i.e. 2.25 for $2.25), ### for percentage (i.e. 50 for 50%).', 'auctionAdmin'); ?><br /><small><?php _e('Set the default fee per credit escrow. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?></small></td>
    </tr>
</table>

<?php } ?>
</table>

    <table class="form-table">
    <tr valign="top">
    <td colspan="2"><div class="dotted"><strong><?php _e('Bank Transfer Escrow', 'auctionAdmin'); ?></strong></div></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Bank Transfer Escrows:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_esc_bank_transfer"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_esc_bank_transfer')); ?></select> <?php _e('Safe / Escrow Payment have to be enabled.', 'auctionAdmin'); ?><br /><small><?php _e('Set this to yes if you want to offer bank transfer as a payment option on safe/escrow payments.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Bank Transfer Escrow Button:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_pay_bt_escrow" value="<?php echo get_option('cp_auction_plugin_pay_bt_escrow'); ?>" size="25" /><br /><small><?php _e('If you choose to use the bank transfer option safe/escrow payment then you can change the text on the escrow payment button, change the text to ie. Escrow by Bank Transfer.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Price Modifier:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_bt_escrow_fee_type" id="cp_auction_bt_escrow_fee_type" style="min-width:100px;">
	<option value="static_off" <?php if(get_option('cp_auction_bt_escrow_fee_type') == 'static_off') echo 'selected="selected"'; ?>><?php _e('Amount of sales price (paid by seller)', 'auctionAdmin'); ?></option>
	<option value="static_on" <?php if(get_option('cp_auction_bt_escrow_fee_type') == 'static_on') echo 'selected="selected"'; ?>><?php _e('Amount added to the sales price (paid by buyer)', 'auctionAdmin'); ?></option>
	<option value="percentage_off" <?php if(get_option('cp_auction_bt_escrow_fee_type') == 'percentage_off') echo 'selected="selected"'; ?>><?php _e('Percentage of sales price (paid by seller)', 'auctionAdmin'); ?></option>
	<option value="percentage_on" <?php if(get_option('cp_auction_bt_escrow_fee_type') == 'percentage_on') echo 'selected="selected"'; ?>><?php _e('Percentage added to the sales price (paid by buyer)', 'auctionAdmin'); ?></option>
	</select> <?php _e('Select how the price should be affected by the fees.', 'auctionAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Escrow Bank Transfer Fee:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_esc_bt_fee" value="<?php echo get_option('cp_auction_plugin_esc_bt_fee'); ?>" size="8" /> <?php _e('Enter #.## for currency (i.e. 2.25 for $2.25), ### for percentage (i.e. 50 for 50%).', 'auctionAdmin'); ?><br /><small><?php _e('Set the default fee per escrow bank transfer. Leave blank or set to 0 for no fee.', 'auctionAdmin'); ?></small></td>
    </tr>
</table>

	<div class="clear20"></div>

	<div class="dotted"><h3><?php _e('OTHER ESCROW SETTINGS', 'auctionAdmin'); ?></h3></div>

    <table class="form-table">
        <tr valign="top">
        <th scope="row"><?php _e('Information to buyer:', 'auctionAdmin'); ?></th>
    <td><textarea class="options" name="cp_auction_plugin_escrow_info" rows="5" cols="80"><?php echo get_option('cp_auction_plugin_escrow_info'); ?></textarea><br /><small><?php _e('Enter some information to the buyer about escrow payments, this information will appear below the PayPal button in users profile page. HTML can be used.', 'auctionAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Disable Escrow Payment:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_pay_user"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_pay_user')); ?></select> <?php _e('Allow user to disable escrow payments.', 'auctionAdmin'); ?><br /><small><?php _e('Set this option to yes if you want to offer users the ability to disable safe/escrow payments. Buyer have to pay for the product/item purchased, as to sellers choice.', 'auctionAdmin'); ?></small></td>
    </tr>

    <tr valign="top">
    <td colspan="2"><input type="submit" value="<?php _e('Save Settings', 'auctionAdmin'); ?>" name="save" class="button-primary"/></td>
    </tr>
</table>
	</form>

	<div class="clear30"></div>

	<div class="dotted"><h3><?php _e('ADAPTIVE PAYMENTS', 'auctionAdmin'); ?></h3></div>

	<p><?php _e('<strong>Note that you can not use credits, escrow or adaptive payments if you use multiple currencies.</strong>', 'auctionAdmin'); ?></p>

	<p><?php _e('<strong>Adaptive Payments</strong> has operations that enable the sending and receiving of payments involving two or more parties. Each Adaptive Payments API transaction includes a sender and one or more receivers of the payment. Each transaction also includes the application owner, called the "API Caller," who is an invisible third party that provides the transaction flow and is the entity that makes the API calls. In most scenarios, payment transactions are initiated by the buyer (in a send type of payment arrangement) or by the seller (in a pay type of payment arrangement).', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="https://www.x.com/developers/paypal/documentation-tools/adaptive-payments/gs_AdaptivePayments" target="_blank"><?php _e('Please read this documentation.', 'auctionAdmin'); ?></a></p>

	<p><?php _e('When you apply for the Adaptive Application ID you might need to fill in information about the payment success page which can be found here:', 'auctionAdmin'); ?> <a style="text-decoration: none;" href="<?php echo get_bloginfo( 'url' ); ?>/?confirmation=1" target="_blank"><?php echo get_bloginfo( 'url' ); ?>/?confirmation=1</a></p>

	<form method="post" action="">

    <table class="form-table">
    <tr valign="top">
    <td colspan="2"><h4 style="cursor:default;"><?php _e('PayPal Live Adaptive Settings', 'auctionAdmin'); ?></h4></td>
    <td colspan="2"><h4 style="cursor:default;"><?php _e('PayPal Sandbox Adaptive Settings', 'auctionAdmin'); ?></h4></td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php _e('Signature:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_signature" value="<?php echo get_option('cp_auction_plugin_signature'); ?>" size="30" /></td>
    <th scope="row"><?php _e('Sandbox Signature:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_sand_signature" value="<?php echo get_option('cp_auction_plugin_sand_signature'); ?>" size="30" /></td>
    </tr>
    
    <tr valign="top">
    <th scope="row"><?php _e('API Password:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_apipass" value="<?php echo get_option('cp_auction_plugin_apipass'); ?>" size="30" /></td>
    <th scope="row"><?php _e('Sandbox API Password:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_sand_apipass" value="<?php echo get_option('cp_auction_plugin_sand_apipass'); ?>" size="30" /></td>
    </tr>
    
    <tr valign="top">
    <th scope="row"><?php _e('API User:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_apiuser" value="<?php echo get_option('cp_auction_plugin_apiuser'); ?>" size="30" /></td>
    <th scope="row"><?php _e('Sandbox API User:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_sand_apiuser" value="<?php echo get_option('cp_auction_plugin_sand_apiuser'); ?>" size="30" /></td>
    </tr>
    
    <tr valign="top">
    <th scope="row"><?php _e('Application ID:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_appid" value="<?php echo get_option('cp_auction_plugin_appid'); ?>" size="30" /></td>
    <th scope="row"><?php _e('Sandbox Application ID:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_sand_appid" value="<?php echo get_option('cp_auction_plugin_sand_appid'); ?>" size="30" /><br /><?php _e('Test App ID:', 'auctionAdmin'); ?> APP-80W284485P519543T</td>
    </tr>
<!-- /
    <tr valign="top">
    <th scope="row"><?php _e('Funding Constraints:', 'auctionAdmin'); ?></th>
    <td><input type="checkbox" name="cp_auction_plugin_echeck" value="ECHECK" <?php if(get_option('cp_auction_plugin_echeck') == 'ECHECK') echo 'checked="checked"'; ?>> <?php _e('Echeck', 'auctionAdmin'); ?><br />
        <input type="checkbox" name="cp_auction_plugin_balance" value="BALANCE" <?php if(get_option('cp_auction_plugin_balance') == 'BALANCE') echo 'checked="checked"'; ?>> <?php _e('Balance', 'auctionAdmin'); ?><br />
        <input type="checkbox" name="cp_auction_plugin_creditcard" value="CREDITCARD" <?php if(get_option('cp_auction_plugin_creditcard') == 'CREDITCARD') echo 'checked="checked"'; ?>> <?php _e('Creditcard', 'auctionAdmin'); ?><br /><?php _e('Funding constraints require advanced permissions levels.', 'auctionAdmin'); ?></td>
    <th scope="row"><?php _e('Funding Constraints:', 'auctionAdmin'); ?></th>
    <td><input type="checkbox" name="cp_auction_plugin_echeck_sand" value="ECHECK" <?php if(get_option('cp_auction_plugin_echeck_sand') == 'ECHECK') echo 'checked="checked"'; ?>> <?php _e('Echeck', 'auctionAdmin'); ?><br />
        <input type="checkbox" name="cp_auction_plugin_balance_sand" value="BALANCE" <?php if(get_option('cp_auction_plugin_balance_sand') == 'BALANCE') echo 'checked="checked"'; ?>> <?php _e('Balance', 'auctionAdmin'); ?><br />
        <input type="checkbox" name="cp_auction_plugin_creditcard_sand" value="CREDITCARD" <?php if(get_option('cp_auction_plugin_creditcard_sand') == 'CREDITCARD') echo 'checked="checked"'; ?>> <?php _e('Creditcard', 'auctionAdmin'); ?><br /><?php _e('Funding constraints require advanced permissions levels.', 'auctionAdmin'); ?></td>
    </tr>
-->
    <tr valign="top">
    <th scope="row"></th>
    <td></td>
    <th scope="row"><?php _e('PayPal Login Email:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_sand_email" value="<?php echo get_option('cp_auction_plugin_sand_email'); ?>" size="30" /></td>
    </tr>

    <tr valign="top">
    <th scope="row"></th>
    <td></td>
    <th scope="row"><?php _e('Sandbox Admin Email:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_admin_sand_email" value="<?php echo get_option('cp_auction_plugin_admin_sand_email'); ?>" size="30" /></td>
    </tr>

    <tr valign="top">
    <th scope="row"></th>
    <td></td>
    <th scope="row"><?php _e('Sandbox Seller Email:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_seller_sand_email" value="<?php echo get_option('cp_auction_plugin_seller_sand_email'); ?>" size="30" /></td>
    </tr>

    <tr valign="top">
    <th scope="row"></th>
    <td></td>
    <th scope="row"><?php _e('Sandbox Buyer Email:', 'auctionAdmin'); ?></th>
    <td><input type="text" name="cp_auction_plugin_buyer_sand_email" value="<?php echo get_option('cp_auction_plugin_buyer_sand_email'); ?>" size="30" /></td>
    </tr>

    <tr valign="top">
    <th scope="row"></th>
    <td></td>
    <th scope="row"><?php _e('Debugger:', 'auctionAdmin'); ?></th>
    <td><select name="cp_auction_plugin_sand_debug"><?php echo cp_auction_yes_no(get_option('cp_auction_plugin_sand_debug')); ?></select> <?php _e('Displays any errors in payment scheme.', 'auctionAdmin'); ?></td>
    </tr>

    <tr valign="top">
    <td colspan="4"><input type="submit" value="<?php _e('Save Adaptive Settings', 'auctionAdmin'); ?>" name="save_adaptive" class="button-primary"/></td>
    </tr>

    </table>

    </form>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}