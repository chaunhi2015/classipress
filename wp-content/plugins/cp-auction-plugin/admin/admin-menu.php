<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| ADMIN MENU GENERAL SETTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_settings( $current = '' ) {

	$current = isset($_GET['tab']) ? $_GET['tab'] : 'general';
	if(isset($_GET['tab'])) $current = $_GET['tab']; ?>

	<div class="wrap">
		<?php screen_icon( 'cp-auction-plugin' ); ?>
		<h2><?php _e( 'CP Auction Settings', 'auctionAdmin' ); ?></h2>

<?php
    $tabs = array( 'general' => __('General', 'auctionAdmin'), 'pages' => __('Pages', 'auctionAdmin'), 'cats' => __('Category Filtering', 'auctionAdmin'), 'payment' => __('Payment', 'auctionAdmin'), 'fields' => __('Custom Fields', 'auctionAdmin'), 'credits' => __('Credit Handler', 'auctionAdmin'), 'social' => __('Social Sharing', 'auctionAdmin'), 'frontpage' => __('Frontpage', 'auctionAdmin'), 'shipping' => __('Shipping', 'auctionAdmin'), 'misc' => __('Misc', 'auctionAdmin'), 'setup' => __('Setup', 'auctionAdmin') );
    $links = array();
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) :
            $links[] = "<a class='nav-tab nav-tab-active' href='?page=cp-auction&tab=$tab'>$name</a>";
        else :
            $links[] = "<a class='nav-tab' href='?page=cp-auction&tab=$tab'>$name</a>";
        endif;
    endforeach;
    echo '<h2 class="nav-tab-wrapper">';
    foreach ( $links as $link )
        echo $link;
    echo '</h2>';

    if ( isset ( $_GET['tab'] ) ) : $tab = $_GET['tab'];
    else:
        $tab = 'general';
    endif;
    switch ( $tab ) :
        case 'general' :
            cp_auction_plugin_general_settings();
            break;
        case 'pages' :
            cp_auction_plugin_page_settings();
            break;
        case 'cats' :
            cp_auction_plugin_filter_cats_settings();
            break;
        case 'payment' :
            cp_auction_plugin_payment_settings();
            break;
        case 'fields' :
            cp_auction_custom_fields();
            break;
        case 'credits' :
            cp_auction_plugin_credit_handler();
            break;
        case 'social' :
            cp_auction_plugin_social_settings();
            break;
        case 'setup' :
            cp_auction_plugin_setup_instructions();
            break;
        case 'frontpage' :
            cp_auction_plugin_setup_frontpage();
            break;
        case 'shipping' :
            cp_auction_plugin_setup_shipping();
            break;
        case 'misc' :
            cp_auction_plugin_misc_settings();
            break;
    endswitch;
}


/*
|--------------------------------------------------------------------------
| ADMIN MENU DOWNLOADS SETTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_downloads( $current = '' ) {

	$current = isset($_GET['tab']) ? $_GET['tab'] : 'uploaded';
	if(isset($_GET['tab'])) $current = $_GET['tab']; ?>

	<div class="wrap">
	<div class="icon32" id="icon-edit-pages"><br/></div>
	<h2><?php _e('Downloads', 'auctionAdmin'); ?></h2>

<?php
    $tabs = array( 'uploaded' => __('Uploaded Files', 'auctionAdmin'), 'downloaded' => __('Download Log', 'auctionAdmin') );
    $links = array();
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) :
            $links[] = "<a class='nav-tab nav-tab-active' href='?page=cp-downloads&tab=$tab'>$name</a>";
        else :
            $links[] = "<a class='nav-tab' href='?page=cp-downloads&tab=$tab'>$name</a>";
        endif;
    endforeach;
    echo '<h2 class="nav-tab-wrapper">';
    foreach ( $links as $link )
        echo $link;
    echo '</h2>';

    if ( isset ( $_GET['tab'] ) ) : $tab = $_GET['tab'];
    else:
        $tab = 'uploaded';
    endif;
    switch ( $tab ) :
        case 'uploaded' :
            cp_auction_plugin_upload_history();
            break;
        case 'downloaded' :
            cp_auction_plugin_download_history();
            break;
    endswitch;
}


/*
|--------------------------------------------------------------------------
| ADMIN MENU MY LISTINGS SETTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_my_listings( $current = '' ) {

	$current = isset($_GET['tab']) ? $_GET['tab'] : 'classified';
	if(isset($_GET['tab'])) $current = $_GET['tab']; ?>

	<div class="wrap">
	<div class="icon32" id="icon-edit-pages"><br/></div>
	<h2><?php _e('My Listings', 'auctionAdmin'); ?></h2>

<?php
    $tabs = array( 'classified' => __('Classifieds', 'auctionAdmin'), 'wanted' => __('Wanted', 'auctionAdmin'), 'auctions' => __('Auctions', 'auctionAdmin') );
    $links = array();
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) :
            $links[] = "<a class='nav-tab nav-tab-active' href='?page=cp-my-listings&tab=$tab'>$name</a>";
        else :
            $links[] = "<a class='nav-tab' href='?page=cp-my-listings&tab=$tab'>$name</a>";
        endif;
    endforeach;
    echo '<h2 class="nav-tab-wrapper">';
    foreach ( $links as $link )
        echo $link;
    echo '</h2>';

    if ( isset ( $_GET['tab'] ) ) : $tab = $_GET['tab'];
    else:
        $tab = 'classified';
    endif;
    switch ( $tab ) :
        case 'classified' :
            cp_auction_plugin_my_classified();
            break;
        case 'wanted' :
            cp_auction_plugin_my_wanted();
            break;
        case 'auctions' :
            cp_auction_plugin_my_auctions();
            break;
    endswitch;
}


/*
|--------------------------------------------------------------------------
| ADMIN MENU MY LISTINGS SETTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_all_listings( $current = '' ) {

	$current = isset($_GET['tab']) ? $_GET['tab'] : 'classified';
	if(isset($_GET['tab'])) $current = $_GET['tab']; ?>

	<div class="wrap">
	<div class="icon32" id="icon-edit-pages"><br/></div>
	<h2><?php _e('Ad Listings', 'auctionAdmin'); ?></h2>

<?php
    $tabs = array( 'classified' => __('Classifieds', 'auctionAdmin'), 'wanted' => __('Wanted', 'auctionAdmin'), 'auctions' => __('Auctions', 'auctionAdmin') );
    $links = array();
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) :
            $links[] = "<a class='nav-tab nav-tab-active' href='?page=cp-all-listings&tab=$tab'>$name</a>";
        else :
            $links[] = "<a class='nav-tab' href='?page=cp-all-listings&tab=$tab'>$name</a>";
        endif;
    endforeach;
    echo '<h2 class="nav-tab-wrapper">';
    foreach ( $links as $link )
        echo $link;
    echo '</h2>';

    if ( isset ( $_GET['tab'] ) ) : $tab = $_GET['tab'];
    else:
        $tab = 'classified';
    endif;
    switch ( $tab ) :
        case 'classified' :
            cp_auction_plugin_all_classifieds();
            break;
        case 'wanted' :
            cp_auction_plugin_all_wanted();
            break;
        case 'auctions' :
            cp_auction_plugin_all_auctions();
            break;
    endswitch;
}


/*
|--------------------------------------------------------------------------
| ADMIN MENU PURCHASES SETTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_all_purchases( $current = '' ) {

	$current = isset($_GET['tab']) ? $_GET['tab'] : 'purchase';
	if(isset($_GET['tab'])) $current = $_GET['tab']; ?>

	<div class="wrap">
	<div class="icon32" id="icon-edit-pages"><br/></div>
	<h2><?php _e('Purchases', 'auctionAdmin'); ?></h2>

<?php
    $tabs = array( 'purchase' => __('Add Purchase', 'auctionAdmin'), 'pending' => __('Pending / Failed', 'auctionAdmin'), 'history' => __('Purchase History', 'auctionAdmin') );
    $links = array();
    foreach( $tabs as $tab => $name ) :
        if ( $tab == $current ) :
            $links[] = "<a class='nav-tab nav-tab-active' href='?page=cp-add-purchase&tab=$tab'>$name</a>";
        else :
            $links[] = "<a class='nav-tab' href='?page=cp-add-purchase&tab=$tab'>$name</a>";
        endif;
    endforeach;
    echo '<h2 class="nav-tab-wrapper">';
    foreach ( $links as $link )
        echo $link;
    echo '</h2>';

    if ( isset ( $_GET['tab'] ) ) : $tab = $_GET['tab'];
    else:
        $tab = 'purchase';
    endif;
    switch ( $tab ) :
        case 'purchase' :
            cp_auction_plugin_add_purchase();
            break;
        case 'pending' :
            cp_auction_plugin_pending_purchase();
            break;
        case 'history' :
            cp_auction_plugin_purchase_history();
            break;
    endswitch;
}