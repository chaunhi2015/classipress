<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


function cp_auction_plugin_setup_shipping() {

	$msg = "";

	if(isset($_POST['save_shipping'])) {
		$enable_geoip = isset( $_POST['cp_auction_enable_geoip'] ) ? esc_attr( $_POST['cp_auction_enable_geoip'] ) : '';
		$remove_enable_shipping = isset( $_POST['cp_auction_disable_the_enable_shipping_option'] ) ? esc_attr( $_POST['cp_auction_disable_the_enable_shipping_option'] ) : '';

		if( $enable_geoip == "yes" ) {
		update_option( 'cp_auction_enable_geoip', $enable_geoip );
		} else {
		delete_option( 'cp_auction_enable_geoip' );
		}
		if( $remove_enable_shipping == "yes" ) {
		update_option( 'cp_auction_disable_the_enable_shipping_option', $remove_enable_shipping );
		} else {
		delete_option( 'cp_auction_disable_the_enable_shipping_option' );
		}
		$msg = ''.__( 'Shipping settings was saved!', 'auctionAdmin' ).'';
		}

?>

	<?php if($msg) echo '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.$msg.'</strong></p></div>'; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Country Shipping Cost Module', 'auctionAdmin'); ?></h3>
            <div class="inside">

	<p><?php _e('An useful tool for your customers to specify the countries they are delivering their goods to, and the shipping cost for the actual country. You can choose to display a message to visitors if the seller based on the current ad do not deliver to the country that the actual visitors come from. The visitors country is recognized via geoip detection which requires the <strong><a style="text-decoration: none;" href="https://wordpress.org/plugins/geoip-detect/" target="_blank">GeoIP Detection</a></strong> plugin.', 'auctionAdmin'); ?></p>

	<p><?php _e('Visit the <strong><a style="text-decoration: none;" href="/wp-admin/admin.php?page=cp-auction&tab=payment">Payment</a></strong> settings to enable / disable the different shipping options for the frontend settings.', 'auctionAdmin'); ?></p>

<form method="post" action="">

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Enable GeoIP Detection:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_enable_geoip"><?php echo cp_auction_yes_no(get_option('cp_auction_enable_geoip')); ?></select> <?php _e('Enable only if you have installed the <strong><a style="text-decoration: none;" href="https://wordpress.org/plugins/geoip-detect/" target="_blank">GeoIP Detection</a></strong> plugin.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option if you want to display the following message: <strong>This seller does not deliver to Country</strong>.', 'auctionAdmin'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Remove Enable Shipping:', 'auctionAdmin'); ?></th>
						<td><select name="cp_auction_disable_the_enable_shipping_option"><?php echo cp_auction_yes_no(get_option('cp_auction_disable_the_enable_shipping_option')); ?></select> <?php _e('Remove the Enable Shipping option from Dashboard.', 'auctionAdmin'); ?><br /><small><?php _e('Enable this option if you want to remove the Enable Shipping dropdown from the frontend dashboard settings. <strong>This will enable country based shipping as default</strong>.', 'auctionAdmin'); ?></small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

			<p class="submit"><input type="submit" name="save_shipping" id="submit" class="button-primary" value="<?php _e('Save Shipping Settings', 'auctionAdmin'); ?>"  /></p>
 
		</form>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt" onclick="javascript:void window.open('http://www.arctic-websolutions.com/wp-content/plugins/cp-auction-plugin/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

    	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}