<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| MY CLASSIFIED ADS
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_my_classified() {

include_once(ABSPATH.'/wp-content/plugins/cp-auction-plugin/scripts/pagination.class.php');

	global $current_user, $wpdb, $cp_options, $cpurl;
	$limit = "";
	$act = get_option('cp_auction_plugin_auctiontype');

	get_currentuserinfo();
	$uid = $current_user->ID;

	$type = ""; if ( isset( $_GET['status'] ) ) $type = $_GET['status'];

	if ( empty( $type ) || $type == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts AS wposts
  		LEFT JOIN $wpdb->postmeta AS postmeta
    		ON wposts.ID = postmeta.post_id
    		AND postmeta.meta_key = 'cp_auction_my_auction_type'
		WHERE wposts.post_author = '$uid' 
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'private' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."' 
  		AND (postmeta.post_id IS NULL OR postmeta.meta_key = 'cp_auction_my_auction_type' AND postmeta.meta_value = 'classified')";

	$res = $wpdb->get_results($sql);
	$items = $wpdb->num_rows;

	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts AS wposts
  		LEFT JOIN $wpdb->postmeta AS postmeta
    		ON wposts.ID = postmeta.post_id
    		AND postmeta.meta_key = 'cp_auction_my_auction_type'
		WHERE wposts.post_author = '$uid' 
		AND wposts.post_status = '$type'
		AND wposts.post_type = '".APP_POST_TYPE."' 
  		AND (postmeta.post_id IS NULL OR postmeta.meta_key = 'cp_auction_my_auction_type' AND postmeta.meta_value = 'classified')";

	$res = $wpdb->get_results($sql);
	$items = $wpdb->num_rows;
	}

	if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-my-listings");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

	if ( empty( $type ) || $type == "all" ) $all = '<strong>'.__('All', 'auctionAdmin').'</strong>';
	else $all = ''.__('All', 'auctionAdmin').'';
	if ( $type == "publish" ) $published = '<strong>'.__('Published', 'auctionAdmin').'</strong>';
	else $published = ''.__('Published', 'auctionAdmin').'';
	if ( $type == "pending" ) $pending = '<strong>'.__('Pending', 'auctionAdmin').'</strong>';
	else $pending = ''.__('Pending', 'auctionAdmin').'';
	if ( $type == "draft" ) $drafts = '<strong>'.__('Drafts', 'auctionAdmin').'</strong>';
	else $drafts = ''.__('Drafts', 'auctionAdmin').'';
	if ( $type == "private" ) $private = '<strong>'.__('Private', 'auctionAdmin').'</strong>';
	else $private = ''.__('Private', 'auctionAdmin').'';
	
?>

	<div class="clear20"></div>

	<a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=classified&status=all"><?php echo $all; ?> (<?php echo cp_auction_count_my_classifieds('all', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=classified&status=publish"><?php echo $published; ?> (<?php echo cp_auction_count_my_classifieds('publish', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=classified&status=pending"><?php echo $pending; ?> (<?php echo cp_auction_count_my_classifieds('pending', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=classified&status=private"><?php echo $private; ?> (<?php echo cp_auction_count_my_classifieds('private', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=classified&status=draft"><?php echo $drafts; ?> (<?php echo cp_auction_count_my_classifieds('draft', $uid); ?>)</a>

<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php

	if ( empty( $type ) || $type == "all" ) {
	$sql2 = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts AS wposts
  		LEFT JOIN $wpdb->postmeta AS postmeta
    		ON wposts.ID = postmeta.post_id
    		AND postmeta.meta_key = 'cp_auction_my_auction_type'
		WHERE wposts.post_author = '$uid' 
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'private' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."' 
  		AND (postmeta.post_id IS NULL OR postmeta.meta_key = 'cp_auction_my_auction_type' AND postmeta.meta_value = 'classified')
  		ORDER BY wposts.post_date DESC $limit";

	$results = $wpdb->get_results($sql2, OBJECT);
	} else {
	$sql2 = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts AS wposts
  		LEFT JOIN $wpdb->postmeta AS postmeta
    		ON wposts.ID = postmeta.post_id
    		AND postmeta.meta_key = 'cp_auction_my_auction_type'
		WHERE wposts.post_author = '$uid' 
		AND wposts.post_status = '$type'
		AND wposts.post_type = '".APP_POST_TYPE."' 
  		AND (postmeta.post_id IS NULL OR postmeta.meta_key = 'cp_auction_my_auction_type' AND postmeta.meta_value = 'classified')
  		ORDER BY wposts.post_date DESC $limit";

	$results = $wpdb->get_results($sql2, OBJECT);
	}

	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID / Edit', 'auctionAdmin'); ?></th>
        <th><?php _e('Ad Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Price', 'auctionAdmin'); ?></th>
        <th><?php _e('Sales', 'auctionAdmin'); ?></th>
        <th><?php _e('Earnings', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Today', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Total', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Shortcode', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID / Edit', 'auctionAdmin'); ?></th>
        <th><?php _e('Ad Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Price', 'auctionAdmin'); ?></th>
        <th><?php _e('Sales', 'auctionAdmin'); ?></th>
        <th><?php _e('Earnings', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Today', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Total', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Shortcode', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $results ) {
	global $post;
	$rowclass = '';
    	foreach( $results as $post ) : setup_postdata($post);
    	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

            $buy_now = get_post_meta($post->ID,'cp_buy_now',true);
            if ( empty( $buy_now ) ) $buy_now = get_post_meta($post->ID,'cp_price',true);
            $table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	    $howmany = $wpdb->get_var( $wpdb->prepare( "SELECT SUM( numbers ) FROM {$table_name} WHERE pid = '%d'", $post->ID ) );
	    $earnings = $buy_now * $howmany;
	    $total = get_post_meta($post->ID, 'cp_total_count', true);
	    $daily = get_post_meta($post->ID, 'cp_daily_count', true);
	    if( empty( $howmany )) $howmany = 0;
	    $last_bid = cp_auction_plugin_get_latest_bid($post->ID);
	    if( $total < 1 ) $total = 0;
	    if( $daily < 1 ) $daily = 0;

        echo '<tr id="tr_'.$post->ID.'" class="'.$rowclass.'">';
            echo '<td>#<a title="'.__('Click here to edit ad.', 'auctionAdmin').'" href="post.php?post='.$post->ID.'&action=edit">'.$post->ID.'</a></td>';
            echo '<td><a title="'.__('Preview ad listing.', 'auctionAdmin').'" href="'.get_permalink( $post->ID ).'" target="_blank">'.$post->post_title.'</a></td>';
            echo '<td>'.cp_auction_format_amount($buy_now).'</td>';
            echo '<td>'.$howmany.' '.__('items', 'auctionAdmin').'</td>';
            echo '<td>'.cp_auction_format_amount($earnings).'</td>';
            echo '<td>'.$daily.' '.__('views', 'auctionAdmin').'</td>';
            echo '<td>'.$total.' '.__('views', 'auctionAdmin').'</td>';
            echo '<td><a href="#" class="upd-auction-status" title="'.__('Change post status.', 'auctionAdmin').'" id="'.$post->ID.'">'.$post->post_status.'</a></td>';
            echo '<td>[buy-now-button pid="'.$post->ID.'"]</td>';
            echo '<td><a href="#" class="admin-bump-ads" title="'.__('Bump up your ad.', 'auctionAdmin').'" id="'.$post->ID.'">'.__('Bump Up', 'auctionAdmin').'</a> | <a href="#" class="delete-auctions" title="'.__('Delete ad listing.', 'auctionAdmin').'" id="'.$post->ID.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';

	endforeach;

	wp_reset_postdata();

} else { ?>
        <tr>
        <td colspan="10"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">
<font color="red"><strong>*</strong></font> <small><?php _e('Use the shortcode to add a buy now button in posts or pages.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
}
}


/*
|--------------------------------------------------------------------------
| MY WANTED ADS
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_my_wanted() {

include_once(ABSPATH.'/wp-content/plugins/cp-auction-plugin/scripts/pagination.class.php');

	global $current_user, $wpdb, $cp_options, $cpurl;
	$limit = "";
	$act = get_option('cp_auction_plugin_auctiontype');

	get_currentuserinfo();
	$uid = $current_user->ID;

	$type = ""; if ( isset( $_GET['status'] ) ) $type = $_GET['status'];

	if ( empty( $type ) || $type == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'wanted')
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'private' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	$items = $wpdb->num_rows;

	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'wanted')
		AND wposts.post_status = '$type'
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	$items = $wpdb->num_rows;

	}

	if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-my-listings&tab=wanted");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

	if ( empty( $type ) || $type == "all" ) $all = '<strong>'.__('All', 'auctionAdmin').'</strong>';
	else $all = ''.__('All', 'auctionAdmin').'';
	if ( $type == "publish" ) $published = '<strong>'.__('Published', 'auctionAdmin').'</strong>';
	else $published = ''.__('Published', 'auctionAdmin').'';
	if ( $type == "pending" ) $pending = '<strong>'.__('Pending', 'auctionAdmin').'</strong>';
	else $pending = ''.__('Pending', 'auctionAdmin').'';
	if ( $type == "draft" ) $drafts = '<strong>'.__('Drafts', 'auctionAdmin').'</strong>';
	else $drafts = ''.__('Drafts', 'auctionAdmin').'';
	if ( $type == "private" ) $private = '<strong>'.__('Private', 'auctionAdmin').'</strong>';
	else $private = ''.__('Private', 'auctionAdmin').'';
	
?>

	<div class="clear20"></div>

	<a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=wanted&status=all"><?php echo $all; ?> (<?php echo cp_auction_count_my_wanted('all', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=wanted&status=publish"><?php echo $published; ?> (<?php echo cp_auction_count_my_wanted('publish', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=wanted&status=pending"><?php echo $pending; ?> (<?php echo cp_auction_count_my_wanted('pending', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=wanted&status=private"><?php echo $private; ?> (<?php echo cp_auction_count_my_wanted('private', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=wanted&status=draft"><?php echo $drafts; ?> (<?php echo cp_auction_count_my_wanted('draft', $uid); ?>)</a>

<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php

	if ( empty( $type ) || $type == "all" ) {
	$sql2 = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'wanted')
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'private' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."'  
		ORDER BY wposts.post_date DESC $limit";

	$results = $wpdb->get_results($sql2, OBJECT);
	} else {
	$sql2 = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'wanted')
		AND wposts.post_status = '$type'
		AND wposts.post_type = '".APP_POST_TYPE."'  
		ORDER BY wposts.post_date DESC $limit";

	$results = $wpdb->get_results($sql2, OBJECT);
	}

	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID / Edit', 'auctionAdmin'); ?></th>
        <th><?php _e('Ad Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Price', 'auctionAdmin'); ?></th>
        <th><?php _e('Sales', 'auctionAdmin'); ?></th>
        <th><?php _e('Earnings', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Today', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Total', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Shortcode', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID / Edit', 'auctionAdmin'); ?></th>
        <th><?php _e('Ad Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Price', 'auctionAdmin'); ?></th>
        <th><?php _e('Sales', 'auctionAdmin'); ?></th>
        <th><?php _e('Earnings', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Today', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Total', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Shortcode', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $results ) {
	global $post;
    	$rowclass = '';
    	foreach( $results as $post ) : setup_postdata($post);
    	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

            $buy_now = get_post_meta($post->ID,'cp_buy_now',true);
            if ( empty( $buy_now ) ) $buy_now = get_post_meta($post->ID,'cp_price',true);
            $table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	    $howmany = $wpdb->get_var( $wpdb->prepare( "SELECT SUM( numbers ) FROM {$table_name} WHERE pid = '%d'", $post->ID ) );
	    $earnings = $buy_now * $howmany;
	    $total = get_post_meta($post->ID, 'cp_total_count', true);
	    $daily = get_post_meta($post->ID, 'cp_daily_count', true);
	    if( empty( $howmany )) $howmany = 0;
	    $last_bid = cp_auction_plugin_get_latest_bid($post->ID);
	    if( $total < 1 ) $total = 0;
	    if( $daily < 1 ) $daily = 0;

        echo '<tr id="tr_'.$post->ID.'" class="'.$rowclass.'">';
            echo '<td>#<a title="'.__('Click here to edit ad.', 'auctionAdmin').'" href="post.php?post='.$post->ID.'&action=edit">'.$post->ID.'</a></td>';
            echo '<td><a title="'.__('Preview ad listing.', 'auctionAdmin').'" href="'.get_permalink( $post->ID ).'" target="_blank">'.$post->post_title.'</a></td>';
            echo '<td>'.cp_auction_format_amount($buy_now).'</td>';
            echo '<td>'.$howmany.' '.__('items', 'auctionAdmin').'</td>';
            echo '<td>'.cp_auction_format_amount($earnings).'</td>';
            echo '<td>'.$daily.' '.__('views', 'auctionAdmin').'</td>';
            echo '<td>'.$total.' '.__('views', 'auctionAdmin').'</td>';
            echo '<td><a href="#" class="upd-auction-status" title="'.__('Change post status.', 'auctionAdmin').'" id="'.$post->ID.'">'.$post->post_status.'</a></td>';
            echo '<td>[buy-now-button pid="'.$post->ID.'"]</td>';
            echo '<td><a href="#" class="admin-bump-ads" title="'.__('Bump up your ad.', 'auctionAdmin').'" id="'.$post->ID.'">'.__('Bump Up', 'auctionAdmin').'</a> | <a href="#" class="delete-auctions" title="'.__('Delete ad listing.', 'auctionAdmin').'" id="'.$post->ID.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';

	endforeach;

	wp_reset_postdata();

} else { ?>
        <tr>
        <td colspan="10"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">
<font color="red"><strong>*</strong></font> <small><?php _e('Use the shortcode to add a buy now button in posts or pages.', 'auctionAdmin'); ?></small>
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
}
}


/*
|--------------------------------------------------------------------------
| MY AUCTION ADS
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_my_auctions() {

include_once(ABSPATH.'/wp-content/plugins/cp-auction-plugin/scripts/pagination.class.php');

	global $current_user, $wpdb, $cp_options, $cpurl;
	$limit = "";
	$act = get_option('cp_auction_plugin_auctiontype');

	get_currentuserinfo();
	$uid = $current_user->ID;

	$type = ""; if ( isset( $_GET['status'] ) ) $type = $_GET['status'];

	if ( empty( $type ) || $type == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'normal' 
		OR wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'reverse')
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'private' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	$items = $wpdb->num_rows;

	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'normal' 
		OR wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'reverse')
		AND wposts.post_status = '$type'
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	$items = $wpdb->num_rows;

	}

	if($items > 0) {
	$get_paging = ""; if ( isset($_GET['paging']) ) $get_paging = $_GET['paging'];
        $p = new pagination;
        $p->items($items);
        $p->limit(20); // Limit entries per page
        $p->target("admin.php?page=cp-my-listings&tab=auctions");
        $p->currentPage($get_paging); // Gets and validates the current page
        $p->calculate(); // Calculates what to show
        $p->parameterName('paging');
        $p->adjacents(4); //No. of page away from the current page

        if(!isset($_GET['paging'])) {
            $p->page = 1;
        } else {
            $p->page = $_GET['paging'];
        }

        //Query for limit paging
        $limit = "LIMIT " . ($p->page - 1) * $p->limit  . ", " . $p->limit;

	if ( empty( $type ) || $type == "all" ) $all = '<strong>'.__('All', 'auctionAdmin').'</strong>';
	else $all = ''.__('All', 'auctionAdmin').'';
	if ( $type == "publish" ) $published = '<strong>'.__('Published', 'auctionAdmin').'</strong>';
	else $published = ''.__('Published', 'auctionAdmin').'';
	if ( $type == "pending" ) $pending = '<strong>'.__('Pending', 'auctionAdmin').'</strong>';
	else $pending = ''.__('Pending', 'auctionAdmin').'';
	if ( $type == "draft" ) $drafts = '<strong>'.__('Drafts', 'auctionAdmin').'</strong>';
	else $drafts = ''.__('Drafts', 'auctionAdmin').'';
	if ( $type == "private" ) $private = '<strong>'.__('Private', 'auctionAdmin').'</strong>';
	else $private = ''.__('Private', 'auctionAdmin').'';
	
?>

	<div class="clear20"></div>

	<a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=auctions&status=all"><?php echo $all; ?> (<?php echo cp_auction_count_my_auctions('all', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=auctions&status=publish"><?php echo $published; ?> (<?php echo cp_auction_count_my_auctions('publish', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=auctions&status=pending"><?php echo $pending; ?> (<?php echo cp_auction_count_my_auctions('pending', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=auctions&status=private"><?php echo $private; ?> (<?php echo cp_auction_count_my_auctions('private', $uid); ?>)</a> &nbsp; | &nbsp; <a style="text-decoration: none" href="admin.php?page=cp-my-listings&tab=auctions&status=draft"><?php echo $drafts; ?> (<?php echo cp_auction_count_my_auctions('draft', $uid); ?>)</a>

<div class="tablenav">
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php

	if ( empty( $type ) || $type == "all" ) {
	$sql2 = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'normal' 
		OR wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'reverse')
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'private' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."'
		ORDER BY wposts.post_date DESC $limit";

	$results = $wpdb->get_results($sql2, OBJECT);
	} else {
	$sql2 = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'normal' 
		OR wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'reverse')
		AND wposts.post_status = '$type'
		AND wposts.post_type = '".APP_POST_TYPE."'
		ORDER BY wposts.post_date DESC $limit";

	$results = $wpdb->get_results($sql2, OBJECT);
	}

	?>
<table class="widefat">
<thead>
    <tr>
        <th><?php _e('ID / Edit', 'auctionAdmin'); ?></th>
        <th><?php _e('Ad Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Buy Now', 'auctionAdmin'); ?></th>
        <th><?php _e('Current Bid', 'auctionAdmin'); ?></th>
        <th><?php _e('Sales', 'auctionAdmin'); ?></th>
        <th><?php _e('Earnings', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Today', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Total', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Shortcode', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</thead>
<tfoot>
    <tr>
        <th><?php _e('ID / Edit', 'auctionAdmin'); ?></th>
        <th><?php _e('Ad Title', 'auctionAdmin'); ?></th>
        <th><?php _e('Buy Now', 'auctionAdmin'); ?></th>
        <th><?php _e('Current Bid', 'auctionAdmin'); ?></th>
        <th><?php _e('Sales', 'auctionAdmin'); ?></th>
        <th><?php _e('Earnings', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Today', 'auctionAdmin'); ?></th>
        <th><?php _e('Views Total', 'auctionAdmin'); ?></th>
        <th><?php _e('Status', 'auctionAdmin'); ?></th>
        <th><?php _e('Shortcode', 'auctionAdmin'); ?></th>
        <th><?php _e('Action', 'auctionAdmin'); ?></th>
    </tr>
</tfoot>
<tbody>

<?php

	if ( $results ) {
	global $post;
    	$rowclass = '';
    	foreach( $results as $post ) : setup_postdata($post);
    	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

            $buy_now = get_post_meta($post->ID,'cp_buy_now',true);
            if ( empty( $buy_now ) ) $buy_now = get_post_meta($post->ID,'cp_price',true);
            $table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	    $howmany = $wpdb->get_var( $wpdb->prepare( "SELECT SUM( numbers ) FROM {$table_name} WHERE pid = '%d'", $post->ID ) );
	    $earnings = $buy_now * $howmany;
	    $total = get_post_meta($post->ID, 'cp_total_count', true);
	    $daily = get_post_meta($post->ID, 'cp_daily_count', true);
	    if( empty( $howmany )) $howmany = 0;
	    $last_bid = cp_auction_plugin_get_latest_bid($post->ID);
	    if( $total < 1 ) $total = 0;
	    if( $daily < 1 ) $daily = 0;

        echo '<tr id="tr_'.$post->ID.'" class="'.$rowclass.'">';
            echo '<td>#<a title="'.__('Click here to edit ad.', 'auctionAdmin').'" href="post.php?post='.$post->ID.'&action=edit">'.$post->ID.'</a></td>';
            echo '<td><a title="'.__('Preview ad listing.', 'auctionAdmin').'" href="'.get_permalink( $post->ID ).'" target="_blank">'.$post->post_title.'</a></td>';
            echo '<td>'.cp_auction_format_amount($buy_now).'</td>';
            echo '<td>'.cp_auction_format_amount($last_bid).'</td>';
            echo '<td>'.$howmany.' '.__('items', 'auctionAdmin').'</td>';
            echo '<td>'.cp_auction_format_amount($earnings).'</td>';
            echo '<td>'.$daily.' '.__('views', 'auctionAdmin').'</td>';
            echo '<td>'.$total.' '.__('views', 'auctionAdmin').'</td>';
            echo '<td><a href="#" class="upd-auction-status" title="'.__('Change post status.', 'auctionAdmin').'" id="'.$post->ID.'">'.$post->post_status.'</a></td>';
            echo '<td>[buy-now-button pid="'.$post->ID.'"]</td>';
            echo '<td><a href="#" class="admin-bump-ads" title="'.__('Bump up your ad.', 'auctionAdmin').'" id="'.$post->ID.'">'.__('Bump Up', 'auctionAdmin').'</a> | <a href="#" class="delete-auctions" title="'.__('Delete ad listing.', 'auctionAdmin').'" id="'.$post->ID.'">'.__('Delete', 'auctionAdmin').'</a></td>';
        echo '</tr>';

	endforeach;

	wp_reset_postdata();

} else { ?>
        <tr>
        <td colspan="11"><?php _e('No Records Found!', 'auctionAdmin'); ?></td>
        </tr>
<?php } ?>
</tbody>
</table>
<div class="tablenav">
<!-- /
<font color="red"><strong>*</strong></font> <small><?php _e('Use the shortcode to add a buy now button in posts or pages.', 'auctionAdmin'); ?></small>
-->
    <div class='tablenav-pages'>
        <?php echo $p->show(); ?>
    </div>
</div>

<?php } else {
	echo '<p>'.__('No Records Found!', 'auctionAdmin').'</p>';
} ?>

<?php
}