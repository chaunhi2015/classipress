<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $post, $cpurl;
    	get_currentuserinfo();
	$uid = $current_user->ID;

	$approve = ""; if(isset($_GET['appr'])) $approve = $_GET['appr'];
	$pid = ""; if(isset($_GET['pid'])) $pid = $_GET['pid'];
	$type = ""; if(isset($_GET['type'])) $type = $_GET['type'];
	else if ( isset( $_REQUEST['cp_auction_my_auction_type'] ) )
	$type = $_REQUEST['cp_auction_my_auction_type'];
	$total = ""; if(isset($_GET['total'])) $total = $_GET['total'];
	$fee = ""; if(isset($_GET['fee'])) $fee = $_GET['fee'];
	$extra = ""; if(isset($_GET['extra'])) $extra = $_GET['extra'];
	$discount = ""; if(isset($_GET['disc'])) $discount = $_GET['disc'];
	$success = ""; if(isset($_GET['success'])) $success = $_GET['success'];
	$txn_id = ""; if(isset($_GET['txn'])) $txn_id = $_GET['txn'];
	$post = get_post($pid);
	$tax = get_user_meta( $post->post_author, 'cp_auction_tax_vat', true );

	delete_user_meta( $uid, 'cp_auction_coupon_code' );

	if(( $success == "ok" ) || ( empty($success) )) {
	$header_title = ''.__('Thank You', 'auctionPlugin').'';
	$breadcrumb = '<a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'">'.__('Dashboard', 'auctionPlugin').'</a> » '.__('Thank You', 'auctionPlugin').'';
	} else {
	$header_title = ''.__('Pending', 'auctionPlugin').'';
	$breadcrumb = '<a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'">'.__('Dashboard', 'auctionPlugin').'</a> » '.__('Pending', 'auctionPlugin').'';
	}
	$pp_url = "https://www.paypal.com";

?>

<?php cp_auction_header($header_title, $breadcrumb); ?>

	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

<?php if( $success == "ok" || empty( $success ) ) { ?>

		<h2 class="dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h2>

		<p><?php echo "".sprintf(__('Thank you for your payment. Your transaction has been completed, and a receipt for your purchase has been emailed to you. You may log into your account at <a href="%s" target="_blank">www.paypal.com</a> to view details of this transaction.','auctionPlugin'), $pp_url ).""; ?></p>

		<?php if( $type == "purchase" || $type == "direct_purchase" ) { ?>

		<div class="clear15"></div>

		<?php if( $fee > 0 ) { ?>

		<div class="price_info">
		<div class="price_info1"><?php _e('Transaction Fee:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( $fee, 'ad', false ); ?></div>
		</div>

		<?php } ?>

		<div class="price_info">
		<div class="price_info1"><?php _e('Paid Total:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><strong><?php echo cp_display_price( $total, 'ad', false ); ?></strong></div>
		</div>

		<?php if( $tax > 0 ) { ?>
		<div class="price_info">
		<div class="price_info1"><?php echo sprintf(__('Hereof %s%s Tax / VAT:', 'auctionPlugin'), $tax, '%'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( cp_auction_taxes( $total, $tax ), 'ad', false ); ?></div>
		</div>	
		<?php } ?>

		<?php if( $txn_id && $txn_id != "N/A" ) { ?>
		<div class="clear15"></div>

		<div class="price_info">
		<div class="price_info1"><?php _e('Transaction ID:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo $txn_id; ?></div>
		</div>
		<?php } ?>

		<?php } if(( $type == "purchase" || $type == "direct_purchase" ) && ( get_option('cp_auction_paypal_approves') == "yes" || $approve == "yes" )) { ?>

		<p><?php _e('<strong>Once your payment has been verified, we will approve your purchase.</strong>', 'auctionPlugin'); ?></p>

		<?php } if( $type == "verification" && get_option('cp_auction_paypal_approves') == "yes" ) { ?>

		<p><?php _e('<strong>An admin must approve the payment before your account gets verified status.</strong>', 'auctionPlugin'); ?></p>

		<?php } if( $type == "credits_purchase" && get_option('cp_auction_paypal_approves') == "yes" ) { ?>

		<p><?php _e('<strong>An admin must approve your payment before the credits are added to your account.</strong>', 'auctionPlugin'); ?></p>

		<?php } ?>

		<div class="clear30"></div>

		<p style="text-align: center"><a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1'); ?>" class="btn_orange"><?php _e('Proceed >>','auctionPlugin'); ?></a></p>

<?php } elseif( $success == "cancel" ) { ?>

		<h1 class="single dotted"><?php _e('Your payment was canceled!', 'auctionPlugin'); ?></h1>

		<p><?php _e('If there were problems then please contact the administration.', 'auctionPlugin'); ?></p>

<?php } elseif( $success == "author" ) { ?>

		<h1 class="single dotted"><?php _e('This is not possible!', 'auctionPlugin'); ?></h1>

		<p><?php _e('Sorry, you can not buy your own products.', 'auctionPlugin'); ?></p>

<?php } elseif( $success == "token" ) { ?>

		<h1 class="single dotted"><?php _e('Something went wrong!', 'auctionPlugin'); ?></h1>

		<p><?php _e('Sorry but there seems to be a problem with the seller`s account setup, or something has gone wrong during the automatic approval here with us or with PayPal. Please contact the seller or the site administration directly.', 'auctionPlugin'); ?></p>

<?php } elseif( $success == "txn" ) { ?>

		<h1 class="single dotted"><?php _e('Something went wrong!', 'auctionPlugin'); ?></h1>

		<p><?php _e('Sorry but this transaction is already registered, or something has gone wrong during the automatic approval here with us or with PayPal. Please contact the site administration.', 'auctionPlugin'); ?></p>

<?php } elseif( $success == "mail" ) { ?>

		<h1 class="single dotted"><?php _e('Something went wrong!', 'auctionPlugin'); ?></h1>

		<p><?php _e('Sorry but there is a problem with the seller`s paypal email, or something has gone wrong during the automatic approval here with us or with PayPal. Please contact the seller or the site administration directly.', 'auctionPlugin'); ?></p>

<?php } elseif( $success == "pdt" ) { ?>

		<h1 class="single dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h1>

		<p><?php _e('Thank you for your payment. Once your payment has been verified, we will approve your purchase.', 'auctionPlugin'); ?></p>
		
		<p><?php _e('If you have any questions about the process please contact the administration or the seller directly.', 'auctionPlugin'); ?></p>

<?php } elseif( $success == "cih" ) { ?>

		<h1 class="single dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h1>

		<p><?php _e('Thank you for your purchase. Please contact the seller to arrange a time to collect your items and to pay them cash in hand.', 'auctionPlugin'); ?></p>
		
		<p><?php _e('If you have any questions regarding this process please contact the seller via email from your dashboard, or via the contact details provided in the confirmation email you now should have received.', 'auctionPlugin'); ?></p>


<?php } elseif( $success == "cod" ) { ?>

		<h1 class="single dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h1>

		<p><?php _e('Thank you for your order. Once your payment has been verified, we will approve your purchase.', 'auctionPlugin'); ?></p>
		
		<p><?php _e('If you have any questions regarding this process please contact the seller via email from your dashboard, or via the contact details provided in the confirmation email you now should have received.', 'auctionPlugin'); ?></p>

<?php } else { ?>

		<h1 class="single dotted"><?php _e('Was there anything that went wrong?', 'auctionPlugin'); ?></h1>

		<p><?php _e('The administration has decided to authorize the payment before the purchase is approved, or something has gone wrong during the automatic approval here with us or with PayPal. Please contact the site administration.', 'auctionPlugin'); ?></p>

<?php } ?>

<?php if( $success && $success != "ok" ) { ?>

<div class="clear30"></div>

		<p style="text-align: center"><a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1'); ?>" class="btn_orange"><?php _e('Proceed >>','auctionPlugin'); ?></a></p>

<?php } ?>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>