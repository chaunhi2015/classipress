<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');
	include 'paypal.class.php';

	if( !is_user_logged_in() ) {
	wp_redirect( home_url('/login/') );
	exit;
	}

/*
|--------------------------------------------------------------------------
| THE PAYPAL CLASS
|--------------------------------------------------------------------------
*/

	global $post, $wpdb, $cp_options;

	$arg = false; if(isset($_POST['args'])) $arg = unserialize( base64_decode( $_POST['args'] ) );
	$credits = false; if(isset($_POST['credits'])) $credits = $_POST['credits'];
	$type = false; if( $arg ) $type = $arg['type'];

	$post_author = false; if( isset( $arg['post_author'] ) ) $post_author = $arg['post_author'];
	$order_id = false; if( isset( $arg['order_id'] ) ) $order_id = $arg['order_id'];
	$txn_id = false; if( isset( $arg['txn_id'] ) ) $txn_id = $arg['txn_id'];
	$quantity = false; if( isset( $arg['quantity'] ) ) $quantity = $arg['quantity'];
	$cp_shipping = false; if( isset( $arg['cp_shipping'] ) ) $cp_shipping = $arg['cp_shipping'];
	$moved_credits = false; if( isset( $arg['moved_credits'] ) ) $moved_credits = $arg['moved_credits'];
	$available = false; if( isset( $arg['available'] ) ) $available = $arg['available'];
	$cc = false; if( isset( $arg['mc_currency'] ) ) $cc = $arg['mc_currency'];
	$enabl = get_option( 'cp_auction_enable_conversion' );

	if( ( $type ) && ( $type == "direct_purchase" || $type == "bump_ad" || $type == "upgrade" ) )
	$post = get_post($arg['post_id']);

	if( ( $type ) && ( $type == "credits_purchase" || $type == "verification" || $type == "bump_ad" || $type == "upgrade" ) )
	$cc = $cp_options->currency_code;
	else $cc = get_user_meta( $post_author, 'currency', true );
	if( empty($cc) || $cc == '-1' ) $cc = $cp_options->currency_code;

	if ( $enabl == "yes" ) $cc = get_option( 'cp_auction_convert_to' );

	$action = ""; if(isset($_GET['action'])) $action = $_GET['action'];
	if( $arg ) $uid = $arg['uid'];

	$p = new paypal_class;
	if( get_option('cp_auction_plugin_sandbox') == "yes" ) {
	$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	} else {
	$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';
	}
	$this_script = get_bloginfo('wpurl').'/?_process_payment=1';

	if(empty($action)) $action = 'process';

	switch ($action) {

   	case 'process': // Process and order...

	if ( $uid == $post_author ) {
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&success=author');
	exit;
	}

/*
|--------------------------------------------------------------------------
| PAYPAL DIRECT PAYMENT - INSERT INTO PURCHASE TABLE
|--------------------------------------------------------------------------
*/

	if( get_option('cp_auction_enable_paypal_direct') == "yes" && $arg['type'] == "direct_purchase" ) {

	$author_approves = get_user_meta( $post_author, 'cp_author_approves', true );
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) $approves = "unpaid";
	else $approves = "paid";

	$tm = time();
	$mc_fee = $arg['mc_fee'];
	$mc_gross = $arg['mc_gross'];
	$taxes = get_user_meta( $post_author, 'cp_auction_tax_vat', true );
	$tax = cp_auction_taxes( $arg['mc_gross'], $taxes );
	$discount = 0;
	$howmany = get_post_meta($post->ID, 'cp_auction_howmany', true);
	$quantity = 1;

	if( $howmany > 0 ) {
	if( $quantity > $howmany ) $quantity = $howmany;
	$how_many = $howmany - $quantity;
	update_post_meta($post->ID,'cp_auction_howmany',$how_many);
	if( $how_many < 1 ) {
	$my_post = array();
	$my_post['ID'] = $post->ID;
	$my_post['post_status'] = 'draft';
	wp_update_post( $my_post );

	update_post_meta($post->ID,'cp_ad_sold','yes');
	update_post_meta($post->ID,'cp_ad_sold_date',$tm);
	}
	}
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$unpaid_total = $mc_gross;
	$query = "INSERT INTO {$table_name} (pid, uid, buyer, item_name, quantity, amount, discount, shipping, mc_fee, mc_gross, tax, datemade, numbers, unpaid, net_total, unpaid_total, type, status) VALUES (%d, %d, %d, %s, %d, %f, %f, %f, %f, %f, %d, %d, %d, %f, %f, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $uid, $post->post_title, $quantity, $arg['net_total'], $discount, $cp_shipping, $mc_fee, $mc_gross, $tax, $tm, $quantity, $quantity, $arg['net_total'], $unpaid_total, 'classified', $approves));

	cp_auction_buy_now_emails( $post->ID, $post, $uid, $mc_gross );
}


/*
|--------------------------------------------------------------------------
| PURCHASE CREDITS
|--------------------------------------------------------------------------
*/

	$aws = false;
	if ( $arg['type'] == "credits_purchase" ) {
	if ( $arg['credit_value'] > '0' ) {
	$value = $credits * $arg['credit_value'];
	} else {
	$value = $credits * get_option('cp_auction_plugin_credit_value');
	}
	$mc_gross = cp_auction_calculate_fees($value, 'paypal', get_option('cp_auction_paypal_fee'));
	if( $mc_gross > $value ) $mc_fee = $mc_gross - $value;
	else $mc_fee = $value - $mc_gross;
	if( get_option('cp_auction_paypal_fee_type') == "static_off" ) $mc_gross = $value;
	elseif( get_option('cp_auction_paypal_fee_type') == "percentage_off" ) $mc_gross = $value;

	$arg['mc_gross'] = $mc_gross;
	$arg['mc_fee'] = $mc_fee;
	$aws = "aws-credits";
	$moved_credits = cp_auction_sanitize_amount($credits);
	$user_available = get_user_meta( $uid, 'cp_credits', true );
	$available = cp_auction_sanitize_amount($user_available + $credits);
	}


/*
|--------------------------------------------------------------------------
| INSERT INTO TRANSACTION TABLE
|--------------------------------------------------------------------------
*/

	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$reg = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$order_id' AND uid = '$uid' AND status = 'pending'");

	$release = get_option( 'cp_auction_paypal_release' );
	$author_approves = get_user_meta( $post_author, 'cp_author_approves', true );
	$approves = "complete"; if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) $approves = "pending";
	else if ( $approves == "complete" && $release == "yes" ) $approves = "complete";
	else $approves = "pending";

	if( $reg ) {
	$lastid = $reg->id;
	} elseif(( ! $reg ) && ( $arg['type'] == "credits_purchase" || $arg['type'] == "verification" || $arg['type'] == "bump_ad" || $arg['type'] == "upgrade" )) {

	$query = "INSERT INTO {$table_trans} (pid, datemade, uid, ip_address, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, receiver_email, address_country, address_state, address_country_code, address_zip, address_street, moved_credits, available, quantity, payment_option, shipping, mc_fee, mc_gross, aws, type, status) VALUES (%d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %f, %f, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $arg['post_id'], $arg['datemade'], $arg['uid'], $arg['ip_address'], $arg['payment_date'], $txn_id, $arg['item_name'], $arg['trans_type'], $arg['mc_currency'], $arg['last_name'], $arg['first_name'], $arg['payer_email'], $arg['receiver_email'], 'N/A', 'N/A', 'N/A', $arg['address_zip'], $arg['address_street'], $moved_credits, $available, $quantity, $arg['howto_pay'], $cp_shipping, $arg['mc_fee'], $arg['mc_gross'], $aws, $arg['type'], $approves));
	$lastid = $wpdb->insert_id;

	} elseif(( ! $reg ) && ( $arg['type'] == "purchase" || $arg['type'] == "direct_purchase" )) {

	$query = "INSERT INTO {$table_trans} (order_id, pid, post_ids, datemade, uid, post_author, ip_address, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, receiver_email, address_country, address_state, address_country_code, address_zip, address_street, moved_credits, available, quantity, payment_option, shipping, mc_fee, mc_gross, aws, type, status) VALUES (%d, %d, %s, %d, %d, %d, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %f, %f, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, $arg['post_id'], $arg['post_ids'], $arg['datemade'], $arg['uid'], $post_author, $arg['ip_address'], $arg['payment_date'], $txn_id, $arg['item_name'], $arg['trans_type'], $arg['mc_currency'], $arg['last_name'], $arg['first_name'], $arg['payer_email'], $arg['receiver_email'], $arg['address_country'], $arg['address_state'], 'N/A', $arg['address_zip'], $arg['address_street'], $moved_credits, $available, $quantity, $arg['howto_pay'], $cp_shipping, $arg['mc_fee'], $arg['mc_gross'], 'N/A', $arg['type'], $approves));
	$lastid = $wpdb->insert_id;

}

	if( $arg['type'] == "purchase" || $arg['type'] == "direct_purchase" ) {
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('order_id' => $order_id);
	$wpdb->update($table_purchases, array('trans_id' => $lastid), $where_array);
	}

	if ( $approves == "complete" ) {

	$tx_token = ''.__('N/A','auctionPlugin').'';
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$lastid'");
	$uid = $res->uid;
	$sid = $res->post_author;
	$type = $res->type;
	$pid = $res->post_ids;

	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	
	$tm = time();

	if( $arg['type'] == "purchase" ) {
	if ( $pid ) {
	$post_ids = explode(",",$pid);
	foreach($post_ids as $post_id) {

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $lastid, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	}
	}
	
	}

	if( $arg['type'] == "direct_purchase" ) {
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('order_id' => $sid, 'trans_id' => $lastid, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	}

}


/*
|--------------------------------------------------------------------------
| SUBMIT THE INFORMATION TO PAYPAL
|--------------------------------------------------------------------------
*/

	$item = ''.__('Order ID:','auctionPlugin').' '.$order_id.'';
        $p->add_field('charset', 'utf-8');
	$p->add_field('business', $arg['receiver_email']);
	$p->add_field('currency_code', $cc);
	$p->add_field('return', $this_script.'&action=success&trans='.$lastid.'&oid='.$order_id.'');
	$p->add_field('cancel_return', $this_script.'&action=cancel&uid='.$uid.'');
	$p->add_field('notify_url', $this_script.'&action=ipn');
	if( $arg['type'] == "purchase" ) {
	$p->add_field('item_name', $item);
	$p->add_field('item_number', $order_id);
	$p->add_field('custom', $lastid.'|'.$quantity.'|'.$post_author.'|'.$arg['post_ids'].'|'.$uid.'|'.$arg['type']);
	} else {
	$p->add_field('item_name', $arg['item_name']);
	$p->add_field('item_number', $order_id);
	$p->add_field('custom', $lastid.'|'.$quantity.'|'.$order_id.'|'.$post->ID.'|'.$uid.'|'.$arg['type']);
	}
	if ( isset( $arg['conversion'] ) && $arg['conversion'] > '0' ) {
	$p->add_field('amount', cp_auction_format_amount($arg['conversion'], 'process'));
	} else {
	$p->add_field('amount', cp_auction_format_amount($arg['mc_gross'], 'process'));
	}
	if( $arg['type'] == 'purchase' || $arg['type'] == 'direct_purchase' ) {
	$lc = get_user_meta( $post_author, 'paypal_lang', true );
	$p->add_field('lc', $lc);
	} else {
	$p->add_field('lc', get_option('cp_auction_paypal_lc'));
	}

	$p->submit_paypal_post(); // submit the fields to paypal

	exit();

/*
|--------------------------------------------------------------------------
| GRAB THE RESULT FROM PAYPAL AND UPDATE TRANSACTIONS
|--------------------------------------------------------------------------
*/

   	case 'success': // Order was successful...

	global $post, $wpdb;
	$tx_token = ""; if( isset( $_GET['tx'] ) ) $tx_token = $_GET['tx'];
	$item_number = ""; if( isset( $_GET['item_number'] ) ) $item_number = $_GET['item_number'];
	if( ! $item_number && isset( $_GET['oid'] ) ) $item_number = $_GET['oid'];
	$trans = ""; if( isset( $_GET['trans'] ) ) $trans = $_GET['trans'];

	if( ! $tx_token && $trans > 0 ) {
	$ids = $trans;
	$tx_token = ''.__('N/A','auctionPlugin').'';
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$ids'");
	$uid = $res->uid;
	$sid = $res->post_author;
	$type = $res->type;
	$pid = $res->post_ids;
	$author_approves = get_user_meta( $sid, 'cp_author_approves', true );
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	
	$tm = time();

	if( $type == "credits_purchase" || $type == "verification" || $type == "bump_ad" || $type == "upgrade" ) {
	cp_auction_confirm_payment_email( $uid, $sid );
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type='.$type.'&success=pdt');
	exit;
	}

	if( $type == "purchase" ) {
	if ( $pid ) {
	$post_ids = explode(",",$pid);
	foreach($post_ids as $post_id) {

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $ids, 'unpaid' => 0, 'paid' => 2, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	} else {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $ids, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$wheres_array = array('id' => $ids);
	$wpdb->update($table_transactions, array('txn_id' => $tx_token, 'status' => 'complete'), $wheres_array);
	}
	}
	}
	
}
	if( $type == "direct_purchase" ) {
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('order_id' => $sid, 'trans_id' => $ids, 'unpaid' => 0, 'paid' => 2, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	} else {
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('order_id' => $sid, 'trans_id' => $ids, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$wheres_array = array('id' => $ids);
	$wpdb->update($table_transactions, array('txn_id' => $tx_token, 'status' => 'complete'), $wheres_array);
	}

}
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$approve = "yes";
	cp_auction_confirm_payment_email($uid, $res->uid, $item_number, $num, $res->mc_fee, $res->mc_gross, $tx_token);
	} else {
	$approve = "no";
	cp_auction_paypal_items_email( $item_number, $res->uid, $uid, $num, $res->mc_fee, $res->mc_gross, $tx_token);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($res->order_id);
	}
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&appr='.$approve.'&total='.$res->mc_gross.'&fee='.$res->mc_fee.'&txn='.$tx_token.'&type='.$type.'&success=ok');
	exit;
}

   	if(isset($_GET['cm'])) {
   	$custs 	= $_GET['cm'];
	$cust 	= explode("|",$custs);
	$ids	= $cust[0];
	$num	= $cust[1];
	$sid	= $cust[2];
	$pid	= $cust[3];
	$uid	= $cust[4];
	$type	= $cust[5];
	$author = $cust[2];
	if( $type == "direct_purchase" || $type == "bump_ad" || $type == "upgrade" ) {
	global $post;
	$post = get_post($pid);
	$author = $post->post_author;
	}
	$author_approves = get_user_meta( $author, 'cp_author_approves', true );
	}

	$pdt = get_option('cp_auction_plugin_pp_pdt');
	$mode = get_option('cp_auction_payment_mode');
	$author_mode = get_user_meta( $author, 'cp_author_payment_mode', true );
	if(( $type == "credits_purchase" || $type == "verification" || $type == "bump_ad" || $type == "upgrade" ) && ( $pdt != "yes" )) {
	cp_auction_confirm_payment_email( $uid, $author );
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type='.$type.'&success=pdt');
	exit;
	}
	elseif(( $type == "purchase" || $type == "direct_purchase" ) && ( $mode == "simple" || $author_mode == "simple" )) {
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$ids'");
	$tm = time();

	if( $type == "purchase" ) {
	if ( $pid ) {
	$post_ids = explode(",",$pid);
	foreach($post_ids as $post_id) {

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $ids, 'unpaid' => 0, 'paid' => 2, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	} else {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $ids, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$wheres_array = array('id' => $ids);
	$wpdb->update($table_transactions, array('txn_id' => $tx_token, 'status' => 'complete'), $wheres_array);
	}
	}
	}
	
}
	if( $type == "direct_purchase" ) {
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $post->post_author, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('order_id' => $sid, 'trans_id' => $ids, 'unpaid' => 0, 'paid' => 2, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	} else {
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $post->post_author, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('order_id' => $sid, 'trans_id' => $ids, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$wheres_array = array('id' => $ids);
	$wpdb->update($table_transactions, array('txn_id' => $tx_token, 'status' => 'complete'), $wheres_array);
	}

}
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	cp_auction_confirm_payment_email($uid, $res->uid, $item_number, $num, $res->mc_fee, $res->mc_gross, $tx_token);
	} else {
	cp_auction_paypal_items_email( $item_number, $res->uid, $uid, $num, $res->mc_fee, $res->mc_gross, $tx_token);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($res->order_id);
	}
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&oid='.$item_number.'&total='.$res->mc_gross.'&fee='.$res->mc_fee.'&txn='.$tx_token.'&type='.$type.'&success=ok');
	exit;
}


/*
|--------------------------------------------------------------------------
| BELOW ACTIONS ONLY WORKS IF PDT IS ENABLED WITH ADMIN AND AUTHOR
|--------------------------------------------------------------------------
*/

	if( $type == "credits_purchase" || $type == "verification" || $type == "bump_ad" || $type == "upgrade" ) {
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$rmail = $wpdb->get_row("SELECT receiver_email FROM {$table_transactions} WHERE id = '$ids'");
	}

	if( $type == "purchase" ) {
	$txn_token = get_user_meta( $sid, 'cp_auction_plugin_identity_token', true );
	}
	elseif( $type == "direct_purchase" ) {
	$txn_token = get_user_meta( $post->post_author, 'cp_auction_plugin_identity_token', true );
	}
	else {
	$txn_token = get_option('cp_auction_identity_token');
	}

	if ( !$txn_token ) {
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_failed_payment_email($uid, $author, 'token');
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type='.$type.'&success=token');
	exit;
	}

	// read the post from PayPal system and add 'cmd'
	$req = 'cmd=_notify-synch';

	$auth_token = $txn_token;
	$req .= "&tx=$tx_token&at=$auth_token";

	// post back to PayPal system to validate
	$header = '';
	$header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
	$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
	$header .= "Content-Length: " . strlen($req) . "\r\n\r\n";
	if( get_option('cp_auction_plugin_sandbox') == "yes" ) {
	$fp = fsockopen ('www.sandbox.paypal.com', 80, $errno, $errstr, 30);
	} else {
	$fp = fsockopen ('www.paypal.com', 80, $errno, $errstr, 30);
	}
	// If possible, securely post back to paypal using HTTPS
	// Your PHP server will need to be SSL enabled
	// $fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);

	if (!$fp) 
	{
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_failed_payment_email($uid, $author, 'postback');
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type='.$type.'&success=no');
	exit;
	}
	else 
	{
	fputs ($fp, $header . $req);
	// read the body data 
	$res = '';
	$headerdone = false;
	while (!feof($fp)) 
	{
	$line = fgets ($fp, 1024);
        if (strcmp($line, "\r\n") == 0) {
            // read the header
            $headerdone = true;
        }
        else if ($headerdone)
        {
            // header has been read. now read the contents
            $res .= $line;
            if( get_option('cp_auction_paypal_debugger') == "yes" ) {
            echo $line;
            }
        }
    }

        // parse the data
        $lines = explode("\n", $res);
        $keyarray = array();
        if (strcmp ($lines[0], "SUCCESS") == 0) 
        {
            for ($i=1; $i<count($lines);$i++)
            {
                list($key,$val) = array_pad(explode("=", $lines[$i]),2,null);
                $keyarray[urldecode($key)] = urldecode($val);
            }

	$processor_fee = cp_auction_sanitize_amount($keyarray['payment_fee']);
	$custom	= $keyarray['custom'];
	$cust	= explode("|",$custom);
	$ids	= $cust[0];
	$num	= $cust[1];
	$sid	= $cust[2];
	$pid	= $cust[3];
	$uid	= $cust[4];
	$type	= $cust[5];
	if( $type == "direct_purchase" || $type == "bump_ad" || $type == "upgrade" )
	global $post;
	$post = get_post($pid);

	$order_id = $keyarray['item_number'];
	if( $type == "direct_purchase" ) $order_id = $sid;

	if( $ids > 0 ) {
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$txn = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE txn_id = '$tx_token'");
	if( $txn ) {
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_failed_payment_email($uid, $author, 'txn');
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type='.$type.'&success=txn');
	exit;
	}

	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $ids);
	$wpdb->update($table_transactions, array('txn_id' => $tx_token, 'last_name' => $keyarray['last_name'], 'first_name' => $keyarray['first_name'], 'payer_email' => $keyarray['payer_email'], 'address_country' => $keyarray['address_country'], 'address_state' => $keyarray['address_state'], 'address_country_code' => $keyarray['address_country_code'], 'address_zip' => $keyarray['address_zip'], 'address_street' => $keyarray['address_street'], 'processor_fee' => $processor_fee), $where_array);
	if( $type == "verification" ) {
	update_user_meta( $uid, 'cp_auction_account_verified', '2' );
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=verification&success=pending');
	exit;
	}
	if( $type == "credits_purchase" ) {
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=credits_purchase&success=ok');
	exit;
	}
	if( $type == "bump_ad" ) {
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=bump_ad&success=pending');
	exit;
	}
	if( $type == "upgrade" ) {
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=upgrade&success=pending');
	exit;
	}
}
else {
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $ids);
	$wpdb->update($table_transactions, array('txn_id' => $tx_token, 'last_name' => $keyarray['last_name'], 'first_name' => $keyarray['first_name'], 'payer_email' => $keyarray['payer_email'], 'address_country' => $keyarray['address_country'], 'address_state' => $keyarray['address_state'], 'address_country_code' => $keyarray['address_country_code'], 'address_zip' => $keyarray['address_zip'], 'address_street' => $keyarray['address_street'], 'processor_fee' => $processor_fee, 'status' => 'complete'), $where_array);
	if( $type == "verification" ) {
	update_user_meta( $uid, 'cp_auction_account_verified', '1' );
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=verification&success=ok');
	exit;
	}
	if( $type == "credits_purchase" ) {
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$ids'");
	if( $res )
	update_user_meta( $res->uid, 'cp_credits', $res->available );
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=credits_purchase&success=ok');
	exit;
	}
	if( $type == "bump_ad" ) {
	$ext = get_option('cp_auction_bump_extend_exp');
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$todayDate = current_time('mysql');
	$table_posts = $wpdb->prefix . "posts";
	$where_array = array('ID' => $post->ID);
	$wpdb->update($table_posts, array('post_date' => $todayDate), $where_array);

	if ( ( $ext > 0 ) && ( empty( $my_type ) || $my_type == "classified" || $my_type == "wanted" ) ) {
	$exp_date = get_post_meta( $post->ID, 'cp_sys_expire_date', true );
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$exp_date + $ext days"), true);
	update_post_meta( $post->ID, 'cp_sys_expire_date', $cp_sys_expire_date );
	}

	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=bump_ad&success=ok');
	exit;
	}
	if( $type == "upgrade" ) {
	$ext = get_option('cp_auction_upgrade_extend_exp');
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	stick_post($post->ID);

	if ( ( $ext > 0 ) && ( empty( $my_type ) || $my_type == "classified" || $my_type == "wanted" ) ) {
	$exp_date = get_post_meta( $post->ID, 'cp_sys_expire_date', true );
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$exp_date + $ext days"), true);
	update_post_meta( $post->ID, 'cp_sys_expire_date', $cp_sys_expire_date );
	}
	
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=upgrade&success=ok');
	exit;
	}
}
}


/*
|--------------------------------------------------------------------------
| DIRECT ITEM PURCHASE - DIRECT PURCHASE
|--------------------------------------------------------------------------
*/

	if( $type == "direct_purchase" ) {

	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$ids'");

	$tm = time();
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $post->post_author, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('order_id' => $sid, 'trans_id' => $ids, 'unpaid' => 0, 'paid' => 2, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	} else {
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $post->post_author, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('order_id' => $sid, 'trans_id' => $ids, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($res->order_id);
	}

	cp_auction_paypal_direct_items_email($post->ID, $post, $uid, $res->mc_fee, $res->mc_gross, $tx_token);
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&pid='.$post->ID.'&total='.$res->mc_gross.'&fee='.$res->mc_fee.'&txn='.$tx_token.'&type=purchase&success=ok');
	exit;
}


/*
|--------------------------------------------------------------------------
| SHOPPING CART ITEM PURCHASE - PURCHASE
|--------------------------------------------------------------------------
*/

	if( $type == "purchase" ) {

	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$ids'");

	if ( $pid ) {
	$post_ids = explode(",",$pid);
	foreach($post_ids as $post_id) {
	$tm = time();
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $ids, 'unpaid' => 0, 'paid' => 2, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	} else {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $ids, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($res->order_id);
	}
	}
	}
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_paypal_items_email($order_id, $sid, $uid, $res->quantity, $res->mc_fee, $res->mc_gross, $tx_token);
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&total='.$res->mc_gross.'&fee='.$res->mc_fee.'&txn='.$tx_token.'&type=purchase&success=ok');
	exit;
}


        }
        else if (strcmp ($lines[0], "FAIL") == 0 && get_option('cp_auction_paypal_debugger') == "yes" ) {
            echo "Failure: " . $lines[0];
            exit;
            // log for manual investigation
        }
}

fclose ($fp);

exit();


/*
|--------------------------------------------------------------------------
| PAYMENT PROCESS CANCELED
|--------------------------------------------------------------------------
*/

   case 'cancel': // Order was canceled...

	if( isset($_GET['uid']) ) {
	delete_user_meta( $_GET['uid'], 'cp_auction_coupon_code' );
	cp_auction_failed_payment_email($_GET['uid']);
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&success=cancel');
	exit;
	}

   case 'ipn':        

	exit;
}
?>