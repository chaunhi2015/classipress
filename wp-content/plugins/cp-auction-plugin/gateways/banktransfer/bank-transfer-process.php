<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	global $post, $wpdb;

	if( !is_user_logged_in() ) {
	wp_redirect( home_url('/login/') );
	exit;
	}

if(isset($_POST['submit_bank_transfer'])) {

	$arg = false; if(isset($_POST['args'])) $arg = unserialize( base64_decode( $_POST['args'] ) );
	$order_id = false; if( isset( $arg['order_id'] ) ) $order_id = $arg['order_id'];
	$uid = false; if( isset( $arg['uid'] ) ) $uid = $arg['uid'];

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$reg = $wpdb->get_row("SELECT * FROM {$table_name} WHERE order_id = '$order_id' AND uid = '$uid' AND status = 'pending'");

	if( $reg ) {
	$lastid = $reg->id;
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $lastid);
	$wpdb->update($table_transactions, array('trans_type' => $arg['trans_type'], 'payment_option' => $arg['howto_pay']), $where_array);
	} else {

	$approves = "pending";

	$query = "INSERT INTO {$table_name} (order_id, post_ids, datemade, uid, post_author, ip_address, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, receiver_email, address_country, address_state, address_country_code, address_zip, address_street, moved_credits, available, quantity, payment_option, shipping, mc_fee, mc_gross, aws, type, status) VALUES (%d, %s, %d, %d, %d, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %f, %f, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, $arg['post_ids'], $arg['datemade'], $uid, $arg['post_author'], $arg['ip_address'], $arg['payment_date'], $arg['txn_id'], '', $arg['trans_type'], $arg['mc_currency'], $arg['last_name'], $arg['first_name'], $arg['payer_email'], $arg['receiver_email'], $arg['address_country'], $arg['address_state'], 'N/A', $arg['address_zip'], $arg['address_street'], '', '', $arg['quantity'], $arg['howto_pay'], $arg['cp_shipping'], $arg['mc_fee'], $arg['mc_gross'], 'N/A', $arg['type'], $approves));
	$lastid = $wpdb->insert_id;
	}

	$tm = time();
	$post_ids = $arg['post_ids'];
	$pids = explode(",",$post_ids);
	foreach($pids as $pid) {
	$post = get_post($pid);
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $post->post_author, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $lastid, 'paid' => 2), $where_array);
	}

	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_cih_cod_email($order_id, $arg['post_author'], $uid, $arg['quantity'], $arg['mc_fee'], $arg['mc_gross'], $arg['howto_pay']);
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=9&txn='.$arg['txn_id'].'&uid='.$arg['post_author'].'&total='.$arg['mc_gross'].'&fee='.$arg['mc_fee'].'&type='.$arg['type'].'&success='.$arg['howto_pay'].'');
	exit;
}