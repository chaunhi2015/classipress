<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $post, $cpurl, $cp_options;
    	get_currentuserinfo();
	$uid = $current_user->ID;

	$credits = ""; if(isset($_GET['cre'])) $credits = $_GET['cre'];
	$total = ""; if(isset($_GET['total'])) $total = $_GET['total'];
	$fee = "" ; if(isset($_GET['fee'])) $fee = $_GET['fee'];
	$txn_id = ""; if(isset($_GET['txn'])) $txn_id = $_GET['txn'];
	$value = get_option('cp_auction_plugin_credit_value') * $credits;

	$cc = $cp_options->currency_code;

	$header_title = ''.__('Banktransfer', 'auctionPlugin').'';
	$breadcrumb = '<a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'">'.__('Dashboard', 'auctionPlugin').'</a> » '.__('Banktransfer', 'auctionPlugin').'';

?>

<?php cp_auction_header($header_title, $breadcrumb); ?>

	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

	<h2 class="dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h2>

	<p><?php _e('Please include the following details when sending the bank transfer. Once your transfer has been verified, we will approve your credit purchase.', 'auctionPlugin'); ?></p>

	<div class="clear15"></div>

	<div class="price_info">
		<div class="price_info1"><?php _e('Number of Credits:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><strong><?php echo $credits; ?></strong> <?php _e('credits', 'auctionPlugin'); ?></div>
	</div>

	<div class="price_info">
		<div class="price_info1"><?php _e('Credits Value in', 'auctionPlugin'); ?> <?php echo $cp_options->currency_code; ?>:</div>
		<div class="price_info2"><?php echo cp_display_price( $value, 'ad', false ); ?></div>
	</div>

	<?php if( $fee > 0 ) { ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Fee for Bank Transfer:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( $fee, 'ad', false ); ?></div>
	</div>

	<?php } ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Total to Pay with Bank Transfer:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><strong><?php echo cp_display_price( $total, 'ad', false ); ?></strong></div>
	</div>

	<?php if( $cp_options->tax_charge > 0 ) { ?>
	<div class="price_info">
		<div class="price_info1"><?php echo sprintf(__('Hereof %s%s Tax / VAT:', 'auctionPlugin'), $cp_options->tax_charge, '%'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( cp_auction_taxes( $total, $cp_options->tax_charge ), 'ad', false ); ?></div>
	</div>	
	<?php } ?>

	<div class="clear15"></div>

	<div class="price_info">
		<div class="price_info1"><?php _e('Transaction ID:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo $txn_id; ?></div>
	</div>

	<div class="clear30"></div>

<?php


	if(get_option('cp_auction_plugin_bank_info') == "") {
	if( get_option('cp_version') < "3.3" ) {
	echo '<div class="cp_terms">'.get_option('cp_bank_instructions').'</div><br /><br /><p style="text-align: center"><a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'" class="btn_orange">'.__('Proceed >>','auctionPlugin').'</a></p>';
        } elseif( get_option('cp_version') > "3.2.1" ) {
	echo '<div class="cp_terms">'.$cp_options->gateways['bank-transfer']['message'].'</div><br /><br /><p style="text-align: center"><a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'" class="btn_orange">'.__('Proceed >>','auctionPlugin').'</a></p>';
	}
	} else {
	echo '<div class="cp_terms">'.get_option('cp_auction_plugin_bank_info').'</div><br /><br /><p style="text-align: center"><a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'" class="btn_orange">'.__('Proceed >>','auctionPlugin').'</a></p>';
	}

?>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>