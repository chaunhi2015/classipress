<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	global $wpdb;

	if( !is_user_logged_in() ) {
	wp_redirect( home_url('/login/') );
	exit;
	}

if(isset($_POST['submit_purchase'])) {

	$arg = ""; if(isset($_POST['args'])) $arg = unserialize( base64_decode( $_POST['args'] ) );
	$credits = ""; if(isset($_POST['credits'])) $credits = $_POST['credits'];
	$total = $credits * get_option('cp_auction_plugin_credit_value');
	$available = get_user_meta( $arg['uid'], 'cp_credits', true );
	$mc_gross = cp_auction_calculate_fees($total, 'bank_transfer', get_option('cp_auction_plugin_bt_fee'));
	if( $mc_gross > $total ) $mc_fee = $mc_gross - $total;
	else $mc_fee = $total - $mc_gross;
	$txn_id = $arg['txn_id'];

	if( get_option('cp_auction_banktransfer_fee_type') == "static_off" ) $mc_gross = $total;
	elseif( get_option('cp_auction_banktransfer_fee_type') == "percentage_off" ) $mc_gross = $total;

	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$txn = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE txn_id = '$txn_id'");
	if( $txn ) {
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_failed_payment_email($uid, $author, 'txn');
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type=credits_purchase&success=txn');
	exit;
	}

	$query = "INSERT INTO {$table_transactions} (pid, datemade, uid, ip_address, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, address_zip, address_street, moved_credits, available, quantity, payment_option, shipping, mc_fee, mc_gross, aws, type, status) VALUES (%d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %f, %f, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, '0', $arg['datemade'], $arg['uid'], $arg['ip_address'], $arg['payment_date'], $txn_id, $arg['item_name'], $arg['trans_type'], $arg['mc_currency'], $arg['last_name'], $arg['first_name'], $arg['payer_email'], $arg['address_zip'], $arg['address_street'], $credits, $available, $credits, 'bank_transfer', '0', $mc_fee, $mc_gross, 'aws-credits', 'credits_purchase', 'pending'));

	wp_redirect(get_bloginfo('wpurl').'/?confirmation=2&cre='.$credits.'&total='.$mc_gross.'&fee='.$mc_fee.'&txn='.$arg['txn_id'].'');
	exit;
}