<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $post, $cpurl, $cp_options;
    	get_currentuserinfo();
	$uid = $current_user->ID;

	$total = false; if(isset($_GET['total'])) $total = $_GET['total'];
	$fee = false; if(isset($_GET['fee'])) $fee = $_GET['fee'];
	$txn_id = false; if(isset($_GET['txn'])) $txn_id = $_GET['txn'];
	$seller = false; if(isset($_GET['uid'])) $seller = $_GET['uid'];
	$net = ( $total - $fee );
	if( $seller ) {
	$tax = get_user_meta( $seller, 'cp_auction_tax_vat', true );
	$bt_info = get_user_meta ( $seller, 'cp_auction_bank_transfer_info', true );
	}

	$cc = get_user_meta( $seller, 'currency', true );
	if( empty($cc) ) $cc = $cp_options->currency_code;

	$header_title = ''.__('Banktransfer', 'auctionPlugin').'';
	$breadcrumb = '<a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'">'.__('Dashboard', 'auctionPlugin').'</a> » '.__('Banktransfer', 'auctionPlugin').'';

?>

<?php cp_auction_header($header_title, $breadcrumb); ?>

	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

	<h2 class="dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h2>

	<p><?php _e('Please include the following details when sending the bank transfer. Once your transfer has been verified, the seller will approve your purchase.', 'auctionPlugin'); ?></p>

	<div class="clear15"></div>

	<div class="price_info">
		<div class="price_info1"><?php _e('Amount', 'auctionPlugin'); ?>:</div>
		<div class="price_info2"><?php echo cp_display_price( $net, 'ad', false ); ?></div>
	</div>

	<?php if( $fee > 0 ) { ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Fee for Bank Transfer:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( $fee, 'ad', false ); ?></div>
	</div>

	<?php } ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Total to Pay with Bank Transfer:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><strong><?php echo cp_display_price( $total, 'ad', false ); ?></strong></div>
	</div>

	<?php if( $tax > 0 ) { ?>
		<div class="price_info">
		<div class="price_info1"><?php echo sprintf(__('Hereof %s%s Tax / VAT:', 'auctionPlugin'), $tax, '%'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( cp_auction_taxes( $total, $tax ), 'ad', false ); ?></div>
		</div>	
		<?php } ?>

	<div class="clear15"></div>

	<div class="price_info">
		<div class="price_info1"><?php _e('Transaction ID:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo $txn_id; ?></div>
	</div>

	<div class="clear30"></div>

<?php


	if( $bt_info ) {
	echo '<div class="cp_terms">'.nl2br($bt_info).'</div><br /><br /><p style="text-align: center"><a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'" class="btn_orange">'.__('Proceed >>','auctionPlugin').'</a></p>';
	}

?>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>