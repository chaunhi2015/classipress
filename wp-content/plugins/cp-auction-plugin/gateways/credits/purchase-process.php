<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	if( !is_user_logged_in() ) {
	wp_redirect( home_url('/login/') );
	exit;
	}

if(isset($_POST['credit_payment'])) {

	$arg = ""; if(isset($_POST['args'])) $arg = unserialize( base64_decode( $_POST['args'] ) );

	global $wpdb;

	$pid = $arg['post_ids'];
	$order_id = $arg['order_id'];
	$uid = $arg['uid'];
	$seller = $arg['post_author'];
	$mc_fee = $arg['mc_fee'];
	$mc_gross = $arg['mc_gross'];
	if( empty($mc_fee)) $mc_fee = cp_auction_format_amount(0, 'process');
	$author_approves = get_user_meta( $seller, 'cp_author_approves', true );

	$total_credits = $mc_gross / get_option('cp_auction_plugin_credit_value');
	if( $arg['users_credits'] < $total_credits ) {
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=4&success=no');
	exit;
	}
	$available = $arg['users_credits'] - $total_credits;
	update_user_meta( $uid, 'cp_credits', $available );

	$author_available = get_user_meta( $seller, 'cp_credits', true );
	$upd_credits = $author_available + $total_credits;
	update_user_meta( $seller, 'cp_credits', $upd_credits );

	if( get_option('cp_auction_credit_approves') == "yes" || $author_approves == "yes" ) $approves = "pending";
	else $approves = "complete";

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$query = "INSERT INTO {$table_name} (order_id, post_ids, datemade, uid, post_author, ip_address, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, address_zip, address_street, moved_credits, available, quantity, payment_option, shipping, mc_fee, mc_gross, aws, type, status) VALUES (%d, %s, %d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %f, %f, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, $pid, $arg['datemade'], $uid, $seller, $arg['ip_address'], $arg['payment_date'], $arg['txn_id'], $arg['item_name'], $arg['trans_type'], $arg['mc_currency'], $arg['last_name'], $arg['first_name'], $arg['payer_email'], $arg['address_zip'], $arg['address_street'], $total_credits, $available, $arg['quantity'], $arg['howto_pay'], $arg['cp_shipping'], $mc_fee, $mc_gross, 'aws-credits', 'purchase', $approves));
	$lastid = $wpdb->insert_id;

	if ( $pid ) {
	$post_ids = explode(",",$pid);
	foreach($post_ids as $post_id) {
	$tm = time();
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	if( get_option('cp_auction_credit_approves') == "yes" || $author_approves == "yes" ) {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $seller, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $lastid, 'unpaid' => 0, 'paid' => 2, 'paiddate' => $tm, 'txn_id' => $arg['txn_id'], 'status' => 'paid'), $where_array);
	} else {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $seller, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $lastid, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $arg['txn_id'], 'status' => 'paid'), $where_array);
	}
	}
	}
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_credit_paid_item_email($order_id, $seller, $uid, $arg['quantity'], $mc_fee, $mc_gross, $arg['txn_id'], $approves);

	wp_redirect(get_bloginfo('wpurl').'/?confirmation=6&cre='.$total_credits.'&fee='.$mc_fee.'&txn='.$arg['txn_id'].'&success=ok');
	exit;
}