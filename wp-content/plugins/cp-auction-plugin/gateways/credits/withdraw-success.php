<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $cpurl;
    	get_currentuserinfo();
	$uid = $current_user->ID;

	$withdraw = false; $sum = false;
	if(isset($_GET['cre'])) {
	$withdraw = $_GET['cre'];
	$sum = $_GET['cre'];
	}
	$success = ""; if(isset($_GET['success'])) $success = $_GET['success'];
	$payout_option = ""; if(isset($_GET['pay'])) $payout_option = $_GET['pay'];

	$header_title = ''.__('Confirmation', 'auctionPlugin').'';
	$breadcrumb = '<a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'">'.__('Dashboard', 'auctionPlugin').'</a> » '.__('Confirmation', 'auctionPlugin').'';

?>

<?php cp_auction_header($header_title, $breadcrumb); ?>

	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

	<?php if( $success == "ok" ) { ?>
	<h2 class="dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h2>
	<?php } else { ?>
	<h2 class="dotted"><?php _e('Error!', 'auctionPlugin'); ?></h2>
	<?php } ?>

	<?php if(( $success != "ok" ) || ( empty($success) )) { ?>

	<div class="error"><?php _e( 'Sorry, you had not enough credits available!', 'auctionPlugin' ); ?></div>

	<div class="clear10"></div>
	
	<p><?php _e('Please update your request with the correct number of credits.', 'auctionPlugin'); ?></p>

	<div class="clear30"></div>

	<p style="text-align: center;"><a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1'); ?>" class="btn_orange"><?php _e( 'My Account >>','auctionPlugin'); ?></a></p>

	<?php } elseif( $success == "ok" ) { ?>

	<p><?php _e('Once the request is confirmed, one of our admins will perform a payout.', 'auctionPlugin'); ?></p>

	<div class="clear15"></div>

	<?php if( get_option('cp_auction_withdraw_fee_paypal') > 0 && $payout_option == 'paypal' ) {
	$total = cp_auction_calculate_fees($withdraw, 'paypal', get_option('cp_auction_withdraw_fee_paypal'));
	if( $withdraw > $total ) $pp_fee = $withdraw - $total;
	else $pp_fee = $total - $withdraw;
	$sum = $withdraw - $pp_fee; ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Fee for PayPal:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( $pp_fee, 'ad', false ); ?></div>
	</div>
	<?php } if( get_option('cp_auction_withdraw_fee_bt') > 0 && $payout_option == 'bank_transfer' ) {
	$total = cp_auction_calculate_fees($withdraw, 'bank_transfer', get_option('cp_auction_withdraw_fee_bt'));
	if( $withdraw > $total ) $bt_fee = $withdraw - $total;
	else $bt_fee = $total - $withdraw;
	$sum = $withdraw - $bt_fee; ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Fee for Bank Transfer:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( $amount, 'ad', false ); ?></div>
	</div>
	<?php } ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Total to Payout:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( $sum, 'ad', false ); ?></div>
	</div>

	<div class="clear30"></div>

	<p style="text-align: center"><a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1'); ?>" class="btn_orange"><?php _e('My Account >>','auctionPlugin'); ?></a></p>

	<?php } ?>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>