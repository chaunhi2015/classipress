<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $cp_options, $cpurl;
    	get_currentuserinfo();
	$uid = $current_user->ID;

	$total = ""; if(isset($_GET['cre'])) $total = $_GET['cre'];
	$success = ""; if(isset($_GET['success'])) $success = $_GET['success'];
	$fee = ""; if(isset($_GET['fee'])) $fee = $_GET['fee'];
	$txn_id = ""; if(isset($_GET['txn'])) $txn_id = $_GET['txn'];
	$paid_author = "";

	if( get_option('cp_auction_credits_fee_type') == "static_off" ) $paid_author = ''.__('paid by seller.', 'auctionPlugin').'';
	elseif( get_option('cp_auction_credits_fee_type') == "percentage_off" ) $paid_author = ''.__('paid by seller.', 'auctionPlugin').'';
	elseif( get_option('cp_auction_credits_fee_type') == "static_on" ) $paid_author = ''.__('paid by buyer.', 'auctionPlugin').'';
	elseif( get_option('cp_auction_credits_fee_type') == "percentage_on" ) $paid_author = ''.__('paid by buyer.', 'auctionPlugin').'';

	$header_title = ''.__('Confirmation', 'auctionPlugin').'';
	$breadcrumb = '<a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'">'.__('Dashboard', 'auctionPlugin').'</a> » '.__('Confirmation', 'auctionPlugin').'';

?>

<?php cp_auction_header($header_title, $breadcrumb); ?>

	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

	<?php if( $success == "ok" ) { ?>
	<h2 class="dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h2>
	<?php } else { ?>
	<h2 class="dotted"><?php _e('Error!', 'auctionPlugin'); ?></h2>
	<?php } ?>

	<?php if(( $success != "ok" ) || ( empty($success) )) { ?>

	<div class="error"><?php _e( 'Sorry, you had not enough credits available!', 'auctionPlugin' ); ?></div>

	<div class="clear10"></div>
	
	<p><?php _e('Please update your account with the necessary number of credits.', 'auctionPlugin'); ?></p>

	<div class="clear30"></div>

	<p style="text-align: center;"><a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1'); ?>" class="btn_orange"><?php _e( 'My Account >>','auctionPlugin'); ?></a></p>

	<?php } elseif( $success == "ok" ) { ?>

	<p><?php _e('Your transaction has been completed, and a receipt for your payment has been emailed to you and the seller.', 'auctionPlugin'); ?></p>

	<div class="clear15"></div>

	<?php if ( $fee > 0 ) { ?>
	<div class="price_info">
		<div class="price_info1"><?php _e('Transaction Fees:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( $fee, 'ad', false ); ?> <?php echo $paid_author; ?></div>
	</div>
	<?php } ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Paid Total:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( $total, 'ad', false ); ?></div>
	</div>

	<?php if( $cp_options->tax_charge > 0 ) { ?>
	<div class="price_info">
		<div class="price_info1"><?php echo sprintf(__('Hereof %s%s Tax / VAT:', 'auctionPlugin'), $cp_options->tax_charge, '%'); ?> </div>
		<div class="price_info2"><?php echo cp_display_price( cp_auction_taxes( $total, $cp_options->tax_charge ), 'ad', false ); ?></div>
	</div>	
	<?php } ?>

	<div class="clear15"></div>

	<div class="price_info">
		<div class="price_info1"><?php _e('Transaction ID:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo $txn_id; ?></div>
	</div>

	<div class="clear30"></div>

	<p style="text-align: center"><a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1'); ?>" class="btn_orange"><?php _e('My Account >>','auctionPlugin'); ?></a></p>

	<?php } ?>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>