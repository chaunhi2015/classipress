<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	global $wpdb;

if(isset($_POST['submit_withdraw'])) {
	$tm = time();
	$uid = isset( $_POST['uid'] ) ? esc_attr( $_POST['uid'] ) : '';
	$IP = isset( $_POST['ip_address'] ) ? esc_attr( $_POST['ip_address'] ) : '';
	$post_withdraw = isset( $_POST['withdraw'] ) ? esc_attr( $_POST['withdraw'] ) : '';
	$payout_option = isset( $_POST['payout_option'] ) ? esc_attr( $_POST['payout_option'] ) : '';
	$payout_details = isset( $_POST['payout_details'] ) ? esc_attr( $_POST['payout_details'] ) : '';
	$details = html_entity_decode($payout_details);

	if( $payout_option == "paypal" ) $fees = get_option('cp_auction_withdraw_fee_paypal');
	if( $payout_option == "bank_transfer" ) $fees = get_option('cp_auction_withdraw_fee_bt');

	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$withdraw = cp_auction_sanitize_amount($post_withdraw);
	$value = cp_auction_sanitize_amount($post_withdraw * get_option('cp_auction_plugin_credit_value'));
	$available = $available_credits - $withdraw;

	$mc_fee = false;
	if( get_option('cp_auction_withdraw_fee_paypal') > 0 && $payout_option == 'paypal' ) {
	$total = cp_auction_calculate_fees($withdraw, 'paypal', get_option('cp_auction_withdraw_fee_paypal'));
	if( $withdraw > $total ) $mc_fee = $withdraw - $total;
	else $mc_fee = $total - $withdraw;
	} elseif( get_option('cp_auction_withdraw_fee_bt') > 0 && $payout_option == 'bank_transfer' ) {
	$total = cp_auction_calculate_fees($withdraw, 'bank_transfer', get_option('cp_auction_withdraw_fee_bt'));
	if( $withdraw > $total ) $mc_fee = $withdraw - $total;
	else $mc_fee = $total - $withdraw;
	}

	$mc_gross = $withdraw;

	if( $post_withdraw > $available_credits ) {
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=5&cre='.$withdraw.'&success=0');
	exit;
	}
	update_user_meta( $uid, 'cp_credits', $available );
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$query = "INSERT INTO {$table_name} (datemade, uid, ip_address, txn_id, trans_type, moved_credits, available, payment_option, payout_details, mc_fee, mc_gross, aws, type, status) VALUES (%d, %d, %s, %s, %s, %f, %f, %s, %s, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $tm, $uid, $IP, 'N/A', 'Withdraw', $withdraw, $available, $payout_option, $details, $mc_fee, $mc_gross, 'aws-credits', 'withdraw', 'pending'));

	cp_auction_withdraw_request_email( $uid, $details, $withdraw, $mc_fee, $mc_gross );

	wp_redirect(get_bloginfo('wpurl').'/?confirmation=5&cre='.$withdraw.'&pay='.$payout_option.'&success=ok');
	exit;
}