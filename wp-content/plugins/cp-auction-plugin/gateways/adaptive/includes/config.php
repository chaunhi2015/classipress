<?php
/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Timezone Setting
 */
date_default_timezone_set('America/Chicago');

/**
  * Enable Sessions
  */
if(!session_id()) session_start();

$cp_auction_plugin_pp_email = get_option('cp_auction_plugin_paypalemail');
$cp_auction_plugin_signature = get_option('cp_auction_plugin_signature');	
$cp_auction_plugin_apipass = get_option('cp_auction_plugin_apipass');
$cp_auction_plugin_apiuser = get_option('cp_auction_plugin_apiuser');
$cp_auction_plugin_appid = get_option('cp_auction_plugin_appid');

$cp_auction_plugin_sand_email = get_option('cp_auction_plugin_sand_email');
$cp_auction_plugin_sand_signature = get_option('cp_auction_plugin_sand_signature');	
$cp_auction_plugin_sand_apipass = get_option('cp_auction_plugin_sand_apipass');
$cp_auction_plugin_sand_apiuser = get_option('cp_auction_plugin_sand_apiuser');
$cp_auction_plugin_sand_appid = get_option('cp_auction_plugin_sand_appid');

$actFEE = get_option('cp_auction_plugin_actfee');
$debug = get_option('cp_auction_plugin_sand_debug');

if(get_option('cp_auction_plugin_sandbox') == "yes") $if_sandbox = TRUE;
else $if_sandbox = FALSE;

/** 
 * Sandbox Mode - TRUE/FALSE
 */
$sandbox = $if_sandbox;
$domain = $sandbox ? 'https://svcs.sandbox.paypal.com/' : 'https://svcs.paypal.com/';
$cancelURL = get_bloginfo('url').'/?confirmation=1&amp;success=cancel';

/**
 * Enable error reporting if running in sandbox mode.
 */
if($sandbox)
{
	error_reporting(E_ALL|E_STRICT);
	ini_set('display_errors', '1');	
}

/**
 * API Credentials
 */

$api_version = '109.0';
$application_id = $sandbox ? $cp_auction_plugin_sand_appid : $cp_auction_plugin_appid;	// Only required for Adaptive Payments.  You get your Live ID when your application is approved by PayPal.
$developer_account_email = $cp_auction_plugin_sand_email;		// This is what you use to sign in to http://developer.paypal.com.  Only required for Adaptive Payments.
$api_username = $sandbox ? $cp_auction_plugin_sand_apiuser : $cp_auction_plugin_apiuser;
$api_password = $sandbox ? $cp_auction_plugin_sand_apipass : $cp_auction_plugin_apipass;
$api_signature = $sandbox ? $cp_auction_plugin_sand_signature : $cp_auction_plugin_signature;

/**
 * Third Party User Values
 * These can be setup here or within each caller directly when setting up the PayPal object.
 */

$api_subject = '';	// If making calls on behalf a third party, their PayPal email address or account ID goes here.
$device_id = '';
$device_ip_address = $_SERVER['REMOTE_ADDR'];

?>