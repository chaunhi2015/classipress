<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

global $wpdb;

	$order_id = false; if(isset($_GET['oid'])) $order_id = $_GET['oid'];
	$trans_id = false; if(isset($_GET['trans'])) $trans_id = $_GET['trans'];
	$sid = false; if(isset($_GET['sid'])) $sid = $_GET['sid'];
	$uid = false; if(isset($_GET['uid'])) $uid = $_GET['uid'];
	if( $sid ) $author_approves = get_user_meta( $sid, 'cp_author_approves', true );


/*
|--------------------------------------------------------------------------
| SHOPPING CART ITEM PURCHASE BY PAYPAL ADAPTIVE
|--------------------------------------------------------------------------
*/

if( $order_id > 0 ) {

	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$trans_id'");

	if( get_option('cp_auction_paypal_approves') != "yes" && $author_approves != "yes" ) {
	$where_array = array('id' => $trans_id);
	$wpdb->update($table_transactions, array('status' => 'complete'), $where_array);
	}

	if ( $res ) {
	$post_ids = explode(",",$res->post_ids);
	foreach($post_ids as $post_id) {
	$tm = time();
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $trans_id, 'unpaid' => 0, 'paid' => 2, 'paiddate' => $tm, 'txn_id' => '', 'status' => 'paid'), $where_array);
	} else {
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $trans_id, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => '', 'status' => 'paid'), $where_array);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($order_id);
	}
	}
	}
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_paypal_adaptive_email($order_id, $sid, $uid);
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1');
	exit;
}
else {
	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1');
	exit;
}
?>