<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	// Include required library files.
	//require_once(ABSPATH . 'wp-includes/pluggable.php');
	require_once('includes/config.php');
	require_once('includes/PayPal.php');

	global $wpdb, $cp_options, $cpurl;

	$arg = false; if( isset( $_POST['args'] ) ) $arg = unserialize( base64_decode( $_POST['args'] ) );

	$uid = false; if( isset( $arg['uid'] ) ) $uid = $arg['uid'];
	$post_author = false; if( isset( $arg['post_author'] ) ) $post_author = $arg['post_author'];
	$order_id = false; if( isset( $arg['order_id'] ) ) $order_id = $arg['order_id'];
	$txn_id = false; if( isset( $arg['txn_id'] ) ) $txn_id = $arg['txn_id'];
	$quantity = false; if( isset( $arg['quantity'] ) ) $quantity = $arg['quantity'];
	$cp_shipping = false; if( isset( $arg['cp_shipping'] ) ) $cp_shipping = $arg['cp_shipping'];
	$moved_credits = false; if( isset( $arg['moved_credits'] ) ) $moved_credits = $arg['moved_credits'];
	$available = false; if( isset( $arg['available'] ) ) $available = $arg['available'];
	$cc = false; if( isset( $arg['mc_currency'] ) ) $cc = $arg['mc_currency'];
	if( empty($cc) ) $cc = $cp_options->currency_code;

	$enabl = get_option( 'cp_auction_enable_conversion' );
	if ( $enabl == "yes" ) $cc = get_option( 'cp_auction_convert_to' );

	$total = false; if( isset( $arg['mc_gross'] ) && $arg['mc_gross'] > '0' ) $total = $arg['mc_gross'];
	if( isset( $arg['conversion'] ) && $arg['conversion'] > '0' ) $total = $arg['conversion'];

	$seller_amount = $total - ($total * (((float)($actFEE))/100));
	$admin_amount = $total - $seller_amount;
	$memo = ''.__('Order ID:', 'auctionPlugin').' '.$order_id.'';

	$buyer_sand = get_option('cp_auction_plugin_buyer_sand_email');
	$admin_sand = get_option('cp_auction_plugin_admin_sand_email');
	$seller_sand = get_option('cp_auction_plugin_seller_sand_email');
	$buyer_email = $arg['payer_email'];
	$admin_email = get_option('cp_auction_plugin_paypalemail');
	$seller_email = $arg['receiver_email'];
	$echeck = $sandbox ? get_option('cp_auction_plugin_echeck_sand') : get_option('cp_auction_plugin_echeck');
	$balance = $sandbox ? get_option('cp_auction_plugin_balance_sand') : get_option('cp_auction_plugin_balance');
	$creditcard = $sandbox ? get_option('cp_auction_plugin_creditcard_sand') : get_option('cp_auction_plugin_creditcard');

	$buyer_pp = $sandbox ? $buyer_sand : $buyer_email;
	$admin_pp = $sandbox ? $admin_sand : $admin_email;
	$seller_pp = $sandbox ? $seller_sand : $seller_email;

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$reg = $wpdb->get_row("SELECT * FROM {$table_name} WHERE order_id = '$order_id' AND uid = '$uid' AND status = 'pending'");

	$release = get_option( 'cp_auction_paypal_release' );
	$author_approves = get_user_meta( $post_author, 'cp_author_approves', true );
	$approves = "complete"; if( get_option('cp_auction_paypal_approves') == "yes" || $author_approves == "yes" ) $approves = "pending";
	else if ( $approves == "complete" && $release == "yes" ) $approves = "complete";
	else $approves = "pending";

	if( $reg ) {
	$lastid = $reg->id;
	} else {

	$query = "INSERT INTO {$table_name} (order_id, pid, post_ids, datemade, uid, post_author, ip_address, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, receiver_email, address_country, address_state, address_country_code, address_zip, address_street, moved_credits, available, quantity, payment_option, shipping, mc_fee, mc_gross, aws, type, status) VALUES (%d, %d, %s, %d, %d, %d, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %f, %f, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, $arg['post_id'], $arg['post_ids'], $arg['datemade'], $arg['uid'], $post_author, $arg['ip_address'], $arg['payment_date'], $txn_id, $arg['item_name'], $arg['trans_type'], $arg['mc_currency'], $arg['last_name'], $arg['first_name'], $arg['payer_email'], $arg['receiver_email'], $arg['address_country'], $arg['address_state'], 'N/A', $arg['address_zip'], $arg['address_street'], $moved_credits, $available, $quantity, $arg['howto_pay'], $cp_shipping, $arg['mc_fee'], $arg['mc_gross'], 'N/A', $arg['type'], $approves));
	$lastid = $wpdb->insert_id;
}

	if ( $approves == "complete" ) {

	$tx_token = ''.__('N/A','auctionPlugin').'';
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$lastid'");
	$uid = $res->uid;
	$sid = $res->post_author;
	$type = $res->type;
	$pid = $res->post_ids;

	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	
	$tm = time();

	if ( $pid ) {
	$post_ids = explode(",",$pid);
	foreach($post_ids as $post_id) {

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('pid' => $post_id, 'buyer' => $uid, 'uid' => $sid, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $lastid, 'unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => $tx_token, 'status' => 'paid'), $where_array);

	}
	}
}

	$ipnCHAINED = get_bloginfo('wpurl').'/?ipn_chained=1&amp;oid='.$order_id.'&amp;trans='.$lastid.'&amp;sid='.$post_author.'&amp;uid='.$uid.'';
	$returnURL = get_bloginfo('wpurl').'/?confirmation=7&amp;oid='.$order_id.'&amp;trans='.$lastid.'&amp;sid='.$post_author.'&amp;uid='.$uid.'';

// Create PayPal object.
$PayPalConfig = array(
		'Sandbox' => $sandbox,
		'DeveloperAccountEmail' => $developer_account_email,
		'ApplicationID' => $application_id,
		'DeviceID' => $device_id,
		'IPAddress' => $_SERVER['REMOTE_ADDR'],
		'APIUsername' => $api_username,
		'APIPassword' => $api_password,
		'APISignature' => $api_signature,
		'APISubject' => $api_subject
		);

$PayPal = new angelleye\PayPal\Adaptive($PayPalConfig);

// Prepare request arrays
$PayRequestFields = array(
		'ActionType' => 'PAY', 		// Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
		'CancelURL' => $cancelURL, 	// Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
		'CurrencyCode' => $cc, 		// Required.  3 character currency code.
		'FeesPayer' => 'EACHRECEIVER', 	// The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
		'IPNNotificationURL' => $ipnCHAINED, 	// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
		'Memo' => $memo, 		// A note associated with the payment (text, not HTML).  1000 char max
		'Pin' => '', 			// The sender's personal id number, which was specified when the sender signed up for the preapproval
		'PreapprovalKey' => '', 	// The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.  
		'ReturnURL' => $returnURL, 	// Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
		'ReverseAllParallelPaymentsOnError' => 'TRUE', 	// Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
		'SenderEmail' => $buyer_pp, 	// Sender's email address.  127 char max.
		'TrackingID' => ''		// Unique ID that you specify to track the payment.  127 char max.
		);
						
$ClientDetailsFields = array(
		'CustomerID' => $buyer_pp, 	// Your ID for the sender  127 char max.
		'CustomerType' => '', 		// Your ID of the type of customer.  127 char max.
		'GeoLocation' => '', 		// Sender's geographic location
		'Model' => '', 			// A sub-identification of the application.  127 char max.
		'PartnerName' => ''		// Your organization's name or ID
		);
						
$FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');	// Funding constrainigs require advanced permissions levels. 'ECHECK', 'BALANCE', 'CREDITCARD'

$Receivers = array();
$Receiver = array(
		'Amount' => $seller_amount, 	// Required.  Amount to be paid to the receiver.
		'Email' => $seller_pp, 		// Receiver's email address. 127 char max.
		'InvoiceID' => $order_id, 		// The invoice number for the payment.  127 char max.
		'PaymentType' => 'GOODS', 	// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
		'PaymentSubType' => '', 	// The transaction subtype for the payment.
		'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
		'Primary' => 'TRUE'		// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
		);
array_push($Receivers,$Receiver);

$Receiver = array(
		'Amount' => $admin_amount, 	// Required.  Amount to be paid to the receiver.
		'Email' => $admin_pp, 		// Receiver's email address. 127 char max.
		'InvoiceID' => $order_id, 	// The invoice number for the payment.  127 char max.
		'PaymentType' => 'GOODS', 	// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
		'PaymentSubType' => '', 	// The transaction subtype for the payment.
		'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
		'Primary' => 'FALSE'		// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
		);
array_push($Receivers,$Receiver);

$SenderIdentifierFields = array(
		'UseCredentials' => ''	// If TRUE, use credentials to identify the sender.  Default is false.
		);
								
$AccountIdentifierFields = array(
		'Email' => $buyer_pp, // Sender's email address.  127 char max.
		'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')	// Sender's phone number.  Numbers only.
		);
								
$PayPalRequestData = array(
		'PayRequestFields' => $PayRequestFields, 
		'ClientDetailsFields' => $ClientDetailsFields, 
		'Receivers' => $Receivers, //'FundingTypes' => $FundingTypes, 
		'SenderIdentifierFields' => $SenderIdentifierFields, 
		'AccountIdentifierFields' => $AccountIdentifierFields
		);

// Pass data into class for processing with PayPal and load the response array into $PayPalResult
$PayPalResult = $PayPal->Pay($PayPalRequestData);

// Write the contents of the response array to the screen for demo purposes.
if( $debug == "yes" ) {
echo '<pre />';
print_r($PayPalResult);
}
wp_redirect(''.$PayPalResult['RedirectURL'].'');
exit();

?>