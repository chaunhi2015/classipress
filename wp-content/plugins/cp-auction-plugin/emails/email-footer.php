<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

?>

                              </div>
                            </td>
                          </tr>

			<?php if( get_option('cp_auction_plugin_banner_url') ) {
			$banner_link = get_option('cp_auction_plugin_banner_link');
			$banner_url = get_option('cp_auction_plugin_banner_url'); ?>
                          <tr>
                            <td style="border-top: solid 1px #d9d9d9;" colspan="2">
                              <div style="padding: 15px 0px; text-align: center;"> <a href="<?php echo $banner_link; ?>" target="_blank"><img border="0" src="<?php echo $banner_url; ?>" width="468" height="60" /></a></div>
                            </td>
                          </tr>
			<?php } ?>

                        </tbody>
                      </table>
                      <table id="footer" style="line-height: 1.5; font-size: 12px; font-family: Arial, sans-serif; margin-right: 30px; margin-left: 30px;" width="490" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr style="font-size: 11px; color: #999999;">
                           <td style="border-top: solid 1px #d9d9d9;" colspan="2"><a href="http://www.arctic-websolutions.com" target="_blank"><img style="padding-top: 28px;" alt="AWS" border="0" src="<?php echo cp_auction_plugin_url(); ?>/images/favicon.ico" width="16" height="16" align="right" /></a>
                              <div style="padding-top: 15px; padding-bottom: 1px;"><img style="vertical-align: middle;" alt="Date" src="<?php echo get_bloginfo('wpurl'); ?>/wp-admin/images/date-button.gif" width="13" height="13" /> <?php _e('Email sent', 'auctionPlugin'); ?> <?php echo date("F j, Y, g:i a"); ?></div>
                              <div><img style="vertical-align: middle;" alt="Contact" src="<?php echo get_bloginfo('wpurl'); ?>/wp-admin/images/comment-grey-bubble.png" width="12" height="12" /> <?php _e('For any requests, please contact', 'auctionPlugin'); ?> <a style="text-decoration: none;" href="mailto:<?php echo get_bloginfo('admin_email'); ?>"><?php echo get_bloginfo('admin_email'); ?></a></div>
                            </td>
                          </tr>
                          <tr>
                            <td style="color: #ffffff;" colspan="2" height="15">.</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
            </center></td>
        </tr>
      </tbody>
    </table>
  </body>
</html>