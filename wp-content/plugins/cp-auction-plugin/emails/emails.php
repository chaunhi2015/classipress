<?php

/*
The following emails can be edited here, or by using the codestyling localization plugin.
 - PAYPAL ADAPTIVE PAYMENT
 - ITEMS ORDERED BY CIH OR COD
 - ITEMS PAID WITH PAYPAL DIRECT
 - ITEMS PAID WITH PAYPAL SHOPPING CART
 - ITEMS PAID WITH PAYPAL IN SIMPLE MODE
 - ITEMS PAID WITH BANK TRANSFER ESCROW
 - ITEMS PAID WITH CREDITS ESCROW
 - ITEMS PAID WITH PAYPAL ESCROW
 - PAYMENT CONFIRMATION TO SELLER / AUTHOR
 - CHOOSE A AUCTION WINNER NOTIFICATIONS
 - USER PROFILE CONTACT USER EMAIL
 - USER PROFILE REPORT FRAUD
 - USER PROFILE REPORT AD
 - WATCHLIST EMAIL NOTIFICATIONS
 - WATCHLIST BUYNOW EMAIL NOTIFICATIONS
 - NEW BIDS POSTED NOTIFICATIONS
 - BUY NOW EMAIL NOTIFICATIONS
 - BUY NOW CANCELLATIONS EMAIL NOTIFICATIONS
 - AUCTION HAS ENDED
 - PURCHASES ADDED VIA ADMIN BACKEND
 - PENDING PURCHASE WAS DELETED
 - WITHDRAW REQUEST
 - ITEMS PAID WITH CREDITS
 - PAYMENT CONFIRMATION TO THE SELLER AFTER COMPLETION OF THE ESCROW
 - FAILED PAYMENT
 - CONFIRM PAYMENT
 - FILE UPLOADED AD APPROVAL NOTICE
 - NEW WANTED OFFER POSTED NOTIFICATIONS
 - WANTED WATCHLIST EMAIL NOTIFICATIONS
 - WANTED OFFER WAS DECLINED
 - NEW FILE WAS UPLOADED BUYERS
 - NEW FILE WAS UPLOADED ADMIN
*/

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

global $post, $cpurl;

/*
|--------------------------------------------------------------------------
| PAYPAL ADAPTIVE PAYMENT
|--------------------------------------------------------------------------
*/

function cp_auction_paypal_adaptive_email($order_id, $sid, $uid) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($sid);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('Your payment was recorded and a confirmation is automatically sent to the seller.','auctionPlugin'); ?><br /><br />

	<?php _e('Sellers Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Sellers Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$subject = "".__('Purchased items was paid','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent you a payment for your items sold at our website.','auctionPlugin'); ?><br /><br />

	<?php _e('Buyers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Buyers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| ITEMS ORDERED BY CIH OR COD
|--------------------------------------------------------------------------
*/

function cp_auction_cih_cod_email($order_id, $seller, $uid, $quantity, $mc_fee, $mc_gross, $type) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');
	$option = false;
	if( $type == "cih" ) $option = "".__('Cash in Hand','auctionPlugin')."";
	else if( $type == "cod" ) $option = "".__('Cash on Delivery','auctionPlugin')."";
	else if( $type == "bank_transfer" ) $option = "".__('Bank Transfer','auctionPlugin')."";

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($seller);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your purchase','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>


	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You have set up an order according to the following:','auctionPlugin'); ?><br />
	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php if( $type == "cod" || $type == "bank_transfer" ) { ?>
	<?php _e('Amounts Payable:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Of which Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php } ?>
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Payment Option:','auctionPlugin'); ?> <?php echo $option; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Sellers Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Sellers Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br />
	<?php if( $type == "cih" ) { ?>
	<?php _e('Please contact the seller and agree on where and when the goods can be picked up.','auctionPlugin'); ?><br /><br /><br />
	<?php } else if( $type == "cod" ) { ?>
	<?php _e('The goods will be sent by Cash on Delivery, and you will receive an e-mail confirming the shipment.','auctionPlugin'); ?><br /><br /><br />
	<?php } ?>


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$subject = "".__('New Order by','auctionPlugin')." ".$option."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just set up an order according to the following:','auctionPlugin'); ?><br />
	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php if( $type == "cod" || $type == "bank_transfer" ) { ?>
	<?php _e('Amounts Payable:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Of which Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php } ?>
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Payment Option:','auctionPlugin'); ?> <?php echo $option; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Buyers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Buyers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br />
	<?php if( $type == "cih" ) { ?>
	<?php _e('Please contact the buyer and agree on where and when the goods can be picked up.','auctionPlugin'); ?><br /><br />
	<?php } else if( $type == "cod" ) { ?>
	<?php _e('Please send the goods by Cash on Delivery as soon as possible, and confirm the shipment by sending the buyer an e-mail.','auctionPlugin'); ?><br /><br />
	<?php } ?>
	<?php _e('To see all the transaction details, log in to your account here with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| ITEMS PAID WITH PAYPAL DIRECT
|--------------------------------------------------------------------------
*/

function cp_auction_paypal_direct_items_email( $pid, $post, $uid, $mc_fee = '', $mc_gross = '', $txn_id = '' ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($post->post_author);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('You`ve sent a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?> <?php _e('for items sold at our website.','auctionPlugin'); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Item Name:','auctionPlugin'); ?> <?php echo $post->post_title; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Sellers Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Sellers Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$subject = "".__('Purchased items was paid','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent you a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?> <?php _e('for your items sold at our website.','auctionPlugin'); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Item Name:','auctionPlugin'); ?> <?php echo $post->post_title; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Buyers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Buyers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| ITEMS PAID WITH PAYPAL SHOPPING CART
|--------------------------------------------------------------------------
*/

function cp_auction_paypal_items_email( $order_id, $seller, $uid, $numbers, $mc_fee = '', $mc_gross = '', $txn_id = '' ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($seller);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You`ve sent a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?> <?php echo sprintf(__('for %s items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Sellers Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Sellers Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$subject = "".__('Purchased items was paid','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent you a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?> <?php _e('for your items sold at our website.','auctionPlugin'); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Buyers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Buyers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| ITEMS PAID WITH BANK TRANSFER ESCROW
|--------------------------------------------------------------------------
*/

function cp_auction_bt_escrow_email( $order_id, $seller, $uid, $numbers, $mc_fee = '', $mc_gross = '', $txn_id = '' ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($seller);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your transfer request','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You`ve sent a bank transfer request of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?> <?php echo sprintf(__('for %s items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Sellers Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Sellers Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$subject = "".__('Purchased items','auctionPlugin')."";
								
	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent us a bank transfer payment request of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php echo sprintf(__('for %s of your items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	<?php _e('You will receive a second email as soon as one of our admins have confirmed the payment.','auctionPlugin'); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Buyers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Buyers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );


	// ADMIN NOTIFICATION
	$users_choice = get_user_meta( $seller, 'cp_use_safe_payment', true );
	if(( get_option('cp_auction_plugin_pay_admin') == "yes" ) && ( get_option('cp_auction_plugin_pay_user') == "yes" )) {
	$pay = "both";
	} elseif(( get_option('cp_auction_plugin_pay_admin') == "yes" ) && ( get_option('cp_auction_plugin_pay_user') == "no" )) {
	$pay = "admin";
	} else {
	$pay = "user";
	}

	if(( $pay == "both" ) && ( $users_choice == "yes" ) || ( $pay == "admin" )) {

	$subject = "".__('Banktransfer escrow request','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Hello!','auctionPlugin'); ?><br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent you a bank transfer request of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php echo sprintf(__('for %s items sold at your website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Authors Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Authors Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );

	}
}


/*
|--------------------------------------------------------------------------
| ITEMS PAID WITH CREDITS ESCROW
|--------------------------------------------------------------------------
*/

function cp_auction_cre_escrow_email( $order_id, $seller, $uid, $numbers, $mc_fee = '', $mc_gross = '', $txn_id = '' ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($seller);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You`ve sent a credit payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?> <?php echo sprintf(__('for %s items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Sellers Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Sellers Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$subject = "".__('Purchased items','auctionPlugin')."";
								
	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent us a credit payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php echo sprintf(__('for %s of your items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	<?php _e('You will receive a second email as soon as one of our admins have confirmed the payment.','auctionPlugin'); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Buyers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Buyers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );


	// ADMIN NOTIFICATION
	$users_choice = get_user_meta( $seller, 'cp_use_safe_payment', true );
	if(( get_option('cp_auction_plugin_pay_admin') == "yes" ) && ( get_option('cp_auction_plugin_pay_user') == "yes" )) {
	$pay = "both";
	} elseif(( get_option('cp_auction_plugin_pay_admin') == "yes" ) && ( get_option('cp_auction_plugin_pay_user') == "no" )) {
	$pay = "admin";
	} else {
	$pay = "user";
	}

	if(( $pay == "both" ) && ( $users_choice == "yes" ) || ( $pay == "admin" )) {

	$subject = "".__('Credit escrow payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Hello!','auctionPlugin'); ?><br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent you a credit payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php echo sprintf(__('for %s items sold at your website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Authors Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Authors Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );

	}
}


/*
|--------------------------------------------------------------------------
| ITEMS PAID WITH PAYPAL ESCROW
|--------------------------------------------------------------------------
*/

function cp_auction_pp_escrow_email( $order_id, $seller, $uid, $numbers, $mc_fee = '', $mc_gross = '', $txn_id = '' ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($seller);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You`ve sent a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?> <?php echo sprintf(__('for %s items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Sellers Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Sellers Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$subject = "".__('Purchased items','auctionPlugin')."";
								
	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent us a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php echo sprintf(__('for %s of your items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	<?php _e('You will receive a second email as soon as one of our admins have confirmed the payment.','auctionPlugin'); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Buyers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Buyers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );


	// ADMIN NOTIFICATION
	$users_choice = get_user_meta( $seller, 'cp_use_safe_payment', true );
	if(( get_option('cp_auction_plugin_pay_admin') == "yes" ) && ( get_option('cp_auction_plugin_pay_user') == "yes" )) {
	$pay = "both";
	} elseif(( get_option('cp_auction_plugin_pay_admin') == "yes" ) && ( get_option('cp_auction_plugin_pay_user') == "no" )) {
	$pay = "admin";
	} else {
	$pay = "user";
	}

	if(( $pay == "both" ) && ( $users_choice == "yes" ) || ( $pay == "admin" )) {

	$subject = "".__('PayPal escrow payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Hello!','auctionPlugin'); ?><br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent you a PayPal payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php echo sprintf(__('for %s items sold at your website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Authors Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Authors Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );

	}
}


/*
|--------------------------------------------------------------------------
| PAYMENT CONFIRMATION TO SELLER / AUTHOR
|--------------------------------------------------------------------------
*/

function cp_auction_author_payment_confirmation( $order_id, $seller, $uid, $net, $txn_id, $payment_date ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$user = get_userdata($uid);
	$user1 = get_userdata($seller);

	// AUTHOR NOTIFICATION
	$subject = "".__('Purchased items was paid','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent us a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $net, 'ad', false ); ?><br />
	<?php _e('for your items sold at our website.','auctionPlugin'); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $net, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Payers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Payers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| CHOOSE A AUCTION WINNER NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_choose_winner_email( $pid, $post, $wid, $amount ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$user = get_userdata($wid);
	$user1 = get_userdata($post->post_author);
	$sphone = get_post_meta( $post->ID, 'cp_contact_phone', true );
	if ( empty( $sphone ) ) $sphone = get_user_meta( $post->post_author, 'cp_phone', true );
	if ( empty( $sphone ) ) $sphone = get_user_meta( $post->post_author, 'cp_telephone', true );

	// WINNER NOTIFICATION
	$subject = "".__('You have won the auction','auctionPlugin')." ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You have won the auction','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('for the total amount of','auctionPlugin'); ?> <?php echo cp_display_price( $amount, 'ad', false ); ?><br /><br />

	<?php _e('You may contact the seller via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$post->post_author.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br />

	<?php _e( 'Sellers Name:','auctionPlugin' ); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e( 'Sellers Email:','auctionPlugin' ); ?> <a href="mailto:<?php echo $user1->user_email; ?>"><?php echo $user1->user_email; ?></a><br />
	<?php if ( $sphone ) { ?><?php _e( 'Sellers Phone:', 'auctionPlugin' ); ?> <?php echo $sphone; ?><br /><br /><br /><?php } else { ?><br /><br /><br /><?php } ?>


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$user1 = get_userdata($post->post_author);
	$user = get_userdata($wid);
	$bphone = get_user_meta( $wid, 'cp_phone', true );
	if ( empty( $bphone ) ) $bphone = get_user_meta( $wid, 'cp_telephone', true );

	$subject = "".__('Your auction','auctionPlugin')." ".$post->post_title." ".__('has ended.','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('You have chosen the price','auctionPlugin'); ?> <?php echo cp_display_price( $amount, 'ad', false ); ?> <?php _e('as winner bid for your auction','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<?php _e('You may contact the bidder via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$wid.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />

	<?php _e( 'Bidders Name:','auctionPlugin' ); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e( 'Bidders Email:','auctionPlugin' ); ?> <a href="mailto:<?php echo $user->user_email; ?>"><?php echo $user->user_email; ?></a><br />
	<?php if ( $bphone ) { ?><?php _e( 'Bidders Phone:', 'auctionPlugin' ); ?> <?php echo $bphone; ?><br /><br /><br /><?php } else { ?><br /><br /><br /><?php } ?>


	<?php _e('Thank you for posting your auctions with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| USER PROFILE CONTACT USER EMAIL
|--------------------------------------------------------------------------
*/

function cp_auction_contact_user_email( $your_message, $your_subject, $your_name, $your_email, $userid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// USER CONTACT NOTIFICATION
	$seller = get_userdata($userid);
	$subject = "".$your_subject."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php echo $your_message; ?><br /><br />

	--------------------------------------------------------------------------- <br />

	<?php _e('This message from','auctionPlugin'); ?> <?php echo $your_name; ?> <?php _e('applies to you as an author or buyer at','auctionPlugin'); ?> <a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo $site_name; ?></a>. <?php _e('Please, click on the reply button to reply to this message.','auctionPlugin'); ?><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$your_name.' <'.$your_email.'>';

	wp_mail( $seller->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| USER PROFILE REPORT FRAUD
|--------------------------------------------------------------------------
*/

function cp_auction_report_fraud_email( $post, $display_user_name, $your_message, $your_name, $your_email, $userid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// ADMIN NOTIFICATION
	$subject = "".__('Re: Fraud attempt by user,', 'auctionPlugin')." ".$display_user_name."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php echo $your_message; ?><br /><br />

	--------------------------------------------------------------------------- <br />

	<?php _e('This message from','auctionPlugin'); ?> <?php echo $your_name; ?> <?php _e('applies to user:','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_author_posts_url($userid); ?>" target="_blank"><?php echo $display_user_name; ?></a><br />
	<?php _e('Please, click on the reply button to reply to this message.','auctionPlugin'); ?><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$your_name.' <'.$your_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );


}


/*
|--------------------------------------------------------------------------
| USER PROFILE REPORT AD
|--------------------------------------------------------------------------
*/

function cp_auction_report_ad_email( $post, $display_user_name, $your_message, $your_name, $your_email, $userid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// ADMIN NOTIFICATION
	$subject = "".__('Re: Inappropriate content,', 'auctionPlugin')." ".$post->post_title."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php echo $your_message; ?><br /><br />

	<?php _e('The reported ad:','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a><br /><br />

	--------------------------------------------------------------------------- <br />

	<?php _e('This message from','auctionPlugin'); ?> <?php echo $your_name; ?> <?php _e('applies to user:','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_author_posts_url($userid); ?>" target="_blank"><?php echo $display_user_name; ?></a><br />
	<?php _e('Please, click on the reply button to reply to this message.','auctionPlugin'); ?><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$your_name.' <'.$your_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| WATCHLIST EMAIL NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_watchlist_email_notifications( $pid, $post, $bid, $uid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($uid);
	$subject = "".__('Watchlist Notification','auctionPlugin')." - ".$site_name."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('A new bid of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been posted for the auction','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<?php _e('You are receiving this email because you have added this auction to your watch list or placed a bid on this auction, you can unsubscribe via your account at anytime','auctionPlugin'); ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| WATCHLIST BUYNOW EMAIL NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_watchlist_buynow_notification( $pid, $post, $bid, $uid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($uid);
	$subject = "".__('Watchlist Notification','auctionPlugin')." - ".$site_name."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('The buy now price','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been met for the auction','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<?php _e('You are receiving this email because you have added this auction to your watch list or placed a bid on this auction, you can unsubscribe via your account at anytime','auctionPlugin'); ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| NEW BIDS POSTED NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_new_bids_posted( $pid, $post, $uid, $bid, $min_price ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($uid);
	$subject = "".__('New bid posted to auction','auctionPlugin')." ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You have posted a bid of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('for the auction','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br /><br />


	<?php _e('Thank you for bidding with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$user1 = get_userdata($post->post_author);
	$subject = "".__('New bid posted to auction','auctionPlugin')." ".$post->post_title;
								
if(($min_price > 0) && ($bid < $min_price)) {

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('A new bid of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been posted for your auction','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br /><br />


	<?php _e('Thank you for posting your auctions with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

} elseif(($min_price > 0) && ($bid > $min_price)) {

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('A new bid of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been posted for your auction','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br />
	<?php _e('The bid exceeded your specified minimum price.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for posting your auctions with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

} else {

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('A new bid of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been posted for your auction','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br /><br />


	<?php _e('Thank you for posting your auctions with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

	}
}


/*
|--------------------------------------------------------------------------
| BUY NOW EMAIL NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_buy_now_emails( $pid, $post, $uid, $bid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BUYER NOTIFICATION
	$user = get_userdata($uid);
	$subject = "".__('Your order of','auctionPlugin')." ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You have successfully added the item','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('for the total amount of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('to your shopping cart.','auctionPlugin'); ?><br /><br />

	<?php _e('Please do not let the seller wait for your payment,','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['cart'], '?shopping_cart=1'); ?>"><?php _e('click here','auctionPlugin'); ?></a> <?php _e('to pay now.','auctionPlugin'); ?><br/><br/>

	<?php _e('You may contact the seller via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$post->post_author.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$user1 = get_userdata($post->post_author);
	$subject = "".$post->post_title." ".__('added to cart','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> ( <?php echo $user->user_login; ?> ) <?php _e('has just added the item','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('for the total amount of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('to their shopping cart.','auctionPlugin'); ?><br /><br />

	<?php _e('You may contact the buyer via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| BUY NOW CANCELLATIONS EMAIL NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_delete_complete_purchase_emails( $pid, $post, $seller, $buyer ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// AUTHOR NOTIFICATION
	$user = get_userdata($buyer);
	$user1 = get_userdata($seller);
	$subject = "".$post->post_title." ".__('removed from cart','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> ( <?php echo $user->user_login; ?> ) <?php _e('has just removed the item','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('from their shopping cart.','auctionPlugin'); ?><br /><br />

	<?php _e('You may contact the buyer via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$buyer.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| AUCTION HAS ENDED
|--------------------------------------------------------------------------
*/

function cp_auction_listing_has_ended_emails( $pid, $post, $user, $cbid = '' ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');
	$cbid = cp_auction_plugin_get_latest_bid($post->ID, 'bid');

	$user = get_userdata($user);

	// AUTHOR NOTIFICATION
	$subject = "".__('Your listing','auctionPlugin')." ".$post->post_title." ".__('has ended.','auctionPlugin')."";

if(empty($cbid)) {

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('Your listing','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has ended.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );

} elseif($cbid > 0) {

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('Your listing','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has ended, the last posted bid was of','auctionPlugin'); ?> <?php echo cp_display_price( $cbid, 'ad', false ); ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );

	}
}


/*
|--------------------------------------------------------------------------
| PURCHASES ADDED VIA ADMIN BACKEND
|--------------------------------------------------------------------------
*/

function cp_auction_backend_payment_receipt( $pid, $item_name, $uid, $mc_fee = '', $mc_gross = '', $txn_id = '' ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You`ve sent a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Item Name:','auctionPlugin'); ?> <?php echo $item_name; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for doing business with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| PENDING PURCHASE WAS DELETED
|--------------------------------------------------------------------------
*/

function cp_auction_pending_purchase_deleted_emails( $pid, $post, $uid, $cbid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$user = get_userdata($uid);

	// BUYER NOTIFICATION
	$subject = "".__('Your purchase','auctionPlugin')." ".$post->post_title." ".__('was canceled.','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('Your pending purchase at','auctionPlugin'); ?> <?php echo cp_display_price( $cbid, 'ad', false ); ?> <?php _e('for the item','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('was canceled','auctionPlugin'); ?> <br /><br /><br />


	<?php _e('You are welcome back for more purchases with us anytime!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| WITHDRAW REQUEST
|--------------------------------------------------------------------------
*/

function cp_auction_withdraw_request_email( $uid, $details, $withdraw, $mc_fee, $mc_gross ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');
	if( $mc_fee < 0 ) $mc_fee = 0;
	$amount = $mc_gross - $mc_fee;

	$user = get_userdata($uid);

	// ADMIN NOTIFICATION
	$subject = "".__('New withdraw request','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Hello,<br /><br />

	A new withdraw request was posted on your website, see the details below:','auctionPlugin'); ?><br /><br />

	<?php _e('Name:', 'auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br />
	<?php _e('Email:', 'auctionPlugin'); ?> <?php echo $user->user_email; ?><br />
	<?php _e('Requested:', 'auctionPlugin'); ?> <?php echo $withdraw; ?> <?php _e('credits', 'auctionPlugin'); ?><br />
	<?php _e('Value:', 'auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:', 'auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<strong><?php _e('Payable:', 'auctionPlugin'); ?> <?php echo cp_display_price( $amount, 'ad', false ); ?></strong><br /><br />

	<?php _e('The user has submitted the following payout details:', 'auctionPlugin'); ?><br />
	<?php echo nl2br($details); ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| ITEMS PAID WITH CREDITS
|--------------------------------------------------------------------------
*/

function cp_auction_credit_paid_item_email($order_id, $seller, $uid, $numbers, $mc_fee = '', $mc_gross = '', $txn_id = '', $approves ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');
	$cc = get_user_meta( $seller, 'cp_currency', true );

	// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($seller);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You`ve sent a credit payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, $cc, false ); ?> <?php echo sprintf(__('for %s items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, $cc, false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, $cc, false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$subject = "".__('Purchased items','auctionPlugin')."";
								
	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('just sent you a credit payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, $cc, false ); ?><br />
	<?php echo sprintf(__('for %s of your items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, $cc, false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, $cc, false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| PAYMENT CONFIRMATION TO THE SELLER AFTER COMPLETION OF THE ESCROW
|--------------------------------------------------------------------------
*/

function cp_auction_escrow_payment_confirmation( $order_id, $seller, $uid, $payout, $fees_total, $date ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$user = get_userdata($uid);
	$user1 = get_userdata($seller);

	// AUTHOR NOTIFICATION
	$subject = "".__('Escrow was completed','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('We have just sent you a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $payout, 'ad', false ); ?><br />
	<?php _e('for your items sold at our website.','auctionPlugin'); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $payout, 'ad', false ); ?><br />
	<?php _e('Fees Total:','auctionPlugin'); ?> <?php echo cp_display_price( $fees_total, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $date; ?><br />
	--------------------------------------------------------------------<br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| FAILED PAYMENT
|--------------------------------------------------------------------------
*/

function cp_auction_failed_payment_email($uid, $sid = '', $cause = '') {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$user = get_userdata($uid);

	// ADMIN NOTIFICATION
	$subject = "".__('Failed Payment','auctionPlugin')."";
	$pdt = get_option('cp_auction_plugin_pp_pdt');
	$check = get_option('cp_auction_check_email');

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Hello,','auctionPlugin'); ?><br /><br />
	<?php if ( $pdt == "yes" && $cause == 'token' ) { ?>
	<?php _e('A payment has been made but is flagged as INVALID (Pending). This is due to the author which has not enabled PDT in their account settings, or with PayPal.','auctionPlugin'); ?><br /><br />
	<?php } elseif ( $cause == 'postback' ) { ?>
	<?php _e('A payment has been made but is flagged as INVALID (Pending). This is due to a temporary problem during the postback from PayPal.','auctionPlugin'); ?><br /><br />
	<?php } elseif ( $cause == 'txn' ) { ?>
	<?php _e('A payment has been made but is flagged as INVALID (Pending). This is due to a double postback of transaction id may be that the payer has reloaded the browser during the payment process.','auctionPlugin'); ?><br /><br />
	<?php } elseif ( $check == "yes" && $cause == 'mail' ) { ?>
	<?php _e('A payment has been made but is flagged as INVALID (Pending). This is due to the activation of Check Receiver Email, and the author which has not registered and using its PRIMARY paypal email in their account settings.','auctionPlugin'); ?><br /><br />
	<?php } else { ?>
	<?php _e('A payment has been made but is flagged as INVALID (Pending), please verify the payment manually and contact the payer if necessary.','auctionPlugin'); ?><br/><br/>
	<?php } ?>
	<?php _e('Payers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Payers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| CONFIRM PAYMENT
|--------------------------------------------------------------------------
*/

function cp_auction_confirm_payment_email( $uid, $author, $item_number = '', $numbers = '', $mc_fee = '', $mc_gross = '', $txn_id = '' ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

		// PAYER NOTIFICATION
	$user = get_userdata($uid);
	$user1 = get_userdata($author);
	$payment_date = date_i18n('h:i:s M d, Y A', false, true);

	$subject = "".__('Receipt for your payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You`ve sent a payment of','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?> <?php echo sprintf(__('for %s items sold at our website.','auctionPlugin'), $numbers); ?><br /><br />

	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />

	<?php _e('Sellers Name:','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>)<br />
	<?php _e('Sellers Email:','auctionPlugin'); ?> <?php echo $user1->user_email; ?><br /><br />

	<?php _e('Any downloadable products can be downloaded from your account with us.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );

	// AUTHOR NOTIFICATION
	$subject = "".__('Pending payment','auctionPlugin')."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php echo sprintf(__('has made ​​a payment for %s items purchased at our website, payment has status as pending so you urgently need to confirm the payment via PayPal and then approve it through your account with us.','auctionPlugin'), $numbers); ?><br /><br />
	<?php if( $txn_id ) { ?>
	--------------------------------------------------------------------<br />
	<?php _e('Order ID:','auctionPlugin'); ?> <?php echo $order_id; ?><br />
	<?php _e('Amount:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_gross, 'ad', false ); ?><br />
	<?php _e('Fees:','auctionPlugin'); ?> <?php echo cp_display_price( $mc_fee, 'ad', false ); ?><br />
	<?php _e('Sent on:','auctionPlugin'); ?> <?php echo $payment_date; ?><br />
	<?php _e('Transaction ID:','auctionPlugin'); ?> <?php echo $txn_id; ?><br />
	--------------------------------------------------------------------<br /><br />
	<?php } ?>
	<?php _e('Payers Name:','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>)<br />
	<?php _e('Payers Email:','auctionPlugin'); ?> <?php echo $user->user_email; ?><br /><br />

	<?php _e('You may contact the buyer via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br />

	<?php _e('Please do not let the buyer wait for your confirmation!','auctionPlugin'); ?><br/><br/><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| FILE UPLOADED AD APPROVAL NOTICE
|--------------------------------------------------------------------------
*/

function cp_auction_upload_approves_email($post, $pid, $uid) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');
	$ad_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );

	$user = get_userdata($uid);

	// ADMIN NOTIFICATION
	$subject = "".__('Files was uploaded -','auctionPlugin')." ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Hello', 'auctionPlugin'); ?>,<br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> (<?php echo $user->user_login; ?>) <?php _e('has uploaded the files to be sold through this ad','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink( $pid ); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<?php _e('Don`t forget you need to approve the ad before it goes live,','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_bloginfo('wpurl'); ?>/wp-admin/admin.php?page=cp-all-listings&tab=<?php echo $ad_type; ?>"><?php _e('click here', 'auctionPlugin'); ?></a><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| NEW WANTED OFFER POSTED NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_new_wanted_bid_posted( $pid, $post, $uid, $msg, $bid, $max_price ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($uid);
	$subject = "".__('New offer was posted for ad','auctionPlugin')." ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You have posted an offer of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('for the ad','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<strong><?php _e('Your Message','auctionPlugin'); ?></strong><br />
	<?php echo $msg; ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$user1 = get_userdata($post->post_author);
	$subject = "".__('New offer was posted for your ad','auctionPlugin')." ".$post->post_title;
								
if(($max_price > 0) && ($bid < $max_price)) {

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('A new offer of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been posted for your ad','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<strong><?php _e('Message','auctionPlugin'); ?></strong><br />
	<?php echo $msg; ?><br /><br />

	<?php _e('You may contact the owner via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

} elseif(($max_price > 0) && ($bid > $max_price)) {

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('A new offer of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been posted for your ad','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a>. <?php _e('The offer exceeded your specified maximum price.','auctionPlugin'); ?><br /><br />

	<strong><?php _e('Message','auctionPlugin'); ?></strong><br />
	<?php echo $msg; ?><br /><br />

	<?php _e('You may contact the owner via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

} else {

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('A new offer of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been posted for your ad','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<strong><?php _e('Message','auctionPlugin'); ?></strong><br />
	<?php echo $msg; ?><br /><br />

	<?php _e('You may contact the owner via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

	}
}


/*
|--------------------------------------------------------------------------
| CHOOSE A WANTED WINNER NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_choose_wanted_winner_email( $pid, $post, $uid, $amount ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// WINNER NOTIFICATION
	$user = get_userdata($uid);
	$subject = "".__('You have got a deal','auctionPlugin')." - ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('You have got a deal for the advert','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('for the total amount of','auctionPlugin'); ?> <?php echo cp_display_price( $amount, 'ad', false ); ?><br /><br />

	<?php _e('You may contact the author via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$post->post_author.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION
	$user1 = get_userdata($post->post_author);
	$subject = "".__('Offer accepted for','auctionPlugin')." ".$post->post_title."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('You have chosen the offer','auctionPlugin'); ?> <?php echo cp_display_price( $amount, 'ad', false ); ?> <?php _e('as winner offer for your ad','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<?php _e('You may contact the offer owner via our','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$uid.''); ?>"><?php _e('website.','auctionPlugin'); ?></a><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| WANTED WATCHLIST EMAIL NOTIFICATIONS
|--------------------------------------------------------------------------
*/

function cp_auction_wanted_watchlist_notifications( $pid, $post, $bid, $uid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($uid);
	$subject = "".__('Watchlist Notification','auctionPlugin')." - ".$site_name."";

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('A new offer of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('has been posted for the ad','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br />

	<?php _e('You are receiving this email because you have added this Wanted ad to your watchlist or placed an offer on this ad, you can unsubscribe via your account at anytime','auctionPlugin'); ?><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| WANTED OFFER WAS DECLINED
|--------------------------------------------------------------------------
*/

function cp_auction_wanted_offer_was_declined( $pid, $post, $cid, $bid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($cid);
	$subject = "".__('Your offer was declined for listing','auctionPlugin')." ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('Your offer of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('for the listing','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has been declined.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$user1 = get_userdata($post->post_author);
	$subject = "".__('Offer was declined for listing','auctionPlugin')." ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?>!<br /><br />

	<?php _e('The offer of','auctionPlugin'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('for the listing','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has been declined.','auctionPlugin'); ?><br /><br /><br />


	<?php _e('Thank you for posting your ads with us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| NEW FILE WAS UPLOADED BUYERS
|--------------------------------------------------------------------------
*/

function cp_auction_new_file_uploaded_buyers($pid, $buyer) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$post = get_post( $pid );

	// BUYER NOTIFICATION
	$user = get_userdata($buyer);

	$subject = "".__('New version released','auctionPlugin')." - ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Dear','auctionPlugin'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?>!<br /><br />

	<?php _e('A new version of','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('is made ​​available for download from your account at','auctionPlugin'); ?> <a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo $site_name; ?></a>.<br /><br />
	<?php if( get_user_meta( $post->post_author, 'cp_auction_update_msg', true ) )
	echo ''.nl2br( get_user_meta( $post->post_author, "cp_auction_update_msg", true ) ).'<br /><br /><br />';
	else echo '<br />'; ?>

	<?php _e('If you have any questions feel free to contact us!','auctionPlugin'); ?><br /><br />

	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );

}


/*
|--------------------------------------------------------------------------
| NEW FILE WAS UPLOADED ADMIN
|--------------------------------------------------------------------------
*/

function cp_auction_new_file_uploaded_admin( $pid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$post = get_post( $pid );

	// ADMIN NOTIFICATION
	$user1 = get_userdata($post->post_author);

	$subject = "".__('A new file was uploaded','auctionPlugin')." - ".$post->post_title;

	ob_start();

	if( $plain_html == "html" ) {
	include("email-header.php");
	} ?>

	<?php _e('Hello','auctionPlugin'); ?>,<br /><br />

	<?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?> (<?php echo $user1->user_login; ?>) <?php _e('has just uploaded a new file for their ad listing','auctionPlugin'); ?> <a style="text-decoration: none" href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a><br /><br /><br />


	<strong><?php echo $site_name; ?></strong><br />
	<a style="text-decoration: none" href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( $plain_html == "html" ) {
	include("email-footer.php");
	}

	$message = ob_get_contents();

	if( $plain_html == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( get_bloginfo('admin_email'), $subject, $message, $headers );
}