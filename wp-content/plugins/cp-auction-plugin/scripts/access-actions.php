<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| ACTION NO ACCESS TO CHOOSEN CONTENTS OR PAGES BY VISITORS
|--------------------------------------------------------------------------
*/

function cp_auction_no_access_out() {

$single = get_option('cp_auction_plugin_access_single');
$pages = get_option('cp_auction_plugin_access_pages');
$form = get_option('cp_auction_plugin_disable_alogin');
$pageid = get_the_ID();
$ids = explode(',', $pages);

$page_id = "";
if ( $pageid && in_array($pageid, $ids) ) {
	$page_id = "yes";
} else {
	return;
}

if (( $single == "yes" && is_singular( APP_POST_TYPE ) && !is_user_logged_in() ) || ( $page_id == "yes" && !is_user_logged_in() )) { ?>

		<div class="shadowblock_out">

			<div class="shadowblock">

					<h2 class="dotted"><span class="colour"><?php echo get_option('cp_auction_plugin_access_title'); ?></span></h2>

					<?php if( get_option('cp_auction_plugin_access_info') ) { ?>
					<div class="notice success"><?php echo nl2br(get_option('cp_auction_plugin_access_info')); ?></div>
					<?php } ?>

					<div class="clear10"></div>

				<?php if( $form != "yes" ) { ?>

					<div class="padd10">
					<p><?php _e( 'Please complete the fields below to login to your account.', 'auctionPlugin' ); ?></p>
					</div>

						<form action="<?php echo wp_login_url(); ?>" method="post" class="loginform" id="login-form">
		
							<p>
								<label for="login_username"><?php _e( 'Username:', 'auctionPlugin' ); ?></label>
								<input type="text" class="text required" name="log" id="login_username" value="<?php if (isset($posted['login_username'])) echo esc_attr($posted['login_username']); ?>" />
							</p>

							<p>
								<label for="login_password"><?php _e( 'Password:', 'auctionPlugin' ); ?></label>
								<input type="password" class="text required" name="pwd" id="login_password" value="" />
							</p>

							<div class="clr"></div>

							<div id="checksave">

								<p class="rememberme">
									<input name="rememberme" class="checkbox" id="rememberme" value="forever" type="checkbox" checked="checked" />
									<label for="rememberme"><?php _e( 'Remember me', 'auctionPlugin' ); ?></label>
								</p>

								<p class="submit">
									<input type="submit" class="btn_orange" name="login" id="login" value="<?php _e( 'Login &raquo;', 'auctionPlugin' ); ?>" />
									<?php echo APP_Login::redirect_field(); ?>
									<input type="hidden" name="testcookie" value="1" />
								</p>

								<p class="lostpass">
									<a class="lostpass" href="<?php echo appthemes_get_password_recovery_url(); ?>" title="<?php _e( 'Password Lost and Found', 'auctionPlugin' ); ?>"><?php _e( 'Lost your password?', 'auctionPlugin' ); ?></a>
								</p>

								<?php wp_register('<p class="register">','</p>'); ?>

								<?php do_action('login_form'); ?>

							</div>

						</form>

						<!-- autofocus the field -->
						<script type="text/javascript">try{document.getElementById('login_username').focus();}catch(e){}</script>

					<div class="clr"></div>

				<?php } ?><!-- /login form -->

				</div><!-- /shadowblock -->

			</div><!-- /shadowblock_out -->

			<?php if( get_option('stylesheet') == "twinpress_classifieds") { ?>
            		</div><!-- /conl -->
			<?php } ?>

			</div><!-- /content_left -->

			<?php get_sidebar( 'page' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->

		<?php appthemes_before_footer(); ?>
		<?php get_footer( app_template_base() ); ?>
		<?php appthemes_after_footer(); ?>

<?php if(( get_option('stylesheet') == "citrus-night" ) || ( get_option('stylesheet') == "eldorado") || ( get_option('stylesheet') == "Phoenix_2_1" ) || ( get_option('stylesheet') == "greenymarketplace" )) { ?>
	</div><!-- /wrapper -->
	<?php } ?>

<?php wp_footer(); ?>

	<?php appthemes_after(); ?>

</body>

</html>

<?php
exit;
}
}
add_action('appthemes_before_loop', 'cp_auction_no_access_out');
add_action('appthemes_before_page_loop', 'cp_auction_no_access_out');
add_action('appthemes_before_blog_loop', 'cp_auction_no_access_out');
add_action('cp_auction_block_access', 'cp_auction_no_access_out');


/*
|--------------------------------------------------------------------------
| ACTION NO ACCESS TO CHOOSEN CONTENTS OR PAGES BY LOGGED IN USERS
|--------------------------------------------------------------------------
*/

function cp_auction_no_access_in() {

	global $current_user;
    	$current_user = wp_get_current_user();
    	$uid = $current_user->ID;
    	$array = cp_auction_get_user_role($uid);
    	$blocked = "";

$roles = get_option('cp_auction_blocked_roles');
$posts = get_option('cp_auction_plugin_access_posts');
$form = get_option('cp_auction_plugin_disable_ilogin');
$pageid = get_the_ID();
$ids = explode(',', $posts);

$page_id = "";
if (in_array($pageid, $ids)) {
	$page_id = "yes";
} else {
	return;
}

if (in_array($roles, $array)) {
	$blocked = "yes";
}

if ( $blocked == "yes" && is_user_logged_in() ) { ?>

<div class="shadowblock_out">

				<div class="shadowblock">

					<h2 class="dotted"><span class="colour"><?php echo get_option('cp_auction_plugin_roles_title'); ?></span></h2>

					<?php if( get_option('cp_auction_plugin_access_info') ) { ?>
					<div class="notice success"><?php echo nl2br(get_option('cp_auction_plugin_roles_info')); ?></div>
					<?php } ?>

					<div class="clear10"></div>

				<?php if( $form != "yes" ) { ?>

					<div class="padd10">
					<p><?php _e( 'Please complete the fields below to login to your account.', 'auctionPlugin' ); ?></p>
					</div>

						<form action="<?php echo wp_login_url(); ?>" method="post" class="loginform" id="login-form">
		
							<p>
								<label for="login_username"><?php _e( 'Username:', 'auctionPlugin' ); ?></label>
								<input type="text" class="text required" name="log" id="login_username" value="<?php if (isset($posted['login_username'])) echo esc_attr($posted['login_username']); ?>" />
							</p>

							<p>
								<label for="login_password"><?php _e( 'Password:', 'auctionPlugin' ); ?></label>
								<input type="password" class="text required" name="pwd" id="login_password" value="" />
							</p>

							<div class="clr"></div>

							<div id="checksave">

								<p class="rememberme">
									<input name="rememberme" class="checkbox" id="rememberme" value="forever" type="checkbox" checked="checked" />
									<label for="rememberme"><?php _e( 'Remember me', 'auctionPlugin' ); ?></label>
								</p>

								<p class="submit">
									<input type="submit" class="btn_orange" name="login" id="login" value="<?php _e( 'Login &raquo;', 'auctionPlugin' ); ?>" />
									<?php echo APP_Login::redirect_field(); ?>
									<input type="hidden" name="testcookie" value="1" />
								</p>

								<p class="lostpass">
									<a class="lostpass" href="<?php echo appthemes_get_password_recovery_url(); ?>" title="<?php _e( 'Password Lost and Found', 'auctionPlugin' ); ?>"><?php _e( 'Lost your password?', 'auctionPlugin' ); ?></a>
								</p>

								<?php wp_register('<p class="register">','</p>'); ?>

								<?php do_action('login_form'); ?>

							</div>

						</form>

						<!-- autofocus the field -->
						<script type="text/javascript">try{document.getElementById('login_username').focus();}catch(e){}</script>

					<div class="clr"></div>

				<?php } ?><!-- /login form -->

				</div><!-- /shadowblock -->

			</div><!-- /shadowblock_out -->

			<?php if( get_option('stylesheet') == "twinpress_classifieds") { ?>
            		</div><!-- /conl -->
			<?php } ?>

			</div><!-- /content_left -->

			<?php get_sidebar( 'page' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->

		<?php appthemes_before_footer(); ?>
		<?php get_footer( app_template_base() ); ?>
		<?php appthemes_after_footer(); ?>

<?php if(( get_option('stylesheet') == "citrus-night" ) || ( get_option('stylesheet') == "eldorado") || ( get_option('stylesheet') == "Phoenix_2_1" ) || ( get_option('stylesheet') == "greenymarketplace" )) { ?>
	</div><!-- /wrapper -->
	<?php } ?>

<?php wp_footer(); ?>

	<?php appthemes_after(); ?>

</body>

</html>

<?php
exit;
}
}
add_action('appthemes_before_loop', 'cp_auction_no_access_in');
add_action('appthemes_before_page_loop', 'cp_auction_no_access_in');
add_action('appthemes_before_blog_loop', 'cp_auction_no_access_in');
add_action('cp_auction_block_access', 'cp_auction_no_access_in');