<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| CHECK MEMBERSHIP CATEGORIES & ACTIVE MEMBERSHIP PACKS
|--------------------------------------------------------------------------
*/

function cp_auction_membership_requirement($catID) {
	global $cp_options;

	if( get_option('cp_version') < "3.3" ) {
	$required_membership = get_option('cp_required_membership_type');
	$required_cat = get_option('cp_required_categories');
	} else {
	$required_membership = $cp_options->required_membership_type;
	$required_cat = $cp_options->required_categories;
	}

	//if all posts require "required" memberships
	if ( $required_membership == 'all' ) { return 'all'; }
	//if post requirements are based on category specific requirements
	elseif ( $required_membership == 'category' ) {
		//check if catID option exists to determine if its a required to post category
		$required_categories = $required_cat;
		if ( isset($required_categories[$catID])) return $catID;
	}
	//no requirements active
	else return false;
}


//if not meet membership requirement, redirect to membership purchase page
function cp_auction_redirect_membership($catID = '') {
	global $current_user, $app_abbr, $cp_options;

	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	if( get_option('cp_version') < "3.3" ) {
	$memtype = get_option($app_abbr.'_required_membership_type');
	$enable_pack = get_option($app_abbr.'_enable_membership_packs');
	} else {
	$memtype = $cp_options->required_membership_type;
	$enable_pack = $cp_options->enable_membership_packs;
	}

	//if membership required to post, and no membership is active on logged in user, redirect to membership page
	$current_membership = isset($current_user->active_membership_pack) ? get_pack($current_user->active_membership_pack) : false;

	if( $catID ) $current_requirement = cp_auction_membership_requirement($catID);
	if( $memtype == 'all' ) $current_requirement = 'all';

	//if requirement is found, but required is not in the users pack type, fail and redirect to membership page.
	if ( isset($current_requirement) && $current_requirement && $enable_pack == 'yes' ) {
	
		//if no membership, or if membership but not a membership that satisfies required memberships, or if membership expired
		if( !isset($current_membership->pack_type) || !isset($current_user->membership_expires) || (isset($current_membership->pack_type) && !stristr($current_membership->pack_type, 'required')) || (appthemes_days_between_dates($current_user->membership_expires) < 0) ) {
			update_user_meta( $uid, 'cp_auction_membership_active', 'no' );
			$redirect_url = esc_url( add_query_arg( array( 'membership' => 'required', 'cat' => $current_requirement ), CP_MEMBERSHIP_PURCHASE_URL ) );
			wp_redirect( $redirect_url );
			exit;
		}
	}

}

// check if pack is active
function cp_auction_active_pack($packID) {

	global $current_user;

	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	//if membership required to post, and no membership is active on logged in user, redirect to membership page
	$current_membership = isset($current_user->active_membership_pack) ? get_pack($current_user->active_membership_pack) : false;

	if( !isset($current_membership->pack_type) || !isset($current_user->membership_expires) || (isset($current_membership->pack_type) && !stristr($current_membership->pack_type, 'required')) || (appthemes_days_between_dates($current_user->membership_expires) < 0) ) {

	return false;
	}
	else {
	return true;
	}
}

// figure out what the total membership cost will be when both membership and coupon is active
function cp_auction_calc_ad_cost($membership_pack_id = '', $amount, $couponCode = '') {

	if( $membership_pack_id > 0 ) {
	$membership = cp_auction_get_pack($membership_pack_id);
	$res = explode("_", $membership->pack_type);
	$required = $res[0];
	$price_type = $res[1];

	if( get_option('cp_version') < "3.3.0" )
	$cp_coupon = cp_auction_get_coupons($couponCode);
	elseif( get_option('cp_version') > "3.2.1" ) $cp_coupon = cp_auction_get_post_id('code', $couponCode);

	if( $price_type == "static" ) $pack_out = $membership->pack_price;
	if( $price_type == "discount" ) $pack_out = $amount - $membership->pack_price;
	if( $price_type == "percentage" ) $pack_out = $amount - ($amount * (((float)($membership->pack_price))/100));

	if( $pack_out < 0 ) $pack_out = 0;
	$amount = $pack_out;
	}
	if( ! empty( $couponCode ) ) {
	//discount for coupon amount if its set
	if( get_option('cp_version') < "3.3.0" ) {
	if ( isset($cp_coupon[0]->coupon_discount) && isset($cp_coupon[0]->coupon_discount_type) ) {
		if ( $cp_coupon[0]->coupon_discount_type != '%' )
			$amount = $pack_out - (float)$cp_coupon[0]->coupon_discount;
		else
			$amount = $pack_out - ($pack_out * (((float)($cp_coupon[0]->coupon_discount))/100));
	}
	}
	if( get_option('cp_version') > "3.2.1" ) {
	$cp_type = get_post_meta( $cp_coupon, 'type', true );
	$cp_amount = get_post_meta( $cp_coupon, 'amount', true );
		if ( $cp_type != 'percent' )
			$amount = $pack_out - (float)$cp_amount;
		else
			$amount = $pack_out - ($pack_out * (((float)($cp_amount))/100));
	}
	}
	//set proper return format
	$amount_out = cp_display_price( $amount, 'ad', false );	

	//if total cost is less then zero, then make the cost zero (free)
	if ( $amount_out < 0 )
		$amount_out = 0;

	return $amount_out;
}

// figure out what the total membership cost will be with or without active coupon code
function cp_auction_coupon_ad_cost($amount, $couponCode = '') {

	if( ! empty( $couponCode ) ) {

	if( get_option('cp_version') < "3.3.0" )
	$cp_coupon = cp_auction_get_coupons($couponCode);
	elseif( get_option('cp_version') > "3.2.1" ) $cp_coupon = cp_auction_get_post_id('code', $couponCode);

	//discount for coupon amount if its set
	if( get_option('cp_version') < "3.3.0" ) {
	if ( isset($cp_coupon[0]->coupon_discount) && isset($cp_coupon[0]->coupon_discount_type) ) {
		if ( $cp_coupon[0]->coupon_discount_type != '%' )
			$total = $amount - (float)$cp_coupon[0]->coupon_discount;
		else
			$total = $amount - ($amount * (((float)($cp_coupon[0]->coupon_discount))/100));
	}
	}
	if( get_option('cp_version') > "3.2.1" ) {
	$cp_type = get_post_meta( $cp_coupon, 'type', true );
	$cp_amount = get_post_meta( $cp_coupon, 'amount', true );
		if ( $cp_type != 'percent' )
			$total = $amount - (float)$cp_amount;
		else
			$total = $amount - ($amount * (((float)($cp_amount))/100));
	}
	//set proper return format
	$amount_out = cp_display_price( $total, 'ad', false );

	//if total cost is less then zero, then make the cost zero (free)
	if ( $amount_out < 0 )
		$amount_out = 0;

	return $amount_out;

	}
	else {

	return $amount;
}	
}

//function retreives the membership pack name given a membership pack ID
function cp_auction_get_pack($theID, $type = '', $return = '') {
	global $wpdb, $the_pack;

	if ( stristr($theID, 'pend') )
	    $theID = cp_auction_get_pack_id($theID);

	//if the type is dashboard or ad, then get the assume the ID sent is the postID and packID needs to be obtained
	if ( $type == 'ad' || $type == 'dashboard' )
		$theID = cp_auction_get_pack_id( $theID, $type );

	//make sure the value is a proper MySQL int value
	$theID = intval($theID);

	if ( $theID > 0 )
		$table_name = $wpdb->prefix . "cp_ad_packs";
		$the_pack = $wpdb->get_row( "SELECT * FROM {$table_name} WHERE pack_id = '$theID'" );

	if ( !empty($return) && !empty($the_pack)) {
		$the_pack = (array)$the_pack;

		if ( $return == 'array' )
		    return $the_pack;
		else
		    return $the_pack[$return];
	}

	return $the_pack;
}

//function send a string and attempt to filter out and return only the actual packID
function cp_auction_get_pack_id($active_pack, $type = '') {
	if ( !empty($type) ) { /*TODO LOOKUP PACK ID FROM POST - Will be possible once pack is stored with posts*/	}
	preg_match('/^pend(?P<pack_id>\w+)-(?P<order_id>\w+)/', $active_pack, $matches);

	if ($matches)
	    return $matches['pack_id'];
	else
	    return $active_pack;
}

//get a list of coupons, or details about a single coupon if an Coupon Code is passed
function cp_auction_get_coupons($couponCode = '') {
    global $wpdb;
    $table_name = $wpdb->prefix . "cp_coupons";
    $sql = "SELECT * "
    . "FROM {$table_name} ";
    if($couponCode != '')
    $sql .= "WHERE coupon_code='$couponCode' ";
    $sql .= "ORDER BY coupon_id desc";

    $results = $wpdb->get_results($sql);
    return $results;
}