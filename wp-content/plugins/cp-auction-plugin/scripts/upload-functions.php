<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function cp_auction_plugin_upload_dir() {

	$wp_upload_dir = wp_upload_dir();
	$upload_path = $wp_upload_dir['basedir'] . '/auction' . $wp_upload_dir['subdir'];

	if ( !file_exists( $upload_path ) )
		@wp_mkdir_p( $upload_path );

	$inst = false;
	$args = array( 'Options All -Indexes', '', '<FilesMatch ".(htaccess|htpasswd|ini|php|phps|fla|psd|log|sh)$">', 'Order Allow,Deny', 'Deny from all', '</FilesMatch>', '', '<Files *.php>', 'deny from all', '</Files>', '' );
	foreach( $args as $arg ) {
	$inst .= $arg . "\r\n";
	}
	$rules = $inst;

	if ( !@file_get_contents( $wp_upload_dir['basedir'] . '/auction/.htaccess' ) ) {
		wp_mkdir_p( $wp_upload_dir['basedir'] . '/auction' );
		}
		@file_put_contents( $wp_upload_dir['basedir'] . '/auction/.htaccess', $rules );

	if ( wp_mkdir_p( $upload_path ) ) {
		if( ! file_exists( $upload_path . '/index.php' ) ) {
			@file_put_contents( $upload_path . '/index.php', '<?php' . PHP_EOL . '// Silence is healthy.' );
		}
		if( ! file_exists( $upload_path . '/.htaccess' ) ) {
			@file_put_contents( $upload_path . '/.htaccess', $rules );
		}
}

	$fullpath = $upload_path .'/';

	return $fullpath;

}