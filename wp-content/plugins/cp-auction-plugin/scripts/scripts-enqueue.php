<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	global $cp_options;

function cp_auction_ajax_load_scripts() {
	// load our jquery file that sends the $.post request
	wp_enqueue_script( 'ajax-auction', cp_auction_plugin_url() . '/js/jquery-auction.js', array( 'jquery' ), '1.0.0' );
	wp_localize_script( 'ajax-auction', 'translatable_text_string', cp_auction_localize_vars() );
 
	// make the ajaxurl var available to the above script
	wp_localize_script( 'ajax-auction', 'cp_auction_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
}
add_action('wp_print_scripts', 'cp_auction_ajax_load_scripts');


function register_cp_auction_scripts() {

	wp_enqueue_script( 'jquery-tools', cp_auction_plugin_url() . '/js/jquery.tool.js', array( 'jquery','jquery-ui-tabs' ), '3.3' );

	wp_deregister_style( 'jquery-ui-style' );
	wp_enqueue_style( 'jquery-ui-style', cp_auction_plugin_url() . '/css/jquery-ui.css', false, '1.9.2' );
    
}
if( get_option('stylesheet') != "jibo" )
add_action( 'wp_enqueue_scripts', 'register_cp_auction_scripts', 100 );


function cp_auction_load_tabs_scripts() {
	// load our jquery file that sends the $.post request
	wp_enqueue_script( 'ajax-auction-tabs', cp_auction_plugin_url() . '/js/jquery-tabs.js', array( 'jquery','jquery-effects-slide','jquery-effects-blind' ), '3.3' );
}


function cp_auction_load_datepicker() {

	wp_enqueue_script( 'timepicker', cp_auction_plugin_url() . '/js/jquery-ui-timepicker-addon.js', array( 'jquery-ui-core', 'jquery-ui-datepicker' ), '1.0.0' );
	wp_enqueue_style( 'timepicker-styles', cp_auction_plugin_url() . '/css/jquery-ui-timepicker-addon.css', false, '1.10.0' );

}


function cp_auction_custom_style_sheets() {

	global $cp_options;
	$child = get_option('stylesheet'); // returns lowercase
	$stylesheet = ''.supported_child_themes($child).'.css';
	if ( get_option( 'CP_VERSION' ) > '3.4.1' && $child == "classipress" ) $stylesheet = "classipress35.css";
	if ( get_option( 'CP_VERSION' ) > '3.4.1' && $child == "rondell" ) $stylesheet = "rondell35.css";
	$style = "style.css";
	if ( get_option( 'CP_VERSION' ) > '3.4.1' ) $style = "style35.css";

		wp_enqueue_style( 'main-auction-style', plugins_url( 'cp-auction-plugin/css/'.$style.'') );

		if ( ! $cp_options->disable_stylesheet ) {
		if( $child == "simply-responsive-cp" ) {
		wp_enqueue_style( 'cp-child-custom', plugins_url( 'cp-auction-plugin/css/'.$child.'/'.$cp_options->stylesheet, false ) );
		}
		wp_enqueue_style( 'cp-child-custom', plugins_url( 'cp-auction-plugin/css/'.$stylesheet, false ) );
		} else {
		wp_enqueue_style( 'cp-child-custom', plugins_url( 'cp-auction-plugin/css/'.$stylesheet, false ) );
		}
		if( $child == "flatpress" ) {
		if( isset( $_GET['css'] ) ) {
		$css = $_GET['css'];
		} else {
		$css = fl_get_option( 'fl_stylesheet' );
		}
		wp_enqueue_style( 'cp-fl-color', plugins_url( 'cp-auction-plugin/css/flatpress/' . $css . '.css', false ) );
		}
		if( get_option('cp_auction_custom_style') == "yes" ) {
		wp_enqueue_style( 'cp-auction-custom', plugins_url( 'cpauction.css', false ) );
		}

}
add_action('wp_head','cp_auction_custom_style_sheets', 100 );


if( get_option('stylesheet') == "classipress-child" ) {
	add_action('wp_head','cp_auction_plugin_classipress_child_style_sheet');
}
function cp_auction_plugin_classipress_child_style_sheet() {

	wp_enqueue_style( 'child-auction-style', plugins_url( 'cp-auction-plugin/css/classipress_child.css') );

}

if( get_option('stylesheet') == "greenymarketplace" ) {
	add_action('wp_head','cp_auction_plugin_greeny_style_sheet');
}
function cp_auction_plugin_greeny_style_sheet() {

	wp_enqueue_style( 'greeny-auction-style', plugins_url( 'cp-auction-plugin/css/greeny.css') );

}

function cp_auction_register_js_scripts() {

	wp_enqueue_script( 'front-auction-countdown', plugins_url( 'cp-auction-plugin/js/countdown.js', 'jquery', '1.0.0' ) );

}


// to speed things up, don't load these scripts in the WP back-end (which is the default)
if ( !is_admin() ) {
	add_action( 'wp_enqueue_scripts', 'cp_auction_register_js_scripts', 5 );
}


/*
|--------------------------------------------------------------------------
| LOADS CPRATE CSS AND JS FILES ON PAGES WHERE WE NEED THEM
|--------------------------------------------------------------------------
*/

function cp_auction_cprate_scripts() {

	wp_register_style( 'cprate-css', plugins_url('/css/cprate.css', 'cprate-plugin') );
	wp_register_script('cprate_plugin', plugins_url('/js/cprate-jqplugin.js', 'cprate-plugin') , array('jquery'), '2.50', true);
	wp_register_script('cprate_js', plugins_url('/js/cprate.js', 'cprate-plugin') , array('jquery'), '2.50', true);

	wp_enqueue_style( 'cprate-css' );
	wp_enqueue_script( 'cprate_plugin' );
       	wp_enqueue_script( 'cprate_js' );
       	if ( function_exists('cprate_localize_vars') ) {
       	wp_localize_script( 'cprate_js', 'cprate_text_string', cprate_localize_vars() );
	}

}


/*
|--------------------------------------------------------------------------
| LOADS EASY TOOLTIP SCRIPT ON SINGLE ADS PAGE
|--------------------------------------------------------------------------
*/

function cp_auction_easytooltip() {
	if( is_singular() )
	wp_enqueue_script( 'easytooltip', get_template_directory_uri() . '/includes/js/easyTooltip.js', array( 'jquery' ), '1.0' );
}
add_action( 'wp_enqueue_scripts', 'cp_auction_easytooltip', 100 );


/*
|--------------------------------------------------------------------------
| REGISTER ADMIN BACKEND SCRIPTS AND STYLESHEETS
|--------------------------------------------------------------------------
*/

function load_admin_menu_scripts() {

	wp_enqueue_style( 'admin-auction-style', plugins_url( 'cp-auction-plugin/css/admin.css'), false, '1.0.0' );

}

if ( is_admin() ) {
	add_action( 'admin_print_styles', 'load_admin_menu_scripts' );
	add_action('wp_print_scripts', 'cp_auction_ajax_load_scripts');
}