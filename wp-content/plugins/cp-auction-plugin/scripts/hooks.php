<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| HOOKS PLACED IN THE CUSTOM SIDEBAR-AD.PHP FILE
|--------------------------------------------------------------------------
*/

// if google map is removed in the custom sidebar use this hook to add anything else
function cp_auction_custom_sidebar() {
	do_action( 'cp_auction_custom_sidebar' );
}


/*
|--------------------------------------------------------------------------
| HOOKS USED AROUND
|--------------------------------------------------------------------------
*/

// add the sidebar content to the admin settings pages
function cp_auction_admin_sidebar() {
	do_action( 'cp_auction_admin_sidebar' );
}

// block users from pages where hook added
function cp_auction_block_access() {
	do_action( 'cp_auction_block_access' );
}

// add the correct dashboard to cp auction userpanel
function cp_auction_userpanel_selector( $query ) {
	do_action( 'cp_auction_userpanel_selector', $query );
}

// display the shopping cart
function cp_auction_display_cart() {
	do_action( 'cp_auction_display_cart' );
}

function cp_auction_countdown_timer() {
	do_action( 'cp_auction_countdown_timer' );
}

function cp_auction_bidder_box() {
	do_action( 'cp_auction_bidder_box' );
}

function cp_auction_wanted_box() {
	do_action( 'cp_auction_wanted_box' );
}

function cp_auction_bidder_box_tabbed() {
	do_action( 'cp_auction_bidder_box_tabbed' );
}

function cp_auction_bidder_box_widget() {
	do_action( 'cp_auction_bidder_box_widget' );
}

function cp_auction_wanted_box_widget() {
	do_action( 'cp_auction_wanted_box_widget' );
}

function cp_auction_buy_now_button() {
	do_action( 'cp_auction_buy_now_button' );
}

function cp_auction_before_buynow_button() {
	do_action( 'cp_auction_before_buynow_button' );
}

function cp_auction_after_bidder_box() {
	do_action( 'cp_auction_after_bidder_box' );
}

function cp_auction_after_wanted_box() {
	do_action( 'cp_auction_after_wanted_box' );
}

function cp_auction_info_messages() {
	do_action( 'cp_auction_info_messages' );
}

// Add price details to Items I Sold
function cp_auction_price_details( $type = 'details' ) {
	do_action( 'cp_auction_price_' . $type );
}