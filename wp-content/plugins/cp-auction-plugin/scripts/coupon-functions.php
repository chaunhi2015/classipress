<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| CHECK BUY NOW COUPON CODES
|--------------------------------------------------------------------------
*/

//check coupon code against coupons in the database and return the discount
function cp_auction_check_coupon_discount($pid, $author, $couponCode) {
	//stop if no coupon code is passed or passed empty
	if($couponCode == '') return false;

	//get the coupon
	$results = cp_auction_get_user_coupons($couponCode);

	//stop if result is empty or inactive
	if(!$results) return false;
	if(($results[0]->coupon_pid != $pid) && ($results[0]->coupon_pid != 0)) return false;
	if($results[0]->coupon_uid != $author) return false;
	if($results[0]->coupon_status != 'active') return false;
	if(($results[0]->coupon_use_count >= $results[0]->coupon_max_use_count) && ($results[0]->coupon_max_use_count != 0)) return false;
	if(strtotime($results[0]->coupon_expire_date) < strtotime(date("Y-m-d"))) return false;
	if(strtotime($results[0]->coupon_start_date) > strtotime(date("Y-m-d"))) return false;

	//if coupon exists and is not inactive then return the discount
	return $results[0];
}

//function uses a coupon code by incrimenting its value in the database
function cp_auction_use_coupon($couponCode) {
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_coupons";
	$update = $wpdb->prepare( "UPDATE {$table_name} SET coupon_use_count = coupon_use_count + 1 WHERE coupon_code = %s", $couponCode );
	$results = $wpdb->query( $update );
}

//get a list of coupons, or details about a single coupon if an Coupon Code is passed
function cp_auction_get_user_coupons($couponCode = '') {
    global $wpdb;
    $table_name = $wpdb->prefix . "cp_auction_plugin_coupons";
    $sql = "SELECT * "
    . "FROM {$table_name} ";
    if($couponCode != '')
    $sql .= "WHERE coupon_code='$couponCode' ";
    $sql .= "ORDER BY coupon_id desc";

    $results = $wpdb->get_results($sql);
    return $results;
}

// figure out what the total membership cost will be with or without active coupon code
function cp_auction_coupon_item_cost($amount, $couponCode = '') {

	if( ! empty( $couponCode ) ) {

	$cp_coupon = cp_auction_get_user_coupons($couponCode);

	//discount for coupon amount if its set
	if ( isset($cp_coupon[0]->coupon_discount) && isset($cp_coupon[0]->coupon_discount_type) ) {
		if ( $cp_coupon[0]->coupon_discount_type != '%' )
			$total = $amount - (float)$cp_coupon[0]->coupon_discount;
		else
			$total = $amount - ($amount * (((float)($cp_coupon[0]->coupon_discount))/100));
	}
	//set proper return format
	$amount_out = cp_auction_format_amount($total, 'process');

	//if total cost is less then zero, then make the cost zero (free)
	if ( $amount_out < 0 )
		$amount_out = 0;

	return $amount_out;

	}
	else {

	return $amount;
}
}

/* ------------------------------------------ END BUY NOW COUPONS ------------------------------------------ */


/*
|--------------------------------------------------------------------------
| CHECK POST NEW COUPON CODES
|--------------------------------------------------------------------------
*/

//check coupon code against coupons in the database and return the discount
function cp_auction_check_cp_coupon_discount($uid, $couponCode = '') {
	//stop if no coupon code is passed or passed empty
	if($couponCode == '') return false;

	//get the coupon
	$pid = cp_auction_get_post_id('code', $couponCode);
	$amount = get_post_meta( $pid, 'amount', true );
	$start_date = get_post_meta( $pid, 'start_date', true );
	$cp_sys_expire_date = get_post_meta( $pid, 'cp_sys_expire_date', true );
	$use_limit = get_post_meta( $pid, 'use_limit', true );
	$use_count = get_post_meta( $pid, 'use_count', true );
	$user_use_limit = get_post_meta( $pid, 'user_use_limit', true );
	$user_used = get_user_meta( $uid, $couponCode, true );

	//stop if result is empty or inactive
	if(!$pid) return false;
	if(($use_limit != 0) && ($use_count >= $use_limit)) return false;
	if(($user_use_limit != 0) && ($user_used >= $user_use_limit)) return false;
	if(strtotime($cp_sys_expire_date) < strtotime(date("Y-m-d"))) return false;
	if(strtotime($start_date) > strtotime(date("Y-m-d"))) return false;

	//if coupon exists and is not inactive then return the discount
	return $amount;
}