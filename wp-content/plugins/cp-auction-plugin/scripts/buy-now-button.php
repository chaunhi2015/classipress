<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| BUY NOW BUTTON STANDARD
|--------------------------------------------------------------------------
*/

function cp_auction_buy_now_button_standard( $pid ) {

	global $post, $current_user;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	$post = get_post( $pid );

	$singular = is_singular( APP_POST_TYPE );
	if ( get_option( 'cp_auction_enable_loop_shopping' ) == "yes" ) $singular = true;

	if( ! $singular && ! is_search() ) return;
	if ( get_option( 'cp_auction_hide_buy_now_button' ) == "yes" && $uid == $post->post_author ) return;

	$ad_enabled = get_option('cp_auction_buy_now_ad_enabled');
	$paypal = get_user_meta( $post->post_author, 'paypal_email', true );
	$cih = get_user_meta( $post->post_author, 'cp_accept_cih_payment', true );
	$cod = get_user_meta( $post->post_author, 'cp_accept_cod_payment', true );
	$bankt = get_user_meta( $post->post_author, 'cp_accept_bank_transfer_payment', true );
	if( function_exists('aws_cp_settings_transactions') )
	$credits = get_user_meta( $post->post_author, 'cp_accept_credit_payment', true );
	else $credits = false;

	$bank_transfer = false;
	if( get_option('cp_auction_plugin_pay_admin') == "yes" && get_option('cp_auction_plugin_pay_user') == "yes" ) {
	$pay = "both";
	} elseif( get_option('cp_auction_plugin_pay_admin') == "yes" && get_option('cp_auction_plugin_pay_user') == "no" ) {
	$pay = "admin";
	} else {
	$pay = "user";
	}
	$users_choice = get_user_meta( $post->post_author, 'cp_use_safe_payment', true );
	if( empty( $users_choice ) ) $users_choice = "yes";

	if( $pay == "both" && $users_choice == "yes" ) {
	$bt = "yes";
	} else if( $pay == "admin" ) {
	$bt = "yes";
	} else {
	$bt = false;
	}

	$search = get_option('cp_auction_enable_search_shopping');
	$cp_buy_now_enabled = get_post_meta($post->ID, 'cp_buy_now_enabled', true);
	$cp_buy_now = get_post_meta($post->ID, 'cp_buy_now', true);
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if( empty( $cp_buy_now ) && ( empty( $my_type ) || $my_type == "classified" ) )
	$cp_buy_now = get_post_meta($post->ID, 'cp_price', true);
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	if ( $post )
	$imp = get_post_meta( $post->ID, 'a2pItemID', true );
	else $imp = false;

	$wantto = get_post_meta( $post->ID, 'cp_want_to', true );
	$want_to = get_localized_field_values('cp_want_to', $wantto);
	if( empty( $wantto ) ) {
	$wantto = get_post_meta( $post->ID, 'cp_iwant_to', true );
	$want_to = get_localized_field_values('cp_iwant_to', $wantto);
	}
	if( get_option('cp_auction_plugin_esc_bank_transfer') == "yes" && $bt == "yes" && get_option('cp_auction_plugin_pay_admin') == "yes" ) $bt_escrow = true;
	if( get_option('cp_auction_plugin_cih') == "yes" && $cih == "yes" ) $cih = true;
	if( get_option('cp_auction_plugin_cod') == "yes" && $cod == "yes" ) $cod = true;
	if( get_option('cp_auction_plugin_bank_transfer') == "yes" && $bankt == "yes" ) $bankt = true;

	if( ( ( $ad_enabled == "yes" && $cp_buy_now_enabled == true ) && ( ( $cp_buy_now > '0' && $closed != "yes" && $imp == false ) && ( empty ( $want_to ) || $want_to == "Sell" ) 
	&& ( $paypal == true || $credits == true || $bt_escrow == true || $cih == true || $cod == true || $bankt == true ) ) ) || ( ( is_search() && $search == "yes" ) 
	&& ( ( $ad_enabled == "yes" && $cp_buy_now_enabled == true ) && ( ( $cp_buy_now > '0' && $closed != "yes" && $imp == false ) && ( empty( $my_type ) || $my_type == "classified" ) 
	&& ( empty ( $want_to ) || $want_to == "Sell" ) && ( $paypal == true || $credits == true || $bt_escrow == true || $cih == true || $cod == true || $bankt == true ) ) ) ) )
	return cp_auction_buy_now_action($post);
	else return false;

}


/*
|--------------------------------------------------------------------------
| BUY NOW BUTTON SHORTCODE
|--------------------------------------------------------------------------
*/

function cp_auction_buy_now_button_shortcode( $ids ) {

	global $post;
	$pid = "{$ids['pid']}";
	$post = get_post( $pid );

	$ad_enabled = get_option('cp_auction_buy_now_ad_enabled');
	$paypal = get_user_meta( $post->post_author, 'paypal_email', true );
	$cih = get_user_meta( $post->post_author, 'cp_accept_cih_payment', true );
	$cod = get_user_meta( $post->post_author, 'cp_accept_cod_payment', true );
	$bankt = get_user_meta( $post->post_author, 'cp_accept_bank_transfer_payment', true );
	if( function_exists('aws_cp_settings_transactions') )
	$credits = get_user_meta( $post->post_author, 'cp_accept_credit_payment', true );
	else $credits = false;

	$bank_transfer = false;
	if( get_option('cp_auction_plugin_pay_admin') == "yes" && get_option('cp_auction_plugin_pay_user') == "yes" ) {
	$pay = "both";
	} elseif( get_option('cp_auction_plugin_pay_admin') == "yes" && get_option('cp_auction_plugin_pay_user') == "no" ) {
	$pay = "admin";
	} else {
	$pay = "user";
	}
	$users_choice = get_user_meta( $post->post_author, 'cp_use_safe_payment', true );
	if( empty( $users_choice ) ) $users_choice = "yes";

	if( $pay == "both" && $users_choice == "yes" ) {
	$bt = "yes";
	} else if( $pay == "admin" ) {
	$bt = "yes";
	} else {
	$bt = false;
	}

	$search = get_option('cp_auction_enable_search_shopping');
	$cp_buy_now_enabled = get_post_meta($post->ID, 'cp_buy_now_enabled', true);
	$cp_buy_now = get_post_meta($post->ID, 'cp_buy_now', true);
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if( empty( $cp_buy_now ) && ( empty( $my_type ) || $my_type == "classified" ) )
	$cp_buy_now = get_post_meta($post->ID, 'cp_price', true);
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	if ( $post )
	$imp = get_post_meta( $post->ID, 'a2pItemID', true );
	else $imp = false;

	$wantto = get_post_meta( $post->ID, 'cp_want_to', true );
	$want_to = get_localized_field_values('cp_want_to', $wantto);
	if( empty( $wantto ) ) {
	$wantto = get_post_meta( $post->ID, 'cp_iwant_to', true );
	$want_to = get_localized_field_values('cp_iwant_to', $wantto);
	}
	if( get_option('cp_auction_plugin_esc_bank_transfer') == "yes" && $bt == "yes" && get_option('cp_auction_plugin_pay_admin') == "yes" ) $bt_escrow = true;
	if( get_option('cp_auction_plugin_cih') == "yes" && $cih == "yes" ) $cih = true;
	if( get_option('cp_auction_plugin_cod') == "yes" && $cod == "yes" ) $cod = true;
	if( get_option('cp_auction_plugin_bank_transfer') == "yes" && $bankt == "yes" ) $bankt = true;

	if( ( $ad_enabled == "yes" && $cp_buy_now_enabled == true ) && ( ( $cp_buy_now > '0' && $closed != "yes" && $imp == false ) && ( empty ( $want_to ) || $want_to == "Sell" ) 
	&& ( $paypal == true || $credits == true || $bt_escrow == true || $cih == true || $cod == true || $bankt == true ) ) )
	return cp_auction_buy_now_action($post, 'short');
	else return false;

}
add_shortcode('buy-now-button', 'cp_auction_buy_now_button_shortcode');


/*
|--------------------------------------------------------------------------
| ADD THE BUY NOW BUTTON ON CLASSIFIED & AUCTION ADS
|--------------------------------------------------------------------------
*/

function cp_auction_buy_now_code( $post ) {

	global $post;

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );
	$closed = get_post_meta( $post->ID, 'cp_ad_sold', true );
	if( $post->post_status == "publish" && $closed != "yes" ) $display = true;
	else $display = false;

if((( get_option('cp_auction_enable_auction_buy_widget') == "yes" && get_option('cp_auction_enable_bid_widget') == "yes" && $display == true ) && ( $my_type == "normal" )) || (( get_option( 'aws_bo_enable_buy_widget' ) == "yes" && get_option('aws_bo_use_widget') == "yes" && $bestoffer == true && $display == true ) && ( empty( $my_type ) || $my_type == 'classified' ))) return false;

?>

	<div class="clr"></div>
	<?php if( function_exists('cp_auction_buy_now_button_standard') ) echo cp_auction_buy_now_button_standard( $post->ID ); ?>
	<div class="clr"></div>

<?php
}
global $post;
if ( $post )
$imported = get_post_meta( $post->ID, 'a2pItemID', true );
else $imported = false;
if( get_option('cp_auction_use_buy_hooks') == "yes" && $imported == false )
add_action( 'cp_auction_buy_now_button', 'cp_auction_buy_now_code' );
elseif( get_option('cp_auction_use_buy_hooks') != "yes" && $imported == false )
add_action( 'appthemes_after_post_content', 'cp_auction_buy_now_code', 2 );


/*
|--------------------------------------------------------------------------
| BUY NOW BUTTON ACTION FUNCTION
|--------------------------------------------------------------------------
*/

function cp_auction_buy_now_action( $post, $short = '' ) {

ob_start();

	global $current_user, $post, $cp_options, $cpurl;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$payer = get_userdata($uid);
	$ip_address = cp_auction_users_ip();
	$cc = get_user_meta( $post->post_author, 'currency', true );
	if( empty($cc) ) $cc = $cp_options->currency_code;
	$adaptive = get_option('cp_auction_plugin_actfee');
	$float = get_option('cp_auction_buynow_button_float');
	if( $short == "short" )
	$float = get_option('cp_auction_buynow_button_float_short');
	$enabl = get_option( 'cp_auction_enable_conversion' );
	$cuto = get_option( 'cp_auction_convert_to' );

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$cp_buy_now = get_post_meta( $post->ID, 'cp_buy_now', true );
	if( empty( $cp_buy_now ) ) $cp_buy_now = get_post_meta( $post->ID, 'cp_price', true );

	$country = get_user_meta( $uid, 'cp_country', true );
	if ( empty( $country ) && get_option('cp_auction_enable_geoip') == "yes" )
	$country = do_shortcode( '[geoip_detect2 property="country"]' );

	$not = false;
	$cp_shipping = false;
	$cp_shipping_options = get_post_meta( $post->ID, 'cp_shipping_options', true );
	$shipping_options = get_localized_field_values( 'cp_shipping_options', $cp_shipping_options );
	if ( $shipping_options == "" ) $not = __( 'This product can not be shipped!', 'auctionPlugin' );
	else if ( $shipping_options == "Free delivery" ) $cp_shipping = false;
	else $cp_shipping = cp_auction_add_shipping( $post->ID, $post->post_author, $uid, $country, 'return' );

	$free_shipping = get_user_meta( $post->post_author, 'cp_auction_free_shipping', true );
	if ( ( $free_shipping > '0' ) && ( $cp_buy_now >= $free_shipping ) ) $cp_shipping = false;

	$total = $cp_buy_now + $cp_shipping;
	$confirm = get_option('cp_auction_confirmation_popup');
	$receiver_email = cp_auction_get_paypalemail( $uid, $post->post_author );
	$checkout = "".cp_auction_url($cpurl['cart'], '?shopping_cart=1')."";
	$howmany = get_post_meta($post->ID, 'cp_auction_howmany', true);
	$howmany_added = get_post_meta($post->ID, 'cp_auction_howmany_added', true);

	$boffwidget = get_option('aws_bo_use_widget');
	$buyoffer = get_option('aws_bo_enable_buy_widget');
	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );

	if( $bestoffer == true && $boffwidget == "yes" && $buyoffer == "yes" ) $boffer = true;
	else $boffer = false;

?>

	<div class="clear5"></div>

<script type="text/javascript" language="javascript">
// <![CDATA[
function showHide<?php echo $post->ID; ?>() {
    var ele = document.getElementById("showHideDiv<?php echo $post->ID; ?>");
    if(ele.style.display == "block") {
            ele.style.display = "none";
      }
    else {
        ele.style.display = "block";
    }
    var but = document.getElementById("showHideBut<?php echo $post->ID; ?>");
    if(but.style.display == "none") {
            but.style.display = "block";
      }
    else {
        but.style.display = "none";
    }
}
// ]]>
</script>

<div id="showHideDiv<?php echo $post->ID; ?>" style="display:none;">

<div class="cart_wrap">

<?php if ( is_search() || in_the_loop() ) { ?>
<form method="post" action="<?php echo get_permalink($post->ID); ?>?cart=true">
<?php } else { ?>
<form method="post" action="?cart=true">
<?php } ?>

<?php if((( is_search() ) || ( get_option('cp_auction_enable_auction_buy_widget') == "yes" && get_option('cp_auction_enable_bid_widget') == "yes" ) && ( $my_type == "normal" )) || ( $boffer == true )) { ?>

	<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Coupon', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:60px"><div style="text-align: center;"><?php _e('Quantity', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone" style="width:80px"><div style="text-align: center;"><?php _e('Price', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

	<tr>
	<td class="text-left footable-first-column"><input class="widget-cart-coupon" type="text" name="cp_coupon_code" value="" /></td>
	<?php if( $howmany < 2 && $howmany_added != "unlimited" ) { ?>
	<td class="text-center">1<input type="hidden" name="quantity" value="1"></td>
	<?php } else { ?>
	<td class="text-center"><input class="widget-cart-quantity" type="text" name="quantity" value="1" /></td>
	<?php } ?>
	<td class="text-right footable-last-column"><strong><?php echo cp_display_price( $total, 'ad', false ); ?></strong></td>
	</tbody>
</table>

<?php } else { ?>

	<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column"><div style="text-align: left;">&nbsp; <?php _e('Product', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone"><div style="text-align: left;"><?php _e('Coupon', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone"><div style="text-align: center;"><?php _e('Quantity', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone"><div style="text-align: center;"><?php _e('Price', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

	<tr>
	<td class="text-left footable-first-column"><a href="<?php echo get_permalink($post->ID); ?>"><strong><?php echo $post->post_title; ?></strong></a></td>
	<td class="text-left"><input class="cart-coupon" type="text" name="cp_coupon_code" value="" /></td>
	<?php if( $howmany < 2 && $howmany_added != "unlimited" ) { ?>
	<td class="text-center">1<input type="hidden" name="quantity" value="1"></td>
	<?php } else { ?>
	<td class="text-right"><input class="cart-quantity" type="text" name="quantity" value="1" /></td>
	<?php } ?>
	<td class="text-right footable-last-column"><strong><?php echo cp_display_price( $total, 'ad', false ); ?></strong></td>
	</tbody>
</table>

<?php } ?>

	<div class="clear5"></div>

	<div style="text-align: right;">
	<input type="hidden" name="pid" value="<?php echo $post->ID; ?>">
	<input type="button" value="<?php _e('Cancel', 'auctionPlugin'); ?>" onclick="return showHide<?php echo $post->ID; ?>();" />&nbsp;&nbsp;&nbsp;<input type=button onClick="window.open('<?php echo get_permalink($post->ID); ?>?cart=true','_self')" value="<?php _e('View Cart', 'auctionPlugin'); ?>">&nbsp;&nbsp;&nbsp;<?php if ( get_option( 'cp_auction_deactivate_confirmation_popup' ) == "yes" || get_option( 'cp_auction_deactivate_confirmation_popups' ) == "yes" ) { ?><input type="submit" name="short_cp_buy_now" value="<?php _e('Add to Cart', 'auctionPlugin'); ?>" class="cbtn btn_orange cp-width" /><?php } else { ?><input type="submit" name="short_cp_buy_now" onclick="return confirm('<?php echo $confirm; ?>')" value="<?php _e('Add to Cart', 'auctionPlugin'); ?>" class="cbtn btn_orange cp-width" /><?php } ?>
	</form>
		</div>

	</div>
</div>

<?php if( ( get_option('cp_auction_enable_paypal_direct') == "yes" && empty( $adaptive ) ) && ( empty( $my_type ) || $my_type == "classified" ) ) { ?>

	<form class="payform" action="<?php echo get_bloginfo('wpurl'); ?>/?_process_payment=1" method="post" enctype="multipart/form-data">
<?php

	$author = get_userdata($post->post_author);
	$receiver_email = get_user_meta( $post->post_author, 'paypal_email', true );
	if( empty($receiver_email) ) $receiver_email = $author->user_email;

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $payer->user_email;

	if ( get_option('cp_auction_plugin_admins_only') == "yes" ) {
	$fees = get_option('cp_auction_paypal_fee');
	$mc_gross = cp_auction_calculate_fees($total, 'paypal', $fees);
	if( $mc_gross > $total ) $mc_fee = $mc_gross - $total;
	else $mc_fee = $total - $mc_gross;
	}
	else {
	$fees = get_user_meta( $post->post_author, 'cp_auction_paypal_fees', true );
	$mc_gross = cp_auction_calculate_fees($total, 'paypal', $fees, 'author');
	if( $mc_gross > $total ) $mc_fee = $mc_gross - $total;
	else $mc_fee = $total - $mc_gross;
	}
	global $wpdb;
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$order = $wpdb->get_var( "SELECT order_id FROM {$table_purchases} WHERE uid = '$post->post_author' AND buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY order_id DESC" );
	if ( $order < 1 )
	$order = $wpdb->get_var( "SELECT order_id FROM {$table_purchases} ORDER BY order_id DESC" );
	else $order = $order - 1;
	$order_id = $order + 1;

	$conversion = false;
	if ( $enabl == "yes" && class_exists("Currencyr") )
	$conversion = currencyr_exchange( $mc_gross, strtolower( $cuto ) );

	$args = array(
	'order_id' => $order_id,
	'post_id' => $post->ID,
	'post_ids' => $post->ID,
	'uid' => $uid,
	'post_author' => $post->post_author,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'item_name' => $post->post_title,
	'trans_type' => 'Direct by PayPal',
	'mc_currency' => $cc,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'receiver_email' => $receiver_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'quantity' => 1,
	'mc_fee' => cp_auction_sanitize_amount($mc_fee),
	'conversion' => cp_auction_sanitize_amount($conversion),
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'net_total' => cp_auction_sanitize_amount($cp_buy_now),
	'cp_shipping' => cp_auction_sanitize_amount($cp_shipping),
	'howto_pay' => 'paypal',
	'type' => 'direct_purchase'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $args ) ).'">';
?>

	<div class="clear15"></div>

<?php if ( is_user_logged_in() ) { ?>
	<?php if( get_option('cp_auction_plugin_button') == 1 ) { ?>
	<?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><?php if ( get_option( 'cp_auction_deactivate_confirmation_popup' ) == "yes" || get_option( 'cp_auction_deactivate_confirmation_popups' ) == "yes" ) { ?><input type="image" src="http://images.paypal.com/images/x-click-but1.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"><?php } else { ?><input type="image" onclick="return confirm('<?php echo $confirm; ?>')" src="http://images.paypal.com/images/x-click-but1.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"><?php } if( $cp_shipping > 0 ) { ?><br /><div class="small-text6"><?php _e('Incl.', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_shipping, 'ad', false ); ?> <?php _e('est. shipping', 'auctionPlugin'); ?> &nbsp; </div><?php } else { ?><br /><div class="small-text6"><?php echo $not; ?> &nbsp; </div><?php } ?></div>
	<?php } elseif( get_option('cp_auction_plugin_button') == 2 ) { ?>
	<?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><?php if ( get_option( 'cp_auction_deactivate_confirmation_popup' ) == "yes" || get_option( 'cp_auction_deactivate_confirmation_popups' ) == "yes" ) { ?><input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"><?php } else { ?><input type="image" onclick="return confirm('<?php echo $confirm; ?>')" src="https://www.paypal.com/en_GB/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"><?php } if( $cp_shipping > 0 ) { ?><br /><div class="small-text6"><?php _e('Incl.', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_shipping, 'ad', false ); ?> <?php _e('est. shipping', 'auctionPlugin'); ?> &nbsp; </div><?php } else { ?><br /><div class="small-text6"><?php echo $not; ?> &nbsp; </div><?php } ?></div>
	<?php } else { ?>
	<?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><?php if ( get_option( 'cp_auction_deactivate_confirmation_popup' ) == "yes" || get_option( 'cp_auction_deactivate_confirmation_popups' ) == "yes" ) { ?><input type="submit" name="submit" value="<?php echo cp_display_price( $total, 'ad', false ); ?> - <?php _e('Pay with PayPal', 'auctionPlugin'); ?>" class="btn_orange cp-width" /><?php } else { ?><input type="submit" name="submit" onclick="return confirm('<?php echo $confirm; ?>')" value="<?php echo cp_display_price( $total, 'ad', false ); ?> - <?php _e('Pay with PayPal', 'auctionPlugin'); ?>" class="btn_orange cp-width" /><?php } if( $cp_shipping > 0 ) { ?><br /><div class="small-text6"><?php _e('Incl.', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_shipping, 'ad', false ); ?> <?php _e('est. shipping', 'auctionPlugin'); ?> &nbsp; </div><?php } else { ?><br /><div class="small-text6"><?php echo $not; ?> &nbsp; </div><?php } ?></div>
	<?php } ?>
<?php } else { ?>
	<?php if( get_option('cp_auction_plugin_button') == 1 ) { ?>
	<?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><a href="<?php echo wp_login_url( get_permalink() ); ?>"><img src="http://images.paypal.com/images/x-click-but1.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"></a><?php if( $cp_shipping > 0 ) { ?><br /><div class="small-text6"><?php _e('Incl.', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_shipping, 'ad', false ); ?> <?php _e('est. shipping', 'auctionPlugin'); ?> &nbsp; </div><?php } else { ?><br /><div class="small-text6"><?php echo $not; ?> &nbsp; </div><?php } ?></div>
	<?php } elseif( get_option('cp_auction_plugin_button') == 2 ) { ?>
	<?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><a href="<?php echo wp_login_url( get_permalink() ); ?>"><img src="https://www.paypal.com/en_GB/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"></a><?php if( $cp_shipping > 0 ) { ?><br /><div class="small-text6"><?php _e('Incl.', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_shipping, 'ad', false ); ?> <?php _e('est. shipping', 'auctionPlugin'); ?> &nbsp; </div><?php } else { ?><br /><div class="small-text6"><?php echo $not; ?> &nbsp; </div><?php } ?></div>
	<?php } else { ?>
	<?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><a href="<?php echo wp_login_url( get_permalink() ); ?>" class="mbtn btn_orange cp-width" tabindex="2"><?php echo cp_display_price( $total, 'ad', false ); ?> - <?php _e('Pay with PayPal', 'auctionPlugin'); ?></a><?php if( $cp_shipping > 0 ) { ?><br /><div class="small-text6"><?php _e('Incl.', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_shipping, 'ad', false ); ?> <?php _e('est. shipping', 'auctionPlugin'); ?> &nbsp; </div><?php } else { ?><br /><div class="small-text6"><?php echo $not; ?> &nbsp; </div><?php } ?></div>
	<?php } ?>
<?php } ?>
	</form>

<?php } else {
	$button_text = get_option( 'cp_auction_button_text' );
	if( empty( $button_text ) ) $button_text = ''.__('Buy Now! for', 'auctionPlugin').' '.cp_display_price( $total, 'ad', false ).'';
?><!-- /paypal_direct -->

	<div class="clr"></div>

<?php if ( is_user_logged_in() ) { ?>
	<div id="showHideBut<?php echo $post->ID; ?>">
		<form method="post" action="">
                <?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><input type="button" class="mbtn btn_orange cp-width" value="<?php echo $button_text; ?>" onclick="return showHide<?php echo $post->ID; ?>();"><?php if( $cp_shipping > 0 ) { ?><br /><div class="small-text6"><?php _e('Incl.', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_shipping, 'ad', false ); ?> <?php _e('est. shipping', 'auctionPlugin'); ?> &nbsp; </div><?php } else { ?><br /><div class="small-text6"><?php echo $not; ?> &nbsp; </div><?php } ?></div>
                </form>
	</div>
<?php } else { ?>
	<?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><a href="<?php echo wp_login_url( get_permalink() ); ?>" class="mbtn btn_orange cp-width" tabindex="2"><?php echo $button_text; ?></a><?php if( $cp_shipping > 0 ) { ?><br /><div class="small-text6"><?php _e('Incl.', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_shipping, 'ad', false ); ?> <?php _e('est. shipping', 'auctionPlugin'); ?> &nbsp; </div><?php } else { ?><br /><div class="small-text6"><?php echo $not; ?> &nbsp; </div><?php } ?></div>
<?php } ?>

<?php } ?>

<div class="clear15"></div>

<?php
	return ob_get_clean();
}