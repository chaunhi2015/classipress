<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| ADD THE CORRECT DASHBOARD TO THE CP AUCTION USERPANEL PAGE
|--------------------------------------------------------------------------
*/

function cp_auction_userpanel_selector_code( $query ) {

	global $current_user, $post, $cp_options, $cpurl, $cp_block, $i;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$ct = get_option('stylesheet');

	if ( !is_user_logged_in() )
		return;

while( $query->have_posts() ) : $query->the_post(); $i++; ?>

<?php
$expire_time = strtotime( get_post_meta($post->ID, 'cp_sys_expire_date', true) );
$expire_date = appthemes_display_date( $expire_time );
$ad_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
if ( empty( $ad_type ) ) $ad_type = "classified";
$total_cost = get_post_meta($post->ID, 'cp_sys_total_ad_cost', true);

if( empty( $ad_type ) ) $DASHBOARD_URL = CP_DASHBOARD_URL;
else $DASHBOARD_URL = CP_AUCTION_MYADS;

if (get_post_meta($post->ID, 'cp_total_count', true))
$ad_views = number_format(get_post_meta($post->ID, 'cp_total_count', true));
else $ad_views = '-';

if ( get_option('cp_version') > '3.3.3' ) {
$status = cp_get_listing_status_name( $post->ID );
}

$user = get_userdata($uid);
$ip_address = cp_auction_users_ip();
$cc = $cp_options->currency_code;
$bump_price = get_option('cp_auction_bump_ad_price');
$upg_price = get_option('cp_auction_upgrade_ad_price');
$pp = get_option('cp_auction_plugin_paypalemail');
if( empty( $pp ) && isset( $cp_options->gateways['paypal'] ) ) $pp = $cp_options->gateways['paypal']['email_address']; // Use classipress email.

$payer_email = get_user_meta( $uid, 'paypal_email', true );
if( empty($payer_email) ) $payer_email = $user->user_email;

// now let's figure out what the ad status and options should be
// it's a live and published ad
if ( $post->post_status == 'publish' ) {

$post_status = 'live';
$post_status_name = '<span class="status">'.__( 'Live', 'auctionPlugin' ) . '</span><p class="small">(' . $expire_date . ')</p>';
$fontcolor = '#666666';
$postimage = 'pause.png';
$postalt =  __( 'Pause Ad', 'auctionPlugin' );
$postaction = 'pause';

// it's a pending ad which gives us several possibilities
} elseif ( $post->post_status == 'pending' ) {

if ( cp_have_pending_payment( $post->ID ) ) {
$post_status = 'pending_payment';
$post_status_name = __( 'Awaiting payment', 'auctionPlugin' );
$fontcolor = '#b22222';
$postimage = '';
$postalt = '';
$postaction = 'pending';
} else {
$post_status = 'pending';
$post_status_name = __( 'Awaiting approval', 'auctionPlugin' );
$fontcolor = '#b22222';
$postimage = '';
$postalt = '';
$postaction = 'pending';
}

} elseif ( $post->post_status == 'draft' ) {

// current date is past the expires date so mark ad ended
if ( current_time('timestamp') > $expire_time ) {
$post_status = 'ended';
$post_status_name = __( 'Ended', 'auctionPlugin' ) . '<br /><p class="small">(' . $expire_date . ')</p>';
$fontcolor = '#666666';
$postimage = '';
$postalt = '';
$postaction = 'ended';

// ad has been paused by ad owner
} else {
$post_status = 'offline';
$post_status_name = __( 'Offline', 'auctionPlugin' );
$fontcolor = '#bbbbbb';
$postimage = 'start-blue.png';
$postalt = __( 'Restart ad', 'auctionPlugin' );
$postaction = 'restart';
}

} elseif ( $post->post_status == 'private' ) {
$post_status = 'private';
$post_status_name = __( 'Private', 'auctionPlugin' );
$fontcolor = '#bbbbbb';
$postimage = 'start-blue.png';
$postalt = __( 'Restart ad', 'auctionPlugin' );
$postaction = 'restart';
}
else {
$post_status = '&mdash;';
}
?>


<tr class="even <?php echo $status; ?>">
<td class="text-right no-th"><?php echo $i; ?>.</td>

<td>
<h3>
<?php if ( $post_status == 'live' ) { ?>

<div class="showadthumb-left">
	<?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?>
</div>

<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

<?php } else { ?>

<div class="showadthumb-left">
	<?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?>
</div>

<?php the_title(); ?>

<?php } ?>
</h3>
<?php if( $ct == "simply-responsive-cp" ) { ?>
<div class="metad"><span class="folder"> &nbsp; <?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="clockb"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
<?php } elseif( $ct == "flatpress" ) { ?>
<div class="meta"><span class="folder"><?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="clock"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
<?php } else { ?>
<div class="meta"><span class="dashicons-before folder"><?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="dashicons-before clock"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
<?php } ?>
</td>

<td class="text-center views"><?php echo $ad_views; ?></td>

<?php if( ( get_option( 'cp_auction_bump_ad_enabled' ) == "yes" || get_option( 'cp_auction_upgrade_ad_enabled' ) == "yes" ) && ( get_option( 'cp_auction_bump_ad_link' ) == "table" ) ) { ?>
<td class="text-center bump">
<?php

if ( get_option( 'cp_auction_bump_ad_enabled' ) == "yes" ) {
echo '<form id="bump'.$post->ID.'" action="'.get_bloginfo('wpurl').'/?_process_payment=1" method="post" id="mainform" enctype="multipart/form-data">';
$bump_arg = cp_auction_bump_ads($uid, $post->ID, $ip_address, $payer_email, $pp, $bump_price);
echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $bump_arg ) ).'">';
echo '<a title="'.__( 'Bumping up your Ad will reset the post date of your Ad to the time of the bump, thus moving your Ad from its current position in the listings back up to the top of the first page.', 'auctionPlugin' ).'" href="javascript:{}" onclick="document.getElementById(\'bump'.$post->ID.'\').submit(); return false;">'.__('Bump Ad', 'auctionPlugin').'</a><br />';
echo '</form>';
}

if ( ! is_sticky($post->ID) && get_option( 'cp_auction_upgrade_ad_enabled' ) == "yes" ) {
echo '<form id="upgrade'.$post->ID.'" action="'.get_bloginfo('wpurl').'/?_process_payment=1" method="post" id="mainform" enctype="multipart/form-data">';
$upgrade_arg = cp_auction_upgrade_ads($uid, $post->ID, $ip_address, $payer_email, $pp, $upg_price);
echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $upgrade_arg ) ).'">';
echo '<a title="'.__( 'Featured Ads are randomly selected to appear on rotation at the top in the main category they’ve been posted in.', 'auctionPlugin' ).'" href="javascript:{}" onclick="document.getElementById(\'upgrade'.$post->ID.'\').submit(); return false;">'.__('Featured', 'auctionPlugin').'</a>';
echo '</form>';
}

?>

</td>
<?php } ?>

<td class="text-center status"><span style="color:<?php echo $fontcolor; ?>;"><?php echo $post_status_name; ?></span></td>

<td class="text-center">
<?php
if ( $post_status == 'pending' ) {

echo '&mdash;';

} elseif ( $post_status == 'pending_payment' ) {

$order_url = cp_get_order_permalink( $post->ID );
echo html( 'a', array( 'href' => $order_url ), __( 'Pay now', 'auctionPlugin' ) );

} elseif ( $post_status == 'ended' ) {

if ( $cp_options->allow_relist ) {
// relisting url
if ( get_option('cp_version') < '3.4' )
$relist_url = esc_url( add_query_arg( array( 'renew' => $post->ID, 'type' => $ad_type ), CP_ADD_NEW_URL ) );
else $relist_url = esc_url( add_query_arg( array( 'listing_renew' => $post->ID, 'type' => $ad_type ), CP_ADD_NEW_URL ) );
echo html( 'a', array( 'href' => $relist_url, 'title' => __( 'Relist Ad', 'auctionPlugin' ) ), __( 'Relist Ad', 'auctionPlugin' ) );
} else {
echo '&mdash;';
}

} else {
if ( get_option( 'cp_version' ) > '3.4.1' ) {
	cp_auction_dashboard_listing_actions( $post->ID, $ad_type );
} else {
echo '<span class="iconfix">';
if ( $cp_options->ad_edit ) {
if ( get_option('cp_version') < '3.4' )
$edit_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'type' => $ad_type ), CP_EDIT_URL ) );
else $edit_url = esc_url( add_query_arg( array( 'listing_edit' => $post->ID, 'type' => $ad_type ), CP_EDIT_URL ) );
if( $ct == "simply-responsive-cp" )
$edit_img = html( 'img', array( 'src' => get_stylesheet_directory_uri() . '/images/pencil.png', 'border' => '0', 'title' => __( 'Edit Ad', 'auctionPlugin' ), 'alt' => __( 'Edit Ad', 'auctionPlugin' ) ) );
elseif( $ct == "flatpress" )
//$edit_img = html( 'i', array( 'class' => 'editOptions icon-pencil', 'title' => __( 'Edit Ad', 'auctionPlugin' ), 'alt' => __( 'Edit Ad', 'auctionPlugin' ) ) );
$edit_img = html( 'img', array( 'src' => cp_auction_plugin_url() . '/css/images/pencil.png', 'border' => '0', 'title' => __( 'Edit Ad', 'auctionPlugin' ), 'alt' => __( 'Edit Ad', 'auctionPlugin' ) ) );
else
$edit_img = html( 'img', array( 'src' => get_template_directory_uri() . '/images/pencil.png', 'border' => '0', 'title' => __( 'Edit Ad', 'auctionPlugin' ), 'alt' => __( 'Edit Ad', 'auctionPlugin' ) ) );
echo html( 'a', array( 'href' => $edit_url, 'title' => __( 'Edit Ad', 'auctionPlugin' ) ), $edit_img ) . ' ';
}

$delete_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'action' => 'delete' ), $DASHBOARD_URL ) );
if( $ct == "simply-responsive-cp" )
$delete_img = html( 'img', array( 'src' => get_stylesheet_directory_uri() . '/images/cross.png', 'border' => '0', 'title' => __( 'Delete Ad', 'auctionPlugin' ), 'alt' => __( 'Delete Ad', 'auctionPlugin' ) ) );
elseif( $ct == "flatpress" )
//$delete_img = html( 'i', array( 'class' => 'editOptions icon-remove', 'title' => __( 'Delete Ad', 'auctionPlugin' ), 'alt' => __( 'Delete Ad', 'auctionPlugin' ) ) );
$delete_img = html( 'img', array( 'src' => cp_auction_plugin_url() . '/css/images/cross1.png', 'border' => '0', 'title' => __( 'Delete Ad', 'auctionPlugin' ), 'alt' => __( 'Delete Ad', 'auctionPlugin' ) ) );
else
$delete_img = html( 'img', array( 'src' => get_template_directory_uri() . '/images/cross.png', 'border' => '0', 'title' => __( 'Delete Ad', 'auctionPlugin' ), 'alt' => __( 'Delete Ad', 'auctionPlugin' ) ) );
echo html( 'a', array( 'href' => $delete_url, 'onclick' => 'return confirmBeforeDeleteAd();', 'title' => __( 'Delete Ad', 'auctionPlugin' ) ), $delete_img ) . ' ';

$postaction_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'action' => $postaction ), $DASHBOARD_URL ) );
if( $ct == "simply-responsive-cp" )
$postaction_img = html( 'img', array( 'src' => get_stylesheet_directory_uri() . '/images/' . $postimage, 'border' => '0', 'title' => $postalt, 'alt' => $postalt ) );
elseif( $ct == "flatpress" )
//$postaction_img = html( 'i', array( 'class' => 'editOptions icon-pause', 'title' => $postalt, 'alt' => $postalt ) );
$postaction_img = html( 'img', array( 'src' => cp_auction_plugin_url() . '/css/images/' . $postimage, 'border' => '0', 'title' => $postalt, 'alt' => $postalt ) );
else
$postaction_img = html( 'img', array( 'src' => get_template_directory_uri() . '/images/' . $postimage, 'border' => '0', 'title' => $postalt, 'alt' => $postalt ) );

echo html( 'a', array( 'href' => $postaction_url, 'title' => $postalt ), $postaction_img ) . '</span>';
}
if ( get_option( 'cp_auction_bump_ad_enabled' ) == "yes" && get_option( 'cp_auction_bump_ad_link' ) == "block" ) {
echo '<form id="bump'.$post->ID.'" action="'.get_bloginfo('wpurl').'/?_process_payment=1" method="post" id="mainform" enctype="multipart/form-data">';
$bump_arg = cp_auction_bump_ads($uid, $post->ID, $ip_address, $payer_email, $pp, $bump_price);
echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $bump_arg ) ).'">';
echo '<a title="'.__( 'Bumping up your Ad will reset the post date of your Ad to the time of the bump, thus moving your Ad from its current position in the listings back up to the top of the first page.', 'auctionPlugin' ).'" href="javascript:{}" onclick="document.getElementById(\'bump'.$post->ID.'\').submit(); return false;">'.__('Bump Ad', 'auctionPlugin').'</a></form>';
}

if ( ! is_sticky($post->ID) && get_option( 'cp_auction_upgrade_ad_enabled' ) == "yes" && get_option( 'cp_auction_bump_ad_link' ) == "block" ) {
echo '<form id="upgrade'.$post->ID.'" action="'.get_bloginfo('wpurl').'/?_process_payment=1" method="post" id="mainform" enctype="multipart/form-data">';
$upgrade_arg = cp_auction_upgrade_ads($uid, $post->ID, $ip_address, $payer_email, $pp, $upg_price);
echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $upgrade_arg ) ).'">';
echo '<a title="'.__( 'Featured Ads are randomly selected to appear on rotation at the top in the main category they’ve been posted in.', 'auctionPlugin' ).'" href="javascript:{}" onclick="document.getElementById(\'upgrade'.$post->ID.'\').submit(); return false;">'.__('Featured', 'auctionPlugin').'</a></form>';
}

if ( get_post_meta($post->ID, 'cp_ad_sold', true) != 'yes' && get_post_meta($post->ID, 'cp_ad_pick', true) != 'yes' && $post_status != 'private' ) {
$setSold_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'action' => 'setSold' ), $DASHBOARD_URL ) );
echo '<div class="clr"></div>' . html( 'a', array( 'href' => $setSold_url, 'title' => __( 'Mark Sold', 'auctionPlugin' ) ), __( 'Mark Sold', 'auctionPlugin' ) );
} else {
if ( $post_status != 'private' && get_post_meta($post->ID, 'cp_ad_sold', true) == 'yes' ) {
$unsetSold_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'action' => 'unsetSold' ), $DASHBOARD_URL ) );
echo '<div class="clr"></div>' . html( 'a', array( 'href' => $unsetSold_url, 'title' => __( 'Unmark Sold', 'auctionPlugin' ) ), __( 'Unmark Sold', 'auctionPlugin' ) );
}
}

if ( get_option('cp_auction_allow_pause_ad') == "yes" && !in_array( $post_status, array( 'pending', 'private', 'pending_payment', 'ended' ) ) ) {
$setPrivate_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'action' => 'setPrivate' ), $DASHBOARD_URL ) );
echo '<div class="clr"></div>' . html( 'a', array( 'href' => $setPrivate_url, 'title' => __( 'Mark Private', 'auctionPlugin' ) ), __( 'Mark Private', 'auctionPlugin' ) );
}

if( $ct == "simply-responsive-cp" || metadata_exists( 'post', $post->ID, 'cp_ad_pick' ) ) {
if ( get_post_meta($post->ID, 'cp_ad_pick', true) != 'yes' && get_post_meta($post->ID, 'cp_ad_sold', true) != 'yes' && $post_status != 'private' ) {
$setPick_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'action' => 'setPick' ), $DASHBOARD_URL ) );
echo '<div class="clr"></div>' . html( 'a', array( 'href' => $setPick_url, 'title' => __( 'Mark Pending', 'auctionPlugin' ) ), __( 'Mark Pending', 'auctionPlugin' ) );
} else {
if ( $post_status != 'private' && get_post_meta($post->ID, 'cp_ad_pick', true) == 'yes' ) {
$unsetPick_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'action' => 'unsetPick' ), $DASHBOARD_URL ) );
echo '<div class="clr"></div>' . html( 'a', array( 'href' => $unsetPick_url, 'title' => __( 'Unmark Pending', 'auctionPlugin' ) ), __( 'Unmark Pending', 'auctionPlugin' ) );
}
}
}
}

if ( in_array( $post_status, array( 'pending', 'pending_payment', 'ended' ) ) ) {
$delete_url = esc_url( add_query_arg( array( 'aid' => $post->ID, 'action' => 'delete' ), $DASHBOARD_URL ) );
echo html( 'a', array( 'href' => $delete_url, 'title' => __( 'Delete Ad', 'auctionPlugin' ), 'onclick' => 'return confirmBeforeDeleteAd();', 'style' => 'display: block;' ), __( 'Delete Ad', 'auctionPlugin' ) );
}
?>
</td>
</tr>
<?php endwhile; ?>

<?php
}
add_action( 'cp_auction_userpanel_selector', 'cp_auction_userpanel_selector_code' );