<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| AD LISTING LOOP - CATEGORIZED
|--------------------------------------------------------------------------
*/

function cp_auction_loop_ad_listing($post) {

	global $post, $cp_options;

	if ( ! $post ) return;
?>

        <div class="post-block-out <?php cp_display_style( 'featured' ); ?>">

            <div class="post-block">
            <?php if( get_option('stylesheet') == 'twinpress_classifieds' ) { ?>
                <?php if (get_post_meta($post->ID, 'cp_ad_sold', true) == 'yes') : ?><div class="pm_corner_sold"></div><?php endif; ?>
                <?php if ( get_post_meta( $post->ID, 'cp_price_negotiable', true ) ) echo '<span class="negotiable-list"></span>'; else echo ''; ?>
                <?php if(is_sticky()) {?><div class="pm_corner_feat"></div> <?php } ?>
                <div class="clr"></div>
                <?php } ?>

                <div class="post-left">

		<?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?>

                </div>

		<?php if( get_option('stylesheet') != 'twinpress_classifieds' ) { ?>
                <div class="<?php cp_display_style( array( 'ad_images', 'ad_class' ) ); ?>">
		<?php } ?>

		<?php appthemes_before_post_title(); ?>
<?php
		$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
		if ( $my_type == 'normal' || $my_type == 'reverse' ) {
		if( get_option('stylesheet') == 'twinpress_classifieds' )
		$ad_type = '</h3><span class="ad-type">'.__('Auction', 'auctionPlugin').'</span>';
		else
		$ad_type = '</h3><span class="ad-type">'.__('Auction', 'auctionPlugin').'</span> <h3>';
		} else {
		$ad_type = "";
		}
		$ct = get_option('stylesheet');
		if( $ct == 'twinpress_classifieds' ) { ?>
		<span class="pm_post_title"><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= 75 ) echo ''.$ad_type.' '.mb_substr( get_the_title(), 0, 75 ).'...'; else echo ''.$ad_type.''; the_title(); ?></a></span>

		<?php } elseif( $ct != "twinpress_classifieds" && $ct != "jibo" && $ct != "simply-responsive-cp" ) { ?>
		<h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= 75 ) echo ''.$ad_type.' '.mb_substr( get_the_title(), 0, 75 ).'...'; else echo ''.$ad_type.''; the_title(); ?></a></h3>

		<?php } else { ?>
		<h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= 75 ) echo mb_substr( get_the_title(), 0, 75 ).'...'; else the_title(); ?></a></h3>
		<?php } ?>

		<?php if( get_option('stylesheet') != 'twinpress_classifieds' ) { ?>
		<div class="clr"></div>
		<?php } ?>

                    <?php appthemes_after_post_title(); ?>

		<?php if( get_option('stylesheet') != 'twinpress_classifieds' ) { ?>
		<div class="clr"></div>
		<?php } ?>

                    <?php cp_auction_countdown_timer(); ?>

			<div class="clr"></div>

                    <p class="post-desc"><?php echo cp_get_content_preview( 160 ); ?></p>

		<?php if( get_option('stylesheet') == 'simply-responsive-cp' ) { ?>
		<span class="clock-ft"><span><?php echo appthemes_date_posted($post->post_date); ?></span></span>
		<?php } ?>

                    <?php appthemes_after_post_content(); ?>
                    
                    <div class="clr"></div>

		<?php if( get_option('stylesheet') != 'twinpress_classifieds' ) { ?>
                </div>
		<?php } ?>

                <div class="clr"></div>
        
            </div><!-- /post-block -->

        </div><!-- /post-block-out -->

<?php
}