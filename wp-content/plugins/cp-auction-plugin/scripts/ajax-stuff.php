<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function admin_bump_ads_code() {

	if ( isset( $_POST["post_var"] ) ) {

	$sid = $_POST["post_var"];
	$ext = get_option('cp_auction_bump_extend_exp');
	$my_type = get_post_meta( $sid, 'cp_auction_my_auction_type', true );

	global $wpdb;
	$table_posts = $wpdb->prefix . "posts";
	$todayDate = current_time('mysql');
	$where_array = array('ID' => $sid);
	$wpdb->update($table_posts, array('post_date' => $todayDate), $where_array);

	if ( ( $ext > 0 ) && ( empty( $my_type ) || $my_type == "classified" || $my_type == "wanted" ) ) {
	$exp_date = get_post_meta( $sid, 'cp_sys_expire_date', true );
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$exp_date + $ext days"), true);
	update_post_meta( $sid, 'cp_sys_expire_date', $cp_sys_expire_date );
	}
	exit;
	}

}
add_action('wp_ajax_admin_bump_ads', 'admin_bump_ads_code');


function update_bumped_status_code() {

	if ( isset( $_POST["post_var"] ) ) {

	$sid = $_POST["post_var"];
	$ext = get_option('cp_auction_bump_extend_exp');

	global $wpdb;
	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE id = '$sid'");
	$my_type = get_post_meta( $row->pid, 'cp_auction_my_auction_type', true );

	$table_posts = $wpdb->prefix . "posts";
	$rows = $wpdb->get_row("SELECT * FROM {$table_posts} WHERE ID = '$row->pid'");

	if( $row->status == "complete" ) {
	$where_array = array('ID' => $row->pid);
	$wpdb->update($table_posts, array('post_date' => $rows->post_date_gmt), $where_array);
	$wheres_array = array('id' => $sid);
	$wpdb->update($table_trans, array('status' => 'pending'), $wheres_array);

	if ( ( $ext > 0 ) && ( empty( $my_type ) || $my_type == "classified" || $my_type == "wanted" ) ) {
	$exp_date = get_post_meta( $row->pid, 'cp_sys_expire_date', true );
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$exp_date - $ext days"), true);
	update_post_meta( $row->pid, 'cp_sys_expire_date', $cp_sys_expire_date );
	}
	} else if( $row->status == "pending" ) {
	$todayDate = current_time('mysql');
	$where_array = array('ID' => $row->pid);
	$wpdb->update($table_posts, array('post_date' => $todayDate), $where_array);
	$wheres_array = array('id' => $sid);
	$wpdb->update($table_trans, array('status' => 'complete'), $wheres_array);

	if ( ( $ext > 0 ) && ( empty( $my_type ) || $my_type == "classified" || $my_type == "wanted" ) ) {
	$exp_date = get_post_meta( $row->pid, 'cp_sys_expire_date', true );
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$exp_date + $ext days"), true);
	update_post_meta( $row->pid, 'cp_sys_expire_date', $cp_sys_expire_date );
	}
	}
	exit;
	}

}
add_action('wp_ajax_update_bumped_status', 'update_bumped_status_code');


function update_featured_status_code() {

	if ( isset( $_POST["post_var"] ) ) {

	$sid = $_POST["post_var"];
	$ext = get_option('cp_auction_upgrade_extend_exp');

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$sid'");
	$my_type = get_post_meta( $row->pid, 'cp_auction_my_auction_type', true );

	if( $row->status == "complete" ) {
	$where_array = array('id' => $sid);
	$wpdb->update($table_name, array('status' => 'pending'), $where_array);
	unstick_post($row->pid);

	if ( ( $ext > 0 ) && ( empty( $my_type ) || $my_type == "classified" || $my_type == "wanted" ) ) {
	$exp_date = get_post_meta( $row->pid, 'cp_sys_expire_date', true );
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$exp_date - $ext days"), true);
	update_post_meta( $row->pid, 'cp_sys_expire_date', $cp_sys_expire_date );
	}
	} else if( $row->status == "pending" ) {
	$where_array = array('id' => $sid);
	$wpdb->update($table_name, array('status' => 'complete'), $where_array);
	stick_post($row->pid);

	if ( $ext > 0 ) {
	$exp_date = get_post_meta( $row->pid, 'cp_sys_expire_date', true );
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$exp_date + $ext days"), true);
	update_post_meta( $row->pid, 'cp_sys_expire_date', $cp_sys_expire_date );
	}
	}
	exit;
	}

}
add_action('wp_ajax_update_featured_status', 'update_featured_status_code');


function bumped_delete_pid_code() {

	if ( isset( $_POST["post_var"] ) ) {

	$sid = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $sid));
	exit;
	}

}
add_action('wp_ajax_bumped_delete_pid', 'bumped_delete_pid_code');


function featured_delete_pid_code() {

	if ( isset( $_POST["post_var"] ) ) {

	$sid = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $sid));
	exit;
	}

}
add_action('wp_ajax_featured_delete_pid', 'featured_delete_pid_code');


function cp_add_to_favorites() {

	$uid = "";
	$sid = "";
	if ( isset( $_POST["post_var"] ) ) {
	$ids = $_POST["post_var"];
	$res = explode("|", $ids);
	$uid = $res[0];
	$sid = $res[1];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_favorites";
	$favorites = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND sid = '$sid'", ARRAY_A);
	
	$res =  $wpdb->num_rows;
	if( $res < 1 ) {
	$table_name = $wpdb->prefix . "cp_auction_plugin_favorites";
	$query = "INSERT INTO {$table_name} (uid, sid) VALUES (%d, %d)"; 
    	$wpdb->query($wpdb->prepare($query, $uid, $sid));

	$result['type'] = "success";
	$result['notices'] = '<div class="notice success"><div>'.__( 'Seller was added to your favourite list!', 'auctionPlugin' ).'</div></div>';
	}
	else {
	$result['type'] = "error";
	$result['notices'] = '<div class="notice error"><div>'.__( 'Sorry, but something went wrong, or the seller is already on your list!', 'auctionPlugin' ).'</div></div>';
	}

	if ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest') {
	$result = json_encode( $result );
	echo $result;
	}
	else {
	header("Location: ".$_SERVER["HTTP_REFERER"]);
	}

	exit;
	}
}
add_action('wp_ajax_cp_add_favorites', 'cp_add_to_favorites');


function cp_user_logged_out() {

	if ( isset( $_POST["post_var"] ) ) {
	$msg = ''.__('You must be logged in to perform this action!', 'auctionPlugin').'';
	echo $msg;
	exit;
	}
}
add_action('wp_ajax_nopriv_cp_add_favorites', 'cp_user_logged_out');
add_action('wp_ajax_nopriv_cp_add_watchlist', 'cp_user_logged_out');


function cp_delete_favorites() {

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_favorites";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));

	$result['type'] = "success";
	$result['notices'] = '<div class="notice success"><div>'.__( 'The favorite was removed from your favourite list!', 'auctionPlugin' ).'</div></div>';

	if ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest') {
	$result = json_encode( $result );
	echo $result;
	}
	else {
	header("Location: ".$_SERVER["HTTP_REFERER"]);
	}

	exit;
	}
}
add_action('wp_ajax_cp_del_favorites', 'cp_delete_favorites');


function cp_add_to_watchlist() {

	$uid = "";
	$aid = "";

	if ( isset( $_POST["post_var"] ) ) {
	$ids = $_POST["post_var"];
	$res = explode("|", $ids);
	$uid = $res[0];
	$aid = $res[1];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$watchlist = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND aid = '$aid'", ARRAY_A);

	$res =  $wpdb->num_rows;
	if( $res < 1 ) {
	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$query = "INSERT INTO {$table_name} (uid, aid) VALUES (%d, %d)"; 
    	$wpdb->query($wpdb->prepare($query, $uid, $aid));

	$result['type'] = "success";
	$result['notices'] = '<div class="notice success"><div>'.__( 'Auction was added to your watchlist!', 'auctionPlugin' ).'</div></div>';
	}
	else {
	$result['type'] = "error";
	$result['notices'] = '<div class="notice error"><div>'.__( 'Sorry, but something went wrong, or the auction is already on your list!', 'auctionPlugin' ).'</div></div>';
	}

	if ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest') {
	$result = json_encode( $result );
	echo $result;
	}
	else {
	header("Location: ".$_SERVER["HTTP_REFERER"]);
	}

	exit;
	}
}
add_action('wp_ajax_cp_add_watchlist', 'cp_add_to_watchlist');


function cp_delete_watchlist() {

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));

	$result['type'] = "success";
	$result['notices'] = '<div class="notice success"><div>'.__( 'Auction was removed from watchlist!', 'auctionPlugin' ).'</div></div>';

	if ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest') {
	$result = json_encode( $result );
	echo $result;
	}
	else {
	header("Location: ".$_SERVER["HTTP_REFERER"]);
	}

	exit;
	}
}
add_action('wp_ajax_cp_del_watchlist', 'cp_delete_watchlist');


function cp_delete_coupon() {

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_coupons";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE coupon_id = '%d'", $id));

	exit;
	}
}
add_action('wp_ajax_cp_del_coupon', 'cp_delete_coupon');


function cp_delete_uploads() {

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'", ARRAY_A);

	$fullpath = ''.$row['fullpath'].''.$row['filename'].'';
	unlink( $fullpath );

	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('buyer' => '0', 'fname' => '', 'filename' => '', 'fullpath' => '', 'download' => '', 'size' => '', 'ip' => '', 'datemade' => '0', 'type' => ''), $where_array);

	$result['type'] = "success";
	$result['notices'] = '<div class="notice success"><div>'.__( 'The package was successfully deleted!', 'auctionPlugin' ).'</div></div>';

	if ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest') {
	$result = json_encode( $result );
	echo $result;
	}
	else {
	header("Location: ".$_SERVER["HTTP_REFERER"]);
	}

	exit;
	}
}
add_action('wp_ajax_cp_del_uploads', 'cp_delete_uploads');


function cp_delete_log() {

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));

	exit;
	}
}
add_action('wp_ajax_cp_del_log', 'cp_delete_log');


function cp_bid_delete_pid() {

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");

	$my_type = get_post_meta( $res->pid, 'cp_auction_my_auction_type', true );
	$last_bid = cp_auction_plugin_get_latest_bid($res->pid);
	$adjust = ($res->bid - $res->last_bid);
	if( $my_type == "normal" && get_option('cp_auction_plugin_price_type') == "current" && $res->bid == $last_bid ) {
		$new_price = (get_post_meta( $res->pid, 'cp_price', true ) - $adjust);
		update_post_meta( $res->pid, 'cp_price', $new_price );
	} elseif( $my_type == "reverse" && get_option('cp_auction_plugin_price_type') == "current" && $res->bid == $last_bid ) {
		$new_price = (get_post_meta( $res->pid, 'cp_price', true ) + $adjust);
		update_post_meta( $res->pid, 'cp_price', $new_price );
	}
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));

	$result['type'] = "success";
	$result['notices'] = '<div class="notice success"><div>'.__( 'The bid was deleted!', 'auctionPlugin' ).'</div></div>';

	if ( !empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest') {
	$result = json_encode( $result );
	echo $result;
	}
	else {
	header("Location: ".$_SERVER["HTTP_REFERER"]);
	}

	exit;
	}
}
add_action('wp_ajax_cp_bid_del_pid', 'cp_bid_delete_pid');


function cp_auction_delete_wanted_offer() {

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];

	global $wpdb, $post;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	$post = get_post($res->pid);

	cp_auction_wanted_offer_was_declined( $res->pid, $post, $res->uid, $res->bid );

	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));

	exit;
	}
}
add_action('wp_ajax_cp_delete_wanted_offer', 'cp_auction_delete_wanted_offer');


function cp_mark_listing_as_paid() {

	if ( isset( $_POST["post_var"] ) ) {

	global $wpdb, $post;

	$id = $_POST["post_var"];

	$res = explode("|", $id);
	$oid = $res[0];
	$pid = $res[1];
	$uid = $res[2];
	$no  = $res[3];

	$post = get_post($pid);
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );
	$last_bid = cp_auction_plugin_get_latest_bid($pid);

	if( empty($my_type) || $my_type == "classified" ) {
	$tm = time();
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$order = $wpdb->get_var( "SELECT order_id FROM {$table_name} WHERE uid = '$post->post_author' AND buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY order_id DESC" );
	if ( $order < 1 )
	$order = $wpdb->get_var( "SELECT order_id FROM {$table_name} ORDER BY order_id DESC" );
	else $order = $order - 1;

	if ( $oid > 0 ) {
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE order_id = '$oid'");
	} else {
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$pid' AND buyer = '$uid' AND uid = '$post->post_author' AND status = 'unpaid'");
	}

	$id = $row->id;
	$unpaid = $row->unpaid;
	$order_id = $row->order_id;
	$txn_id = $row->txn_id;
	$shipping = ($row->unpaid_total - $row->net_total);
	if( ! $txn_id || $txn_id == '1' ) $txn_id = "N/A";
	if( $unpaid == $no ) {
	$paid = 1;
	$status = "paid";
	} elseif( $unpaid > $no ) {
	$paid = 2;
	$status = "unpaid";
	}
	$left = $unpaid - $no;
	if( $left < 1 ) $left = 0;
	$mc_currency = get_user_meta( $post->post_author, 'currency', true );
	$fname = get_user_meta( $uid, 'first_name', true );
	$lname = get_user_meta( $uid, 'last_name', true );
	$street = get_user_meta( $uid, 'cp_street', true );
	$zip = get_user_meta( $uid, 'cp_zipcode', true );
	$country = get_user_meta( $uid, 'cp_country', true );
	$email = get_user_meta( $uid, 'user_email', true );
	$amail = get_user_meta( $post->post_author, 'user_email', true );

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	if ( empty( $order_id ) || $order_id < 1 ) {
	$order_id = $order + 1;
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('order_id' => $order_id), $where_array);
	}
	$wheres_array = array('order_id' => $order_id);
	$wpdb->update($table_name, array('unpaid' => $left, 'paid' => $paid, 'paiddate' => $tm, 'txn_id' => 'N/A', 'status' => $status), $wheres_array);

	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$rows = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$order_id' AND uid = '$uid' AND post_author = '$post->post_author'");
	if( $rows ) $trans_type = $rows->trans_type;
	if( empty($trans_type) ) $trans_type = "Marked as paid";
	if( ! $rows ) {
	$query = "INSERT INTO {$table_trans} (order_id, pid, post_ids, datemade, uid, post_author, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, receiver_email, address_country, address_zip, address_street, quantity, payment_option, shipping, mc_fee, mc_gross, tax, aws, type, status) VALUES (%d, %d, %s, %d, %d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %f, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, '0', $pid, $tm, $uid, $post->post_author, $tm, $txn_id, $row->item_name, $trans_type, $mc_currency, $lname, $fname, $email, $amail, $country, $zip, $street, $unpaid, 'N/A', $shipping, $row->mc_fee, $row->mc_gross, $row->tax, 'N/A', 'purchase', 'complete'));

	$where_arrays = array('id' => $id);
	$wpdb->update($table_name, array('trans_id' => $wpdb->insert_id), $where_arrays);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($order_id);
	} else {
	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('order_id' => $order_id, 'uid' => $uid, 'post_author' => $post->post_author);
	$wpdb->update($table_trans, array('txn_id' => $rows->txn_id, 'status' => 'complete'), $where_array);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($order_id);
	}
	} else {
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	if ( $oid > 0 ) {
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE order_id = '$oid'");
	} else {
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$pid' AND buyer = '$uid' AND uid = '$post->post_author' AND status = 'unpaid'");
	}

	$id = $row->id;
	$unpaid = $row->unpaid;
	$order_id = $row->order_id;
	$txn_id = $row->txn_id;
	$shipping = ($row->unpaid_total - $row->net_total);
	if( ! $txn_id || $txn_id == '1' ) $txn_id = "N/A";
	if( $unpaid == $no ) {
	$paid = 1;
	$status = "paid";
	} elseif( $unpaid > $no ) {
	$paid = 2;
	$status = "unpaid";
	}
	$left = $unpaid - $no;
	if( $left < 1 ) $left = 0;
	$mc_currency = get_user_meta( $post->post_author, 'currency', true );
	$fname = get_user_meta( $uid, 'first_name', true );
	$lname = get_user_meta( $uid, 'last_name', true );
	$street = get_user_meta( $uid, 'cp_street', true );
	$zip = get_user_meta( $uid, 'cp_zipcode', true );
	$country = get_user_meta( $uid, 'cp_country', true );
	$email = get_user_meta( $uid, 'user_email', true );
	$amail = get_user_meta( $post->post_author, 'user_email', true );

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	if ( empty( $order_id ) || $order_id < 1 ) {
	$order_id = $order + 1;
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('order_id' => $order_id), $where_array);
	}
	$wheres_array = array('order_id' => $order_id);
	$wpdb->update($table_name, array('unpaid' => 0, 'paid' => 1, 'paiddate' => $tm, 'txn_id' => 'N/A', 'status' => 'paid'), $wheres_array);

	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$rows = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$order_id' AND uid = '$uid' AND post_author = '$post->post_author'");
	$trans_type = $rows->trans_type;
	if( empty($trans_type) ) $trans_type = "Marked as paid";
	if( ! $rows ) {
	$query = "INSERT INTO {$table_trans} (order_id, pid, post_ids, datemade, uid, post_author, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, receiver_email, address_country, address_zip, address_street, quantity, payment_option, shipping, mc_fee, mc_gross, tax, aws, type, status) VALUES (%d, %d, %s, %d, %d, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %f, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, '0', $pid, $tm, $uid, $post->post_author, $tm, $txn_id, $row->item_name, $trans_type, $mc_currency, $lname, $fname, $email, $amail, $country, $zip, $street, $unpaid, 'N/A', $shipping, $row->mc_fee, $row->mc_gross, $row->tax, 'N/A', 'purchase', 'complete'));

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$wheres_array = array('id' => $id);
	$wpdb->update($table_name, array('trans_id' => $wpdb->insert_id), $wheres_array);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($order_id);
	} else {
	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('order_id' => $order_id, 'uid' => $uid, 'post_author' => $post->post_author);
	$wpdb->update($table_trans, array('txn_id' => $rows->txn_id, 'status' => 'complete'), $where_array);
	if ( function_exists('AWS_after_product_sale') ) AWS_after_product_sale($order_id);
	}

	$bids = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$pid' AND uid = '$uid'");

	if( $bids ) {
	$win_bid = $bids->id;
	$winner = 1;

	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$where_arrays = array('id' => $win_bid);
	$wpdb->update($table_bids, array('winner' => $winner), $where_arrays);
	update_post_meta($pid, 'winner_bid', $win_bid);
	}
	update_post_meta($pid, 'user_paid', '1');
	update_post_meta($pid, 'winner', $uid);
	}

	exit;
	}
}
add_action('wp_ajax_cp_mark_as_paid', 'cp_mark_listing_as_paid');


function cp_deactivate_users_purchase() {

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$row = $wpdb->get_row("SELECT * FROM {$table_purchases} WHERE id = '$id'");

	$status = $row->status;

	if( $status == "unpaid" || $status == "paid" ) {
	$where_array = array('id' => $id);
	$wpdb->update($table_purchases, array('status' => 'deactivated'), $where_array);
	} elseif( $status == "deactivated" ) {
	$where_array = array('id' => $id);
	$wpdb->update($table_purchases, array('status' => 'paid'), $where_array);
	}

	exit;
	}
}
add_action('wp_ajax_cp_deactivate_purchase', 'cp_deactivate_users_purchase');


function cp_delete_auctions() {

	if ( isset( $_POST["post_var"] ) ) {

	$pid = $_POST["post_var"];
	delete_relist_bids($pid);
	delete_purchase($pid);
	wp_delete_post($pid, true);
	exit;
	}
}
add_action('wp_ajax_delete_auctions', 'cp_delete_auctions');


function cp_auction_trans_delete_pid() {

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$id'");

	$order_id = $row->order_id;
	$uid = $row->uid;
	$type = $row->type;
	$credits = $row->moved_credits;
	$status = $row->status;
	if ( $order_id > 0 ) {
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_purchases} WHERE order_id = '%d'", $order_id));
	}
	if ( $type == "withdraw" ) {
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$add_credits = ($available_credits + $credits);
	update_user_meta( $uid, 'cp_credits', $add_credits );
	}
	if ( $type == "credits_purchase" ) {
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$remove_credits = ($available_credits - $credits);
	if ( $status == "complete" )
	update_user_meta( $uid, 'cp_credits', $remove_credits );
	}
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_transactions} WHERE id = '%d'", $id));
	exit;
	}
}
add_action('wp_ajax_trans_delete_pid', 'cp_auction_trans_delete_pid');


function cp_auction_trans_delete_credits() {

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");

	if ( $row ) {
	$uid = $row->uid;
	$credits = $row->moved_credits;
	$status = $row->status;
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$remove_credits = ($available_credits - $credits);
	if ( $status == "complete" )
	update_user_meta( $uid, 'cp_credits', $remove_credits );

	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));
	}
	exit;
	}
}
add_action('wp_ajax_trans_delete_credits', 'cp_auction_trans_delete_credits');


function cp_auction_trans_delete_withdraw() {

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");

	if ( $row ) {
	$uid = $row->uid;
	$credits = $row->moved_credits;
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$add_credits = ($available_credits + $credits);
	update_user_meta( $uid, 'cp_credits', $add_credits );

	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));
	}
	exit;
	}
}
add_action('wp_ajax_trans_delete_withdraw', 'cp_auction_trans_delete_withdraw');


function cp_auction_delete_complete_purchase() {

	if ( isset( $_POST["post_var"] ) ) {

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	$id = $_POST["post_var"];

	global $wpdb, $post;
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$row = $wpdb->get_row("SELECT * FROM {$table_purchases} WHERE id = '$id'");

	$pid = $row->pid;
	$post = get_post($pid);
	$trans_id = $row->trans_id;
	$uid = $row->uid;
	$buyer = $row->buyer;
	$datemade = $row->datemade;
	$howmany = get_post_meta( $pid, 'cp_auction_howmany', true );
	$howmany_added = get_post_meta( $pid, 'cp_auction_howmany_added', true );

	if ( $row->ref == "process" && $row->status == "unpaid" ) {

	if( $howmany > '0' || $howmany_added == "yes" ) {
	$quantity = $howmany + $row->unpaid;
	update_post_meta( $pid,'cp_auction_howmany', $quantity );
	}

	}

	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_bids} WHERE pid = '%d' AND uid = '%d' AND datemade = '%d'", $pid, $buyer, $datemade));

	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$trans_id'");
	if( $res ) {
	$post_ids = $res->post_ids;
	$list = explode(',', $post_ids);
	if( in_array( $pid, $list ) ) {
	unset($list[array_search($pid, $list)]);
	$upd_list = implode(",", $list);
	$ids = trim($upd_list, ',');
	if ( empty( $ids ) ) {
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_transactions} WHERE id = '%d'", $trans_id));
	} else {
	$where_array = array('id' => $trans_id);
	$wpdb->update($table_transactions, array('post_ids' => $ids), $where_array);
	}
	}
	}
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_purchases} WHERE id = '%d'", $id));
	}
	cp_auction_delete_complete_purchase_emails( $pid, $post, $uid, $buyer );
	exit;
}
add_action('wp_ajax_delete_complete_purchase', 'cp_auction_delete_complete_purchase');


function cp_auction_update_credit_status() {

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");

	if ( $row ) {
	$uid = $row->uid;
	$credits = $row->moved_credits;
	$status = $row->status;
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	$add_credits = $available_credits + $credits;
	$remove_credits = $available_credits - $credits;

	if($status == "pending") {
	$upd_status = "complete";
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('available' => $add_credits, 'status' => $upd_status), $where_array);
	update_user_meta( $uid, 'cp_credits', $add_credits );
	} elseif($status == "complete") {
	$upd_status = "pending";
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('available' => $remove_credits, 'status' => $upd_status), $where_array);
	update_user_meta( $uid, 'cp_credits', $remove_credits );
	}
}
	exit;
	}
}
add_action('wp_ajax_update_credit_status', 'cp_auction_update_credit_status');


function cp_upd_auction_status() {

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	if ( isset( $_POST["post_var"] ) ) {

	$pid = $_POST["post_var"];

	global $post;
	$post = get_post($pid);
	$status = $post->post_status;
	$tm = time();

	if($status == "private") {
	$my_post = array();
	$my_post['ID'] = $pid;
	$my_post['post_status'] = 'publish';

	wp_update_post( $my_post );
	update_post_meta($pid,'cp_ad_sold','no');
	delete_post_meta($pid,'cp_ad_sold_date');
	}
	elseif($status == "draft") {
	$my_post = array();
	$my_post['ID'] = $pid;
	$my_post['post_status'] = 'publish';

	wp_update_post( $my_post );
	update_post_meta($pid,'cp_ad_sold','no');
	delete_post_meta($pid,'cp_ad_sold_date');
	}
	elseif($status == "pending") {
	$my_post = array();
	$my_post['ID'] = $pid;
	$my_post['post_status'] = 'publish';

	wp_update_post( $my_post );
	update_post_meta($pid,'cp_ad_sold','no');
	delete_post_meta($pid,'cp_ad_sold_date');
	}
	elseif($status == "publish") {
	$my_post = array();
	$my_post['ID'] = $pid;
	$my_post['post_status'] = 'pending';

	wp_update_post( $my_post );
	update_post_meta($pid,'cp_ad_sold','yes');
	update_post_meta($pid,'cp_ad_sold_date', $tm);
	}
	exit;
	}
}
add_action('wp_ajax_upd_auction_status', 'cp_upd_auction_status');


function cp_auction_set_escrow_status() {

	if ( isset( $_POST["post_var"] ) ) {

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	$id = $_POST["post_var"];

	global $wpdb;
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$id'");

	$order_id = $row->order_id;
	$post_ids = $row->post_ids;
	$uid = $row->uid;
	$seller = $row->post_author;
	$txn_id = $row->txn_id;
	$status = $row->escrow_status;
	$fee = $row->processor_fee;
	$mc_fee = $row->mc_fee;
	$mc_gross = $row->mc_gross;
	$date = date_i18n('Y-m-d H:i:s', $row->payment_date, true);
	$payout = ($mc_gross - $mc_fee) - fees_covered($fee);

	$fees_total = ($mc_gross - $payout);

	if( empty($status) || $status == "unpaid" ) {
	$where_array = array('id' => $id);
	$wpdb->update($table_transactions, array('escrow_status' => 'paid'), $where_array);
	cp_auction_escrow_payment_confirmation( $order_id, $seller, $uid, $payout, $fees_total, $date );
	} elseif( $status == "paid" ) {
	$where_array = array('id' => $id);
	$wpdb->update($table_transactions, array('escrow_status' => 'unpaid'), $where_array);
	}
	exit;
	}
}
add_action('wp_ajax_set_escrow_status', 'cp_auction_set_escrow_status');


function cp_auction_update_escrow_status() {

	if ( isset( $_POST["post_var"] ) ) {

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	$id = $_POST["post_var"];

	global $wpdb;
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$id'");

	$order_id = $row->order_id;
	$post_ids = $row->post_ids;
	$uid = $row->uid;
	$seller = $row->post_author;
	$txn_id = $row->txn_id;
	$status = $row->status;
	$fee = $row->processor_fee;
	$mc_fee = $row->mc_fee;
	$mc_gross = $row->mc_gross;
	$date = date_i18n('Y-m-d H:i:s', $row->payment_date, true);
	$payout = ($mc_gross - $mc_fee) - fees_covered($fee);

	if ( $status == "pending" ) {
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $id);
	$wpdb->update($table_transactions, array('status' => 'complete'), $where_array);

	$pids = explode(",",$post_ids);
	foreach($pids as $pid) {
	$tm = time();
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('pid' => $pid, 'buyer' => $uid, 'uid' => $seller, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('unpaid' => '0', 'paid' => '1', 'status' => 'paid'), $where_array);
	}
	}
	cp_auction_author_payment_confirmation( $order_id, $seller, $uid, $payout, $txn_id, $date );
	exit;
	}
}
add_action('wp_ajax_update_escrow_status', 'cp_auction_update_escrow_status');


function cp_auction_update_verification_status() {

	if ( isset( $_POST["post_var"] ) ) {

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'", ARRAY_A);

	$uid = $row['uid'];
	$verified = get_user_meta( $uid, 'cp_auction_account_verified', true );
	$IP = $row['ip_address'];

	if( empty($verified) || $verified == 2 ) {
	$upd_status = "approved";
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('status' => $upd_status), $where_array);
	update_user_meta( $uid, 'cp_auction_account_verified', '1' );
	}
	elseif($verified == 1) {
	$upd_status = "pending";
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('status' => $upd_status), $where_array);
	update_user_meta( $uid, 'cp_auction_account_verified', '2' );
	}
	exit;
	}
}
add_action('wp_ajax_update_verification_status', 'cp_auction_update_verification_status');


function cp_auction_verification_delete_pid() {

	if ( isset( $_POST["post_var"] ) ) {

	$sid = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$sid'", ARRAY_A);

	$uid = $row['uid'];

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $sid));
	delete_user_meta( $uid, 'cp_auction_account_verified');
	exit;
	}
}
add_action('wp_ajax_verification_delete_pid', 'cp_auction_verification_delete_pid');


function cp_auction_update_withdraw_status() {

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");

	if ( $row ) {
	$status = $row->status;

	if($status == "pending") {
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('status' => 'complete'), $where_array);
	}
	else {
	return false;
	}
}
	exit;
}
}
add_action('wp_ajax_update_withdraw_status', 'cp_auction_update_withdraw_status');


function cp_auction_update_direct_status() {

	if ( isset( $_POST["post_var"] ) ) {

	$id = $_POST["post_var"];

	global $wpdb, $post;
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE id = '$id'");

	if ( $row ) {
	$order_id = $row->order_id;
	$uid = $row->uid;
	$seller = $row->post_author;
	$status = $row->status;

	if($status == "pending") {
	$upd_status = "complete";
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $id);
	$wpdb->update($table_transactions, array('status' => $upd_status), $where_array);

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('order_id' => $order_id, 'buyer' => $uid, 'uid' => $seller, 'status' => 'paid');
	$wpdb->update($table_purchases, array('paid' => 1), $where_array);
	} elseif($status == "complete") {
	$upd_status = "pending";
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $id);
	$wpdb->update($table_transactions, array('status' => $upd_status), $where_array);

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('order_id' => $order_id, 'buyer' => $uid, 'uid' => $seller, 'status' => 'paid');
	$wpdb->update($table_purchases, array('paid' => 2), $where_array);
	}
	}
}
	exit;
}
add_action('wp_ajax_update_direct_status', 'cp_auction_update_direct_status');


// This is used by purchase history and pending purchase
if(isset($_GET['_update_pending'])) {

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

	$id = $_GET['_update_pending'];

	$quantity = isset( $_GET['quantity'] ) ? esc_attr( $_GET['quantity'] ) : '';
	$unpaid = isset( $_GET['unpaid'] ) ? esc_attr( $_GET['unpaid'] ) : '';

	if( $quantity && !is_numeric($quantity) ) return;
	if( $unpaid && !is_numeric($unpaid) ) return;
	if( $quantity == 0 || empty( $quantity ) ) return;

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");

	$howmany = get_post_meta( $row->pid, 'cp_auction_howmany', true );
	if( ( $howmany > 0 ) && ( $unpaid >= $howmany || $quantity > $howmany ) ) $quantity = $howmany;

	$total = ($row->amount - $row->discount) * $quantity;

	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('numbers' => $quantity, 'unpaid' => $quantity, 'net_total' => $total, 'unpaid_total' => $total), $where_array);
	exit;
}