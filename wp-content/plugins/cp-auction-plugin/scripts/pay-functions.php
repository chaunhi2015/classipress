<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| BUMP ADS PAYMENTS
|--------------------------------------------------------------------------
*/

function cp_auction_bump_ads($uid, $pid, $ip_address, $payer_email, $pp, $bump_price) {
	global $cp_options;

	$cc = $cp_options->currency_code;
	$enabl = get_option( 'cp_auction_enable_conversion' );
	$cuto = get_option( 'cp_auction_convert_to' );
	$conversion = false;
	if ( $enabl == "yes" && class_exists("Currencyr") )
	$conversion = currencyr_exchange( $bump_price, strtolower( $cuto ) );
	$bump_arg = array(
	'uid' => $uid,
	'post_id' => $pid,
	'ip_address' => $ip_address,
	'payment_date' => time(),
	'datemade' => time(),
	'item_name' => 'Bump Ad',
	'trans_type' => 'Bump Ad',
	'mc_currency' => $cc,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'receiver_email' => $pp,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'mc_fee' => false,
	'conversion' => cp_auction_sanitize_amount($conversion),
	'mc_gross' => cp_auction_sanitize_amount($bump_price),
	'howto_pay' => 'paypal',
	'type' => 'bump_ad'
	);

	return $bump_arg;

}


/*
|--------------------------------------------------------------------------
| FEATURED UPGRADE PAYMENTS
|--------------------------------------------------------------------------
*/

function cp_auction_upgrade_ads($uid, $pid, $ip_address, $payer_email, $pp, $upg_price) {
	global $cp_options;

	$cc = $cp_options->currency_code;
	$enabl = get_option( 'cp_auction_enable_conversion' );
	$cuto = get_option( 'cp_auction_convert_to' );
	$conversion = false;
	if ( $enabl == "yes" && class_exists("Currencyr") )
	$conversion = currencyr_exchange( $upg_price, strtolower( $cuto ) );
	$featured_arg = array(
	'uid' => $uid,
	'post_id' => $pid,
	'ip_address' => $ip_address,
	'payment_date' => time(),
	'datemade' => time(),
	'item_name' => 'Upgrade Ad',
	'trans_type' => 'Upgrade Ad',
	'mc_currency' => $cc,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'receiver_email' => $pp,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'mc_fee' => false,
	'conversion' => cp_auction_sanitize_amount($conversion),
	'mc_gross' => cp_auction_sanitize_amount($upg_price),
	'howto_pay' => 'paypal',
	'type' => 'upgrade'
	);

	return $featured_arg;

}


/*
|--------------------------------------------------------------------------
| CALCULATE PAYPAL FEES
|--------------------------------------------------------------------------
*/

function paypal_fees($amount)
{
    $amount += .30;
    return $amount / (1 - .029);
}


/*
|--------------------------------------------------------------------------
| CALCULATE PAYPAL FEES
|--------------------------------------------------------------------------
*/

function fees_covered($fee) {

	$percent = get_option('cp_auction_fees_covered');
	$amount_out = $fee * ($percent / 100);
	if( empty($percent) || $percent == 0 ) return 0;
	else
	return cp_auction_sanitize_amount($amount_out);
}


/*
|--------------------------------------------------------------------------
| CALCULATE VAT / TAXES
|--------------------------------------------------------------------------
*/

function cp_auction_taxes( $amount, $tax ) {

	if ( empty( $tax ) || $tax < 0.1 ) return 0;
	$amount_out = $amount * ($tax / 100);
	return cp_auction_sanitize_amount($amount_out);
}


/*
|--------------------------------------------------------------------------
| SANITIZE AMOUNT
|--------------------------------------------------------------------------
*/

function cp_auction_sanitize_amount( $amount ) {

	global $cp_options;

	$thousands_sep = $cp_options->thousands_separator;
	$decimal_sep   = $cp_options->decimal_separator;

	// Sanitize the amount
	if( $decimal_sep == ',' && false != ( $found = strpos( $amount, $decimal_sep ) ) ) {

		if( $thousands_sep == '.' && false != ( $found = strpos( $amount, $thousands_sep ) ) ) {
			$amount = str_replace( $thousands_sep, '', $amount );
		}

		$amount = str_replace( $decimal_sep, '.', $amount );

	}

	return apply_filters( 'cp_auction_sanitize_amount', $amount );
}


/*
|--------------------------------------------------------------------------
| FORMAT AMOUNT
|--------------------------------------------------------------------------
*/

function cp_auction_format_amount( $amount, $type = '' ) {

	global $post, $cp_options;

	$currency_symbol = "";
	$thousands_sep = $cp_options->thousands_separator;
	$decimal_sep   = $cp_options->decimal_separator;
	$no_decimals = $cp_options->hide_decimals;
	if ( $post )
	$currency_symbol = get_post_meta( $post->ID, 'cp_currency', true );
	if ( !$currency_symbol )
	$currency_symbol = $cp_options->curr_symbol;

	// Format the amount
	if( $decimal_sep == ',' && false != ( $sep_found = strpos( $amount, $decimal_sep ) ) ) {
		$whole = substr( $amount, 0, $sep_found );
		$part = substr( $amount, $sep_found + 1, ( strlen( $amount ) - 1 ) );
		$amount = $whole . '.' . $part;
	}

	// Strip , from the amount (if set as the thousands separator)
	if( $thousands_sep == ',' && false != ( $found = strpos( $amount, $thousands_sep ) ) ) {
		$amount = str_replace( ',', '', $amount );
	}

	$decimals = apply_filters( 'cp_auction_format_amount_decimals', 2 );
	if(( $no_decimals == true ) && ( $amount >= 1000 )) $decimals = 0;

	if( empty( $amount ) ) $amount = 0;

	if( ( $currency_symbol ) && ( $type == "" ) )
	return ''.$currency_symbol.' '.number_format( $amount, $decimals, $decimal_sep, $thousands_sep ).'';
	else
	return number_format( $amount, $decimals, $decimal_sep, $thousands_sep );
}



/*
|--------------------------------------------------------------------------
| CURRENCY DECIMAL FILTER
|--------------------------------------------------------------------------
*/

function cp_auction_currency_decimal_filter( $decimals = 2 ) {
	global $cp_options;

	$currency = $cp_options->currency_code;

	switch( $currency ) {

		case 'RIAL' :
			$decimals = 0;
			break;

		case 'JPY' :
			$decimals = 0;
			break;

	}

	return $decimals;

}
add_filter( 'cp_auction_format_amount_decimals', 'cp_auction_currency_decimal_filter' );


/*
|--------------------------------------------------------------------------
| GET CURRENCY DROPDOWN
|--------------------------------------------------------------------------
*/

function cp_auction_currency_dropdown( $select ) {

$options = false;
$option = get_option('cp_auction_users_currency');
$list = explode(',', $option);
$currency = array(
		'-1' => __('Select Currency','auctionPlugin'),
		'USD' => __('USD - US Dollars','auctionPlugin'),
		'EUR' => __('EUR - Euros','auctionPlugin'),
		'GBP' => __('GBP - British Pounds','auctionPlugin'),
		'AUD' => __('AUD - Australian Dollars','auctionPlugin'),
		'CAD' => __('CAD - Canadian Dollars','auctionPlugin'),
		'NOK' => __('NOK - Norwegian Krone','auctionPlugin'),
		'BRL' => __('BRL - Brazilian Real','auctionPlugin'),
		'MYR' => __('MYR - Malaysian Rinngits','auctionPlugin'),
		'NGN' => __('NGN - Nigerian Naira','auctionPlugin'),
		'JPY' => __('JPY - Japan Yen','auctionPlugin'),
		'NZD' => __('NZD - New Zealand Dollar','auctionPlugin'),
		'RUB' => __('RUB - Russian Ruble','auctionPlugin'),
		'CHF' => __('CHF - Swiss Franc','auctionPlugin'),
		'HKD' => __('HKD - Hong Kong Dollar','auctionPlugin'),
		'SGD' => __('SGD - Singapore Dollar','auctionPlugin'),
		'SEK' => __('SEK - Swedish Krona','auctionPlugin'),
		'DKK' => __('DKK - Danish Krone','auctionPlugin'),
		'MXN' => __('MXN - Mexican Peso','auctionPlugin'),
		'TWD' => __('TWD - Taiwan New Dollar','auctionPlugin'),
		'PHP' => __('PHP - Philippine Peso','auctionPlugin'),
		'TRY' => __('TRY - Turkish Lira','auctionPlugin'),
		'THB' => __('THB - Thai Baht','auctionPlugin'),
		'PLN' => __('PLN - Polish Zloty','auctionPlugin'),
		'HUF' => __('HUF - Hungarian Forint','auctionPlugin'),
		'VUV' => __('VUV - Vanuatu Vatu','auctionPlugin'),
		'CZK' => __('CZK - Czech Koruna','auctionPlugin'),
		'ILS' => __('ILS - Israeli Shekel','auctionPlugin')
		);

	foreach($currency as $currency_id=>$currencies) {
	if($select == $currency_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$currency_id.'">'.$currencies.'</option>';
	} else {
	if( ! is_admin() && in_array($currency_id, $list) )
	$options .= '<option value="'.$currency_id.'">'.$currencies.'</option>';
	else if( is_admin() ) $options .= '<option value="'.$currency_id.'">'.$currencies.'</option>';
	}
	}
		return $options;
}

function cp_auction_currencyr_dropdown( $select ) {

$options = false;
$currency = array(
	'' => __('Select Currency','auctionAdmin'),
	'AED' => __('United Arab Emirates Dirham (AED)', 'auctionAdmin'),
	'AFN' => __('Afghan Afghani (AFN)', 'auctionAdmin'),
	'ALL' => __('Albanian Lek (ALL)', 'auctionAdmin'),
	'AMD' => __('Armenian Dram (AMD)', 'auctionAdmin'),
	'ANG' => __('Neth Antilles Guilder (ANG)', 'auctionAdmin'),
	'AOA' => __('Angolan Kwanza (AOA)', 'auctionAdmin'),
	'ARS' => __('Argentine Peso (ARS)', 'auctionAdmin'),
	'AUD' => __('Australian Dollar (AUD)', 'auctionAdmin'),
	'AWG' => __('Aruba Florin (AWG)', 'auctionAdmin'),
	'AZN' => __('Azerbaijani Manat (AZN)', 'auctionAdmin'),
	'BAM' => __('Bosnia and Herzegovina Convertible Marka (BAM)', 'auctionAdmin'),
	'BBD' => __('Barbadian Dollar (BBD)', 'auctionAdmin'),
	'BDT' => __('Bangladeshi Taka (BDT)', 'auctionAdmin'),
	'BGN' => __('Bulgarian Lev (BGN)', 'auctionAdmin'),
	'BHD' => __('Bahraini Dinar (BHD)', 'auctionAdmin'),
	'BIF' => __('Burundi Franc (BIF)', 'auctionAdmin'),
	'BMD' => __('Bermuda Dollar (BMD)', 'auctionAdmin'),
	'BND' => __('Brunei Dollar (BND)', 'auctionAdmin'),
	'BOB' => __('Bolivian Boliviano (BOB)', 'auctionAdmin'),
	'BRL' => __('Brazilian Real (BRL)', 'auctionAdmin'),
	'BSD' => __('Bahamian Dollar (BSD)', 'auctionAdmin'),
	'BTN' => __('Bhutan Ngultrum (BTN)', 'auctionAdmin'),
	'BWP' => __('Botswana Pula (BWP)', 'auctionAdmin'),
	'BYR' => __('Belarus Ruble (BYR)', 'auctionAdmin'),
	'BZD' => __('Belize Dollar (BZD)', 'auctionAdmin'),
	'CAD' => __('Canada Dollar (CAD)', 'auctionAdmin'),
	'CDF' => __('Congo/Kinshasa Franc (CDF)', 'auctionAdmin'),
	'CHF' => __('Switzerland Franc (CHF)', 'auctionAdmin'),
	'CLP' => __('Chilean Peso (CLP)', 'auctionAdmin'),
	'CNY' => __('Chinese Yuan (CNY)', 'auctionAdmin'),
	'COP' => __('Colombian Peso (COP)', 'auctionAdmin'),
	'CRC' => __('Costa Rica Colon (CRC)', 'auctionAdmin'),
	'CUC' => __('Cuba Convertible Peso (CUC)', 'auctionAdmin'),
	'CUP' => __('Cuban Peso (CUP)', 'auctionAdmin'),
	'CVE' => __('Cape Verde Escudo (CVE)', 'auctionAdmin'),
	'CZK' => __('Czech Koruna (CZK)', 'auctionAdmin'),
	'DJF' => __('Djibouti Franc (DJF)', 'auctionAdmin'),
	'DKK' => __('Danish Krone (DKK)', 'auctionAdmin'),
	'DOP' => __('Dominican Peso (DOP)', 'auctionAdmin'),
	'DZD' => __('Algerian Dinar (DZD)', 'auctionAdmin'),
	'EGP' => __('Egyptian Pound (EGP)', 'auctionAdmin'),
	'ERN' => __('Eritrea Nakfa (ERN)', 'auctionAdmin'),
	'ETB' => __('Ethiopian Birr (ETB)', 'auctionAdmin'),
	'EUR' => __('Euro (EUR)', 'auctionAdmin'),
	'FJD' => __('Fiji Dollar (FJD)', 'auctionAdmin'),
	'FKP' => __('Falkland Islands Pound (FKP)', 'auctionAdmin'),
	'GBP' => __('British Pound (GBP)', 'auctionAdmin'),
	'GEL' => __('Georgian Lari (GEL)', 'auctionAdmin'),
	'GGP' => __('Guernsey Pound (GGP)', 'auctionAdmin'),
	'GHS' => __('GHS (GHS)', 'auctionAdmin'),
	'GIP' => __('Gibraltar Pound (GIP)', 'auctionAdmin'),
	'GMD' => __('Gambian Dalasi (GMD)', 'auctionAdmin'),
	'GNF' => __('Guinea Franc (GNF)', 'auctionAdmin'),
	'GTQ' => __('Guatemala Quetzal (GTQ)', 'auctionAdmin'),
	'GYD' => __('Guyana Dollar (GYD)', 'auctionAdmin'),
	'HKD' => __('Hong Kong Dollar (HKD)', 'auctionAdmin'),
	'HNL' => __('Honduras Lempira (HNL)', 'auctionAdmin'),
	'HRK' => __('Croatian Kuna (HRK)', 'auctionAdmin'),
	'HTG' => __('Haiti Gourde (HTG)', 'auctionAdmin'),
	'HUF' => __('Hungarian Forint (HUF)', 'auctionAdmin'),
	'IDR' => __('Indonesian Rupiah (IDR)', 'auctionAdmin'),
	'ILS' => __('Israeli Shekel (ILS)', 'auctionAdmin'),
	'IMP' => __('Isle of Man Pound (IMP)', 'auctionAdmin'),
	'INR' => __('Indian Rupee (INR)', 'auctionAdmin'),
	'IQD' => __('Iraq Dinar (IQD)', 'auctionAdmin'),
	'IRR' => __('Iran Rial (IRR)', 'auctionAdmin'),
	'ISK' => __('Iceland Krona (ISK)', 'auctionAdmin'),
	'JEP' => __('Jersey Pound (JEP)', 'auctionAdmin'),
	'JMD' => __('Jamaican Dollar (JMD)', 'auctionAdmin'),
	'JOD' => __('Jordanian Dinar (JOD)', 'auctionAdmin'),
	'JPY' => __('Japanese Yen (JPY)', 'auctionAdmin'),
	'KES' => __('Kenya Shilling (KES)', 'auctionAdmin'),
	'KGS' => __('Kyrgyzstani Som (KGS)', 'auctionAdmin'),
	'KHR' => __('Cambodia Riel (KHR)', 'auctionAdmin'),
	'KMF' => __('Comoros Franc (KMF)', 'auctionAdmin'),
	'KPW' => __('North Korean Won (KPW)', 'auctionAdmin'),
	'KRW' => __('South Korean Won (KRW)', 'auctionAdmin'),
	'KWD' => __('Kuwait Dinar (KWD)', 'auctionAdmin'),
	'KYD' => __('Cayman Islands Dollar (KYD)', 'auctionAdmin'),
	'KZT' => __('Kazakhstan Tenge (KZT)', 'auctionAdmin'),
	'LAK' => __('Lao Kip (LAK)', 'auctionAdmin'),
	'LBP' => __('Lebanese Pound (LBP)', 'auctionAdmin'),
	'LKR' => __('Sri Langka Rupee (LKR)', 'auctionAdmin'),
	'LRD' => __('Liberian Dollar (LRD)', 'auctionAdmin'),
	'LSL' => __('Lesotho Loti (LSL)', 'auctionAdmin'),
	'LTL' => __('Lithuanian Lita (LTL)', 'auctionAdmin'),
	'LVL' => __('Latvian Lat (LVL)', 'auctionAdmin'),
	'LYD' => __('Lybian Dinar (LYD)', 'auctionAdmin'),
	'MAD' => __('Moroccan Dirham (MAD)', 'auctionAdmin'),
	'MDL' => __('Moldovan Leu (MDL)', 'auctionAdmin'),
	'MGA' => __('Madagascar Ariary (MGA)', 'auctionAdmin'),
	'MKD' => __('Macedonian Denar (MKD)', 'auctionAdmin'),
	'MMK' => __('Myanmar Kyat (MMK)', 'auctionAdmin'),
	'MNT' => __('Mongolian Tugrik (MNT)', 'auctionAdmin'),
	'MOP' => __('Macau Pataca (MOP)', 'auctionAdmin'),
	'MRO' => __('Mauritania Ougulya (MRO)', 'auctionAdmin'),
	'MUR' => __('Mauritius Rupee (MUR)', 'auctionAdmin'),
	'MVR' => __('Maldives Rufiyaa (MVR)', 'auctionAdmin'),
	'MWK' => __('Malawi Kwacha (MWK)', 'auctionAdmin'),
	'MXN' => __('Mexican Peso (MXN)', 'auctionAdmin'),
	'MYR' => __('Malaysian Ringgit (MYR)', 'auctionAdmin'),
	'MZN' => __('Mozambique Metical (MZN)', 'auctionAdmin'),
	'NAD' => __('Namibian Dollar (NAD)', 'auctionAdmin'),
	'NGN' => __('Nigerian Naira (NGN)', 'auctionAdmin'),
	'NIO' => __('Nicaragua Cordoba (NIO)', 'auctionAdmin'),
	'NOK' => __('Norwegian Krone (NOK)', 'auctionAdmin'),
	'NPR' => __('Nepalese Rupee (NPR)', 'auctionAdmin'),
	'NZD' => __('New Zealand Dollar (NZD)', 'auctionAdmin'),
	'OMR' => __('Omani Rial (OMR)', 'auctionAdmin'),
	'PAB' => __('Panama Balboa (PAB)', 'auctionAdmin'),
	'PEN' => __('Peruvian Nuevo Sol (PEN)', 'auctionAdmin'),
	'PGK' => __('Papua New Guinea Kina (PGK)', 'auctionAdmin'),
	'PHP' => __('Philippine Peso (PHP)', 'auctionAdmin'),
	'PKR' => __('Pakistani Rupee (PKR)', 'auctionAdmin'),
	'PLN' => __('Polish Zloty (PLN)', 'auctionAdmin'),
	'PYG' => __('Paraguayan Guarani (PYG)', 'auctionAdmin'),
	'QAR' => __('Qatar Rial (QAR)', 'auctionAdmin'),
	'RON' => __('Romanian New Leu (RON)', 'auctionAdmin'),
	'RSD' => __('Serbian Dinar (RSD)', 'auctionAdmin'),
	'RUB' => __('Russian Rouble (RUB)', 'auctionAdmin'),
	'RWF' => __('Rwanda Franc (RWF)', 'auctionAdmin'),
	'SAR' => __('Saudi Arabian Riyal (SAR)', 'auctionAdmin'),
	'SBD' => __('Solomon Islands Dollar (SBD)', 'auctionAdmin'),
	'SCR' => __('Seychelles Rupee (SCR)', 'auctionAdmin'),
	'SDG' => __('Sudanese Pound (SDG)', 'auctionAdmin'),
	'SEK' => __('Swedish Krona (SEK)', 'auctionAdmin'),
	'SGD' => __('Singapore Dollar (SGD)', 'auctionAdmin'),
	'SHP' => __('St Helena Pound (SHP)', 'auctionAdmin'),
	'SLL' => __('Sierra Leone Leone (SLL)', 'auctionAdmin'),
	'SOS' => __('Somali Shilling (SOS)', 'auctionAdmin'),
	'SPL' => __('Seborga Luigino (SPL)', 'auctionAdmin'),
	'SRD' => __('Surinamese Dollar (SRD)', 'auctionAdmin'),
	'STD' => __('Sao Tome Dobra (STD)', 'auctionAdmin'),
	'SVC' => __('El Salvador Colon (SVC)', 'auctionAdmin'),
	'SYP' => __('Syrian Pound (SYP)', 'auctionAdmin'),
	'SZL' => __('Swaziland Lilageni (SZL)', 'auctionAdmin'),
	'THB' => __('Thai Baht (THB)', 'auctionAdmin'),
	'TJS' => __('Tajikistan Somoni (TJS)', 'auctionAdmin'),
	'TMT' => __('Turkmenistan Manat (TMT)', 'auctionAdmin'),
	'TND' => __('Tunisian Dinar (TND)', 'auctionAdmin'),
	'TOP' => __('Tonga Pa`ang (TOP)', 'auctionAdmin'),
	'TKY' => __('Turkish Lira (TKY)', 'auctionAdmin'),
	'TTD' => __('Trinidad  Tobago Dollar (TTD)', 'auctionAdmin'),
	'TVD' => __('Tuvaluan Dollar (TVD)', 'auctionAdmin'),
	'TWD' => __('Taiwan Dollar (TWD)', 'auctionAdmin'),
	'TZS' => __('Tanzanian Shilling (TZS)', 'auctionAdmin'),
	'UAH' => __('Ukraine Hryvnia (UAH)', 'auctionAdmin'),
	'UGX' => __('Ugandan Shilling (UGX)', 'auctionAdmin'),
	'USD' => __('United States Dollar (USD)', 'auctionAdmin'),
	'UYU' => __('Uruguayan New Peso (UYU)', 'auctionAdmin'),
	'UZS' => __('Uzbekistan Som (UZS)', 'auctionAdmin'),
	'VEF' => __('Venezuelan Bolivar Fuerte (VEF)', 'auctionAdmin'),
	'VND' => __('Vietnam Dong (VND)', 'auctionAdmin'),
	'VUV' => __('Vanuatu Vatu (VUV)', 'auctionAdmin'),
	'WST' => __('Samoa Tala (WST)', 'auctionAdmin'),
	'XAF' => __('CFA Franc BEAC (XAF)', 'auctionAdmin'),
	'XCD' => __('East Caribbean Dollar (XCD)', 'auctionAdmin'),
	'XDR' => __('Special Drawing Rights (XDR)', 'auctionAdmin'),
	'XOR' => __('CFA Franc BCEAO (XOR)', 'auctionAdmin'),
	'XPF' => __('Pacific Franc (XPF)', 'auctionAdmin'),
	'YER' => __('Yemen Riyal (YER)', 'auctionAdmin'),
	'ZAR' => __('South African Rand (ZAR)', 'auctionAdmin'),
	'ZMK' => __('Zambian Kwacha (ZMK)', 'auctionAdmin'),
	'ZWD' => __('Zimbabwe Dollar (ZWD)', 'auctionAdmin')
		);

	foreach($currency as $currency_id=>$currencies) {
	if($select == $currency_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$currency_id.'">'.$currencies.'</option>';
	} else {
	$options .= '<option value="'.$currency_id.'">'.$currencies.'</option>';
	}
	}
		return $options;
}


/*
|--------------------------------------------------------------------------
| PAYPAL LANGUAGE SETTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_language_dropdown( $select ) {

$options = "";
$option = get_option('cp_auction_users_paypal_lang');
$list = explode(',', $option);

$languages = array(
		'-1' => __('Select Language','auctionPlugin'),
		'AU' => __('Australia','auctionPlugin'),
		'AT' => __('Austria','auctionPlugin'),
		'BE' => __('Belgium','auctionPlugin'),
		'BR' => __('Brazil','auctionPlugin'),
		'CA' => __('Canada','auctionPlugin'),
		'CH' => __('Switzerland','auctionPlugin'),
		'CN' => __('China','auctionPlugin'),
		'DE' => __('Germany','auctionPlugin'),
		'ES' => __('Spain','auctionPlugin'),
		'GB' => __('United Kingdom','auctionPlugin'),
		'FR' => __('France','auctionPlugin'),
		'IT' => __('Italy','auctionPlugin'),
		'NL' => __('Netherlands','auctionPlugin'),
		'PL' => __('Poland','auctionPlugin'),
		'PT' => __('Portugal','auctionPlugin'),
		'RU' => __('Russian Federation','auctionPlugin'),
		'US' => __('United States','auctionPlugin'),
		'DK' => __('Danish','auctionPlugin'),
		'IL' => __('Hebrew','auctionPlugin'),
		'ID' => __('Indonesian','auctionPlugin'),
		'JP' => __('Japanese','auctionPlugin'),
		'NO' => __('Norwegian','auctionPlugin'),
		'BR' => __('Brazilian Portuguese','auctionPlugin'),
		'LT' => __('Lithuania','auctionPlugin'),
		'LV' => __('Latvia','auctionPlugin'),
		'UA' => __('Ukraine','auctionPlugin'),
		'SE' => __('Swedish','auctionPlugin'),
		'TH' => __('Thai','auctionPlugin'),
		'TR' => __('Turkish','auctionPlugin'),
		'ZN' => __('Chinese (For domestic Chinese bank transactions only)','auctionPlugin'),
		'C2' => __('Chinese (For CUP, bank card and cross-border transactions)','auctionPlugin'),
		'TW' => __('Taiwan (Province of China)','auctionPlugin'),
		'HK' => __('Hong Kong','auctionPlugin')
		);

	foreach($languages as $language_id=>$language) {
	if($select == $language_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$language_id.'">'.$language.'</option>';
	} else {
	if( ! is_admin() && in_array($language_id, $list) )
	$options .= '<option value="'.$language_id.'">'.$language.'</option>';
	else if( is_admin() ) $options .= '<option value="'.$language_id.'">'.$language.'</option>';
	}
	}
		return $options;
}


/*
|--------------------------------------------------------------------------
| CALCULATE AMOUNT PLUSS PAYPAL FEES
|--------------------------------------------------------------------------
*/

function pp_fees($amount)
{
    $amount += .30;
    return $amount / (1 - .029);
}


/*
|--------------------------------------------------------------------------
| GET RECEIVER EMAIL WHEN USING ESCROW PAYMENTS
|--------------------------------------------------------------------------
*/

function cp_auction_get_paypalemail( $uid, $author ) {

	global $cp_options;
	
	$act = get_option('cp_auction_plugin_auctiontype');
	$pay_user = get_option('cp_auction_plugin_pay_user');
	$pay_admin = get_option('cp_auction_plugin_pay_admin');

	if(( $pay_admin == "yes" ) && ( $pay_user == "yes" )) {
	$pay = "both";
	} elseif(( $pay_admin == "yes" ) && ( $pay_user == "no" )) {
	$pay = "admin";
	} else {
	$pay = "user";
	}

	$users_choice = get_user_meta( $author, 'cp_use_safe_payment', true );
	if($users_choice == "") $users_choice = "yes";
	if(($pay == "both") && ($users_choice == "no")){
	$pp = get_user_meta( $author, 'paypal_email', true );
	}
	if(($pay == "both") && ($users_choice == "yes")){
	$pp = get_option('cp_auction_plugin_paypalemail');
	if( ( empty( $pp ) ) && ( get_option('cp_version') < '3.2.0') ) $pp = get_option('paypal_email'); // Use classipress email...
	elseif( ( empty( $pp ) ) && ( get_option('cp_version') > '3.1.9') ) $pp = $cp_options->gateways['paypal']['email_address']; // Use classipress email...
	}	
	if($pay == "admin") {
	$pp = get_option('cp_auction_plugin_paypalemail');
	if( ( empty( $pp ) ) && ( get_option('cp_version') < '3.2.0') ) $pp = get_option('paypal_email'); // Use classipress email...
	elseif( ( empty( $pp ) ) && ( get_option('cp_version') > '3.1.9') ) $pp = $cp_options->gateways['paypal']['email_address']; // Use classipress email...
	}
	if($pay == "user") {
	$pp = get_user_meta( $author, 'paypal_email', true );
	}
	return $pp;
}


/*
|--------------------------------------------------------------------------
| CALCULATE PAYMENT FEES
|--------------------------------------------------------------------------
*/

function cp_auction_calculate_fees($amount, $howto_pay, $fee, $user = false) {

	if( $howto_pay == "cp_credits" ) {
		if( get_option('cp_auction_credits_fee_type') == "static_on" )
		return cp_auction_sanitize_amount($amount + $fee);
		elseif( get_option('cp_auction_credits_fee_type') == "percentage_on" )
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
	}
	if( $howto_pay == "item_credits" && $user = false ) {
		if( get_option('cp_auction_credits_fee_type') == "static_on" )
		return cp_auction_sanitize_amount($amount + $fee);
		elseif( get_option('cp_auction_credits_fee_type') == "percentage_on" )
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
		elseif( get_option('cp_auction_credits_fee_type') == "static_off" )
		return cp_auction_sanitize_amount($amount - $fee);
		elseif( get_option('cp_auction_credits_fee_type') == "percentage_off" )
		return cp_auction_sanitize_amount($amount - ($amount * (((float)($fee))/100)));
		else return $amount;
	}
	elseif( $howto_pay == "item_credits" && $user = true ) {
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
	}
	elseif( $howto_pay == "bank_transfer" && $user = false ) {
		if( get_option('cp_auction_banktransfer_fee_type') == "static_on" )
		return cp_auction_sanitize_amount($amount + $fee);
		elseif( get_option('cp_auction_banktransfer_fee_type') == "percentage_on" )
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
	}
	elseif( $howto_pay == "bank_transfer" && $user = true ) {
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
	}
	elseif( $howto_pay == "paypal" && $user = false ) {
		if( get_option('cp_auction_paypal_fee_type') == "static_on" )
		return cp_auction_sanitize_amount($amount + $fee);
		elseif( get_option('cp_auction_paypal_fee_type') == "percentage_on" )
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
	}
	elseif( $howto_pay == "paypal" && $user = true ) {
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
	}
	elseif( $howto_pay == "cod" && $user = false ) {
		if( get_option('cp_auction_cod_fee_type') == "static_on" )
		return cp_auction_sanitize_amount($amount + $fee);
		elseif( get_option('cp_auction_cod_fee_type') == "percentage_on" )
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
	}
	elseif( $howto_pay == "cod" && $user = true ) {
		return cp_auction_sanitize_amount($amount + $fee);
	}
	else {
		return cp_auction_sanitize_amount($amount);
	}
}



/*
|--------------------------------------------------------------------------
| CALCULATE ESCROW PAYMENT FEES
|--------------------------------------------------------------------------
*/

function cp_auction_calculate_esc_fees($amount, $howto_pay, $fee) {

	if( $howto_pay == "cre_escrow" ) {
		if( get_option('cp_auction_cre_escrow_fee_type') == "static_on" )
		return cp_auction_sanitize_amount($amount + $fee);
		elseif( get_option('cp_auction_cre_escrow_fee_type') == "percentage_on" )
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
		elseif( get_option('cp_auction_cre_escrow_fee_type') == "static_off" )
		return cp_auction_sanitize_amount($amount - $fee);
		elseif( get_option('cp_auction_cre_escrow_fee_type') == "percentage_off" )
		return cp_auction_sanitize_amount($amount - ($amount * (((float)($fee))/100)));
		else return $amount;
	}
	elseif( $howto_pay == "bt_escrow" ) {
		if( get_option('cp_auction_bt_escrow_fee_type') == "static_on" )
		return cp_auction_sanitize_amount($amount + $fee);
		elseif( get_option('cp_auction_bt_escrow_fee_type') == "percentage_on" )
		return cp_auction_sanitize_amount($amount + ($amount * (((float)($fee))/100)));
		elseif( get_option('cp_auction_bt_escrow_fee_type') == "static_off" )
		return cp_auction_sanitize_amount($amount - $fee);
		elseif( get_option('cp_auction_bt_escrow_fee_type') == "percentage_off" )
		return cp_auction_sanitize_amount($amount - ($amount * (((float)($fee))/100)));
		else return $amount;
	}
	else {
		return cp_auction_sanitize_amount($amount);
	}
}


/*
|--------------------------------------------------------------------------
| CALCULATE END FEES
|--------------------------------------------------------------------------
*/

function cp_auction_calculate_end_fees($option, $sum, $seller) {

	if( $option == 'cih' ) {
		return $sum;
	}
	if( $option == 'cod' ) {
	if ( get_option('cp_auction_plugin_admins_only') == "yes" ) {
	if( get_option('cp_auction_cod_fee_type') == "static_on" ) $cs = ", ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_cod_fee_type') == "percentage_on" ) $cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";

	$fees = get_option('cp_auction_cod_fee');
	$mc_gross = cp_auction_calculate_fees($sum, 'cod', $fees);
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
	else {
	$cs = ", ".__('paid by buyer.', 'auctionPlugin')."";
	$fees = get_user_meta( $seller, 'cp_auction_cod_fees', true );

	$mc_gross = cp_auction_calculate_fees($sum, 'cod', $fees, 'author');
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
		return ''.$mc_gross.'|'.$mc_fee.'|'.$fees.'|'.$cs.'';
	}
	if( $option == 'paypal' ) {
	if ( get_option('cp_auction_plugin_admins_only') == "yes" ) {
	if( get_option('cp_auction_paypal_fee_type') == "static_on" ) $cs = ", ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_paypal_fee_type') == "percentage_on" ) $cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";

	$fees = get_option('cp_auction_paypal_fee');
	$mc_gross = cp_auction_calculate_fees($sum, 'paypal', $fees);
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
	else {
	$cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";
	$fees = get_user_meta( $seller, 'cp_auction_paypal_fees', true );

	$mc_gross = cp_auction_calculate_fees($sum, 'paypal', $fees, 'author');
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
		return ''.$mc_gross.'|'.$mc_fee.'|'.$fees.'|'.$cs.'';
	}
	if( $option == 'bank_transfer' ) {
	if ( get_option('cp_auction_plugin_admins_only') == "yes" ) {
	if( get_option('cp_auction_banktransfer_fee_type') == "static_on" ) $cs = ", ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_banktransfer_fee_type') == "percentage_on" ) $cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";

	$fees = get_option('cp_auction_plugin_bt_fee');
	$mc_gross = cp_auction_calculate_fees($sum, 'bank_transfer', $fees);
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
	else {
	$cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";
	$fees = get_user_meta( $seller, 'cp_auction_bank_transfer_fees', true );

	$mc_gross = cp_auction_calculate_fees($sum, 'bank_transfer', $fees, 'author');
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
		return ''.$mc_gross.'|'.$mc_fee.'|'.$fees.'|'.$cs.'';
	}
	elseif( $option == 'item_credits' ) {
	if ( get_option('cp_auction_plugin_admins_only') == "yes" ) {
    	if( get_option('cp_auction_credits_fee_type') == "static_on" ) $cs = ", ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_credits_fee_type') == "percentage_on" ) $cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";

	$fees = get_option('cp_auction_plugin_credits_fee');
	$mc_gross = cp_auction_calculate_fees($sum, 'item_credits', $fees);
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
	else {
	$cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";
	$fees = get_user_meta( $seller, 'cp_auction_credit_fees', true );

	$mc_gross = cp_auction_calculate_fees($sum, 'item_credits', $fees, 'author');
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
		return ''.$mc_gross.'|'.$mc_fee.'|'.$fees.'|'.$cs.'';
	}
	elseif( $option == 'cre_escrow' ) {
    	if( get_option('cp_auction_cre_escrow_fee_type') == "static_on" ) $cs = ", ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_cre_escrow_fee_type') == "percentage_on" ) $cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_cre_escrow_fee_type') == "static_off" ) $cs = ", ".__('paid by seller.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_cre_escrow_fee_type') == "percentage_off" ) $cs = "%, ".__('paid by seller.', 'auctionPlugin')."";

	$fees = get_option('cp_auction_plugin_esc_credits_fee');
	$mc_gross = cp_auction_calculate_esc_fees($sum, 'cre_escrow', $fees);
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;

	if( get_option('cp_auction_cre_escrow_fee_type') == "static_off" ) $mc_gross = $sum;
	elseif( get_option('cp_auction_cre_escrow_fee_type') == "percentage_off" ) $mc_gross = $sum;

		return ''.$mc_gross.'|'.$mc_fee.'|'.$fees.'|'.$cs.'';
	}
	elseif( $option == 'bt_escrow' ) {
    	if( get_option('cp_auction_bt_escrow_fee_type') == "static_on" ) $cs = ", ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_bt_escrow_fee_type') == "percentage_on" ) $cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_bt_escrow_fee_type') == "static_off" ) $cs = ", ".__('paid by seller.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_bt_escrow_fee_type') == "percentage_off" ) $cs = "%, ".__('paid by seller.', 'auctionPlugin')."";

	$fees = get_option('cp_auction_plugin_esc_bt_fee');
	$mc_gross = cp_auction_calculate_esc_fees($sum, 'bt_escrow', $fees);
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;

	if( get_option('cp_auction_bt_escrow_fee_type') == "static_off" ) $mc_gross = $sum;
	elseif( get_option('cp_auction_bt_escrow_fee_type') == "percentage_off" ) $mc_gross = $sum;

		return ''.$mc_gross.'|'.$mc_fee.'|'.$fees.'|'.$cs.'';
	}
	elseif( $option == 'pp_adaptive' ) {
	if ( get_option('cp_auction_plugin_admins_only') == "yes" ) {
	if( get_option('cp_auction_paypal_fee_type') == "static_on" ) $cs = ", ".__('paid by buyer.', 'auctionPlugin')."";
	elseif( get_option('cp_auction_paypal_fee_type') == "percentage_on" ) $cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";

	$fees = get_option('cp_auction_paypal_fee');
	$mc_gross = cp_auction_calculate_fees($sum, 'paypal', $fees);
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
	else {
	$cs = "%, ".__('paid by buyer.', 'auctionPlugin')."";
	$fees = get_user_meta( $seller, 'cp_auction_paypal_fees', true );

	$mc_gross = cp_auction_calculate_fees($sum, 'paypal', $fees, 'author');
	if( $mc_gross > $sum ) $mc_fee = $mc_gross - $sum;
	else $mc_fee = $sum - $mc_gross;
	}
		return ''.$mc_gross.'|'.$mc_fee.'|'.$fees.'|'.$cs.'';
	}
}


/*
|--------------------------------------------------------------------------
| SHOPPING CART TIMER & EXPIRATION
|--------------------------------------------------------------------------
*/

function cp_auction_cart_timer( $post_id, $trans_id, $ref, $endTime, $remove, $quantity ) {

	global $wpdb;

	$todayDate = date_i18n('Y-m-d H:i:s', strtotime("current_time('mysql')"), true); // current date
	$currentTime = strtotime( $todayDate ); //Change date into time
	$howmany = get_post_meta( $post_id, 'cp_auction_howmany', true );
	$howmany_added = get_post_meta( $post_id, 'cp_auction_howmany_added', true );

	if( $howmany_added == "unlimited" )
		return true;

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$unpaid = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(unpaid) FROM {$table_name} WHERE pid = %d AND paid != %d AND status != %s", $post_id, '2', 'paid' ) );
	$ordered = $unpaid + $quantity;
	$adjusted = $ordered - $remove;

	if( $howmany_added == "yes" && $howmany >= $ordered )
		return true;

	if ( $endTime > '0' && $currentTime >= $endTime && $ref != "process" ) {

	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $trans_id));

	if( $howmany_added == "yes" && $howmany < $adjusted )
		return false;
	else
		return true;

	}
		return false;

}

function cp_auction_single_cart_timer() {

	if ( ! is_singular( APP_POST_TYPE ) )
		return;

	global $post, $wpdb;

	$todayDate = date_i18n('Y-m-d H:i:s', strtotime("current_time('mysql')"), true); // current date
	$currentTime = strtotime( $todayDate ); //Change date into time

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$ordered = $wpdb->get_results("SELECT * FROM {$table_name} WHERE pid = '$post->ID' AND paid != '2' AND status != 'paid'");

	if ( $ordered ) {
	foreach ( $ordered as $order ) {

	if ( ( $order->expiration > '0' ) && ( $currentTime >= $order->expiration && $order->ref != "process" ) ) {

	$wpdb->query($wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $order->id));

	}
	
	}
	}

}
add_action( 'appthemes_before_loop', 'cp_auction_single_cart_timer' );


/*
|--------------------------------------------------------------------------
| ADD TO CART FUNCTION
|--------------------------------------------------------------------------
*/

function cp_auction_buynow_onpost( $pid ) {

	global $current_user, $post, $cp_options, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	if ( ! is_user_logged_in() || ! $pid ) return;

	$post = get_post($pid);

	$error = false;
	$settings_url = ''.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'';
	$quantity = isset( $_POST['quantity'] ) ? esc_attr( $_POST['quantity'] ) : '';
	$cp_coupon = isset( $_POST['cp_coupon_code'] ) ? esc_attr( $_POST['cp_coupon_code'] ) : '';

	$verified = true;
	if(( get_option('cp_auction_plugin_verification') > 0 ) && ( get_option('cp_auction_plugin_upl_verification') != "yes" )) {
	$user_verified = get_user_meta( $uid, 'cp_auction_account_verified', true );
	if( $user_verified ) $verified = true;
	if( empty( $user_verified )) $verified = false;
	}

	$country = get_user_meta( $uid, 'cp_country', true );
	if ( empty( $country ) && get_option('cp_auction_enable_geoip') == "yes" )
	$country = do_shortcode( '[geoip_detect2 property="country"]' );

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$ordered = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$post->ID' AND buyer = '$uid' AND paid != '2' AND status != 'paid'");
	$unpaid = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(unpaid) FROM {$table_name} WHERE pid = %d AND paid != %d AND status != %s", $post->ID, '2', 'paid' ) );
	$sold = $unpaid + $quantity;

	$coupon_error = 1;
	$discount = 0;
	$timeout = true;
	if ( get_option('cp_auction_hold_in_cart') != '' ) $timeout = cp_auction_cart_timer( $post->ID, $ordered->id, $ordered->ref, $ordered->expiration, $ordered->unpaid, $quantity );
	$bid = get_post_meta($post->ID,'cp_buy_now',true);
	if( empty( $bid ) ) $bid = get_post_meta($post->ID, 'cp_price', true);

	$before_discount = $bid;
	$howmany = get_post_meta( $post->ID, 'cp_auction_howmany', true );
	$howmany_added = get_post_meta( $post->ID, 'cp_auction_howmany_added', true );
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

	if( $cp_coupon ) {
	$check = cp_auction_check_coupon_discount( $post->ID, $post->post_author, $cp_coupon );
	if( $check ) {
	$bid = cp_auction_coupon_item_cost($before_discount, $cp_coupon);
	$discount = $before_discount - $bid;
	} else {
	$error = 1;
	}
	}

	if( $bid < 0 ) $bid = 0;

	if( $error == false && $uid == $post->post_author ) $error = 2;
	if( $error == false && $verified == false ) $error = 3;
	if( ( $error == false ) && ( $howmany > '0' || $howmany_added == "yes" ) && ( $sold > $howmany ) ) $error = 4;
	if( $error == '4' && $timeout == false ) $error = 5;
	if( $error == false && empty( $ordered->id ) && $timeout == false ) $error = 5;
	if( $error == false && $ordered->ref == "process" ) $error = 6;

	if( $error == 0 ) {

	$todayDate = date_i18n('Y-m-d H:i:s', strtotime("current_time('mysql')"), true); // current date
	$hours = get_option( 'cp_auction_hold_in_cart' );
	if ( empty( $hours ) ) $hours = "48 hours";
	$newTime = date_i18n('Y-m-d H:i:s', strtotime("$todayDate + $hours"), true);
	$endTime = strtotime( $newTime );
	$tm = strtotime( $todayDate );

	$add = $ordered->numbers + $quantity;
	$sum_unpaid = $ordered->unpaid + $quantity;

	if( $ordered->id ) {
	$total = $bid * $sum_unpaid;
	$unpaid_total = $bid * $sum_unpaid;
	$where_array = array('id' => $ordered->id);
	$wpdb->update($table_name, array('amount' => $before_discount, 'discount' => $discount, 'datemade' => $tm, 'numbers' => $add, 'unpaid' => $sum_unpaid, 'net_total' => $total, 'unpaid_total' => $unpaid_total, 'expiration' => $endTime), $where_array);
	} else {
	$total = $bid * $quantity;
	$unpaid_total = $bid * $quantity;
	$query = "INSERT INTO {$table_name} (pid, uid, buyer, item_name, amount, discount, mc_gross, datemade, numbers, unpaid, net_total, unpaid_total, type, expiration, status) VALUES (%d, %d, %d, %s, %f, %f, %f, %d, %d, %d, %f, %f, %s, %d, %s)";
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $uid, $post->post_title, $before_discount, $discount, $unpaid_total, $tm, $quantity, $quantity, $total, $unpaid_total, 'classified', $endTime, 'unpaid'));
	}

	cp_auction_add_shipping( $post->ID, $post->post_author, $uid, $country );

	cp_auction_buy_now_emails( $post->ID, $post, $uid, $unpaid_total );

	if( $discount ) {
	cp_auction_use_coupon($cp_coupon);
	update_user_meta( $uid, $post->post_title, $bid );
	}

	if(( get_option('cp_auction_plugin_if_watchlist') == "yes" ) && ( $my_type == "normal" || $my_type == "reverse" )) {
	cp_auction_watchlist_buy_notifications($post->ID, $bid);
	}

	appthemes_display_notice( 'success', __( 'Please wait while your cart is being refreshed...', 'auctionPlugin' ) );
	$redirect = ''.get_permalink($post->ID).'?cart=true';
	cp_auction_page_redirect($redirect);

	}

	if( $error == 1 ) appthemes_display_notice( 'error', sprintf(__('Coupon code "%s" is not active or does not exist!','auctionPlugin'), $cp_coupon) );
	if( $error == 2 ) appthemes_display_notice( 'error', __('Sorry, you can not buy your own products!','auctionPlugin' ) );
	if( $error == 3 ) appthemes_display_notice( 'error', sprintf(__('You must verify your <strong><a href="%s">account</a></strong> before you can make any purchases, <strong><a href="%s">click here!</a></strong>','auctionPlugin'), $settings_url, $settings_url ) );
	if( $error == 4 ) appthemes_display_notice( 'error', sprintf(__('Sorry, the seller has only %s items available for sale!','auctionPlugin'), $howmany) );
	if( $error == 5 ) appthemes_display_notice( 'error', __('Sorry, this product has status pending purchase, please try again later!','auctionPlugin' ) );
	if( $error == 6 ) appthemes_display_notice( 'error', __('This item is in your cart recorded as pending payment, quantity can no longer be adjusted!','auctionPlugin' ) );
}