<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| ADD OTHER ADS LISTED BY THIS USER AFTER POST CONTENT
|--------------------------------------------------------------------------
*/

function cp_auction_other_user_ads() {
	global $post;
	$numbers = get_option('cp_auction_number_of_ads');
	$sarray = array( 'eclassify', 'flatron', 'ultraclassifieds' );

	if( ! is_singular(APP_POST_TYPE) )
		return;
?>

	<div class="clr"></div>

	<div class="pad10"></div>

<?php if( in_array( get_option('stylesheet'), $sarray ) ) { ?>
<div class="related-adso">
<span class="related-ads-title"><?php _e( 'Other items listed by', APP_TD ); ?> <?php the_author_meta('display_name'); ?></span>
</div>
<?php } else { ?>
	<h3><?php _e( 'Other items listed by', APP_TD ); ?> <?php the_author_meta('display_name'); ?></h3>
<?php } ?>

	<div class="pad5"></div>

	<ul>

	<?php $other_items = new WP_Query( array( 'posts_per_page' => $numbers, 'post_type' => APP_POST_TYPE, 'post_status' => 'publish', 'author' => get_the_author_meta('ID'), 'orderby' => 'rand', 'post__not_in' => array( $post->ID ), 'no_found_rows' => true ) ); ?>

	<?php if ( $other_items->have_posts() ) : ?>

	<?php while ( $other_items->have_posts() ) : $other_items->the_post(); ?>

	<li>&raquo; <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

	<?php endwhile; ?>

	<?php else: ?>

	<li><?php _e( 'No other ads by this poster found.', APP_TD ); ?></li>

	<?php endif; ?>

	<?php wp_reset_postdata(); ?>

	</ul>

	<div class="pad5"></div>

	<a href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>" class="btn"><span><?php _e( 'Latest items listed by', APP_TD ); ?> <?php the_author_meta('display_name'); ?> &raquo;</span></a>

	<div class="pad5"></div>

<?php
}
if( get_option('cp_auction_view_all_ads') == "content" )
add_action( 'appthemes_after_post_content', 'cp_auction_other_user_ads', 100 );


/*
|--------------------------------------------------------------------------
| ADD OTHER ADS LISTED BY THIS USER IN THE LOOP
|--------------------------------------------------------------------------
*/

function cp_auction_query_other_user_ads() {
	global $cp_options, $post, $cpurl;

	if( ! is_singular(APP_POST_TYPE) )
		return;

	$numbers = get_option('cp_auction_number_of_ads');
	$apage = get_option( 'cp_auction_author_page' );
	if( $apage == "apage" ) $view_all_url = get_author_posts_url($post->post_author);
	else $view_all_url = cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.'');
	$sarray = array( 'eclassify', 'flatron', 'ultraclassifieds' );

	$theauthorid = get_the_author_meta('ID');
	$args = array( 'post_type' => APP_POST_TYPE, 'posts_per_page' => $numbers, 'orderby' => 'rand', 'author__in' => $theauthorid, 'post__not_in' => array( $post->ID ));
	
		query_posts( $args );
	
		if ( have_posts() ) {
?>

<?php if( in_array( get_option('stylesheet'), $sarray ) ) { ?>
<div class="related-adso">
<span class="related-ads-title"><?php _e( 'Other items listed by', APP_TD ); ?> <?php the_author_meta('display_name'); ?></span>
</div>
<?php } else { ?>

<div class="shadowblock_out">

	<div class="shadowblock">

    	<h2 class="single dotted"><?php _e( 'Other items listed by', APP_TD ); ?> <?php the_author_meta('display_name'); ?></h2>
    	
    </div><!-- .shadowblock -->

</div><!-- .shadowblock_out -->

<?php } ?>

<div class="related">

<?php while ( have_posts() ) : the_post(); ?>
    
	<?php appthemes_before_post(); ?>
    
	<div class="post-block-out <?php cp_display_style( 'featured' ); ?>">
        
    	<div class="post-block">
        
        	<div class="post-left">
        
            	<?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?>
                
            </div>
        
            <div class="<?php cp_display_style( array( 'ad_images', 'ad_class' ) ); ?>">
                
            	<?php appthemes_before_post_title(); ?>
        
                <h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= 75 ) echo mb_substr( get_the_title(), 0, 75 ).'...'; else the_title(); ?></a></h3>
                    
                <div class="clr"></div>
                    
                <?php appthemes_after_post_title(); ?>
                   
                <div class="clr"></div>
                    
                <?php appthemes_before_post_content(); ?>
        
                <p class="post-desc"><?php echo cp_get_content_preview( 160 ); ?></p>
                    
                <?php appthemes_after_post_content(); ?>
                    
                <div class="clr"></div>
        
            </div>
        
            <div class="clr"></div>
        
        </div><!-- /post-block -->
          
	</div><!-- /post-block-out -->   
        
    <?php appthemes_after_post(); ?>
        
<?php endwhile; ?>

</div><!-- .related -->

<div class="pad5"></div>

<?php	
	}
	
	wp_reset_query();
?>
	<div class="paging"><a href="<?php echo $view_all_url; ?>"><?php if ( get_option('stylesheet') == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View all items listed by', APP_TD ); ?> <?php the_author_meta('display_name'); ?> </a></div>
<?php
}
if( get_option('cp_auction_view_all_ads') == "below" )
add_action( 'appthemes_after_loop', 'cp_auction_query_other_user_ads' );


/*
|--------------------------------------------------------------------------
| ADD THE BIDDER BOX ON SINGLE AUCTION AD PAGE
|--------------------------------------------------------------------------
*/

function cp_auction_bidder_box_code() {

	global $current_user, $post, $cp_options, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$user_can = current_user_can( 'manage_options' );
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

	if( ! is_singular( APP_POST_TYPE ) )
		return;

	if( $my_type != "normal" && $my_type != "reverse" )
		return;

	$success = ""; if( isset($_GET['success']) ) $success = $_GET['success'];

	$pos = get_option('cp_auction_timer');
	$list = explode(',', $pos['cp_disable_timer']);

	$option = get_option('cp_auction_fields');
	$table_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_bestoffer'");
	if( $row ) {
	$fname = $row->field_name;
	$enabled = explode(',', $option[$fname]);
	}

	$verified = true;
	if(( get_option('cp_auction_plugin_verification') > 0 ) && ( get_option('cp_auction_plugin_upl_verification') != "yes" )) {
	$user_verified = get_user_meta( $uid, 'cp_auction_account_verified', true );
	if( $user_verified ) $verified = true;
	if( empty( $user_verified )) $verified = false;
	}

	$cp_bid_increments = get_post_meta($post->ID, 'cp_bid_increments', true);
	$cp_start_price = get_post_meta($post->ID, 'cp_start_price', true);
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	$end_date = strtotime(get_post_meta($post->ID, 'cp_sys_expire_date', true));
	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );
	$start_timer = get_option( 'cp_auction_start_timer' );
	$todayDate = current_time('mysql');
	$ad_period = get_post_meta( $post->ID, 'cp_auction_listing_duration', true );
	$period = get_option( 'cp_auction_period_type' );

	if ( ( $closed != 'yes' ) && ( $my_type == 'normal' || $my_type == 'reverse' ) ) {

	if(isset($_POST['best_offer'])) {

	$error = 0;
	$sold = get_post_meta( $post->ID, 'cp_ad_sold', true );
	if(isset($_POST['bid_anonymous'])) $bid_anonymous = $_POST['bid_anonymous'];
	if(isset($_POST['agreement'])) $agreement = $_POST['agreement'];
	if(isset($_POST['bid'])) $bid = cp_auction_sanitize_amount($_POST['bid']);

	$tm = time();
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$latest = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$uid' AND type = 'bestoffer'");
	$id = $latest->id;
	$boff_usage = $latest->boff_usage;
	$upd_usage = $boff_usage + 1;

	if(empty($bid_anonymous)) {
	$bid_anonymous = "no";
	}
	if( empty($agreement) ) $error = 1;
	elseif( $bid_anonymous == "yes" && get_option('cp_allow_anonymous'.$post->post_author) == "no" ) $error = 3;
	elseif( is_user_logged_in() != true ) $error = 4;
	elseif( $uid == $post->post_author ) $error = 5;
	elseif( !is_numeric($bid) ) $error = 6;
	elseif( $boff_usage >= get_option( 'aws_bo_no_best_offer' ) ) $error = 7;
	elseif( $sold == "yes" ) $error = 8;

	if( $error == 0 ) {

	if( $latest ) {
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('datemade' => $tm, 'bid' => $bid, 'boff_usage' => $upd_usage, 'latest_offer' => $bid, 'acceptance' => '', 'date_type' => 'datemade'), $where_array);
	} else {
	$query = "INSERT INTO {$table_name} (pid, datemade, bid, uid, anonymous, type, boff_usage, latest_offer, date_type) VALUES (%d, %d, %f, %d, %s, %s, %d, %f, %s)"; 
	$wpdb->query($wpdb->prepare($query, $pid, $tm, $bid, $uid, $bid_anonymous, 'bestoffer', '1', $bid, 'datemade'));
	}
	aws_bo_new_boffer_was_posted_email( $pid, $post, $uid, $bid );

	appthemes_display_notice( 'success', __( 'Please wait while your offer is processed...', 'auctionPlugin' ) );
	$redirect = ''.get_permalink($post->ID).'?success=1';
	cp_auction_page_redirect($redirect);
	}
	if( $error == 1 ) appthemes_display_notice( 'error', __( 'You must agree that you have read our policies!','auctionPlugin' ) );
	elseif( $error == 3 ) appthemes_display_notice( 'error', __( 'Sorry, the author does not allow anonymous offers.','auctionPlugin' ) );
	elseif( $error == 4 ) appthemes_display_notice( 'error', __( 'You must be logged in to submit your best offer!', 'auctionPlugin' ) );
	elseif( $error == 5 ) appthemes_display_notice( 'error', __( 'You can not post offers on your own ads!', 'auctionPlugin' ) );
	elseif( $error == 6 ) appthemes_display_notice( 'error', __( 'Your best offer needs to be a numeric value!', 'auctionPlugin' ) );
	elseif( $error == 7 ) appthemes_display_notice( 'error', __( 'Sorry, you do not have permission to post more best offers!','auctionPlugin' ) );
	elseif( $error == 8 ) appthemes_display_notice( 'error', __( 'Sorry but this product is already sold!','auctionPlugin' ) );
}

	if(isset($_POST['bid_now'])) { ?>

	<div id="bid" class="clear50"></div>

	<?php
	$bid     = cp_auction_sanitize_amount($_POST['bid']);
	if(isset($_POST['bid_anonymous'])) $bid_anonymous = $_POST['bid_anonymous'];
	if(isset($_POST['agreement'])) $agreement = $_POST['agreement'];
	$previous_bid = cp_auction_plugin_get_latest_bid($post->ID);
	$last_bid = $previous_bid;
	$latest_bid = cp_auction_plugin_get_latest_bid($post->ID, 'bid');
	if(( !empty( $cp_bid_increments ) ) && ( $my_type == 'normal' || $my_type == "reverse" )) {
	$last_bid = $last_bid + ($cp_bid_increments - 0.01);
	}
	if( !empty( $cp_bid_increments ) && $my_type == 'normal' && get_option('cp_auction_enable_freebids') == "yes" && get_option('cp_auction_last_freebids') != "yes" ) {
	$latest_bid = $latest_bid + ($cp_bid_increments - 0.01);
	}
	$error = 0;

	if ( $start_timer == "yes" ) {
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$todayDate + $ad_period $period"), true);
	update_post_meta( $post->ID, 'cp_sys_expire_date', $cp_sys_expire_date );
	update_post_meta( $post->ID, 'cp_end_date', strtotime($cp_sys_expire_date) );
	update_post_meta( $post->ID, 'cp_sys_ad_duration', $ad_period );
	}

	if(empty($bid_anonymous)) {
	$bid_anonymous = "no";
	}
	if( get_option( 'cp_auction_deactivate_cp_agreement' ) != "yes" && empty($agreement) ) $error = 1;
	elseif( $bid_anonymous == "yes" && get_option('cp_allow_anonymous'.$post->post_author) == "no" ) $error = 3;
	elseif( is_user_logged_in() != true ) $error = 4;
	elseif( $uid == $post->post_author ) $error = 5;
	elseif( !is_numeric($bid) ) $error = 6;
	elseif( $my_type == "normal" && get_option('cp_auction_enable_freebids') != "yes" ) {
	if( $bid > $last_bid )
	$error = 0;
	else
	$error = 7;
	}
	elseif( $my_type == "normal" && get_option('cp_auction_enable_freebids') == "yes" && get_option('cp_auction_last_freebids') != "yes" ) {
	if( $bid > $latest_bid )
	$error = 0;
	else
	$error = 7;
	}
	elseif( $my_type == "reverse" ) {
	if($bid < $last_bid)
	$error = 0;
	else
	$error = 8;
	}

	if( $error == 0 ) {

	$tm = time();
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$query = "INSERT INTO {$table_name} (pid, post_author, datemade, bid, last_bid, uid, anonymous, type) VALUES (%d, %d, %d, %f, %f, %d, %s, %s)"; 
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $tm, $bid, $previous_bid, $uid, $bid_anonymous, 'bid'));

	$highest_bid = cp_auction_plugin_get_highest_bid($post->ID);

	if( get_option('cp_auction_plugin_price_type') == "current" ) {
	update_post_meta( $post->ID, 'cp_price', $bid );
	}
	elseif( get_option('cp_auction_plugin_price_type') == "highest" ) {
	update_post_meta( $post->ID, 'cp_price', $highest_bid );
	}

	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$watchlist = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND aid = '$post->ID'", ARRAY_A);
	$result =  $wpdb->num_rows;

	if( $result < 1 ) {
	$table_watchlist = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$query = "INSERT INTO {$table_watchlist} (uid, aid) VALUES (%d, %d)"; 
	$wpdb->query($wpdb->prepare($query, $uid, $post->ID));
	}
	$cp_min_price = get_post_meta( $post->ID, 'cp_min_price', true );
	if( get_option('cp_auction_plugin_if_watchlist') == "yes" ) {
	cp_auction_watchlist_bid_notifications( $post->ID, $bid );
	}
	cp_auction_new_bids_posted( $post->ID, $post, $uid, $bid, $cp_min_price );

	appthemes_display_notice( 'success', __( 'Please wait while your bid is processed...', 'auctionPlugin' ) );
	$redirect = ''.get_permalink($post->ID).'?success=2';
	cp_auction_page_redirect($redirect);
	}

	if( $error == 1 ) appthemes_display_notice( 'error', __( 'You must agree that you have read our policies!','auctionPlugin' ) );
	elseif( $error == 3 ) appthemes_display_notice( 'error', __( 'Sorry, the auction author does not allow anonymous bids.','auctionPlugin' ) );
	elseif( $error == 4 ) appthemes_display_notice( 'error', __( 'You need to be logged in before bidding!', 'auctionPlugin' ) );
	elseif( $error == 5 ) appthemes_display_notice( 'error', __( 'You cannot bid on your own auction!', 'auctionPlugin' ) );
	elseif( $error == 6 ) appthemes_display_notice( 'error', __( 'Your bid needs to be numeric!', 'auctionPlugin' ) );
	elseif( $error == 7 ) appthemes_display_notice( 'error', sprintf(__( 'Your bid needs to be higher than the last bid plus %s bid increments.','auctionPlugin' ), cp_display_price( $cp_bid_increments, 'ad', false ) ) );
	elseif( $error == 8 ) appthemes_display_notice( 'error', __( 'Your bid needs to be lower than the last bid.','auctionPlugin' ) );
	}
	if( $success == 1 ) appthemes_display_notice( 'success', __( 'Your offer was registered!', 'auctionPlugin' ) );
	if( $success == 2 ) appthemes_display_notice( 'success', __( 'Your bid was registered!', 'auctionPlugin' ) );
	$confirm = get_option('cp_auction_confirmation_popup');

	$closed = end_auction($post->ID);
	$dt2 = strtotime(get_post_meta($post->ID,'cp_sys_expire_date',true));
	$offset = get_option('gmt_offset');
	if( $offset < 0 ) $a = "";
	if( $offset >= 0 ) $a = "+";

?>

<script type="text/javascript">
function saveScrollPositions(theForm) {
if(theForm) {
var scrolly = typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop;
var scrollx = typeof window.pageXOffset != 'undefined' ? window.pageXOffset : document.documentElement.scrollLeft;
theForm.scrollx.value = scrollx;
theForm.scrolly.value = scrolly;
}
}
</script>

    <script type="text/javascript">
function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
    </script>

		<div id="overlay">
     		<div>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
     		<?php if(get_option('cp_auction_plugin_cp_agreement') == "") {
     		if(( get_option('cp_version') < "3.3" )) {
          	echo get_option('cp_ads_tou_msg');
          	} elseif( get_option('cp_version') > "3.2.1" ) {
          	echo $cp_options->ads_tou_msg;
          	}
            	} else {
          	echo get_option('cp_auction_plugin_cp_agreement');
          	} ?>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
     		</div>
		</div>

	<div class="wrap_container">

	<div class="bids_wrap">

	<?php
	$last_bid = cp_auction_plugin_get_latest_bid($post->ID);
	if( ( ! in_array("single", $list) && $start_timer != "yes" ) || ( $start_timer == "yes" && $last_bid > $cp_start_price ) )  { ?>

<font color="navy"><strong><?php if($closed == 'yes') echo ''.__('Auction is closed.','auctionPlugin').'</strong></font>';
				else
				{ $i = 1; ?>

</font></strong><meta scheme="countdown<?php echo $i; ?>" name="event_msg" content="<?php _e('Auction is closed.', 'auctionPlugin'); ?>"><meta scheme="countdown<?php echo $i; ?>" name="d_unit" content=" <?php _e('day', 'auctionPlugin'); ?>"><meta scheme="countdown<?php echo $i; ?>" name="d_units" content=" <?php _e('days', 'auctionPlugin'); ?>"> <p class="member-title"><?php _e('The auction closes in', 'auctionPlugin'); ?> <font color="navy"><span id="countdown<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></font></p>

<div class="clear5"></div>

<div class="dotted"></div>

	<?php } } ?>

	<div class="clear5"></div>

            <?php
			$winner_bid = get_post_meta($post->ID, 'winner_bid', true);
			global $wpdb;
			$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
			$results = $wpdb->get_results("SELECT * FROM {$table_bids} WHERE pid = '$post->ID' AND (type = '' OR type = 'bid') ORDER BY datemade DESC");

			if( ! $results ) echo '<i>'.__('There are no posted bids!','auctionPlugin').'</i>';
			else
			{

				if( is_logged_in_user_owner($post) == true && ( get_option('cp_auction_plugin_auctiontype') == 'reverse' || $my_type == "reverse" ))
				echo '<table style="width: 100%" class="padded-table">';
				else
				echo '<table style="width: 100%" class="padded-table">';
				echo '<tr class="bid_tr">';
				echo '<td style="width: 20%; padding: 2px 2px 2px 2px;"> &nbsp; <strong>'.__('Bidder','auctionPlugin').'</strong></td>';
				echo '<td align="center" style="width: 20%"><strong>'.__('Date','auctionPlugin').'</strong></td>';
				echo '<td align="center" style="width: 20%">&nbsp;&nbsp;&nbsp;&nbsp; <strong>'.__('Bids','auctionPlugin').'</strong></td>';
				echo '<td align="right" style="width: 3%"> </td>';

				if( is_logged_in_user_owner($post) == true && ( get_option('cp_auction_plugin_auctiontype') == 'reverse' || $my_type == "reverse" ))
				echo '<td align="center" style="width: 20%"><strong>'.__('Winner?','auctionPlugin').'</strong> &nbsp; </td>';
				elseif( is_logged_in_user_owner($post) == true && ( get_option('cp_auction_plugin_auctiontype') == "reverse" || $my_type == "reverse" || get_option('cp_auction_enable_freebids') == "yes" ))
				echo '<td align="right" style="width: 20%"><strong>'.__('Action','auctionPlugin').'</strong> &nbsp; </td>';
				else
				echo '<td style="width: 20%"></td>';
				echo '</tr>';

				$rowclass = '';
				foreach ( $results as $res ) {
				$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

					$user = get_userdata($res->uid);

					if( $res->anonymous == "no" || $res->anonymous == "") $public = '<a title="'.$user->user_login.'" href="'.get_author_posts_url($res->uid).'">'.truncate($user->user_login, 8, '..').'</a>';
					else
					$public = ''.__('Anonymous','auctionPlugin').'';

					echo '<tr id="'.$res->id.'" class="bids_trs '.$rowclass.'">';
					echo '<td style="width: 20%; padding-left: 2px;">&nbsp; '.$public.'</td>';
					echo '<td align="center" style="width: 20%">'.date_i18n('d/m/y', $res->datemade, true).'</td>';
					echo '<td align="right" style="width: 20%">'.cp_display_price( $res->bid, 'ad', false ).'</td>';
					if(( is_logged_in_user_owner($post) == true && get_option('cp_auction_plugin_owner_delete') == "yes" ) || ( $user_can == true )) {
					echo '<td align="right" style="width: 3%"><a href="#" class="delete-bid" title="'.__('You should delete only faulty & fraudelent bids.', 'auctionPlugin').'" id="'.$res->id.'"><div class="deleteico"></div></a></td>';

					} elseif(($res->uid == $uid && get_option('cp_auction_plugin_bidder_delete') == "yes" ) || ( $user_can == true )) {
					echo '<td align="right" style="width: 3%"><a href="#" class="delete-bid" title="'.__('You should delete only faulty bids.', 'auctionPlugin').'" id="'.$res->id.'"><div class="deleteico"></div></a></td>';
					} else {
					echo '<td align="right" style="width: 3%"> </td>';
					}

					if( is_logged_in_user_owner($post) == true && ( get_option('cp_auction_plugin_auctiontype') == "reverse" || $my_type == "reverse" || get_option('cp_auction_enable_freebids') == "yes" )) {
						if($closed == 'yes') 
						{
							echo '<td>';
							if($winner_bid == $res->id) echo ''.__('Yes','auctionPlugin').'&nbsp; ';
							echo '</td>';						
						}
						else						
						echo '<td align="right" style="width: 20%"><a href="'.cp_auction_url($cpurl['winner'], '?_choose_winner=1', 'id='.$res->id.'').'">'.__('Accept','auctionPlugin').'</a>&nbsp; </td>';

					}
					else {
					echo '<td style="width: 20%"></td>';
					}
					echo '</tr>';

				}
				
				echo '</table>';
			}
			
			?>

</div><!-- /bids_wrap -->

                <form action="" method="post" onsubmit="return saveScrollPositions(this);">

                <div class="bid_wrap">
                <div class="item_detail bid">
                <div class="item_detail1"><label for="bid"><?php _e('Bid Amount:', 'auctionPlugin'); ?></label></div>
                <div class="item_detail2"><input type="text" size="8" name="bid" id="bid" value="" tabindex="1" style="width:92%; height:30px; padding:0 4px;" /></div>
		</div>

		<div class="clear10"></div>

		<?php if ( get_option( 'cp_auction_plugin_bid_anonymous' ) == "yes" ) { ?>
		<div class="item_detail bid">
                        <div class="item_detail1"><label for="no yes"><?php _e('Anonymous:', 'auctionPlugin'); ?></label></div>
                        <div class="item_detail2"><input type="radio" name="bid_anonymous" id="no" value="no" checked /> <label for="no"><?php _e('No', 'auctionPlugin'); ?></label>
		<input type="radio" name="bid_anonymous" id="yes" value="yes" /> <label for="yes"><?php _e('Yes', 'auctionPlugin'); ?></label></div>
                </div>
                <div class="clear5"></div>

		<?php } if ( get_option( 'cp_auction_deactivate_cp_agreement') != "yes" ) { ?>
		<div class="item_detail bid">
                        <div class="item_detail1"><a href="#" title="<?php _e('Click here to read our policies!', 'auctionPlugin'); ?>" onclick="overlay()"><label for="agreement"><?php _e('Agreement:', 'auctionPlugin'); ?></a></div>
                        <div class="item_detail2"><input type="checkbox" name="agreement" id="agreement" value="yes" tabindex="3" /> <label for="agreement"><?php _e('Yes I Agree.', 'auctionPlugin'); ?></label></div>
                </div>
                <?php } ?>

                <div class="clear5"></div>
		<input type="hidden" name="scrollx" id="scrollx" value="0" />
		<input type="hidden" name="scrolly" id="scrolly" value="0" />
                <input type="submit" name="bid_now" value="<?php _e('Place Bid', 'auctionPlugin'); ?>" class="mbtn btn_orange cp-width" tabindex="4" />
                <?php if( $bestoffer == true && in_array($my_type, $enabled) ) { ?> &nbsp; <input type="submit" name="best_offer" value="<?php _e('Make an Offer', 'auctionPlugin'); ?>" class="mbtn btn_orange ads_button" tabindex="5" /><?php } ?>
                </form>
                </div>

</div><!-- /wrap_container -->

<div class="clr"></div>

<?php cp_auction_after_bidder_box(); ?>

	<div class="clr"></div>
	<div class="text-right"><?php if( function_exists('cp_auction_buy_now_button_standard') ) echo cp_auction_buy_now_button_standard( $post->ID ); ?></div>
	<div class="clr"></div>

<?php
$scrollx = 0;
$scrolly = 0;
if(!empty($_REQUEST['scrollx'])) {
$scrollx = $_REQUEST['scrollx'];
}
if(!empty($_REQUEST['scrolly'])) {
$scrolly = $_REQUEST['scrolly'];
}
?>

<script type="text/javascript">
window.scrollTo(<?php echo "$scrollx" ?>, <?php echo "$scrolly" ?>);
</script>

<?php
}
}
global $post;
if ( $post )
$imported = get_post_meta( $post->ID, 'a2pItemID', true );
else $imported = false;
if( get_option('cp_auction_use_hooks') == "yes" && get_option('cp_auction_enable_bid_widget') != "yes" && $imported == false )
add_action( 'cp_auction_bidder_box', 'cp_auction_bidder_box_code' );
elseif( get_option('cp_auction_use_hooks') != "yes" && get_option('cp_auction_enable_bid_widget') != "yes" && $imported == false )
add_action( 'appthemes_after_post_content', 'cp_auction_bidder_box_code' );


/*
|--------------------------------------------------------------------------
| ADD THE AUCTION BIDDER BOX ON SINGLE ADS SIDEBAR AS AN TABBED WIDGET
|--------------------------------------------------------------------------
*/

function cp_auction_bidder_box_widget_code() {

?>

<style type="text/css">
p.success, div.success {
	margin:0 !important;
	font-weight: bold;
}
</style>

<?php

	global $current_user, $post, $cp_options, $cpurl, $wpdb;

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

	if( ! is_singular(APP_POST_TYPE) && $my_type != "normal" && $my_type != "reverse" )
		return;

	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$user_can = current_user_can( 'manage_options' );
	$pos = get_option('cp_auction_timer');
	$list = explode(',', $pos['cp_disable_timer']);

	$success = ""; if( isset($_GET['success']) ) $success = $_GET['success'];

	$option = get_option('cp_auction_fields');
	$table_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_bestoffer'");
	if( $row ) {
	$fname = $row->field_name;
	$enabled = explode(',', $option[$fname]);
	}

	$verified = true;
	if(( get_option('cp_auction_plugin_verification') > 0 ) && ( get_option('cp_auction_plugin_upl_verification') != "yes" )) {
	$user_verified = get_user_meta( $uid, 'cp_auction_account_verified', true );
	if( $user_verified ) $verified = true;
	if( empty( $user_verified )) $verified = false;
	}

	$cp_bid_increments = get_post_meta($post->ID, 'cp_bid_increments', true);
	$cp_start_price = get_post_meta($post->ID, 'cp_start_price', true);
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	$end_date = strtotime(get_post_meta($post->ID, 'cp_sys_expire_date', true));
	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );
	$boffer_widget = get_option( 'aws_bo_use_widget' );
	$start_timer = get_option( 'cp_auction_start_timer' );
	$todayDate = current_time('mysql');
	$ad_period = get_post_meta( $post->ID, 'cp_auction_listing_duration', true );
	$period = get_option( 'cp_auction_period_type' );

	if(isset($_POST['best_offer'])) {

	$error = 0;
	$sold = get_post_meta( $post->ID, 'cp_ad_sold', true );
	if(isset($_POST['bid_anonymous'])) $bid_anonymous = $_POST['bid_anonymous'];
	if(isset($_POST['agreement'])) $agreement = $_POST['agreement'];
	if(isset($_POST['bid'])) $bid = cp_auction_sanitize_amount($_POST['bid']);

	$tm = time();
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$latest = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$uid' AND type = 'bestoffer'");
	$id = $latest->id;
	$boff_usage = $latest->boff_usage;
	$upd_usage = $boff_usage + 1;

	if(empty($bid_anonymous)) {
	$bid_anonymous = "no";
	}
	if( get_option( 'cp_auction_deactivate_cp_agreement' ) != "yes" && empty($agreement) ) $error = 1;
	elseif( $bid_anonymous == "yes" && get_option('cp_allow_anonymous'.$post->post_author) == "no" ) $error = 3;
	elseif( is_user_logged_in() != true ) $error = 4;
	elseif( $uid == $post->post_author ) $error = 5;
	elseif( !is_numeric($bid) ) $error = 6;
	elseif( $boff_usage >= get_option( 'aws_bo_no_best_offer' ) ) $error = 7;
	elseif( $sold == "yes" ) $error = 8;

	if( $error == 0 ) {

	if( $latest ) {
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('datemade' => $tm, 'bid' => $bid, 'boff_usage' => $upd_usage, 'latest_offer' => $bid, 'acceptance' => '', 'date_type' => 'datemade'), $where_array);
	} else {
	$query = "INSERT INTO {$table_name} (pid, datemade, bid, uid, anonymous, type, boff_usage, latest_offer, date_type) VALUES (%d, %d, %f, %d, %s, %s, %d, %f, %s)"; 
	$wpdb->query($wpdb->prepare($query, $pid, $tm, $bid, $uid, $bid_anonymous, 'bestoffer', '1', $bid, 'datemade'));
	}
	aws_bo_new_boffer_was_posted_email( $pid, $post, $uid, $bid );

	appthemes_display_notice( 'success', __( 'Please wait while your offer is processed...', 'auctionPlugin' ) );
	$redirect = ''.get_permalink($post->ID).'?success=1';
	cp_auction_page_redirect($redirect);
	}
	if( $error == 1 ) appthemes_display_notice( 'error', __( 'You must agree that you have read our policies!','auctionPlugin' ) );
	elseif( $error == 3 ) appthemes_display_notice( 'error', __( 'Sorry, the author does not allow anonymous offers.','auctionPlugin' ) );
	elseif( $error == 4 ) appthemes_display_notice( 'error', __( 'You must be logged in to submit your best offer!', 'auctionPlugin' ) );
	elseif( $error == 5 ) appthemes_display_notice( 'error', __( 'You can not post offers on your own ads!', 'auctionPlugin' ) );
	elseif( $error == 6 ) appthemes_display_notice( 'error', __( 'Your best offer needs to be a numeric value!', 'auctionPlugin' ) );
	elseif( $error == 7 ) appthemes_display_notice( 'error', __( 'Sorry, you do not have permission to post more best offers!','auctionPlugin' ) );
	elseif( $error == 8 ) appthemes_display_notice( 'error', __( 'Sorry but this product is already sold!','auctionPlugin' ) );
}

	if(isset($_POST['bid_now'])) {

	$bid     = cp_auction_sanitize_amount($_POST['bid']);
	if(isset($_POST['bid_anonymous'])) $bid_anonymous = $_POST['bid_anonymous'];
	if(isset($_POST['agreement'])) $agreement = $_POST['agreement'];
	$previous_bid = cp_auction_plugin_get_latest_bid($post->ID);
	$last_bid = $previous_bid;
	$latest_bid = cp_auction_plugin_get_latest_bid($post->ID, 'bid');
	if(( !empty( $cp_bid_increments ) ) && ( $my_type == 'normal' || $my_type == "reverse" )) {
	$last_bid = $last_bid + ($cp_bid_increments - 0.01);
	}
	if( !empty( $cp_bid_increments ) && $my_type == 'normal' && get_option('cp_auction_enable_freebids') == "yes" && get_option('cp_auction_last_freebids') != "yes" ) {
	$latest_bid = $latest_bid + ($cp_bid_increments - 0.01);
	}
	$error = 0;

	if(empty($bid_anonymous)) {
	$bid_anonymous = "no";
	}
	if( get_option( 'cp_auction_deactivate_cp_agreement' ) != "yes" && empty($agreement) ) $error = 1;
	elseif( $bid_anonymous == "yes" && get_option('cp_allow_anonymous'.$post->post_author) == "no" ) $error = 3;
	elseif( is_user_logged_in() != true ) $error = 4;
	elseif( $uid == $post->post_author ) $error = 5;
	elseif( !is_numeric($bid) ) $error = 6;
	elseif( $my_type == "normal" && get_option('cp_auction_enable_freebids') != "yes" ) {
	if( $bid > $last_bid )
	$error = 0;
	else
	$error = 7;
	}
	elseif( $my_type == "normal" && get_option('cp_auction_enable_freebids') == "yes" && get_option('cp_auction_last_freebids') != "yes" ) {
	if( $bid > $latest_bid )
	$error = 0;
	else
	$error = 7;
	}
	elseif( $my_type == "reverse" ) {
	if($bid < $last_bid)
	$error = 0;
	else
	$error = 8;
	}

	if( $error == 0 ) {

	if ( $start_timer == "yes" ) {
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$todayDate + $ad_period $period"), true);
	update_post_meta( $post->ID, 'cp_sys_expire_date', $cp_sys_expire_date );
	update_post_meta( $post->ID, 'cp_end_date', strtotime($cp_sys_expire_date) );
	update_post_meta( $post->ID, 'cp_sys_ad_duration', $ad_period );
	}

	$tm = time();
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$query = "INSERT INTO {$table_name} (pid, post_author, datemade, bid, last_bid, uid, anonymous, type) VALUES (%d, %d, %d, %f, %f, %d, %s, %s)"; 
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $tm, $bid, $previous_bid, $uid, $bid_anonymous, 'bid'));

	$highest_bid = cp_auction_plugin_get_highest_bid($post->ID);

	if( get_option('cp_auction_plugin_price_type') == "current" ) {
	update_post_meta( $post->ID, 'cp_price', $bid );
	}
	elseif( get_option('cp_auction_plugin_price_type') == "highest" ) {
	update_post_meta( $post->ID, 'cp_price', $highest_bid );
	}

	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$watchlist = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND aid = '$post->ID'", ARRAY_A);
	$result =  $wpdb->num_rows;

	if( $result < 1 ) {
	$table_watchlist = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$query = "INSERT INTO {$table_watchlist} (uid, aid) VALUES (%d, %d)"; 
	$wpdb->query($wpdb->prepare($query, $uid, $post->ID));
	}
	$cp_min_price = get_post_meta( $post->ID, 'cp_min_price', true );
	if( get_option('cp_auction_plugin_if_watchlist') == "yes" ) {
	cp_auction_watchlist_bid_notifications( $post->ID, $bid );
	}
	cp_auction_new_bids_posted( $post->ID, $post, $uid, $bid, $cp_min_price );

	appthemes_display_notice( 'success', __( 'Please wait while your bid is processed...', 'auctionPlugin' ) );
	$redirect = ''.get_permalink($post->ID).'?success=2';
	cp_auction_page_redirect($redirect);
	}

	if( $error == 1 ) appthemes_display_notice( 'error', __( 'You must agree that you have read our policies!','auctionPlugin' ) );
	elseif( $error == 3 ) appthemes_display_notice( 'error', __( 'Sorry, the auction author does not allow anonymous bids.','auctionPlugin' ) );
	elseif( $error == 4 ) appthemes_display_notice( 'error', __( 'You need to be logged in before bidding!', 'auctionPlugin' ) );
	elseif( $error == 5 ) appthemes_display_notice( 'error', __( 'You cannot bid on your own auction!', 'auctionPlugin' ) );
	elseif( $error == 6 ) appthemes_display_notice( 'error', __( 'Your bid needs to be numeric!', 'auctionPlugin' ) );
	elseif( $error == 7 ) appthemes_display_notice( 'error', sprintf(__( 'Your bid needs to be higher than the last bid plus %s bid increments.','auctionPlugin' ), cp_display_price( $cp_bid_increments, 'ad', false ) ) );
	elseif( $error == 8 ) appthemes_display_notice( 'error', __( 'Your bid needs to be lower than the last bid.','auctionPlugin' ) );
	}

	if( $success == 1 ) appthemes_display_notice( 'success', __( 'Your offer was registered!', 'auctionPlugin' ) );
	elseif( $success == 2 ) appthemes_display_notice( 'success', __( 'Your bid was registered!', 'auctionPlugin' ) );

	$confirm = get_option('cp_auction_confirmation_popup');

	$closed = end_auction($post->ID);
	$dt2 = strtotime(get_post_meta($post->ID,'cp_sys_expire_date',true));
	$offset = get_option('gmt_offset');
	if( $offset < 0 ) $a = "";
	if( $offset >= 0 ) $a = "+";
?>

<script type="text/javascript">
function saveScrollPositions(theForm) {
if(theForm) {
var scrolly = typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop;
var scrollx = typeof window.pageXOffset != 'undefined' ? window.pageXOffset : document.documentElement.scrollLeft;
theForm.scrollx.value = scrollx;
theForm.scrolly.value = scrolly;
}
}
</script>

    <script type="text/javascript">
function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
    </script>

		<div id="overlay">
     		<div>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
     		<?php if(get_option('cp_auction_plugin_cp_agreement') == "") {
     		if(( get_option('cp_version') < "3.3" )) {
          	echo get_option('cp_ads_tou_msg');
          	} elseif( get_option('cp_version') > "3.2.1" ) {
          	echo $cp_options->ads_tou_msg;
          	}
            	} else {
          	echo get_option('cp_auction_plugin_cp_agreement');
          	} ?>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
     		</div>
		</div>

	<div class="clr"></div>

<?php if ( $uid != $post->post_author ) { ?>

                <form action="" method="post" onsubmit="return saveScrollPositions(this);">

                <div class="bid_wrap_widget">

	<?php

	echo '<h2 class="dotted">'.__('Đặt giá cho sản phẩm này','auctionPlugin').'</h2>';

	$last_bid = cp_auction_plugin_get_latest_bid($post->ID);
	if( ( ! in_array("bids", $list) && $start_timer != "yes" ) || ( $start_timer == "yes" && $last_bid > $cp_start_price ) )  { ?>

<font color="navy"><strong><?php if($closed == 'yes') echo ''.__('THIS AUCTION HAS BEEN CLOSED','auctionPlugin').'</strong></font>';
				else
				{ $i = 1; ?>

</font></strong><meta scheme="bidwidget<?php echo $i; ?>" name="event_msg" content="<?php _e('THIS AUCTION HAS BEEN CLOSED', 'auctionPlugin'); ?>"><meta scheme="bidwidget<?php echo $i; ?>" name="d_unit" content=" <?php _e('day', 'auctionPlugin'); ?>"><meta scheme="bidwidget<?php echo $i; ?>" name="d_units" content=" <?php _e('days', 'auctionPlugin'); ?>"> <p class="member-title"><?php _e('The auction closes in', 'auctionPlugin'); ?> <font color="navy"><span id="bidwidget<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></font></p>

<div class="clear5"></div>

<div class="dotted"></div>

	<?php } } ?>

	<div class="clear5"></div>

                <div class="item_detail bid">
                <div class="item_detail1"> &nbsp; <label for="bid"><?php _e('Đặt:', 'auctionPlugin'); ?></label></div>
                <div class="item_detail2"><input type="text" size="8" name="bid" id="bid" value="" tabindex="1" style="width:94%; height:30px; padding:0 4px;" /></div>
		</div>

		<div class="clear10"></div>

		<?php if ( get_option('cp_auction_plugin_bid_anonymous') == "yes" ) { ?>
		<div class="item_detail bid">
                        <div class="item_detail1"> &nbsp; <label for="no yes"><?php _e('Khách:', 'auctionPlugin'); ?></label></div>
                        <div class="item_detail2"><input type="radio" name="bid_anonymous" id="no" value="no" checked /> <label for="no"><?php _e('Không', 'auctionPlugin'); ?></label>
		<input type="radio" name="bid_anonymous" id="yes" value="yes" /> <label for="yes"><?php _e('Có', 'auctionPlugin'); ?></label></div>
                </div>

	<div class="clear5"></div>

		<?php } if ( get_option( 'cp_auction_deactivate_cp_agreement') != "yes" ) { ?>
		<div class="item_detail bid">
                        <div class="item_detail1"> &nbsp; <a href="#" title="<?php _e('Click here to read our policies!', 'auctionPlugin'); ?>" onclick="overlay()"><label for="agreement"><?php _e('Đồng ý đặt:', 'auctionPlugin'); ?></a></div>
                        <div class="item_detail2"><input type="checkbox" name="agreement" id="agreement" value="yes" tabindex="3" /> <label for="agreement"><?php _e('Tôi đồng ý.', 'auctionPlugin'); ?></label></div>
                </div>
		<?php } ?>

                <div class="clear5"></div>
		<input type="hidden" name="scrollx" id="scrollx" value="0" />
		<input type="hidden" name="scrolly" id="scrolly" value="0" />
                <input type="submit" name="bid_now" value="<?php _e('Đặt', 'auctionPlugin'); ?>" class="mbtn btn_orange cp-width" tabindex="4" />
                <?php if( $bestoffer == true && $boffer_widget == "yes" && in_array($my_type, $enabled) ) { ?>&nbsp;&nbsp;&nbsp;<input type="submit" name="best_offer" value="<?php _e('Make an Offer', 'auctionPlugin'); ?>" class="mbtn btn_orange ads_button" tabindex="5" /><?php } ?>
                </form>
                </div>

<?php } ?><!-- / if not post author -->

<?php
	$scrollx = 0;
	$scrolly = 0;
	if(!empty($_REQUEST['scrollx'])) {
	$scrollx = $_REQUEST['scrollx'];
	}
	if(!empty($_REQUEST['scrolly'])) {
	$scrolly = $_REQUEST['scrolly'];
	}

?>

<script type="text/javascript">
window.scrollTo(<?php echo "$scrollx" ?>, <?php echo "$scrolly" ?>);
</script>

	<div class="clear5"></div>

	<div class="bids_wrap_widget">

        <?php
			$winner_bid = get_post_meta($post->ID, 'winner_bid', true);
			global $wpdb;
			$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
			$results = $wpdb->get_results("SELECT * FROM {$table_bids} WHERE pid = '$post->ID' AND (type = '' OR type = 'bid') ORDER BY datemade DESC");

			echo '<h2 class="dotted">'.__('Đặt cược mới nhất:','auctionPlugin').'</h2>';
			
			if( ! $results ) echo '<i>'.__('Chưa có ai đặt!','auctionPlugin').'</i>';
			else
			{

				if( is_logged_in_user_owner($post) == true && ( get_option('cp_auction_plugin_auctiontype') == 'reverse' || $my_type == "reverse" ))
				echo '<table style="width: 100%" class="padded-table">';
				else
				echo '<table style="width: 100%" class="padded-table">';
				echo '<tr class="bid_tr">';
				echo '<td style="width: 20%; padding: 2px 2px 2px 2px;"> &nbsp; <strong>'.__('Bidder','auctionPlugin').'</strong></td>';
				echo '<td align="center" style="width: 20%"><strong>'.__('Date','auctionPlugin').'</strong></td>';
				echo '<td align="center" style="width: 20%">&nbsp;&nbsp;&nbsp;&nbsp; <strong>'.__('Bids','auctionPlugin').'</strong></td>';
				echo '<td align="right" style="width: 3%"> </td>';

				if( is_logged_in_user_owner($post) == true && ( get_option('cp_auction_plugin_auctiontype') == 'reverse' || $my_type == "reverse" ))
				echo '<td align="center" style="width: 20%"><strong>'.__('Winner?','auctionPlugin').'</strong> &nbsp; </td>';
				elseif( is_logged_in_user_owner($post) == true && ( get_option('cp_auction_plugin_auctiontype') == "reverse" || $my_type == "reverse" || get_option('cp_auction_enable_freebids') == "yes" ))
				echo '<td align="right" style="width: 20%"><strong>'.__('Action','auctionPlugin').'</strong> &nbsp; </td>';
				else
				echo '<td style="width: 20%"></td>';
				echo '</tr>';

				$rowclass = '';
				foreach ( $results as $res ) {
				$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

					$user = get_userdata($res->uid);

					if(($res->anonymous == "no") || ($res->anonymous == "")) $public = '<a title="'.$user->user_login.'" href="'.get_author_posts_url($res->uid).'">'.truncate($user->user_login, 8, '..').'</a>';
					else
					$public = ''.__('Anonymous','auctionPlugin').'';
					
					echo '<tr id="'.$res->id.'" class="bids_tr '.$rowclass.'">';
					echo '<td style="width: 20%;">&nbsp; '.$public.'</td>';
					echo '<td align="center" style="width: 20%">'.date_i18n('d/m/y', $res->datemade, true).'</td>';
					echo '<td align="right" style="width: 20%">'.cp_display_price( $res->bid, 'ad', false ).'</td>';
					if(( is_logged_in_user_owner($post) == true && get_option('cp_auction_plugin_owner_delete') == "yes" ) || ( $user_can == true )) {
					echo '<td align="right" style="width: 3%"><a href="#" class="delete-bid" title="'.__('You should delete only faulty & fraudelent bids.', 'auctionPlugin').'" id="'.$res->id.'"><div class="deleteico"></div></a></td>';

					} elseif(($res->uid == $uid && get_option('cp_auction_plugin_bidder_delete') == "yes" ) || ( $user_can == true )) {
					echo '<td align="right" style="width: 3%"><a href="#" class="delete-bid" title="'.__('You should delete only faulty bids.', 'auctionPlugin').'" id="'.$res->id.'"><div class="deleteico"></div></a></td>';
					} else {
					echo '<td align="right" style="width: 3%"> </td>';
					}

					if( is_logged_in_user_owner($post) == true && ( get_option('cp_auction_plugin_auctiontype') == "reverse" || $my_type == "reverse" || get_option('cp_auction_enable_freebids') == "yes" )) {
						if($closed == 'yes') 
						{
							echo '<td>';
							if($winner_bid == $res->id) echo ''.__('Yes','auctionPlugin').'&nbsp; ';
							echo '</td>';						
						}
						else						
						echo '<td align="right" style="width: 20%"><a href="'.cp_auction_url($cpurl['winner'], '?_choose_winner='.$res->id.'', 'id='.$res->id.'').'">'.__('Accept','auctionPlugin').'</a>&nbsp; </td>';

					}
					else {
					echo '<td style="width: 20%"></td>';
					}
					echo '</tr>';

				}

				echo '</table>';

			} ?>

</div><!-- /bids_wrap -->

<?php if( get_option('cp_auction_enable_auction_buy_widget') == "yes" ) {
echo '<div class="clear5"></div>';
if( function_exists('cp_auction_buy_now_button_standard') ) echo cp_auction_buy_now_button_standard( $post->ID );
} ?>

	<div class="clear10"></div>

<?php
}
add_action( 'cp_auction_bidder_box_widget', 'cp_auction_bidder_box_widget_code' );


/*
|--------------------------------------------------------------------------
| ADD THE BIDDER BOX MENU ON SINGLE ADS SIDEBAR AS AN TABBED WIDGET
|--------------------------------------------------------------------------
*/

function cp_auction_bidder_box_tabbed_code() {

	if( ! is_singular(APP_POST_TYPE) )
		return;

	global $post, $gmap_active;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$allowdeals = get_post_meta( $post->ID, 'cp_allow_deals', true );
	$allow_deals = get_localized_field_values('cp_allow_deals', $allowdeals);
	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	if( $post->post_status == "publish" && $closed != "yes" ) $display = true;
	else $display = false;
	$no_map = get_option('cp_auction_remove_map_tab');
	$sarray = array( 'twinpress_classifieds', 'phoenix', 'citrus-night', 'eldorado', 'multiport', 'rondell' );

	if( get_option('stylesheet') == "jibo" ) { ?>
	<ul class="tabmenu">
	<?php } else { ?>
	<ul class="tabnavig">
	<?php } ?>
	<?php if(( get_option('cp_auction_enable_bid_widget') == "yes" && $display ) && ( $my_type == "normal" || $my_type == "reverse" )) { ?>
	<li><a href="#priceblock0"><span class="big"><?php _e( 'Bids', 'auctionPlugin' ); ?></span></a></li>
	<?php } elseif(( get_option('cp_auction_enable_wanted_widget') == "yes" && $allow_deals == "Yes" && $display ) && ( empty( $my_type ) || $my_type == 'wanted' || $my_type == 'classified' )) { ?>
	<li><a href="#priceblock0"><span class="big"><?php _e( 'Offer', 'auctionPlugin' ); ?></span></a></li>
	<?php } elseif(( get_option('aws_bo_use_widget') == "yes" && $bestoffer == true && $display ) && ( empty( $my_type ) || $my_type == 'classified' )) { ?>
	<li><a href="#priceblock0"><span class="big"><?php _e( 'Offer', 'auctionPlugin' ); ?></span></a></li>
	<?php } if ( $no_map != "yes" && $gmap_active && ! in_array( get_option('stylesheet'), $sarray ) ) { ?>
	<li><a href="#priceblock1"><span class="big"><?php _e( 'Bản đồ', 'auctionPlugin' ); ?></span></a></li>
	<?php } else {
	$gmap_active = $gmap_active;
	} ?>
	<li><a href="#priceblock2"><span class="big"><?php _e( 'Liên hệ', 'auctionPlugin' ); ?></span></a></li>
	<li><a href="#priceblock3"><span class="big"><?php _e( 'Tác giả', 'auctionPlugin' ); ?></span></a></li>
	</ul>

	<?php if(( get_option('cp_auction_enable_bid_widget') == "yes" && $display ) && ( $my_type == "normal" || $my_type == "reverse" )) { ?>

	<script type="text/javascript" src="<?php echo cp_auction_plugin_url(); ?>/js/countdown-bidwidget.js" defer="defer"></script>

	<div id="priceblock0">

	<div class="clr"></div>

	<div class="singletab">

	<?php cp_auction_bidder_box_widget(); ?>

	</div><!-- /singletab -->

	</div>
<?php
} elseif(( get_option('cp_auction_enable_wanted_widget') == "yes" && $allow_deals == "Yes" && $display ) && ( empty( $my_type ) || $my_type == 'wanted' || $my_type == 'classified' )) { ?>

	<div id="priceblock0">

	<div class="clr"></div>

	<div class="singletab">

	<?php cp_auction_wanted_box_widget(); ?>

	</div><!-- /singletab -->

	</div>
<?php
} elseif(( get_option('aws_bo_use_widget') == "yes" && $bestoffer == true && $display ) && ( empty( $my_type ) || $my_type == 'classified' )) { ?>

	<div id="priceblock0">

	<div class="clr"></div>

	<div class="singletab">

	<?php if( function_exists('aws_best_offer_box') ) aws_best_offer_box(); ?>

	</div><!-- /singletab -->

	</div>
<?php
}
}
add_action( 'cp_auction_bidder_box_tabbed', 'cp_auction_bidder_box_tabbed_code' );


/*
|--------------------------------------------------------------------------
| ADD THE WANTED BIDDER BOX ON SINGLE ADS PAGE
|--------------------------------------------------------------------------
*/

function cp_auction_wanted_box_code() {

	global $current_user, $post, $cp_options, $cpurl;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

	if( get_option('cp_auction_enable_wanted_loop') != "yes" && ! is_singular(APP_POST_TYPE) || $my_type != "wanted" )
		return;

	$allowdeals = get_post_meta( $post->ID, 'cp_allow_deals', true );
	$allow_deals = get_localized_field_values('cp_allow_deals', $allowdeals);
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	$end_date = strtotime(get_post_meta($post->ID, 'cp_sys_expire_date', true));

	if ( $allow_deals == "Yes" && $closed != "yes" ) {

	if(isset($_POST['bid_now']) && isset($_POST['post_id'])) {

	if( $_POST['post_id'] != $post->ID )
		return;
	?>

	<div id="bid" class="clear50"></div>

	<?php
	$bid     = cp_auction_sanitize_amount($_POST['bid']);
	if(isset($_POST['message'])) $message = stripslashes(html_entity_decode($_POST['message']));
	if(isset($_POST['bid_anonymous'])) $bid_anonymous = $_POST['bid_anonymous'];
	if(isset($_POST['agreement'])) $agreement = $_POST['agreement'];
	$previous_bid = cp_auction_plugin_get_latest_bid($post->ID);

	$error = 0;

	if(empty($bid_anonymous)) {
	$bid_anonymous = "no";
	}
	if( get_option( 'cp_auction_deactivate_cp_agreement' ) != "yes" && empty($agreement) ) $error = 1;
	elseif( $bid_anonymous == "yes" && get_option('cp_allow_anonymous'.$post->post_author) == "no" ) $error = 3;
	elseif( is_user_logged_in() != true ) $error = 4;
	elseif( $uid == $post->post_author ) $error = 5;
	elseif( !is_numeric($bid) ) $error = 6;
	elseif( empty($message) ) $error = 7;

	if( $error == 0 ) {

	$tm = time();
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$check = $wpdb->get_results("SELECT * FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$uid' AND message = '$message'");
	if( ! $check ) {
	$querye = "INSERT INTO {$table_name} (pid, post_author, datemade, bid, last_bid, uid, message, anonymous, type) VALUES (%d, %d, %d, %f, %f, %d, %s, %s, %s)"; 
	$wpdb->query($wpdb->prepare($querye, $post->ID, $post->post_author, $tm, $bid, $previous_bid, $uid, $message, $bid_anonymous, 'wanted'));
	}
	$table_watchlist = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$watchlist = $wpdb->get_results("SELECT * FROM {$table_watchlist} WHERE uid = '$uid' AND aid = '$post->ID' AND type = 'wanted'", ARRAY_A);
	$result =  $wpdb->num_rows;

	if( $result < 1 ) {
	$table_watchlist = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$query = "INSERT INTO {$table_watchlist} (uid, aid, type) VALUES (%d, %d, %s)"; 
	$wpdb->query($wpdb->prepare($query, $uid, $post->ID, 'wanted'));
	}
	$cp_max_price = get_post_meta( $post->ID, 'cp_max_price', true );
	if( get_option('cp_auction_plugin_if_watchlist') == "yes" && ! $check ) {
	cp_auction_watchlist_bid_notifications( $post->ID, $bid, 'wanted' );
	}
	if( ! $check ) {
	cp_auction_new_wanted_bid_posted( $post->ID, $post, $uid, $message, $bid, $cp_max_price );
	}
	appthemes_display_notice( 'success', __( 'Please wait while your offer is processed...', 'auctionPlugin' ) );
	if( get_option('cp_auction_enable_wanted_loop') == "yes" ) {
	$redirect = ''.get_bloginfo('wpurl').'?pid='.$post->ID.'&success=1';
	} else {
	$redirect = ''.get_permalink($post->ID).'?success=1';
	}
	cp_auction_page_redirect($redirect);
	}

	if( $error == 1 ) appthemes_display_notice( 'error', __( 'You must agree that you have read our policies!','auctionPlugin' ) );
	elseif( $error == 3 ) appthemes_display_notice( 'error', __( 'Sorry, the ad author does not allow anonymous offers.','auctionPlugin' ) );
	elseif( $error == 4 ) appthemes_display_notice( 'error', __( 'You need to be logged in before you can post any offers!', 'auctionPlugin' ) );
	elseif( $error == 5 ) appthemes_display_notice( 'error', __( 'You cannot post any offers on your own ads!', 'auctionPlugin' ) );
	elseif( $error == 6 ) appthemes_display_notice( 'error', __( 'Your offer needs to be of a numeric value!', 'auctionPlugin' ) );
	elseif( $error == 7 ) appthemes_display_notice( 'error', __( 'Please enter a product description!', 'auctionPlugin' ) );
	}
	if( get_option('cp_auction_enable_wanted_loop') != "yes" ) {
	if( isset($_GET['success']) == 1 ) appthemes_display_notice( 'success', __( 'Your offer was registered!', 'auctionPlugin' ) );
	}
	$confirm = get_option('cp_auction_confirmation_popup');

?>

<script type="text/javascript">
function saveScrollPositions(theForm) {
if(theForm) {
var scrolly = typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop;
var scrollx = typeof window.pageXOffset != 'undefined' ? window.pageXOffset : document.documentElement.scrollLeft;
theForm.scrollx.value = scrollx;
theForm.scrolly.value = scrolly;
}
}
</script>

    <script type="text/javascript">
function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
    </script>

		<div id="overlay">
     		<div>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
     		<?php if(get_option('cp_auction_plugin_cp_agreement') == "") {
     		if(( get_option('cp_version') < "3.3" )) {
          	echo get_option('cp_ads_tou_msg');
          	} elseif( get_option('cp_version') > "3.2.1" ) {
          	echo $cp_options->ads_tou_msg;
          	}
            	} else {
          	echo get_option('cp_auction_plugin_cp_agreement');
          	} ?>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
     		</div>
		</div>

<script type="text/javascript">
function msg(id) {
	el = document.getElementById("msg"+id);
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>

	<div class="wrap_container">

	<div class="clr"></div>

	<h3 class="description-area"><?php _e('Please send me your offer', 'auctionPlugin'); ?></h3>

	<div class="clear5"></div>

	<div class="bids_wrap">

	<div class="clear10"></div>

            <?php
			$winner_bid = get_post_meta($post->ID, 'winner_bid', true);

			global $wpdb;
			$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
			$offers = $wpdb->get_results("SELECT * FROM {$table_bids} WHERE pid='$post->ID' AND type='wanted' ORDER BY datemade desc");

			if( ! $offers ) echo '<div class="page_info">'.nl2br(get_option('cp_auction_plugin_wanted_info')).'</div><div class="clear5"></div><div class="dotted"></div><br /><i>'.__('There are currently no offers!','auctionPlugin').'</i>';
			else
			{
				echo '<div class="page_info">'.nl2br(get_option('cp_auction_plugin_wanted_info')).'</div><div class="clear10"></div>';
				if( is_logged_in_user_owner($post) == true )
				echo '<table style="width: 100%" class="padded-table">';
				else
				echo '<table style="width: 100%" class="padded-table">';
				echo '<tr class="bid_tr">';
				echo '<td style="width: 20%; padding: 2px 2px 2px 2px;"> &nbsp; <strong>'.__('User','auctionPlugin').'</strong></td>';
				echo '<td align="center" style="width: 20%"><strong>'.__('Date','auctionPlugin').'</strong></td>';
				echo '<td align="center" style="width: 20%">&nbsp;&nbsp;&nbsp;&nbsp; <strong>'.__('Offers','auctionPlugin').'</strong></td>';
				echo '<td align="right" style="width: 3%"> </td>';

				if( is_logged_in_user_owner($post) == true )
				echo '<td align="center" style="width: 20%"><strong>'.__('Accept?','auctionPlugin').'</strong> &nbsp; </td>';
				else
				echo '<td style="width: 20%"></td>';
				echo '</tr>';

				$rowclass = '';
				foreach ( $offers as $row ) {
				$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

					$user = get_userdata($row->uid);

					if(($row->anonymous == "no") || ($row->anonymous == "")) $public = '<a title="'.$user->user_login.'" href="'.get_author_posts_url($row->uid).'">'.truncate($user->user_login, 8, '..').'</a>';
					else
					$public = ''.__('Anonymous','auctionPlugin').'';

					echo '<tr id="'.$row->id.'" class="bids_tr '.$rowclass.'">';
					echo '<td style="width: 20%; padding-left: 2px;">&nbsp; '.$public.'</td>';
					echo '<td align="center" style="width: 20%">'.date_i18n('d/m/y', $row->datemade, true).'</td>';
					echo '<td align="right" style="width: 20%">'.cp_display_price( $row->bid, 'ad', false ).'</td>';
					if(( is_logged_in_user_owner($post) == true && get_option('cp_auction_plugin_owner_delete') == "yes" ) || ( $user_can == true )) {
					echo '<td align="right" style="width: 3%"><a href="#" class="delete-bid" title="'.__('You should delete only faulty & fraudelent bids.', 'auctionPlugin').'" id="'.$row->id.'"><div class="deleteico"></div></a></td>';

					} elseif(($row->uid == $uid && get_option('cp_auction_plugin_bidder_delete') == "yes" ) || ( $user_can == true )) {
					echo '<td align="right" style="width: 3%"><a href="#" class="delete-bid" title="'.__('You should delete only faulty bids.', 'auctionPlugin').'" id="'.$row->id.'"><div class="deleteico"></div></a></td>';
					} else {
					echo '<td align="right" style="width: 3%"> </td>';
					}

					if( is_logged_in_user_owner($post) == true ) {

?>

<div id="msg<?php echo $row->id; ?>" class="wanted">
	<div>
	<p align="left"><strong><?php _e('Product Description', 'auctionPlugin'); ?></strong></p>
	<?php if( empty($row->message) ) echo ''.__('Sorry, there have been no description of the product.','auctionPlugin').''; else echo $row->message; ?>
	<p align="right">[<a href="#" onclick="msg(<?php echo $row->id; ?>)"><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
	</div>
</div>

<?php
	if($winner < 1) echo '<td align="center" style="width: 20%"><a href="#" title="'.__('Click here to read the message!', 'auctionPlugin').'" onclick="msg(\''.$res->id.'\')">'.__('Msg', 'auctionPlugin').'</a> | <a title="'.__('Click here to accept this offer!', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['winner'], '?_choose_winner=1', 'id='.$res->id.'').'">'.__('Ok','auctionPlugin').'</a>&nbsp; </td>';
	else echo '<td align="center" style="color: green; width: 20%">'.__('Accepted','auctionPlugin').'&nbsp; </td>';

					}
					else {
					echo '<td style="width: 20%"></td>';
					}
					echo '</tr>';

				}
				
				echo '</table>';
			}
			
			?>

</div><!-- /bids_wrap -->

                <form action="" method="post" onsubmit="return saveScrollPositions(this);">

                <div class="bid_wrap">
                <div class="item_detail bid">
                <div class="item_detail1"> &nbsp; <label for="bid"><?php _e('Offer Price:', 'auctionPlugin'); ?></label></div>
                <div class="item_detail2"><input type="text" size="8" name="bid" id="bid" value="" tabindex="1" style="width:92%; height:30px; padding:0 4px;" /></div>
		</div>

		<div class="clear5"></div>

		<div class="item_detail bid"> &nbsp; <label for="message"><?php _e('Describe your product:', 'auctionPlugin'); ?></label>
                <textarea name="message" id="message" tabindex="2"></textarea>
		</div>

		<div class="clear10"></div>

		<?php if ( get_option('cp_auction_plugin_bid_anonymous') == "yes" ) { ?>
		<div class="item_detail bid">
                        <div class="item_detail1"> &nbsp; <label for="no yes"><?php _e('Anonymous:', 'auctionPlugin'); ?></label></div>
                        <div class="item_detail2"><input type="radio" name="bid_anonymous" id="no" value="no" checked /> <label for="no"><?php _e('No', 'auctionPlugin'); ?></label>
		<input type="radio" name="bid_anonymous" id="yes" value="yes" /> <label for="yes"><?php _e('Yes', 'auctionPlugin'); ?></label></div>
                </div>
                <div class="clear5"></div>

		<?php } if ( get_option('cp_auction_deactivate_cp_agreement') != "yes" ) { ?>

		<div class="item_detail bid">
                        <div class="item_detail1"> &nbsp; <a href="#" title="<?php _e('Click here to read our policies!', 'auctionPlugin'); ?>" onclick="overlay()"><label for="agreement"><?php _e('Agreement:', 'auctionPlugin'); ?></a></div>
                        <div class="item_detail2"><input type="checkbox" name="agreement" id="agreement" value="yes" tabindex="3" /> <label for="agreement"><?php _e('Yes I Agree.', 'auctionPlugin'); ?></label></div>
                </div>
		<?php } ?>

                <div class="clear5"></div>
		<input type="hidden" name="scrollx" id="scrollx" value="0" />
		<input type="hidden" name="scrolly" id="scrolly" value="0" />
		<input type="hidden" name="post_id" id="post_id" value="<?php echo $post->ID; ?>" />
                <input type="submit" name="bid_now" value="<?php _e('Place Offer', 'auctionPlugin'); ?>" class="mbtn btn_orange cp-width" tabindex="4" />
                </form>
                </div>

</div><!-- /wrap_container -->

<div class="clr"></div>

<?php cp_auction_after_wanted_box(); ?>

<?php
$scrollx = 0;
$scrolly = 0;
if(!empty($_REQUEST['scrollx'])) {
$scrollx = $_REQUEST['scrollx'];
}
if(!empty($_REQUEST['scrolly'])) {
$scrolly = $_REQUEST['scrolly'];
}
?>

<script type="text/javascript">
window.scrollTo(<?php echo "$scrollx" ?>, <?php echo "$scrolly" ?>);
</script>

<?php
}
}
global $post;
if ( $post )
$imported = get_post_meta( $post->ID, 'a2pItemID', true );
else $imported = false;
if( get_option('cp_auction_use_wanted_hooks') == "yes" && get_option('cp_auction_enable_wanted_widget') != "yes" && $imported == false )
add_action( 'cp_auction_wanted_box', 'cp_auction_wanted_box_code' );
elseif( get_option('cp_auction_use_wanted_hooks') != "yes" && get_option('cp_auction_enable_wanted_widget') != "yes" && $imported == false )
add_action( 'appthemes_after_post_content', 'cp_auction_wanted_box_code' );


/*
|--------------------------------------------------------------------------
| ADD THE WANTED BIDDER BOX ON SINGLE ADS SIDEBAR AS AN TABBED WIDGET
|--------------------------------------------------------------------------
*/

function cp_auction_wanted_box_widget_code() {

?>

<style type="text/css">
p.success, div.success {
	margin:0 !important;
	font-weight: bold;
}
</style>

<?php

	global $current_user, $post, $cp_options, $cpurl;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$user_can = current_user_can( 'manage_options' );

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

	if( ! is_singular(APP_POST_TYPE) && $my_type != "" && $my_type != "wanted"  && $my_type != "classified" )
		return;

	$allowdeals = get_post_meta( $post->ID, 'cp_allow_deals', true );
	$allow_deals = get_localized_field_values('cp_allow_deals', $allowdeals);
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	$end_date = strtotime(get_post_meta($post->ID, 'cp_sys_expire_date', true));
	if ( $post )
	$imported = get_post_meta( $post->ID, 'a2pItemID', true );
	else $imported = false;

	if(isset($_POST['bid_now'])) {

	$bid     = cp_auction_sanitize_amount($_POST['bid']);
	if(isset($_POST['message'])) $message = stripslashes(html_entity_decode($_POST['message']));
	if(isset($_POST['bid_anonymous'])) $bid_anonymous = $_POST['bid_anonymous'];
	if(isset($_POST['agreement'])) $agreement = $_POST['agreement'];
	$previous_bid = cp_auction_plugin_get_latest_bid($post->ID);

	$error = 0;

	if(empty($bid_anonymous)) {
	$bid_anonymous = "no";
	}
	if( get_option( 'cp_auction_deactivate_cp_agreement' ) != "yes" && empty($agreement) ) $error = 1;
	elseif( $bid_anonymous == "yes" && get_option('cp_allow_anonymous'.$post->post_author) == "no" ) $error = 3;
	elseif( is_user_logged_in() != true ) $error = 4;
	elseif( $uid == $post->post_author ) $error = 5;
	elseif( !is_numeric($bid) ) $error = 6;
	elseif( empty($message) ) $error = 7;

	if( $error == 0 ) {

	$tm = time();
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$query = "INSERT INTO {$table_name} (pid, post_author, datemade, bid, last_bid, uid, message, anonymous, type) VALUES (%d, %d, %d, %f, %f, %d, %s, %s, %s)"; 
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $tm, $bid, $previous_bid, $uid, $message, $bid_anonymous, 'wanted'));

	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$watchlist = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND aid = '$post->ID' AND type = 'wanted'", ARRAY_A);
	$result =  $wpdb->num_rows;

	if( $result < 1 ) {
	$table_watchlist = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$query = "INSERT INTO {$table_watchlist} (uid, aid, type) VALUES (%d, %d, %s)"; 
	$wpdb->query($wpdb->prepare($query, $uid, $post->ID, 'wanted'));
	}
	$cp_max_price = get_post_meta( $post->ID, 'cp_max_price', true );
	if( get_option('cp_auction_plugin_if_watchlist') == "yes" ) {
	cp_auction_watchlist_bid_notifications( $post->ID, $bid, 'wanted' );
	}
	cp_auction_new_wanted_bid_posted( $post->ID, $post, $uid, $message, $bid, $cp_max_price );

	appthemes_display_notice( 'success', __( 'Please wait while your offer is processed...', 'auctionPlugin' ) );
	$redirect = ''.get_permalink($post->ID).'?success=1';
	cp_auction_page_redirect($redirect);
	}

	if( $error == 1 ) appthemes_display_notice( 'error', __( 'You must agree that you have read our policies!','auctionPlugin' ) );
	elseif( $error == 3 ) appthemes_display_notice( 'error', __( 'Sorry, the ad author does not allow anonymous offers.','auctionPlugin' ) );
	elseif( $error == 4 ) appthemes_display_notice( 'error', __( 'You need to be logged in before you can post any offers!', 'auctionPlugin' ) );
	elseif( $error == 5 ) appthemes_display_notice( 'error', __( 'You cannot post any offers on your own ads!', 'auctionPlugin' ) );
	elseif( $error == 6 ) appthemes_display_notice( 'error', __( 'Your offer needs to be of a numeric value!', 'auctionPlugin' ) );
	elseif( $error == 7 ) appthemes_display_notice( 'error', __( 'Please enter a product description!', 'auctionPlugin' ) );
	}
	if( isset($_GET['success']) == 1 ) appthemes_display_notice( 'success', __( 'Your offer was registered!', 'auctionPlugin' ) );
	$confirm = get_option('cp_auction_confirmation_popup');

?>

<script type="text/javascript">
function saveScrollPositions(theForm) {
if(theForm) {
var scrolly = typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement.scrollTop;
var scrollx = typeof window.pageXOffset != 'undefined' ? window.pageXOffset : document.documentElement.scrollLeft;
theForm.scrollx.value = scrollx;
theForm.scrolly.value = scrolly;
}
}
</script>

<script type="text/javascript">
function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>

<script type="text/javascript">
function msg(id) {
	el = document.getElementById("msg"+id);
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>

		<div id="overlay">
     		<div>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
     		<?php if(get_option('cp_auction_plugin_cp_agreement') == "") {
     		if(( get_option('cp_version') < "3.3" )) {
          	echo get_option('cp_ads_tou_msg');
          	} elseif( get_option('cp_version') > "3.2.1" ) {
          	echo $cp_options->ads_tou_msg;
          	}
            	} else {
          	echo get_option('cp_auction_plugin_cp_agreement');
          	} ?>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
     		</div>
		</div>

	<div class="clr"></div>

<?php if ( $uid != $post->post_author ) { ?>

                <form action="" method="post" onsubmit="return saveScrollPositions(this);">

                <div class="bid_wrap_widget">

                <?php if( get_option('cp_auction_plugin_wanted_info') ) echo '<div class="page_info">'.nl2br(get_option('cp_auction_plugin_wanted_info')).'</div><div class="clear10"></div>'; ?>

               	<div class="clear10"></div>

                <div class="item_detail bid">
                <div class="item_detail1"> &nbsp; <label for="bid"><?php _e('Offer Price:', 'auctionPlugin'); ?></label></div>
                <div class="item_detail2"><input type="text" size="8" name="bid" id="bid" value="" tabindex="1" style="width:94%; height:30px; padding:0 4px;" /></div>
		</div>

		<div class="clear5"></div>

		<div class="item_detail bid"> &nbsp; <label for="message"><?php _e('Describe your product:', 'auctionPlugin'); ?></label>
                <textarea name="message" id="message" tabindex="2"></textarea>
		</div>

		<div class="clear10"></div>

		<?php if ( get_option('cp_auction_plugin_bid_anonymous') == "yes") { ?>
		<div class="item_detail bid">
                        <div class="item_detail1"> &nbsp; <label for="no yes"><?php _e('Anonymous:', 'auctionPlugin'); ?></label></div>
                        <div class="item_detail2"><input type="radio" name="bid_anonymous" id="no" value="no" checked /> <label for="no"><?php _e('No', 'auctionPlugin'); ?></label>
		<input type="radio" name="bid_anonymous" id="yes" value="yes" /> <label for="yes"><?php _e('Yes', 'auctionPlugin'); ?></label></div>
                </div>
                <div class="clear5"></div>

		<?php } if ( get_option('cp_auction_deactivate_cp_agreement') != "yes" ) { ?>

		<div class="item_detail bid">
                        <div class="item_detail1"> &nbsp; <a href="#" title="<?php _e('Click here to read our policies!', 'auctionPlugin'); ?>" onclick="overlay()"><label for="agreement"><?php _e('Agreement:', 'auctionPlugin'); ?></a></div>
                        <div class="item_detail2"><input type="checkbox" name="agreement" id="agreement" value="yes" tabindex="3" /> <label for="agreement"><?php _e('Yes I Agree.', 'auctionPlugin'); ?></label></div>
                </div>
		<?php } ?>

                <div class="clear5"></div>
		<input type="hidden" name="scrollx" id="scrollx" value="0" />
		<input type="hidden" name="scrolly" id="scrolly" value="0" />
                <input type="submit" name="bid_now" value="<?php _e('Place Offer', 'auctionPlugin'); ?>" class="mbtn btn_orange cp-width" tabindex="4" />
                </form>
                </div>

<?php } ?><!-- / if not post author -->

<?php
	$scrollx = 0;
	$scrolly = 0;
	if(!empty($_REQUEST['scrollx'])) {
	$scrollx = $_REQUEST['scrollx'];
	}
	if(!empty($_REQUEST['scrolly'])) {
	$scrolly = $_REQUEST['scrolly'];
	}
	$closed = end_auction($post->ID);
	$dt2 = strtotime(get_post_meta($post->ID,'cp_sys_expire_date',true));
	$offset = get_option('gmt_offset');
	if( $offset < 0 ) $a = "";
	if( $offset >= 0 ) $a = "+";
?>

<script type="text/javascript">
window.scrollTo(<?php echo "$scrollx" ?>, <?php echo "$scrolly" ?>);
</script>

	<div class="clear5"></div>

	<div class="bids_wrap_widget">

            <?php
			global $wpdb;
			$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
			$results = $wpdb->get_results("SELECT * FROM {$table_bids} WHERE pid = '$post->ID' AND type = 'wanted' ORDER BY datemade DESC");

			echo '<h2 class="dotted">'.__('Latest Offers:','auctionPlugin').'</h2>';

			if( ! $results ) echo '<i>'.__('There are currently no offers!','auctionPlugin').'</i>';
			else
			{
				echo '<div class="clear5"></div>';
				if( is_logged_in_user_owner($post) == true )
				echo '<table style="width: 100%" class="padded-table">';
				else
				echo '<table style="width: 100%" class="padded-table">';
				echo '<tr class="bid_tr">';
				echo '<td style="width: 20%; padding: 2px 2px 2px 2px;">&nbsp; <strong>'.__('User','auctionPlugin').'</strong></td>';
				echo '<td align="center" style="width: 20%"><strong>'.__('Date','auctionPlugin').'</strong></td>';
				echo '<td align="center" style="width: 20%">&nbsp;&nbsp;&nbsp;&nbsp; <strong>'.__('Offers','auctionPlugin').'</strong></td>';
				echo '<td align="right" style="width: 3%"> </td>';

				if( is_logged_in_user_owner($post) == true )
				echo '<td align="center" style="width: 20%"><strong>'.__('Accept?','auctionPlugin').'</strong>&nbsp; </td>';
				else
				echo '<td style="width: 20%"></td>';
				echo '</tr>';

				$rowclass = '';
				foreach ( $results as $res ) {
				$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

					$user = get_userdata($res->uid);
					$winner = $res->winner;

					if(($res->anonymous == "no") || ($res->anonymous == "")) $public = '<a title="'.$user->user_login.'" href="'.get_author_posts_url($res->uid).'">'.truncate($user->user_login, 8, '..').'</a>';
					else
					$public = ''.__('Anonymous','auctionPlugin').'';

					echo '<tr id="'.$res->id.'" class="bids_tr '.$rowclass.'">';
					echo '<td style="width: 20%; padding-left: 2px;">&nbsp; '.$public.'</td>';
					echo '<td align="center" style="width: 20%">'.date_i18n('d/m/y', $res->datemade, true).'</td>';
					echo '<td align="right" style="width: 20%">'.cp_display_price( $res->bid, 'ad', false ).'</td>';
					if(( is_logged_in_user_owner($post) == true && get_option('cp_auction_plugin_owner_delete') == "yes" ) || ( $user_can == true )) {
					echo '<td align="right" style="width: 3%"><a href="#" class="delete-bid" title="'.__('You should delete only fraudelent bids.', 'auctionPlugin').'" id="'.$res->id.'"><div class="deleteico"></div></a></td>';

					} elseif(($res->uid == $uid && get_option('cp_auction_plugin_bidder_delete') == "yes" ) || ( $user_can == true )) {
					echo '<td align="right" style="width: 3%"><a href="#" class="delete-bid" title="'.__('You should delete only faulty bids.', 'auctionPlugin').'" id="'.$res->id.'"><div class="deleteico"></div></a></td>';
					} else {
					echo '<td align="right" style="width: 3%"> </td>';
					}

					if( is_logged_in_user_owner($post) == true ) {

?>

<div id="msg<?php echo $res->id; ?>" class="wanted">
	<div>
	<p align="left"><strong><?php _e('Product Description', 'auctionPlugin'); ?></strong></p>
	<?php if( empty($res->message) ) echo ''.__('Sorry, there have been no description of the product.','auctionPlugin').''; else echo $res->message; ?>
	<p align="right">[<a href="#" onclick="msg(<?php echo $res->id; ?>)"><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
	</div>
</div>

<?php
	if($winner < 1) echo '<td align="center" style="width: 20%"><a href="#" title="'.__('Click here to read the message!', 'auctionPlugin').'" onclick="msg(\''.$res->id.'\')">'.__('Msg', 'auctionPlugin').'</a> | <a title="'.__('Click here to accept this offer!', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['winner'], '?_choose_winner=1', 'id='.$res->id.'').'">'.__('Ok','auctionPlugin').'</a>&nbsp; </td>';
	else echo '<td align="center" style="color: green; width: 20%">'.__('Accepted','auctionPlugin').'&nbsp; </td>';

					}
					else {
					echo '<td style="width: 20%"></td>';
					}
					echo '</tr>';

				}
				
				echo '</table>';
			}
			
			?>

</div><!-- /bids_wrap -->

<div class="clear10"></div>

<?php
}
add_action( 'cp_auction_wanted_box_widget', 'cp_auction_wanted_box_widget_code' );


/*
|--------------------------------------------------------------------------
| ADD THE FACEBOOK LIKE CODE TO THE HEADER
|--------------------------------------------------------------------------
*/

function cp_auction_fblike_header_code() {

	$lang = get_bloginfo( 'language' );
	$lc = preg_replace('/-/', '_', $lang).''; 
?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
  	var js, fjs = d.getElementsByTagName(s)[0];
  	if (d.getElementById(id)) return;
  	js = d.createElement(s); js.id = id;
  	js.src = "//connect.facebook.net/<?php echo $lc; ?>/all.js#xfbml=1";
  	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

<?php
}
add_action('appthemes_before', 'cp_auction_fblike_header_code');


/*
|--------------------------------------------------------------------------
| ADD THE SOCIAL SHARING CODE ON WANTED & AUCTION ADS
|--------------------------------------------------------------------------
*/

function cp_auction_social_sharing_auctions() {

	if( ! is_singular(APP_POST_TYPE) )
		return;

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ); ?>

	<?php if( ( get_option("cp_add_this_activate") == "true" ) && ( ! empty( $my_type ) && $my_type != "classified" ) ) { ?>
            <div class="clear5"></div>
            <?php echo get_option("cp_add_this_code"); ?>
	    <div class="clr"></div>
	    <?php } ?>
<?php
}
$sarray = array( 'flatpress', 'eclassify', 'flatron', 'ultraclassifieds' );
if( get_option('cp_auction_addthis_top') == '1' ) 
add_action( 'appthemes_after_post_title', 'cp_auction_social_sharing_auctions' );
if( get_option('cp_auction_addthis_top') == '1' && ! in_array( get_option('stylesheet'), $sarray ) )
add_action( 'appthemes_before_post', 'cp_auction_social_sharing_auctions' );
if( get_option('cp_auction_addthis_middle') == '1' )
add_action( 'appthemes_before_post_content', 'cp_auction_social_sharing_auctions' );
if( get_option('cp_auction_addthis_bottom') == '1' )
add_action( 'appthemes_after_post_content', 'cp_auction_social_sharing_auctions' );


/*
|--------------------------------------------------------------------------
| ADD THE FACEBOOK SEND & LIKE BUTTONS ON WANTED & AUCTION ADS
|--------------------------------------------------------------------------
*/

function cp_auction_fb_like_auctions() {

	if( ! is_singular(APP_POST_TYPE) )
		return;

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ); ?>

	<?php if( ( get_option("cp_fb_like_activate") == "true" ) && ( ! empty( $my_type ) && $my_type != "classified" ) ) { ?>
	<div class="clear10"></div>
	<?php echo cp_fb_like(); ?>
	<div class="clr"></div>
	<?php } ?>
<?php
}
$sarray = array( 'flatpress', 'eclassify', 'flatron', 'ultraclassifieds' );
if( get_option("cp_fb_like_top") == '1' )
add_action( 'appthemes_after_post_title', 'cp_auction_fb_like_auctions' );
if( get_option('cp_auction_addthis_top') == '1' && ! in_array( get_option('stylesheet'), $sarray ) )
add_action( 'appthemes_before_post', 'cp_auction_fb_like_auctions' );
if( get_option("cp_fb_like_middle") == '1' )
add_action( 'appthemes_before_post_content', 'cp_auction_fb_like_auctions' );
if( get_option("cp_fb_like_bottom") == '1' )
add_action( 'appthemes_after_post_content', 'cp_auction_fb_like_auctions' );


/*
|--------------------------------------------------------------------------
| ADD THE SOCIAL SHARING CODE ON CLASSIFIEDS
|--------------------------------------------------------------------------
*/

function cp_auction_social_sharing_classifieds() {

	if( ! is_singular(APP_POST_TYPE) )
		return;

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ); ?>

	<?php if( ( get_option("cp_add_this_activate_classifieds") == "true" ) && ( empty( $my_type ) || $my_type == "classified" ) ) { ?>
            <div class="clear5"></div>
            <?php echo get_option("cp_add_this_code"); ?>
	    <div class="clr"></div>
	    <?php } ?>
<?php
}
if( get_option('cp_auction_addthis_top') == '1' )
add_action( 'appthemes_after_post_title', 'cp_auction_social_sharing_classifieds' );
if( get_option('cp_auction_addthis_top') == '1' )
add_action( 'appthemes_before_post', 'cp_auction_social_sharing_classifieds' );
if( get_option('cp_auction_addthis_middle') == '1' )
add_action( 'appthemes_before_post_content', 'cp_auction_social_sharing_classifieds' );
if( get_option('cp_auction_addthis_bottom') == '1' )
add_action( 'appthemes_after_post_content', 'cp_auction_social_sharing_classifieds' );


/*
|--------------------------------------------------------------------------
| ADD THE FACEBOOK SEND & LIKE BUTTONS ON CLASSIFIED ADS
|--------------------------------------------------------------------------
*/

function cp_auction_fb_like_classified() {

	if( ! is_singular(APP_POST_TYPE) )
		return;

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true ); ?>

	<?php if( ( get_option("cp_fb_like_activate_classifieds") == "true" ) && ( empty( $my_type ) || $my_type == "classified" ) ) { ?>
	<div class="clear10"></div>
	<?php echo cp_fb_like(); ?>
	<div class="clr"></div>
	<?php } ?>
<?php
}
$sarray = array( 'flatpress', 'eclassify', 'flatron', 'ultraclassifieds' );
if( get_option("cp_fb_like_top") == '1' )
add_action( 'appthemes_after_post_title', 'cp_auction_fb_like_classified' );
if( get_option('cp_auction_addthis_top') == '1' && ! in_array( get_option('stylesheet'), $sarray ) )
add_action( 'appthemes_before_post', 'cp_auction_fb_like_classified' );
if( get_option("cp_fb_like_middle") == '1' )
add_action( 'appthemes_before_post_content', 'cp_auction_fb_like_classified' );
if( get_option("cp_fb_like_bottom") == '1' )
add_action( 'appthemes_after_post_content', 'cp_auction_fb_like_classified' );


/*
|--------------------------------------------------------------------------
| ADD THE SOCIAL SHARING CODE IN HEADER
|--------------------------------------------------------------------------
*/

function cp_auction_social_sharing_header() {

	if( get_option("cp_add_this_activate_header") == "true" && get_option("cp_auction_addthis_header") == '1' ) { ?>

            <div class="clear10"></div>
            <div style="float: right;"><?php echo get_option("cp_add_this_code_header"); ?></div>
	    <div class="clr"></div>
	    <?php } if( get_option("cp_fb_like_activate_header") == "true" && get_option("cp_fb_like_header") == '1' ) { ?>
	    <div class="clear10"></div>
	    <?php echo cp_fb_like(); ?>
	    <div class="clr"></div>
	    <?php } ?>

<?php
}
if( get_option("cp_add_this_activate_header") == "true" || get_option("cp_fb_like_activate_header") == "true" )
add_action( 'appthemes_advertise_header', 'cp_auction_social_sharing_header', 11 );


/*
|--------------------------------------------------------------------------
| SOLD AD BANNER BELOW TITLE ON SINGLE AD PAGE
|--------------------------------------------------------------------------
*/

function cp_auction_sold_banner() {

	if( ! is_singular(APP_POST_TYPE) )
		return;

	global $post;

	if( get_option('cp_auction_plugin_sold_banner') == "yes" && get_post_meta($post->ID, 'cp_ad_sold', true) == "yes" ) { ?>

	<div class="is_sold"><span><?php _e( 'Sản phẩm này đã được bán!', 'auctionPlugin' ); ?></span></div>

<?php
	}
}
add_action( 'appthemes_after_post_title', 'cp_auction_sold_banner' );


/*
|--------------------------------------------------------------------------
| SOLD AD RIBBON
|--------------------------------------------------------------------------
*/

function cp_auction_sold_ribbon() {

	global $post;
	$sarray = array( 'classipress', 'classipress-headline-blue', 'classipress-headline-green', 'classipress-headline-orange', 'custom', 'classiclean', 'classipress-headline-purple', 'classipress-headline-red' );

	if ( is_singular(APP_POST_TYPE) || get_option('cp_auction_sold_ribbon') != "yes" || ! in_array( get_option('stylesheet'), $sarray ) )
		return;

	$cp_ad_sold = end_auction($post->ID);
	if( $cp_ad_sold == "yes" ) { ?>
        <div class="cp-front-ribbon-list"></div>
        <?php }

}
add_action( 'appthemes_before_post', 'cp_auction_sold_ribbon' );


/*
|--------------------------------------------------------------------------
| FEATURED AD RIBBON
|--------------------------------------------------------------------------
*/

function cp_auction_featured_ribbon() {

	$sarray = array( 'classipress', 'classipress-headline-blue', 'classipress-headline-green', 'classipress-headline-orange', 'custom', 'classiclean', 'classipress-headline-purple', 'classipress-headline-red' );

	if ( is_singular(APP_POST_TYPE) || ! is_sticky() || get_option('cp_auction_featured_ribbon') != "yes" || ! in_array( get_option('stylesheet'), $sarray ) )
		return;
?>

	<div class="cp-front-featured-ribbon"></div>

<?php
}
add_action( 'appthemes_before_post', 'cp_auction_featured_ribbon' );


/*
|--------------------------------------------------------------------------
| AUCTION TYPE AD RIBBON ON TITLE
|--------------------------------------------------------------------------
*/

function cp_auction_ad_ribbon_code($title) {

	global $post, $cpurl;

	if ( ! $post ) return $title;

	if( $post ) $post_type = get_post_type( $post );

	if ( is_single() || $post_type != APP_POST_TYPE || ! in_the_loop() )
		return $title;

	$ribbon = false;
	$wantto = false;
	$closed = false;

	$ct = get_option('stylesheet');
	$ribbon = get_option('cp_auction_auction_ribbon');
	if( $post )
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	else $closed = false;

	$upanel = $cpurl['userpanel'];
	$my_ads = $cpurl['myads'];
	$dpanel = "dashboard";
	$uri = $_SERVER['REQUEST_URI'];
	$pattern = '#^/'.$upanel.'(/.*)?#';
	$pattern2 = '#^/'.$my_ads.'(/.*)?#';
	$pattern3 = '#^/'.$dpanel.'(/.*)?#';
	if( preg_match($pattern, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern2, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern3, $uri, $matches) ) $display = false;
	else $display = true;
	$post_type = get_post_type( $post );
	if( $closed == "yes" ) $ribbon = __('Sold', 'auctionPlugin');

	if ( $ribbon == true && $display && $ct != "jibo" && $ct != "simply-responsive-cp" && $ct != "flatpress" && get_option( 'aws_gridview_installed' ) != "yes" ) {
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if ( $my_type == "normal" && $closed == "yes" ) {
		if( $ct == 'twinpress_classifieds' )
		return '</h3><span class="ad-type-sold">'.$ribbon.'</span> '.$title.'';
		else if ( $ct == 'flatron' )
		return '<span class="ad-type-sold">'.$ribbon.'</span> '.$title.'';
		else
		return '</h3><span class="ad-type-sold">'.$ribbon.'</span> <h3>'.$title.'';
		
	} elseif( $my_type == "normal" && $closed != "yes" ) {
		if( $ct == 'twinpress_classifieds' )
		return '</h3><span class="ad-type">'.$ribbon.'</span> '.$title.'';
		else if ( $ct == 'flatron' )
		return '<span class="ad-type">'.$ribbon.'</span> '.$title.'';
		else
		return '</h3><span class="ad-type">'.$ribbon.'</span> <h3>'.$title.'';
	} else {
	return $title;
	}
	}
	else {
	return $title;
	}
}
add_filter( 'the_title', 'cp_auction_ad_ribbon_code' );


/*
|--------------------------------------------------------------------------
| REVERSE AUCTION TYPE AD RIBBON ON TITLE
|--------------------------------------------------------------------------
*/

function cp_auction_reverse_ad_ribbon_code($title) {

	global $post, $cpurl;

	if ( ! $post ) return $title;

	if( $post ) $post_type = get_post_type( $post );

	if ( is_single() || $post_type != APP_POST_TYPE || ! in_the_loop() )
		return $title;

	$ribbon = false;
	$wantto = false;
	$closed = false;

	$ct = get_option('stylesheet');
	$ribbon = get_option('cp_auction_reverse_ribbon');
	if( $post )
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	else $closed = false;

	$upanel = $cpurl['userpanel'];
	$my_ads = $cpurl['myads'];
	$dpanel = "dashboard";
	$uri = $_SERVER['REQUEST_URI'];
	$pattern = '#^/'.$upanel.'(/.*)?#';
	$pattern2 = '#^/'.$my_ads.'(/.*)?#';
	$pattern3 = '#^/'.$dpanel.'(/.*)?#';
	if( preg_match($pattern, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern2, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern3, $uri, $matches) ) $display = false;
	else $display = true;
	$post_type = get_post_type( $post );
	if( $closed == "yes" ) $ribbon = __('Sold', 'auctionPlugin');

	if ( $ribbon == true && $display && $ct != "jibo" && $ct != "simply-responsive-cp" && $ct != "flatpress" && get_option( 'aws_gridview_installed' ) != "yes" ) {
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if ( $my_type == "reverse" && $closed == "yes" ) {
		if( $ct == 'twinpress_classifieds' )
		return '</h3><span class="ad-type-sold">'.$ribbon.'</span> '.$title.'';
		else if ( $ct == 'flatron' )
		return '<span class="ad-type-sold">'.$ribbon.'</span> '.$title.'';
		else
		return '</h3><span class="ad-type-sold">'.$ribbon.'</span> <h3>'.$title.'';
		
	} elseif( $my_type == "reverse" && $closed != "yes" ) {
		if( $ct == 'twinpress_classifieds' )
		return '</h3><span class="ad-type">'.$ribbon.'</span> '.$title.'';
		else if ( $ct == 'flatron' )
		return '<span class="ad-type">'.$ribbon.'</span> '.$title.'';
		else
		return '</h3><span class="ad-type">'.$ribbon.'</span> <h3>'.$title.'';
	} else {
	return $title;
	}
	}
	else {
	return $title;
	}
}
add_filter( 'the_title', 'cp_auction_reverse_ad_ribbon_code' );


/*
|--------------------------------------------------------------------------
| WANTED TYPE AD RIBBON ON TITLE
|--------------------------------------------------------------------------
*/

function cp_auction_wanted_ad_ribbon_code($title) {

	global $post, $cpurl;

	if ( ! $post ) return $title;

	$ct = get_option('stylesheet');
	$sarray = array( 'jibo', 'simply-responsive-cp', 'flatpress' );
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if( $post ) $post_type = get_post_type( $post );

	if ( is_single() || $post_type != APP_POST_TYPE || ! in_the_loop() || in_array( get_option('stylesheet'), $sarray ) || $my_type != "wanted" || get_option( 'aws_gridview_installed' ) == "yes" )
		return $title;

	$ribbon = false;
	$wantto = false;
	$closed = false;

	$ct = get_option('stylesheet');
	$ribbon = get_option('cp_auction_wanted_ribbon');
	$upanel = $cpurl['userpanel'];
	$my_ads = $cpurl['myads'];
	$uribbon = get_option('cp_auction_users_ribbon');
	if( $post ) {
	$wantto = get_post_meta( $post->ID, 'cp_want_to', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	} else {
	$wantto = false;
	$closed = false;
	}
	$dpanel = "dashboard";
	$uri = $_SERVER['REQUEST_URI'];
	$pattern = '#^/'.$upanel.'(/.*)?#';
	$pattern2 = '#^/'.$my_ads.'(/.*)?#';
	$pattern3 = '#^/'.$dpanel.'(/.*)?#';
	if( preg_match($pattern, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern2, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern3, $uri, $matches) ) $display = false;
	else $display = true;

	if( $uribbon == "yes" ) $ribbon = cp_auction_users_ribbon($post->ID, 'cp_want_to');
	if( empty( $ribbon ) ) $ribbon = get_option('cp_auction_wanted_ribbon');
	if( $closed == "yes" ) $ribbon = __('Sold', 'auctionPlugin');

	if ( $ribbon == true && $display ) {

	if ( $closed == "yes" ) {
		if( $ct == 'twinpress_classifieds' )
		return '</h3><span class="ad-type-sold">'.$ribbon.'</span> '.$title.'';
		else if ( $ct == 'flatron' )
		return '<span class="ad-type-sold">'.$ribbon.'</span> '.$title.'';
		else
		return '</h3><span class="ad-type-sold">'.$ribbon.'</span> <h3>'.$title.'';
	} else if ( $closed != "yes" ) {
		if( $ct == 'twinpress_classifieds' )
		return '</h3><span class="ad-type">'.$ribbon.'</span> '.$title.'';
		else if ( $ct == 'flatron' )
		return '<span class="ad-type">'.$ribbon.'</span> '.$title.'';
		else
		return '</h3><span class="ad-type">'.$ribbon.'</span> <h3>'.$title.'';
	} else {
	return $title;
	}

	}
	else {
	return $title;
	}
}
add_filter( 'the_title', 'cp_auction_wanted_ad_ribbon_code' );


/*
|--------------------------------------------------------------------------
| CLASSIFIED TYPE AD RIBBON ON TITLE
|--------------------------------------------------------------------------
*/

function cp_auction_classified_ad_ribbon_code($title) {

	global $post, $cp_options, $cpurl;

	if ( ! $post ) return $title;

	$ct = get_option('stylesheet');
	$sarray = array( 'jibo', 'simply-responsive-cp', 'flatpress' );
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if( $post ) $post_type = get_post_type( $post );

	if ( is_single() || $post_type != APP_POST_TYPE || ! in_the_loop() || in_array( get_option('stylesheet'), $sarray ) || $my_type != "classified" || get_option( 'aws_gridview_installed' ) == "yes" )
		return $title;

	$ribbon = false;
	$wantto = false;
	$closed = false;

	$ribbon = get_option('cp_auction_classified_ribbon');
	$upanel = $cpurl['userpanel'];
	$my_ads = $cpurl['myads'];
	$uribbon = get_option('cp_auction_users_ribbon');
	if( $post ) {
	$wantto = get_post_meta( $post->ID, 'cp_iwant_to', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	} else {
	$wantto = false;
	$closed = false;
	}
	$dpanel = "dashboard";
	$uri = $_SERVER['REQUEST_URI'];
	$pattern = '#^/'.$upanel.'(/.*)?#';
	$pattern2 = '#^/'.$my_ads.'(/.*)?#';
	$pattern3 = '#^/'.$dpanel.'(/.*)?#';
	if( preg_match($pattern, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern2, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern3, $uri, $matches) ) $display = false;
	else $display = true;

	if( $uribbon == "yes" ) $ribbon = cp_auction_users_ribbon($post->ID, 'cp_iwant_to');
	if( empty( $ribbon ) ) $ribbon = get_option('cp_auction_classified_ribbon');
	if( $closed == "yes" ) $ribbon = __('Sold', 'auctionPlugin');

	if ( $ribbon == true && $display ) {

	if ( $closed == "yes" ) {
		if( $ct == 'twinpress_classifieds' )
		return '</h3><span class="ad-type-sold">'.$ribbon.'</span> '.$title.'';
		else if ( $ct == 'flatron' )
		return '<span class="ad-type-sold">'.$ribbon.'</span> '.$title.'';
		else
		return '</h3><span class="ad-type-sold">'.$ribbon.'</span> <h3>'.$title.'';
	} else if ( $closed != "yes" ) {
		if( $ct == 'twinpress_classifieds' )
		return '</h3><span class="ad-type">'.$ribbon.'</span> '.$title.'';
		else if ( $ct == 'flatron' )
		return '<span class="ad-type">'.$ribbon.'</span> '.$title.'';
		else
		return '</h3><span class="ad-type">'.$ribbon.'</span> <h3>'.$title.'';
	} else {
	return $title;
	}

	}
	else {
	return $title;
	}
}
add_filter( 'the_title', 'cp_auction_classified_ad_ribbon_code' );


/*
|--------------------------------------------------------------------------
| AD TYPE RIBBON FOR JIBO CHILD THEME
|--------------------------------------------------------------------------
*/

function cp_auction_grid_ad_type() {

	global $post, $query, $cpurl;

	if( $post ) $post_type = get_post_type( $post );

	if ( is_single() || $post_type != APP_POST_TYPE || ! in_the_loop() )
		return;

	$ribbon = false;
	$wantto = false;
	$closed = false;

	if( $post )
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	else $my_type = false;
	$upanel = $cpurl['userpanel'];
	$my_ads = $cpurl['myads'];
	$uribbon = get_option('cp_auction_users_ribbon');
	if( empty( $my_type ) || $my_type == "classified" ) {
	$fname = "cp_iwant_to";
	$wantto = get_post_meta( $post->ID, 'cp_iwant_to', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	} elseif( $my_type == "wanted" ) {
	$fname = "cp_want_to";
	$wantto = get_post_meta( $post->ID, 'cp_want_to', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	} elseif( $my_type == "normal" ) {
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);	
	} else {
	$fname = false;
	$wantto = false;
	$closed = false;
	}

	$dpanel = "dashboard";
	$uri = $_SERVER['REQUEST_URI'];
	$pattern = '#^/'.$upanel.'(/.*)?#';
	$pattern2 = '#^/'.$my_ads.'(/.*)?#';
	$pattern3 = '#^/'.$dpanel.'(/.*)?#';
	if( preg_match($pattern, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern2, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern3, $uri, $matches) ) $display = false;
	else $display = true;

	if( $wantto && $uribbon == "yes" ) $ribbon = cp_auction_users_ribbon($post->ID, $fname);
	if( $closed == "yes" ) $ribbon = __('Sold', 'auctionPlugin');

	if ( $query = cp_get_popular_ads() && $display ) {

	if( $ribbon ) {
	if( $closed == "yes" ) { ?>
	<p class="type-ad-sold"><?php echo $ribbon; ?></p>
	<?php } else { ?>
	<p class="type-ad"><?php echo $ribbon; ?></p>
	<?php } } else { ?>
	<?php if ( get_option('cp_auction_auction_ribbon') == true && $my_type == "normal" ) { ?>
	<p class="type-ad"><?php echo get_option('cp_auction_auction_ribbon'); ?></p>
	<?php }	elseif ( get_option('cp_auction_reverse_ribbon') == true && $my_type == "reverse" ) { ?>
	<p class="type-ad"><?php echo get_option('cp_auction_reverse_ribbon'); ?></p>
	<?php }	elseif ( get_option('cp_auction_wanted_ribbon') == true && $my_type == "wanted" ) { ?>
	<p class="type-ad"><?php echo get_option('cp_auction_wanted_ribbon'); ?></p>
	<?php }	elseif ( get_option('cp_auction_classified_ribbon') == true && empty( $my_type ) || $my_type == "classified" ) { ?>
	<p class="type-ad"><?php echo get_option('cp_auction_classified_ribbon'); ?></p>

<?php
	}
	}
}
}
if( get_option('stylesheet') == "jibo" && get_option( 'aws_gridview_installed' ) != "yes" )
add_action( 'appthemes_before_post_title', 'cp_auction_grid_ad_type' );
if ( get_option( 'aws_gridview_installed' ) == "yes" )
add_action( 'awsolutions_gridview_adtype_hook', 'cp_auction_grid_ad_type' );


/*
|--------------------------------------------------------------------------
| AD TYPE RIBBON FOR FLATPRESS CHILD THEME
|--------------------------------------------------------------------------
*/

function cp_auction_flatpress_ad_type() {

	global $post, $query, $cpurl;

	if( $post ) $post_type = get_post_type( $post );

	if ( is_single() || $post_type != APP_POST_TYPE || ! in_the_loop() )
		return;

	$ribbon = false;
	$wantto = false;
	$closed = false;

	if( $post )
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	else $my_type = false;
	$upanel = $cpurl['userpanel'];
	$my_ads = $cpurl['myads'];
	$uribbon = get_option('cp_auction_users_ribbon');
	if( empty( $my_type ) || $my_type == "classified" ) {
	$fname = "cp_iwant_to";
	$wantto = get_post_meta( $post->ID, 'cp_iwant_to', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	} elseif( $my_type == "wanted" ) {
	$fname = "cp_want_to";
	$wantto = get_post_meta( $post->ID, 'cp_want_to', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	} elseif( $my_type == "normal" ) {
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);	
	} else {
	$fname = false;
	$wantto = false;
	$closed = false;
	}
	
	$dpanel = "dashboard";
	$uri = $_SERVER['REQUEST_URI'];
	$pattern = '#^/'.$upanel.'(/.*)?#';
	$pattern2 = '#^/'.$my_ads.'(/.*)?#';
	$pattern3 = '#^/'.$dpanel.'(/.*)?#';
	if( preg_match($pattern, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern2, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern3, $uri, $matches) ) $display = false;
	else $display = true;

	if( $wantto && $uribbon == "yes" ) $ribbon = cp_auction_users_ribbon($post->ID, $fname);
	if( $closed == "yes" ) $ribbon = __('Sold', 'auctionPlugin');

	if ( $query = cp_get_popular_ads() && $display ) {

	if( $ribbon ) {
	if( $closed == "yes" ) { ?>
	<p class="type-ad-sold"><?php echo $ribbon; ?></p>
	<?php } else { ?>
	<p class="type-ad"><?php echo $ribbon; ?></p>
	<?php } } else { ?>
	<?php if ( get_option('cp_auction_auction_ribbon') == true && $my_type == "normal" ) { ?>
	<p class="type-ad"><?php echo get_option('cp_auction_auction_ribbon'); ?></p>
	<?php }	elseif ( get_option('cp_auction_reverse_ribbon') == true && $my_type == "reverse" ) { ?>
	<p class="type-ad"><?php echo get_option('cp_auction_reverse_ribbon'); ?></p>
	<?php }	elseif ( get_option('cp_auction_wanted_ribbon') == true && $my_type == "wanted" ) { ?>
	<p class="type-ad"><?php echo get_option('cp_auction_wanted_ribbon'); ?></p>
	<?php }	elseif ( get_option('cp_auction_classified_ribbon') == true && empty( $my_type ) || $my_type == "classified" ) { ?>
	<p class="type-ad"><?php echo get_option('cp_auction_classified_ribbon'); ?></p>

<?php
	}
	}
}
}
if( get_option('stylesheet') == "flatpress" )
add_action( 'appthemes_before_post_title', 'cp_auction_flatpress_ad_type' );


/*
|--------------------------------------------------------------------------
| AD TYPE RIBBON FOR SIMPLY RESPONSIVE CHILD THEME
|--------------------------------------------------------------------------
*/

function cp_auction_ad_loop_meta() {
	global $post, $cp_options, $query, $cpurl;

	if( $post ) $post_type = get_post_type( $post );

	if ( is_single() || $post_type != APP_POST_TYPE || ! in_the_loop() )
		return;

	$ribbon = false;
	$wantto = false;
	$closed = false;

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$upanel = $cpurl['userpanel'];
	$my_ads = $cpurl['myads'];
	$uribbon = get_option('cp_auction_users_ribbon');
	if( empty( $my_type ) || $my_type == "classified" ) {
	$fname = "cp_iwant_to";
	$wantto = get_post_meta( $post->ID, 'cp_iwant_to', true );
	} elseif( $my_type == "wanted" ) {
	$fname = "cp_want_to";
	$wantto = get_post_meta( $post->ID, 'cp_want_to', true );
	} elseif( $my_type == "normal" ) {
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);	
	} else {
	$fname = false;
	$wantto = false;
	}

	$dpanel = "dashboard";
	$uri = $_SERVER['REQUEST_URI'];
	$pattern = '#^/'.$upanel.'(/.*)?#';
	$pattern2 = '#^/'.$my_ads.'(/.*)?#';
	$pattern3 = '#^/'.$dpanel.'(/.*)?#';
	if( preg_match($pattern, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern2, $uri, $matches) ) $display = false;
	elseif( preg_match($pattern3, $uri, $matches) ) $display = false;
	else $display = true;

	if( $wantto && $uribbon == "yes" ) $ribbon = cp_auction_users_ribbon($post->ID, $fname);

	if ( $query = cp_get_popular_ads() && $display ) {

	if( $ribbon ) {
	$type = $ribbon;
	} else {
	if ( get_option('cp_auction_auction_ribbon') == true && $my_type == "normal" ) $type = get_option('cp_auction_auction_ribbon');
	if ( get_option('cp_auction_reverse_ribbon') == true && $my_type == "reverse" ) $type = get_option('cp_auction_reverse_ribbon');
	if ( get_option('cp_auction_wanted_ribbon') == true && $my_type == "wanted" ) $type = get_option('cp_auction_wanted_ribbon');
	if ( get_option('cp_auction_classified_ribbon') == true && empty( $my_type ) || $my_type == "classified" ) $type = get_option('cp_auction_classified_ribbon');
	}
?>
	<p class="post-meta">
        <span class="ad-type"><?php echo $type; ?></span> &nbsp; &nbsp;<span class="folder"> &nbsp;</span><?php if ( $post->post_type == 'post' ) the_category(', '); else echo get_the_term_list( $post->ID, APP_TAX_CAT, '', ' - ', '' ); ?> | <span class="owner"><?php if ( $cp_options->ad_gravatar_thumb ) appthemes_get_profile_pic( get_the_author_meta('ID'), get_the_author_meta('user_email'), 16 ); ?><?php the_author_posts_link(); ?></span>
	</p>
<?php
}
}

// De-register the action, and add the above ones instead
function cp_auction_modify_sr_actions() {
	remove_action( 'appthemes_after_post_title', 'ft_ad_loop_meta' );
	add_action( 'appthemes_after_post_title', 'cp_auction_ad_loop_meta' );
}
if( get_option('stylesheet') == "simply-responsive-cp" )
add_action( 'init', 'cp_auction_modify_sr_actions' );


/*
|--------------------------------------------------------------------------
| AUCTION COUNTDOWN TIMER
|--------------------------------------------------------------------------
*/

function cp_auction_countdown_timer_code() {

	global $post;

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$count = did_action('cp_auction_countdown_timer');
	$cp_ad_sold = end_auction($post->ID);
	$cp_start_price = get_post_meta($post->ID,'cp_start_price',true);
	$dt2 = strtotime(get_post_meta($post->ID,'cp_sys_expire_date',true));

	$start_timer = get_option('cp_auction_start_timer');
	$offset = get_option('gmt_offset');
	if($offset < 0) $a = "";
	if($offset >= 0) $a = "+";

	$i = $count;

	if ( $start_timer == "yes" ) $last_bid = cp_auction_plugin_get_latest_bid($post->ID);
	if( ( $dt2 && $cp_start_price > 0 && $start_timer != "yes" ) || ( $start_timer == "yes" && $last_bid > $cp_start_price ) )  { ?>

	<div class="clear-timer"></div>

	    <div class="timer"><strong><?php _e('Expires:', 'auctionPlugin'); ?> <font color="navy"><?php if($cp_ad_sold == 'yes') { echo ''.__('Auction is closed.','auctionPlugin').''; ?></font></strong><div class="clr"></div>
			<?php } else
				{ ?></font></strong><meta scheme="countdown<?php echo $i; ?>" name="event_msg" content="<?php _e('Auction is closed.', 'auctionPlugin'); ?>" /><meta scheme="countdown<?php echo $i; ?>" name="d_unit" content=" <?php _e('day', 'auctionPlugin'); ?>" /><meta scheme="countdown<?php echo $i; ?>" name="d_units" content=" <?php _e('days', 'auctionPlugin'); ?>" /><font color="navy"><strong><span id="countdown<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></strong></font>

	<?php if( get_option('stylesheet') == 'ultraclassifieds' ) { ?>
	<div class="clear5"></div>
	<?php } else { ?>
	<div class="clr"></div>
	<?php } ?>

	<?php } ?>
	</div>

	<?php
	}
}
add_action('cp_auction_countdown_timer', 'cp_auction_countdown_timer_code');


function cp_auction_view_countdown() {

	global $post;
	$pos = get_option('cp_auction_timer');
	$list = explode(',', $pos['cp_disable_timer']);
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

	if ( ( is_single() || ! in_the_loop() || in_array("pages", $list) ) || ( $my_type != "normal" && $my_type != "reverse" ) )
		return;

	$cp_ad_sold = end_auction($post->ID);

		if( $cp_ad_sold == "yes" ) { ?>
		<div class="clear-timer"></div>
		<div class="timer">
		<strong>
		<?php _e('Expires:', 'auctionPlugin'); ?> <font color="navy"><?php _e('Auction is closed.','auctionPlugin'); ?></font></strong>
		</div>
		<?php if( get_option('stylesheet') == 'ultraclassifieds' ) { ?>
		<div class="clear5"></div>
		<?php } else { ?>
		<div class="clr"></div>
		<?php } ?>
		<?php } else {
			cp_auction_countdown_timer();
		}

}
add_action('appthemes_before_post_content', 'cp_auction_view_countdown');


/*
|--------------------------------------------------------------------------
| DISPLAY THE CRITIC RATING STARS IN AD LISTINGS
|--------------------------------------------------------------------------
*/


function cp_auction_view_critic_stars() {

	if( is_single() || ! in_the_loop() )
		return;

	global $critic_options, $post;

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if ( function_exists( 'display_critic_reviews' ) ) {
	$AT_Critic = new AT_Critic;
	$critic = $AT_Critic->critic_mini_ratings();
	} else {
	$critic = false;
	}

	if ( isset( $critic_options["$my_type"] ) ) {

		echo $critic;

	}
}
add_action('appthemes_before_post_content', 'cp_auction_view_critic_stars');


/*
|--------------------------------------------------------------------------
| DISPLAY CART HOOK - STANDARD ON TOP OF SINGLE AD LISTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_display_cart_code() {

	global $post, $wpdb, $current_user, $cpurl;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$user = get_userdata( $uid );
	$ip_address = cp_auction_users_ip();
	$enabl = get_option( 'cp_auction_enable_conversion' );
	$cuto = get_option( 'cp_auction_convert_to' );

?>

<script type="text/javascript">
	function update_pending(id)
	{
		 var quantity = jQuery("#quantity_"+id).val();
		 var unpaid = jQuery("#unpaid_"+id).val();

		 jQuery.ajax({
				method: 'get',
				url : '<?php echo cp_auction_plugin_url('url'); ?>/index.php/?_update_pending='+id+'&quantity='+quantity+'&unpaid='+unpaid,
				dataType : 'text',
				success: function() { window.location.reload(true); }
				});
}
</script>

	<?php if( isset( $_POST['short_cp_buy_now'] ) || isset( $_GET['cart'] ) ) { ?>

	<div class="shadowblock_out">

		<div class="shadowblock">

			<div id="cart-hook" class="aws-box">

	<h2 class="dotted"><?php _e('Shopping Cart', 'auctionPlugin'); ?></h2>

	<?php } ?>

<?php
		if( isset( $_POST['short_cp_buy_now'] ) ) {
		if( isset( $_POST['pid'] ) ) $pid = $_POST['pid'];
		if( ( $post->ID ) && ( $pid == $post->ID ) ) {
			cp_auction_buynow_onpost( $pid );
		} elseif( $pid > 0 ) {
			cp_auction_buynow_onpost( $pid );
		}
	}

?>

	<p><?php _e('Any fee is calculated based on the choice of payment method during the next step, checkout.', 'auctionPlugin'); ?></p>

	<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column"><div style="text-align: left;"><?php _e('Product', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone"><?php _e('Price', 'auctionPlugin'); ?></th>
		<th data-hide="phone"><?php _e('Quantity', 'auctionPlugin'); ?></th>
		<th data-hide="phone"><?php _e('Sum', 'auctionPlugin'); ?></th>
		<th class="footable-last-column" data-hide="phone"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY uid ASC");
	if( $results ) {
	foreach( $results as $res ) {

	$seller = $res->uid;
	$cs = get_user_meta( $seller, 'cp_currency', true );
	if( empty( $cs ) ) $cs = $cp_options->curr_symbol;
	$counts = $wpdb->get_var( "SELECT COUNT(uid) FROM {$table_name} WHERE uid = '$seller' AND buyer = '$uid' AND unpaid > '0' AND paid != '2'" );
	$wpdb->query( "UPDATE {$table_name} SET txn_id = '' WHERE uid = '$seller' AND buyer = '$uid' AND unpaid > '0' AND paid != '2'" );
	$latest = $wpdb->get_var( "SELECT id FROM {$table_name} WHERE uid = '$seller' AND buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY id DESC LIMIT 1" );
	$wpdb->query( "UPDATE {$table_name} SET txn_id = '$counts' WHERE id = '$latest'" );
	}
}

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY uid ASC", ARRAY_A);
	$i=1;
	if( $items ) {
	$rowclass = '';
	foreach( $items as $item ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$seller = $item['uid'];
	$counts = $wpdb->get_var( "SELECT COUNT(uid) FROM {$table_name} WHERE uid = '$seller' AND buyer = '$uid' AND unpaid > '0' AND paid != '2'" );

	$id = $item['id'];
	$pid = $item['pid'];
	$item_name = $item['item_name'];
	$amount = $item['amount'];
	$discount = $item['discount'];
	$unpaid = $item['unpaid'];
	$count = $item['txn_id'];
	$total = ($amount - $discount) * $unpaid;
	$item_price = $amount - $discount;
	$ads_url = '<a title="'.$item_name.'" href="'.get_permalink($pid).'">'.truncate($item_name, 15).'</a>';
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );
	$author = get_userdata( $seller );
	$howmany = get_post_meta( $pid, 'cp_auction_howmany', true );
	$howmany_added = get_post_meta( $pid, 'cp_auction_howmany_added', true );

	$not = false;
	$shipping = false;
	$cp_shipping_options = get_post_meta( $pid, 'cp_shipping_options', true );
	$shipping_options = get_localized_field_values( 'cp_shipping_options', $cp_shipping_options );
	if ( $shipping_options == "" ) $not = '<br /><span class="small-text6" style="color: red">'.__( 'This product can not be shipped!', 'auctionPlugin' ).'</span>';
	else if ( $shipping_options == "Free delivery" ) $not = '<br /><span class="small-text6">'.__( 'Free Shipping', 'auctionPlugin' ).'</span>';

   echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column">'.$ads_url.''.$not.'</td>
	<td class="text-right">'.cp_display_price( $item_price, $cs, false ).'</td>';
	if( $howmany < 2 && $howmany_added != "unlimited" ) {
   echo '<td class="text-center">1</td>';
   } else if ( $item['ref'] == "process" ) {
   echo '<td align="center">'.$unpaid.'</td>';
	} else {
   echo '<td class="text-right"><input id="unpaid_'.$id.'" name="unpaid" value="'.$unpaid.'" type="hidden"><input class="cart-quantity" id="quantity_'.$id.'" name="quantity" value="'.$unpaid.'" style="text-align:right;" type="text"></td>';
	}
   echo '<td class="text-right">'.cp_display_price( $total, $cs, false ).'</td>
	<td class="text-center footable-last-column">'; if( $my_type == "classified" && $item['ref'] != "process" ) { echo '<a title="'.__('Adjust the quantity of this product.', 'auctionPlugin').'" href="javascript: void(0)" onclick="update_pending(\''.$id.'\')">'.__('Update', 'auctionPlugin').'</a> | '; } echo '<a title="'.__('Contact', 'auctionPlugin').' '.$author->first_name.' '.$author->last_name.' ('.$author->user_login.')" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$seller.'').'">'.__('Contact', 'auctionPlugin').'</a> | <a href="#" class="delete-complete-purchase" title="'.__('Remove this product from cart.', 'auctionPlugin').'" id="'.$id.'">'.__('Delete', 'auctionPlugin').'</a></td>
	</tr>';

	if ( $counts == $count ) {
	$cto = false;
	$sub_total = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(net_total) FROM {$table_name} WHERE uid = %s AND buyer = %s AND unpaid > %s AND paid != %s", $seller, $uid, '0', '2' ) );

	$country = get_user_meta( $uid, 'cp_country', true );
	if ( empty( $country ) && get_option('cp_auction_enable_geoip') == "yes" )
	$country = do_shortcode( '[geoip_detect2 property="country"]' );

	$free_shipping = get_user_meta( $seller, 'cp_auction_free_shipping', true );
	$shipping = cp_auction_calculate_shipping( $seller, $uid );
	if ( ( $free_shipping > '0' ) && ( $sub_total >= $free_shipping ) ) $shipping = false;

	$sum_total = $sub_total + $shipping;

	if ( $sum_total > 0 )
	//if ( $enabl == "yes" && class_exists("Currencyr") ) $cto = '('.currencyr_exchange( $sum_total, strtolower( $cuto ) ).' '.$cuto.')';
   echo '<tr class="cart"><td colspan="4" class="text-left"><strong>'.__('Total from vendor', 'auctionPlugin').' <a title="'.__('Contact', 'auctionPlugin').' '.$author->first_name.' '.$author->last_name.' ('.$author->user_login.')" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$seller.'').'">'.$author->user_login.'</a>: '.cp_display_price( $sum_total, $cs, false ).'</strong> '.$cto.''; if( $shipping > 0 ) echo ' ('.__('incl.', 'auctionPlugin').' '.cp_display_price( $shipping, $cs, false ).' '.__('shipping', 'auctionPlugin').')'; echo ''.cp_auction_geoip_cart( $pid, $seller, $country ).''; echo '</td><td colspan="1" class="text-right"><input type="button" class="mbtn btn_orange cp-width" onClick="parent.location=\''.cp_auction_url($cpurl['cart'], '?shopping_cart=1', 'confirm-order='.$seller.'').'\'" value="'.__('Place Order', 'auctionPlugin').'"></td></tr>';

	$sum += $sum_total;
	$sum_shipping += $shipping;
	}
	}

	if( $sum > 0 ) {
	$cto1 = false; //if ( $enabl == "yes" && class_exists("Currencyr") ) $cto1 = '('.currencyr_exchange( $sum, strtolower( $cuto ) ).' '.$cuto.')';
	echo '<tr><td colspan="5"></td></tr>';
	echo '<tr class="cart"><td colspan="5" class="text-right"><strong>'.__('Cart Total:', 'auctionPlugin').' '.cp_display_price( $sum, $cs, false ).'</strong> '.$cto1.''; if( $shipping > 0 ) echo ' ('.__('incl.', 'auctionPlugin').' '.cp_display_price( $sum_shipping, $cs, false ).' '.__('shipping', 'auctionPlugin').')'; else ' - '.$not.'';
	}
}
else {
   echo '<tr><td colspan="5">'.__('Your cart is currently empty!', 'auctionPlugin').'</td></tr>';
   }

	?>
	</tbody>
	</table>

<div class="clear20"></div>

<p style="text-align: center; float: right;"><?php if( isset( $_POST['short_cp_buy_now'] ) || isset( $_GET['cart'] ) ) { ?><input type="button" onClick="window.open('<?php echo get_permalink($post->ID); ?>','_self')" value="<?php _e('Close Cart', 'auctionPlugin'); ?>">&nbsp;&nbsp;&nbsp;<?php } ?><input type="button" class="mbtn btn_orange cp-width" onClick="parent.location='<?php echo get_bloginfo('wpurl'); ?>'" value="<?php _e('Continue Shopping', 'auctionPlugin'); ?>"></p>

<div class="clr"></div>

	<?php if( isset( $_POST['short_cp_buy_now'] ) || isset( $_GET['cart'] ) ) { ?>

		</div><!-- /cart-hook aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php
}
}
add_action( 'cp_auction_display_cart', 'cp_auction_display_cart_code' );
if( isset( $_POST['short_cp_buy_now'] ) || isset( $_GET['cart'] ) ) {
add_action( 'appthemes_before_loop', 'cp_auction_display_cart_code' );
add_action( 'appthemes_before_blog_loop', 'cp_auction_display_cart_code' );
add_action( 'appthemes_before_page_loop', 'cp_auction_display_cart_code' );
}