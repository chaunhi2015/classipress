<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	global $cp_options;


/*
|--------------------------------------------------------------------------
| ADD EXTRA PROFILE FIELDS
|--------------------------------------------------------------------------
*/

add_action( 'show_user_profile', 'cp_auction_extra_profile_fields' );
add_action( 'edit_user_profile', 'cp_auction_extra_profile_fields' );

function cp_auction_extra_profile_fields( $user ) {

	global $cp_options;
	$new_class = "";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}

	$act = get_option('cp_auction_plugin_auctiontype');
	$if_allow_anonymous = get_option( 'cp_auction_plugin_bid_anonymous' );
	$cp_allow_anonymous = get_user_meta( $user->ID, 'cp_anonymous', true );
	$cp_use_safe_payment = get_user_meta( $user->ID, 'cp_use_safe_payment', true );
	$pp_required = get_option('cp_auction_disable_paypal');

?>

<script type="text/javascript">
jQuery(document).ready(function($) {
$('#cp_city').change(function(){
if($('#cp_city').val() === 'Other')
   {
   $('#other').show();
   $('#cp_city').hide();
   }
else
   {
   $('#other').hide();
document.getElementById("other").disabled = true;
   }
});
});
</script>

	<table class="form-table">

		<?php if( get_option('cp_version') > "3.2.1" ) { ?>
		<tr>
			<th><label for="paypal_email"><?php if( $pp_required == "required" ) echo '<font color="red">*</font>'; ?> <?php _e('PayPal Email:', 'auctionPlugin'); ?></label></th>

			<td>
				<input type="text" name="paypal_email" id="paypal_email" value="<?php echo esc_attr( get_the_author_meta( 'paypal_email', $user->ID ) ); ?>" class="regular-text" /><br />

				<span class="description"><?php _e('Please enter your PayPal Email.', 'auctionPlugin'); ?></span>

			</td>
		</tr>
		<?php } if ( get_option( 'cp_auction_disable_company' ) != "yes" ) { ?>
		<tr>
			<th><label for="company_name"><?php _e('Company Name:', 'auctionPlugin'); ?></label></th>
			<td>
				<input type="text" name="company_name" id="company_name" value="<?php echo esc_attr( get_the_author_meta( 'company_name', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your Company Name.', 'auctionPlugin'); ?></span>
			</td>
		</tr>
		<?php } if ( get_option( 'cp_auction_disable_address' ) != "yes" ) { ?>
		<tr>
			<th><label for="cp_street"><?php _e('Address:', 'auctionPlugin'); ?></label></th>
			<td>
				<input type="text" name="cp_street" id="cp_street" value="<?php echo esc_attr( get_the_author_meta( 'cp_street', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your Street / Address.', 'auctionPlugin'); ?></span>
			</td>
		</tr>
		<?php } if ( get_option( 'cp_auction_disable_zipcode' ) != "yes" ) { ?>
		<tr>
			<th><label for="cp_zipcode"><?php _e('ZIP Code:', 'auctionPlugin'); ?></label></th>
			<td>
				<input type="text" name="cp_zipcode" id="cp_zipcode" value="<?php echo esc_attr( get_the_author_meta( 'cp_zipcode', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your Post / Zip code.', 'auctionPlugin'); ?></span>
			</td>
		</tr>
		<?php } if ( get_option( 'cp_auction_disable_city' ) != "yes" ) { ?>
		<tr>
			<th><label for="cp_city"><?php _e('City:', 'auctionPlugin'); ?></label></th>
			<td>
				<br />
				<input type="text" id="other" name="cp_city" value="<?php echo esc_attr( get_the_author_meta( 'cp_city', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your Town / City.', 'auctionPlugin'); ?></span>
			</td>
		</tr>
		<?php } if ( get_option( 'cp_auction_disable_state' ) != "yes" ) { ?>
		<tr>
			<th><label for="cp_state"><?php _e('State:', 'auctionPlugin'); ?></label></th>
			<td>
				<input type="text" name="cp_state" id="cp_state" value="<?php echo esc_attr( get_the_author_meta( 'cp_state', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your State.', 'auctionPlugin'); ?></span>
			</td>
		</tr>
		<?php } if ( get_option( 'cp_auction_disable_country' ) != "yes" ) { ?>
		<tr>
			<th><label for="cp_country"><?php _e('Country:', 'auctionPlugin'); ?></label></th>
			<td>
				<?php cp_auction_get_dropdown('cp_country', $user->ID); ?>
				<div class="clear10"></div>
				<span class="description"><?php _e('Please select your Country.', 'auctionPlugin'); ?></span>
			</td>
		</tr>
		<?php } if ( get_option( 'cp_auction_disable_phone' ) != "yes" ) { ?>
		<tr>
			<th><label for="cp_phone"><?php _e('Phone:', 'auctionPlugin'); ?></label></th>
			<td>
				<input type="text" id="cp_phone" name="cp_phone" value="<?php echo esc_attr( get_the_author_meta( 'cp_phone', $user->ID ) ); ?>" class="regular-text" /><br />

				<span class="description"><?php _e('Please enter your phone.', 'auctionPlugin'); ?></span>
			</td>
		</tr>

		<?php } if(( $act != "webshop" ) && ( $if_allow_anonymous == "yes" )) { ?>
		<tr>
			<th><label for="cp_allow_anonymous"><?php _e('Anonymous Bids:', 'auctionPlugin'); ?></label></th>

			<td>
				<select class="<?php echo $new_class; ?>" name="cp_allow_anonymous" id="cp_allow_anonymous">
    				<option value=""><?php _e('Select', 'auctionPlugin'); ?></option>
    				<option value="yes" <?php if($cp_allow_anonymous == "yes") echo 'selected="selected"'; ?>><?php _e('Yes', 'auctionPlugin'); ?></option>
    				<option value="no" <?php if($cp_allow_anonymous == "no") echo 'selected="selected"'; ?>><?php _e('No', 'auctionPlugin'); ?></option>
    				</select>
    				<div class="clear5"></div>
    				<span class="description"><?php _e('Select yes if you want to allow anonymous bids.', 'auctionPlugin'); ?></span>
			</td>
		</tr>
		<?php } if(( get_option('cp_auction_plugin_pay_user') == "yes" ) && ( get_option('cp_auction_plugin_pay_admin') == "yes" )) { ?>
		<tr>
			<th><label for="cp_use_safe_payment"><?php _e('Safe Payments:', 'auctionPlugin'); ?></label></th>

			<td>
				<select class="<?php echo $new_class; ?>" name="cp_use_safe_payment" id="cp_use_safe_payment">
    				<option value=""><?php _e('Select', 'auctionPlugin'); ?></option>
    				<option value="no" <?php if($cp_use_safe_payment == "no") echo 'selected="selected"'; ?>><?php _e('No', 'auctionPlugin'); ?></option>
    				<option value="yes" <?php if($cp_use_safe_payment == "yes") echo 'selected="selected"'; ?>><?php _e('Yes', 'auctionPlugin'); ?></option>
    				</select>
    				<div class="clear5"></div>
				<span class="description"><?php _e('Set this option to yes if you want to use safe payments. Buyer have to pay for the product/item purchased to admin, and you will receive your money when the buyer has received their purchased product/item.', 'auctionPlugin'); ?></span>
			</td>
		</tr>
		<?php } ?>

	</table>
<?php }


/*
|--------------------------------------------------------------------------
| SAVE THE EXTRA PROFILE FIELDS
|--------------------------------------------------------------------------
*/

add_action( 'personal_options_update', 'cp_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'cp_save_extra_profile_fields' );

function cp_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	if( isset( $_POST['paypal_email'] ) && get_option( 'cp_auction_disable_paypal' ) != "yes" ) {
	update_user_meta( $user_id, 'paypal_email', $_POST['paypal_email'] );
	} if ( isset( $_POST['company_name'] ) && get_option( 'cp_auction_disable_company' ) != "yes" ) {
	update_user_meta( $user_id, 'company_name', $_POST['company_name'] );
	} if ( isset( $_POST['cp_street'] ) && get_option( 'cp_auction_disable_address' ) != "yes" ) {
	update_user_meta( $user_id, 'cp_street', $_POST['cp_street'] );
	} if ( isset( $_POST['cp_zipcode'] ) && get_option( 'cp_auction_disable_zipcode' ) != "yes" ) {
	update_user_meta( $user_id, 'cp_zipcode', $_POST['cp_zipcode'] );
	} if ( isset( $_POST['cp_city'] ) && get_option( 'cp_auction_disable_city' ) != "yes" ) {
	update_user_meta( $user_id, 'cp_city', $_POST['cp_city'] );
	} if ( isset( $_POST['cp_state'] ) && get_option( 'cp_auction_disable_state' ) != "yes" ) {
	update_user_meta( $user_id, 'cp_state', $_POST['cp_state'] );
	} if ( isset( $_POST['cp_country'] ) && get_option( 'cp_auction_disable_country' ) != "yes" ) {
	update_user_meta( $user_id, 'cp_country', $_POST['cp_country'] );
	} if ( isset( $_POST['cp_phone'] ) && get_option( 'cp_auction_disable_phone' ) != "yes" ) {
	update_user_meta( $user_id, 'cp_phone', $_POST['cp_phone'] );
	}
	if( isset( $_POST['cp_allow_anonymous'] ) )
	update_user_meta( $user_id, 'cp_allow_anonymous', $_POST['cp_allow_anonymous'] );
	if( isset( $_POST['cp_use_safe_payment'] ) )
	update_user_meta( $user_id, 'cp_use_safe_payment', $_POST['cp_use_safe_payment'] );
}


/*
|--------------------------------------------------------------------------
| CHECK FILLED IN PROFILE
|--------------------------------------------------------------------------
*/

function check_profile_settings($uid = '') {

	global $current_user;
	$current_user = wp_get_current_user();
	if( empty( $uid ) )
	$uid = $current_user->ID;

	$can = current_user_can( 'manage_options' );

	if( get_option('cp_auction_disable_company') == "required" && ! $can && get_user_meta( $uid, 'company_name', true ) == false ) return false;
	else if( get_option('cp_auction_disable_address') == "required" && ! $can && get_user_meta( $uid, 'cp_street', true ) == false ) return false;
	else if( get_option('cp_auction_disable_zipcode') == "required" && ! $can && get_user_meta( $uid, 'cp_zipcode', true ) == false ) return false;
	else if( get_option('cp_auction_disable_city') == "required" && ! $can && get_user_meta( $uid, 'cp_city', true ) == false ) return false;
	else if( get_option('cp_auction_disable_state') == "required" && ! $can && get_user_meta( $uid, 'cp_state', true ) == false ) return false;
	else if( get_option('cp_auction_disable_cuntry') == "required" && ! $can && get_user_meta( $uid, 'cp_country', true ) == false ) return false;
	else if( get_option('cp_auction_disable_phone') == "required" && ! $can && get_user_meta( $uid, 'cp_phone', true ) == false ) return false;
	else if( get_option('cp_auction_disable_paypal') == "required" && ! $can && get_user_meta( $uid, 'paypal_email', true ) == false ) return false;
	else return true;

}


/*
|--------------------------------------------------------------------------
| DISPLAY THE CUSTOM REGISTRATION FIELDS
|--------------------------------------------------------------------------
*/

function cp_auction_custom_reg_fields() {

	if ( get_option( 'cp_auction_enable_account_type' ) !== "yes" )
		return;

?>

	<div class="clr"></div>
	<p class="cp-account-type">
		<label for="cp_account_type"><?php _e( 'Account Type:', 'auctionPlugin' ); ?></label>
		<input type="radio" name="cp_account_type" value="private" class="cp-account-type-radio"><span class="cp-account-type-label"><?php _e( 'Private Account.', 'auctionPlugin' ); ?></span> <input type="radio" name="cp_account_type" value="company" class="cp-account-type-radio"><span class="cp-account-type-label"><?php _e( 'Business Account.', 'auctionPlugin' ); ?></span>
	</p>

<?php
}
// Display the custom fields on registration form
add_action( 'register_form', 'cp_auction_custom_reg_fields' );


/*
|--------------------------------------------------------------------------
| VALIDATE THE CUSTOM REGISTRATION FIELDS
|--------------------------------------------------------------------------
*/

function cp_auction_validate_custom_reg_fields( $login, $email, $errors ) {

	if ( get_option( 'cp_auction_enable_account_type' ) !== "yes" )
		return;

  // fields to be validated
  $fields = array(
	'cp_account_type' => __( 'Account Type', 'auctionPlugin' )
  );

  // check for empty required fields an display notice
  foreach ( $fields as $field => $value ) {
	if ( empty( $_POST[$field] ) ) {
		$errors->add('empty_fields', __('<strong>ERROR</strong>: Please select an appropriate account type.', 'auctionPlugin'));
	}
  }

}
// Validate custom fields
add_action( 'register_post', 'cp_auction_validate_custom_reg_fields', 10, 3 );


/*
|--------------------------------------------------------------------------
| REGISTER THE EXTRA FIELDS AS USER META
|--------------------------------------------------------------------------
*/

function cp_auction_register_custom_fields( $user_id, $password = "", $meta = array() ) {

	if ( get_option( 'cp_auction_enable_account_type' ) !== "yes" )
		return;

	// custom fields
	$fields = array(
		'cp_account_type'
	);
        // cleans and updates the custom fields
	foreach ( $fields as $field ) {
	    $value = stripslashes( trim( $_POST[$field] ) ) ;
	    if ( ! empty( $value ) ) {
	  	 update_user_meta( $user_id, $field, $value );
	    }
	}

}
// Save the custom fields to the database as soon as the user is registered on the database
add_action( 'user_register', 'cp_auction_register_custom_fields' );