<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if( get_option('cp_auction_plugin_if_avatar') == "yes" ) {

class CP_Auction_Avatars {
	private $edit_cp_user_id;

	public function __construct() {
		add_filter( 'get_avatar', array( $this, 'get_avatar' ), 10, 5 );
		add_action( 'admin_init', array( $this, 'admin_init' ) );
		add_action( 'show_user_profile', array( $this, 'edit_user_profile' ) );
		add_action( 'edit_user_profile', array( $this, 'edit_user_profile' ) );
		add_action( 'personal_options_update', array( $this, 'edit_user_profile_update' ) );
		add_action( 'edit_user_profile_update', array( $this, 'edit_user_profile_update' ) );
		add_filter( 'avatar_defaults', array( $this, 'avatar_defaults' ) );
	}

	public function get_avatar( $avatar = '', $id_or_email, $size = 96, $default = '', $alt = false ) {

		if ( is_numeric($id_or_email) )
			$user_id = (int) $id_or_email;
		elseif ( is_string( $id_or_email ) && ( $user = get_user_by( 'email', $id_or_email ) ) )
			$user_id = $user->ID;
		elseif ( is_object( $id_or_email ) && ! empty( $id_or_email->user_id ) )
			$user_id = (int) $id_or_email->user_id;

		if ( empty( $user_id ) )
			return $avatar;

		$avatars = get_user_meta( $user_id, 'cp_auction_avatar', true );

		if ( empty( $avatars ) || empty( $avatars['full'] ) )
			return $avatar;

		$size = (int) $size;

		if ( empty( $alt ) )
			$alt = get_the_author_meta( 'display_name', $user_id );

		if ( empty( $avatars[$size] ) ) {
			$upload_path = wp_upload_dir();
			$avatar_full_path = str_replace( $upload_path['baseurl'], $upload_path['basedir'], $avatars['full'] );
			//$image_sized = image_resize( $avatar_full_path, $size, $size, true );
			$editor = wp_get_image_editor( $avatar_full_path );
			if ( is_wp_error( $editor ) )
    				return $editor;
			$editor->set_quality( 100 );
			$resized = $editor->resize( $size, $size, true );
			$dest_file = $editor->generate_filename( NULL, NULL );
			$saved = $editor->save( $dest_file );
			if ( is_wp_error( $saved ) )
    				return $saved;
			$image_sized = $dest_file;

			$avatars[$size] = is_wp_error($image_sized) ? $avatars[$size] = $avatars['full'] : str_replace( $upload_path['basedir'], $upload_path['baseurl'], $image_sized );

			update_user_meta( $user_id, 'cp_auction_avatar', $avatars );
		} elseif ( substr( $avatars[$size], 0, 4 ) != 'http' ) {
			$avatars[$size] = home_url( $avatars[$size] );
		}

		$author_class = is_author( $user_id ) ? ' current-author' : '' ;
		$avatar = "<img alt='" . esc_attr( $alt ) . "' src='" . $avatars[$size] . "' class='avatar avatar-{$size}{$author_class} photo' height='{$size}' width='{$size}' />";

		return apply_filters( 'cp_auction_avatar', $avatar );
	}

	public function admin_init() {

		register_setting( 'discussion', 'cp_auction_avatars_enable', array( $this, 'sanitize_options' ) );
		add_settings_field( 'cp-auction-avatars-caps', __('Avatar Permissions','auctionPlugin'), array( $this, 'avatar_settings_field' ), 'discussion', 'avatars' );
	}

	public function sanitize_options( $input ) {
		$new_input['cp_auction_avatars_enable'] = empty( $input['cp_auction_avatars_enable'] ) ? 0 : 1;
		return $new_input;
	}

	public function avatar_settings_field( $args ) {		
		$options = get_option('cp_auction_avatars_enable');

		echo '
			<label for="cp_auction_avatars_enable">
				<input type="checkbox" name="cp_auction_avatars_enable" id="cp_auction_avatars_enable" value="1" ' . @checked( $options['cp_auction_avatars_enable'], 1, false ) . ' />
				' . __('Only allow users with file upload capabilities to upload avatars, Authors and above.','auctionPlugin') . '
			</label>
		';
	}

	public function edit_user_profile( $userprofile ) {
	?>
	
	<table class="form-table">
		<tr>
			<th><label for="cp-auction-avatar"><?php _e('Upload Avatar:','auctionPlugin'); ?></label></th>
			<td style="width: 50px;" valign="top">
				<?php echo get_avatar( $userprofile->ID ); ?>
			</td>
			<td style="padding-left: 10px; vertical-align:top;">
			<?php
				$options = get_option('cp_auction_avatars_enable');

				if ( empty($options['cp_auction_avatars_enable']) || current_user_can('upload_files') ) {
					do_action( 'cp_auction_avatar_notices' ); 
					wp_nonce_field( 'cp_auction_avatar_nonce', '_cp_auction_avatar_nonce', false ); 
			?>
					<input type="file" name="cp-auction-avatar" id="cp-auction-avatar" /><br />
			<?php
					if ( empty( $userprofile->cp_auction_avatar ) )
						echo '<span class="description">' . __('No avatar is set up. Use the upload field to add your own avatar.','auctionPlugin') . '</span>';
					else 
						echo '
							<input type="checkbox" name="cp-auction-avatar-erase" value="1" /> ' . __('Delete avatar','auctionPlugin') . '<br />
							<span class="description">' . __('Replace the avatar by uploading a new avatar, or erase the avatar (falling back to a set up gravatar) by checking the delete option.','auctionPlugin') . '</span>
						';
				} else {
					if ( empty( $userprofile->cp_auction_avatar ) )
						echo '<span class="description">' . __('No avatar is set up. Set up your avatar at Gravatar.com.','auctionPlugin') . '</span>';
					else 
						echo '<span class="description">' . __('You do not have management permissions. To change your avatar, contact the website administrator.','auctionPlugin') . '</span>';
				}
			?>
			</td>
		</tr>
	</table>
	<script type="text/javascript">var form = document.getElementById('your-profile');form.encoding = 'multipart/form-data';form.setAttribute('enctype', 'multipart/form-data');</script>
	<?php
	}

	public function edit_user_profile_update( $user_id ) {
		if ( ! isset( $_POST['_cp_auction_avatar_nonce'] ) || ! wp_verify_nonce( $_POST['_cp_auction_avatar_nonce'], 'cp_auction_avatar_nonce' ) )
			return;

		if ( ! empty( $_FILES['cp-auction-avatar']['name'] ) ) {
			$mimes = array(
				'jpg|jpeg|jpe' => 'image/jpeg',
				'gif' => 'image/gif',
				'png' => 'image/png',
				'bmp' => 'image/bmp',
				'tif|tiff' => 'image/tiff'
			);

			if ( ! function_exists( 'wp_handle_upload' ) )
				require_once( ABSPATH . 'wp-admin/includes/file.php' );

			$this->avatar_delete( $user_id );
			if ( strstr( $_FILES['cp-auction-avatar']['name'], '.php' ) )
				wp_die('For security reasons, the extension ".php" cannot be in your file name.');

			$this->edit_cp_user_id = $user_id;
			$avatar = wp_handle_upload( $_FILES['cp-auction-avatar'], array( 'mimes' => $mimes, 'test_form' => false, 'unique_filename_callback' => array( $this, 'unique_filename_callback' ) ) );

			if ( empty($avatar['file']) ) {
				switch ( $avatar['error'] ) {
					case 'File type does not meet security guidelines. Try another.' :
						add_action( 'user_profile_update_errors', create_function('$a','$a->add("avatar_error",__("Please upload a valid image file for the avatar.","auctionPlugin"));') );
						break;
					default :
						add_action( 'user_profile_update_errors', create_function('$a','$a->add("avatar_error","<strong>".__("There was an error uploading the avatar:","auctionPlugin")."</strong> ' . esc_attr( $avatar['error'] ) . '");') );
				}

				return;
			}

			update_user_meta( $user_id, 'cp_auction_avatar', array( 'full' => $avatar['url'] ) );
		} elseif ( ! empty( $_POST['cp-auction-avatar-erase'] ) ) {
			$this->avatar_delete( $user_id );
		}
	}

	public function avatar_defaults( $avatar_defaults ) {
		remove_action( 'get_avatar', array( $this, 'get_avatar' ) );
		return $avatar_defaults;
	}

	public function avatar_delete( $user_id ) {
		$old_avatars = get_user_meta( $user_id, 'cp_auction_avatar', true );
		$upload_path = wp_upload_dir();

		if ( is_array($old_avatars) ) {
			foreach ($old_avatars as $old_avatar ) {
				$old_avatar_path = str_replace( $upload_path['baseurl'], $upload_path['basedir'], $old_avatar );
				@unlink( $old_avatar_path );	
			}
		}

		delete_user_meta( $user_id, 'cp_auction_avatar' );
	}

	public function unique_filename_callback( $dir, $name, $ext ) {
		$user = get_user_by( 'id', (int) $this->edit_cp_user_id ); 
		$name = $base_name = sanitize_file_name( $user->display_name . '_avatar' );
		$number = 1;
		
		while ( file_exists( $dir . "/$name$ext" ) ) {
			$name = $base_name . '_' . $number;
			$number++;
		}
				
		return $name . $ext;
	}
}

$cp_auction_avatars = new CP_Auction_Avatars;

function get_cp_auction_avatar( $id_or_email, $size = '96', $default = '', $alt = false ) {
	global $cp_auction_avatars;
	$avatar = $cp_auction_avatars->get_avatar( '', $id_or_email, $size, $default, $alt );

	if ( empty ( $avatar ) )
		$avatar = get_avatar( $id_or_email, $size, $default, $alt );

	return $avatar;
}
}