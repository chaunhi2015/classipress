<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| REWRITE URL GLOBALS
|--------------------------------------------------------------------------
*/

	$option = get_option( 'cp_auction_page_slugs' );

	$GLOBALS['cpurl'] = array(
	'rewrite' => isset($option['enable']) == true ? $option['enable'] : '',
	'log' => isset($option['log']) == true ? $option['log'] : '',
	'coupons' => isset($option['coupons']) == true ? $option['coupons'] : '',
	'downloads' => isset($option['downloads']) == true ? $option['downloads'] : '',
	'upload' => isset($option['upload']) == true ? $option['upload'] : '',
	'new' => isset($option['new']) == true ? $option['new'] : '',
	'winner' => isset($option['winner']) == true ? $option['winner'] : '',
	'authors' => isset($option['authors']) == true ? $option['authors'] : '',
	'overview' => isset($option['overview']) == true ? $option['overview'] : '',
	'affiliate' => isset($option['affiliate']) == true ? $option['affiliate'] : '',
	'userpanel' => isset($option['userpanel']) == true ? $option['userpanel'] : '',
	'myads' => isset($option['myads']) == true ? $option['myads'] : '',
	'favorites' => isset($option['favorites']) == true ? $option['favorites'] : '',
	'watchlist' => isset($option['watchlist']) == true ? $option['watchlist'] : '',
	'contact' => isset($option['contact']) == true ? $option['contact'] : '',
	'cart' => isset($option['cart']) == true ? $option['cart'] : '',
	//'bestoffer' => get_option('aws_bo_rewrite_offer')
	);