<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| GET LATEST AUCTIONS / LISTINGS
|--------------------------------------------------------------------------
*/

class cp_auctions_latest_auctions extends WP_Widget {

function __construct() {
        $widget_ops = array( false, 'description' => __('Show latest listings in the sidebar.','auctionPlugin') );
        parent::__construct( false, 'CP Auction Latest Listings', $widget_ops );
}

function widget($args, $instance) {

		extract($args);
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
			else
		echo $before_title;?><?php $arr = get_option('LatestAuctions_widget'); echo $arr['title']; ?><?php echo $after_title;

?>

<script type="text/javascript" src="<?php echo cp_auction_plugin_url(); ?>/js/countdown-widget.js" defer="defer"></script>

	<div style="margin:10px 0;overflow:hidden">

    <?php
		$nbr = $arr['cp_auctions_number'];

					global $wpdb;

				$pageposts = $wpdb->get_results("
					SELECT distinct wposts.* 
					FROM $wpdb->posts wposts 
					WHERE  wposts.post_status = 'publish' 
					AND wposts.post_type = '".APP_POST_TYPE."'  
					ORDER BY wposts.post_date DESC LIMIT $nbr");

				 ?>

					 <?php $t = 0; ?>
					 <?php $i = 0; if ($pageposts): ?>
					 <?php global $post; ?>

                     <?php foreach ($pageposts as $post): ?>
                     <?php setup_postdata($post); ?>
                     <?php echo _cp_auction_get_post_things_widget($post, $t); $i++; $t++; ?>
                     <?php endforeach; ?>
                     <?php else : ?>
                     <div class="padd100"><?php _e('Sorry, there are no listings posted yet.', 'auctionPlugin'); ?></div>
                     <?php endif; ?>
   </div>
<?php

		echo $after_widget;
}

function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

function form( $instance ) {
 $options = get_option("LatestAuctions_widget");
  if (!is_array( $options ))
	{
	$options = array(
      'title' => ''.__('Latest Listings','auctionPlugin').'',
	  'cp_auctions_number' => '5'
      );
  	}    

  if (isset($_POST['submit']))
  {
    $options['title'] = htmlspecialchars($_POST['WidgetTitle']);
	$options['cp_auctions_number'] = htmlspecialchars($_POST['cp_auctions_number']);

    update_option("LatestAuctions_widget", $options);
  }

?>

  <table width="100%">
  <tr>
  <td>
    <label for="WidgetTitle"><?php _e('Widget Title:', 'auctionPlugin'); ?> </label></td>
   <td> <input type="text" id="WidgetTitle" name="WidgetTitle" value="<?php echo $options['title'];?>" /></td>
   </tr>
   <tr>
    <td><label for="cp_auctions_number"><?php _e('Display Number:', 'auctionPlugin'); ?> </label></td>
    <td><input type="text" id="cp_auctions_number" name="cp_auctions_number" value="<?php echo $options['cp_auctions_number'];?>" /></td>
    </tr>
    </table>

    <input type="hidden" id="submit" name="submit" value="1" />
  </p>

  <?php

}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "cp_auctions_latest_auctions" );' ) );


/*
|--------------------------------------------------------------------------
| GET FEATURED AUCTIONS / LISTINGS
|--------------------------------------------------------------------------
*/

class cp_auctions_featured_auctions extends WP_Widget {

function __construct() {
        $widget_ops = array( false, 'description' => __('Show latest featured listings in the sidebar.','auctionPlugin') );
        parent::__construct( false, 'CP Auction Featured Listings', $widget_ops );
}

function widget($args, $instance) {

		extract($args);
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
			else
		echo $before_title;?><?php $arr = get_option('FeaturedAuctions_widget'); echo $arr['title']; ?><?php echo $after_title;
?>

<script type="text/javascript" src="<?php echo cp_auction_plugin_url(); ?>/js/countdown-fwidget.js" defer="defer"></script>

	<div style="margin:10px 0;overflow:hidden">

    <?php

		$nbr = $arr['cp_auctions_number'];
		$pageposts = query_posts( array( 'post_type' => APP_POST_TYPE, 'post__in' => get_option('sticky_posts'), 'posts_per_page' => $nbr ) ); ?>

			<?php $t = 0; ?>
			<?php $i = 0; if ($pageposts): ?>
			<?php global $post; ?>

		<?php foreach ($pageposts as $post): ?>
		<?php setup_postdata($post); ?>

		<?php echo _cp_auction_get_post_things_featured_widget($post, $t); $i++; $t++;?>

		<?php endforeach; ?>
		<?php else : ?>
		<div class="padd100"><?php _e('Sorry, there are no featured listings yet.', 'auctionPlugin'); ?></div>
		<?php endif; ?>

   </div>

<?php
		echo $after_widget;
}

function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

function form( $instance ) {
 $options = get_option("FeaturedAuctions_widget");
  if (!is_array( $options ))
	{

	$options = array(
      'title' => ''.__('Featured Listings','auctionPlugin').'',
	  'cp_auctions_number' => '5'
      );
  	}    

  if (isset($_POST['submit']))
  {
    $options['title'] = htmlspecialchars($_POST['WidgetTitle']);
	$options['cp_auctions_number'] = htmlspecialchars($_POST['cp_auctions_number']);
    update_option("FeaturedAuctions_widget", $options);
  }

?>

  <table width="100%">
  <tr>
  <td>
    <label for="WidgetTitle"><?php _e('Widget Title:', 'auctionPlugin'); ?> </label></td>
   <td> <input type="text" id="WidgetTitle" name="WidgetTitle" value="<?php echo $options['title'];?>" /></td>
   </tr>
   <tr>
    <td><label for="cp_auctions_number"><?php _e('Display Number:', 'auctionPlugin'); ?> </label></td>
    <td><input type="text" id="cp_auctions_number" name="cp_auctions_number" value="<?php echo $options['cp_auctions_number'];?>" /></td>
    </tr>
    </table>

    <input type="hidden" id="submit" name="submit" value="1" />
  </p>

  <?php

}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "cp_auctions_featured_auctions" );' ) );


/*
|--------------------------------------------------------------------------
| GET LAST CHANCE AUCTIONS / LISTINGS
|--------------------------------------------------------------------------
*/

class cp_auctions_last_auctions extends WP_Widget {

function __construct() {
        $widget_ops = array( false, 'description' => __('Show soon to end auctions in the sidebar.','auctionPlugin') );
        parent::__construct( false, 'CP Auction Last Chance', $widget_ops );
}

function widget($args, $instance) {

		extract($args);
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
			else
		echo $before_title;?><?php $arr = get_option('LastAuctions_widget'); echo $arr['title']; ?><?php echo $after_title;
?>

<script type="text/javascript" src="<?php echo cp_auction_plugin_url(); ?>/js/countdown-lwidget.js" defer="defer"></script>

	<div style="margin:10px 0;overflow:hidden">

    <?php

		$nbr = $arr['cp_auctions_number'];

					global $wpdb;
					$hours = get_option('cp_auction_plugin_lastchance');
					$todayDate = current_time('mysql'); // current date
					$currentTime = time($todayDate); //Change date into time
					$newtime = date("Y-m-d H:i:s",strtotime("$todayDate + $hours"));
					$date = strtotime($newtime);

				 $pageposts = $wpdb->get_results("
					SELECT DISTINCT wposts.* 
					FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta2, $wpdb->postmeta wpostmeta3 
					WHERE wposts.ID = wpostmeta2.post_id 
					AND wposts.ID = wpostmeta3.post_id 
					AND (wpostmeta2.meta_key = 'cp_auction_my_auction_type' AND wpostmeta2.meta_value != 'classified') 
					AND (wpostmeta2.meta_key = 'cp_auction_my_auction_type' AND wpostmeta2.meta_value != 'wanted') 
					AND (wpostmeta3.meta_key = 'cp_end_date' AND wpostmeta3.meta_value > '0') 
					AND (wpostmeta3.meta_key = 'cp_end_date' AND wpostmeta3.meta_value < '$date') 
					AND wposts.post_status = 'publish' 
					AND wposts.post_type = '".APP_POST_TYPE."' 
					ORDER BY wposts.post_date DESC LIMIT $nbr");

				 ?>

					 <?php $t = 0; ?>
					 <?php $i = 0; if ($pageposts): ?>
					 <?php global $post; ?>
                     <?php foreach ($pageposts as $post): ?>
                     <?php setup_postdata($post); ?>
                     <?php echo _cp_auction_get_post_things_last_widget($post, $t); $i++; $t++;?>
                     <?php endforeach; ?>
                     <?php else : ?>
                     <div class="padd100"><?php _e('Sorry, no auctions that close soon.', 'auctionPlugin'); ?></div>
                     <?php endif; ?>

   </div>

<?php

		echo $after_widget;
}

function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

function form( $instance ) {
 $options = get_option("LastAuctions_widget");
  if (!is_array( $options ))
	{

	$options = array(
      'title' => ''.__('Auctions Closing Soon','auctionPlugin').'',
	  'cp_auctions_number' => '5'
      );
  	}    

  if (isset($_POST['submit']))
  {

    $options['title'] = htmlspecialchars($_POST['WidgetTitle']);
	$options['cp_auctions_number'] = htmlspecialchars($_POST['cp_auctions_number']);
    update_option("LastAuctions_widget", $options);

  }

?>

  <table width="100%">
  <tr>
  <td>
    <label for="WidgetTitle"><?php _e('Widget Title:', 'auctionPlugin'); ?> </label></td>
   <td> <input type="text" id="WidgetTitle" name="WidgetTitle" value="<?php echo $options['title'];?>" /></td>
   </tr>
   <tr>
    <td><label for="cp_auctions_number"><?php _e('Display Number:', 'auctionPlugin'); ?> </label></td>
    <td><input type="text" id="cp_auctions_number" name="cp_auctions_number" value="<?php echo $options['cp_auctions_number'];?>" /></td>
    </tr>
    </table>

    <input type="hidden" id="submit" name="submit" value="1" />
  </p>

  <?php

}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "cp_auctions_last_auctions" );' ) );


/*
|--------------------------------------------------------------------------
| PLACE AN BID OPTION WIDGET IN SIDEBAR
|--------------------------------------------------------------------------
*/

class cp_auctions_bidder_widget extends WP_Widget {

function __construct() {
        $widget_ops = array( false, 'description' => __('Display an bid option box in the sidebar.','auctionPlugin') );
        parent::__construct( false, 'CP Auction Bid Option', $widget_ops );
}

function widget($args, $instance) {

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if ( $post )
	$imported = get_post_meta( $post->ID, 'a2pItemID', true );
	else $imported = false;

	if(( get_option('cp_auction_enable_bid_widget') == "yes" && is_singular(APP_POST_TYPE) && $imported == false ) && ( $my_type == "normal" || $my_type == "reverse" ) ) {

		extract($args);
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
			else
		echo $before_title;?><?php $arr = get_option('BidderBox_widget'); echo $arr['title']; ?><?php echo $after_title;
?>

<script type="text/javascript" src="<?php echo cp_auction_plugin_url(); ?>/js/countdown-bidwidget.js" defer="defer"></script>

	<div style="margin:10px 0;overflow:hidden">

    <?php cp_auction_bidder_box_widget(); ?>

</div>
<?php

		echo $after_widget;
}
}

function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

function form( $instance ) {
 $options = get_option("BidderBox_widget");
  if (!is_array( $options ))
	{

	$options = array(
	'title' => ''.__('Bid this Auction','auctionPlugin').''
      );
  	}    

  if (isset($_POST['submit']))
  {

    $options['title'] = htmlspecialchars($_POST['WidgetTitle']);
    update_option("BidderBox_widget", $options);

  }

?>

  <table width="100%">
  <tr>
  <td>
    <label for="WidgetTitle"><?php _e('Widget Title:', 'auctionPlugin'); ?> </label></td>
   <td> <input type="text" id="WidgetTitle" name="WidgetTitle" value="<?php echo $options['title'];?>" /></td>
   </tr>

    </table>

    <input type="hidden" id="submit" name="submit" value="1" />
  </p>

  <?php

}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "cp_auctions_bidder_widget" );' ) );


/*
|--------------------------------------------------------------------------
| PLACE AN WANTED OPTION WIDGET IN SIDEBAR
|--------------------------------------------------------------------------
*/

class cp_auctions_wanted_widget extends WP_Widget {

function __construct() {
        $widget_ops = array( false, 'description' => __('Display an Wanted option box in the sidebar.','auctionPlugin') );
        parent::__construct( false, 'CP Auction Wanted Option', $widget_ops );
}

function widget($args, $instance) {

	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$allow_deals = get_post_meta( $post->ID, 'cp_allow_deals', true );
	$closed = get_post_meta($post->ID, 'cp_ad_sold', true);
	$yes = ''.__('Yes','auctionPlugin').'';
	if ( $post )
	$imported = get_post_meta( $post->ID, 'a2pItemID', true );
	else $imported = false;

	if(( get_option('cp_auction_enable_wanted_widget') == "yes" && is_singular(APP_POST_TYPE) && $allow_deals == $yes && $closed != 'yes' ) 
	&& ( empty( $my_type ) || $my_type == 'wanted' || $my_type == 'classified' )) {

		extract($args);
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
			else
		echo $before_title;?><?php $arr = get_option('WantedBox_widget'); echo $arr['title']; ?><?php echo $after_title;
?>

	<div style="margin:10px 0;overflow:hidden">

    <?php cp_auction_wanted_box_widget(); ?>

</div>

<?php

		echo $after_widget;
}
}

function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

function form( $instance ) {
 $options = get_option("WantedBox_widget");
  if (!is_array( $options ))
	{

	$options = array(
	'title' => ''.__('Place Your Offer','auctionPlugin').''
      );
  	}    

  if (isset($_POST['submit']))
  {

    $options['title'] = htmlspecialchars($_POST['WidgetTitle']);
    update_option("WantedBox_widget", $options);

  }

?>

  <table width="100%">
  <tr>
  <td>
    <label for="WidgetTitle"><?php _e('Widget Title:', 'auctionPlugin'); ?> </label></td>
   <td> <input type="text" id="WidgetTitle" name="WidgetTitle" value="<?php echo $options['title'];?>" /></td>
   </tr>

    </table>

    <input type="hidden" id="submit" name="submit" value="1" />
  </p>

  <?php

}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "cp_auctions_wanted_widget" );' ) );


/*
|--------------------------------------------------------------------------
| GET WANTED ADS WIDGET
|--------------------------------------------------------------------------
*/

class cp_auctions_wanted_ads extends WP_Widget {

function __construct() {
        $widget_ops = array( false, 'description' => __('Show latest wanted ads in the sidebar.','auctionPlugin') );
        parent::__construct( false, 'CP Auction Wanted Ads', $widget_ops );
}

function widget($args, $instance) {

		extract($args);
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
			else
		echo $before_title;?><?php $arr = get_option('WantedAds_widget'); echo $arr['title']; ?><?php echo $after_title;

?>

	<div style="margin:10px 0;overflow:hidden">

    <?php
		$nbr = $arr['cp_auctions_number'];

					global $wpdb;

				 $pageposts = $wpdb->get_results("
					SELECT DISTINCT wposts.* 
					FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta2
					WHERE wposts.ID = wpostmeta2.post_id 
					AND (wpostmeta2.meta_key = 'cp_auction_my_auction_type' AND wpostmeta2.meta_value = 'wanted' 
					OR wpostmeta2.meta_key = 'cp_want_to' AND wpostmeta2.meta_value != '') 
					AND wposts.post_status = 'publish' 
					AND wposts.post_type = '".APP_POST_TYPE."' 
					ORDER BY wposts.post_date DESC LIMIT $nbr");

				 ?>

					 <?php $i = 0; if ($pageposts): ?>
					 <?php global $post; ?>

                     <?php foreach ($pageposts as $post): ?>
                     <?php setup_postdata($post); ?>
                     <?php echo _cp_auction_get_wanted_ads_widget($post); $i++;?>
                     <?php endforeach; ?>
                     <?php else : ?>
                     <div class="padd100"><?php _e('Sorry, there are no listings posted yet.', 'auctionPlugin'); ?></div>
                     <?php endif; ?>
   </div>
<?php

		echo $after_widget;
}

function update( $new_instance, $old_instance ) {

		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

function form( $instance ) {
 $options = get_option("WantedAds_widget");
  if (!is_array( $options ))
	{
	$options = array(
      'title' => ''.__('Wanted Listings','auctionPlugin').'',
	  'cp_auctions_number' => '5'
      );
  	}    

  if (isset($_POST['submit']))
  {
    $options['title'] = htmlspecialchars($_POST['WidgetTitle']);
	$options['cp_auctions_number'] = htmlspecialchars($_POST['cp_auctions_number']);

    update_option("WantedAds_widget", $options);
  }

?>

  <table width="100%">
  <tr>
  <td>
    <label for="WidgetTitle"><?php _e('Widget Title:', 'auctionPlugin'); ?> </label></td>
   <td> <input type="text" id="WidgetTitle" name="WidgetTitle" value="<?php echo $options['title'];?>" /></td>
   </tr>
   <tr>
    <td><label for="cp_auctions_number"><?php _e('Display Number:', 'auctionPlugin'); ?> </label></td>
    <td><input type="text" id="cp_auctions_number" name="cp_auctions_number" value="<?php echo $options['cp_auctions_number'];?>" /></td>
    </tr>
    </table>

    <input type="hidden" id="submit" name="submit" value="1" />
  </p>

  <?php

}
}
add_action( 'widgets_init', create_function( '', 'register_widget( "cp_auctions_wanted_ads" );' ) );