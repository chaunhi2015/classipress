<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| HOOKS PLACED IN TAB-FUNCTIONS.PHP - CUSTOM FRONTPAGE
|--------------------------------------------------------------------------
*/

// starting with #one above slider, the very top of page content
function cp_auction_frontpage_one() {
	do_action( 'cp_auction_frontpage_one' );
}

// on top of left block
function cp_auction_frontpage_two() {
	do_action( 'cp_auction_frontpage_two' );
}

// above category block
function cp_auction_frontpage_three() {
	do_action( 'cp_auction_frontpage_three' );
}

// the next hokks goes in between the different custom blocks set in frontpage settings.
function cp_auction_frontpage_four() {
	do_action( 'cp_auction_frontpage_four' );
}

function cp_auction_frontpage_five() {
	do_action( 'cp_auction_frontpage_five' );
}

function cp_auction_frontpage_six() {
	do_action( 'cp_auction_frontpage_six' );
}

function cp_auction_frontpage_seven() {
	do_action( 'cp_auction_frontpage_seven' );
}
// just above the tabbed menu and above Flatron custom content
function cp_auction_frontpage_eight() {
	do_action( 'cp_auction_frontpage_eight' );
}

// just above the tabbed menu and below Flatron custom content
function cp_auction_frontpage_nine() {
	do_action( 'cp_auction_frontpage_nine' );
}

// bottom of page, below ad listings
function cp_auction_frontpage_ten() {
	do_action( 'cp_auction_frontpage_ten' );
}