<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| DELIVERY NOTE
|--------------------------------------------------------------------------
*/

function cp_auction_geoip_country() {
	global $post;

	$geoip = get_option('cp_auction_enable_geoip');
	$disabled = get_option('cp_auction_disable_the_enable_shipping_option');
	$option = get_option('cp_auction_simple_settings');
	$opt = explode(',', $option);

	if ( ! is_singular(APP_POST_TYPE) )
		return;

	if ( $geoip != "yes" )
		return;

	$country = do_shortcode( '[geoip_detect2 property="country"]' );

	if ( empty( $country ) )
		return;

	$enable = get_user_meta( $post->post_author, 'cp_auction_enable_shipping', true );
	$shipping = cp_auction_calculate_shipping_country( $post->post_author, $country );
	if ( ( ( $opt && in_array("shipping_module", $opt ) && $disabled == "yes" ) || ( $enable == "yes" ) ) && ( $shipping == false ) ) {
	echo '<div class="delivery-note">'.sprintf( __( 'This seller does not deliver to %s.', 'auctionPlugin' ), $country ).'</div>';
	}

}
add_action( 'appthemes_after_post_title', 'cp_auction_geoip_country' );

function cp_auction_free_delivery() {
	global $post, $cp_options;

	if ( ! is_singular(APP_POST_TYPE) )
		return;

	$cs = get_user_meta($post->post_author, 'cp_currency', true );
	if( empty( $cs ) ) $cs = $cp_options->curr_symbol;

	$free_shipping = get_user_meta( $post->post_author, 'cp_auction_free_shipping', true );

	if ( empty( $free_shipping ) )
		return;

	echo '<div class="is_ok"><span>'.sprintf( __( 'THIS SELLER OFFER FREE SHIPPING ON ALL ORDERS ABOVE %s', 'auctionPlugin' ), cp_display_price( $free_shipping, $cs, false ) ).'</span></div>';

}
add_action( 'appthemes_after_post_title', 'cp_auction_free_delivery' );

// Based on country reqognized from IP adress
function cp_auction_geoip_cart( $post_id = '', $author = '', $country = '' ) {
	global $post;

	$geoip = get_option('cp_auction_enable_geoip');
	$disabled = get_option('cp_auction_disable_the_enable_shipping_option');
	$option = get_option('cp_auction_simple_settings');
	$opt = explode(',', $option);

	if ( $geoip != "yes" )
		return;

	if ( empty( $country ) )
		return;

	if ( $post_id > '0' ) {
	$post = get_post( $post_id );
	$author = $post->post_author;
	}
	else {
		return false;
	}

	if ( empty( $author ) ) return false;

	$enable = get_user_meta( $author, 'cp_auction_enable_shipping', true );
	$shipping = cp_auction_calculate_shipping_country( $post->post_author, $country );
	if ( ( ( $opt && in_array("shipping_module", $opt ) && $disabled == "yes" ) || ( $enable == "yes" ) ) && ( $shipping == false ) ) {
	return '<div class="delivery-cart">'.sprintf( __( 'This seller does not deliver to %s.', 'auctionPlugin' ), $country ).'</div>';
	}

}

// Based on country selected by buyer
function cp_auction_buyers_country( $post_id = '', $author = '', $country = '' ) {
	global $post;

	if ( $post_id > '0' ) $post = get_post( $post_id );
	$disabled = get_option('cp_auction_disable_the_enable_shipping_option');
	$option = get_option('cp_auction_simple_settings');
	$opt = explode(',', $option);

	$enable = get_user_meta( $post->post_author, 'cp_auction_enable_shipping', true );
	$shipping = cp_auction_calculate_shipping_country( $post->post_author, $country );
	if ( ( ( $opt && in_array("shipping_module", $opt ) && $disabled == "yes" ) || ( $enable == "yes" ) ) && ( $shipping == false ) ) {
		return "no";
	}

		return false;

}


/*
|--------------------------------------------------------------------------
| CALCULATE SHIPPING COST
|--------------------------------------------------------------------------
*/

function cp_auction_calculate_shipping( $seller, $buyer ) {
	global $wpdb;

	$add_shipping = get_user_meta( $seller, 'cp_auction_add_shipping', true );
	$max_shipping = get_user_meta( $seller, 'cp_auction_max_shipping', true );

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$items = $wpdb->get_results("SELECT * FROM {$table_purchases} WHERE uid = '$seller' AND buyer = '$buyer' AND unpaid > '0' AND paid != '2' AND status != 'paid'");

	if( $items && $add_shipping > 0 ) {
	$y = 0;
	$x = 0;
	foreach( $items as $item ) {

	$unpaid = ($item->unpaid - '1');

	if ( $item->special_delivery > 0 ) $x++;
	if ( $item->shipping > 0 ) $y++;

	$cp_special_delivery += $item->special_delivery + ( $item->special_delivery * $add_shipping / 100 * $unpaid );

	$addition += ( $item->shipping * $add_shipping / 100 * $unpaid );
	$cp_shipping += $item->shipping;

	}
	}
	else if( $items && ! $add_shipping ) {
	$y = 0;
	$x = 0;
	foreach( $items as $item ) {

	if ( $item->special_delivery > 0 ) $x++;
	if ( $item->shipping > 0 ) $y++;

	$cp_special_delivery += ( $item->special_delivery );
	$cp_shipping += $item->shipping;

	}
	}

	if ( $addition > 0 ) {
		if ( $x > 0 && $y == 0 ) $shipping = ( $cp_special_delivery + $addition );
		if ( $y > 0 && $x == 0 ) $shipping = ( ( $cp_shipping / $y ) + ( $addition ) );
		if ( $x > 0 && $y > 0 ) $shipping = ( ( $cp_shipping / $y ) + ( $cp_special_delivery + $addition ) );
	}
	else {
		if ( $x > 0 && $y == 0 ) $shipping = $cp_special_delivery;
		if ( $y > 0 && $x == 0 ) $shipping = ( $cp_shipping / $y );
		if ( $x > 0 && $y > 0 ) $shipping = ( ( $cp_shipping / $y ) + ( $cp_special_delivery ) );
	}

	if ( ( $max_shipping > 0 ) && ( $shipping > $max_shipping ) ) $shipping = $max_shipping;

	return $shipping;

}


function cp_auction_add_shipping( $post_id, $seller, $buyer, $country, $return = '' ) {
	global $wpdb;

	$enable = get_user_meta( $seller, 'cp_auction_enable_shipping', true );
	$cp_shipping_options = get_post_meta( $post_id, 'cp_shipping_options', true );
	$shipping_options = get_localized_field_values( 'cp_shipping_options', $cp_shipping_options );
	$disabled = get_option('cp_auction_disable_the_enable_shipping_option');
	$option = get_option('cp_auction_simple_settings');
	$opt = explode(',', $option);

	$shipping = get_post_meta( $post_id, 'cp_shipping', true );
	$cp_shipping_options = get_post_meta( $post_id, 'cp_shipping_options', true );

	if ( empty( $shipping ) && empty( $cp_shipping_options ) )
		return false;

	if ( ( empty( $shipping ) ) && ( $opt && in_array("shipping_module", $opt ) && $disabled == "yes" || $enable == "yes" ) ) {

		if ( $shipping_options == "Payable on purchase" ) $shipping = cp_auction_calculate_shipping_country( $seller, $country );
		if ( $shipping_options == "Free delivery" || $shipping_options == "Payable on pickup" ) $shipping = '0.00';
		if ( $shipping_options == "Special delivery" ) $special_delivery = get_post_meta( $post_id, 'cp_special_delivery', true );

	} else if ( empty( $shipping ) && $enable != "yes" ) {

		if ( $shipping_options == "Payable on purchase" ) $shipping = get_user_meta( $seller, 'cp_auction_shipping', true );
		if ( $shipping_options == "Free delivery" || $shipping_options == "Payable on pickup" ) $shipping = '0.00';
		if ( $shipping_options == "Special delivery" ) $special_delivery = get_post_meta( $post_id, 'cp_special_delivery', true );

	}

	if ( ( ! $return ) && ( ! empty( $shipping ) || $special_delivery > '0' ) ) {

		if ( empty( $shipping ) ) $shipping = '0.00';
		if ( empty( $special_delivery ) ) $special_delivery = '0.00';
		$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
		$wpdb->query($wpdb->prepare("UPDATE {$table_purchases} SET special_delivery = '$special_delivery', shipping = '$shipping' WHERE pid = '%d' AND buyer = '%d' AND unpaid > '0' AND paid != '2'", $post_id, $buyer));

	}

	if ( isset( $special_delivery ) ) $shipping = $special_delivery;

	if ( $return )
		return $shipping; // Shipping per single ads

}


/*
|--------------------------------------------------------------------------
| CALCULATE SHIPPING COST BY COUNTRY
|--------------------------------------------------------------------------
*/

function cp_auction_calculate_shipping_country( $author, $country ) {

	if ( empty( $author ) || empty( $country ) )
		return false;

	$option = get_user_meta( $author, 'shipto', true );
	if ( isset( $option[$country] ) ) $list = explode(',', $option[$country]);
	if ( ( isset( $option['ww'] ) ) && ( empty( $list[0] ) || $list[0] == '0' ) ) $list = explode(',', $option['ww']);

	if ( empty( $list[0] ) || $list[0] == '0' )
		return false;

	return $list[0];
}


/*
|--------------------------------------------------------------------------
| GET SHIPPING COUNTRIES DROPDOWN
|--------------------------------------------------------------------------
*/

function cp_auction_shipping_countries( $field_name, $uid = '' ) {

	global $post, $cp_options, $wpdb;

	$table_name = $wpdb->prefix . "cp_ad_fields";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE field_name = '$field_name'");

	foreach ( $results as $result ) {
	if ( $uid == false )
	$meta_val = ( $post ) ? get_post_meta($post->ID, $result->field_name, true) : false;
	else $meta_val = get_user_meta($uid, $field_name, true);

	if ( $result->field_type == "drop-down" ) {
	$options = explode( ',', $result->field_values );
	$options = array_map( 'trim', $options );
	$html_options = '';

	$html_options .= html( 'option', array( 'value' => '' ), __( 'Select Location', 'auctionPlugin' ) );
	foreach ( $options as $option ) {
	$args = array( 'value' => $option );
		//if ( $option == $meta_val )
		//$args['selected'] = 'selected';
		$html_options .= html( 'option', $args, $option );
		}
	$html_options .= html( 'option', array( 'value' => 'ww' ), __( 'Everywhere else', 'auctionPlugin' ) );

	$field_class = ( $result->field_req ) ? 'dropdownlist required' : 'dropdownlist';
	if( !$cp_options->selectbox ) {
	$field_class = "cp-dropdown";
	}
	if ( $result->field_name == "cp_country" )
	$args = array( 'name' => 'cp_shipping_country', 'id' => 'cp_shipping_country', 'class' => $field_class );
	else $args = array( 'name' => $result->field_name, 'id' => $result->field_name, 'class' => $field_class );

	echo html( 'select', $args, $html_options );

	} else {
	$field_class = ( $result->field_req ) ? 'text required' : 'text';
	$field_minlength = ( empty( $result->field_min_length ) ) ? '2' : $result->field_min_length;
	$args = array( 'value' => $value, 'name' => $result->field_name, 'id' => $result->field_name, 'type' => 'text', 'class' => $field_class, 'minlength' => $field_minlength );

	echo html( 'input', $args );
	echo html( 'div', array( 'class' => 'clr' ) );
	}
	}
}