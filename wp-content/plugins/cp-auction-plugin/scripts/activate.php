<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| INSTALL DATABASE
|--------------------------------------------------------------------------
*/

global $cp_auction_db_version, $cp_auction_db_date, $cp_options, $wpdb;
$cp_auction_db_version = "9.3";
$cp_auction_db_date = "27 July 2015";

function cp_auction_plugin_activate( $network_wide ) {
	global $wpdb;

	if ( function_exists( 'is_multisite' ) && is_multisite() && $network_wide ) {
		$current_blog = $wpdb->blogid;
		$blogs = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
		foreach ($blogs as $blog) {
			switch_to_blog($blog);
			cp_auction_plugin_install();
		}
		switch_to_blog($current_blog);
	} else {
		cp_auction_plugin_install();
	}
}

function cp_auction_plugin_install() {

   global $cp_auction_db_version, $cp_options, $wpdb;

   require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

if( get_option( "cp_auction_db_version" ) < '7.8' ) {

   $table_name = $wpdb->prefix . "cp_auction_plugin_bids";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

   $sql = "CREATE TABLE $table_name (
  id INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
  pid INT( 11 ) UNSIGNED NOT NULL,
  post_author INT( 11 ) UNSIGNED NOT NULL,
  datemade BIGINT( 20 ) UNSIGNED NOT NULL,
  bid DECIMAL( 12,2 ) NOT NULL,
  last_bid DECIMAL( 12,2 ) NOT NULL,
  uid INT( 11 ) UNSIGNED NOT NULL,
  message TEXT NOT NULL,
  winner INT( 4 ) UNSIGNED NOT NULL,
  anonymous VARCHAR( 4 ) NOT NULL,
  type VARCHAR( 20 ) NOT NULL,
  status VARCHAR( 20 ) NOT NULL,
  PRIMARY KEY  (id)
    );";

   dbDelta($sql);
}

   $table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
   if($wpdb->get_var("show tables like '$table_fields'") != $table_fields) {

   $sql = "CREATE TABLE $table_fields (
  `field_id` int(10) NOT NULL auto_increment,
  `field_name` varchar(255) NOT NULL,
  `field_label` varchar(255) NOT NULL,
  `field_desc` longtext,
  `field_type` varchar(255) NOT NULL,
  `field_values` longtext,
  `trans_values` longtext,
  `field_tooltip` longtext,
  `field_search` varchar(255) default '0',
  `field_perm` int(11) NOT NULL default '0',
  `field_core` int(11) NOT NULL default '0',
  `field_req` int(11) NOT NULL default '0',
  `field_owner` varchar(255) NOT NULL default 'CP Auction',
  `field_created` datetime NOT NULL default '0000-00-00 00:00:00',
  `field_modified` datetime NOT NULL default '0000-00-00 00:00:00',
  `field_min_length` int(11) NOT NULL default '0',
  `field_validation` longtext,
  PRIMARY KEY  (`field_id`)
    );";

   dbDelta($sql);

   $fdate = date('Y-m-d h:i:s');
   $sqls = "INSERT INTO {$table_fields} (`field_name`, `field_label`, `field_desc`, `field_type`, `field_values`, `field_tooltip`, `field_search`, `field_perm`, `field_core`, `field_req`, `field_owner`, `field_created`, `field_modified`, `field_min_length`, `field_validation`) VALUES ('cp_auction_howmany', 'Quantity', 'This is the Quantity field for the Classified & Wanted ad type. It is a core CP Auction field and should not be deleted.', 'text box', '', 'Enter a 0 to disable or if the number of available items are unlimited, otherwise your ad will be marked as sold after the first sale.', '', 0, 1, 1, 'CP Auction', '$fdate', '$fdate', 1, NULL),
('cp_start_price', 'Start Price', 'This is the Start Price field for the auction ad type. It is a core CP Auction field and should not be deleted.', 'text box', '', NULL, '', 0, 1, 1, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_min_price', 'Reserve Price', 'This is the Reserve Price field for the auction ad type. It is a core CP Auction field and should not be deleted.', 'text box', '', NULL, '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_bid_increments', 'Bid Increments', 'This is the Bid Increment field for the auction ad type. It is a core CP Auction field and should not be deleted.', 'text box', '', NULL, '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_buy_now', 'Buy Now Price', 'This is the Buy Now price field for the auction ad type. It is a core CP Auction field and should not be deleted.', 'text box', '', NULL, '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_shipping_options', 'Shipping Options', 'This is the Shipping Options field to be used with all ad types. It is a core CP Auction field and should not be deleted.', 'radio', 'Free delivery,Payable on pickup,Payable on purchase,Special delivery', 'Choose your preferred shipping option. If you choose the Payable on purchase option then you need to make sure that you have specified a shipping rate in the settings area of your dashboard. Use the Special delivery option if this item will most likely have a whole different shipping cost than the shipping rate you have specified.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 1, NULL),
('cp_special_delivery', 'Special Delivery', 'This is the Special Delivery field to be used with all ad types. It is a core CP Auction field and should not be deleted.', 'text box', '', 'You should choose to use Special delivery if this item will most likely have a whole different shipping cost than the shipping rate you have specified in the settings area of your dashboard. If you have not set the Additional Cost (%) under settings in your dashboard so the shipping costs you specify here will apply per 1pcs purchased, ie. if 2pcs purchased then doubled cost. Use only numeric values, ie. 100 or 100.00', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 1, NULL),
('cp_auction_listing_duration', 'Ad Listing Period', 'This is the Ad Listing Period field for the auction ad type. It is a core CP Auction field and should not be deleted.', 'drop-down', '1,3,5,7,10', NULL, '', 0, 1, 1, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_want_to', 'I Want To', 'This is the I Want To field to be used with Wanted Ad type. It is a core CP Auction field and should not be deleted.', 'drop-down', 'Buy,Swap,Give Away', 'Please select the option that best suits your intentions with this ad.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_max_price', 'Max. Price', 'This is the Max. Price field to be used with Wanted Ads. It is a core CP Auction field and should not be deleted.', 'text box', '', 'Enter a maximum price if the advertisement relates to a product you want to buy.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_swap_with', 'Swap With', 'This is the Swap With field to be used with Wanted Ads. It is a core CP Auction field and should not be deleted.', 'text box', NULL, 'If you want to swap with a specific product then you can write the name of the desired product in this field.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_allow_deals', 'Allow Deals', 'This is the Allow Deals field to be used with Wanted Ads. It is a core CP Auction field and should not be deleted.', 'drop-down', 'No,Yes', 'If you want the holder of the product should be able to provide you with offers in the form of bid and bid rounds then select yes here.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_demopage_url', 'Demo Page URL', 'This is the demo page url field to be used with all ad types. It is a core CP Auction field and should not be deleted.', 'text box', NULL, 'If you have a separate demo page for this product then you can enter the full URL here, eg. http://www.yoursite.com/.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_iwant_to', 'I Want To', 'This is the I Want To field to be used with Classified ad type. It is a core CP Auction field and should not be deleted.', 'drop-down', 'Sell,Buy,Swap,Give Away', 'Please select the option that best suits your intentions with this ad.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_pausead', 'Set Ad Status', 'This is the ad status field for all ad types. Author can through out the advertising process choose to pause their ad or make it private. It is a core CP Auction field and should not be deleted unless the Author set Ad Status settings are disabled.', 'drop-down', 'Public,Private,Paused', '', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_contact_phone', 'Telephone Number', 'This is the contact phone field to be used with all ad types and the custom sidebar-ad.php. It is a core CP Auction field and should not be deleted.', 'text box', '', 'Please enter a telephone number where potential buyers can get in contact with you.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_contact_email', 'Email Address', 'This is the contact email adress field to be used with all ad types and the custom sidebar-ad.php. It is a core CP Auction field and should not be deleted.', 'text box', '', 'Please enter an email adress where potential buyers can get in contact with you.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_contact_notes', 'Notes', 'This is the notes field to be used with all ad types and the custom sidebar-ad.php. The seller can list relevent items like best time to call, text or special instructions. It is a core CP Auction field and should not be deleted.', 'text area', '', 'In this text area you can list relevent items like best time to call, text or special instructions.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL),
('cp_price_negotiable', 'Price Negotiable?', 'This is the Is Price Negotiable field to be used with all ad types.', 'checkbox', 'Tick to enable', 'Please tick the checkbox if your price is negotiable. Leave empty if not.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 0, NULL)";

   $wpdb->query($sqls);
}

   $table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

   $sql = "CREATE TABLE $table_name (
  id INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
  order_id INT( 11 ) UNSIGNED NOT NULL,
  pid INT( 11 ) UNSIGNED NOT NULL,
  post_ids VARCHAR( 255 ) NOT NULL,
  datemade BIGINT( 20 ) UNSIGNED NOT NULL,
  uid INT( 11 ) UNSIGNED NOT NULL,
  post_author INT( 11 ) UNSIGNED NOT NULL,
  ip_address VARCHAR( 20 ) NOT NULL,
  payment_date BIGINT( 20 ) UNSIGNED NOT NULL,
  txn_id VARCHAR( 75 ) NOT NULL,
  item_name VARCHAR( 75 ) NOT NULL,
  trans_type VARCHAR( 75 ) NOT NULL,
  mc_currency VARCHAR( 10 ) NOT NULL,
  last_name VARCHAR( 75 ) NOT NULL,
  first_name VARCHAR( 75 ) NOT NULL,
  payer_email VARCHAR( 75 ) NOT NULL,
  receiver_email VARCHAR( 75 ) NOT NULL,
  address_country VARCHAR( 75 ) NOT NULL,
  address_state VARCHAR( 75 ) NOT NULL,
  address_country_code VARCHAR( 75 ) NOT NULL,
  address_zip VARCHAR( 10 ) NOT NULL,
  address_street VARCHAR( 75 ) NOT NULL,
  moved_credits DECIMAL( 12,2 ) NOT NULL,
  available DECIMAL( 12,2 ) NOT NULL,
  quantity INT( 5 ) NOT NULL,
  payment_option VARCHAR( 75 ) NOT NULL,
  payout_details VARCHAR( 255 ) NOT NULL,
  processor_fee DECIMAL( 12,2 ) NOT NULL,
  shipping DECIMAL( 12,2 ) NOT NULL,
  mc_fee DECIMAL( 12,2 ) NOT NULL,
  mc_gross DECIMAL( 12,2 ) NOT NULL,
  tax DECIMAL( 12,2 ) NOT NULL,
  aws VARCHAR( 20 ) NOT NULL,
  type VARCHAR( 75 ) NOT NULL,
  status VARCHAR( 75 ) NOT NULL,
  escrow_status VARCHAR( 20 ) NOT NULL,
  PRIMARY KEY  (id)
    );";

   dbDelta($sql);
}

   $table_name = $wpdb->prefix . "cp_auction_plugin_tabs";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

   $sql = "CREATE TABLE $table_name (
  id INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
  position VARCHAR( 10 ) NOT NULL,
  block VARCHAR( 10 ) NOT NULL,
  title VARCHAR( 20 ) NOT NULL,
  sequence INT( 4 ) UNSIGNED NOT NULL,
  PRIMARY KEY  (id)
    );";

   dbDelta($sql);
}

   $table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

   $sql = "CREATE TABLE $table_name (
  id INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
  order_id INT( 11 ) UNSIGNED NOT NULL,
  trans_id INT( 11 ) UNSIGNED NOT NULL,
  pid INT( 11 ) UNSIGNED NOT NULL,
  uid INT( 11 ) UNSIGNED NOT NULL,
  buyer INT( 11 ) UNSIGNED NOT NULL,
  item_name VARCHAR( 75 ) NOT NULL,
  quantity INT( 5 ) NOT NULL,
  amount DECIMAL( 12,2 ) NOT NULL,
  discount DECIMAL( 12,2 ) NOT NULL,
  special_delivery DECIMAL( 12,2 ) NOT NULL,
  shipping DECIMAL( 12,2 ) NOT NULL,
  mc_fee DECIMAL( 12,2 ) NOT NULL,
  mc_gross DECIMAL( 12,2 ) NOT NULL,
  tax DECIMAL( 12,2 ) NOT NULL,
  datemade BIGINT( 20 ) UNSIGNED NOT NULL,
  numbers INT( 4 ) NOT NULL,
  unpaid INT( 4 ) NOT NULL,
  net_total DECIMAL( 12,2 ) NOT NULL,
  unpaid_total DECIMAL( 12,2 ) NOT NULL,
  paid INT( 4 ) UNSIGNED NOT NULL,
  paiddate BIGINT( 20 ) UNSIGNED NOT NULL,
  txn_id VARCHAR( 75 ) NOT NULL,
  type VARCHAR( 20 ) NOT NULL,
  ref VARCHAR( 20 ) NOT NULL,
  expiration BIGINT( 20 ) UNSIGNED NOT NULL,
  status VARCHAR( 20 ) NOT NULL,
  PRIMARY KEY  (id)
    );";

   dbDelta($sql);
}

   $table_name = $wpdb->prefix . "cp_auction_plugin_favorites";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

   $sql = "CREATE TABLE $table_name (
  id INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
  uid INT( 11 ) UNSIGNED NOT NULL,
  sid INT( 11 ) UNSIGNED NOT NULL,
  PRIMARY KEY  (id)
    );";

   dbDelta($sql);
}

   $table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

   $sql = "CREATE TABLE $table_name (
  id INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
  uid INT( 11 ) UNSIGNED NOT NULL,
  aid INT( 11 ) UNSIGNED NOT NULL,
  type VARCHAR( 20 ) NOT NULL,
  PRIMARY KEY  (id)
    );";

   dbDelta($sql);
}

   $table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

   $sql = "CREATE TABLE $table_name (
  id INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT,
  pid INT( 11 ) UNSIGNED NOT NULL,
  post_author INT( 11 ) UNSIGNED NOT NULL,
  buyer INT( 11 ) UNSIGNED NOT NULL,
  fname VARCHAR( 255 ) NOT NULL,
  filename VARCHAR( 255 ) NOT NULL,
  fullpath VARCHAR( 255 ) NOT NULL,
  download VARCHAR( 255 ) NOT NULL,
  size VARCHAR( 10 ) NOT NULL,
  ip VARCHAR( 20 ) NOT NULL,
  datemade BIGINT( 20 ) UNSIGNED NOT NULL,
  type VARCHAR( 20 ) NOT NULL,
  PRIMARY KEY  (id)
    );";

   dbDelta($sql);
}

   $table_name = $wpdb->prefix . "cp_auction_plugin_coupons";
   if($wpdb->get_var("show tables like '$table_name'") != $table_name) {

    $sql = "CREATE TABLE $table_name (
  coupon_id int(10) NOT NULL AUTO_INCREMENT,
  coupon_uid int(10) NOT NULL,
  coupon_pid int(10) NOT NULL,
  coupon_code varchar(100) NOT NULL,
  coupon_desc longtext,
  coupon_discount decimal(12,2) unsigned NOT NULL default '0.00',
  coupon_discount_type varchar(50) NOT NULL,
  coupon_start_date datetime NOT NULL default '0000-00-00 00:00:00',
  coupon_expire_date datetime NOT NULL default '0000-00-00 00:00:00',
  coupon_status varchar(50) NOT NULL,
  coupon_use_count int(11) NOT NULL default '0',
  coupon_max_use_count int(11) NOT NULL default '0',
  coupon_owner varchar(255) NOT NULL,
  coupon_created datetime NOT NULL default '0000-00-00 00:00:00',
  coupon_modified datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (coupon_id),
  UNIQUE KEY coupon_code (coupon_code),
  UNIQUE KEY coupon_code_2 (coupon_code),
  UNIQUE KEY coupon_code_3 (coupon_code),
  UNIQUE KEY coupon_code_4 (coupon_code)
    );";

   dbDelta($sql);
}


   $table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
   $results = $wpdb->get_results("SELECT * FROM {$table_fields} ORDER BY field_id ASC");
   if( $results ) {
   foreach($results as $res) {
   $table_ad_fields = $wpdb->prefix . "cp_ad_fields";
   $row = $wpdb->get_row("SELECT * FROM {$table_ad_fields} WHERE field_name = '$res->field_name'");
   if( !$row ) {
   $query = "INSERT INTO {$table_ad_fields} (field_name, field_label, field_desc, field_type, field_values, field_tooltip, field_search, field_perm, field_core, field_req, field_owner, field_created, field_modified, field_min_length, field_validation) VALUES (%s, %s, %s, %s, %s, %s, %d, %d, %d, %d, %s, %s, %s, %d, %d)"; 
    	$wpdb->query($wpdb->prepare($query, $res->field_name, $res->field_label, $res->field_desc, $res->field_type, $res->field_values, $res->field_tooltip, $res->field_search, $res->field_perm, $res->field_core, $res->field_req, $res->field_owner, $res->field_created, $res->field_modified, $res->field_min_length, $res->field_validation));
   }
   }
}

   $option = array();
   $cp_ad_fields = cp_get_ad_fields();
   if( $cp_ad_fields ) {
   foreach($cp_ad_fields as $field) {
        if ( $field->field_name == "cp_price" )
        $option[$field->field_name] = "classified";
        elseif ( $field->field_name == "cp_auction_howmany" )
        $option[$field->field_name] = "classified,wanted";
        elseif ( $field->field_name == "cp_region" || $field->field_name == "cp_size" || $field->field_name == "cp_feedback" || $field->field_name == "cp_bestoffer" || $field->field_name == "cp_currency" )
        $option[$field->field_name] = "";
        elseif ( $field->field_name == "cp_buy_now" || $field->field_name == "cp_auction_listing_duration" || $field->field_name == "cp_start_price" || $field->field_name == "cp_min_price" || $field->field_name == "cp_bid_increments" )
        $option[$field->field_name] = "normal";
        elseif ( $field->field_name == "cp_auction_listing_duration" || $field->field_name == "cp_start_price" || $field->field_name == "cp_bid_increments" )
        $option[$field->field_name] = "reverse";
        elseif ( $field->field_name == "cp_auction" )
        $option[$field->field_name] = "classified";
        elseif ( $field->field_name == "cp_shipping_options" || $field->field_name == "cp_special_delivery" || $field->field_name == "cp_price_negotiable" )
        $option[$field->field_name] = "classified,wanted,normal";
        elseif ( $field->field_name == "cp_demopage_url" )
        $option[$field->field_name] = "classified,wanted,normal,reverse";
        elseif ( $field->field_name == "cp_max_price" || $field->field_name == "cp_want_to" || $field->field_name == "cp_swap_with" || $field->field_name == "cp_allow_deals" )
        $option[$field->field_name] = "wanted";
        elseif ( $field->field_name == "cp_iwant_to" )
        $option[$field->field_name] = "classified";
	else
   	$option[$field->field_name] = "classified,wanted,normal,reverse";
   	}
   }
   if ( ! empty( $option ) )
   update_option('cp_auction_fields', $option);

   $options = array();
   $cp_ad_details = cp_get_ad_fields();
   if( $cp_ad_details ) {
   foreach($cp_ad_details as $detail) {
        if ( $detail->field_name == "cp_state" || $detail->field_name == "cp_auction_howmany" || $detail->field_name == "cp_buy_now" || $detail->field_name == "cp_country" || $detail->field_name == "cp_zipcode" || $detail->field_name == "cp_start_price" || $detail->field_name == "cp_min_price" || $detail->field_name == "cp_bid_increments" || $detail->field_name == "cp_street" || $detail->field_name == "cp_city" || $detail->field_name == "cp_max_price" )
        $options[$detail->field_name] = "yes";
	else
   	$options[$detail->field_name] = "no";
   	}
   }
   if ( ! empty( $options ) )
   update_option('cp_auction_details', $options);

   $prices = array();
   $cp_ad_pricefields = cp_get_ad_fields();
   if( $cp_ad_pricefields ) {
   foreach($cp_ad_pricefields as $price) {
        if ( $price->field_name == "cp_price" || $price->field_name == "cp_start_price" || $price->field_name == "cp_min_price" || $price->field_name == "cp_bid_increments" || $price->field_name == "cp_special_delivery" || $price->field_name == "cp_buy_now" || $price->field_name == "cp_max_price" )
        $prices[$price->field_name] = "yes";
	else
   	$prices[$price->field_name] = "no";
   	}
   }
   if ( ! empty( $prices ) )
   update_option('cp_auction_pricefields', $prices);

	$table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
	$where_array = array('field_perm' => 2);
	$wpdb->update($table_fields, array('field_perm' => 0), $where_array);

	$results = $wpdb->get_results("SELECT * FROM {$table_fields} ORDER BY field_id ASC");
	if( $results ) {
	foreach($results as $res) {
	$table_ad_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_ad_fields} WHERE field_name = '$res->field_name'");
	if( $row ) {
	$where_array = array('field_name' => $res->field_name);
	$wpdb->update($table_ad_fields, array('field_perm' => 0), $where_array);
	}
	}
	}
	update_option( "cp_auction_db_version", $cp_auction_db_version );
}

if( get_option( "cp_auction_db_version" ) < '8.9' ) {

   $table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
   $row = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_shipping_options'");
   if( !$row ) {
   $fdate = date('Y-m-d h:i:s');
   $sql = "INSERT INTO {$table_fields} (`field_name`, `field_label`, `field_desc`, `field_type`, `field_values`, `field_tooltip`, `field_search`, `field_perm`, `field_core`, `field_req`, `field_owner`, `field_created`, `field_modified`, `field_min_length`, `field_validation`) VALUES ('cp_shipping_options', 'Shipping Options', 'This is the Shipping Options field to be used with all ad types. It is a core CP Auction field and should not be deleted.', 'radio', 'Free delivery,Payable on pickup,Payable on purchase,Special delivery', 'Choose your preferred shipping option. If you choose the Payable on purchase option then you need to make sure that you have specified a shipping rate in the settings area of your dashboard. Use the Special delivery option if this item will most likely have a whole different shipping cost than the shipping rate you have specified.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 1, NULL),
('cp_special_delivery', 'Special Delivery', 'This is the Special Delivery field to be used with all ad types. It is a core CP Auction field and should not be deleted.', 'text box', '', 'You should choose to use Special delivery if this item will most likely have a whole different shipping cost than the shipping rate you have specified in the settings area of your dashboard. If you have not set the Additional Cost (%) under settings in your dashboard so the shipping costs you specify here will apply per 1pcs purchased, ie. if 2pcs purchased then doubled cost. Use only numeric values, ie. 100 or 100.00', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 1, NULL)";

   dbDelta($sql);

   }

   $table_ad_fields = $wpdb->prefix . "cp_ad_fields";
   $rows = $wpdb->get_row("SELECT * FROM {$table_ad_fields} WHERE field_name = 'cp_shipping_options'");
   if( !$rows ) {
   $fdate = date('Y-m-d h:i:s');
   $sqls = "INSERT INTO {$table_ad_fields} (`field_name`, `field_label`, `field_desc`, `field_type`, `field_values`, `field_tooltip`, `field_search`, `field_perm`, `field_core`, `field_req`, `field_owner`, `field_created`, `field_modified`, `field_min_length`, `field_validation`) VALUES ('cp_shipping_options', 'Shipping Options', 'This is the Shipping Options field to be used with all ad types. It is a core CP Auction field and should not be deleted.', 'radio', 'Free delivery,Payable on pickup,Payable on purchase,Special delivery', 'Choose your preferred shipping option. If you choose the Payable on purchase option then you need to make sure that you have specified a shipping rate in the settings area of your dashboard. Use the Special delivery option if this item will most likely have a whole different shipping cost than the shipping rate you have specified.', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 1, NULL),
('cp_special_delivery', 'Special Delivery', 'This is the Special Delivery field to be used with all ad types. It is a core CP Auction field and should not be deleted.', 'text box', '', 'You should choose to use Special delivery if this item will most likely have a whole different shipping cost than the shipping rate you have specified in the settings area of your dashboard. If you have not set the Additional Cost (%) under settings in your dashboard so the shipping costs you specify here will apply per 1pcs purchased, ie. if 2pcs purchased then doubled cost. Use only numeric values, ie. 100 or 100.00', '', 0, 1, 0, 'CP Auction', '$fdate', '$fdate', 1, NULL)";

   dbDelta($sqls);

   }

   $option = get_option( 'cp_auction_fields' );
   $option['cp_shipping_options'] = "classified,wanted,normal";
   $option['cp_special_delivery'] = "classified,wanted,normal";
   if ( ! empty( $option ) )
   update_option('cp_auction_fields', $option);

   $prices = get_option( 'cp_auction_pricefields' );
   $prices['cp_special_delivery'] = "yes";
   if ( ! empty( $prices ) )
   update_option('cp_auction_pricefields', $prices);

   $table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
   $sql0 = "ALTER TABLE {$table_purchases} ADD COLUMN special_delivery DECIMAL( 12,2 ) NOT NULL AFTER discount";
   @$wpdb->query($sql0);

   $sql1 = "ALTER TABLE {$table_purchases} ADD COLUMN shipping DECIMAL( 12,2 ) NOT NULL AFTER special_delivery";
   @$wpdb->query($sql1);

   $table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
   $wpdb->query($wpdb->prepare("DELETE FROM {$table_fields} WHERE field_name = '%s'", 'cp_shipping'));

   $table_ad_fields = $wpdb->prefix . "cp_ad_fields";
   $wpdb->query($wpdb->prepare("DELETE FROM {$table_ad_fields} WHERE field_name = '%s'", 'cp_shipping'));

}

if( get_option( "cp_auction_db_version" ) < '9.0' ) {

   $table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
   $sql0 = "ALTER TABLE {$table_purchases} ADD COLUMN special_delivery DECIMAL( 12,2 ) NOT NULL AFTER discount";
   @$wpdb->query($sql0);

}

if( get_option( "cp_auction_db_version" ) < '9.1' ) {

   $table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
   $sql = "ALTER TABLE {$table_purchases} ADD COLUMN ref VARCHAR( 20 ) NOT NULL AFTER type";
   @$wpdb->query($sql);

}

if( get_option( "cp_auction_db_version" ) < '9.2' ) {

   $table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
   $sql = "ALTER TABLE {$table_purchases} ADD COLUMN expiration BIGINT( 20 ) UNSIGNED NOT NULL AFTER ref";
   @$wpdb->query($sql);

}

if( get_option( "cp_auction_db_version" ) < '9.3' ) {

   $table_tabs = $wpdb->prefix . "cp_auction_plugin_tabs";
   $sql = "ALTER TABLE {$table_tabs} ADD COLUMN position VARCHAR( 10 ) NOT NULL AFTER id";
   @$wpdb->query($sql);

}

   update_option( "cp_auction_db_version", $cp_auction_db_version );

}


/*
|--------------------------------------------------------------------------
| FUNCTIONS TO USE DURING THE INSTALLATION PROCESS
|--------------------------------------------------------------------------
*/

function cp_get_ad_fields() {
	global $wpdb;
	$sql = "SELECT field_name, field_label, field_type, field_desc, field_perm, field_id "
		. "FROM " . $wpdb->prefix . "cp_ad_fields "
		. "ORDER BY field_name desc";

	return $wpdb->get_results( $sql );
}
?>