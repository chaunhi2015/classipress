<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| SUPPORTED CHILD THEMES
|--------------------------------------------------------------------------
*/

function supported_child_themes($stylesheet) {

	if ( $stylesheet == "skye-child-theme-1.0" || $stylesheet == "skye-child-theme-1.1" ) $stylesheet = "skye";

	$childs = array("phoenix", "citrus-night", "multiport", "jibo", "simply-responsive-cp", "simply-responsive-cp-133", "flannel", "ultraclassifieds", "classiclean", "classiestate", "classipress", "eldorado", "twinpress_classifieds", "classipress-headline-blue", "classipress-headline-green", "classipress-headline-orange", "classipress-headline-purple", "classipress-headline-red", "eclassify", "grid-mod", "rondell", "rondell_370", "flatpress", "flatron", "skye", "dealpress", "adsplash");

	if( in_array( $stylesheet, $childs ) )
	return $stylesheet;
	else return "custom";
}


/*
|-------------------------------------------------------------------------------
| THEME HEADER WRAPPER
|-------------------------------------------------------------------------------
*/

function cp_auction_header( $title = '', $breadcrumb = '', $post_id = '', $full = '' ) {

	global $cp_options;

?>

<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?>> <!--<![endif]-->

<head>

	<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />

	<title><?php wp_title(); ?></title>

	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php echo appthemes_get_feed_url(); ?>" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>

</head>
<?php flush(); ?>
<body <?php body_class(); ?>>

	<?php appthemes_before(); ?>

	<div class="container">

	<?php if ( $cp_options->debug_mode ) { ?><div class="debug"><h3><?php _e( 'Debug Mode On', 'auctionPlugin' ); ?></h3><?php print_r( $wp_query->query_vars ); ?></div><?php } ?>

<?php 
        $sarray = array( 'citrus-night', 'eldorado' );
	if( in_array( get_option('stylesheet'), $sarray ) ) { ?>
	<div class="sam_wrapper">
	<?php } ?>

<?php if( get_option('stylesheet') == "greenymarketplace" ) { ?>
	<div class="rb_wrapper">
	<?php } ?>

		<?php appthemes_before_header(); ?>
		<?php get_header( app_template_base() ); ?>
		<?php appthemes_after_header(); ?>

<div class="content user-dashboard">

    <div class="content_botbg">

        <div class="content_res">

<?php if( get_option('cp_auction_disable_breadcrumb') != "yes") {
	$inst = false;
	$inst_folder = get_option('cp_auction_inst_folder');
	if( empty( $inst_folder ) ) $inst = false;
	else $inst = ''.get_option('cp_auction_inst_folder').'/';
?>
        <div id="breadcrumb">
        <div id="crumbs"><a href="<?php get_bloginfo('wpurl'); ?>/<?php echo $inst; ?>"><?php _e('Home', 'auctionPlugin'); ?></a> » <?php if( empty( $title ) ) echo the_title(); else echo $title; ?></div>
	</div>

<?php } ?>

<?php if( get_option('stylesheet') == "pinterclass2") { ?>

	<div class="clear20"></div>

<?php } ?>

<?php if( !$full ) { ?>
            <div class="content_left">
<?php } ?>

	<?php if( get_option('stylesheet') == "twinpress_classifieds") { ?>
            <div class="conl">
	<?php } ?>

	<?php if( $post_id ) appthemes_stats_update( $post_id ); ?>

<?php
}


/*
|--------------------------------------------------------------------------
| THEME FOOTER
|--------------------------------------------------------------------------
*/

function cp_auction_footer( $page, $full = '' ) {
?>

			<?php appthemes_after_post(); ?>

			<?php if( get_option('stylesheet') == "twinpress_classifieds") { ?>
			</div><!-- /conl -->
			<?php } ?>

		<?php if( !$full ) { ?>
                </div><!-- /content_left -->
		<?php get_sidebar($page); ?>
		<?php } ?>

            <div class="clr"></div>

        </div><!-- /content_res -->

    </div><!-- /content_botbg -->

</div><!-- /content -->

		<?php appthemes_before_footer(); ?>
		<?php get_footer( app_template_base() ); ?>
		<?php appthemes_after_footer(); ?>

<?php 
	$sarray = array( 'citrus-night', 'eldorado', 'greenymarketplace' );
	if( in_array( get_option('stylesheet'), $sarray ) ) { ?>
	</div><!-- /wrapper -->
	<?php } ?>

<?php wp_footer(); ?>

	<?php appthemes_after(); ?>

</body>

</html>

<?php
}