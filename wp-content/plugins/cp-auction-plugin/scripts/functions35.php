<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| RETURNS AD IMAGES IF RONDELL CHILD THEME
|--------------------------------------------------------------------------
*/

if ( get_option( 'CP_VERSION' ) == "rondell" && ! function_exists( 'cp_ad_loop_thumbnail' ) ) :
	function cp_ad_loop_thumbnail() {
		global $post, $cp_options;

		// go see if any images are associated with the ad
		$image_id = cp_get_featured_image_id( $post->ID );

		// set the class based on if the hover preview option is set to "yes"
		$prevclass = ( $cp_options->ad_image_preview ) ? 'preview' : 'nopreview';

		if ( $image_id > 0 ) {

			// get 100x100 v3.0.5+ image size
			$adthumbarray = wp_get_attachment_image( $image_id, 'ad-thumb' );

			// grab the large image for onhover preview
			$adlargearray = wp_get_attachment_image_src( $image_id, 'large' );
			$img_large_url_raw = $adlargearray[0];

			// must be a v3.0.5+ created ad
			if ( $adthumbarray ) {
				echo '<a href="'. get_permalink() .'" title="'. the_title_attribute( 'echo=0' ) .'" class="'. $prevclass .'" data-rel="'. $img_large_url_raw .'">'. $adthumbarray .'</a>';

			// maybe a v3.0 legacy ad
			} else {
				$adthumblegarray = wp_get_attachment_image_src($image_id, 'thumbnail');
				$img_thumbleg_url_raw = $adthumblegarray[0];
				echo '<a href="'. get_permalink() .'" title="'. the_title_attribute( 'echo=0' ) .'" class="'. $prevclass .'" data-rel="'. $img_large_url_raw .'">'. $adthumblegarray .'</a>';
			}

		// no image so return the placeholder thumbnail
		} else {
			echo '<a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '"><img class="attachment-ad-medium" alt="" title="" src="' . appthemes_locate_template_uri( 'images/no-thumb-150x150.png' ) . '" /></a>';
		}

	}
endif;


/*
|--------------------------------------------------------------------------
| RETURNS DASHBOARD AD LISTING ACTIONS
|--------------------------------------------------------------------------
*/

function cp_auction_get_dashboard_listing_actions( $listing_id = 0, $ad_type ) {
	global $cp_options;

	if( empty( $ad_type ) ) $DASHBOARD_URL = CP_DASHBOARD_URL;
	else $DASHBOARD_URL = CP_AUCTION_MYADS;

	$actions = array();
	$listing_id = $listing_id ? $listing_id : get_the_ID();
	$listing = get_post( $listing_id );
	$listing_status = cp_get_listing_status_name( $listing_id );

	// edit button
	if ( $cp_options->ad_edit ) {
		$edit_attr = array(
			'title' => __( 'Edit Ad', 'auctionPlugin' ),
			'href' => add_query_arg( array( 'listing_edit' => $listing->ID, 'type' => $ad_type ), CP_EDIT_URL ),
		);

		if ( in_array( $listing_status, array( 'live', 'offline' ) ) ) {
			$actions['edit'] = $edit_attr;
		}
		if ( $cp_options->moderate_edited_ads && in_array( $listing_status, array( 'pending_moderation', 'pending_payment' ) ) ) {
			$actions['edit'] = $edit_attr;
		}
	}

	// delete button
	$actions['delete'] = array(
		'title' => __( 'Delete Ad', 'auctionPlugin' ),
		'href' => add_query_arg( array( 'aid' => $listing->ID, 'action' => 'delete' ), $DASHBOARD_URL ),
		'onclick' => 'return confirmBeforeDeleteAd();',
	);

	// pause button
	if ( $listing_status == 'live' ) {
		$actions['pause'] = array(
			'title' => __( 'Pause Ad', 'auctionPlugin' ),
			'href' => add_query_arg( array( 'aid' => $listing->ID, 'action' => 'pause' ), $DASHBOARD_URL ),
		);
	}

	// restart button
	if ( $listing_status == 'offline' ) {
		$actions['restart'] = array(
			'title' => __( 'Restart ad', 'auctionPlugin' ),
			'href' => add_query_arg( array( 'aid' => $listing->ID, 'action' => 'restart' ), $DASHBOARD_URL ),
		);
	}

	return apply_filters( 'cp_dashboard_listing_actions', $actions, $listing );
}



/*
|--------------------------------------------------------------------------
| DISPLAYS DASHBOARD AD LISTING ACTIONS
|--------------------------------------------------------------------------
*/

function cp_auction_dashboard_listing_actions( $listing_id = 0, $ad_type ) {
	$listing_id = $listing_id ? $listing_id : get_the_ID();

	$actions = cp_auction_get_dashboard_listing_actions( $listing_id, $ad_type );
	$li = '';

	$iconized = array( 'edit', 'delete', 'pause', 'restart' );

	foreach ( $actions as $action => $attr ) {

		$description = $attr['title'];

		if ( in_array( $action, $iconized ) ) {
			$attr['class'] = $action . ' dashicons-before' ;
			$description = '';
		}

		$a = html( 'a', $attr, $description );
		$li .= html( 'li', array( 'class' => $action ), $a );
	}

	$ul = html( 'ul', array( 'id' => 'listing-actions-' . $listing_id, 'class' => 'listing-actions' ), $li );
	echo $ul;
}


/*
|--------------------------------------------------------------------------
| CREATES NIGERIAN NAIRA CURRENCY
|--------------------------------------------------------------------------
*/

add_action( 'init', 'cp_auction_add_nigerian_naira' );
function cp_auction_add_nigerian_naira(){
 
    // Give your currency a name and symbol
    $args = array(
       'name' => 'Nigerian Naira',
       'symbol' => '₦'
    );
    // The first argument is the currency code. Usually a 3 letter code. ( United States Dollars  = USD )
    if ( class_exists("APP_Currencies") ) APP_Currencies::add_currency( 'NGN', $args );
 
}


/*
|--------------------------------------------------------------------------
| CREATES VANUATU VATU CURRENCY
|--------------------------------------------------------------------------
*/

add_action( 'init', 'cp_auction_add_vanuatu_vatu' );
function cp_auction_add_vanuatu_vatu(){
 
    // Give your currency a name and symbol
    $args = array(
       'name' => 'Vanuatu Vatu',
       'symbol' => 'VT'
    );
    // The first argument is the currency code. Usually a 3 letter code. ( United States Dollars  = USD )
    if ( class_exists("APP_Currencies") ) APP_Currencies::add_currency( 'VUV', $args );
 
}


/*
|--------------------------------------------------------------------------
| CREATES THE CATEGORY CHECKLIST BOX
|--------------------------------------------------------------------------
*/

function cp_auction_category_checklist( $checkedcats, $exclude = '' ) {

	$walker = new Walker_Category_Checklist;

	$args = array();

	if ( is_array( $checkedcats ) ) {
		$args['selected_cats'] = $checkedcats;
	} else {
		$args['selected_cats'] = array();
	}

	$args['popular_cats'] = array();
	$categories = get_categories( array(
		'hide_empty' => 0,
		'taxonomy' => APP_TAX_CAT,
		'exclude' => $exclude,
	) );

	return call_user_func_array( array( &$walker, 'walk' ), array( $categories, 0, $args ) );
}


/*
|--------------------------------------------------------------------------
| AD POSTER SIDEBAR CONTACT FORM EMAIL
|--------------------------------------------------------------------------
*/

function cp_auction_ad_owner_email( $post_id ) {
	$errors = new WP_Error();
	$pull_fields = get_option('cp_auction_pull_fields');

	// check for required post data
	$expected = array( 'from_name', 'from_email', 'subject', 'message' );
	foreach ( $expected as $field_name ) {
		if ( empty( $_POST[ $field_name ] ) ) {
			$errors->add( 'empty_field', __( 'ERROR: All fields are required.', 'auctionPlugin' ) );
			return $errors;
		}
	}

	// check for required anti-spam post data
	$expected_numbers = array( 'rand_total', 'rand_num', 'rand_num2' );
	foreach ( $expected_numbers as $field_name ) {
		if ( ! isset( $_POST[ $field_name ] ) || ! is_numeric( $_POST[ $field_name ] ) ) {
			$errors->add( 'invalid_captcha', __( 'ERROR: Incorrect captcha answer.', 'auctionPlugin' ) );
			return $errors;
		}
	}

	// verify captcha answer
	$rand_post_total = (int) $_POST['rand_total'];
	$rand_total = (int) $_POST['rand_num'] + (int) $_POST['rand_num2'];
	if ( $rand_total != $rand_post_total )
		$errors->add( 'invalid_captcha', __( 'ERROR: Incorrect captcha answer.', 'auctionPlugin' ) );

	// verify senders email
	if ( ! is_email( $_POST['from_email'] ) )
		$errors->add( 'invalid_email', __( 'ERROR: Incorrect email address.', 'auctionPlugin' ) );

	// verify post
	$post = get_post( $post_id );
	if ( ! $post )
		$errors->add( 'invalid_post', __( 'ERROR: Ad does not exist.', 'auctionPlugin' ) );

	if( $pull_fields == "" || $pull_fields == "profile" )
	$mailto = get_the_author_meta( 'user_email', $post->post_author );
	else if( $pull_fields == "custom" )
	$mailto = get_post_meta( $post_id, 'cp_contact_email', true );

	// verify receivers email
	if ( ! is_email( $mailto ) )
		$errors->add( 'invalid_email', __( 'ERROR: Incorrect author email address.', 'auctionPlugin' ) );

	if ( $errors->get_error_code() )
		return $errors;

	$from_name = appthemes_filter( appthemes_clean( $_POST['from_name'] ) );
	$from_email = appthemes_clean( $_POST['from_email'] );
	$subject = appthemes_filter( appthemes_clean( $_POST['subject'] ) );
	$posted_message = appthemes_filter( appthemes_clean( $_POST['message'] ) );

	$sitename = wp_specialchars_decode( get_bloginfo( 'name' ), ENT_QUOTES );
	$siteurl = home_url('/');
	$permalink = get_permalink( $post_id );

	$message = sprintf( __( 'Someone is interested in your ad listing: %s', 'auctionPlugin' ), $permalink ) . "\r\n\r\n";
	$message .= '"' . wordwrap( $posted_message, 70 ) . '"' . "\r\n\r\n";
	$message .= sprintf( __( 'Name: %s', 'auctionPlugin' ), $from_name ) . "\r\n";
	$message .= sprintf( __( 'E-mail: %s', 'auctionPlugin' ), $from_email ) . "\r\n\r\n";
	$message .= '-----------------------------------------' . "\r\n";
	$message .= sprintf( __( 'This message was sent from %s', 'auctionPlugin' ), $sitename ) . "\r\n";
	$message .=  $siteurl . "\r\n\r\n";
	$message .= __( 'Sent from IP Address: ', 'auctionPlugin' ) . appthemes_get_ip() . "\r\n\r\n"; 

	$email = array( 'to' => $mailto, 'subject' => $subject, 'message' => $message, 'from' => $from_email, 'from_name' => $from_name );
	$email = apply_filters( 'cp_email_user_ad_contact', $email, $post_id );

	APP_Mail_From::apply_once( array( 'email' => $email['from'], 'name' => $email['from_name'], 'reply' => true ) );
	wp_mail( $email['to'], $email['subject'], $email['message'] );

	return $errors;
}


/*
|--------------------------------------------------------------------------
| CHANGE THE AUTHOR URL TO CUSTOM URL
|--------------------------------------------------------------------------
*/

function cp_auction_author_permalinks() {

    global $wp_rewrite;
    $aslug = get_option( 'cp_auction_author_page_slug' );
    if( $aslug ) $slug = $aslug;
    else return;

    // Change the value of the author permalink base here
    $wp_rewrite->author_base = $slug;
    $wp_rewrite->author_structure = '/' . $wp_rewrite->author_base. '/%author%';

}
add_action( 'init','cp_auction_author_permalinks' );


/*
|--------------------------------------------------------------------------
| UNHOOK FOOTER META DEFAULT FUNCTIONS
|--------------------------------------------------------------------------
*/


function unhook_footer_meta_functions() {

	remove_action( 'appthemes_after_post_content', 'cp_blog_post_meta_footer' );

	if( get_option('stylesheet') == "flatpress" && get_option( 'cp_version' ) < '3.4' )
	remove_action( 'appthemes_after_post_content', 'fl_blog_post_meta_footer', 100 );
}
add_action( 'init','unhook_footer_meta_functions' );

function cp_auction_meta_footer() {
	global $post, $cp_options;
	if ( ! is_singular( array( 'post', APP_POST_TYPE ) ) )
		return;
?>		
	<div class="prdetails">
	    <?php if ( is_singular( 'post' ) ) { ?>
        <p class="tags"><?php if ( get_the_tags() ) echo the_tags( '', '&nbsp;', '' ); else _e( 'No Tags', 'auctionPlugin' ); ?></p>
        <?php } else { ?>
        <p class="tags"><?php if ( get_the_term_list( $post->ID, APP_TAX_TAG ) ) echo get_the_term_list( $post->ID, APP_TAX_TAG, '', '&nbsp;', '' ); else _e( 'No Tags', 'auctionPlugin' ); ?></p>
        <?php } ?>
        <?php if ( $cp_options->ad_stats_all && current_theme_supports( 'app-stats' ) ) { ?><p class="stats"><?php appthemes_stats_counter( $post->ID ); ?></p> <?php } ?>
        <p class="print"><?php if ( function_exists('wp_email') ) email_link(); ?>&nbsp;&nbsp;<?php if ( function_exists('wp_print') ) print_link(); ?></p>
        <?php cp_auction_edit_ad_link(); ?>
    </div>
    
<?php
}

function cp_fl_blog_post_meta_footer() {
	global $post, $cp_options;
	if ( ! is_singular( array( 'post', APP_POST_TYPE ) ) )
		return;
?>		
	<div class="content-bar text-footer iconfix">
	    <?php if ( is_singular( 'post' ) ) { ?>
        <p class="tags"><i class="icon-tags"></i><?php if ( get_the_tags() ) echo the_tags( '', ', ', '' ); else _e( 'No Tags', 'auctionPlugin' ); ?></p>
        <?php } else { ?>
        <p class="tags"><i class="icon-tags"></i><?php if ( get_the_term_list( $post->ID, APP_TAX_TAG ) ) echo get_the_term_list( $post->ID, APP_TAX_TAG, '', ', ', '' ); else _e( 'No Tags', 'auctionPlugin' ); ?></p>
        <?php } ?>
        <?php if ( $cp_options->ad_stats_all && current_theme_supports( 'app-stats' ) ) { ?><p class="stats"><i class="icon-bar-chart"></i><?php appthemes_stats_counter( $post->ID ); ?></p> <?php } ?>
    </div>
	<?php cp_auction_edit_ad_link(); ?>
	<?php if( $cp_options->ad_stats_all ) {
		appthemes_reset_stats_link();
	} ?>
	<?php if ( function_exists('wp_email') ) { ?>
		<p class="edit"><?php email_link(); ?></p>
	<?php } ?>
	<?php if ( function_exists('wp_print') ) { ?>
		<p class="edit"><?php print_link(); ?></p>
	<?php } ?>
    
<?php
}
if( get_option('stylesheet') == "flatpress" && get_option( 'cp_version' ) < '3.4' )
add_action( 'appthemes_after_post_content', 'cp_fl_blog_post_meta_footer', 100 );
else if( get_option('stylesheet') != "flatpress" )
add_action( 'appthemes_after_post_content', 'cp_auction_meta_footer', 100 );


/*
|--------------------------------------------------------------------------
| CREATES NEW EDIT AD LINK, USE ONLY IN LOOP
|--------------------------------------------------------------------------
*/

function cp_auction_edit_ad_link() {
  global $post, $current_user, $cp_options;
  $my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
  if( is_user_logged_in() ) :
    if( current_user_can('manage_options') ) {
      edit_post_link( __( 'Edit Post', 'auctionPlugin' ), '<p class="edit">', '</p>', $post->ID );
    } elseif( $cp_options->ad_edit && $post->post_author == $current_user->ID ) {
      $edit_link = esc_url( add_query_arg('aid', $post->ID, CP_EDIT_URL) );
      echo '<p class="edit"><a class="post-edit-link" href="'.$edit_link.'&type='.$my_type.'" title="'. __( 'Edit Ad', 'auctionPlugin' ) .'">'. __( 'Edit Ad', 'auctionPlugin' ) .'</a></p>';
    }
  endif;
}


/*
|--------------------------------------------------------------------------
| UNHOOK CLASSIPRESS FUNCTIONS
|--------------------------------------------------------------------------
*/

function cp_auction_unhook_cp_functions() {

	$ct = get_option('stylesheet');
	$sarray = array( 'phoenix', 'eldorado', 'multiport', 'ultraclassifieds', 'flatron', 'citrus-night' );
	remove_action( 'appthemes_before_post_title', 'cp_ad_loop_price' );
	if( in_array( get_option('stylesheet'), $sarray ) )
	remove_action( 'appthemes_before_post_title', 'cp_remove_loop_price' );
	if( $ct == "jibo" )
	remove_action( 'appthemes_before_post_title', 'jibo_ad_loop_price' );
}
add_action( 'appthemes_init','cp_auction_unhook_cp_functions', 11 );

function cp_auction_ad_loop_price() {
	global $post;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$ct = get_option('stylesheet');

	if ( $post->post_type != APP_POST_TYPE )
		return;

	$price = get_post_meta( $post->ID, 'cp_price', true );

	if ( !empty( $price ) && $price > 0 ) { ?>
	<?php if( $ct == "jibo" && is_singular( APP_POST_TYPE ) ) { ?>
	<div class="price-wrap-single">
	<p class="post-price-single"><?php cp_get_price( $post->ID, 'cp_price' ); ?></p>
	</div>
	<?php } else { ?>
	<?php if( $ct == "jibo" ) { ?>
	<div class="price-wrap">
	<p class="post-price"><?php cp_get_price( $post->ID, 'cp_price' ); ?></p>
	</div>
	<?php } else { ?>
	<div class="tags price-wrap">
		<span class="tag-head"><p class="post-price"><?php cp_get_price( $post->ID, 'cp_price' ); ?></p></span>
		<?php if ( get_option('cp_auction_enable_negotiable') == "yes" && get_post_meta( $post->ID, 'cp_price_negotiable', true ) ) echo '<span class="negotiable-ads"></span>'; else echo ''; ?>
	</div>
	<?php }
	}
	} elseif( empty( $price ) && $my_type == "wanted" ) { ?>

	<div class="tags price-wrap">
		<span class="tag-head"><p class="post-price"><?php _e( 'Wanted', 'auctionPlugin' ); ?></p></span>
	</div>

	<?php } else {
		// do not display empty price field
	}
}
if( get_option('stylesheet') != "eclassify" )
add_action( 'appthemes_before_post_title', 'cp_auction_ad_loop_price' );


/*
|--------------------------------------------------------------------------
| UNHOOK ECLASSIFY DEFAULT FUNCTIONS AND SPECIFY NEW ONES
|--------------------------------------------------------------------------
*/

function unhook_eclassify_functions() {
	remove_action( 'appthemes_before_post_title', 'cp_remove_loop_price' );
	add_action( 'appthemes_before_post_title', 'cp_eclassify_loop_price' );
}
if( get_option('stylesheet') == "eclassify" )
add_action( 'appthemes_init', 'unhook_eclassify_functions' );
 
function cp_eclassify_loop_price() {
	if ( is_page() ) return; // don't do ad-meta on pages
	global $post;
	if ( $post->post_type == 'page' || $post->post_type == 'post' ) return;
	$price = get_post_meta($post->ID, 'cp_price', true);
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

	if ( !empty( $price ) AND ( $price>0 ) ) { ?>
	<div class="price-wrap">
	<span class="tag-head">&nbsp;</span><p class="post-price">
	<?php if ( get_post_meta( $post->ID, 'price', true ) ) cp_get_price_legacy( $post->ID );
	else cp_get_price( $post->ID, 'cp_price' ); ?></p>
	</div>
	<?php
	} elseif( empty( $price ) && $my_type == "wanted" ) { ?>

	<div class="price-wrap">
		<span class="tag-head">&nbsp;</span><p class="post-price"><?php _e( 'Wanted', 'auctionPlugin' ); ?></p>
	</div>

	<?php } else {
		// do not display empty price field
	}
}


/*
|--------------------------------------------------------------------------
| UNHOOK SIMPLY RESPONSIVE DEFAULT FUNCTIONS AND SPECIFY NEW ONES
|--------------------------------------------------------------------------
*/

function unhook_simply_functions() {
	remove_action( 'appthemes_before_post_title', 'cp_auction_ad_loop_price' );
	remove_action( 'appthemes_loop_else_ft', 'cp_ad_loop_else_ft' );
	remove_action( 'appthemes_before_post_title', 'ft_ad_loop_price' );
	remove_action( 'appthemes_before_post_title', 'ftm_ad_loop_price' );
	remove_action( 'appthemes_after_post_title', 'ftm_ad_loop_meta' );
	add_action( 'appthemes_loop_else_ft', 'cp_auction_loop_else_ft');
	add_action( 'appthemes_before_post_title', 'cp_auction_modify_price' );
}
if( get_option('stylesheet') == "simply-responsive-cp" )
add_action( 'appthemes_init','unhook_simply_functions', 11 );

function cp_auction_loop_else_ft() {
?>
    <div class="shadowblock_out">
		<div class="shadowblock">

			<div class="pad10"></div>
			<p class="text-center"><?php _e( 'Sorry, no listings were found.', 'auctionPlugin' ); ?></p>
			<div class="pad10"></div>

		</div><!-- /shadowblock -->
	</div><!-- /shadowblock_out -->
<?php
}


// Update the price field if ad type wanted
function cp_auction_modify_price() {
	global $post, $cp_options;
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

	if ( $post->post_type != APP_POST_TYPE )
		return;

	$price = get_post_meta( $post->ID, 'cp_price', true );

	if ( !empty( $price ) && $price > 0 ) { // unless empty add the ad price field in the loop as usual
	?>

		<div class="price-wrap">
			<span class="tag-head">&nbsp;</span><p class="post-price"><?php if ( get_post_meta( $post->ID, 'price', true ) ) cp_get_price_legacy( $post->ID ); else cp_get_price( $post->ID, 'cp_price' ); ?></p>
		</div>

	<?php } elseif( empty( $price ) && $my_type == "wanted" ) { ?>

		<div class="price-wrap">
			<span class="tag-head">&nbsp;</span><p class="post-price"><?php _e( 'Wanted', 'auctionPlugin' ); ?></p>
		</div>

	<?php } else {
		// do not display empty price field
	}
}


/*
|--------------------------------------------------------------------------
| UNHOOK RONDELL DEFAULT FUNCTIONS
|--------------------------------------------------------------------------
*/

function unhook_rondell_functions() {
	remove_action( 'appthemes_before_post_title', 'cp_auction_ad_loop_price' );
	remove_action( 'appthemes_before_post_title', 'wpm_remove_loop_price' );
}
if( get_option('stylesheet') == "redrondell" || get_option('stylesheet') == "rondell_370" || get_option('stylesheet') == "rondell" )
add_action('init','unhook_rondell_functions');

function cp_auction_remove_loop_price() {
    global $post;
    $my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

      if ( $post->post_type != APP_POST_TYPE )
		return;

      $price = get_post_meta($post->ID, 'cp_price', true); 
      if ( $my_type != "wanted" && !empty( $price ) AND ( $price > 0 ) ) {
?>        
    <div class="price-wrap">
    <?php if ( get_post_meta( $post->ID, 'cp_ad_sold', true ) == 'yes' ) echo '<span class="tag-head-sold"></span>'; 
	 elseif  ( get_post_meta( $post->ID, 'cp_price_negotiable', true ) ) echo '<span class="tag-head-plus"></span>';
	 else echo '<span class="tag-head"></span>'; ?> <p class="post-price">

    <?php if ( get_post_meta( $post->ID, 'price', true ) ) cp_get_price_legacy( $post->ID );
     else cp_get_price( $post->ID, 'cp_price' ); ?></p>
        </div>
 <?php
} else { ?>
    <div class="price-wrap">
       <?php if ( !empty( $price ) AND ( $price > 0 ) ) { ?>
       <span title="<?php _e('Max. Price:', 'auctionPlugin'); ?> <?php echo cp_display_price( $price, 'ad', false ); ?>" class="tag-zero"></span>
       <?php } else { ?>
       <span class="tag-zero"></span>
       <?php } ?>
        </div>
 <?php
}
}
if( get_option('stylesheet') == "rondell" )
add_action( 'appthemes_before_post_title', 'cp_auction_remove_loop_price' );

/*
|--------------------------------------------------------------------------
| GET TEXT FIELD OR DROPDOWN BASED ON FIELD TYPE
|--------------------------------------------------------------------------
*/

function cp_auction_get_dropdown( $field_name, $uid = '' ) {

	global $post, $cp_options, $wpdb;

	$table_name = $wpdb->prefix . "cp_ad_fields";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE field_name = '$field_name'");

	foreach ( $results as $result ) {
	if ( $uid == false )
	$meta_val = ( $post ) ? get_post_meta($post->ID, $result->field_name, true) : false;
	else
	$meta_val = get_user_meta($uid, $field_name, true);

	if ( $result->field_type == "drop-down" ) {
	$options = explode( ',', $result->field_values );
	$options = array_map( 'trim', $options );
	$html_options = '';

	$html_options .= html( 'option', array( 'value' => '' ), __( '-- Select --', 'auctionPlugin' ) );
	foreach ( $options as $option ) {
	$args = array( 'value' => $option );
		if ( $option == $meta_val )
		$args['selected'] = 'selected';
		$html_options .= html( 'option', $args, $option );
		}

	$field_class = ( $result->field_req ) ? 'dropdownlist required' : 'dropdownlist';
	if( !$cp_options->selectbox ) {
	$field_class = "cp-dropdown";
	}
	$args = array( 'name' => $result->field_name, 'id' => $result->field_name, 'class' => $field_class );

	echo html( 'select', $args, $html_options );
	echo html( 'div', array( 'class' => 'clr' ) );
	} else {
	$field_class = ( $result->field_req ) ? 'text required' : 'text';
	$field_minlength = ( empty( $result->field_min_length ) ) ? '2' : $result->field_min_length;
	$args = array( 'value' => $value, 'name' => $result->field_name, 'id' => $result->field_name, 'type' => 'text', 'class' => $field_class, 'minlength' => $field_minlength );

	echo html( 'input', $args );
	echo html( 'div', array( 'class' => 'clr' ) );
	}
}
}


/*
|--------------------------------------------------------------------------
| SIMPLE IP BLOCKER CODE
|--------------------------------------------------------------------------
*/

function cp_auction_block_ip() {

$list = get_option('cp_auction_blocked_ips');
$url = get_option('cp_auction_blocked_redirect_url');
$deny = explode(',', $list);
if (in_array ($_SERVER['REMOTE_ADDR'], $deny)) {
   if( $url ) {
   header("location: ".$url."");
   exit();
   } else {
   header("location: http://www.google.com/");
   exit();
   }
   }
}
add_action('appthemes_init', 'cp_auction_block_ip');


/*
|--------------------------------------------------------------------------
| GET USER ROLE BY USER ID
|--------------------------------------------------------------------------
*/

function cp_auction_get_user_role( $user_id ) {
    $user = get_userdata( $user_id );
    return empty( $user ) ? array() : $user->roles;
}


/*
|--------------------------------------------------------------------------
| IF HEADLINE CHILD THEME
|--------------------------------------------------------------------------
*/

function headline_child_themes($stylesheet) {

	$childs = array("classipress-headline-blue", "classipress-headline-green", "classipress-headline-orange", "classipress-headline-purple", "classipress-headline-red");

	if( in_array( $stylesheet, $childs ) )
	return true;
	else return false;
}


/*
|--------------------------------------------------------------------------
| REDIRECT OPTION
|--------------------------------------------------------------------------
*/

function cp_auction_page_redirect($location) {
   echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
}

/* delayed redirect 2 sec*/
function cp_auction_delayed_redirect($location) {
   echo '<META HTTP-EQUIV="Refresh" Content="2; URL='.$location.'">';
}


/*
|--------------------------------------------------------------------------
| CLASSIFIED, WANTED & AUCTION AD DETAILS
|--------------------------------------------------------------------------
*/

add_action('appthemes_before_post_content', 'cp_auction_before_post_content', 10, 3 );

function cp_auction_before_post_content() {

	if( ! is_singular(APP_POST_TYPE) )
		return;

global $current_user, $post, $cpurl;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$max_price = get_post_meta( $post->ID, 'cp_max_price', true );
	$want_to = get_post_meta( $post->ID, 'cp_want_to', true );
	if( empty( $want_to ) ) $want_to = get_post_meta( $post->ID, 'cp_iwant_to', true );
	$swap_with = get_post_meta( $post->ID, 'cp_swap_with', true );
	$cp_demopage_url = get_post_meta($post->ID, 'cp_demopage_url', true);
	$pms_opt = get_user_meta( $post->post_author, 'cartpaujPM_uOptions', true );
	if( $pms_opt ) $pms = $pms_opt['allow_messages'];
	else $pms = false;

	$view_all_url = get_author_posts_url($post->post_author);

	if ( is_singular( APP_POST_TYPE ) && ( $my_type == 'normal' || $my_type == 'reverse' ) ) {

	global $wpdb;
	if (class_exists("cartpaujPM")) {
	$pageID =  $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE post_content LIKE '%[cartpauj-pm]%' AND post_status = 'publish' AND post_type = 'page'");
	$link = get_permalink($pageID);
	}
	$allow_pms = get_user_meta( $post->post_author, 'cp_auction_allow_pms', true );
?>
	<div class="clr"></div>

<?php if( get_option('cp_auction_move_menu_bar') != "yes" ) { ?>

	<div class="cp-ads-meta">
	<div class="cp-ads-links"><?php if( current_user_can( 'manage_options' ) ) { ?><a href="#" class="admin-bump-ads" title="<?php _e('Click here to bump up this ad.', 'auctionPlugin'); ?>" id='<?php echo $post->ID; ?>'><?php _e('Tăng lên', 'auctionPlugin'); ?></a> &nbsp; | &nbsp; <?php } ?><?php if( get_option('cp_auction_plugin_if_watchlist') == "yes" && !cp_auction_check_watchlist($uid, $post->ID) ) { ?><a href="#" class="add-watchlist" title="<?php _e('Add this auction to your watchlist.', 'auctionPlugin'); ?>" id='<?php echo $uid; ?>|<?php echo $post->ID; ?>'><?php _e('Thêm vào danh sách theo dõi', 'auctionPlugin'); ?></a> &nbsp; | &nbsp; <?php } ?><?php if( get_option('cp_auction_plugin_if_favorites') == "yes" && !cp_auction_check_favoritelist($uid, $post->post_author) ) { ?> <a href="#" class="add-favorites" title="<?php _e('Add this author / shop to your favourites.', 'auctionPlugin'); ?>" id='<?php echo $uid; ?>|<?php echo $post->post_author; ?>'><?php _e('Thêm vào danh sách ưa thích', 'auctionPlugin'); ?></a> &nbsp; | &nbsp; <?php } ?> <a href="<?php echo $view_all_url; ?>"><?php _e('Xem hết bài viết', 'auctionPlugin'); ?></a> <?php if ( class_exists("cartpaujPM") && $pms == "true" && $allow_pms == "yes" ) { ?> &nbsp; | &nbsp; <a href="<?php echo $link; ?>?pmaction=newmessage&to=<?php echo $post->post_author; ?>"><?php _e('Send PM', 'auctionPlugin'); ?></a> <?php } ?><?php if ( get_option('cp_auction_enable_report_ad') == "yes" ) { ?> &nbsp; | &nbsp; <a href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$post->post_author.'&pid='.$post->ID.''); ?>"><?php _e('Báo cáo bài viết', 'auctionPlugin'); ?></a> <?php } ?></div>

	</div>

<?php } else { ?>

	<div class="clear20"></div>

<?php } ?>

	<?php if( $cp_demopage_url ) {
	    echo '<div class="clear5"></div>';
	    echo '<a href="'.$cp_demopage_url.'" target="_blank" class="mbtn btn_orange cp-width" type="button">'.__('View Demo','auctionPlugin').'</a>';
	    echo '<div class="clear10"></div>';
	    }
}
elseif ( is_singular( APP_POST_TYPE ) && ( empty( $my_type ) || $my_type == 'classified' || $my_type == 'wanted' ) ) {

	global $wpdb;
	if (class_exists("cartpaujPM")) {
	$pageID =  $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE post_content LIKE '%[cartpauj-pm]%' AND post_status = 'publish' AND post_type = 'page'");
	$link = get_permalink($pageID);
	}
	$allow_pms = get_user_meta( $post->post_author, 'cp_auction_allow_pms', true );
?>
	<div class="clr"></div>

<?php if( get_option('cp_auction_move_menu_bar') != "yes" ) { ?>

	<div class="cp-ads-meta">
	<div class="cp-ads-links"><?php if( current_user_can( 'manage_options' ) ) { ?><a href="#" class="admin-bump-ads" title="<?php _e('Click here to bump up this ad.', 'auctionPlugin'); ?>" id='<?php echo $post->ID; ?>'><?php _e('Bump Up', 'auctionPlugin'); ?></a> &nbsp; | &nbsp; <?php } ?><?php if(( get_option('cp_auction_plugin_if_favorites') == "yes" ) && ( !cp_auction_check_favoritelist($uid, $post->post_author) )) { ?> <a href="#" class="add-favorites" title="<?php _e('Add this author / shop to your favourites.', 'auctionPlugin'); ?>" id='<?php echo $uid; ?>|<?php echo $post->post_author; ?>'><?php _e('Add to Favourites', 'auctionPlugin'); ?></a> &nbsp; | &nbsp; <?php } ?> <a href="<?php echo $view_all_url; ?>"><?php _e('View all Ads', 'auctionPlugin'); ?></a> <?php if ( class_exists("cartpaujPM") && $pms == "true" && $allow_pms == "yes" ) { ?> &nbsp; | &nbsp; <a href="<?php echo $link; ?>?pmaction=newmessage&to=<?php echo $post->post_author; ?>"><?php _e('Send PM', 'auctionPlugin'); ?></a> <?php } ?><?php if ( get_option('cp_auction_enable_report_ad') == "yes" ) { ?> &nbsp; | &nbsp; <a href="<?php echo cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$post->post_author.'&pid='.$post->ID.''); ?>"><?php _e('Report Ad', 'auctionPlugin'); ?></a> <?php } ?></div>

	<div class="clear5"></div>

<?php } else { ?>

	<div class="cp-ads-meta">

<?php } ?>

	<?php if( $my_type == 'wanted' && $want_to ) {
	    echo '<div class="clear15"></div>
	    <div class="swap-info"><strong>'.__('Want to','auctionPlugin').' '.$want_to.'</strong>'; if( $swap_with ) echo ': '.$swap_with.''; if( empty( $swap_with ) && $max_price > 0 ) echo ', '.__('Max. price','auctionPlugin').' '.cp_display_price( $max_price, 'ad', false ).'';
	    echo '</div>';
	    } ?>

	<div class="clr"></div>

	<?php if( $cp_demopage_url ) {
	    echo '<div class="clear5"></div>';
	    echo '<a href="'.$cp_demopage_url.'" target="_blank" class="mbtn btn_orange cp-width" type="button">'.__('View Demo','auctionPlugin').'</a>';
	    echo '<div class="clear10"></div>';
?>
	<div class="clr"></div>

<?php } ?>
	</div>
<?php
}
}


/*
|--------------------------------------------------------------------------
| GET URLS
|--------------------------------------------------------------------------
*/

function cp_auction_url($rewrite, $standard, $extra = '') {

	global $cpurl;
	if( $cpurl['rewrite'] == "yes" ) {
		if( empty( $extra ) ) {
		return ''.get_bloginfo('wpurl').'/'.$rewrite.'/';
		} else {
		return ''.get_bloginfo('wpurl').'/'.$rewrite.'/?'.$extra.'';
		}
	}
	else {
		if( empty( $extra ) ) {
		return ''.get_bloginfo('wpurl').'/'.$standard.'';
		} else {
		return ''.get_bloginfo('wpurl').'/'.$standard.'&amp;'.$extra.'';
		}
	}
}


/*
|--------------------------------------------------------------------------
| TRANSLATE .JS CONFIRM TEXT
|--------------------------------------------------------------------------
*/

function cp_auction_localize_vars() {
    return array(
        'AddToFavorites' => __('Are you sure you want to add this to your favorites?', 'auctionPlugin'),
        'DeleteComplPurchase' => __('Are you sure you want to delete this purchase?', 'auctionPlugin'),
        'AddToWatchlist' => __('Are you sure you want to add this to your watchlist?', 'auctionPlugin'),
        'DeleteFavorite' => __('Are you sure you want to delete this favourite?', 'auctionPlugin'),
        'DeleteWatchlist' => __('Are you sure you want to delete this listing from your watchlist?', 'auctionPlugin'),
        'DeleteBid' => __('Are you sure you want to delete this bid?', 'auctionPlugin'),
        'DeleteWantedOffer' => __('Are you sure you want to delete this offer?', 'auctionPlugin'),
        'DeleteCoupon' => __('Are you sure you want to delete this coupon?', 'auctionPlugin'),
        'DeleteUploads' => __('Are you sure you want to delete these files?', 'auctionPlugin'),
        'MarkAsPaid' => __('Are you sure you want to mark it as paid?', 'auctionPlugin'),
        'DeactivatePurchase' => __('Are you sure you want to deactivate this purchase?', 'auctionPlugin'),
        'ConfirmText' => __('Are you sure you want to continue?', 'auctionPlugin'),
        'UpdateList' => __('Please wait while the list is being updated...', 'auctionPlugin'),
        'DeletingBids' => __('Please wait while the bid is being deleted...', 'auctionPlugin'),
        'DeletingUploads' => __('Please wait while the uploads is being deleted...', 'auctionPlugin'),
        'FreightCost' => __('Enter the cost as flat shipping cost, ie. 10 or 10.00 (numeric values only).', 'auctionPlugin'),
        'ShippingOptions' => get_localized_field_values( 'cp_shipping_options', 'Special delivery', 'locale' )
    );
} //End localize_vars


/*
|--------------------------------------------------------------------------
| GET LOCALIZED CUSTOM FIELD VALUES
|--------------------------------------------------------------------------
*/

function get_localized_field_values($fname, $fvalue, $local = '') {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = '$fname'");

	if( $row ) {
	$field = explode(",",$row->field_values);
	$trans = explode(",",$row->trans_values);
	if( ! $local ) {
	if( isset( $trans[0] ) && $trans[0] !== "" && isset( $field[0] ) && $field[0] !== "" && $fvalue == $field[0] ) return $trans[0];
	elseif( isset( $trans[1] ) && $trans[1] !== "" && isset( $field[1] ) && $field[1] !== "" && $fvalue == $field[1] ) return $trans[1];
	elseif( isset( $trans[2] ) && $trans[2] !== "" && isset( $field[2] ) && $field[2] !== "" && $fvalue == $field[2] ) return $trans[2];
	elseif( isset( $trans[3] ) && $trans[3] !== "" && isset( $field[3] ) && $field[3] !== "" && $fvalue == $field[3] ) return $trans[3];
	elseif( isset( $trans[4] ) && $trans[4] !== "" && isset( $field[4] ) && $field[4] !== "" && $fvalue == $field[4] ) return $trans[4];
	else return $fvalue;
	} else {
	if( isset( $field[0] ) && $field[0] !== "" && isset( $trans[0] ) && $field[0] !== "" && $fvalue == $trans[0] ) return $field[0];
	elseif( isset( $field[1] ) && $field[1] !== "" && isset( $trans[1] ) && $field[1] !== "" && $fvalue == $trans[1] ) return $field[1];
	elseif( isset( $field[2] ) && $field[2] !== "" && isset( $trans[2] ) && $field[2] !== "" && $fvalue == $trans[2] ) return $field[2];
	elseif( isset( $field[3] ) && $field[3] !== "" && isset( $trans[3] ) && $field[3] !== "" && $fvalue == $trans[3] ) return $field[3];
	elseif( isset( $field[4] ) && $field[4] !== "" && isset( $trans[4] ) && $field[4] !== "" && $fvalue == $trans[4] ) return $field[4];
	else return $fvalue;
	}
	}
	else {
		return $fvalue;
	}
}


/*
|--------------------------------------------------------------------------
| COUNT USER POSTS
|--------------------------------------------------------------------------
*/

function cp_auction_count_user_posts($uid, $post_type = APP_POST_TYPE) {
  	global $wpdb;
  	$where = get_posts_by_author_sql($post_type, TRUE, $uid);
  	$table_name = $wpdb->prefix . "posts";
  	$count = $wpdb->get_var( "SELECT COUNT(*) FROM {$table_name} $where" );
  	return apply_filters('get_usernumposts', $count, $uid);
}


/*
|--------------------------------------------------------------------------
| GET USERS REAL IP
|--------------------------------------------------------------------------
*/

function cp_auction_users_ip() {

$ip="unknown"; //catch the missed 1%

if (strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'mobile') || strstr(strtolower($_SERVER['HTTP_USER_AGENT']), 'android')) { //check for mobile devices
$ip = "mobile";
}

elseif (!empty($_SERVER['HTTP_CLIENT_IP'])) { //check if IP is from shared Internet
$ip=$_SERVER['HTTP_CLIENT_IP'];
}

elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { //check if IP is passed from proxy
$ip_array=explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
$ip=trim($ip_array[count($ip_array) - 1]);
}

elseif (!empty($_SERVER['REMOTE_ADDR'])) { //standard IP check
$ip=$_SERVER['REMOTE_ADDR'];
}

return $ip;
}


/*
|--------------------------------------------------------------------------
| GET POST ID BY META KEY AND VALUE
|--------------------------------------------------------------------------
*/

function cp_auction_get_post_id($key, $value) {
	global $wpdb;
	$key = esc_sql( $key );
	$value = esc_sql( $value );
	$meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='".$key."' AND meta_value='".$value."'");
	if (is_array($meta) && !empty($meta) && isset($meta[0])) {
	$meta = $meta[0];
	}	
	if (is_object($meta)) {
	return $meta->post_id;
	}
	else {
	return false;
	}
}


/*
|--------------------------------------------------------------------------
| GET CATEGORIES DROPDOWN
|--------------------------------------------------------------------------
*/

function cp_auction_get_cat_dropdown($cc = '', $tab = '', $parent = '') {
	global $cp_options;
	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
}
$cats_parent_auction = get_option('cp_auction_cats_parent_auction');
$cats_parent_market = get_option('cp_auction_cats_parent_market');
if(( $parent == "normal" || $parent == "reverse" ) && ( $cats_parent_auction )) $ids = get_option('cp_auction_cats_parent_auction');
if( $parent == "classified" && $cats_parent_market ) $ids = get_option('cp_auction_cats_parent_market');
if( $parent && $ids ) {
wp_dropdown_categories(array('class' => $new_class, 'taxonomy' => APP_TAX_CAT, 'hide_empty' => 0, 'parent' => $ids, 'name' => 'category_parent', 'id' => 'category_parent', 'tabindex' => $tab, 'orderby' => 'name', 'selected' => $cc, 'hierarchical' => true, 'show_option_none' => __('Select Category','auctionPlugin')));
}
else {
wp_dropdown_categories(array('class' => $new_class, 'taxonomy' => APP_TAX_CAT, 'hide_empty' => 0, 'name' => 'category_parent', 'id' => 'category_parent', 'tabindex' => $tab, 'orderby' => 'name', 'selected' => $cc, 'hierarchical' => true, 'show_option_none' => __('Select Category','auctionPlugin')));
}
}


/*
|--------------------------------------------------------------------------
| GET YES / NO DROPDOWN
|--------------------------------------------------------------------------
*/

function cp_auction_yes_no( $select = '' ) {

$options = "";
$what = array(
		'no' => __('No','auctionPlugin'),
		'yes' => __('Yes','auctionPlugin')
		);

	foreach($what as $what_id=>$yes_no) {
	if($select == $what_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$what_id.'">'.$yes_no.'</option>';
	} else {
	$options .= '<option value="'.$what_id.'">'.$yes_no.'</option>';
	}
	}
		return $options;
}


/*
|--------------------------------------------------------------------------
| GET THE AD TYPE
|--------------------------------------------------------------------------
*/

function cp_auction_select_auction_type( $select = '' ) {

	global $current_user;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

$act = get_option('cp_auction_plugin_auctiontype');
$classified = get_option('cp_auction_plugin_classified') ? esc_attr( 'classified' ) : '';
$wanted = get_option('cp_auction_plugin_wanted') ? esc_attr( 'wanted' ) : '';
$normal = get_option('cp_auction_plugin_normal') ? esc_attr( 'normal' ) : '';
$reverse = get_option('cp_auction_plugin_reverse') ? esc_attr( 'reverse' ) : '';
$only_admins = get_option('cp_auction_only_admins_post_auctions');

$list = ''.$classified.','.$wanted.','.$normal.','.$reverse.'';
$array = explode( ',', $list );

$options = "";
if( $only_admins == "yes" && current_user_can( 'manage_options' ) ) {
$types = array(
		'' => get_option('cp_auction_dropdown_select_title'),
		'classified' => get_option('cp_auction_dropdown_classified_title'),
		'wanted' => get_option('cp_auction_dropdown_wanted_title'),
		'normal' => get_option('cp_auction_dropdown_auction_title'),
		'reverse' => get_option('cp_auction_dropdown_reverse_title')
		);

	foreach($types as $type_id=>$type) {
	if($select == $type_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$type_id.'">'.$type.'</option>';
	} else {
	$options .= '<option value="'.$type_id.'">'.$type.'</option>';
	}
	}
		return $options;

} elseif( $act == "mixed" ) {
$types = array(
		'' => get_option('cp_auction_dropdown_select_title'),
		'classified' => get_option('cp_auction_dropdown_classified_title'),
		'wanted' => get_option('cp_auction_dropdown_wanted_title'),
		'normal' => get_option('cp_auction_dropdown_auction_title'),
		'reverse' => get_option('cp_auction_dropdown_reverse_title')
		);

	foreach($types as $type_id=>$type) {
	if($select == $type_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$type_id.'">'.$type.'</option>';
	} else {
	if( in_array($type_id, $array) )
	$options .= '<option value="'.$type_id.'">'.$type.'</option>';
	}
	}
		return $options;

} elseif( $act == "normal" ) {
$types = array(
		'' => get_option('cp_auction_dropdown_select_title'),
		'classified' => $classified == true ? get_option('cp_auction_dropdown_classified_title') : '',
		'normal' => $normal == true ? get_option('cp_auction_dropdown_auction_title') : '',
		);

	foreach($types as $type_id=>$type) {
	if($select == $type_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$type_id.'">'.$type.'</option>';
	} else {
	$options .= '<option value="'.$type_id.'">'.$type.'</option>';
	}
	}
		return $options;

} elseif( $act == "reverse" ) {
$types = array(
		'' => get_option('cp_auction_dropdown_select_title'),
		'classified' => $classified == true ? get_option('cp_auction_dropdown_classified_title') : '',
		'reverse' => $reverse == true ? get_option('cp_auction_dropdown_reverse_title') : '',
		);

	foreach($types as $type_id=>$type) {
	if($select == $type_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$type_id.'">'.$type.'</option>';
	} else {
	$options .= '<option value="'.$type_id.'">'.$type.'</option>';
	}
	}
		return $options;

} elseif( $act == "classified" ) {
$types = array(
		'' => get_option('cp_auction_dropdown_select_title'),
		'classified' => $classified == true ? get_option('cp_auction_dropdown_classified_title') : '',
		'wanted' => $wanted == true ? get_option('cp_auction_dropdown_wanted_title') : '',
		);

	foreach($types as $type_id=>$type) {
	if($select == $type_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$type_id.'">'.$type.'</option>';
	} else {
	$options .= '<option value="'.$type_id.'">'.$type.'</option>';
	}
	}
		return $options;

} elseif( $act == "wanted" ) {
$types = array(
		'' => get_option('cp_auction_dropdown_select_title'),
		'classified' => $classified == true ? get_option('cp_auction_dropdown_classified_title') : '',
		'wanted' => $wanted == true ? get_option('cp_auction_dropdown_wanted_title') : '',
		);

	foreach($types as $type_id=>$type) {
	if($select == $type_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$type_id.'">'.$type.'</option>';
	} else {
	$options .= '<option value="'.$type_id.'">'.$type.'</option>';
	}
	}
		return $options;

} else {
$types = array(
		'' => get_option('cp_auction_dropdown_select_title'),
		'classified' => $classified == true ? get_option('cp_auction_dropdown_classified_title') : '',
		);

	foreach($types as $type_id=>$type) {
	if($select == $type_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$type_id.'">'.$type.'</option>';
	} else {
	$options .= '<option value="'.$type_id.'">'.$type.'</option>';
	}
	}
		return $options;
}
}


/*
|--------------------------------------------------------------------------
| GET THE AD TYPE BASED ON USERS SETTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_select_ad_type( $select = '' ) {

$options = "";
$types = array(
		'' => __('Select Ad Type','auctionPlugin'),
		'Sell' => __('For Sale','auctionPlugin'),
		'Buy' => __('Want to Buy','auctionPlugin'),
		'Swap' => __('Want to Swap','auctionPlugin'),
		'Give Away' => __('Give Away','auctionPlugin'),
		);

	foreach($types as $type_id=>$type) {
	if($select == $type_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$type_id.'">'.$type.'</option>';
	} else {
	$options .= '<option value="'.$type_id.'">'.$type.'</option>';
	}
	}
		return $options;

}


/*
|--------------------------------------------------------------------------
| GET HIGHEST BID FROM DB
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_get_highest_bid($pid) {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$bid = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$pid' AND (type = '' OR type = 'bid') ORDER BY bid DESC");

	if( $bid )
		return $bid->bid;
	else return 0;

}


/*
|--------------------------------------------------------------------------
| GET LATEST BID OR START PRICE FROM DB
|--------------------------------------------------------------------------
*/

function cp_auction_plugin_get_latest_bid($pid, $bid = '') {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$lastbid = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$pid' AND (type = '' OR type = 'bid') ORDER BY id DESC");

	if( !$lastbid && !$bid ) {

		$cp_start_price = get_post_meta($pid,'cp_start_price',true);

		if( !$cp_start_price ) return 0;
		else return $cp_start_price;
	
	}
	elseif( $lastbid ) {

		return $lastbid->bid;		
	}
	else {

		return 0;
	}
}


/*
|--------------------------------------------------------------------------
| CHECK AND CLOSE EXPIRED AUCTIONS
|--------------------------------------------------------------------------
*/

function end_auction($pid) {

	if( !$pid ) return;

	global $post;
	$post = get_post($pid);
	$cp_ad_sold = get_post_meta( $post->ID,'cp_ad_sold',true );
	$cbid = cp_auction_plugin_get_latest_bid($post->ID, 'bid');
	$dt2 = strtotime( get_post_meta( $post->ID,'cp_sys_expire_date',true ) );
	$today = strtotime(current_time('mysql'));
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );

if (( $dt2 > 0 ) && ( $dt2 <= $today && $cp_ad_sold != "yes" && $post->post_status == "publish" )) {

		$my_post = array();
		$my_post['ID'] = $post->ID;
		$my_post['post_status'] = 'draft';
		wp_update_post( $my_post );

	update_post_meta( $post->ID,'cp_ad_sold','yes' );
	update_post_meta( $post->ID,'cp_ad_sold_date',$today );

	cp_auction_listing_has_ended_emails( $post->ID, $post, $post->post_author );

	if($cbid > 0) auction_closed_do_action($post->ID);
}
	return get_post_meta($post->ID,'cp_ad_sold',true);
}


/*
|--------------------------------------------------------------------------
| AUCTION CLOSED DO ACTION
|--------------------------------------------------------------------------
*/

function auction_closed_do_action($pid) {

	global $wpdb, $post;
	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$bids = $wpdb->get_row("SELECT * FROM {$table_bids} WHERE pid = '$pid' ORDER BY id DESC");

	$bid = $bids->bid;
	$uid = $bids->uid;
	$tm = $bids->datemade;

	$post = get_post($pid);
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$cp_shipping = get_post_meta($post->ID, 'cp_shipping', true);
	if ( empty( $cp_shipping ) )
	$cp_shipping = get_user_meta( $post->post_author, 'cp_auction_shipping', true );

	$cp_min_price = get_post_meta( $post->ID,'cp_min_price',true );
	if(empty($cp_min_price)) $cp_min_price = 0;

if(( $my_type == "normal" || $my_type == "reverse" ) && ( $bid >= $cp_min_price )) {

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$where_array = array('pid' => $post->ID, 'datemade' => $tm, 'bid' => $bid, 'uid' => $uid);
	$wpdb->update($table_name, array('winner' => 1), $where_array);

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$winbid = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$post->ID' AND datemade = '$tm' AND bid = '$bid' AND uid = '$uid'");
	$win_bid = $winbid->id;

	update_post_meta( $post->ID,'winner',$uid );
	update_post_meta( $post->ID,'winner_bid',$win_bid );
	update_post_meta( $post->ID,'winner_amount',$bid );

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$unpaid_total = $bid + $cp_shipping;
	$query = "INSERT INTO {$table_purchases} (pid, uid, buyer, item_name, amount, mc_gross, datemade, numbers, unpaid, net_total, unpaid_total, type, status) VALUES (%d, %d, %d, %s, %f, %f, %d, %d, %d, %f, %f, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $uid, $post->post_title, $bid, $unpaid_total, $tm, '1', '1', $bid, $unpaid_total, 'classified', 'unpaid'));

	cp_auction_choose_winner_email( $post->ID, $post, $uid, $bid );

	return 1;
} else {
	return false;
}
}


/*
|--------------------------------------------------------------------------
| DELETE BIDS
|--------------------------------------------------------------------------
*/

function cp_auction_delete_bids($empty = '') {

	global $post, $wpdb;
	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";

	if ( $empty == "empty" ) {
	$wpdb->query("TRUNCATE TABLE {$table_bids}");

	return 2;

	}

	$bids = $wpdb->get_results("SELECT * FROM {$table_bids}");

	if( $bids ) {
	foreach($bids as $bid) {

	$pid = $bid->pid;
	$post = get_post($pid);
	if(empty($post)) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_bids} WHERE pid = '%d'", $pid));
	}
}

}
	return 2;
}

/*
|--------------------------------------------------------------------------
| DELETE PURCHASES
|--------------------------------------------------------------------------
*/

function cp_auction_delete_purchases($empty = '') {

	global $post, $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";

	if ( $empty == "empty" ) {
	$wpdb->query("TRUNCATE TABLE {$table_name}");

	return 2;

	}

	$purchases = $wpdb->get_results("SELECT * FROM {$table_name} ORDER BY id DESC");

	if( $purchases ) {
	foreach($purchases as $purchase) {

	$pid = $purchase->pid;
	$post = get_post($pid);
	if(empty($post)) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE pid = '%d'", $pid));
	}
}

}
	return 2;
}

/*
|--------------------------------------------------------------------------
| DELETE TRANSACTIONS
|--------------------------------------------------------------------------
*/

function cp_auction_delete_transactions($empty = '') {

	global $post, $wpdb;
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";

	if ( $empty == "empty" ) {
	$wpdb->query("TRUNCATE TABLE {$table_transactions}");

	return 2;

	}

	$result = $wpdb->get_results("SELECT * FROM {$table_transactions} WHERE pid > '0' OR post_ids > '0'");

	if( $result ) {
	foreach($result as $res) {

	$trans_id = $res->id;
	$post_id = $res->pid;
	$post_ids = $res->post_ids;
	$pids = explode(",",$post_ids);
	foreach($pids as $pid) {
	$post = get_post($pid);
	if(empty($post)) {
	unset($pids[array_search($pid, $pids)]);
	$upd_list = implode(",", $pids);
	$ids = trim($upd_list, ',');
	if ( empty( $ids ) ) {
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_transactions} WHERE id = '%d'", $trans_id));
	} else {
	$where_array = array('id' => $trans_id);
	$wpdb->update($table_transactions, array('post_ids' => $ids), $where_array);
	}
	}
	}
	$post = get_post($post_id);
	if(empty($post)) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_transactions} WHERE pid = '%d'", $pid));
	}
}
}
	return 2;
}


/*
|--------------------------------------------------------------------------
| DELETE ALL BIDS BEFORE AUCTION RELISTING
|--------------------------------------------------------------------------
*/

function delete_relist_bids($ids) {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$bids = $wpdb->get_results("SELECT * FROM {$table_name} WHERE pid = '$ids' ORDER BY id DESC");

	if( $bids ) {
	foreach($bids as $bid) {

	$pid = $bid->pid;
	$post = get_post($pid);
	if( $post ) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE pid = '%d'", $pid));
	}
	}
}
}


/*
|--------------------------------------------------------------------------
| DELETE SINGLE PURCHASE HISTORY
|--------------------------------------------------------------------------
*/

function delete_purchase($ids) {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$wpdb->query( $wpdb->prepare( "DELETE FROM {$table_name} WHERE pid = %d ", $ids ));
}


/*
|--------------------------------------------------------------------------
| DELETE WATCHLIST
|--------------------------------------------------------------------------
*/

function cp_auction_delete_watchlist($empty = '') {

	global $post, $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";

	if ( $empty == "empty" ) {
	$wpdb->query("TRUNCATE TABLE {$table_name}");

	return 2;

	}

	$watchlists = $wpdb->get_results("SELECT * FROM {$table_name} ORDER BY id DESC");

	if( $watchlists ) {
	foreach($watchlists as $watchlist) {

	$pid = $watchlist->aid;
	$post = get_post($pid);
	if(empty($post)) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE aid = '%d'", $pid));
	}
}

}
	return 2;
}


/*
|--------------------------------------------------------------------------
| DELETE FAVOURITES
|--------------------------------------------------------------------------
*/

function cp_auction_delete_favourites($empty = '') {

	global $post, $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_favorites";

	if ( $empty == "empty" ) {
	$wpdb->query("TRUNCATE TABLE {$table_name}");

	return 2;

	}

	$favorites = $wpdb->get_results("SELECT * FROM {$table_name} ORDER BY id DESC");

	if( $favorites ) {
	foreach($favorites as $favorite) {

	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE aid = '%d'", $pid));

	}

}
	return 2;
}

/*
|--------------------------------------------------------------------------
| DELETE COUPONS
|--------------------------------------------------------------------------
*/

function cp_auction_delete_coupons($empty = '') {

	global $post, $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_coupons";

	if ( $empty == "empty" ) {
	$wpdb->query("TRUNCATE TABLE {$table_name}");

	return 2;

	}

	$coupons = $wpdb->get_results("SELECT * FROM {$table_name} ORDER BY coupon_id DESC");

	if( $coupons ) {
	foreach($coupons as $coupon) {

	$pid = $coupon->coupon_pid;
	if( $pid > 0 ) {
	$post = get_post($pid);
	if(empty($post)) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE coupon_pid = '%d'", $pid));
	}
	}
}

}
	return 2;
}


/*
|--------------------------------------------------------------------------
| DELETE UPLOADS
|--------------------------------------------------------------------------
*/

function cp_auction_delete_uploads($empty = '') {

	global $post, $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_uploads";

	if ( $empty == "empty" ) {
	$wpdb->query("TRUNCATE TABLE {$table_name}");

	return 2;

	}

	$uploads = $wpdb->get_results("SELECT * FROM {$table_name} ORDER BY id DESC");

	if( $uploads ) {
	foreach($uploads as $upload) {

	$pid = $upload->pid;
	if( $pid > 0 ) {
	$post = get_post($pid);
	if(empty($post)) {
	$fullpath = $upload->fullpath;
	if ( $fullpath )
	unlink( $fullpath );

	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE pid = '%d'", $pid));
	}
	}
}

}
	return 2;
}


/*
|--------------------------------------------------------------------------
| SOCIAL SHARING - FACEBOOK LIKES
|--------------------------------------------------------------------------
*/

if ( !function_exists('cp_fb_like') ) {
function cp_fb_like() {

		$layout = get_option("cp_fb_like_layout");
		if($layout == '') { $layout = 'standard'; }

		$action = get_option("cp_fb_like_action");
		if($action == '') { $layout = 'like'; }

		$font = get_option("cp_fb_like_font");
		if($font == '') { $font = 'arial'; }

		$colorscheme = get_option("cp_fb_like_colorscheme");
		if($colorscheme == '') { $colorscheme = 'light'; }

		$width = get_option("cp_fb_like_width");
		if($width == '') { $width = '450'; }

		$height = get_option("cp_fb_like_height");
		if($height == '') { $height = '100'; }

		$fbsend = get_option("cp_fb_like_send");
		if($fbsend == '') { $fbsend = 'false'; }

		$output = '<div id="cp_fb_like_button" style="height:'.$height.'px;"><fb:like href="'.get_permalink().'" send="'.$fbsend.'" layout="'.$layout.'" width="'.$width.'" font="'.$font.'" action="'.$action.'" colorscheme="'.$colorscheme.'" show_faces="false"></fb:like></div>';

		return $output;
}
}


/*
|--------------------------------------------------------------------------
| ADD IMAGES TO RSS FEEDS
|--------------------------------------------------------------------------
*/

function cp_auction_embed_rss_image($content) {

 global $post;

	if(is_feed()) {

 if(get_post_meta($post->ID, 'images', true)) $thumb_image = cp_single_image_legacy($post->ID, get_option('medium_size_w'), get_option('medium_size_h')); else $thumb_image = cp_get_image($post->ID, 'medium', 1);

	$content = $thumb_image . "<br />" . $content;

	}

	return $content;

	}

 add_filter('the_excerpt_rss', 'cp_auction_embed_rss_image');


/*
|--------------------------------------------------------------------------
| VALIDATE EMAIL ADDRESSES
|--------------------------------------------------------------------------
*/

function cp_auction_check_email_address($email) {
  // First, we check that there's one @ symbol, 
  // and that the lengths are right.
  if (!preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $email)) {
    // Email invalid because wrong number of characters 
    // in one section or wrong number of @ symbols.
    return false;
  }
  // Split it into sections to make life easier
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) {
    if
(!preg_match("@^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&
↪'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$@",
$local_array[$i])) {
      return false;
    }
  }
  // Check if domain is IP. If not, 
  // it should be valid domain name
  if (!preg_match("/^\[?[0-9\.]+\]?$/", $email_array[1])) {
    $domain_array = explode(".", $email_array[1]);
    if (sizeof($domain_array) < 2) {
        return false; // Not enough parts to domain
    }
    for ($i = 0; $i < sizeof($domain_array); $i++) {
      if
(!preg_match("/^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|
↪([A-Za-z0-9]+))$/",
$domain_array[$i])) {
        return false;
      }
    }
  }
  return true;
}


/*
|--------------------------------------------------------------------------
| WATCHLIST NOTIFICATIONS - BUY NOW
|--------------------------------------------------------------------------
*/

function cp_auction_watchlist_buy_notifications($pid, $bid) {

	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');
	$plain_html = get_option('cp_auction_plugin_plain_html');

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE aid = '$pid'");

	global $post;
	$post = get_post($pid);

	if( $results ) {
	foreach($results as $result) {
	
	$user_id = $result->uid;

	cp_auction_watchlist_buynow_notification( $pid, $post, $bid, $user_id );

	}
	}

}


/*
|--------------------------------------------------------------------------
| WATCHLIST NOTIFICATIONS - NEW BID
|--------------------------------------------------------------------------
*/

function cp_auction_watchlist_bid_notifications($pid, $bid, $my_type = '') {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE aid = '$pid'");

	global $post;
	$post = get_post($pid);

	if( $results ) {
	foreach($results as $result) {
	
	$user_id = $result->uid;

	if( $my_type == "wanted" ) {
	cp_auction_wanted_watchlist_notifications( $pid, $post, $bid, $user_id );
	} else {
	cp_auction_watchlist_email_notifications( $pid, $post, $bid, $user_id );
	}

	}
	}
}


/*
|--------------------------------------------------------------------------
| CHECK FAVORITE LIST
|--------------------------------------------------------------------------
*/

function cp_auction_check_favoritelist($uid, $sid) {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_favorites";
	$in_list = $wpdb->get_row("SELECT * FROM {$table_name} WHERE uid = '$uid' AND sid = '$sid'");

	if( $in_list ) return true;
	else
	return false;
}


/*
|--------------------------------------------------------------------------
| CHECK WATCHLIST
|--------------------------------------------------------------------------
*/

function cp_auction_check_watchlist($uid, $pid) {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$in_list = $wpdb->get_row("SELECT * FROM {$table_name} WHERE uid = '$uid' AND aid = '$pid'");

	if( $in_list ) return true;
	else
	return false;
}


/*
|--------------------------------------------------------------------------
| TRUNCATE POST CONTENT ON ADS
|--------------------------------------------------------------------------
*/

function truncate($str, $length = '', $trailing = '...'){
	$length-=mb_strlen($trailing);
	if (mb_strlen($str)> $length){
	return mb_substr($str,0,$length).$trailing;
	}else{
	$res = $str;
}
	return $res;
}


/*
|--------------------------------------------------------------------------
| FIND USER UPLOADED FILES
|--------------------------------------------------------------------------
*/

function cp_auction_find_file($dirname, $fname, &$file_path) {

  $dir = opendir($dirname);

  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        cp_auction_find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return $file_path;
        }
      }
    }
  }

} // find_file


/*
|--------------------------------------------------------------------------
| FIND ALL ADS BASED ON POST TYPE AND AUTHOR - DROPDOWN
|--------------------------------------------------------------------------
*/

function get_ad_listing_dropdown($uid = '', $select = '') {

	global $wpdb, $cp_options;
	$new_class = false;
	$sel = false;
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$table_name = $wpdb->prefix . "posts";
	if( !$uid )
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_status = 'publish' AND post_type = '".APP_POST_TYPE."'");
	else
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_author = '$uid' AND post_status = 'publish' AND post_type = '".APP_POST_TYPE."'");

   	if( $results ) {
	echo "<select class='".$new_class."' name = 'page_id' id = 'page_id'>";
      	echo "<option value=''>".__('Select Item','auctionPlugin')."</option>";
      	if( $uid ) {
      	if( $select == '0' )
      	$sel = 'selected="selected"';
      	echo "<option $sel value='0'>".__('All Items','auctionPlugin')."</option>";
	}
	foreach($results as $res) {
      	if( $select == $res->ID ) {
      	$sel = 'selected="selected"';
      	echo "<option $sel value='$res->ID'>$res->post_title</option>";
	} else {
	echo "<option value='$res->ID'>$res->post_title</option>";
	}
	}
	echo "</select>";
	}
}


/*
|--------------------------------------------------------------------------
| FIND ALL ADS ALLOWED FOR COUPONS
|--------------------------------------------------------------------------
*/

function get_ad_coupons_dropdown($uid = '', $select = '') {

	global $wpdb, $cp_options;
	$new_class = false;
	$sel = false;
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$table_name = $wpdb->prefix . "posts";
	if( !$uid )
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_status = 'publish' AND post_type = '".APP_POST_TYPE."'");
	else
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_author = '$uid' AND post_status = 'publish' AND post_type = '".APP_POST_TYPE."'");

   	if( $results ) {
	echo "<select class='".$new_class."' name = 'ads_id' id = 'ads_id'>";
      	echo "<option value=''>".__('Select Item','auctionPlugin')."</option>";
      	if( $uid ) {
      	if( $select == '0' )
      	$sel = 'selected="selected"';
      	echo "<option $sel value='0'>".__('All Items','auctionPlugin')."</option>";
	}
	foreach($results as $res) {
      	if( $select == $res->ID ) {
      	$sel = 'selected="selected"';
      	echo "<option $sel value='$res->ID'>$res->post_title</option>";
	} else {
	echo "<option value='$res->ID'>$res->post_title</option>";
	}
	}
	echo "</select>";
	}
}


/*
|--------------------------------------------------------------------------
| FIND ALL ADS AND PURCHASE OPTIONS - BACKEND DROPDOWN
|--------------------------------------------------------------------------
*/

function get_add_purchase_dropdown($select = '') {

	global $wpdb, $cp_options;
	$new_class = false;
	$sel = false;
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$table_name = $wpdb->prefix . "posts";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_status = 'publish' AND post_type = '".APP_POST_TYPE."'");

   	if( $results ) {
	echo "<select class='".$new_class."' name = 'page_id' id = 'page_id'>";
      	echo "<option value=''>".__('Select Item','auctionPlugin')."</option>";
      	echo "<option value='Verification'>".__('Verification','auctionPlugin')."</option>";

	foreach($results as $res) {
      	if($select == $res->ID) {
      	$sel = 'selected="selected"';
      	echo "<option $sel value='$res->ID'>$res->post_title</option>";
	} else {
	echo "<option value='$res->ID'>$res->post_title</option>";
	}
	}
	echo "</select>";
	}
}


/*
|--------------------------------------------------------------------------
| DISPLAY SHOPPING CART INFO IN THE HEADER - FLATRON CHILD THEME
|--------------------------------------------------------------------------
*/

if (!function_exists('cp_login_head') && get_option('stylesheet') == "flatron" ) {
	function cp_login_head() {

	if (is_user_logged_in()) :
	global $current_user, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$display_user_name = cp_get_user_name();
	$logout_url = cp_logout_url();

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$get_items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND unpaid > '0' AND paid != '2'");
	$items = $wpdb->num_rows;

	if (class_exists("cartpaujPM")) {
	$table_messages = $wpdb->prefix . "cartpauj_pm_messages";
	$get_pms = $wpdb->get_results($wpdb->prepare("SELECT id FROM {$table_messages} WHERE (to_user = %d AND parent_id = 0 AND to_del <> 1 AND message_read = 0 AND last_sender <> %d) OR (from_user = %d AND parent_id = 0 AND from_del <> 1 AND message_read = 0 AND last_sender <> %d)", $uid, $uid, $uid, $uid));
	$pms = $wpdb->num_rows;
	}
			?>
		<div class="fl-logins">
		<?php _e( 'Welcome,', 'auctionPlugin' ); ?> <strong><?php echo $display_user_name; ?></strong> [ <a href="<?php echo CP_DASHBOARD_URL; ?>"><?php _e( 'My Dashboard', 'auctionPlugin' ); ?></a> <?php if (class_exists("cartpaujPM")) { ?> | <a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'to=1'); ?>"><?php _e('PMs', 'auctionPlugin'); ?> (<?php echo $pms; ?>)</a><?php } ?> <span class="no-cart">| <a href="<?php echo cp_auction_url($cpurl['cart'], '?shopping_cart=1'); ?>"><?php _e('Cart', 'auctionPlugin'); ?> (<?php echo $items; ?>)</a> </span>| <a href="<?php echo $logout_url; ?>"><?php _e( 'Log out', 'auctionPlugin' ); ?></a> ]&nbsp;
		</div>
		<?php else : ?>
		<div class="fl-logout">
		<?php _e( 'Welcome,', 'auctionPlugin' ); ?> <strong><?php _e( 'visitor!', 'auctionPlugin' ); ?></strong> [ <a href="<?php echo appthemes_get_registration_url(); ?>"><?php _e( 'Register', 'auctionPlugin' ); ?></a> | <a href="<?php echo wp_login_url(); ?>"><?php _e( 'Login', 'auctionPlugin' ); ?></a> ]&nbsp;
		</div>
		<?php endif;

	}
}


/*
|--------------------------------------------------------------------------
| DISPLAY SHOPPING CART INFO IN THE HEADER - DISABLED FOR HEADLINE
|--------------------------------------------------------------------------
*/

if ( ! function_exists( 'cp_login_head' ) && headline_child_themes( get_option('stylesheet') ) == false && get_option('stylesheet') != "flannel" ) {
	function cp_login_head() {

	if (is_user_logged_in()) :
	global $current_user, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$display_user_name = cp_get_user_name();
	$logout_url = cp_logout_url();

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$get_items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND unpaid > '0' AND paid != '2'");
	$items = $wpdb->num_rows;

	if (class_exists("cartpaujPM")) {
	$table_messages = $wpdb->prefix . "cartpauj_pm_messages";
	$get_pms = $wpdb->get_results($wpdb->prepare("SELECT id FROM {$table_messages} WHERE (to_user = %d AND parent_id = 0 AND to_del <> 1 AND message_read = 0 AND last_sender <> %d) OR (from_user = %d AND parent_id = 0 AND from_del <> 1 AND message_read = 0 AND last_sender <> %d)", $uid, $uid, $uid, $uid));
	$pms = $wpdb->num_rows;
	}
			?>
		<?php _e( 'Xin chào,', 'auctionPlugin' ); ?> <strong><?php echo $display_user_name; ?></strong> [ <a href="<?php echo CP_DASHBOARD_URL; ?>"><?php _e( 'Bảng điều khiển', 'auctionPlugin' ); ?></a> <?php if (class_exists("cartpaujPM")) { ?> | <a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'to=1'); ?>"><?php _e('PMs', 'auctionPlugin'); ?> (<?php echo $pms; ?>)</a><?php } ?> | <a href="<?php echo $logout_url; ?>"><?php _e( 'Đăng xuất', 'auctionPlugin' ); ?></a> ]&nbsp;
		<?php else : ?>
		<?php _e( 'Xin chào,', 'auctionPlugin' ); ?> <strong><?php _e( 'Khách!', 'auctionPlugin' ); ?></strong> [ <a href="<?php echo appthemes_get_registration_url(); ?>"><?php _e( 'Đăng ký', 'auctionPlugin' ); ?></a> | <a href="<?php echo wp_login_url(); ?>"><?php _e( 'Đăng nhập', 'auctionPlugin' ); ?></a> ]&nbsp;
		<?php endif;

	}
}


/*
|--------------------------------------------------------------------------
| DISPLAY SHOPPING CART INFO IN THE HEADER - FLATPRESS
|--------------------------------------------------------------------------
*/

if (!function_exists('fl_login_head') && get_option('stylesheet') == "flatpress" ) {
	function fl_login_head() {

	if (is_user_logged_in()) :
	global $current_user, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$display_user_name = cp_get_user_name();
	$logout_url = cp_logout_url();

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$get_items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND unpaid > '0' AND paid != '2'");
	$items = $wpdb->num_rows;

	if (class_exists("cartpaujPM")) {
	$table_messages = $wpdb->prefix . "cartpauj_pm_messages";
	$get_pms = $wpdb->get_results($wpdb->prepare("SELECT id FROM {$table_messages} WHERE (to_user = %d AND parent_id = 0 AND to_del <> 1 AND message_read = 0 AND last_sender <> %d) OR (from_user = %d AND parent_id = 0 AND from_del <> 1 AND message_read = 0 AND last_sender <> %d)", $uid, $uid, $uid, $uid));
	$pms = $wpdb->num_rows;
	}
			?>
		<a href="<?php echo CP_DASHBOARD_URL; ?>"><?php _e( 'My Dashboard', 'auctionPlugin' ); ?></a> <?php if (class_exists("cartpaujPM")) { ?> | <a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'to=1'); ?>"><?php _e('PMs', 'auctionPlugin'); ?> (<?php echo $pms; ?>)</a><?php } ?> <span class="no-cart">| <a href="<?php echo cp_auction_url($cpurl['cart'], '?shopping_cart=1'); ?>"><?php _e('Cart', 'auctionPlugin'); ?> (<?php echo $items; ?>)</a> </span>| <a href="<?php echo $logout_url; ?>"><?php _e( 'Log out', 'auctionPlugin' ); ?></a> ]&nbsp;
		<?php else : ?>
		<a href="<?php echo appthemes_get_registration_url(); ?>"><?php _e( 'Register', 'auctionPlugin' ); ?></a> | <a href="<?php echo wp_login_url(); ?>"><?php _e( 'Login', 'auctionPlugin' ); ?></a> ]&nbsp;
		<?php endif;

	}
}


/*
|--------------------------------------------------------------------------
| DISPLAY SHOPPING CART INFO IN THE HEADER FOR MYDENTITY & RONDELL
|--------------------------------------------------------------------------
*/

if ( ( ! function_exists( 'cp_login_head_custom' ) ) && ( get_option('stylesheet') == "redrondell" || get_option('stylesheet') == "rondell_370" || get_option('stylesheet') == "rondell" ) ) {
function cp_login_head_custom() {

	if (is_user_logged_in()) :
	global $current_user, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$display_user_name = cp_get_user_name();
	$logout_url = cp_logout_url();

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$get_items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND unpaid > '0' AND paid != '2'");
	$items = $wpdb->num_rows;

	if (class_exists("cartpaujPM")) {
	$table_messages = $wpdb->prefix . "cartpauj_pm_messages";
	$get_pms = $wpdb->get_results($wpdb->prepare("SELECT id FROM {$table_messages} WHERE (to_user = %d AND parent_id = 0 AND to_del <> 1 AND message_read = 0 AND last_sender <> %d) OR (from_user = %d AND parent_id = 0 AND from_del <> 1 AND message_read = 0 AND last_sender <> %d)", $uid, $uid, $uid, $uid));
	$pms = $wpdb->num_rows;
	}
			?>
	<div class="login-header"><span><?php _e( 'Welcome,', 'auctionPlugin' ); ?> <strong><?php echo $display_user_name; ?></strong></span> <span>[ <a href="<?php echo CP_DASHBOARD_URL; ?>"><?php _e( 'My Dashboard', 'auctionPlugin' ); ?></a> <?php if (class_exists("cartpaujPM")) { ?> | <a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'to=1'); ?>"><?php _e('PMs', 'auctionPlugin'); ?> (<?php echo $pms; ?>)</a><?php } ?> <span class="no-cart">| <a href="<?php echo cp_auction_url($cpurl['cart'], '?shopping_cart=1'); ?>"><?php _e('Cart', 'auctionPlugin'); ?> (<?php echo $items; ?>)</a> </span>| <a href="<?php echo $logout_url; ?>"><?php _e( 'Log out', 'auctionPlugin' ); ?></a> ] &nbsp; <span></div>
		<?php else : ?>
	<div class="login-header"><span><?php _e( 'Welcome,', 'auctionPlugin' ); ?> <strong><?php _e( 'visitor!', 'auctionPlugin' ); ?></strong></span> <span>[ <a href="<?php echo appthemes_get_registration_url(); ?>"><?php _e( 'Register', 'auctionPlugin' ); ?></a> | <a href="<?php echo wp_login_url(); ?>"><?php _e( 'Login', 'auctionPlugin' ); ?></a> ] &nbsp; </span></div>
		<?php endif;
	}
}


/*
|--------------------------------------------------------------------------
| SEPARATE CART LINK FROM LOGIN HEADER
|--------------------------------------------------------------------------
*/

function cp_auction_cart_link() {

	if (is_user_logged_in()) :
	global $current_user, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$get_items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND unpaid > '0' AND paid != '2'");
	$items = $wpdb->num_rows;
?>
	<div class="separate-cart"><a href="<?php echo cp_auction_url($cpurl['cart'], '?shopping_cart=1'); ?>"><?php _e('Cart', 'auctionPlugin'); ?> (<?php echo $items; ?>)</a></div>

	<?php endif;
}
add_shortcode('cp-cart-link', 'cp_auction_cart_link');


/*
|--------------------------------------------------------------------------
| SEPARATE CART LINK FROM LOGIN HEADER AS BUTTON
|--------------------------------------------------------------------------
*/

function cp_auction_cart_button() {

	if (is_user_logged_in()) :
	global $current_user, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$get_items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND unpaid > '0' AND paid != '2'");
	$items = $wpdb->num_rows;
?>
	<a href="<?php echo cp_auction_url($cpurl['cart'], '?shopping_cart=1'); ?>" class="btn-cart obtn btn_orange"><?php _e('Cart', 'auctionPlugin'); ?> (<?php echo $items; ?>)</a>

	<?php endif;
}
add_shortcode('cp-cart-button', 'cp_auction_cart_button');


/*
|--------------------------------------------------------------------------
| SEPARATE CARTPAUJ PM LINK FROM LOGIN HEADER
|--------------------------------------------------------------------------
*/

function cp_auction_pm_link() {

	if (is_user_logged_in()) :
	global $current_user, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	if (class_exists("cartpaujPM")) {
	$table_messages = $wpdb->prefix . "cartpauj_pm_messages";
	$get_pms = $wpdb->get_results($wpdb->prepare("SELECT id FROM {$table_messages} WHERE (to_user = %d AND parent_id = 0 AND to_del <> 1 AND message_read = 0 AND last_sender <> %d) OR (from_user = %d AND parent_id = 0 AND from_del <> 1 AND message_read = 0 AND last_sender <> %d)", $uid, $uid, $uid, $uid));
	$pms = $wpdb->num_rows;

?>
	<div class="separate-cartpauj"><a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'to=1'); ?>"><?php _e('PMs', 'auctionPlugin'); ?> (<?php echo $pms; ?>)</a></div>
	<?php } endif;
}
add_shortcode('cp-pm-link', 'cp_auction_pm_link');


/*
|--------------------------------------------------------------------------
| SEPARATE CARTPAUJ PM LINK FROM LOGIN HEADER AS BUTTON
|--------------------------------------------------------------------------
*/

function cp_auction_pm_button() {

	if (is_user_logged_in()) :
	global $current_user, $cpurl, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	if (class_exists("cartpaujPM")) {
	$table_messages = $wpdb->prefix . "cartpauj_pm_messages";
	$get_pms = $wpdb->get_results($wpdb->prepare("SELECT id FROM {$table_messages} WHERE (to_user = %d AND parent_id = 0 AND to_del <> 1 AND message_read = 0 AND last_sender <> %d) OR (from_user = %d AND parent_id = 0 AND from_del <> 1 AND message_read = 0 AND last_sender <> %d)", $uid, $uid, $uid, $uid));
	$pms = $wpdb->num_rows;

?>
	<a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'to=1'); ?>" class="btn-cartpauj obtn btn_orange"><?php _e('PMs', 'auctionPlugin'); ?> (<?php echo $pms; ?>)</a>
	<?php } endif;
}
add_shortcode('cp-pm-button', 'cp_auction_pm_button');


/*
|--------------------------------------------------------------------------
| AUCTIONS ON CLASSIFIED ADS
|--------------------------------------------------------------------------
*/

function cp_auction_on_classified_ads( $post_id ) {

	global $post;
	$post = get_post($post_id);

	$enable = get_option('cp_auction_enable_classified_auction');
	$user_enabled = get_post_meta($post->ID, 'cp_auction', true);
	$price = get_post_meta($post->ID, 'cp_price', true);
	$ad_length = get_option('cp_auction_classified_expiration');

	if( $enable == "yes" && $user_enabled == "Yes" ) {

	$todayDate = current_time('mysql'); // current date
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$todayDate + $ad_length days"), true);
	$cp_end_date = strtotime( $cp_sys_expire_date );
	$cp_buy_now = get_post_meta($post->ID, 'cp_price', true);
	update_post_meta($post->ID, 'cp_start_price', cp_auction_sanitize_amount($price));
	update_post_meta($post->ID, 'cp_sys_expire_date', $cp_sys_expire_date);
	update_post_meta($post->ID, 'cp_end_date', $cp_end_date);
	update_post_meta($post->ID, 'cp_auction_my_auction_type', 'normal');
	}
}
add_action( 'publish_ad_listing', 'cp_auction_on_classified_ads' );


/*
|--------------------------------------------------------------------------
| SIDEBAR WIDGET THUMBNAILS
|--------------------------------------------------------------------------
*/

if ( !function_exists('cp_auction_widget_thumbnail') ) :
	function cp_auction_widget_thumbnail() {
		global $post, $cp_options;

		// go see if any images are associated with the ad
		$image_id = cp_get_featured_image_id($post->ID);

		// set the class based on if the hover preview option is set to "yes"
		$prevclass = ( $cp_options->ad_image_preview ) ? 'preview' : 'nopreview';

		if ( $image_id > 0 ) {

			// get 75x75 v3.0.5+ image size
			$adthumbarray = wp_get_attachment_image( $image_id, array(50,50) );

			// grab the large image for onhover preview
			$adlargearray = wp_get_attachment_image_src( $image_id, 'large' );
			$img_large_url_raw = $adlargearray[0];

			// must be a v3.0.5+ created ad
			if ( $adthumbarray ) {
				echo '<a href="'. get_permalink() .'" title="'. the_title_attribute('echo=0') .'" class="'.$prevclass.'" data-rel="'.$img_large_url_raw.'">'.$adthumbarray.'</a>';

			// maybe a v3.0 legacy ad
			} else {
				$adthumblegarray = wp_get_attachment_image_src($image_id, 'thumbnail');
				$img_thumbleg_url_raw = $adthumblegarray[0];
				echo '<a href="'. get_permalink() .'" title="'. the_title_attribute('echo=0') .'" class="'.$prevclass.'" data-rel="'.$img_large_url_raw.'">'.$adthumblegarray.'</a>';
			}

		// no image so return the placeholder thumbnail
		} else {
			echo '<a href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '"><img class="attachment-sidebar-thumbnail" alt="" title="" src="' . appthemes_locate_template_uri('images/no-thumb-sm.jpg') . '" /></a>';
		}

	}
endif;


/*
|--------------------------------------------------------------------------
| THUMBNAILS FOR SOLD AND PURCHASED LISTINGS IN DASHBOARD
|--------------------------------------------------------------------------
*/

if ( !function_exists('cp_auction_dashboard_thumbnail') ) :
	function cp_auction_dashboard_thumbnail() {
		global $post, $cp_options;

		// go see if any images are associated with the ad
		$image_id = cp_get_featured_image_id($post->ID);

		// set the class based on if the hover preview option is set to "yes"
		$prevclass = ( $cp_options->ad_image_preview ) ? 'preview' : 'nopreview';

		if ( $image_id > 0 ) {

			// get 80x80 v3.0.5+ image size
			$adthumbarray = wp_get_attachment_image( $image_id, array(80,80) );

			// grab the large image for onhover preview
			$adlargearray = wp_get_attachment_image_src( $image_id, 'large' );
			$img_large_url_raw = $adlargearray[0];

			// must be a v3.0.5+ created ad
			if ( $adthumbarray ) {
				echo '<a href="'. get_permalink() .'" title="'. the_title_attribute('echo=0') .'" class="'.$prevclass.'" data-rel="'.$img_large_url_raw.'">'.$adthumbarray.'</a>';

			// maybe a v3.0 legacy ad
			} else {
				$adthumblegarray = wp_get_attachment_image_src($image_id, 'thumbnail');
				$img_thumbleg_url_raw = $adthumblegarray[0];
				echo '<a href="'. get_permalink() .'" title="'. the_title_attribute('echo=0') .'" class="'.$prevclass.'" data-rel="'.$img_large_url_raw.'">'.$adthumblegarray.'</a>';
			}

		// no image so return the placeholder thumbnail
		} else {
			echo '<a href="' . get_permalink() . '" title="' . the_title_attribute('echo=0') . '"><img class="attachment-sidebar-thumbnail" alt="" title="" src="' . appthemes_locate_template_uri('images/no-thumb-sm.jpg') . '" /></a>';
		}

	}
endif;


/*
|--------------------------------------------------------------------------
| ADD AD COUNTER TO CLASSIFIEDS IN AD LISTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_count_classifieds($status) {
	global $wpdb;

	if ( empty( $status ) || $status == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts AS wposts
  		LEFT JOIN $wpdb->postmeta AS postmeta
    		ON wposts.ID = postmeta.post_id
    		AND postmeta.meta_key = 'cp_auction_my_auction_type'
		WHERE (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."' 
  		AND (postmeta.post_id IS NULL OR postmeta.meta_key = 'cp_auction_my_auction_type' AND postmeta.meta_value = 'classified')";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts AS wposts
  		LEFT JOIN $wpdb->postmeta AS postmeta
    		ON wposts.ID = postmeta.post_id
    		AND postmeta.meta_key = 'cp_auction_my_auction_type'
		WHERE wposts.post_status = '$status'
		AND wposts.post_type = '".APP_POST_TYPE."' 
  		AND (postmeta.post_id IS NULL OR postmeta.meta_key = 'cp_auction_my_auction_type' AND postmeta.meta_value = 'classified')";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	}
}


/*
|--------------------------------------------------------------------------
| ADD AD COUNTER TO WANTED ADS IN AD LISTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_count_wanted($status) {
	global $wpdb;

	if ( empty( $status ) || $status == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'wanted')
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'wanted')
		AND wposts.post_status = '$status'
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	}
}


/*
|--------------------------------------------------------------------------
| ADD AD COUNTER TO AUCTIONS IN AD LISTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_count_auctions($status) {
	global $wpdb;

	if ( empty( $status ) || $status == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'normal' 
		OR wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'reverse')
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'normal' 
		OR wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'reverse')
		AND wposts.post_status = '$status'
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	}
}


/*
|--------------------------------------------------------------------------
| ADD AD COUNTER TO CLASSIFIEDS IN MY LISTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_count_my_classifieds($status, $uid) {
	global $wpdb;

	if ( empty( $status ) || $status == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts AS wposts
  		LEFT JOIN $wpdb->postmeta AS postmeta
    		ON wposts.ID = postmeta.post_id
    		AND postmeta.meta_key = 'cp_auction_my_auction_type'
		WHERE wposts.post_author = '$uid' 
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."' 
  		AND (postmeta.post_id IS NULL OR postmeta.meta_key = 'cp_auction_my_auction_type' AND postmeta.meta_value = 'classified')";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts AS wposts
  		LEFT JOIN $wpdb->postmeta AS postmeta
    		ON wposts.ID = postmeta.post_id
    		AND postmeta.meta_key = 'cp_auction_my_auction_type'
		WHERE wposts.post_author = '$uid' 
		AND wposts.post_status = '$status'
		AND wposts.post_type = '".APP_POST_TYPE."' 
  		AND (postmeta.post_id IS NULL OR postmeta.meta_key = 'cp_auction_my_auction_type' AND postmeta.meta_value = 'classified')";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	}
}


/*
|--------------------------------------------------------------------------
| ADD AD COUNTER TO WANTED ADS IN MY LISTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_count_my_wanted($status, $uid) {
	global $wpdb;

	if ( empty( $status ) || $status == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'wanted')
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'wanted')
		AND wposts.post_status = '$status'
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	}
}


/*
|--------------------------------------------------------------------------
| ADD AD COUNTER TO AUCTIONS IN MY LISTINGS
|--------------------------------------------------------------------------
*/

function cp_auction_count_my_auctions($status, $uid) {
	global $wpdb;

	if ( empty( $status ) || $status == "all" ) {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'normal' 
		OR wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'reverse')
		AND (wposts.post_status = 'publish' OR wposts.post_status = 'pending' OR wposts.post_status = 'draft')
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	} else {
	$sql = "SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta
		WHERE wposts.post_author = '$uid'
		AND wposts.ID = wpostmeta.post_id
		AND (wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'normal' 
		OR wpostmeta.meta_key = 'cp_auction_my_auction_type' AND wpostmeta.meta_value = 'reverse')
		AND wposts.post_status = '$status'
		AND wposts.post_type = '".APP_POST_TYPE."'";

	$res = $wpdb->get_results($sql);
	return $wpdb->num_rows;
	}
}


/*
|--------------------------------------------------------------------------
| SET TITLES FOR USERS RIBBONS - Buy,Swap,Give Away
|--------------------------------------------------------------------------
*/

function cp_auction_users_ribbon($post_id, $fname) {

	global $wpdb;
	if( $post_id > 0 )
	$wantto = get_post_meta( $post_id, $fname, true );
	else $wantto = false;
	$table_name = $wpdb->prefix . "cp_auction_plugin_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = '$fname'");

	if( $wantto && $row ) {
	$field = explode(",",$row->field_values);
	$trans = explode(",",$row->trans_values);

	if( $fname == "cp_iwant_to" ) {

	if( isset( $field[0] ) && $wantto == $field[0] || isset( $trans[0] ) && $wantto == $trans[0] ) return ''.__('For Sale', 'auctionPlugin').'';
	elseif( isset( $field[1] ) && $wantto == $field[1] || isset( $trans[1] ) && $wantto == $trans[1] ) return ''.__('Want to Buy', 'auctionPlugin').'';
	elseif( isset( $field[2] ) && $wantto == $field[2] || isset( $trans[2] ) && $wantto == $trans[2] ) return ''.__('Want to Swap', 'auctionPlugin').'';
	elseif( isset( $field[3] ) && $wantto == $field[3] || isset( $trans[3] ) && $wantto == $trans[3] ) return ''.__('Give Away', 'auctionPlugin').'';
	else return false;

	} elseif( $fname == "cp_want_to" ) {
	
	if( isset( $field[0] ) && $wantto == $field[0] || isset( $trans[0] ) && $wantto == $trans[0] ) return ''.__('Want to Buy', 'auctionPlugin').'';
	elseif( isset( $field[1] ) && $wantto == $field[1] || isset( $trans[1] ) && $wantto == $trans[1] ) return ''.__('Want to Swap', 'auctionPlugin').'';
	elseif( isset( $field[2] ) && $wantto == $field[2] || isset( $trans[2] ) && $wantto == $trans[2] ) return ''.__('Give Away', 'auctionPlugin').'';
	else return false;

	}
	}
	else {
		return false;
	}

}


/*
|--------------------------------------------------------------------------
| GET THE PAGE SLUG POST_NAME BY ID
|--------------------------------------------------------------------------
*/

function cp_get_the_slug( $pid ) {

	$data = get_post( $pid );
	$slug = $data->post_name;
	return $slug; 

}