<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Do the necessary changes to the classipress/includes/views.php
function cp_auction_override_views() {

	$TemplateFileSourceURL1 = CP_AUCTION_PLUGIN_DIR . 'library/copyus/views.php';

	$TemplateFileTargetURL1 = ABSPATH . 'wp-content/themes/classipress/includes/views.php';

	if ( !file_exists( $TemplateFileSourceURL1 ) ) {
        return FALSE;
	}

	$GetTemplate1 = file_get_contents( $TemplateFileSourceURL1 );

	if ( !$GetTemplate1 ) {
	return FALSE;
	}

	$WriteTemplate1 = file_put_contents( $TemplateFileTargetURL1, $GetTemplate1 );

	if ( !$WriteTemplate1 ) {
	return FALSE;
	}

	return TRUE;

}

// Do the necessary changes to the themes/classipress/includes/payments.php
function cp_auction_override_payments() {
	global $siteinfo;

	if ( get_option( 'cp_version' ) < '3.4' )
	$TemplateFileSourceURL2 = CP_AUCTION_PLUGIN_DIR . 'library/copyus/payments.php';
	else $TemplateFileSourceURL2 = CP_AUCTION_PLUGIN_DIR . 'library/copyus/payments34.php';

	$TemplateFileTargetURL2 = ABSPATH . 'wp-content/themes/classipress/includes/payments.php';

	if ( !file_exists( $TemplateFileSourceURL2 ) ) {
        return FALSE;
	}

	$GetTemplate2 = file_get_contents( $TemplateFileSourceURL2 );

	if ( !$GetTemplate2 ) {
	return FALSE;
	}

	$WriteTemplate2 = file_put_contents( $TemplateFileTargetURL2, $GetTemplate2 );

	if ( !$WriteTemplate2 ) {
	return FALSE;
	}

	return TRUE;

}

// Do the necessary changes to the classipress/tpl-registration.php
function cp_auction_override_registration() {
	global $wp_version;

	if ( $wp_version < 4.3 )
		return;

	$TemplateFileSourceURL1 = CP_AUCTION_PLUGIN_DIR . 'library/copyus/tpl-registration.php';

	$TemplateFileTargetURL1 = ABSPATH . 'wp-content/themes/classipress/tpl-registration.php';

	if ( !file_exists( $TemplateFileSourceURL1 ) ) {
        return FALSE;
	}

	$GetTemplate1 = file_get_contents( $TemplateFileSourceURL1 );

	if ( !$GetTemplate1 ) {
	return FALSE;
	}

	$WriteTemplate1 = file_put_contents( $TemplateFileTargetURL1, $GetTemplate1 );

	if ( !$WriteTemplate1 ) {
	return FALSE;
	}

	return TRUE;

}