<?php
/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| FILTER CATEGORIES BY AD TYPE
|--------------------------------------------------------------------------
*/

if ( ! function_exists( 'cp_dropdown_categories_prices' ) ) {
	function cp_dropdown_categories_prices( $args = '' ) {

	global $cp_adtype;

	if ( isset( $_GET['type'] ) ) $cp_adtype = $_GET['type'];
	else if ( isset( $_REQUEST['cp_auction_my_auction_type'] ) )
	$cp_adtype = $_REQUEST['cp_auction_my_auction_type'];

	if( empty( $cp_adtype ) ) $cp_adtype = "classified";

	$option = get_option('cp_auction_'.$cp_adtype.'_cats');

		$defaults = array(
			'show_option_all' => '',
			'show_option_none' => __( 'Select one', 'auctionPlugin' ),
			'orderby' => 'name',
			'order' => 'ASC',
			'show_last_update' => 0,
			'show_count' => 0,
			'hide_empty' => 0,
			'child_of' => 0,
			'exclude' => '',
			'include' => $option,
			'echo' => 1,
			'selected' => 0,
			'hierarchical' => 1,
			'name' => 'cat',
			'id' => 'ad_cat_id',
			'class' => 'dropdownlist',
			'depth' => 1,
			'tab_index' => 0,
			'taxonomy' => APP_TAX_CAT,
		);

		$defaults['selected'] = ( is_category() ) ? get_query_var( 'cat' ) : 0;
		$r = wp_parse_args( $args, $defaults );
		$r['include_last_update_time'] = $r['show_last_update'];
		extract( $r );

		$tab_index_attribute = '';
		if ( (int) $tab_index > 0 ) {
			$tab_index_attribute = " tabindex=\"$tab_index\"";
		}

		// TODO: remove dirty fix, consider to use 2 parames array: one for
		// get_categories() another for cp_category_dropdown_tree()
		unset( $r['name'] );
		$categories = get_categories( $r );
		$name = esc_attr( $name );
		$r['name'] = $name;
		$class = esc_attr( $class );
		$id = $id ? esc_attr( $id ) : $name;

		$output = '';
		if ( ! empty( $categories ) ) {
			$output = "<select name='$name' id='$id' class='$class' $tab_index_attribute>\n";

			if ( $show_option_all ) {
				$show_option_all = apply_filters( 'list_cats', $show_option_all );
				$selected = ( '0' === strval( $r['selected'] ) ) ? " selected='selected'" : '';
				$output .= "\t<option value='0'$selected>$show_option_all</option>\n";
			}

			if ( $show_option_none ) {
				$show_option_none = apply_filters( 'list_cats', $show_option_none );
				$selected = ( '-1' === strval( $r['selected'] ) ) ? " selected='selected'" : '';
				$output .= "\t<option value='-1'$selected>$show_option_none</option>\n";
			}

			if ( $hierarchical ) {
				$depth = $r['depth']; // Walk the full depth.
			} else {
				$depth = -1; // Flat.
			}

			$output .= cp_category_dropdown_tree( $categories, $depth, $r );
			$output .= "</select>\n";
		}

		$output = apply_filters( 'wp_dropdown_cats', $output );

		if ( $echo ) {
			echo $output;
		} else {
			return $output;
		}
	}
}


/*
|--------------------------------------------------------------------------
| SELECT WHICH FORM FIELDS TO DISPLAY IN FORM BASED ON AD TYPE
|--------------------------------------------------------------------------
*/

function cp_auction_display_ad_form_fields( $result ) {
	global $cp_adtype;

	$cp_adtype = false; if ( isset( $_GET['type'] ) ) $cp_adtype = $_GET['type'];
	else if ( isset( $_REQUEST['cp_auction_my_auction_type'] ) )
	$cp_adtype = $_REQUEST['cp_auction_my_auction_type'];

	$pid = false; if ( isset( $_GET['aid'] ) ) $pid = $_GET['aid'];

	if( empty( $cp_adtype ) ) $cp_adtype = "classified";

	$option = get_option('cp_auction_fields');
	$approves = get_option('cp_auction_plugin_approves');
	$cla_approves = get_option('cp_auction_plugin_cla_approves');
	$wnt_approves = get_option('cp_auction_plugin_wnt_approves');
	$upload = get_option('cp_auction_force_upload');
	$mrp_upload = get_option('cp_auction_mrp_force_upload');
	$wantto = get_option('cp_auction_want_to_defaults');

	if ( $pid > 0 )
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );

if ( !$result )
	return false;

// Hooks into cp_formbuilder_field and cp_formbuilder_review_field to disable
// fields not in use for the actual ad type.

	$fname = $result->field_name;
	$flist = explode(',', $option[$fname]);
if ( $cp_adtype == "classified" && !in_array("classified", $flist) ) return false;
if ( $cp_adtype == "wanted" && !in_array("wanted", $flist) ) return false;
if ( $cp_adtype == "normal" && !in_array("normal", $flist) ) return false;
if ( $cp_adtype == "reverse" && !in_array("reverse", $flist) ) return false;

if ( ( $cp_adtype == "classified" && $result->field_name == "cp_pausead" ) && ( $cla_approves == "yes" || $mrp_upload == "yes" ) )
	return false;

if ( $cp_adtype == "wanted" && $result->field_name == "cp_pausead" && $wnt_approves == "yes" )
	return false;

if ( ( $cp_adtype == "normal" || $cp_adtype == "reverse" ) && ( $result->field_name == "cp_pausead" ) && ( $approves == "yes" || $upload == "yes" ) )
	return false;

if ( ( isset( $_GET['aid'] ) > 0 ) && ( $my_type == "normal" || $my_type == "reverse" ) && ( $cp_adtype == "normal" || $cp_adtype == "reverse" ) && ( $result->field_name == "cp_auction_listing_duration" ) )
	return false;

if ( $cp_adtype == "classified" && get_option('cp_auction_buynow_on_classifieds') == "yes" && $result->field_name == "cp_buy_now_enabled" )
	return false;

if ( $cp_adtype == "normal" && get_option('cp_auction_buynow_on_auctions') == "yes" && $result->field_name == "cp_buy_now_enabled" )
	return false;

if ( $cp_adtype == "wanted" && ! empty( $wantto ) && $result->field_name == "cp_want_to" )
	return false;

if ( $cp_adtype == "wanted" && get_option('cp_auction_allow_deals_auto') == "yes" && $result->field_name == "cp_allow_deals" )
	return false;

if ( get_option('aws_bo_negotiable_price') == "yes" && $result->field_name == "cp_bestoffer" )
	return false;

else

	return $result;
}
add_filter( 'cp_formbuilder_field', 'cp_auction_display_ad_form_fields' );
add_filter( 'cp_formbuilder_review_field', 'cp_auction_display_ad_form_fields' );


/*
|--------------------------------------------------------------------------
| SELECT WHICH FORM FIELDS TO DISPLAY IN AD DETAILS ON AD TYPE
|--------------------------------------------------------------------------
*/

function cp_auction_display_ad_details_fields( $result ) {
	global $post;

	if ( !$result )
		return false;

	$option_fname = false;
	$options_fname = false;
	$flist = false;
	$ad_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$option = get_option('cp_auction_fields');
	$options = get_option('cp_auction_details');
	if( empty( $ad_type ) ) $ad_type = "classified";

// Hooks into cp_ad_details_field to disable fields not to display in details.

	$fname = $result->field_name;
	if ( isset( $option[$fname] ) ) $option_fname = $option[$fname];
	if ( isset( $options[$fname] ) ) $options_fname = $options[$fname];
	if( $option_fname ) $flist = explode(',', $option_fname);

if ( $flist && !in_array($ad_type, $flist) || $options_fname != "yes" ) return false;

else

return $result;
}
add_filter( 'cp_ad_details_field', 'cp_auction_display_ad_details_fields' );


/*
|--------------------------------------------------------------------------
| SELECT WHICH FIELDS TO DISPLAY WITH A CURRENCY SYMBOL.
|--------------------------------------------------------------------------
*/

// this function needs to understand meta names of auction fields and add appropriate filters
function auction_display_ad_details( $result ) {

if ( !$result )
return false;


// get our fields array with properties and assign to variable

// example of option contents: $fields = array( 'cp_field1' => 'yes', 'cp_field2' => 'yes', )
$fields = get_option( 'cp_auction_pricefields' );

$name = $result->field_name;

if ( $name != "cp_min_price" && isset( $fields[ $name ] ) && 'yes' === $fields[ $name ] )
add_filter( 'cp_ad_details_' . $name, 'auction_currency_symbol' );

return $result;

}
add_filter(  'cp_ad_details_field', 'auction_display_ad_details' );


// This function needs to add currency to the value of certain field
function auction_currency_symbol( $args ) {
	$args['value'] = cp_display_price( $args['value'], 'ad', false );

	return $args;

}


/*
|--------------------------------------------------------------------------
| CHANGE THE RESULT OF RESERVE PRICE ON AUCTIONS.
|--------------------------------------------------------------------------
*/

// this function needs to understand meta names of auction fields and add appropriate filters
function cp_auction_display_ad_details( $result ) {

global $post;

if ( !$result )
return false;

$cbid = cp_auction_plugin_get_latest_bid($post->ID);
$min_price = get_post_meta( $post->ID, 'cp_min_price', true );
$name = "cp_min_price";

if ( $cbid >= $min_price )
add_filter( 'cp_ad_details_' . $name, 'auction_min_price_is_achieved' );
else
add_filter( 'cp_ad_details_' . $name, 'auction_min_price_not_achieved' );

return $result;

}
add_filter(  'cp_ad_details_field', 'cp_auction_display_ad_details' );

function auction_min_price_is_achieved( $args ) {
	$args['value'] = ''.__('Is achieved', 'auctionPlugin').'';

	return $args;
}

function auction_min_price_not_achieved( $args ) {
	$args['value'] = ''.__('Not achieved', 'auctionPlugin').'';

	return $args;
}


/*
|--------------------------------------------------------------------------
| UPDATE THE NEW LISTING BASED ON AD TYPE
|--------------------------------------------------------------------------
*/

function cp_auction_add_new_listing( $post_id ) {
	global $post, $cp_adtype, $cp_options, $wpdb;

	if ( isset( $_GET['type'] ) ) $cp_adtype = $_GET['type'];
	else if ( isset( $_REQUEST['cp_auction_my_auction_type'] ) )
	$cp_adtype = $_REQUEST['cp_auction_my_auction_type'];

	if( empty( $cp_adtype ) ) $cp_adtype = "classified";

	$post = get_post( $post_id );

	$todayDate = current_time('mysql');
	$ad_period = get_post_meta( $post_id, 'cp_auction_listing_duration', true );
	$period = get_option( 'cp_auction_period_type' );
	$howmany = get_post_meta( $post_id, 'cp_auction_howmany', true );
	$pausead = get_post_meta( $post_id, 'cp_pausead', true );
	$pause_ad = get_localized_field_values('cp_pausead', $pausead);
	$cp_max_price = get_post_meta( $post_id, 'cp_max_price', true );
	$cp_start_price = get_post_meta( $post_id, 'cp_start_price', true );
	$total_cost = get_post_meta( $post_id, 'cp_sys_total_ad_cost', true );
	$permit = get_option('cp_auction_enable_comments');
	$approves = get_option('cp_auction_plugin_approves');
	$cla_approves = get_option('cp_auction_plugin_cla_approves');
	$wnt_approves = get_option('cp_auction_plugin_wnt_approves');
	$upload = get_option('cp_auction_force_upload');
	$mrp_upload = get_option('cp_auction_mrp_force_upload');
	$option = get_option('cp_auction_fields');
	$allow = get_option('cp_auction_allow_pause_ad');
	$available = get_option('cp_auction_uploads');
	$use_credits = get_option('cp_auction_plugin_credits');
	$rewards = get_option('cp_auction_plugin_credits_on_posting');
	$upl = explode(',', $available['cp_ad_types']);
	$currency = get_user_meta( $post->post_author, 'cp_currency', true );
	if( ! $currency )
	$currency = get_post_meta( $post_id, 'cp_currency', true );
	if( ! $currency || $currency == "-1" )
	$currency = $cp_options->curr_symbol;

    	if ( ! wp_is_post_revision( $post_id ) ) {
	remove_action('cp_action_add_new_listing', 'cp_auction_add_new_listing');

	if( in_array($cp_adtype, $upl) ) {
	$table_uploads = $wpdb->prefix . "cp_auction_plugin_uploads";
	$query = "INSERT INTO {$table_uploads} (pid, post_author) VALUES (%d, %d)";
	$wpdb->query($wpdb->prepare($query, $post_id, $post->post_author));
	}

	update_post_meta( $post_id, 'cp_auction_my_auction_type', $cp_adtype );
	update_post_meta( $post_id, 'cp_currency', $currency );

	if ( $use_credits == "yes" && $rewards > 0 ) {
	$tm = time();
	$ip_address = cp_auction_users_ip();
	$value = cp_auction_sanitize_amount($rewards * get_option('cp_auction_plugin_credit_value'));
	$available_credits = get_user_meta( $post->post_author, 'cp_credits', true );
	$add_credits = $available_credits + $rewards;
	update_user_meta( $post->post_author, 'cp_credits', $add_credits );
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$query = "INSERT INTO {$table_transactions} (datemade, uid, ip_address, trans_type, moved_credits, available, mc_gross, aws, type, status) VALUES (%d, %d, %s, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $tm, $post->post_author, $ip_address, 'Rewards', $rewards, $add_credits, $value, 'aws-credits', 'purchase', 'complete'));
	}

	if ( $cp_adtype == "normal" || $cp_adtype == "reverse" ) {
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$todayDate + $ad_period $period"), true);
	update_post_meta( $post_id, 'cp_price', $cp_start_price );
	update_post_meta( $post_id, 'cp_sys_expire_date', $cp_sys_expire_date );
	update_post_meta( $post_id, 'cp_end_date', strtotime($cp_sys_expire_date) );
	update_post_meta( $post_id, 'cp_sys_ad_duration', $ad_period );
	}

	if( $howmany > 0 ) {
	update_post_meta( $post_id, 'cp_auction_howmany_added', "yes" );
	}
	elseif( $howmany == 0 && $cp_adtype == "classified" ) {
	update_post_meta( $post_id, 'cp_auction_howmany_added', "unlimited" );
	}
	else {
	update_post_meta( $post_id, 'cp_auction_howmany_added', "none" );
	}

	if( $cp_adtype == "wanted" )
	update_post_meta( $post_id, 'cp_price', $cp_max_price );

	$negotiable = get_post_meta( $post_id, 'cp_price_negotiable', true );
	$buy_price = get_post_meta( $post_id, 'cp_buy_now', true );
	$wantto = get_option('cp_auction_want_to_defaults');

	if ( $cp_adtype == "classified" && get_option('cp_auction_buynow_on_classifieds') == "yes" )
		update_post_meta( $post_id, 'cp_buy_now_enabled', "yes" );

	if ( $cp_adtype == "normal" && get_option('cp_auction_buynow_on_auctions') == "yes" && $buy_price > 0 )
		update_post_meta( $post_id, 'cp_buy_now_enabled', "yes" );

	if ( $negotiable == true && get_option('aws_bo_negotiable_price') == "yes" )
		update_post_meta( $post_id, 'cp_bestoffer', "Tick to enable" );

	if ( $cp_adtype == "wanted" && ! empty( $wantto ) )
		update_post_meta( $post_id, 'cp_want_to', get_option('cp_auction_want_to_defaults') );

	if ( $cp_adtype == "wanted" && get_option('cp_auction_allow_deals_auto') == "yes" )
		update_post_meta( $post_id, 'cp_allow_deals', "Yes" );

	if ( isset( $option['cp_pausead'] ) && $total_cost < '0.01' ) {
	$flist = explode(',', $option['cp_pausead']);
	$status = $post->post_status;
	if ( $pause_ad == "Paused" ) $status = "draft";
	if ( $pause_ad == "Private" ) $status = "private";

	if ( $post->post_status == 'publish' || $pause_ad == "Private" ) {
	if ( $allow == "yes" && $cp_adtype == "classified" && in_array("classified", $flist) ) {
	$new_post = array();
	$new_post['ID'] = $post_id;
	$new_post['post_status'] = $status;
	wp_update_post( $new_post );
	}
	if ( $allow == "yes" && $cp_adtype == "wanted" && in_array("wanted", $flist) ) {
	$new_post = array();
	$new_post['ID'] = $post_id;
	$new_post['post_status'] = $status;
	wp_update_post( $new_post );
	}
	if ( $allow == "yes" && $cp_adtype == "normal" && in_array("normal", $flist) ) {
	$new_post = array();
	$new_post['ID'] = $post_id;
	$new_post['post_status'] = $status;
	wp_update_post( $new_post );
	}
	if ( $allow == "yes" && $cp_adtype == "reverse" && in_array("reverse", $flist) ) {
	$new_post = array();
	$new_post['ID'] = $post_id;
	$new_post['post_status'] = $status;
	wp_update_post( $new_post );
	}
	}
}

	$comments = "closed";
	if( $permit == "yes" ) $comments = "open";
	$new_post = array();
	$new_post['ID'] = $post_id;
	$new_post['comment_status'] = $comments;
	wp_update_post( $new_post );


	if ( ( $post->post_status == 'publish' || $post->post_status == 'draft' ) && ( $approves == "yes" || $upload == "yes" )
	&& ( $cp_adtype == "normal" || $cp_adtype == "reverse" ) ) {
	$new_post = array();
	$new_post['ID'] = $post_id;
	$new_post['post_status'] = 'pending';
	wp_update_post( $new_post );
	}

	if ( ( $post->post_status == 'publish' || $post->post_status == 'draft' ) && ( $cp_adtype == "classified" ) 
	&& ( $cla_approves == "yes" || $mrp_upload == "yes" ) ) {
	$new_post = array();
	$new_post['ID'] = $post_id;
	$new_post['post_status'] = 'pending';
	wp_update_post( $new_post );
	}

	if ( ( $post->post_status == 'publish' || $post->post_status == 'draft' ) && ( $cp_adtype == "wanted" && $wnt_approves == "yes" ) ) {
	$new_post = array();
	$new_post['ID'] = $post_id;
	$new_post['post_status'] = 'pending';
	wp_update_post( $new_post );
	}
        add_action('cp_action_add_new_listing', 'cp_auction_add_new_listing');
}
}
add_action( 'cp_action_add_new_listing', 'cp_auction_add_new_listing', 10, 1 );


/*
|--------------------------------------------------------------------------
| UPDATE THE EDIT LISTING BASED ON AD TYPE
|--------------------------------------------------------------------------
*/

function cp_auction_update_listing( $post_id ) {
	global $post, $cp_adtype, $cp_options, $wpdb;

	if ( isset( $_GET['type'] ) ) $cp_adtype = $_GET['type'];
	else if ( isset( $_REQUEST['cp_auction_my_auction_type'] ) )
	$cp_adtype = $_REQUEST['cp_auction_my_auction_type'];

	if( empty( $cp_adtype ) ) $cp_adtype = "classified";

	$post = get_post( $post_id );
	$todayDate = current_time('mysql');
	$ad_period = get_post_meta( $post_id, 'cp_auction_listing_duration', true );
	$cp_sys_ad_duration = get_post_meta( $post_id, 'cp_sys_ad_duration', true );
	$new_ad_duration = $cp_options->prun_period;
	$cp_end_date = get_post_meta( $post_id, 'cp_end_date', true );
	$cp_start_price = get_post_meta( $post_id, 'cp_start_price', true );
	$currency = get_user_meta( $post->post_author, 'cp_currency', true );
	if( ! $currency )
	$currency = get_post_meta( $post_id, 'cp_currency', true );
	if( ! $currency || $currency == "-1" )
	$currency = $cp_options->curr_symbol;
	$howmany = get_post_meta( $post_id, 'cp_auction_howmany', true );
	$cp_max_price = get_post_meta( $post_id, 'cp_max_price', true );
	$my_type = get_post_meta( $post_id, 'cp_auction_my_auction_type', true );

	if ( ! wp_is_post_revision( $post_id ) ) {
	remove_action('cp_action_update_listing', 'cp_auction_update_listing');

	if ( ( $cp_adtype != $my_type ) && ( $my_type == "normal" || $my_type == "reverse" ) ) {
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$todayDate + $new_ad_duration days"), true);
	update_post_meta( $post_id, 'cp_auction_my_auction_type', $cp_adtype );
	update_post_meta( $post_id, 'cp_sys_expire_date', $cp_sys_expire_date );
	update_post_meta( $post_id, 'cp_sys_ad_duration', $new_ad_duration );
	delete_post_meta( $post_id, 'cp_start_price' );
	delete_post_meta( $post_id, 'cp_min_price' );
	delete_post_meta( $post_id, 'cp_buy_now' );
	delete_post_meta( $post_id, 'cp_bid_increments' );
	delete_post_meta( $post_id, 'cp_auction_listing_duration' );
	delete_post_meta( $post_id, 'cp_end_date' );
	}
	else if ( ( $cp_adtype != $my_type ) && ( $my_type == "classified" || $my_type == "wanted" ) ) {
	$cp_sys_expire_date = date_i18n('Y-m-d H:i:s', strtotime("$todayDate + $ad_period days"), true);
	update_post_meta( $post_id, 'cp_auction_my_auction_type', $cp_adtype );
	update_post_meta( $post_id, 'cp_price', $cp_start_price );
	update_post_meta( $post_id, 'cp_sys_expire_date', $cp_sys_expire_date );
	update_post_meta( $post_id, 'cp_end_date', strtotime($cp_sys_expire_date) );
	update_post_meta( $post_id, 'cp_sys_ad_duration', $ad_period );
	delete_post_meta( $post_id, 'cp_iwant_to' );
	delete_post_meta( $post_id, 'cp_auction_howmany' );
	delete_post_meta( $post_id, 'cp_auction_howmany_added' );
	}

	update_post_meta( $post_id, 'cp_auction_my_auction_type', $cp_adtype );
	update_post_meta( $post_id, 'cp_currency', $currency );

	if ( $cp_adtype == "normal" || $cp_adtype == "reverse" ) {
	update_post_meta( $post_id, 'cp_price', $cp_start_price );
	}

	if( $howmany > 0 ) {
	update_post_meta( $post_id, 'cp_auction_howmany_added', "yes" );
	}
	elseif( $howmany == 0 && $cp_adtype == "classified" ) {
	update_post_meta( $post_id, 'cp_auction_howmany_added', "unlimited" );
	}
	else {
	update_post_meta( $post_id, 'cp_auction_howmany_added', "none" );
	}

	if( $cp_adtype == "wanted" ) {
	update_post_meta( $post_id, 'cp_price', $cp_max_price );
	}

}
}
add_action( 'cp_action_update_listing', 'cp_auction_update_listing', 10, 1 );