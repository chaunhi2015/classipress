<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| ORDER OF FRONTPAGE TABBED MENU
|--------------------------------------------------------------------------
*/

function cp_auction_frontpage_tab_order() {
	global $wpdb;

	$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";
	$tabs = $wpdb->get_results("SELECT DISTINCT * FROM {$table_name} ORDER BY sequence ASC");

	foreach ( $tabs as $tab ) {

	if ( $tab->position == "front" || $tab->position == "both" )
	echo '<li><a href="#'.$tab->block.'"><span class="big">'.$tab->title.'</span></a></li>';

	}
}


/*
|--------------------------------------------------------------------------
| FRONTPAGE CONTENT
|--------------------------------------------------------------------------
*/

function cp_auction_frontpage_content($ct) {

	if ( ! is_front_page() ) return;

	global $wpdb, $wp_query, $cp_options, $gmap_active;
	$maxposts = get_option('posts_per_page');
	$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";
	$only_admins = get_option('cp_auction_only_admins_post_auctions');
	$active = 0;
	$position = array( 'front', 'both' );
?>

<script type="text/javascript">
function toggle_visibility(id) {
    var e = document.getElementById(id);
    if (e.style.display == 'block') e.style.display = 'none';
    else e.style.display = 'block';
    
    hideAllBut(id);
}

function hideAllBut(id) {
    var lists = document.querySelectorAll('#directory');
    for (var i = lists.length; i--; ) {
        if (lists[i].id != id) {
            lists[i].style.display = 'none';
        }
    }
}
</script>

<?php if( $ct == "flatpress" ) { ?>

<?php if( function_exists( 'tb_slider_ultimate' ) ) { ?>
	<div class="content_res">

<?php if ( function_exists('cp_auction_frontpage_one') ) cp_auction_frontpage_one(); ?>

		<?php tb_slider_ultimate(); ?>
	</div>

<?php } else {

if ( function_exists('cp_auction_frontpage_one') ) cp_auction_frontpage_one();

	if (function_exists('tb_slider_ultimate')) {
		tb_slider_ultimate();
	} else if (function_exists('xtremecarousel')) {
		xtremecarousel();
	} else {
		get_template_part( 'featured' );
	}
} ?>

<?php } if( $ct == "ultraclassifieds" ) { ?>
<div class="content_rest">

<?php if ( function_exists('cp_auction_frontpage_one') ) cp_auction_frontpage_one(); ?>

<?php if (get_option('enable_slider')=='Yes') :
	if (function_exists('tb_slider_ultimate')) {
		tb_slider_ultimate();
	} else if (function_exists('xtremecarousel')) {
		xtremecarousel();
	} else {
		get_template_part( 'featured' );
	}
endif;
?>

</div>
<div class="clear"></div>

<?php } if( $ct == "eclassify" ) { ?>
<div class="content_rest">

<?php if ( function_exists('cp_auction_frontpage_one') ) cp_auction_frontpage_one(); ?>

<?php if (get_option('enable_featured_main')=='Enable') :
	if (function_exists('tb_slider_ultimate')) {
		tb_slider_ultimate();
	} else if (function_exists('xtremecarousel')) {
		xtremecarousel();
	} else {
		get_template_part( 'featured' );
	}
endif;
?>

</div>
<div class="clear"></div>

<div class="clear"></div>
<?php } if( $ct == "flannel" ) { ?>

<div class="content">

	<div class="content_botbg">

		<div class="content_res pre-slider">

<?php if ( function_exists('cp_auction_frontpage_one') ) cp_auction_frontpage_one(); ?>

			<?php if (function_exists('tb_slider_ultimate')) {
					tb_slider_ultimate();
				} else if (function_exists('xtremecarousel')) {
					xtremecarousel();
				} else {
					get_template_part( 'featured' );
				} ?>

		<!-- left block -->
		</div> <!-- /.content_res pre-slider -->

	</div> <!-- /.content_bot_bg -->

</div> <!-- /.content -->

<?php // Closing off HTML markup above to seperat slider from main content to generate box shadow ?>

<div class="content second">

<?php } else { ?>

<div class="content">

<?php if ( function_exists('cp_auction_frontpage_one') ) cp_auction_frontpage_one(); ?>

<?php } ?>

	<div class="content_botbg">

		<div class="content_res">

			<?php if( $ct == "rondell" ) { ?>
			<div class="content_cpauction">
			<?php } ?>

				<?php if ( get_option('cp_auction_frontpage_title') != "" && get_option('cp_auction_frontpage_pos') == "above_slider" ) { ?>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php echo get_option('cp_auction_frontpage_title'); ?></h2>

						<div class="page_info"><?php echo nl2br( get_option('cp_auction_frontpage_text') ); ?></div>

								<div class="clear20"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

				<?php } ?>

			<?php if( $ct == "rondell" ) { ?>
			</div><!-- content_cpauction -->
			<div class="clr"></div>
			<?php } ?>

			<?php if( $ct != "flannel" && $ct != "flatpress" && $ct != "flatron" && $ct != "ultraclassifieds" && $ct != "eclassify" ) { ?>

				<?php if (function_exists('tb_slider_ultimate')) {
					tb_slider_ultimate();
				} else if (function_exists('xtremecarousel')) {
					xtremecarousel();
				} else {
					get_template_part( 'featured' );
				} ?>

			<?php } ?>

			<!-- left block -->
			<div class="content_left">
			<?php if( $ct == "twinpress_classifieds" ) { ?>
			<div class="conl">
			<?php if ( function_exists('cp_auction_frontpage_two') ) cp_auction_frontpage_two(); ?>
			<?php } if( $ct == "eclassify" ) { ?>
			<?php if ( function_exists('cp_auction_frontpage_two') ) cp_auction_frontpage_two(); ?>
			<?php if (get_option('enable_featured_small')=='Enable') : ?>
			<?php  get_template_part( 'featured-alt' ); ?>
			<?php endif; ?>  
			<div class="clear"></div>
			<?php } ?>

<?php if( $ct == "jibo" || get_option( 'cp_auction_slide_in_ads' ) == "yes" ) { ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({
    activate: function (e, ui) {
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' });
    },
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
    active: $.cookie('homeTab')
});
});
</script>
<?php } else { ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({
    activate: function (e, ui) {
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' });
    },
    active: $.cookie('homeTab')
});
});
</script>

<?php } if ( function_exists('cp_auction_frontpage_three') ) cp_auction_frontpage_three(); ?>

				<?php if ( get_option('cp_auction_frontpage_title') != "" && get_option('cp_auction_frontpage_pos') == "above_cats" ) { ?>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php echo get_option('cp_auction_frontpage_title'); ?></h2>

						<div class="page_info"><?php echo nl2br( get_option('cp_auction_frontpage_text') ); ?></div>

								<div class="clear20"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

				<?php } ?>

				<?php if ( ( $cp_options->home_layout == 'directory' && $ct != "flatron" && $ct != "eclassify" && $ct != "ultraclassifieds" ) || ( get_option('layout_style') == 'Simple' ) ) { ?>

				<div id="toggle"></div>

				<?php if( $ct == "simply-responsive-cp" ) { ?>
				<div class="shadowblock_out_cat">
				<?php } ?>

					<div class="shadowblock_out">

						<div class="shadowblock">

						<?php if( get_option('cp_auction_toggle_cats') == "yes" ) { ?>

						<?php if( $ct == "flatpress" || $ct == "rondell" ) { ?>
						<h2 class="dotted"><a href="#toggle" onclick="toggle_visibility('directory');" class="mbtn btn_orange"><?php _e( 'Ad Categories', 'auctionPlugin' ); ?></a></h2>
						<?php } else { ?>
						<p class="dotted"><a href="#toggle" onclick="toggle_visibility('directory');" class="sbtn btn_orange"><?php _e( 'Ad Categories', 'auctionPlugin' ); ?></a></p>
						<?php } ?>

						<p><?php _e( 'Click the button to display or hide the categories.', 'auctionPlugin' ); ?></p>

						<div id="directory" class="directory <?php cp_display_style( 'dir_cols' ); ?>" style="display:none;">

						<?php } else { ?>
						<h2 class="dotted"><?php _e( 'Ad Categories', 'auctionPlugin' ); ?></h2>

						<div id="directory" class="directory <?php cp_display_style( 'dir_cols' ); ?>">

						<?php } ?>

						<?php echo cp_create_categories_list( 'dir' ); ?>

						<div class="clr"></div>

						</div><!--/directory-->

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

				<?php if( $ct == "simply-responsive-cp" ) { ?>
				</div><!-- /shadowblock_out_cat -->
				<?php } ?>

				<?php } ?>

<?php if ( function_exists('cp_auction_frontpage_four') ) cp_auction_frontpage_four(); ?>

		<?php if( get_option( 'cp_auction_rondell_banner' ) != "yes" && $ct == "rondell" ) { ?>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php _e( 'Maybe a banner here? You decide!', 'auctionPlugin' ); ?></h2>

								<div class="pad5 dotted"></div>

									<center>

										<p><a href="http://www.wpmorphed.com" target="_blank"><img src= "<?php echo get_stylesheet_directory_uri() ?>/images/2_advert468_60.png" alt="ClassiPress Premium Child Themes" width="468" height="60"/></a></p>

									</center>

								<div class="clr"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

		<?php } ?>

<?php if ( function_exists('cp_auction_frontpage_five') ) cp_auction_frontpage_five(); ?>

				<?php if ( get_option('cp_auction_frontpage_title') != "" && get_option('cp_auction_frontpage_pos') == "above_menu" ) { ?>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php echo get_option('cp_auction_frontpage_title'); ?></h2>

						<div class="page_info"><?php echo nl2br( get_option('cp_auction_frontpage_text') ); ?></div>

								<div class="clear20"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

				<?php } ?>

<?php if ( function_exists('cp_auction_frontpage_six') ) cp_auction_frontpage_six(); ?>

				<?php if ( get_option('cp_auction_front_title') != "" && get_option('cp_auction_front_text') != "" ) { ?>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php echo get_option('cp_auction_front_title'); ?></h2>

						<div class="page_info"><?php echo nl2br( get_option('cp_auction_front_text') ); ?></div>

								<div class="clear20"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

				<?php } ?>

<?php if ( function_exists('cp_auction_frontpage_seven') ) cp_auction_frontpage_seven(); ?>

				<?php if( get_option('cp_auction_rondell_searchbar') != "yes" && $ct == "rondell" ) { ?>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php _e( 'Search in Ads', 'auctionPlugin' ); ?></h2>

								<div class="pad2 dotted"></div>

									<center>

									<?php get_template_part( 'includes/theme', 'searchbar' ); ?>

									</center>

								<div class="clr"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

				<?php } ?>

<?php if ( function_exists('cp_auction_frontpage_eight') ) cp_auction_frontpage_eight(); ?>

<?php if ($ct == "flatron" && get_option('enable_contents') == 'true'):
	$import_page = get_page_by_title(get_option('home_page_import'));
	echo "$import_page->post_content";
endif; ?>

<?php if ( function_exists('cp_auction_frontpage_nine') ) cp_auction_frontpage_nine(); ?>

				<?php if( $ct == "eclassify" ) { ?>

				<div class="tabcontrol">

					<ul class="tabnavig">
				<?php } else { ?>

					<div class="tabhome tabcontrol">

<?php if( $ct == "flatron" && get_option( 'cp_version' ) > '3.3.3'  ) { ?>
<style>
.view-cnt {
	display: none;
}
</style>
<?php } if( $ct == "flatron" ) { ?>
<style>
.list, .grid {
	margin-top: 2px !important;
}
</style>
<div id="list" class="list "><span></span></div>
<div id="grid" class="grid"><span></span></div>
<?php } ?>

						<ul class="tabmenu">
				<?php } ?>

						<?php cp_auction_frontpage_tab_order(); ?>
					</ul>

					<?php
						remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
						$post_type_url = esc_url( add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) ) );
					?>

					<?php if( $ct == "jibo" && in_array( get_option( 'cp_auction_jibo_tab_where' ), $position ) ) { ?>

					<!-- tab 0 -->
					<div id="block0">
		
						<div class="clr"></div>

						<div class="tabunder"><span class="big"><?php _e( 'Welcome', 'auctionPlugin' ); ?> / <strong><span class="colour"><?php _e( 'Ad posting rules', 'auctionPlugin' ); ?></span></strong></span></div>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php _e( 'Terms and conditions', 'auctionPlugin' ); ?></h2>

							<?php echo get_option('jibo_welcome_tab') ?>

								<div class="clr"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

					</div><!-- /block0 -->

					<?php } if( in_array( get_option( 'cp_auction_listed_tab_where' ), $position ) ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block1'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 1 -->
					<div id="block1">

						<div class="clr"></div>

						<?php if( ( $ct != "simply-responsive-cp" ) || ( $ct == "simply-responsive-cp" && get_option( 'aws_gridview_installed' ) == "yes" ) ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_listed_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// show all ads but make sure the sticky featured ads don't show up first
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$latest_url = esc_url( add_query_arg( array( 'sort' => 'latest' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $latest_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php 
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block1 -->

					<?php } if( in_array( get_option( 'cp_auction_featured_tab_where' ), $position ) ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block2'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 2 -->
            				<div id="block2">

              				<div class="clr"></div>

					<?php if( $ct != "simply-responsive-cp" ) { ?>
					<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_featured_tab'); ?> / <strong><span class="colour"><?php _e( 'Featured Listings', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views1" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views1" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
					<?php } ?>

                				<?php
                  					// show all featured ads
                					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'post__in' => get_option('sticky_posts'), 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

                				<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
									$featured_url = esc_url( add_query_arg( array( 'sort' => 'featured' ), $post_type_url ) );
						?>
									<div class="paging"><a href="<?php echo $featured_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

            				</div><!-- /block2 -->

					<?php } if( in_array( get_option( 'cp_auction_popular_tab_where' ), $position ) ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block3'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 3 -->
					<div id="block3">

						<div class="clr"></div>

						<?php if( ( $ct != "simply-responsive-cp" ) || ( $ct == "simply-responsive-cp" && get_option( 'aws_gridview_installed' ) == "yes" ) ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_popular_tab'); ?> / <strong><span class="colour"><?php _e( 'Most Popular', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views2" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views2" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// show all most popular ads
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_key' => 'cp_total_count',
							'orderby' => 'meta_value_num',
							'order' => 'DESC',
							'meta_query' => 
								array( 
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$popular_url = esc_url( add_query_arg( array( 'sort' => 'popular' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $popular_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block3 -->

					<?php } if( in_array( get_option( 'cp_auction_classified_tab_where' ), $position ) ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block4'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 4 -->
					<div id="block4">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_classified_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views3" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views3" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array( 
									'key' => 'cp_auction_my_auction_type', 
									'value' => array( 'classified', '' ), 
									'compare' => 'IN'
									),
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$classifieds_url = esc_url( add_query_arg( array( 'sort' => 'classifieds' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $classifieds_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block4 -->

					<?php } if( in_array( get_option( 'cp_auction_auction_tab_where' ), $position ) ) {

					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block6'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 6 -->
					<div id="block6">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_auction_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views4" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views4" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array( 
									'key' => 'cp_auction_my_auction_type', 
									'value' => array( 'normal', 'reverse' ), 
									'compare' => 'IN'
									),
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);
						
							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$auction_url = esc_url( add_query_arg( array( 'sort' => 'auctions' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $auction_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block6 -->

					<?php } if( in_array( get_option( 'cp_auction_wanted_tab_where' ), $position ) ) {

					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block7'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 7 -->
					<div id="block7">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_wanted_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views5" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views5" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array( 
									'key' => 'cp_auction_my_auction_type', 
									'value' => 'wanted', 
									'compare' => 'IN'
									),
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);
						
							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$wanted_url = esc_url( add_query_arg( array( 'sort' => 'wanted' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $wanted_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block7 -->

				<?php } if( get_option('cp_auction_menu_tab_one') != "" ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block8'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 8 -->
					<div id="block8">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_menu_tab_one'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views6" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views6" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// show all ads but make sure the sticky featured ads don't show up first
							$cats_one = get_option('cp_auction_menu_tab_cats_one');
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'tax_query' => 
								array( 
									array( 
									'taxonomy' => 'ad_cat',
									'field' => 'term_id',
									'terms' => array($cats_one)
									) 
								) 
							) 
						);

						$total_pages = max( 1, absint( $wp_query->max_num_pages ) );
						?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$tabone_url = esc_url( add_query_arg( array( 'sort' => 'tabone' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $tabone_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block8 -->

					<?php } if( get_option('cp_auction_menu_tab_two') != "" ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block9'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 9 -->
					<div id="block9">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_menu_tab_two'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views7" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views7" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// show all ads but make sure the sticky featured ads don't show up first
							$cats_two = get_option('cp_auction_menu_tab_cats_two');
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'tax_query' => 
								array( 
									array( 
									'taxonomy' => 'ad_cat',
									'field' => 'term_id',
									'terms' => array($cats_two)
									) 
								) 
							) 
						);

						$total_pages = max( 1, absint( $wp_query->max_num_pages ) );
						?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$tabtwo_url = esc_url( add_query_arg( array( 'sort' => 'tabtwo' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $tabtwo_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block9 -->

					<?php } if( get_option('cp_auction_menu_tab_three') != "" ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block10'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 10 -->
					<div id="block10">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_menu_tab_three'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views8" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views8" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// show all ads but make sure the sticky featured ads don't show up first
							$cats_three = get_option('cp_auction_menu_tab_cats_three');
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'tax_query' => 
								array( 
									array( 
									'taxonomy' => 'ad_cat',
									'field' => 'term_id',
									'terms' => array($cats_three)
									) 
								) 
							) 
						);

						$total_pages = max( 1, absint( $wp_query->max_num_pages ) );
						?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$tabthree_url = esc_url( add_query_arg( array( 'sort' => 'tabthree' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $tabthree_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block10 -->

				<?php } if( in_array( get_option( 'cp_auction_categories_tab_where' ), $position ) ) { ?>

					<!-- tab 11 -->
            				<div id="block11">

              				<div class="clr"></div>

					<?php if( $ct != "simply-responsive-cp" ) { ?>
              				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_categories_tab'); ?> / <strong><span class="colour"><?php _e( 'Ad Categories', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views9" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views9" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
					<?php } ?>

				<?php if( $ct == "simply-responsive-cp" ) { ?>
				<div class="shadowblock_out_cat">
				<?php } ?>

              				<div class="shadowblock_out">
 
                    					<div class="shadowblock">

								<h2 class="dotted"><?php _e( 'Categories', 'auctionPlugin' ); ?></h2>

							<?php if( get_option('cp_auction_toggle_cats') == "yes" ) { ?>
							<div id="directories" class="directory <?php cp_display_style( 'dir_cols' ); ?>">
							<?php } else { ?>
							<div id="directory" class="directory <?php cp_display_style( 'dir_cols' ); ?>">
							<?php } ?>

								<?php echo cp_create_categories_list( 'dir' ); ?>

								<div class="clr"></div>

							</div><!--/directory(ies) -->

						</div><!-- /shadowblock -->

						</div><!-- /shadowblock_out -->

				<?php if( $ct == "simply-responsive-cp" ) { ?>
				</div><!-- /shadowblock_out_cat -->
				<?php } ?>

            				</div><!-- /block11 -->

				<?php } if( in_array( get_option( 'cp_auction_blog_tab_where' ), $position ) ) { ?>

					<!-- tab 12 -->
					<div id="block12">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_blog_tab'); ?> / <strong><span class="colour"><?php _e( 'Latest News', 'auctionPlugin' ); ?></span></strong></span></div>
						<?php } ?>

						<div class="clr"></div>

						<?php
							// show all posts
							remove_action('appthemes_after_blog_endwhile', 'cp_do_pagination' );
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => 'post', 
							'post_status' => 'publish', 
							'paged' => $paged 
							) 
						);

							$total_pages = max( 1, absint( $wp_query->max_num_pages ) );
						?>

						<?php get_template_part( 'loop' ); ?>

						<?php wp_reset_query(); ?>

						<?php
							if ( $total_pages > 1 ) {
								$blog_url = esc_url( add_query_arg( array( 'sort' => 'blog' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $blog_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Posts', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts('post');
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'posts online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

						<div class="clear20"></div>

				</div><!-- /block12 -->

					<?php } if( get_option('cp_auction_menu_tab_textbox') != "" ) { ?>

					<!-- tab 13 -->
					<div id="block13">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_menu_tab_textbox'); ?><?php if( get_option('cp_auction_textbox_title') != "" ) { ?> / <strong><span class="colour"><?php echo get_option('cp_auction_textbox_title'); ?></span></strong><?php } ?></span></div>
						<?php } ?>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<?php if( get_option('cp_auction_textbox_title') != "" ) { ?>
							<h2 class="dotted"><?php echo get_option('cp_auction_textbox_title'); ?></h2>
							<?php } ?>

						<div class="page_info"><?php echo nl2br( get_option('cp_auction_textbox_text') ); ?></div>

								<div class="clear20"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

					</div><!-- /block13 -->

					<?php } if( in_array( get_option( 'cp_auction_category_tab_where' ), $position ) ) { ?>

					<!-- tab 14 -->
					<div id="block14">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_category_tab'); ?> / <strong><span class="colour"><?php _e('View ads by category', 'auctionPlugin'); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views10" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views10" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
				<?php } ?>

				<?php cp_auction_list_by_category(); ?>

					</div><!-- /block14 -->

					<?php } if( in_array( get_option( 'cp_auction_buynow_tab_where' ), $position ) ) { ?>

					<!-- tab 15 -->
					<div id="block15">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_buynow_tab'); ?> / <strong><span class="colour"><?php _e('Just Listed', 'auctionPlugin'); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views11" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views11" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
				<?php } ?>

				<?php cp_auction_buy_now_auctions(); ?>

					</div><!-- /block15 -->

					<?php } if( in_array( get_option( 'cp_auction_closing_tab_where' ), $position ) ) { ?>

					<!-- tab 16 -->
					<div id="block16">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_closing_tab'); ?> / <strong><span class="colour"><?php _e('Soon to close auctions', 'auctionPlugin'); ?></span></strong></span></div>
				<?php } ?>

				<?php cp_auction_soon_to_close(); ?>

					</div><!-- /block16 -->

					<?php } if( in_array( get_option( 'cp_auction_intention_tab_where' ), $position ) ) { ?>

					<!-- tab 17 -->
					<div id="block17">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_intention_tab'); ?> / <strong><span class="colour"><?php _e('View ads by ad type', 'auctionPlugin'); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views12" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views12" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
				<?php } ?>

				<?php cp_auction_list_by_intention(); ?>

					</div><!-- /block17 -->

					<?php } if( in_array( get_option( 'cp_auction_buynow_classifieds_tab_where' ), $position ) ) { ?>

					<!-- tab 18 -->
					<div id="block18">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_buynow_classifieds_tab'); ?> / <strong><span class="colour"><?php _e('Buy it Now Ads', 'auctionPlugin'); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views grid-views13" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views list-views13" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
				<?php } ?>

				<?php cp_auction_buy_now_classifieds(); ?>

					</div><!-- /block18 -->

					<?php } if( in_array( get_option( 'cp_auction_sold_ads_tab_where' ), $position ) ) {

					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block19'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 19 -->
					<div id="block19">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_sold_ads_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span></div>
						<?php } ?>

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array(
										'key' => 'cp_ad_sold',
										'value' => 'yes', 
										'compare' => 'IN'
										), 
									),
								) 
							);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$sold_ads_url = esc_url( add_query_arg( array( 'sort' => 'soldads' ), $post_type_url ) );
						?>
								<div class="paging"><a href="<?php echo $sold_ads_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block19 -->

				<?php } if( get_option('cp_auction_rondell_banner_bottom') != "yes" && $ct == "rondell" ) { ?>

				<div class="clear15"></div>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php _e( 'Sponsored Links', APP_TD ); ?></h2>

								<div class="pad5 dotted"></div>

									<center>

										<p><a href="http://www.wpmorphed.com" target="_blank"><img src= "<?php echo get_stylesheet_directory_uri() ?>/images/2_advert468_60.png" alt="ClassiPress Premium Child Themes" width="468" height="60"/></a></p>

									</center>

								<div class="clr"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

				<?php } ?>

				<?php wp_reset_query(); ?>

				</div><!-- /tab_control -->

			<?php if( $ct == "twinpress_classifieds" ) { ?>
			</div><!-- /conl -->
			<?php } ?>

<?php if ( function_exists('cp_auction_frontpage_ten') ) cp_auction_frontpage_ten(); ?>

			</div><!-- /content_left -->

			<?php get_sidebar(); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

<?php if( $ct == "eclassify" ) { ?>

	</div><!-- /content -->

<?php } if( $ct == "simply-responsive-cp" ) { ?>
<div id="allCat" style="display:none"><?php _e( 'View All', APP_TD ); ?></div>

<script type="text/javascript">

/* custom categories toggle script */
    jQuery(document).ready(function($) {

		jQuery(".shadowblock #directory .catcol ul .maincat").children("a").each(function () {
			var $ftogg = jQuery(this).parent(".maincat").children(".subcat-list").html();

			try {

				if ($ftogg.indexOf("li") > 0) {
					jQuery(this).parent(".maincat").children(".subcat-list").html("<li class='allcats'><a href='"+jQuery(this).attr("href")+"'>"+jQuery('#allCat').html()+"</a></li>" + $ftogg);
					jQuery(this).attr("href","#categories");
				}
			}
			catch (err) {}
    		});	

		jQuery(".shadowblock #directory .catcol ul .maincat ul").css("display","none");

    		jQuery(".shadowblock #directory .catcol ul .maincat").each(function () {
			var sCode = jQuery(this).html();

			var longi = "";

			try {
				longi = jQuery(this).children(".subcat-list").length;
			}
			catch (err) {
					$ftog = "";
			}

			if (longi==1) { jQuery(this).html("<div class='expand'></div>" + sCode); }
			else { jQuery(this).html("<div class='expand2'></div>" + sCode); }
    		});

    		jQuery(".shadowblock #directory .catcol ul .maincat").each(function () {
			var longi = "";

			try {
				longi = jQuery(this).children(".subcat-list").length;
			}
			catch (err) {
					$ftog = "";
			}

			if (longi==0) { jQuery(this).children("div").css("background-image","none"); }
    		});

		jQuery(".shadowblock #directory .catcol ul .maincat .expand").click(function(){
			var sVisible = jQuery(this).parent("li").children("ul").css("display");

			if (sVisible == "none") {
				jQuery(this).parent("li").children("ul").css("display","");
				jQuery(this).css("background-position","0 -10px");
			}
			else {
				jQuery(this).parent("li").children("ul").css("display","none");
				jQuery(this).css("background-position","0 0px");
			}
		});

		jQuery(".shadowblock #directory .catcol ul .maincat").children("a").click(function(){
			var sVisible = jQuery(this).parent("li").children("ul").css("display");

			if (sVisible == "none") {
				jQuery(this).parent("li").children("ul").css("display","");
				jQuery(this).parent("li").children(".expand").css("background-position","0 -10px");
			}
			else {
				jQuery(this).parent("li").children("ul").css("display","none");
				jQuery(this).parent("li").children(".expand").css("background-position","0 0px");
			}
		});

		jQuery(".shadowblock .title .toggle").click(function(){
			var sVisible = "" + jQuery(".shadowblock #directory").css("display");
			if (sVisible == "none") {
				jQuery(".shadowblock #directory").css("display","");
				jQuery(this).css("background-position","0 -25px");
			}
			else {
				jQuery(".shadowblock #directory").css("display","none");
				jQuery(this).css("background-position","0 0");
			}
		});

	});
</script>

<?php }
}


/*
|--------------------------------------------------------------------------
| SEARCH / LIST ADS BY CATEGORY
|--------------------------------------------------------------------------
*/

function cp_auction_list_by_category() {
	global $wp_query, $post, $cp_options;

	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$cpa = get_option('cp_auction_plugin_auctiontype');

?>
	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

<?php
	if( $cpa == "classified" || $cpa == "wanted" || $cpa == "mixed" || $cpa == "reverse" ) {
	$view_listings = ''.__('View Listings', 'auctionPlugin').'';
	} else {
	$view_listings = ''.__('View Auctions', 'auctionPlugin').'';
	} ?>

	<div class="clear5"></div>

	<form class="cp-ecommerce" action="" method="post" id="mainform">

        <?php if($cpa == "mixed") { ?>
<p>
	<label for="cp_auction_my_auction_type"><?php _e('Select the type of Ad:', 'auctionPlugin'); ?></label>
	<?php $auction_type = ""; if(isset($_POST['cp_auction_my_auction_type'])) $auction_type = $_POST['cp_auction_my_auction_type']; ?>
	<select class="<?php echo $new_class; ?>" name="cp_auction_my_auction_type" id="cp_auction_my_auction_type" tabindex="1"><?php echo cp_auction_select_auction_type($auction_type); ?></select>
</p>
	<?php } ?>
<p>
	<label for="category_parent"><?php _e('Select Category:', 'auctionPlugin'); ?></label>
	<?php $category_parent = ""; if(isset($_POST['category_parent'])) $category_parent = $_POST['category_parent']; ?>
	<?php echo cp_auction_get_cat_dropdown($category_parent, 7); ?>
</p>

	<div class="clear10"></div>

<p class="submit">
<input type="submit" name="view" value="<?php echo $view_listings; ?>" class="btn_orange" />
</p>

        </form>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

                <?php if(isset($_POST['view'])) {

		$cp_auction_category 	= isset( $_POST['category_parent'] ) ? esc_attr( $_POST['category_parent'] ) : '';
		$my_type	 	= isset( $_POST['cp_auction_my_auction_type'] ) ? esc_attr( $_POST['cp_auction_my_auction_type'] ) : '';
		if( empty( $my_type ) ) $my_type = get_option('cp_auction_plugin_auctiontype');

		global $wpdb;

		$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;

		$querystr = "
		SELECT * FROM $wpdb->posts
		LEFT JOIN $wpdb->postmeta m1 ON($wpdb->posts.ID = m1.post_id)
		LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
		LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
		WHERE $wpdb->term_taxonomy.term_id = '$cp_auction_category'
		AND $wpdb->term_taxonomy.taxonomy = '".APP_TAX_CAT."'
		AND (m1.meta_key = 'cp_auction_my_auction_type' AND m1.meta_value = '$my_type')
		AND $wpdb->posts.post_status = 'publish'
		AND $wpdb->posts.post_type = '".APP_POST_TYPE."'
		ORDER BY $wpdb->posts.post_date DESC";

		$rows = $wpdb->get_results($querystr, OBJECT);

		if( $wpdb->num_rows != 0 ) {

   		global $post;
   		foreach ($rows as $post) { 
     		setup_postdata($post);

		$cp_ad_sold = end_auction($post->ID);
		if( $cp_ad_sold != "yes" ) { ?>

		<?php echo cp_auction_loop_ad_listing($post); ?>

		<?php } ?>

		<?php } if(function_exists('appthemes_pagination')) appthemes_pagination(); ?>

	<?php } else { ?>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'Sorry, there are currently no ads listed.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

            </div><!-- /shadowblock_out -->
          
        </div><!-- /shadowblock -->  

	<?php }
	}
}


/*
|--------------------------------------------------------------------------
| SEARCH / LIST ADS BY AD TYPE CONTROLLED BY AUTHOR
|--------------------------------------------------------------------------
*/

function cp_auction_list_by_intention() {
	global $wp_query, $post, $cp_options;

	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$cpa = get_option('cp_auction_plugin_auctiontype');

?>
	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

	<p><?php _e('<strong>Note</strong>: If you do not select a category you will then find all the ads in relation to the type of ad you are searching.', 'auctionPlugin'); ?></p>

<?php
	if( $cpa == "classified" || $cpa == "wanted" || $cpa == "mixed" || $cpa == "reverse" ) {
	$view_listings = ''.__('View Listings', 'auctionPlugin').'';
	} else {
	$view_listings = ''.__('View Auctions', 'auctionPlugin').'';
	} ?>

	<div class="clear5"></div>

	<form class="cp-ecommerce" action="" method="post" id="mainform">

<p>
	<label for="cp_auction_my_intention"><?php _e('Select the type of Ad:', 'auctionPlugin'); ?></label>
	<?php $list_type = ""; if(isset($_POST['cp_auction_my_intention'])) $list_type = $_POST['cp_auction_my_intention']; ?>
	<select class="<?php echo $new_class; ?>" name="cp_auction_my_intention" id="cp_auction_my_intention" tabindex="1"><?php echo cp_auction_select_ad_type($list_type); ?></select>
</p>

<p>
	<label for="category_parent"><?php _e('Select Category (optional):', 'auctionPlugin'); ?></label>
	<?php $category_parent = ""; if(isset($_POST['category_parent'])) $category_parent = $_POST['category_parent']; ?>
	<?php echo cp_auction_get_cat_dropdown($category_parent, 7); ?>
</p>

	<div class="clear10"></div>

<p class="submit">
<input type="submit" name="view" value="<?php echo $view_listings; ?>" class="btn_orange" />
</p>

        </form>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

                <?php if(isset($_POST['view'])) {

		$cp_category 	= isset( $_POST['category_parent'] ) ? esc_attr( $_POST['category_parent'] ) : '';
		$type		= isset( $_POST['cp_auction_my_intention'] ) ? esc_attr( $_POST['cp_auction_my_intention'] ) : '';
		$trans		= get_localized_field_values('cp_iwant_to', $type, 'local');
		if( $cp_category < 1 ) $cp_category = false;

		global $wpdb;

		$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;

		if( ! $cp_category ) {

		$querystr = "
		SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta 
		WHERE wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_iwant_to' AND wpostmeta.meta_value = '$trans' 
		OR wpostmeta.meta_key = 'cp_want_to' AND wpostmeta.meta_value = '$trans') 
		AND wposts.post_status = 'publish' 
		AND wposts.post_type = '".APP_POST_TYPE."' 
		ORDER BY wposts.post_date DESC";

		} else {

		$querystr = "
		SELECT * FROM $wpdb->posts
		LEFT JOIN $wpdb->postmeta m1 ON($wpdb->posts.ID = m1.post_id)
		LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
		LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
		WHERE $wpdb->term_taxonomy.term_id = '$cp_category'
		AND $wpdb->term_taxonomy.taxonomy = '".APP_TAX_CAT."'
		AND (m1.meta_key = 'cp_iwant_to' AND m1.meta_value = '$trans')
		AND $wpdb->posts.post_status = 'publish'
		AND $wpdb->posts.post_type = '".APP_POST_TYPE."'
		ORDER BY $wpdb->posts.post_date DESC";

		}

		$rows = $wpdb->get_results($querystr, OBJECT);

		if( $wpdb->num_rows != 0 ) {

   		global $post;
   		foreach ($rows as $post) { 
     		setup_postdata($post);

		$cp_ad_sold = end_auction($post->ID);
		if( $cp_ad_sold != "yes" ) { ?>

		<?php echo cp_auction_loop_ad_listing($post); ?>

		<?php } ?>

		<?php } if(function_exists('appthemes_pagination')) appthemes_pagination(); ?>

	<?php } else { ?>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'Sorry, there are currently no ads listed.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

            </div><!-- /shadowblock_out -->
          
        </div><!-- /shadowblock -->  

	<?php }
	}
}


/*
|--------------------------------------------------------------------------
| AUCTION LAST CHANGE LISTINGS - SOON TO CLOSE
|--------------------------------------------------------------------------
*/

function cp_auction_soon_to_close() {
	global $wp_query, $post;
	$maxposts = get_option('posts_per_page');

	remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
	$post_type_url = esc_url( add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) ) );
?>

	<?php
	global $wpdb;
	$hours = get_option('cp_auction_plugin_lastchance');
	$todayDate = date_i18n('m/d/Y H.i', strtotime("current_time('mysql')"), true); // current date
	$currentTime = strtotime($todayDate); //Change date into time
	$newtime = date_i18n('m/d/Y H.i', strtotime("$todayDate + $hours"), true);
	$date = strtotime($newtime);

	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'paged' => $paged, 
	'meta_query' => 
		array( 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => array( 'normal', 'reverse' ) 
			), 
			array( 
			'key' => 'cp_end_date', 
			'value' => $date,
			'compare' => '<'
			) 
		) 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

						<?php get_template_part( 'loop', 'ad_listing' ); ?>

						<?php
							if ( $total_pages > 1 ) {
							$closing_url = esc_url( add_query_arg( array( 'sort' => 'closing' ), $post_type_url ) );
						?>
						<div class="paging"><a href="<?php echo $closing_url; ?>"><?php if ( get_option('stylesheet') == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php }
}


/*
|--------------------------------------------------------------------------
| BUY NOW CLASSIFIEDS
|--------------------------------------------------------------------------
*/

function cp_auction_buy_now_classifieds() {
	global $wp_query, $post;
	$maxposts = get_option('posts_per_page');

	remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
	$post_type_url = esc_url( add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) ) );
?>

	<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'paged' => $paged, 
	'meta_query' => 
		array( 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => 'classified' 
			), 
			array( 
			'key' => 'cp_buy_now_enabled', 
			'compare' => 'EXISTS'
			) 
		) 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

			<?php get_template_part( 'loop', 'ad_listing' ); ?>

			<?php
				if ( $total_pages > 1 ) {
				$buynow_url = esc_url( add_query_arg( array( 'sort' => 'buyitnow' ), $post_type_url ) );
			?>
				<div class="paging"><a href="<?php echo $buynow_url; ?>"><?php if ( get_option('stylesheet') == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
			<?php }
}


/*
|--------------------------------------------------------------------------
| BUY NOW AUCTIONS
|--------------------------------------------------------------------------
*/

function cp_auction_buy_now_auctions() {
	global $wp_query, $post;
	$maxposts = get_option('posts_per_page');

	remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
	$post_type_url = esc_url( add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) ) );
?>

	<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'paged' => $paged, 
	'meta_query' => 
		array( 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => 'normal', 'reverse' 
			), 
			array( 
			'key' => 'cp_buy_now', 
			'value' => 0, 
			'compare' => '>'
			) 
		) 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

			<?php get_template_part( 'loop', 'ad_listing' ); ?>

			<?php
				if ( $total_pages > 1 ) {
				$buynow_url = esc_url( add_query_arg( array( 'sort' => 'buynow' ), $post_type_url ) );
			?>
				<div class="paging"><a href="<?php echo $buynow_url; ?>"><?php if ( get_option('stylesheet') == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
			<?php }
}