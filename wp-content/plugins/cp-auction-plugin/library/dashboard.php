<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $cp_options, $post, $cpurl, $wpdb;
	$current_user = wp_get_current_user(); // grabs the user info and puts into vars
	$display_user_name = cp_get_user_name();
	$maxposts = get_option('posts_per_page');
	$apage = get_option( 'cp_auction_author_page' );

	$uid = $current_user->ID;
	$user = get_userdata($uid);
	$cc = $cp_options->currency_code;
	$ct = get_option('stylesheet');
	$ip_address = cp_auction_users_ip();
	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}

	$pid = ""; if(isset( $_GET['pid'] )) $pid = $_GET['pid'];
	if( $pid ) $post = get_post($pid);
	$howto_pay = ""; if(isset( $_GET['howto_pay'] )) $howto_pay = $_GET['howto_pay'];
	$counter = ""; if(isset($_GET['counter'])) $counter = $_GET['counter'];
	if(isset($_GET['ads'])) $info = "".__('Sorry, you cannot access this page until you have posted any ads!','auctionPlugin')."";
	if(isset($_GET['pay'])) $info = "".__('Sorry, you can not access this page until we have verified your purchase!','auctionPlugin')."";
	$empty = false; $bo_error = false; $bo_success = false; $mssg = false;

	$ok = "";
	$cp_your_credits = get_user_meta( $uid, 'cp_credits', true );
	if($cp_your_credits == "") $cp_your_credits = 0;
	$verified = get_user_meta( $uid, 'cp_auction_account_verified', true );
	if(( $verified == false ) && ( current_user_can( 'manage_options' ) ) )
	update_user_meta( $uid, 'cp_auction_account_verified', 1 );

	if( $apage == "apage" ) $webshop_url = get_author_posts_url($uid);
	else $webshop_url = cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$uid.'');

	$pp_pdt = get_user_meta( $uid, 'cp_auction_plugin_pp_pdt', true );
	$pp_url = "https://www.paypal.com";

	$exp_time = get_option('aws_bo_expiration');
	$enabl = get_option( 'cp_auction_enable_conversion' );
	$cuto = get_option( 'cp_auction_convert_to' );

	if(isset($_POST['uid_counter_offer'])) {

		$ok = 1;
		if(isset($_POST['id'])) $id = $_POST['id'];
		if(isset($_POST['counter_offer_amount'])) $bid = cp_auction_sanitize_amount($_POST['counter_offer_amount']);
		$new_msg = false; if(isset($_POST['message'])) $new_msg = $_POST['message'];
		if( $new_msg ) $mssg = '<br /><br /><strong>'.__('Message from bidder:','auctionPlugin').'</strong><br />'.$new_msg.'';

		$tm = time();
		$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
		$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
		$pid = $get->pid;
		$cid = $get->uid;
		$uid_usage = $get->uid_usage;
		$upd_usage = $uid_usage + 1;
		$post = get_post($pid);

		if( (!is_numeric($bid)) || ( empty( $bid ) ) ) {
		$ok = 2;
		}
		if( $uid_usage >= get_option('aws_bo_no_user_counter_offer' )) {
		$ok = 3;
		}
		if( $ok == 2 ) {
		$bo_error = ''.__('Your counter offer needs to be a numeric value!','auctionPlugin').'';
		}
		if( $ok == 3 ) {
		$bo_error = ''.__('Sorry, you do not have permission to post more counter offers!','auctionPlugin').'';
		}
		if( $ok == 1 ) {
		$message = ''.$get->message.''.$mssg.'';
		$where_array = array('id' => $id);
		$wpdb->update($table_name, array('uid_counter_offer' => $bid, 'message' => stripslashes(html_entity_decode($message)), 'uid_usage' => $upd_usage, 'uid_date' => $tm, 'latest_offer' => $bid, 'date_type' => 'uid_date'), $where_array);

		$bo_success = ''.__('Your counter offer was successfully delivered!','auctionPlugin').'';
		aws_bo_uid_counter_offer_posted_email( $pid, $post, $cid, $bid, $new_msg );
		}
		else {
		$bo_success = ''.__('Sorry, something went wrong!','auctionPlugin').'';
		}
	}

		if(isset($_POST['aut_counter_offer'])) {

		$ok = 1;
		if(isset($_POST['id'])) $id = $_POST['id'];
		if(isset($_POST['counter_offer_amount'])) $bid = cp_auction_sanitize_amount($_POST['counter_offer_amount']);
		$new_msg = false; if(isset($_POST['message'])) $new_msg = $_POST['message'];
		if( $new_msg ) $mssg = '<br /><br /><strong>'.__('Message from seller:','auctionPlugin').'</strong><br />'.$new_msg.'';

		$tm = time();
		$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
		$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
		$pid = $get->pid;
		$cid = $get->uid;
		$aut_usage = $get->aut_usage;
		$upd_usage = $aut_usage + 1;
		$post = get_post($pid);

		if( (!is_numeric($bid)) || ( empty( $bid ) ) ) {
		$ok = 2;
		}
		if( $aut_usage >= get_option('aws_bo_no_author_counter_offer' )) {
		$ok = 3;
		}
		if( $ok == 2 ) {
		$bo_error = ''.__('Your counter offer needs to be a numeric value!','auctionPlugin').'';
		}
		if( $ok == 3 ) {
		$bo_error = ''.__('Sorry, you do not have permission to post more counter offers!','auctionPlugin').'';
		}
		if( $ok == 1 ) {
		$message = ''.$get->message.''.$mssg.'';
		$where_array = array('id' => $id);
		$wpdb->update($table_name, array('aut_counter_offer' => $bid, 'message' => stripslashes(html_entity_decode($message)), 'aut_usage' => $upd_usage, 'aut_date' => $tm, 'latest_offer' => $bid, 'date_type' => 'aut_date'), $where_array);

		$bo_success = ''.__('Your counter offer was successfully delivered!','auctionPlugin').'';
		aws_bo_aut_counter_offer_posted_email( $pid, $post, $cid, $bid, $new_msg );
		}
		else {
		$bo_error = ''.__('Sorry, something went wrong!','auctionPlugin').'';
		}
	}

	if( isset( $_POST['save_settings'] ) ) {
		$ok = 1;
		$accept_cih = isset( $_POST['accept_cih'] ) ? esc_attr( $_POST['accept_cih'] ) : '';
		$accept_cod = isset( $_POST['accept_cod'] ) ? esc_attr( $_POST['accept_cod'] ) : '';
		$accept_bt = isset( $_POST['accept_bt'] ) ? esc_attr( $_POST['accept_bt'] ) : '';
		$accept_credits = isset( $_POST['accept_credits'] ) ? esc_attr( $_POST['accept_credits'] ) : '';
		$paypal_email = isset( $_POST['paypal_email'] ) ? esc_attr( $_POST['paypal_email'] ) : '';
		$currency = isset( $_POST['currency'] ) ? esc_attr( $_POST['currency'] ) : '';
		$symbol = isset( $_POST['cp_currency'] ) ? esc_attr( $_POST['cp_currency'] ) : '';
		$paypal_lang = isset( $_POST['paypal_lang'] ) ? esc_attr( $_POST['paypal_lang'] ) : '';
		$tax_vat = isset( $_POST['tax_vat'] ) ? esc_attr( $_POST['tax_vat'] ) : '';
		$cod_fees = isset( $_POST['cp_auction_cod_fees'] ) ? esc_attr( $_POST['cp_auction_cod_fees'] ) : '';
		$bt_fees = isset( $_POST['cp_auction_bank_transfer_fees'] ) ? esc_attr( $_POST['cp_auction_bank_transfer_fees'] ) : '';
		$bt_info = isset( $_POST['cp_auction_bank_transfer_info'] ) ? esc_attr( $_POST['cp_auction_bank_transfer_info'] ) : '';
		$credit_fees = isset( $_POST['cp_auction_credit_fees'] ) ? esc_attr( $_POST['cp_auction_credit_fees'] ) : '';
		$paypal_fees = isset( $_POST['cp_auction_paypal_fees'] ) ? esc_attr( $_POST['cp_auction_paypal_fees'] ) : '';
		$anonymous = isset( $_POST['cp_allow_anonymous'] ) ? esc_attr( $_POST['cp_allow_anonymous'] ) : '';
		$safe_payment = isset( $_POST['cp_use_safe_payment'] ) ? esc_attr( $_POST['cp_use_safe_payment'] ) : '';
		$allow_pms = isset( $_POST['cp_auction_allow_pms'] ) ? esc_attr( $_POST['cp_auction_allow_pms'] ) : '';
		$enable_shipping = isset( $_POST['cp_auction_enable_shipping'] ) ? esc_attr( $_POST['cp_auction_enable_shipping'] ) : '';
		$shipping_charge = isset( $_POST['cp_auction_shipping'] ) ? esc_attr( $_POST['cp_auction_shipping'] ) : '';
		$free_shippinga = isset( $_POST['cp_auction_free_shipping'] ) ? esc_attr( $_POST['cp_auction_free_shipping'] ) : '';
		$shipping_cost = isset( $_POST['shipto'] ) ? esc_attr( $_POST['shipto'] ) : '';
		$additional_costa = isset( $_POST['cp_auction_add_shipping'] ) ? esc_attr( $_POST['cp_auction_add_shipping'] ) : '';
		$max_shipping_costa = isset( $_POST['cp_auction_max_shipping'] ) ? esc_attr( $_POST['cp_auction_max_shipping'] ) : '';
		$free_shippingb = isset( $_POST['cp_auction_free_shipping1'] ) ? esc_attr( $_POST['cp_auction_free_shipping1'] ) : '';
		$additional_costb = isset( $_POST['cp_auction_add_shipping1'] ) ? esc_attr( $_POST['cp_auction_add_shipping1'] ) : '';
		$max_shipping_costb = isset( $_POST['cp_auction_max_shipping1'] ) ? esc_attr( $_POST['cp_auction_max_shipping1'] ) : '';
		$info_top = isset( $_POST['cp_auction_info_top'] ) ? esc_attr( $_POST['cp_auction_info_top'] ) : '';
		$info_bottom = isset( $_POST['cp_auction_info_bottom'] ) ? esc_attr( $_POST['cp_auction_info_bottom'] ) : '';

		if ( empty( $free_shippinga ) || empty( $free_shippingb ) ) $free_shipping = ($free_shippinga + $free_shippingb);
		else $free_shipping = isset( $_POST['cp_auction_free_shipping'] ) ? esc_attr( $_POST['cp_auction_free_shipping'] ) : esc_attr( $_POST['cp_auction_free_shipping1'] );
		if ( empty( $additional_costa ) || empty( $additional_costb ) ) $additional_cost = ($additional_costa + $additional_costb);
		else $additional_cost = isset( $_POST['cp_auction_add_shipping'] ) ? esc_attr( $_POST['cp_auction_add_shipping'] ) : esc_attr( $_POST['cp_auction_add_shipping1'] );
		if ( empty( $max_shipping_costa ) || empty( $max_shipping_costb ) ) $max_shipping_cost = ($max_shipping_costa + $max_shipping_costb);
		else $max_shipping_cost = isset( $_POST['cp_auction_max_shipping'] ) ? esc_attr( $_POST['cp_auction_max_shipping'] ) : esc_attr( $_POST['cp_auction_max_shipping1'] );

		if( $tax_vat && !is_numeric($tax_vat) ) $ok = 2;
		if( $cod_fees && !is_numeric($cod_fees) ) $ok = '3.5';
		if( $bt_fees && !is_numeric($bt_fees) ) $ok = '3.6';
		if( $credit_fees && !is_numeric($credit_fees) ) $ok = 3;
		if( $paypal_fees && !is_numeric($paypal_fees) ) $ok = 4;
		//if( $shipping_cost && !is_numeric($shipping_cost) ) $ok = 5;
		if( $additional_cost && !is_numeric($additional_cost) ) $ok = 6;
		if( $max_shipping_cost && !is_numeric($max_shipping_cost) ) $ok = 7;

		if( $ok == 1 ) {

		if( $accept_cih == "yes" ) {
		update_user_meta( $uid, 'cp_accept_cih_payment', $accept_cih );
		} else {
		delete_user_meta( $uid, 'cp_accept_cih_payment' );
		}
		if( $accept_cod == "yes" ) {
		update_user_meta( $uid, 'cp_accept_cod_payment', $accept_cod );
		} else {
		delete_user_meta( $uid, 'cp_accept_cod_payment' );
		}
		if( $accept_bt == "yes" ) {
		update_user_meta( $uid, 'cp_accept_bank_transfer_payment', $accept_bt );
		} else {
		delete_user_meta( $uid, 'cp_accept_bank_transfer_payment' );
		}
		if( $accept_credits == "yes" ) {
		update_user_meta( $uid, 'cp_accept_credit_payment', $accept_credits );
		} else {
		delete_user_meta( $uid, 'cp_accept_credit_payment' );
		}
		if( $paypal_email ) {
		update_user_meta( $uid, 'paypal_email', $paypal_email );
		} else {
		delete_user_meta( $uid, 'paypal_email' );
		}
		if( $currency ) {
		update_user_meta( $uid, 'currency', $currency );
		} else {
		delete_user_meta( $uid, 'currency' );
		}
		if( $symbol ) {
		update_user_meta( $uid, 'cp_currency', $symbol );
		} else {
		delete_user_meta( $uid, 'cp_currency' );
		}
		if( $paypal_lang ) {
		update_user_meta( $uid, 'paypal_lang', $paypal_lang );
		} else {
		delete_user_meta( $uid, 'paypal_lang' );
		}
		if( $tax_vat ) {
		update_user_meta( $uid, 'cp_auction_tax_vat', cp_auction_sanitize_amount($tax_vat) );
		} else {
		delete_user_meta( $uid, 'cp_auction_tax_vat' );
		}
		if( $cod_fees ) {
		update_user_meta( $uid, 'cp_auction_cod_fees', cp_auction_sanitize_amount($cod_fees) );
		} else {
		delete_user_meta( $uid, 'cp_auction_cod_fees' );
		}
		if( $bt_fees ) {
		update_user_meta( $uid, 'cp_auction_bank_transfer_fees', cp_auction_sanitize_amount($bt_fees) );
		} else {
		delete_user_meta( $uid, 'cp_auction_bank_transfer_fees' );
		}
		if( $bt_info ) {
		update_user_meta( $uid, 'cp_auction_bank_transfer_info', stripslashes( html_entity_decode( $bt_info ) ) );
		} else {
		delete_user_meta( $uid, 'cp_auction_bank_transfer_info' );
		}
		if( $credit_fees ) {
		update_user_meta( $uid, 'cp_auction_credit_fees', cp_auction_sanitize_amount($credit_fees) );
		} else {
		delete_user_meta( $uid, 'cp_auction_credit_fees' );
		}
		if( $paypal_fees ) {
		update_user_meta( $uid, 'cp_auction_paypal_fees', cp_auction_sanitize_amount($paypal_fees) );
		} else {
		delete_user_meta( $uid, 'cp_auction_paypal_fees' );
		}
		if( $anonymous == "yes" ) {
		update_user_meta( $uid, 'cp_allow_anonymous', $anonymous );
		} else {
		delete_user_meta( $uid, 'cp_allow_anonymous' );
		}
		if( $safe_payment == "yes" ) {
		update_user_meta( $uid, 'cp_use_safe_payment', $safe_payment );
		} else if( $safe_payment == "no" ) {
		update_user_meta( $uid, 'cp_use_safe_payment', $safe_payment );
		} else {
		delete_user_meta( $uid, 'cp_use_safe_payment' );
		}
		if( $allow_pms == "yes" ) {
		update_user_meta( $uid, 'cp_auction_allow_pms', $allow_pms );
		} else {
		delete_user_meta( $uid, 'cp_auction_allow_pms' );
		}
		if( $enable_shipping == "yes" ) {
		update_user_meta( $uid, 'cp_auction_enable_shipping', $enable_shipping );
		} else {
		delete_user_meta( $uid, 'cp_auction_enable_shipping' );
		}
		if( $shipping_charge ) {
		update_user_meta( $uid, 'cp_auction_shipping', $shipping_charge );
		} else {
		delete_user_meta( $uid, 'cp_auction_shipping' );
		}
		if( $free_shipping ) {
		update_user_meta( $uid, 'cp_auction_free_shipping', $free_shipping );
		} else {
		delete_user_meta( $uid, 'cp_auction_free_shipping' );
		}
		if( $shipping_cost ) {
		$option = array();
		//delete_user_meta( $uid, 'shipto' );
		foreach ( $_POST['shipto'] as $item => $value ) {
		$option[$item] = $value;
		if ( ! empty( $option ) )
		update_user_meta( $uid, 'shipto', $option );
		}
		}
		if( $additional_cost ) {
		update_user_meta( $uid, 'cp_auction_add_shipping', $additional_cost );
		} else {
		delete_user_meta( $uid, 'cp_auction_add_shipping' );
		}
		if( $max_shipping_cost ) {
		update_user_meta( $uid, 'cp_auction_max_shipping', $max_shipping_cost );
		} else {
		delete_user_meta( $uid, 'cp_auction_max_shipping' );
		}
		if( $info_top ) {
		update_user_meta( $uid, 'cp_auction_info_top', stripslashes( html_entity_decode( $info_top ) ) );
		} else {
		delete_user_meta( $uid, 'cp_auction_info_top' );
		}
		if( $info_bottom ) {
		update_user_meta( $uid, 'cp_auction_info_bottom', stripslashes( html_entity_decode( $info_bottom ) ) );
		} else {
		delete_user_meta( $uid, 'cp_auction_info_bottom' );
		}
		$info = "".__('Your settings was saved.','auctionPlugin')."";
	}
}

	if( isset( $_POST['change_mode'] ) ) {
		if( $_POST['mode'] == "pro" )
		update_user_meta( $uid, 'cp_author_payment_mode', 'simple' );
		elseif( $_POST['mode'] == "simple" )
		update_user_meta( $uid, 'cp_author_payment_mode', 'pro' );

		$info = "".__('Your payment mode was updated!','auctionPlugin')."";
	}
	if( isset( $_POST['simple_mode'] ) ) {
		update_user_meta( $uid, 'cp_author_payment_mode', 'simple' );
		$info = "".__('Your payment mode was updated!','auctionPlugin')."";
	}

	if( isset( $_POST['activate_pdt'] ) ) {
		if( empty( $pp_pdt ) ) {
		update_user_meta( $uid, 'cp_auction_plugin_pp_pdt', 1 );
		update_user_meta( $uid, 'cp_auction_plugin_identity_token', $_POST['identity_token'] );
		if ( empty( $_POST['identity_token'] ) ) $msg = "".__('Please enter your identity token.','auctionPlugin')."";
		else $info = "".__('Your PDT was successfully activated.','auctionPlugin')."";
		} else {
		delete_user_meta( $uid, 'cp_auction_plugin_pp_pdt' );
		update_user_meta( $uid, 'cp_auction_plugin_identity_token', '' );
		$info = "".__('Your PDT was successfully deactivated.','auctionPlugin')."";
		}

	}
	$status_pdt = get_user_meta( $uid, 'cp_auction_plugin_pp_pdt', true );
	$identity_token = get_user_meta( $uid, 'cp_auction_plugin_identity_token', true );
	$pp_required = get_option('cp_auction_disable_paypal');

	global $wpdb;
	if (class_exists("cartpaujPM")) {
	$table_name = $wpdb->prefix . "cartpauj_pm_messages";
	$get_pms = $wpdb->get_results($wpdb->prepare("SELECT id FROM {$table_name} WHERE (to_user = %d AND parent_id = 0 AND to_del <> 1 AND message_read = 0 AND last_sender <> %d) OR (from_user = %d AND parent_id = 0 AND from_del <> 1 AND message_read = 0 AND last_sender <> %d)", $uid, $uid, $uid, $uid));
	$pms = $wpdb->num_rows;
	}

	$payment_mode = get_option( 'cp_auction_payment_mode' );
	$upayment_mode = get_option( 'cp_auction_users_payment_mode' );
	if( $upayment_mode == false ) delete_user_meta( $uid, 'cp_author_payment_mode' );
	$mode = get_user_meta( $uid, 'cp_author_payment_mode', true );
	if( ! $mode && $payment_mode != "simple" ) $mode = "pro";
	$btn_mode = __('Enable Advanced Mode', 'auctionPlugin');
	if( $mode == "pro" ) $btn_mode = __('Enable Simple Mode', 'auctionPlugin');
	if( $mode == "simple" ) $btn_mode = __('Enable Advanced Mode', 'auctionPlugin');

	if(( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( ! $mode || $mode == "simple" )) {
	$option = get_option('cp_auction_simple_settings');
	$opt = explode(',', $option);
	} else {
	$opt = false;
	}

?>

<?php cp_auction_header(); ?>

<div class="shadowblock_out" id="noise">

	<div class="shadowblock">

		<div class="aws-box">

	<h2 class="dotted"><?php echo the_title(); ?></h2>

	<?php if(isset($info)) { ?>
	<div class="notice success"><?php echo "".$info.""; ?></div>
	<?php } ?>
	<?php if(isset($msg)) { ?>
	<div class="notice error"><?php echo "".$msg.""; ?></div>
	<?php } ?>

	<?php if( get_option('cp_auction_plugin_actfee') > 0 ) { ?>
	<div class="price_info">
		<div class="price_info1"><?php _e('Administrative Fee:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><?php echo get_option('cp_auction_plugin_actfee'); ?> % <?php _e('deducted from the sale price.', 'auctionPlugin'); ?></div>
	</div>

       	<?php } if( get_option('cp_auction_paypal_fee') > 0 ) { ?>
	<div class="price_info">
		<div class="price_info1"><?php _e('Fee for PayPal Payment:', 'auctionPlugin'); ?> </div>
		<?php if( get_option('cp_auction_paypal_fee_type') == "percentage_on" ) { ?>
		<div class="price_info2"><?php echo cp_auction_format_amount(get_option('cp_auction_paypal_fee'), 'process'); ?> %</div>
		<?php } else { ?>
		<div class="price_info2"><?php echo cp_display_price( get_option('cp_auction_paypal_fee'), 'ad', false ); ?></div>
		<?php } ?>
	</div>

	<?php } if( get_option('cp_auction_plugin_bank_transfer') == "yes" && get_option('cp_auction_plugin_bt_fee') > 0 ) { ?>
	<div class="price_info">
		<div class="price_info1"><?php _e('Fee for Bank Transfer:', 'auctionPlugin'); ?> </div>
		<?php if( get_option('cp_auction_banktransfer_fee_type') == "percentage_on" ) {?>
		<div class="price_info2"><?php echo cp_auction_format_amount(get_option('cp_auction_plugin_bt_fee'), 'process'); ?> %</div>
		<?php } else { ?>
		<div class="price_info2"><?php echo cp_display_price( get_option('cp_auction_plugin_bt_fee'), 'ad', false ); ?></div>
		<?php } ?>
	</div>

	<?php } if( get_option('cp_auction_plugin_credits') == "yes" && get_option('cp_auction_plugin_credits_fee') > 0 ) { ?>
	<div class="price_info">
		<div class="price_info1"><?php _e('Fee for Credit Payment:', 'auctionPlugin'); ?> </div>
		<?php if( get_option('cp_auction_credits_fee_type') == "percentage_on" ) { ?>
		<div class="price_info2"><?php echo cp_auction_format_amount(get_option('cp_auction_plugin_credits_fee'), 'process'); ?> %</div>
		<?php } else { ?>
		<div class="price_info2"><?php echo cp_display_price( get_option('cp_auction_plugin_credits_fee'), 'ad', false ); ?></div>
		<?php } ?>
	</div>

	<?php } if(( get_option('cp_auction_display_max_duration') == "yes" && $cp_options->prun_period > 0 ) && ( get_option('cp_auction_plugin_auctiontype') == "clasified" || get_option('cp_auction_plugin_auctiontype') == "wanted" || get_option('cp_auction_plugin_auctiontype') == "mixed" )) { ?>            
	<div class="price_info">
		<div class="price_info1"><?php _e('Max Duration in Days:', 'auctionPlugin'); ?> </div>
		<?php if( $cp_options->prun_period > '365' ) { ?>
		<div class="price_info2"><?php _e('Unlimited', 'auctionPlugin'); ?></div>
		<?php } else { ?>
		<div class="price_info2"><?php echo $cp_options->prun_period; ?> <?php _e('days', 'auctionPlugin'); ?></div>
		<?php } ?>
	</div>

	<?php } if( get_option('cp_auction_plugin_admins_only') != "yes" || current_user_can( 'manage_options' ) ) { ?>

	<div class="price_info">
		<div class="price_info1"><?php _e('Your Webshop URL:', 'auctionPlugin'); ?></div>
		<div class="price_info2"><a href="<?php echo $webshop_url; ?>" target="_blank"><?php echo $webshop_url; ?></a></div>
	</div>

      	<?php } if(( get_option('cp_auction_plugin_verification') > 0) && ( $verified != 1 )) { ?>
	<?php if( $verified == 2 ) { ?>
	<div class="price_info">
		<div class="price_info1"><?php _e('Verification Status:', 'auctionPlugin'); ?> </div>
		<div class="price_info2"><font color="red"><strong><?php _e('Not Approved', 'auctionPlugin'); ?></strong></font></div>
	</div>

	<?php }
	    else {

	echo '<div class="clear10"></div>';
	echo '<div class="page_info">'.nl2br(get_option('cp_auction_plugin_verification_info')).'</div>';

	$pp = get_option('cp_auction_plugin_paypalemail');
	if( empty( $pp ) ) $pp = $cp_options->gateways['paypal']['email_address']; // Use classipress email.

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;

	$conversion = false;
	if ( $enabl == "yes" && class_exists("Currencyr") )
	$conversion = currencyr_exchange( get_option('cp_auction_plugin_verification'), strtolower( $cuto ) );

	$pp_arg = array(
	'uid' => $uid,
	'ip_address' => $ip_address,
	'payment_date' => time(),
	'datemade' => time(),
	'item_name' => 'Verification',
	'trans_type' => 'Verification',
	'mc_currency' => $cc,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'receiver_email' => $pp,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'mc_fee' => false,
	'conversion' => cp_auction_sanitize_amount($conversion),
	'mc_gross' => cp_auction_sanitize_amount(get_option('cp_auction_plugin_verification')),
	'howto_pay' => 'paypal',
	'type' => 'verification'
	);
	echo '<form class="cp-ecommerce" action="'.get_bloginfo('wpurl').'/?_process_payment=1" method="post" id="mainform" enctype="multipart/form-data">';
	// PayPal Payment
	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $pp_arg ) ).'">';

	if( get_option('cp_auction_plugin_button') == 1 ) {
	echo '<div class="small_button"><input type="image" src="http://images.paypal.com/images/x-click-but1.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"></div>';
	} elseif( get_option('cp_auction_plugin_button') == 2 ) {
	echo '<div class="small_button"><input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!"></div>';
	} else {
	echo '<div class="small_button"><input type="submit" name="submit" value="'.__('Pay with PayPal', 'auctionPlugin').'" class="btn_orange" /></div>';
	}
	echo '</form>';
	// End PayPal Payment
	}

} elseif(( get_option('cp_auction_plugin_verification') > 0 ) && ( $verified == 1 )) { ?>
	<div class="price_info">
		<div class="price_info1"><?php _e('Verification Status:', 'auctionPlugin'); ?></div>
		<div class="price_info2"><font color="green"><strong><?php _e('Approved', 'auctionPlugin'); ?></strong></font></div>
	</div>
<?php } ?>

	<?php if( get_option('cp_auction_dashboard_page_info') ) {
	echo '<div class="clear10"></div>';
	echo '<div class="page_info">'.nl2br(get_option('cp_auction_dashboard_page_info')).'</div>';
	} ?>

	<div class="clr"></div>

	<?php do_action( 'appthemes_notices' ); ?>

	<div class="clr"></div>

			</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<?php if( $ct == "eclassify" ) {
	$tabs = "tabnavig"; ?>

	<div class="tabcontrol">

		<ul class="tabnavig">
<?php } else {
	$tabs = "tabmenu"; ?>

	<div class="tabhome">

		<ul class="tabmenu">
<?php } ?>

	<?php $t = 0; ?>
	<?php if( get_option('cp_auction_plugin_admins_only') != "yes" && get_option('cp_auction_disable_settings') != "yes" || current_user_can( 'manage_options' ) ) { ?>
	<?php $a = $t++; ?>
	<li><a href="#acc-settings"><span class="big"><?php _e('Settings', 'auctionPlugin'); ?></span></a></li>
	<?php } if( get_option('cp_auction_plugin_admins_only') != "yes" || current_user_can( 'manage_options' ) ) { ?>
	<?php $b = $t++; ?>
	<li><a href="#sold"><span class="big"><?php _e('Sold', 'auctionPlugin'); ?></span></a></li>
	<?php } ?>
	<?php $c = $t++; ?>
	<li><a href="#won"><span class="big"><?php _e('Bought', 'auctionPlugin'); ?></span></a></li>
	<?php if( get_option('cp_auction_plugin_wanted') == true || get_option('cp_auction_enable_classified_wanted') == "yes" ) { ?>
	<?php $d = $t++; ?>
	<li><a href="#wanted"><span class="big"><?php _e('Wanted Offers', 'auctionPlugin'); ?></span></a></li>
	<?php } if( get_option( 'aws_bo_enable_offer' ) == "yes" ) { ?>
	<?php $e = $t++; ?>
	<li><a href="#boffer"><span class="big"><?php _e('Best Offers', 'auctionPlugin'); ?></span></a></li>
	<?php } if(( get_option('cp_auction_plugin_auctiontype') == "normal" || get_option('cp_auction_plugin_auctiontype') == "reverse" ) 
	|| ( get_option('cp_auction_plugin_auctiontype') == "mixed" && ( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true ))) { ?>
	<?php $f = $t++; ?>
	<li><a href="#bid"><span class="big"><?php _e('Bids', 'auctionPlugin'); ?></span></a></li>
	<?php } if (class_exists("cartpaujPM")) { ?>
	<?php $g = $t++; ?>
	<li><a href="#pms"><span class="big"><?php _e('PMs', 'auctionPlugin'); ?> (<?php echo $pms; ?>)</span></a></li>
	<?php } ?>
	</ul>

<?php if( $ct == "jibo" || get_option( 'cp_auction_slide_in_ads' ) == "yes" ) { ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
	var vars = [], hash;
	var q = document.URL.split('?')[1];
	if(q != undefined){
        q = q.split('&');
        for(var i = 0; i < q.length; i++){
	hash = q[i].split('=');
	vars.push(hash[1]);
	vars[hash[0]] = hash[1];
        }
}
	if($.cookie('homeTab') == undefined && vars['to'] == false) { 
    	$.cookie('homeTab', '0', {expires:365, path:'/'});
	}
	else if(vars['to'] > 0) {
	$.cookie('homeTab', '<?php if(isset($g)) echo $g; ?>', {expires:365, path:'/'});
	}
	else if(vars['bid'] > 0) {
	$.cookie('homeTab', '<?php if(isset($f)) echo $f; ?>', {expires:365, path:'/'});
	}
	else if(vars['boffer'] > 0) {
	$.cookie('homeTab', '<?php if(isset($e)) echo $e; ?>', {expires:365, path:'/'});
	}
	else if(vars['wanted'] > 0) {
	$.cookie('homeTab', '<?php if(isset($d)) echo $d; ?>', {expires:365, path:'/'});
	}
	else if(vars['won'] > 0) {
	$.cookie('homeTab', '<?php if(isset($c)) echo $c; ?>', {expires:365, path:'/'});
	}
	else if(vars['sold'] > 0) {
	$.cookie('homeTab', '<?php if(isset($b)) echo $b; ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({ 
	activate: function (e, ui) { 
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' }); 
	},
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
	active: $.cookie('homeTab')
});
});
</script>

<?php } else { ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
	var vars = [], hash;
	var q = document.URL.split('?')[1];
	if(q != undefined){
        q = q.split('&');
        for(var i = 0; i < q.length; i++){
	hash = q[i].split('=');
	vars.push(hash[1]);
	vars[hash[0]] = hash[1];
        }
}
	if($.cookie('homeTab') == undefined && vars['to'] == false) { 
    	$.cookie('homeTab', '0', {expires:365, path:'/'});
	}
	else if(vars['to'] > 0) {
	$.cookie('homeTab', '<?php if(isset($g)) echo $g; ?>', {expires:365, path:'/'});
	}
	else if(vars['bid'] > 0) {
	$.cookie('homeTab', '<?php if(isset($f)) echo $f; ?>', {expires:365, path:'/'});
	}
	else if(vars['boffer'] > 0) {
	$.cookie('homeTab', '<?php if(isset($e)) echo $e; ?>', {expires:365, path:'/'});
	}
	else if(vars['wanted'] > 0) {
	$.cookie('homeTab', '<?php if(isset($d)) echo $d; ?>', {expires:365, path:'/'});
	}
	else if(vars['won'] > 0) {
	$.cookie('homeTab', '<?php if(isset($c)) echo $c; ?>', {expires:365, path:'/'});
	}
	else if(vars['sold'] > 0) {
	$.cookie('homeTab', '<?php if(isset($b)) echo $b; ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({ 
	activate: function (e, ui) { 
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' }); 
	},
	active: $.cookie('homeTab')
});
});
</script>

<?php } ?>

<script type="text/javascript">
jQuery('ul.<?php echo $tabs; ?> a').click(function() {
var URL = window.location.href;
var NewURL = URL.split('?')[0];
window.history.pushState('', '', NewURL);
});
</script>

<script type="text/javascript">
jQuery('ul.<?php echo $tabs; ?> a').click(function() {
var URL = window.location.href;
var NewURL = URL.split('page')[0];
window.history.pushState('', '', NewURL);
if ( NewURL != URL ) {
window.location = window.location.href;
}
});
</script>

<script type="text/javascript">
function ValidateME()
{    
if (document.getElementById("credits").value == "")
{
alert("<?php _e('Please enter the number of credits!', 'auctionPlugin'); ?>");  // Give alert to user
           document.getElementById("credits").focus(); // Set focus on textbox
 return false;
}

}
</script>

<?php if( get_option('cp_auction_plugin_admins_only') != "yes" && get_option('cp_auction_disable_settings') != "yes" || current_user_can( 'manage_options' ) ) { ?>

    <div id="acc-settings">

	<div class="clr"></div>

	<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('Account Settings', 'auctionPlugin'); ?></span></strong></span></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

<?php if(( get_option('cp_auction_disable_settings') != "yes" && get_option('cp_auction_plugin_admins_only') != "yes" || current_user_can( 'manage_options' ) ) 
&& ( get_option('cp_auction_plugin_pay_admin') != "yes" || get_option('cp_auction_plugin_pay_user') != "no" || current_user_can( 'manage_options' ) )) { ?>

      	    <div class="clear10"></div>

<?php if( $ok == 2 ) echo '<div class="error">'.__('Please enter a numeric tax charge!', 'auctionPlugin').'</div><div class="clear10"></div>'; ?>
<?php if( $ok == 3 ) echo '<div class="error">'.__('Please enter a numeric credit extra fee!', 'auctionPlugin').'</div><div class="clear10"></div>'; ?>
<?php if( $ok == '3.5' ) echo '<div class="error">'.__('Please enter a numeric COD extra fee!', 'auctionPlugin').'</div><div class="clear10"></div>'; ?>
<?php if( $ok == '3.6' ) echo '<div class="error">'.__('Please enter a numeric Bank Transfer extra fee!', 'auctionPlugin').'</div><div class="clear10"></div>'; ?>
<?php if( $ok == 4 ) echo '<div class="error">'.__('Please enter a numeric paypal extra fee!', 'auctionPlugin').'</div><div class="clear10"></div>'; ?>
<?php if( $ok == 5 ) echo '<div class="error">'.__('Please enter a numeric shipping cost!', 'auctionPlugin').'</div><div class="clear10"></div>'; ?>
<?php if( $ok == 6 ) echo '<div class="error">'.__('Please enter a numeric additional cost percentage!', 'auctionPlugin').'</div><div class="clear10"></div>'; ?>
<?php if( $ok == 7 ) echo '<div class="error">'.__('Please enter a numeric max. shipping cost!', 'auctionPlugin').'</div><div class="clear10"></div>'; ?>

<form class="cp-ecommerce" action="" method="post" id="mainform">

<?php if( $upayment_mode == "yes" ) { ?>

<p><?php _e('Depending on how familiar you are with PayPal settings and whether you want full control of your sales so you can here select the payment mode you would feel most comfortable with.', 'auctionPlugin'); ?></p>

<?php if( $payment_mode == "simple" || $mode == "simple" ) { ?>
<p><?php _e('Whatever you choose, we will strongly advise you to set up Autoreturn and / or activating Payment Data Transfer (PDT) with PayPal. If you do not use any of these PayPal settings so we recommend that you <strong>DO NOT</strong> turn on automatic payment approval.', 'auctionPlugin'); ?></p>
<?php } ?>

	<div class="clear10"></div>

<p class="submit">
	<input type="hidden" name="mode" value="<?php echo $mode; ?>">
	<input tabindex="1" class="mbtn btn_orange" name="change_mode" id="change_mode" value="<?php echo $btn_mode; ?>" type="submit">
</p>

	<div class="clear20"></div>

<?php } if(( $payment_mode == "simple" && $upayment_mode != "yes" ) || ( $mode == "simple" )) { ?>
<?php if( get_option( 'cp_auction_users_payment_mode' ) == "yes" ) { ?>
<p><strong><?php _e('Simple Settings:', 'auctionPlugin'); ?></strong></p>
<?php } ?>
<p><?php _e('Please fill in your information in the fields below. Hover your mouse over the (?) for help in completing.', 'auctionPlugin'); ?></p>
<?php if( ( get_option('cp_auction_plugin_paypal') == "yes" || get_option('cp_auction_plugin_credits') == "yes" ) && (( $payment_mode == "simple" || $upayment_mode == "yes" ) 
&& ( $opt && in_array("paypal_email", $opt ))) || (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) { ?>
<p><?php _e('<strong>Note that PayPal email is mandatory if you want PayPal as payment option added to your ads</strong>.', 'auctionPlugin'); ?></p>
<?php } } elseif((( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) { ?>
<?php if( get_option( 'cp_auction_users_payment_mode' ) == "yes" ) { ?>
<p><strong><?php _e('Advanced Settings:', 'auctionPlugin'); ?></strong></p>
<?php } ?>
<p><?php _e('Fill in your information in the fields below to activate payment options in your ads. Once registration is complete, you will be able to track your sales and receive payments for your advertised products. Hover your mouse over the (?) for help in completing.', 'auctionPlugin'); ?></p>
<?php if( ( get_option('cp_auction_plugin_paypal') == "yes" || get_option('cp_auction_plugin_credits') == "yes" ) && (( $payment_mode == "simple" || $upayment_mode == "yes" ) 
&& ( $opt && in_array("paypal_email", $opt ))) || (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) { ?>
<p><?php _e('<strong>Note that it is mandatory to enable Credits OR enter a PayPal email if you want a payment function added to your ads</strong>. Which payment options you can choose depend on which options is provided by the system administrator.', 'auctionPlugin'); ?></p>
<?php } } ?>

      	    <div class="clear10"></div>

<?php if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("currencies", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="currency"><a title="<?php _e('Select your preferred currency.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select your preferred currency.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Currency:', 'auctionPlugin'); ?></label>
	<select tabindex="6" class="<?php echo $new_class; ?>" name="currency" id="currency"><?php echo cp_auction_currency_dropdown(get_user_meta( $uid, 'currency', true )); ?></select>
</p>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("symbol", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="cp_currency"><a title="<?php _e('Enter the currency symbol you want to appear next to prices on your ads (i.e. $, €, £, ¥)', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter the currency symbol you want to appear next to prices on your ads (i.e. $, €, £, ¥)', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Currency Symbol:', 'auctionPlugin'); ?></label>
	<input tabindex="7" id="cp_currency" name="cp_currency" value="<?php echo get_user_meta( $uid, 'cp_currency', true ); ?>" type="text">
</p>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("tax_vat", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="tax_vat"><a title="<?php _e('Enter a numeric value for the tax / vat. Taxes if added is calculated based on the sale price, this means that the selling price will always be inclusive of tax which will be specified in the payment overview.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a numeric value for the tax / vat. Taxes if added is calculated based on the sale price, this means that the selling price will always be inclusive of tax which will be specified in the payment overview.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Tax / Vat (%):', 'auctionPlugin'); ?></label>
	<input tabindex="8" id="tax_vat" name="tax_vat" value="<?php echo get_user_meta( $uid, 'cp_auction_tax_vat', true ); ?>" type="text">
</p>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("author_approves", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="cp_author_approves"><a title="<?php _e('Choose to manually approve all payments regardless of other settings. If you sell digital products and the buyer has direct access to download these after purchase so it is recommended to enable this setting, this unless you have the ability to activate the PDT, and PDT is activated.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Choose to manually approve all payments regardless of other settings. If you sell digital products and the buyer has direct access to download these after purchase so it is recommended to enable this setting, this unless you have the ability to activate the PDT, and PDT is activated.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Approve Payments:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="9" id="cp_author_approves" name="cp_author_approves"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_author_approves', true )); ?></select>
</p>

<?php } if((((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("allow_pms", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) && ( class_exists("cartpaujPM") )) 
 || ( current_user_can( 'manage_options' ) && class_exists("cartpaujPM") )) { ?>
<p>
	<label for="allow_pms"><a title="<?php _e('If you enable PM on Ads there will appear a Send PM link on all your ads, and any logged in user will be able to send you private messages.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('If you enable PM on Ads there will appear a Send PM link on all your ads, and any logged in user will be able to send you private messages.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Enable PM on Ads:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="10" id="allow_pms" name="cp_auction_allow_pms"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_auction_allow_pms', true )); ?></select>
</p>

<?php } if((((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("anonymous", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) && ( get_option('cp_auction_plugin_bid_anonymous') == "yes" )) 
|| ( current_user_can( 'manage_options' ) && get_option('cp_auction_plugin_bid_anonymous') == "yes" )) { ?>
<p>
	<label for="anonymous"><a title="<?php _e('Allow users to bid on your auctions anonymous.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Allow users to bid on your auctions anonymous.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Allow users to bid on your auctions anonymous.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Allow users to bid on your auctions anonymous.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Allow Anonymous Bids:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="11" id="anonymous" name="cp_allow_anonymous"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_allow_anonymous', true )); ?></select>
</p>

<?php } if((((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("safe_pay", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) && ( get_option('cp_auction_plugin_pay_admin') == "yes" 
&& get_option('cp_auction_plugin_pay_user') == "yes" )) || ( current_user_can( 'manage_options' ) && get_option('cp_auction_plugin_pay_admin') == "yes" && get_option('cp_auction_plugin_pay_user') == "yes" )) { ?>
<p>
	<label for="safe_payment"><a title="<?php _e('Enable secure payment / escrow. Payments and goods delivery is then protected by us.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enable secure payment / escrow. Payments and goods delivery is then protected by us.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Enable Safe Payments:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="12" id="safe_payment" name="cp_use_safe_payment"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_use_safe_payment', true )); ?></select>
</p>

<?php } if( get_option('cp_auction_plugin_cih') == "yes" ) { ?>

<?php if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("accept_cih", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<div class="shipping-frame">
<p>
	<label for="accept_cod"><a title="<?php _e('Select Yes if you accept that your customers pay for their purchases with cash in hand (CIH).', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select Yes if you accept that your customers pay for their purchases with cash in hand (CIH).', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Accept Cash in Hand:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="13" id="accept_cih" name="accept_cih"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_accept_cih_payment', true )); ?></select>
</p>
</div>
<?php } ?>

<?php } if(( get_option('cp_auction_plugin_cod') == "yes" ) && (((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("accept_cod", $opt ) || $opt && in_array("cod_fees", $opt ) )) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) ))) { ?>
<div class="shipping-frame">
<?php if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("accept_cod", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>

<p>
	<label for="accept_cod"><a title="<?php _e('Select Yes if you accept that your customers pay for their purchases with cash on delivery (COD).', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select Yes if you accept that your customers pay for their purchases with cash on delivery (COD).', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Accept Cash on Delivery:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="14" id="accept_cod" name="accept_cod"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_accept_cod_payment', true )); ?></select>
</p>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("cod_fees", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="cod_fees"><a title="<?php _e('Enter a numeric value if you want to charge the customers a fee when they pay with cash on delivery. This fee will overwrite other specified shipping costs if buyer choose to pay with cash on delivery.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a numeric value if you want to charge the customers a fee when they pay with cash on delivery. This fee will overwrite other specified shipping costs if buyer choose to pay with cash on delivery.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Charge Extra Fee (Flat):', 'auctionPlugin'); ?></label>
	<input tabindex="15" id="cod_fees" name="cp_auction_cod_fees" value="<?php echo get_user_meta( $uid, 'cp_auction_cod_fees', true ); ?>" type="text">
</p>

<?php } ?>
</div>
<?php } if(( get_option('cp_auction_plugin_bank_transfer') == "yes" ) && (((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("accept_bt", $opt ) || $opt && in_array("bt_fees", $opt ) || 
$opt && in_array("accept_bt", $opt ) )) || (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) ))) { ?>
<div class="shipping-frame">
<?php if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("accept_bt", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>

<p>
	<label for="accept_bt"><a title="<?php _e('Select Yes if you accept that your customers pay for their purchases using bank transfer.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select Yes if you accept that your customers pay for their purchases using bank transfer.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Accept Bank Transfer:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="16" id="accept_bt" name="accept_bt"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_accept_bank_transfer_payment', true )); ?></select>
</p>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("bt_fees", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="bt_fees"><a title="<?php _e('Enter a numeric value if you want to charge the customers a fee when they pay with bank transfer.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a numeric value if you want to charge the customers a fee when they pay with bank transfer.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Charge Extra Fee (%):', 'auctionPlugin'); ?></label>
	<input tabindex="17" id="bt_fees" name="cp_auction_bank_transfer_fees" value="<?php echo get_user_meta( $uid, 'cp_auction_bank_transfer_fees', true ); ?>" type="text">
</p>
<?php } ?>

<?php if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("accept_bt", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="info_bt"><a title="<?php _e('Enter your bank transfer instructions, ie. name, bank name and bank account id / number. This so the buyer knows where and how to send the payment for your goods.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter your bank transfer instructions, ie. name, bank name and bank account id / number. This so the buyer knows where and how to send the payment for your goods.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Bank Transfer Instructions:', 'auctionPlugin'); ?></label>
	<textarea tabindex="18" name="cp_auction_bank_transfer_info" id="info_bt"><?php echo get_user_meta( $uid, 'cp_auction_bank_transfer_info', true ); ?></textarea>
</p>

<?php } ?>
</div>
<?php } if(( get_option('cp_auction_plugin_paypal') == "yes" ) && (((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("paypal_email", $opt ) || $opt && in_array("language", $opt ) || 
$opt && in_array("pp_fees", $opt ) )) || (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) ))) { ?>
<div class="shipping-frame">
<?php if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("paypal_email", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="paypal_email"><a title="<?php _e('Please enter your PayPal email if you accept that your customers pay for their purchases with PayPal or credit card.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Please enter your PayPal email if you accept that your customers pay for their purchases with PayPal or credit card.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php if( $pp_required == "required" ) echo '<font color="red">*</font>'; ?> <?php _e('Accept PayPal:', 'auctionPlugin'); ?></label>
	<input tabindex="19" id="paypal_email" name="paypal_email" value="<?php echo get_user_meta( $uid, 'paypal_email', true ); ?>" type="text">
</p>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("language", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="paypal_lang"><a title="<?php _e('Select your preferred PayPal language.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select your preferred PayPal language.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('PayPal Language:', 'auctionPlugin'); ?></label>
	<select tabindex="20" class="<?php echo $new_class; ?>" name="paypal_lang" id="paypal_lang"><?php echo cp_auction_language_dropdown(get_user_meta( $uid, 'paypal_lang', true )); ?></select>
</p>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("pp_fees", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="paypal_fees"><a title="<?php _e('Enter a numeric value if you want to charge the customers a fee when they pay with PayPal.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a numeric value if you want to charge the customers a fee when they pay with PayPal.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Charge Extra Fee (%):', 'auctionPlugin'); ?></label>
	<input tabindex="21" id="paypal_fees" name="cp_auction_paypal_fees" value="<?php echo get_user_meta( $uid, 'cp_auction_paypal_fees', true ); ?>" type="text">
</p>
<?php } ?>
</div>
<?php } if(( function_exists('aws_cp_settings_transactions') && get_option('cp_auction_plugin_credits') == "yes" ) && (((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("accept_credits", $opt ) || 
$opt && in_array("credit_fees", $opt ) )) || (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) ))) { ?>
<div class="shipping-frame">
<?php if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("accept_credits", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>

<p>
	<label for="accept_credits"><a title="<?php _e('Select Yes if you accept that your customers pay for their purchases with credits. You may at any time redeem your credits in cash.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select Yes if you accept that your customers pay for their purchases with credits.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Accept Credit Payments:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="22" id="accept_credits" name="accept_credits"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_accept_credit_payment', true )); ?></select>
</p>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("credit_fees", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<p>
	<label for="credit_fees"><a title="<?php _e('Enter a numeric value if you want to charge the customers a fee when they pay with credits.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a numeric value if you want to charge the customers a fee when they pay with credits.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Charge Extra Fee (%):', 'auctionPlugin'); ?></label>
	<input tabindex="23" id="credit_fees" name="cp_auction_credit_fees" value="<?php echo get_user_meta( $uid, 'cp_auction_credit_fees', true ); ?>" type="text">
</p>

<?php } ?>
</div>
<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("shipping", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<div class="shipping-frame">
<p>
	<label for="shipping"><a title="<?php _e('Enter a standard shipping cost. If you provide a shipping cost during the post an ad process the shipping cost entered here will be overwritten.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a standard shipping cost. If you provide a shipping cost during the post an ad process the shipping cost entered here will be overwritten.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Shipping Cost:', 'auctionPlugin'); ?></label>
	<input tabindex="24" id="shipping" name="cp_auction_shipping" value="<?php echo get_user_meta( $uid, 'cp_auction_shipping', true ); ?>" type="text">
</p>

<p>
	<label for="free_shipping"><a title="<?php _e('Enter a free shipping minimum order amount. Leave blank to disable.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a free shipping minimum order amount. Leave blank to disable.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Free Shipping:', 'auctionPlugin'); ?></label>
	<input tabindex="25" id="free_shipping" name="cp_auction_free_shipping" value="<?php echo get_user_meta( $uid, 'cp_auction_free_shipping', true ); ?>" type="text">
</p>

<p>
	<label for="add_shipping"><a title="<?php _e('Enter how many percent the shipping cost will increase per item if the buyer buys more than one product.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter how many percent the shipping cost will increase per item if the buyer buys more than one product.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Additional Cost (%):', 'auctionPlugin'); ?></label>
	<input tabindex="26" id="add_shipping" name="cp_auction_add_shipping" value="<?php echo get_user_meta( $uid, 'cp_auction_add_shipping', true ); ?>" type="text">
</p>

<p>
	<label for="max_shipping"><a title="<?php _e('Enter the maximum shipping cost to be charged to the buyer.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter the maximum shipping cost to be charged to the buyer.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Max. Shipping Cost:', 'auctionPlugin'); ?></label>
	<input tabindex="27" id="max_shipping" name="cp_auction_max_shipping" value="<?php echo get_user_meta( $uid, 'cp_auction_max_shipping', true ); ?>" type="text">
</p>
</div>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("shipping_module", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<div class="shipping-frame">
<?php if ( get_option('cp_auction_disable_the_enable_shipping_option') != "yes" ) { ?>
<p>
	<label for="cp_auction_enable_shipping"><a title="<?php _e('Enable the country based shipping. If you provide a shipping cost during the post an ad process the shipping cost entered here will be overwritten.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enable the country based shipping. If you provide a shipping cost during the post an ad process the shipping cost entered here will be overwritten.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Enable Shipping:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="28" id="cp_auction_enable_shipping" name="cp_auction_enable_shipping"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_auction_enable_shipping', true )); ?></select>
</p>
<?php } ?>

<p class="cp_shipping_country">
	<label for="cp_shipping_country"><a title="<?php _e('Select a country and then enter the shipping cost for deliveries to this country, continue in the same way with the next country. Potential buyers from countries other than the countries you specify, will be dismissed. Enter the cost as flat shipping cost, ie. 10 or 10.00 (numeric values only).', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select a country and then enter the shipping cost for deliveries to this country, continue in the same way with the next country. Potential buyers from countries other than the countries you specify, will be dismissed. Enter the cost as flat shipping cost, ie. 10 or 10.00 (numeric values only).', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Delivery Location:', 'auctionPlugin'); ?></label>
	<?php echo cp_auction_shipping_countries( 'cp_country', $uid ); ?>

<?php
	$option = get_user_meta( $uid, 'shipto', true );
	if ( ! empty( $option ) ) {
	$i = 0;
	foreach ( $option as $item => $value ) {
	if ( $value > '0' ) {
	$country = $item;
	if ( $item == "ww" ) $item = __( 'Everywhere else', 'auctionPlugin' );
	echo '<br /><span id="'.$i.'"><label for="shipto['.$country.']"><a title="'.__('Enter the cost as flat shipping cost, ie. 10 or 10.00 (numeric values only).', 'auctionPlugin').'" href="#" tip="'.__('Enter the cost as flat shipping cost, ie. 10 or 10.00 (numeric values only).', 'auctionPlugin').'" tabindex="99"><span class="helpico"></span></a>'.$item.':</label> <input tabindex="99" id="shipto['.$country.']" name="shipto['.$country.']" value="'.$value.'" style="width: 100px;" type="text"> <a href="#" class="delete-country" title="'.__('Remove the shipping cost for this country.', 'auctionPlugin').'" id="'.$i.'">'.__('Delete', 'auctionPlugin').'</a></span>';
	}
	$i++;
	}
	}
?>
</p>

<p>
	<label for="free_shipping"><a title="<?php _e('Enter a free shipping minimum order amount. Leave blank to disable.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a free shipping minimum order amount. Leave blank to disable.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Free Shipping:', 'auctionPlugin'); ?></label>
	<input tabindex="30" id="free_shipping" name="cp_auction_free_shipping1" value="<?php echo get_user_meta( $uid, 'cp_auction_free_shipping', true ); ?>" type="text">
</p>

<p>
	<label for="add_shipping"><a title="<?php _e('Enter how many percent the shipping cost will increase per item if the buyer buys more than one product.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter how many percent the shipping cost will increase per item if the buyer buys more than one product.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Additional Cost (%):', 'auctionPlugin'); ?></label>
	<input tabindex="31" id="add_shipping" name="cp_auction_add_shipping1" value="<?php echo get_user_meta( $uid, 'cp_auction_add_shipping', true ); ?>" type="text">
</p>

<p>
	<label for="max_shipping"><a title="<?php _e('Enter the maximum shipping cost to be charged to the buyer.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter the maximum shipping cost to be charged to the buyer.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Max. Shipping Cost:', 'auctionPlugin'); ?></label>
	<input tabindex="32" id="max_shipping" name="cp_auction_max_shipping1" value="<?php echo get_user_meta( $uid, 'cp_auction_max_shipping', true ); ?>" type="text">
</p>

<p class="submit">
	<input tabindex="33" class="btn_orange" name="save_settings" id="save_settings" value="<?php _e('Save Changes', 'auctionPlugin'); ?>" type="submit">
</p>

</div>

<?php } if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("infotext", $opt ))) 
|| (( ! $payment_mode || $payment_mode == "pro" ) && ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>
<div class="shipping-frame">
<p>
	<label for="info_top"><a title="<?php _e('Enter any information you wish to be visible to your customers at the top of your marketplace page.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter any information you wish to be visible to your customers at the top of your marketplace page.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Info Text - Top:', 'auctionPlugin'); ?></label>
	<textarea tabindex="34" name="cp_auction_info_top" id="info_top"><?php echo get_user_meta( $uid, 'cp_auction_info_top', true ); ?></textarea><div class="clr"></div><div class="info-textarea"><small><p><?php _e('Enter any information you wish to be visible to your customers at the <strong>top</strong> of your marketplace page.', 'auctionPlugin'); ?> <a href="<?php echo $webshop_url; ?>" target="_blank"><?php _e('Preview your shop!', 'auctionPlugin'); ?></a></p></small></div>
</p>

<p>
	<label for="info_bottom"><a title="<?php _e('Enter any information you wish to be visible to your customers at the bottom of your marketplace page.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter any information you wish to be visible to your customers at the bottom of your marketplace page.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Info Text - Bottom:', 'auctionPlugin'); ?></label>
	<textarea tabindex="35" name="cp_auction_info_bottom" id="info_bottom"><?php echo get_user_meta( $uid, 'cp_auction_info_bottom', true ); ?></textarea><div class="clr"></div><div class="info-textarea"><small><p><?php _e('Enter any information you wish to be visible to your customers at the <strong>bottom</strong> of your marketplace page.', 'auctionPlugin'); ?> <a href="<?php echo $webshop_url; ?>" target="_blank"><?php _e('Preview your shop!', 'auctionPlugin'); ?></a></p></small></div>
</p>
</div>
<?php } ?>

	<div class="clear10"></div>

<p class="submit">
	<input tabindex="36" class="btn_orange" name="save_settings" id="save_settings" value="<?php _e('Save Settings', 'auctionPlugin'); ?>" type="submit">
</p>

</form>

<?php if(((( $payment_mode == "simple" || $upayment_mode == "yes" ) && ( $opt && in_array("pdt", $opt ))) || (( ! $payment_mode || $payment_mode == "pro" ) 
&& ( $upayment_mode != "yes" )) || ( $mode == "pro" )) || ( current_user_can( 'manage_options' ) )) { ?>

	<div class="clear30"></div>

<?php if( empty( $status_pdt ) )
$activate_pdt = __('Activate PDT', 'auctionPlugin');
else
$activate_pdt = __('Deactivate PDT', 'auctionPlugin'); ?>

<div class="box_title"><?php _e('Payment Data Transfer (PDT)', 'auctionPlugin'); ?></div>

	<div class="clear10"></div>

<form class="cp-ecommerce" action="" method="post" id="mainform">

<p><?php _e('<strong>NOTE</strong>: If you choose to enable PayPal payments in your ads then it is recommended to set up your PayPal to accept PDT. Payment Data Transfer (PDT) allows you to receive notification of successful payments as they are made. The use of PDT depends on your system configuration and your Return URL. Please note that in order to use PDT, you must turn on Auto Return URL, the URL that will be used to redirect your customers upon payment completion. It does not matter what URL you choose to use because our website will overwrite the selected url anyway,', 'auctionPlugin'); ?> <a href="https://www.x.com/developers/paypal/products/payment-data-transfer" target="_blank"><strong><?php _e('read more.', 'auctionPlugin'); ?></strong></a></p>

<?php if( empty( $status_pdt ) ) { ?>
<p>
	<label for="identity"><?php _e('Identity Token (optional):', 'auctionPlugin'); ?></label>
	<input tabindex="37" id="identity" name="identity_token" value="" type="text">
</p>

<?php } if(( $status_pdt == 1 ) && ( empty( $identity_token ))) {
echo '<p><font color="red"><strong>'.__('Please deactivate and enter a correct identity token!', 'auctionPlugin').'</strong></font></p>';
} ?>

	<div class="clear10"></div>

<p class="submit">
	<input tabindex="38" class="btn_orange" name="activate_pdt" id="activate_pdt" value="<?php echo $activate_pdt; ?>" type="submit">
</p>

</form>

<?php }
} else {
	$empty = 1;
} ?>

	<?php if( function_exists('aws_cp_settings_transactions') && get_option('cp_auction_plugin_credits') == "yes" ) { ?>

	<div class="clear30"></div>

	<div class="box_title"><?php _e('My Credits', 'auctionPlugin'); ?></div>

	<div class="clear10"></div>

	    <?php if( $howto_pay == "paypal" ) {
	    echo '<form class="cp-ecommerce" action="'.get_bloginfo('wpurl').'/?_process_payment=1" method="post" id="mainform" enctype="multipart/form-data">';
	    } elseif( $howto_pay == "bank_transfer" ) {
            echo '<form class="cp-ecommerce" action="'.get_bloginfo('wpurl').'/?_pay_bt_credits=1" method="post" id="mainform" enctype="multipart/form-data">';
            } else {
            echo '<div class="cp-ecommerce">';
            } ?>


	<div class="price_info">
		<div class="price_info1"><?php _e('Available Credits / Value:', 'auctionPlugin'); ?></div>
		<?php $value = get_user_meta( $uid, 'cp_credits', true ) * get_option('cp_auction_plugin_credit_value');
            	  $available = get_user_meta( $uid, 'cp_credits', true ); if( empty($available) ) $available = 0; ?>
		<div class="price_info2"><strong><?php echo $available; ?></strong> / <strong><?php echo cp_display_price( $value, 'ad', false ); ?></strong></div>
	</div>

	<div class="price_info">
		<div class="price_info1"><?php _e('Price per Credit:', 'auctionPlugin'); ?></div>
		<div class="price_info2"><strong><?php echo cp_display_price( get_option('cp_auction_plugin_credit_value'), 'ad', false ); ?></strong></div>
	</div>


	<div class="clear15"></div>

	<div class="box_title"><?php _e('Purchase Credits', 'auctionPlugin'); ?></div>

	<div class="clear15"></div>

<p>
	<label for="howto_pay"><a title="<?php _e('Select your preferred payment method.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select your preferred payment method.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Select Payment Option:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="19" name="howto_pay" id="howto_pay" onchange="window.location='<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'howto_pay='); ?>' + this.value;">
	<option value=""><?php _e('Select Option', 'auctionPlugin'); ?></option>
	<?php if( get_option('cp_auction_plugin_paypal') == "yes" ) { ?>
	<option value="paypal" <?php if($howto_pay == "paypal") echo 'selected="selected"'; ?>><?php _e('PayPal', 'auctionPlugin'); ?></option>
	<?php } if( get_option('cp_auction_plugin_bank_transfer') == "yes" ) { ?>
	<option value="bank_transfer" <?php if($howto_pay == "bank_transfer") echo 'selected="selected"'; ?>><?php _e('Bank Transfer', 'auctionPlugin'); ?></option>
	<?php } ?>
	</select>
</p>

	<?php if( $howto_pay ) { ?>

<p>
	<label for="credits"><a title="<?php _e('Enter the number of credits you want to purchase.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter the number of credits you want to purchase.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Number of Credits:', 'auctionPlugin'); ?></label>
	<input tabindex="20" id="credits" name="credits" value="" type="text" />
</p>

<?php } if( $howto_pay == "paypal" ) {

	$pp = get_option('cp_auction_plugin_paypalemail');
	if( empty( $pp ) ) $pp = $cp_options->gateways['paypal']['email_address']; // Use classipress email.

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;

	$conversion = false;
	if ( $enabl == "yes" && class_exists("Currencyr") )
	$conversion = currencyr_exchange( get_option('cp_auction_plugin_credit_value'), strtolower( $cuto ) );

	$cre_arg = array(
	'uid' => $uid,
	'ip_address' => $ip_address,
	'payment_date' => time(),
	'datemade' => time(),
	'item_name' => 'Credits',
	'trans_type' => 'Credits by PayPal',
	'mc_currency' => $cc,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'receiver_email' => $pp,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'credit_value' => cp_auction_sanitize_amount($conversion),
	'howto_pay' => 'paypal',
	'type' => 'credits_purchase'
	);

	// PayPal Payment
	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $cre_arg ) ).'">';

	if( get_option('cp_auction_plugin_button') == 1 ) {
	echo '<div class="small_button"><input tabindex="21" type="image" src="http://images.paypal.com/images/x-click-but1.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" onclick="return ValidateME();"></div><div class="clear20"></div>';
	} elseif( get_option('cp_auction_plugin_button') == 2 ) {
	echo '<div class="small_button"><input tabindex="22" type="image" src="https://www.paypal.com/en_GB/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" onclick="return ValidateME();"></div><div class="clear20"></div>';
	} else {
	echo '<div class="small_button"><input tabindex="23" type="submit" name="submit" value="'.__('Pay with PayPal','auctionPlugin').'" class="btn_orange" onclick="return ValidateME();" /></div><div class="clear20"></div>';
	}
	// End PayPal Payment

	} if($howto_pay == "bank_transfer") {

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;

	$txn_id = 'BT'.$pid.'A'.time().'P';

	$bt_arg = array(
	'uid' => $uid,
	'ip_address' => $ip_address,
	'payment_date' => time(),
	'datemade' => time(),
	'txn_id' => $txn_id,
	'item_name' => 'Credits',
	'trans_type' => 'Credits by Bank Transfer',
	'mc_currency' => $cc,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'howto_pay' => 'bank_transfer',
	'type' => 'credits_purchase'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $bt_arg ) ).'">';

	echo '<div class="small_button"><input tabindex="24" type="submit" name="submit_purchase" value="'.__('Pay with Bank Transfer', 'auctionPlugin').'" class="btn_orange" onclick="return ValidateME();" /></div><div class="clear20"></div>';

}
if( $howto_pay ) { ?>
	</form>
        <?php } else { ?>
        </div>
        <?php } ?>

	<?php $min_withdraw = get_option('cp_auction_minimum_withdraw'); ?>
	<?php if( empty($min_withdraw) || $available >= $min_withdraw ) { ?>
	<form class="cp-ecommerce" action="<?php echo get_bloginfo('wpurl'); ?>/?_process_withdraw=1" method="post" id="mainform" enctype="multipart/form-data">

	<div class="clear15"></div>

	<div class="box_title"><?php _e('Withdraw / Exchange', 'auctionPlugin'); ?></div>

	<div class="clear15"></div>

<p>
	<label for="credits"><a title="<?php _e('Enter the number of credits you want to withdraw.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter the number of credits you want to withdraw.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Number of Credits:', 'auctionPlugin'); ?></label>
	<input tabindex="25" id="withdraw" name="withdraw" value="" type="text" />
</p>

<p>
	<label for="transfer paypal"><a title="<?php _e('Select your preferred payout option.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select your preferred payout option.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Select Payout Option:', 'auctionPlugin'); ?></label>
	<?php if( get_option('cp_auction_paypal_withdraw_credits') == "yes" ) { ?>
	<input tabindex="26" type="radio" id="paypal" name="payout_option" value="paypal"> <?php _e('PayPal <i>(Update your profile with your PayPal email)</i>', 'auctionPlugin'); ?>
	<br />
	<?php } if( get_option('cp_auction_bt_withdraw_credits') == "yes" ) { ?>
	<input tabindex="27" type="radio" id="transfer" name="payout_option" value="bank_transfer"> <?php _e('Bank Transfer <i>(Fill in the requested details below)</i>', 'auctionPlugin'); ?>
	<?php } ?>
</p>

<p>
	<label for="payout_details"><a title="<?php _e('Enter your payout details, paypal email or bank transfer information. 250 chars max.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter your payout details, paypal email or bank transfer information. 250 chars max.', 'auctionPlugin'); ?>" tabindex="99"><span class="helpico"></span></a><?php _e('Payout Details:', 'auctionPlugin'); ?></label>
	<textarea tabindex="28" id="payout_details" name="payout_details"><?php echo get_option('cp_auction_payout_info'); ?></textarea><div class="clr"></div><div class="info-textarea"><small><p><?php _e('Enter your payout details, paypal email or bank transfer information. 250 chars max.', 'auctionPlugin'); ?></p></small></div>
</p>
	<input type="hidden" name="uid" value="<?php echo $uid; ?>">
	<input type="hidden" name="ip_address" value="<?php echo cp_auction_users_ip(); ?>">

<p class="submit">
	<input tabindex="29" class="btn_orange" name="submit_withdraw" id="submit_withdraw" value="<?php _e('Withdraw', 'auctionPlugin'); ?>" onclick="return ValidateME();" type="submit">
</p>

</form>

	<?php } ?>

	<div class="clear20"></div>

<?php } else {
	$empty = $empty + 1;
}
if(( $empty == 2 ) && ( get_option('cp_auction_plugin_admins_only') != "yes" && get_option('cp_auction_disable_settings') != "yes" || current_user_can( 'manage_options' ) )) { ?>

	<p class="text-center"><?php _e( 'There are no specific settings for your account.', 'auctionPlugin' ); ?></p>
	<div class="pad10"></div>

<?php } ?>

			</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

</div><!-- /settings - tab_content -->

<?php } ?><!-- /settings disabled -->

<script type="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, windowname, 'width=800,height=500,scrollbars=yes');
return false;
}
//-->
</script>

<?php if( get_option('cp_auction_plugin_admins_only') != "yes" || current_user_can( 'manage_options' ) ) { ?>

    <div id="sold">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('Items I Sold', 'auctionPlugin'); ?></span></strong></span></div>

<?php

global $wpdb;
$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";

$rows_per_page = $maxposts;
$paginate_links = ""; $before = ""; $after = "";
$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
 
$rows = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' ORDER BY id DESC");

if( $wpdb->num_rows != 0 ) {

	global $wp_rewrite;
 
$pagination_args = array(
	'base' => @esc_url( add_query_arg('paged','%#%') ),
	'format' => '',
	'total' => ceil(sizeof($rows)/$rows_per_page),
	'current' => $current,
	'show_all' => false,
	'type' => 'plain',
	'prev_text' => '&lsaquo;&lsaquo;',
	'next_text' => '&rsaquo;&rsaquo;',
);
 
if( $wp_rewrite->using_permalinks() )
	$pagination_args['base'] = trailingslashit( esc_url( remove_query_arg('s',cp_auction_url($cpurl['userpanel'], '?_user_panel=1') ) ) ) . 'page/%#%/';
 
if( !empty($wp_query->query_vars['s']) )
	$pagination_args['add_args'] = array('s'=>get_query_var('s'));

	if( $pagination_args['total'] > 1 )
	$paginate_links .= '<span class="total">' . sprintf( __( 'Page %s of %s', 'auctionPlugin' ), $pagination_args['current'], $pagination_args['total'] ) . '</span>';

	$paginate_links .= paginate_links($pagination_args);

	$start = ($current - 1) * $rows_per_page;
	$end = $start + $rows_per_page;
	$end = (sizeof($rows) < $end) ? sizeof($rows) : $end;
 
	for ($i=$start;$i < $end ;++$i ) {
	$row = $rows[$i];

	$id = $row->id;
	$pid = $row->pid;
	global $post;
	$post = get_post($pid);
	$featured = get_post_meta($pid,'featured',true);
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );
	$cp_buy_now = get_post_meta($pid,'cp_buy_now',true);
	if( empty( $cp_buy_now ) ) $cp_buy_now = get_post_meta($post->ID, 'cp_price', true);
	if( $my_type == "reverse" ) {
	$cp_buy_now = get_post_meta($pid,'winner_amount',true);
	}

	$orderid = $row->order_id;
	$unpaid = $row->unpaid;
	$paid = $row->paid;
	$sold = $row->numbers;
	if( !$sold ) $sold = 0;
	$howmany = $sold;
	$buyer_id = $row->buyer;
	$seller_id = $row->uid;
	if( $row->status == "paid" ) $status = ''.__('deactivate','auctionPlugin').'';
	if( $row->status == "deactivated" ) $status = '<font color="red">'.__('deactivated','auctionPlugin').'</font>';
	$user = get_user_by('id', $buyer_id);

	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$orderid'");
	if( $row )
	$order_id = $row->order_id;
	else $order_id = false; ?>

	<div class="post-block-out <?php cp_display_style( 'featured' ); ?>">

	<div class="post-block">

		<div class="aws-post">

	<div class="sold_container">
		<div class="sold_img"><?php if ( get_option('stylesheet') == "simply-responsive-cp" && $cp_options->ad_images ) echo cp_auction_dashboard_thumbnail(); else echo cp_ad_loop_thumbnail(); ?></div>

	<div class="sold_ads"><div class="small_title"><a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a></div>
	<?php if($sold > 0) {
	echo '<div class="sold_paid"><strong>'.__('STATUS: ', 'auctionPlugin').''; if(($unpaid > 0) && ($paid != 2 )) { echo '<font color="red">'.__('UNPAID', 'auctionPlugin').'</font></strong> [<a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$buyer_id.'').'">'.__('contact buyer', 'auctionPlugin').'</a>]'; if( $post->post_author == $uid ) { echo ' [<a href="#" class="mark-paid" title="'.__('Mark this listing as paid.', 'auctionPlugin').'" id="'.$orderid.'|'.$pid.'|'.$buyer_id.'|'.$unpaid.'">'.__('mark paid', 'auctionPlugin').'</a>]'; echo ' [<a href="#" class="delete-complete-purchase" id="'.$id.'">'.__('delete', 'auctionPlugin').'</a>]'; } } if(($unpaid == 0) && ($paid != 2 )) { echo '<font color="green">'.__('PAID', 'auctionPlugin').'</font></strong>'; echo ' [<a href="#" class="deactivate-purchase" id="'.$id.'">'.$status.'</a>]'; if( $order_id > 0 ) { echo ' [<a href="'.get_bloginfo('wpurl').'/?get_invoice='.$order_id.'" onclick="return popup(this, \'notes\')">'.__('invoice', 'auctionPlugin').'</a>]'; } } if($paid == 2) { echo '<font color="orange">'.__('PENDING', 'auctionPlugin').'</font></strong> [<a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$buyer_id.'').'">'.__('contact buyer', 'auctionPlugin').'</a>]'; if( $post->post_author == $uid ) { echo ' [<a href="#" class="mark-paid" title="'.__('Mark this listing as paid.', 'auctionPlugin').'" id="'.$orderid.'|'.$pid.'|'.$buyer_id.'|'.$unpaid.'">'.__('mark paid', 'auctionPlugin').'</a>]'; echo ' [<a href="'.get_bloginfo('wpurl').'/?get_invoice='.$order_id.'" onclick="return popup(this, \'notes\')">'.__('details', 'auctionPlugin').'</a>]'; echo ' [<a href="#" class="delete-complete-purchase" id="'.$id.'">'.__('delete', 'auctionPlugin').'</a>]'; } } echo '</div>';
	}

	if( $order_id > 0 ) { echo '<strong>'.__('Order ID:', 'auctionPlugin').'</strong>'; if(($unpaid == 0) && ($paid != 2 )) { echo ' <a href="'.get_bloginfo('wpurl').'/?get_invoice='.$order_id.'" onclick="return popup(this, \'notes\')">'.$order_id.'</a>'; } else { echo ' '.$order_id.''; } echo '<br/>'; }
	echo '<strong>'.__('Item Price:', 'auctionPlugin').'</strong> '.cp_display_price( $cp_buy_now, 'ad', false ).'<br/>';
	cp_auction_price_details();
	echo '<strong>'.__('Sold:', 'auctionPlugin').'</strong> '.$howmany.' '.__('items to', 'auctionPlugin').' <a href="'.get_author_posts_url($buyer_id).'"> '.$user->user_login.'</a>'; if( $unpaid > 0 ) { echo ', '.$unpaid.' '.__('items is not paid.', 'auctionPlugin').''; } ?></div>
	</div>

		</div><!-- /aws-post -->

	</div><!-- /post-block -->

</div><!-- /post-block-out -->

<?php
}
	if( $paginate_links ) {
	echo '<div class="clear10"></div>';
	echo '<div class="cp-paging">';
	echo $before . '<div class="pages">';
	echo $paginate_links;
	echo '</div><div class="clr"></div>' . $after;
	echo '</div>';
	}
	$paginate_links = '';
}
else { ?>
<div class="shadowblock_out">
	<div class="shadowblock">

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'You currently have no sold items.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

	</div><!-- /shadowblock -->
</div><!-- /shadowblock_out -->
<?php } ?>

	</div><!-- /sold - tab_content -->

<?php } ?><!-- /admins only -->

	<div id="won">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('My Purchases', 'auctionPlugin'); ?></span></strong></span></div>

<?php

global $wpdb;
$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";

$paginate_links = ""; $before = ""; $after = "";
$results_per_page = $maxposts;
$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
 
$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE buyer = '$uid' AND status != 'deactivated' ORDER BY id DESC");

if( $wpdb->num_rows != 0 ) {

	global $wp_rewrite;
 
$pagination_args = array(
	'base' => @esc_url( add_query_arg('paged','%#%') ),
	'format' => '',
	'total' => ceil(sizeof($results)/$results_per_page),
	'current' => $current,
	'show_all' => false,
	'type' => 'plain',
	'prev_text' => '&lsaquo;&lsaquo;',
	'next_text' => '&rsaquo;&rsaquo;',
);

if( $wp_rewrite->using_permalinks() )
	$pagination_args['base'] = trailingslashit( esc_url( remove_query_arg('s',cp_auction_url($cpurl['userpanel'], '?_user_panel=1') ) ) ) . 'page/%#%/';
 
if( !empty($wp_query->query_vars['s']) )
	$pagination_args['add_args'] = array('s'=>get_query_var('s'));

	if( $pagination_args['total'] > 1 )
	$paginate_links .= '<span class="total">' . sprintf( __( 'Page %s of %s', 'auctionPlugin' ), $pagination_args['current'], $pagination_args['total'] ) . '</span>';

	$paginate_links .= paginate_links($pagination_args);

	$start = ($current - 1) * $results_per_page;
	$end = $start + $results_per_page;
	$end = (sizeof($results) < $end) ? sizeof($results) : $end;
 
	for ($i=$start;$i < $end ;++$i ) {
	$result = $results[$i];

	$id = $result->id;
	$pid = $result->pid;
	global $post;
	$post = get_post($pid);
	$featured = get_post_meta($pid,'featured',true);
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );
	$cp_buy_now = get_post_meta($pid,'cp_buy_now',true);
	if( empty( $cp_buy_now ) ) $cp_buy_now = get_post_meta($post->ID, 'cp_price', true);
	if( $my_type == "reverse" ) {
	$cp_buy_now = get_post_meta($pid,'winner_amount',true);
	if( empty( $cp_buy_now )) $cp_buy_now = cp_auction_plugin_get_latest_bid($pid);
	}

	$orderid = $result->order_id;
	$unpaid = $result->unpaid;
	$paid = $result->paid;
	$sold = $result->numbers;
	if( !$sold ) $sold = 0;
	$howmany = $sold;
	$buyer_id = $result->buyer;
	$seller_id = $result->uid;
	$user = get_user_by('id', $seller_id);

	$table_trans = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$orderid'");
	if( $row )
	$order_id = $row->order_id;
	$paid_by = $row->payment_option;
	$bt_info = get_user_meta ( $seller_id, 'cp_auction_bank_transfer_info', true ); ?>

	<div class="post-block-out <?php cp_display_style( 'featured' ); ?>">

		<div class="post-block">

			<div class="aws-post">

	<div class="sold_container">
		<div class="sold_img"><?php if ( get_option('stylesheet') == "simply-responsive-cp" && $cp_options->ad_images ) echo cp_auction_dashboard_thumbnail(); else echo cp_ad_loop_thumbnail(); ?></div>

	<div class="sold_ads"><div class="small_title"><a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a></div>
	<?php if($sold > 0) {
	echo '<div class="sold_paid"><strong>'.__('STATUS: ', 'auctionPlugin').''; if(($unpaid > 0) && ($paid != 2 )) { echo '<font color="red">'.__('UNPAID', 'auctionPlugin').'</font></strong> [<a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$seller_id.'').'">'.__('contact seller', 'auctionPlugin').'</a>] [<a href="'.cp_auction_url($cpurl['cart'], '?shopping_cart=1').'">'.__('pay now', 'auctionPlugin').'</a>]'; if($paid_by == 'bank_transfer') { echo ' [<a href="#" class="show-bt" id="'.$order_id.'">'.__('bank transfer', 'auctionPlugin').'</a>]'; } } if(($unpaid == 0) && ($paid != 2 )) { echo '<font color="green">'.__('PAID', 'auctionPlugin').'</font></strong>'; if( $order_id > 0 ) { echo ' [<a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$seller_id.'').'">'.__('contact seller', 'auctionPlugin').'</a>] [<a href="'.get_bloginfo('wpurl').'/?get_invoice='.$order_id.'" onclick="return popup(this, \'notes\')">'.__('invoice', 'auctionPlugin').'</a>]'; } } if($paid == 2) { echo '<font color="orange">'.__('PENDING', 'auctionPlugin').'</font></strong> [<a href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$seller_id.'').'">'.__('contact seller', 'auctionPlugin').'</a>]'; if($paid_by == 'bank_transfer') { echo ' [<a href="#" class="show-bt" id="'.$order_id.'">'.__('bank transfer', 'auctionPlugin').'</a>]'; } } echo '</div>';
	}

	if( $order_id > 0 ) { echo '<strong>'.__('Order ID:', 'auctionPlugin').'</strong>'; if(($unpaid == 0) && ($paid != 2 )) { echo ' <a href="'.get_bloginfo('wpurl').'/?get_invoice='.$order_id.'" onclick="return popup(this, \'notes\')">'.$order_id.'</a>'; } else { echo ' '.$order_id.''; } echo '<br/>'; }
	echo '<strong>'.__('Item Price:', 'auctionPlugin').'</strong> '.cp_display_price( $cp_buy_now, 'ad', false ).'<br/>';
	cp_auction_price_details();
	echo '<strong>'.__('Purchased:', 'auctionPlugin').'</strong> '.$howmany.' '.__('items from', 'auctionPlugin').' <a href="'.get_author_posts_url($seller_id).'">'.$user->user_login.'</a>'; if( $unpaid > 0 ) { echo ', '.$unpaid.' '.__('items is not paid.', 'auctionPlugin').''; } ?>

	<div class="bt-payment-<?php echo $order_id; ?>" style="display: none;">
	<div class="clear20"></div>
	<p><strong><?php _e('Sum Total', 'auctionPlugin'); ?></strong>: <?php echo cp_display_price( $row->mc_gross, 'ad', false ); ?></p>

	<?php echo nl2br($bt_info); ?>
	</div><!-- /bt-payment -->

		</div><!-- /sold_ads -->
	</div><!-- /sold_container -->

		</div><!-- /aws-post -->

	</div><!-- /post-block -->

</div><!-- /post-block-out -->

<?php
}
	if( $paginate_links ) {
	echo '<div class="clear10"></div>';
	echo '<div class="cp-paging">';
	echo $before . '<div class="pages">';
	echo $paginate_links;
	echo '</div><div class="clr"></div>' . $after;
	echo '</div>';
	}
	$paginate_links = '';
}
else { ?>
<div class="shadowblock_out">
	<div class="shadowblock">

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'You currently have no purchased items.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

	</div><!-- /shadowblock -->
</div><!-- /shadowblock_out -->
<?php } ?>

	    </div><!-- /won - tab_content -->

	<?php if( get_option('cp_auction_plugin_wanted') == true || get_option('cp_auction_enable_classified_wanted') == "yes" ) { ?>

    <div id="wanted">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('Wanted Ads Offers', 'auctionPlugin'); ?></span></strong></span> - <?php _e( 'Here`s your list of typical <strong>wanted ads</strong> requests you have received regarding your own desires, objects searching, or your requests to others looking to buy, trade or give away items of interest to you.', 'auctionPlugin' ); ?></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">	

<h1 class="single dotted"><?php _e('My Wanted Offers', 'auctionPlugin'); ?></h1>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Ad Title', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:90px"><div style="text-align: center;"><?php _e('My Offers', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone" style="width:180px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$bids = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND type = 'wanted' ORDER BY datemade DESC");
	$i=1;
	if( $bids ) {
	$rowclass = '';
	foreach($bids as $bid) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$acceptance = $bid->status;
	if( empty( $bid->status ) || $bid->status != "accepted" ) $status = '<font color="orange"><strong>'.__('Pending','auctionPlugin').'</strong></font>';
	else $status = '<font color="green"><strong>'.__('Accepted','auctionPlugin').'</strong></font>';

?>

<div id="msg<?php echo $bid->id; ?>" class="wanted">
	<div>
	<p align="left"><strong><?php _e('Product Description', 'auctionPlugin'); ?></strong></p>
	<?php if( empty($bid->message) ) echo ''.__('Sorry, there have been no description of the product.','auctionPlugin').''; else echo $bid->message; ?>
	<p align="right">[<a href="#" onclick="msg(<?php echo $bid->id; ?>)"><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
	</div>
</div>

<?php
	global $post, $cpurl;
	$post = get_post($bid->pid);

	$user_info = get_userdata($post->post_author);
	$wad_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'"><strong>'.truncate(strip_tags($post->post_title), 12, '..').'</strong></a>';

   echo '<tr id="'.$bid->id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column">'; if( $post->post_status == "publish" ) echo ''.$wad_title.''; else echo '<strong>'.truncate($post->post_title, 40).'</strong>'; echo '</td>
	<td class="text-right">'.cp_display_price( $bid->bid, 'ad', false ).'</td>
	<td class="text-center footable-last-column"><a title="'.__('Click here to contact the author!', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$bid->post_author.'').'">'.__('Contact', 'auctionPlugin').'</a> | '.$status.''; if( $acceptance != "accepted" ) echo ' | <a href="#" class="delete-wanted-offer" title="'.__('Click here to delete this offer.', 'auctionPlugin').'" id="'.$bid->id.'">'.__('Delete', 'auctionPlugin').'</a></td>
	</tr>';

	}
	
} else {
   echo '<tr><td colspan="3">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

<script type="text/javascript">
function msg(id) {
	el = document.getElementById("msg"+id);
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>

<?php if( get_option('cp_auction_plugin_admins_only') != "yes" || current_user_can( 'manage_options' ) ) { ?>

	<div class="clear50"></div>

<h1 class="single dotted"><?php _e('Offers Received', 'auctionPlugin'); ?></h1>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Ad Title', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:110px"><div style="text-align: center;"><?php _e('Offers Received', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone" style="width:250px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$bids = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_author = '$uid' AND type = 'wanted' ORDER BY datemade DESC");
	$i=1;
	if( $bids ) {
	$rowclass = '';
	foreach($bids as $bid) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$acceptance = $bid->status;
	if( empty( $bid->status ) || $bid->status != "accepted" ) $status = '<a title="'.__('Click here to accept this offer!', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['winner'], '?_choose_winner=1', 'id='.$bid->id.'').'">'.__('Accept','auctionPlugin').'</a>';
	else $status = '<font color="green"><strong>'.__('Accepted','auctionPlugin').'</strong></font>';

?>

<div id="msg<?php echo $bid->id; ?>" class="wanted">
	<div>
	<p align="left"><strong><?php _e('Product Description', 'auctionPlugin'); ?></strong></p>
	<?php if( empty($bid->message) ) echo ''.__('Sorry, there have been no description of the product.','auctionPlugin').''; else echo $bid->message; ?>
	<p align="right">[<a href="#" onclick="msg(<?php echo $bid->id; ?>)"><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
	</div>
</div>

<?php
	global $post, $cpurl;
	$post = get_post($bid->pid);

	$user_info = get_userdata($post->post_author);
	$wads_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'"><strong>'.truncate(strip_tags($post->post_title), 12, '..').'</strong></a>';

   echo '<tr id="'.$bid->id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column">'; if( $post->post_status == "publish" ) echo ''.$wads_title.''; else echo '<strong>'.truncate(strip_tags($post->post_title), 12, '..').'</strong>'; echo '</td>
	<td class="text-right">'.cp_display_price( $bid->bid, 'ad', false ).'</td>
	<td class="text-center footable-last-column"><a href="#" title="'.__('Click here to read the message!', 'auctionPlugin').'" onclick="msg(\''.$bid->id.'\')">'.__('Message', 'auctionPlugin').'</a> | <a title="'.__('Click here to contact the offeror!', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$bid->uid.'').'">'.__('Contact', 'auctionPlugin').'</a> | '.$status.''; if( $acceptance != "accepted" ) echo ' | <a href="#" class="delete-wanted-offer" title="'.__('click here to reject this offer.', 'auctionPlugin').'" id="'.$bid->id.'">'.__('Reject', 'auctionPlugin').'</a></td>
	</tr>';

	}
	
} else {
   echo '<tr><td colspan="3">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

<?php } ?><!-- /admins only -->

	<div class="clear20"></div>

			</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

</div><!-- /wanted - tab_content -->

	<?php } ?>

	<?php if( get_option( 'aws_bo_enable_offer' ) == "yes" ) { ?>

    <div id="boffer">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('Best Offers', 'auctionPlugin'); ?></span></strong></span> - <?php _e('Here are the bids received on your ads, this is bids from users who want to negotiate price etc. You can accept the bids, reject, or make counter proposal.', 'auctionPlugin'); ?></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

<h1 class="single dotted"><?php _e('My Best Offers', 'auctionPlugin'); ?></h1>

	    	<?php if( $bo_error ) echo '<div class="error">'.$bo_error.'</div><div class="clear10"></div>'; ?>
	    	<?php if( $bo_success ) echo '<div class="success">'.$bo_success.'</div><div class="clear10"></div>'; ?>

	<p><?php echo sprintf(__('In the field Status you will get the result, accepted, rejected, or counteroffer displayed like this, ie. <strong>Amount | Accept</strong>. All offers are valid for <strong>%s</strong> hours so if you do not do anything against the received counter offer within the time deadline the counteroffer will expire.', 'auctionPlugin'), $exp_time ); ?></p>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
	<thead>
	<tr>
	<th class="footable-first-column" style="width:90px"><div style="text-align: left;">&nbsp; <?php _e('Date', 'auctionPlugin'); ?></div></th>
	<th style="width:100px"><div style="text-align: left;"><?php _e('Ads', 'auctionPlugin'); ?></div></th>
	<th data-hide="phone" style="width:60px"><div style="text-align: left;"><?php _e('Author', 'auctionPlugin'); ?></div></th>
	<th data-hide="phone" style="width:100px"><div style="text-align: center;"><?php _e('Offer', 'auctionPlugin'); ?></div></th>
	<th data-hide="phone" style="width:180px"><div style="text-align: center;"><?php _e('Status', 'auctionPlugin'); ?></div></th>
	<th class="footable-last-column" data-hide="phone" style="width:80px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
	</tr>
	</thead>
	<tbody>

<?php

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$numposts = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND type IN ('bestoffer', 'boffer-item','boffer-ads')");
	$i=0;
	$exs_bid = false;
	if( $numposts ) {
	$rowclass = '';
        foreach( $numposts as $num ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$i++;

	$id = $num->id;
	$pid = $num->pid;
	$datemade = $num->datemade;
	$bid = $num->bid;
	$uid = $num->uid;
	$uid_counter_offer = $num->uid_counter_offer;
	$aut_counter_offer = $num->aut_counter_offer;
	$acceptance = $num->acceptance;

?>

<div id="msg<?php echo $num->id; ?>" class="wanted">
	<div>
	<p align="left"><strong><?php _e('Product Description', 'auctionPlugin'); ?></strong></p>
	<?php if( empty($num->message) ) echo ''.__('Sorry, there have been no description of the product.','auctionPlugin').''; else echo $num->message; ?>
	<p align="right">[<a href="#" onclick="msg(<?php echo $num->id; ?>)"><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
	</div>
</div>

<?php

	global $post, $posts;
	$post = get_post( $pid );
	if( $num->product_pid > 0 ) $posts = get_post( $num->product_pid );
	$date = date_i18n('d M. y', $num->datemade, true);
	$user = get_userdata($post->post_author);
	if( $aut_counter_offer > 0 ) $coffer = ''.cp_display_price( $aut_counter_offer, 'ad', false ).' | <a href="#" class="accept-offer" title="'.__('Accept their counter offer!', 'auctionPlugin').'" id="'.$id.'">'.__('Accept', 'auctionPlugin').'</a><br /><a title="'.__('Submit a counter offer!', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'id='.$id.'&counter=1').'">'.__('Counter Offer', 'auctionPlugin').'</a>';
	else $coffer = ''.__('Pending', 'auctionPlugin').'';
	if( $acceptance ) $coffer = $acceptance;
	elseif(( empty($aut_counter_offer) ) && ( empty($acceptance) )) $coffer = ''.__('Pending', 'auctionPlugin').'';
	if( $coffer == "Declined" ) $coffer = '<span class="declined">'.__('Rejected', 'auctionPlugin').'</span>';
	if( $coffer == "Accepted" ) $coffer = '<span class="accepted">'.__('Accepted', 'auctionPlugin').'</span>';
	if(( empty( $acceptance )) && ( $num->uid_date > $num->aut_date )) $coffer = ''.__('Pending', 'auctionPlugin').'';
	if( $uid_counter_offer > $bid ) $bid = $uid_counter_offer;
	if( get_post_status( $pid ) != "publish" ) $coffer = ''.__('Listing Closed', 'auctionPlugin').'';

	if( $num->bid > 0 ) $exs_bid = '<br />+ '.cp_display_price( $bid, 'ad', false ).'';

	if( $num->product_pid > 0 )
	$pro_title = '<a title="'.$posts->post_title.'" href="'.get_permalink($num->product_pid).'" target="_blank">'.truncate(strip_tags($posts->post_title), 15, '..').'</a>';
	$ads_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'">'.truncate(strip_tags($post->post_title), 15, '..').'</a>';
	$author = '<a title="'.$user->user_login.'" href="#">'.truncate(strip_tags($user->user_login), 12, '..').'</a>';

	if( $num->latest_offer > 0 ) $win_bid = $num->latest_offer;
	else $win_bid = $num->bid;

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$paid = $wpdb->get_row("SELECT * FROM {$table_purchases} WHERE pid = '$pid' AND uid = '$post->post_author' AND buyer = '$uid' AND amount = '$win_bid'");
	if( $paid ) $p_status = $paid->status;
	else $p_status = false;

   echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-left">'.$date.'</td>';
	if(( $acceptance == "Accepted" ) || ( get_post_status( $num->pid ) != "publish" ))
	echo '<td class="text-left">'.truncate(strip_tags($post->post_title), 20).'</td>';
	else
	echo '<td class="text-left">'.$ads_title.'</td>';
	echo '<td class="text-left">'.$author.'</td>';
	if( $num->item_title )
	echo '<td class="text-center"><a href="#" title="'.__('Click here to read the message!', 'auctionPlugin').'" onclick="msg(\''.$num->id.'\')">'.truncate(strip_tags($num->item_title), 15, '..').'</a><br />+ '.cp_display_price( $bid, 'ad', false ).'</td>';
	elseif( $num->product_pid > 0 )
	echo '<td class="text-center">'.$pro_title.''.$exs_bid.'</td>';
	else
	echo '<td class="text-right">'.cp_display_price( $bid, 'ad', false ).'</td>';
	if( $num->message )
	echo '<td class="text-center">'.$coffer.'<br /><a href="#" title="'.__('Click here to read the message!', 'auctionPlugin').'" onclick="msg(\''.$num->id.'\')">'.__('Message', 'auctionPlugin').'</a></td>';
	else
	echo '<td class="text-center">'.$coffer.'</td>';
	if( $acceptance == "Accepted" && $p_status == "unpaid" )
	echo '<td class="text-center"><a href="'.cp_auction_url($cpurl['cart'], '?shopping_cart=1').'" title="'.__('Pay for your purchase!', 'auctionPlugin').'">'.__('Make Payment', 'auctionPlugin').'</a></td></tr>';
	else
	echo '<td class="text-center"><a href="#" class="delete-offer" title="'.__('Delete your offer!', 'auctionPlugin').'" id="'.$id.'">'.__('Delete', 'auctionPlugin').'</a></td>
	</tr>';

}
} else {
   echo '<tr><td colspan="6">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

	<?php if( $counter == 1 ) {
	if(isset($_GET['id'])) $id = $_GET['id'];
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$type = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'"); ?>
	<div class="clear20"></div>
	<form method="post" action="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'id='.$id.'&counter=1'); ?>">
	<input type="hidden" name="id" value="<?php echo $id; ?>">
	<div class="counter_wrap">
                <div class="item_detail my_bo">
                <div class="item_detail1"><label for="my_bo">&nbsp; <?php _e('Amount:', 'auctionPlugin'); ?></label></div>
                <div class="item_detail2"><input type="text" name="counter_offer_amount" id="my_bo" value="" tabindex="1" /></div>
                </div>
                <?php if( $type->type != "bestoffer" ) { ?>
                <div class="clear5"></div>
                <div class="item_detail my_bo">
                <div class="item_detail bid"> &nbsp; <label for="message"><?php _e('Enter a message:', 'auctionPlugin'); ?></label>
        	<textarea name="message" id="message" tabindex="2"></textarea>
		</div>
		</div>
		<?php } ?>

		<div class="clear10"></div>

                <input type="submit" name="uid_counter_offer" value="<?php _e('Submit Counter Offer!', 'auctionPlugin'); ?>" class="mbtn btn_orange aws_button" tabindex="3" />

                </div></form>
	<?php } ?>

<?php if( get_option('cp_auction_plugin_admins_only') != "yes" || current_user_can( 'manage_options' ) ) { ?>

	<div class="clear50"></div>

<h1 class="single dotted"><?php _e('Offers Received', 'auctionPlugin'); ?></h1>

<p><?php echo sprintf(__('You can accept the Best Offer and end the listing, reject the Best Offer, or respond with a counter offer. If the buyer doesn`t respond within the time deadline of <strong>%s</strong> hours the counter offer will expire.', 'auctionPlugin'), $exp_time ); ?></p>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
	<thead>
	<tr>
	<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Date', 'auctionPlugin'); ?></div></th>
	<th data-hide="phone" style="width:100px"><div style="text-align: left;"><?php _e('Product', 'auctionPlugin'); ?></div></th>
	<th data-hide="phone" style="width:60px"><div style="text-align: left;"><?php _e('Bidder', 'auctionPlugin'); ?></div></th>
	<th data-hide="phone" style="width:95px"><div style="text-align: center;"><?php _e('Offer', 'auctionPlugin'); ?></div></th>
	<th data-hide="phone" style="width:180px"><div style="text-align: center;"><?php _e('Counter Offer', 'auctionPlugin'); ?><br /><div class="small_text2"><?php _e('Bidder | Author', 'auctionPlugin'); ?></div></div></th>
	<th class="footable-last-column" data-hide="phone" style="width:140px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
	</tr>
	</thead>
	<tbody>

<?php
	$ok = 0;
	$ex_bid = false;
	$has_posts = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_author = '$uid' AND post_status = 'publish'");
	$i=0;
	if( $has_posts ) {
	$rowclass = '';
        foreach( $has_posts as $has ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$i++;
	$post_id = $has->ID;

	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$result = $wpdb->get_results("SELECT * FROM {$table_bids} WHERE pid = '$post_id' AND type IN ('bestoffer', 'boffer-item','boffer-ads')");

	if( $result ) {
	foreach( $result as $res ) {
	$ok++;
	$ids		= $res->id;
	$pid		= $res->pid;
	$datemade	= $res->datemade;
	$bid 		= $res->bid;
	$cid 		= $res->uid;
	$uid_counter_offer = $res->uid_counter_offer;
	$aut_counter_offer = $res->aut_counter_offer;
	$acceptance	= $res->acceptance;

?>

<div id="msg<?php echo $res->id; ?>" class="wanted">
	<div>
	<p align="left"><strong><?php _e('Product Description', 'auctionPlugin'); ?></strong></p>
	<?php if( empty($res->message) ) echo ''.__('Sorry, there have been no description of the product.','auctionPlugin').''; else echo $res->message; ?>
	<p align="right">[<a href="#" onclick="msg(<?php echo $res->id; ?>)"><?php _e('CLOSE', 'auctionPlugin'); ?></a>]</p>
	</div>
</div>

<?php

	global $post, $posts;
	$post = get_post( $pid );
	if( $res->product_pid > 0 ) $posts = get_post( $res->product_pid );
	$date = date_i18n('d M. y', $res->datemade, true);
	$user = get_userdata($cid);
	if( $res->product_pid > 0 )
	$pr_title = '<a title="'.$posts->post_title.'" href="'.get_permalink($res->product_pid).'" target="_blank">'.truncate(strip_tags($posts->post_title), 12, '..').'</a>';
	$ad_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'">'.truncate(strip_tags($post->post_title), 12, '..').'</a>';
	$bidder = '<a title="'.$user->user_login.'" href="#">'.truncate(strip_tags($user->user_login), 12, '..').'</a>';

	if( $res->bid > 0 ) $ex_bid = '<br />+ '.cp_display_price( $bid, 'ad', false ).'';

   echo '<tr id="'.$ids.'" class="'.$rowclass.'">
	<td class="text-left">'.$date.'</td>
	<td class="text-left">'.$ad_title.'</td>
	<td class="text-left">'.$bidder.'</td>';
	if( $res->item_title )
	echo '<td class="text-center"><a href="#" title="'.__('Click here to read the message!', 'auctionPlugin').'" onclick="msg(\''.$res->id.'\')">'.truncate(strip_tags($res->item_title), 12, '..').'</a><br />+ '.cp_display_price( $bid, 'ad', false ).'</td>';
	elseif( $res->product_pid > 0 )
	echo '<td class="text-center">'.$pr_title.''.$ex_bid.'</td>';
	else
	echo '<td class="text-right">'.cp_display_price( $bid, 'ad', false ).'</td>';
	if( $res->message )
	echo '<td class="text-center">'.cp_display_price( $uid_counter_offer, 'ad', false ).' | '.cp_display_price( $aut_counter_offer, 'ad', false ).'<br /><a href="#" title="'.__('Click here to read the message!', 'auctionPlugin').'" onclick="msg(\''.$res->id.'\')">'.__('Message', 'auctionPlugin').'</a></td>';
	else
	echo '<td class="text-center">'.cp_display_price( $uid_counter_offer, 'ad', false ).' | '.cp_display_price( $aut_counter_offer, 'ad', false ).'</td>';
	if( $acceptance == "Accepted" )
	echo '<td class="text-center"><span class="accepted">'.__('Accepted', 'auctionPlugin').'</span></td>';
	elseif( $acceptance == "Declined" )
	echo '<td class="text-center"><span class="declined">'.__('Rejected', 'auctionPlugin').'</span></td>';
	elseif( $res->aut_date > $res->uid_date )
	echo '<td class="text-center"><a href="#" class="decline-offer" title="'.__('Reject their offer!', 'auctionPlugin').'" id="'.$ids.'">'.__('Reject', 'auctionPlugin').'</a></td>';
	else
	echo '<td class="text-center"><a href="#" class="accept-offer" title="'.__('Accept their offer and end your listing!', 'auctionPlugin').'" id="'.$ids.'">'.__('Accept', 'auctionPlugin').'</a> | <a href="#" class="decline-offer" title="'.__('Reject their offer!', 'auctionPlugin').'" id="'.$ids.'">'.__('Reject', 'auctionPlugin').'</a><br /><a title="'.__('Submit a counter offer!', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'ids='.$ids.'&counter=2').'">'.__('Counter Offer', 'auctionPlugin').'</a></td>
	</tr>';
	}
	}
}
}
	if(( empty( $has_posts ) ) || ( $ok == 0 )) {
   	echo '<tr><td colspan="6">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
	}
	?>
	</tbody>
	</table>

	<?php if( $counter == 2 ) {
	if(isset($_GET['ids'])) $ids = $_GET['ids'];
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$type = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$ids'"); ?>
	<div class="clear20"></div>
	<form method="post" action="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1', 'ids='.$ids.'&counter=2'); ?>">
	<input type="hidden" name="id" value="<?php echo $ids; ?>">
	<div class="counter_wrap">
                <div class="item_detail my_bo">
                <div class="item_detail1"><label for="my_bo">&nbsp; <?php _e('Amount:', 'auctionPlugin'); ?></label></div>
                <div class="item_detail2"><input type="text" name="counter_offer_amount" id="my_bo" value="" tabindex="1" /></div>
                </div>
                <?php if( $type->type != "bestoffer" ) { ?>
                <div class="clear5"></div>
                <div class="item_detail my_bo">
                <div class="item_detail bid"> &nbsp; <label for="message"><?php _e('Enter a message:', 'auctionPlugin'); ?></label>
        	<textarea name="message" id="message" tabindex="2"></textarea>
		</div>
		</div>
		<?php } ?>

		<div class="clear10"></div>

                <input type="submit" name="aut_counter_offer" value="<?php _e('Submit Counter Offer!', 'auctionPlugin'); ?>" class="mbtn btn_orange aws_button" tabindex="3" />

                </div></form>
	<?php } ?>

<?php } ?><!-- /admins only -->

	<div class="clear20"></div>

			</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

</div><!-- /boffer - tab_content -->

	<?php } ?><!-- /best offer -->

	<?php if(( get_option('cp_auction_plugin_auctiontype') == "normal" || get_option('cp_auction_plugin_auctiontype') == "reverse" ) 
	|| ( get_option('cp_auction_plugin_auctiontype') == "mixed" && ( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true ))) { ?>

    <div id="bid">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('My Bids', 'auctionPlugin'); ?></span></strong></span></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

<h1 class="single dotted"><?php _e('Auctions I Bid', 'auctionPlugin'); ?></h1>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Auction', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:90px"><?php _e('Last Bid', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:90px"><?php _e('My Bid', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:50px"><?php _e('Winner', 'auctionPlugin'); ?></th>
		<th class="footable-last-column" data-hide="phone" style="width:180px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$bids = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' AND type = 'bid' ORDER BY id DESC");
	$i=1;
	if( $bids ) {
	$rowclass = '';
	foreach($bids as $bid) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

	$id = $bid->id;
	$pid = $bid->pid;
	$my_bid = $bid->bid;
	if( $bid->winner > 0 ) $winner = '<strong>'.__('Yes','auctionPlugin').'</strong>';
	else $winner = ''.__('No','auctionPlugin').'';

	global $post;
	$post = get_post($pid);
	$cbid = $bid->last_bid; //cp_auction_plugin_get_latest_bid($pid);
	
	$user_info = get_userdata($post->post_author);
	$bid_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'">'.truncate(strip_tags($post->post_title), 12, '..').'</a>';

   echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column"><strong>'.$bid_title.'</a></td>
	<td class="text-right">'.cp_display_price( $cbid, 'ad', false ).'</td>
	<td class="text-right">'.cp_display_price( $my_bid, 'ad', false ).'</td>
	<td class="text-center">'.$winner.'</td>
	<td class="text-center footable-last-column"><a href="'.get_author_posts_url($post->post_author).'">'.__('View Profile', 'auctionPlugin').'</a> | <a href="'.cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.'').'">'.__('View Listings', 'auctionPlugin').'</a></td>
	</tr>';

	}
	
} else {
   echo '<tr><td colspan="5">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

<?php if ( get_option('cp_auction_plugin_normal') == "yes" || get_option('cp_auction_plugin_auctiontype') == "normal" ) { ?>

	<div class="clear50"></div>

<h1 class="single dotted"><?php _e('Highest Bid on My Auctions', 'auctionPlugin'); ?></h1>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Auction', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:80px"><div style="text-align: center;">&nbsp; <?php _e('Date', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:90px"><div style="text-align: left;">&nbsp; <?php _e('Bidder', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:60px"><?php _e('Bid', 'auctionPlugin'); ?></th>
		<th class="footable-last-column" data-hide="phone" style="width:180px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$rounds = $wpdb->get_results("SELECT * FROM {$table_name} WHERE bid IN ( SELECT MAX(bid) FROM {$table_name} WHERE post_author = '$uid' AND type = 'bid' GROUP BY pid )");

	$i=0;
	$y=0;
	if( $rounds ) {
	$rowclass = '';
	foreach($rounds as $round) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

	$id = $round->id;
	$pid = $round->pid;
	$buid = $round->uid;
	$highest_bid = $round->bid;
	if( $round->winner > 0 ) $winner = '<strong>'.__('Winner Bid','auctionPlugin').'</strong>';
	else $winner = false;

	global $post;
	$post = get_post($pid);
	$cbid = $round->last_bid; //cp_auction_plugin_get_latest_bid($pid);
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );

	$user_info = get_userdata($buid);
	$bid_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'">'.truncate(strip_tags($post->post_title), 15, '..').'</a>';

	if ( $my_type == "reverse" ) {
		$i++;
	}
	else if ( $my_type == "normal" ) {

   echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column"><strong>'.$bid_title.'</a></td>
	<td class="text-right">'.date_i18n('d/m/y', $round->datemade, true).'</td>
	<td class="text-left"><a title="'.__('View Profile', 'auctionPlugin').'" href="'.get_author_posts_url($buid).'">'.$user_info->user_login.'</a></td>
	<td class="text-right">'.cp_display_price( $highest_bid, 'ad', false ).'</td>
	<td class="text-center footable-last-column">'; if( $winner ) echo ''.$winner.' | '; else if( get_option('cp_auction_plugin_auctiontype') == "reverse" || $my_type == "reverse" || get_option('cp_auction_enable_freebids') == "yes" ) echo '<a title="'.__('Accept this bid and end your auction.', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['winner'], '?_choose_winner='.$round->id.'', 'id='.$round->id.'').'">'.__('Accept Bid','auctionPlugin').'</a> | '; if( get_option('cp_auction_plugin_owner_delete') == "yes" || current_user_can( 'manage_options' ) ) echo '<a href="#" class="delete-bid" title="'.__('You should delete only faulty & fraudelent bids.', 'auctionPlugin').'" id="'.$round->id.'">'.__('Delete','auctionPlugin').'</a>'; echo '</td>
	</tr>';

	$y++;
	}
	}

	if ( $i > 0 && $y < 1 ) echo '<tr><td colspan="5">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';

} else {
   echo '<tr><td colspan="5">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

<?php } if ( get_option('cp_auction_plugin_reverse') == "yes" || get_option('cp_auction_plugin_auctiontype') == "reverse" ) { ?>

	<div class="clear50"></div>

<h1 class="single dotted"><?php _e('My Reverse Auctions', 'auctionPlugin'); ?></h1>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Auction', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:80px"><div style="text-align: center;">&nbsp; <?php _e('Date', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:90px"><div style="text-align: left;">&nbsp; <?php _e('Bidder', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:60px"><?php _e('Bid', 'auctionPlugin'); ?></th>
		<th class="footable-last-column" data-hide="phone" style="width:180px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$revs = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_author = '$uid' AND type = 'bid' ORDER BY id DESC");

	$i=0;
	$y=0;
	if( $revs ) {
	$rowclass = '';
	foreach($revs as $rev) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

	$id = $rev->id;
	$pid = $rev->pid;
	$buid = $rev->uid;
	$bid = $rev->bid;
	if( $rev->winner > 0 ) $winner = '<strong>'.__('Winner Bid','auctionPlugin').'</strong>';
	else $winner = false;

	global $post;
	$post = get_post($pid);
	$cbid = $rev->last_bid; //cp_auction_plugin_get_latest_bid($pid);
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );
	
	$user_info = get_userdata($buid);
	$rev_title = '<a title="'.$post->post_title.'" href="'.get_permalink($pid).'">'.truncate(strip_tags($post->post_title), 12, '..').'</a>';

	if ( $my_type == "normal" ) {
		$i++;
	}
	else if ( $my_type == "reverse" ) {

  echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column"><strong>'.$rev_title.'</a></td>
	<td class="text-right">'.date_i18n('d/m/y', $rev->datemade, true).'</td>
	<td class="text-left"><a title="'.__('View Profile', 'auctionPlugin').'" href="'.get_author_posts_url($buid).'">'.$user_info->user_login.'</a></td>
	<td class="text-right">'.cp_display_price( $bid, 'ad', false ).'</td>';
	if( $post->post_status !== "publish" && ! $winner ) {
   echo '<td class="text-center footable-last-column">'; echo ''.__('Rejected','auctionPlugin').' '; if( get_option('cp_auction_plugin_owner_delete') == "yes" || current_user_can( 'manage_options' ) ) echo '| <a href="#" class="delete-bid" title="'.__('You should delete only faulty & fraudelent bids.', 'auctionPlugin').'" id="'.$rev->id.'">'.__('Delete','auctionPlugin').'</a>';
	}
	else {
   echo '<td class="text-center footable-last-column">'; if( $winner ) echo ''.$winner.' | <a title="'.__('Click here to contact the bidder!', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$buid.'').'">'.__('Contact', 'auctionPlugin').'</a> | '; else if( get_option('cp_auction_plugin_auctiontype') == "reverse" || $my_type == "reverse" || get_option('cp_auction_enable_freebids') == "yes" ) echo '<a title="'.__('Accept this bid and end your auction.', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['winner'], '?_choose_winner='.$rev->id.'', 'id='.$rev->id.'').'">'.__('Accept Bid','auctionPlugin').'</a> | '; if( get_option('cp_auction_plugin_owner_delete') == "yes" || current_user_can( 'manage_options' ) ) echo '<a href="#" class="delete-bid" title="'.__('You should delete only faulty & fraudelent bids.', 'auctionPlugin').'" id="'.$rev->id.'">'.__('Delete','auctionPlugin').'</a>';
	}
   echo '</td>
	</tr>';

	$y++;
	}
	}

	if ( $i > 0 && $y < 1 ) echo '<tr><td colspan="5">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';

} else {
   echo '<tr><td colspan="5">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

<?php } ?>

	<div class="clear20"></div>

			</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

</div><!-- /bid - tab_content -->

	<?php } ?><!-- /bid -->

<?php if (class_exists("cartpaujPM")) { ?>

<div id="pms">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e( 'My Messages', 'auctionPlugin' ); ?></span></strong></span></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

	<?php
	global $wpdb;
	$pageID =  $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE post_content LIKE '%[cartpauj-pm]%' AND post_status = 'publish' AND post_type = 'page'");
	$include = get_pages('include='.$pageID.'');
	$content = apply_filters('the_content',$include[0]->post_content);
	echo $content; ?>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

</div><!-- /pms -->

<?php } ?>

	    </div><!-- /tab_container -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
}
?>