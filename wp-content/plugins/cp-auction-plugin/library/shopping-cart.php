<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $post, $wpdb, $current_user, $cp_options, $cpurl;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$user = get_userdata( $uid );
	$ip_address = cp_auction_users_ip();
	$available_credits = get_user_meta( $uid, 'cp_credits', true );
	if($available_credits == "") $available_credits = 0;
	$value = $available_credits * get_option('cp_auction_plugin_credit_value');
	$error = false; $success = false; $sum = false; $sum_shipping = false; $mc_fee = false;

	$seller = false; if ( isset( $_GET['confirm-order'] ) ) $seller = $_GET['confirm-order'];
	$post_id = false; if ( isset( $_POST['pid'] ) ) $post_id = $_POST['pid'];
	$cs = get_user_meta($seller, 'cp_currency', true );
	if( empty( $cs ) ) $cs = $cp_options->curr_symbol;

	if ( isset( $_POST['args'] ) ) {
	$arg = false; if(isset($_POST['args'])) $arg = unserialize( base64_decode( $_POST['args'] ) );
	$order_id = false; if( isset( $arg['order_id'] ) ) $order_id = $arg['order_id'];

	$table_name = $wpdb->prefix . "cp_auction_plugin_transactions";
	$reg = $wpdb->get_row("SELECT * FROM {$table_name} WHERE order_id = '$order_id' AND uid = '$uid' AND status = 'pending'");

	if( $reg ) {
	$lastid = $reg->id;
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$where_array = array('id' => $lastid);
	$wpdb->update($table_transactions, array('trans_type' => $arg['trans_type']), $where_array);
	}
	else {

	$approves = "pending";

	$query = "INSERT INTO {$table_name} (order_id, post_ids, datemade, uid, post_author, ip_address, payment_date, txn_id, item_name, trans_type, mc_currency, last_name, first_name, payer_email, receiver_email, address_country, address_state, address_country_code, address_zip, address_street, moved_credits, available, quantity, payment_option, shipping, mc_fee, mc_gross, aws, type, status) VALUES (%d, %s, %d, %d, %d, %s, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %f, %f, %d, %s, %f, %f, %f, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $order_id, $arg['post_ids'], $arg['datemade'], $uid, $arg['post_author'], $arg['ip_address'], $arg['payment_date'], 'N/A', '', $arg['trans_type'], $arg['mc_currency'], $arg['last_name'], $arg['first_name'], $arg['payer_email'], $arg['receiver_email'], $arg['address_country'], $arg['address_state'], 'N/A', $arg['address_zip'], $arg['address_street'], '', '', $arg['quantity'], $arg['howto_pay'], $arg['cp_shipping'], $arg['mc_fee'], $arg['mc_gross'], 'N/A', $arg['type'], $approves));
	$lastid = $wpdb->insert_id;
	}

	$tm = time();
	$post_ids = $arg['post_ids'];
	$pids = explode(",",$post_ids);
	foreach($pids as $pid) {
	$post = get_post($pid);
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('pid' => $post->ID, 'buyer' => $uid, 'uid' => $post->post_author, 'status' => 'unpaid');
	$wpdb->update($table_purchases, array('trans_id' => $lastid, 'paid' => 2), $where_array);
	}

	delete_option('cp_auction_purchase_'.$uid.'');
	delete_user_meta( $uid, 'cp_auction_coupon_code' );
	cp_auction_cih_cod_email($order_id, $arg['post_author'], $uid, $arg['quantity'], $arg['mc_fee'], $arg['mc_gross'], $arg['howto_pay']);
	wp_redirect(get_bloginfo('wpurl').'/?confirmation=1&type='.$arg['type'].'&success='.$arg['howto_pay'].'');
	exit;
}

	if ( isset( $_POST['process-order'] ) ) {
	$first_name = isset( $_POST['first_name'] ) ? esc_attr( $_POST['first_name'] ) : '';
	$last_name = isset( $_POST['last_name'] ) ? esc_attr( $_POST['last_name'] ) : '';
	$cp_street = isset( $_POST['cp_street'] ) ? esc_attr( $_POST['cp_street'] ) : '';
	$cp_zipcode = isset( $_POST['cp_zipcode'] ) ? esc_attr( $_POST['cp_zipcode'] ) : '';
	$cp_city = isset( $_POST['cp_city'] ) ? esc_attr( $_POST['cp_city'] ) : '';
	$cp_country = isset( $_POST['cp_country'] ) ? esc_attr( $_POST['cp_country'] ) : '';
	$shipto = cp_auction_buyers_country( $_POST['pid'], $_POST['seller'], $cp_country );
	$user_email = isset( $_POST['user_email'] ) ? esc_attr( $_POST['user_email'] ) : '';
	$cp_phone = isset( $_POST['cp_phone'] ) ? esc_attr( $_POST['cp_phone'] ) : '';
	$payment_option = isset( $_POST['option'] ) ? esc_attr( $_POST['option'] ) : '';

	if( $first_name ) {
	update_user_meta( $uid, 'first_name', $first_name );
	} else {
	$error = ''.__('Please fill in the required fields.', 'auctionPlugin').'';
	$first_label = '<font color="red">'.__('First Name', 'auctionPlugin').'</font>';
	}
	if( $last_name ) {
	update_user_meta( $uid, 'last_name', $last_name );
	} else {
	$error = ''.__('Please fill in the required fields.', 'auctionPlugin').'';
	$last_label = '<font color="red">'.__('Last Name', 'auctionPlugin').'</font>';
	}
	if( $cp_street ) {
	update_user_meta( $uid, 'cp_street', $cp_street );
	} else {
	$error = ''.__('Please fill in the required fields.', 'auctionPlugin').'';
	$street_label = '<font color="red">'.__('Address', 'auctionPlugin').'</font>';
	}
	if( $cp_zipcode ) {
	update_user_meta( $uid, 'cp_zipcode', $cp_zipcode );
	} else {
	$error = ''.__('Please fill in the required fields.', 'auctionPlugin').'';
	$zip_label = '<font color="red">'.__('Postcode / Zip', 'auctionPlugin').'</font>';
	}
	if( $cp_city ) {
	update_user_meta( $uid, 'cp_city', $cp_city );
	} else {
	$error = ''.__('Please fill in the required fields.', 'auctionPlugin').'';
	$city_label = '<font color="red">'.__('Town / City', 'auctionPlugin').'</font>';
	}
	if( $shipto == "no" ) {
	$error = ''.sprintf( __( 'This seller does not deliver to %s.', 'auctionPlugin' ), $cp_country ).'';
	$country_label = '<font color="red">'.__('Country', 'auctionPlugin').'</font>';
	}
	if( $cp_country && $shipto != "no" ) {
	update_user_meta( $uid, 'cp_country', $cp_country );
	} else {
	if( $shipto == "no" )
	$error = ''.sprintf( __( 'This seller does not deliver to %s.', 'auctionPlugin' ), $cp_country ).'';
	else $error = ''.__('Please fill in the required fields.', 'auctionPlugin').'';
	$country_label = '<font color="red">'.__('Country', 'auctionPlugin').'</font>';
	}
	if( $user_email ) {
	update_user_meta( $uid, 'user_email', $user_email );
	} else {
	$error = ''.__('Please fill in the required fields.', 'auctionPlugin').'';
	$email_label = '<font color="red">'.__('Email Address', 'auctionPlugin').'</font>';
	}
	if( $cp_phone ) {
	update_user_meta( $uid, 'cp_phone', $cp_phone );
	} else {
	//$error = ''.__('Please fill in the required fields.', 'auctionPlugin').'';
	}
	if( !$payment_option ) {
	$error = ''.__('Please select a payment method.', 'auctionPlugin').'';
	}
	if ( !$error ) {
	$success = true;
	$option = isset( $_POST['option'] ) ? esc_attr( $_POST['option'] ) : '';
	}
	}

	$enabl = get_option( 'cp_auction_enable_conversion' );
	$cuto = get_option( 'cp_auction_convert_to' );

	cp_auction_header(); ?>

<script type="text/javascript">
	function update_pending(id)
	{
		 var quantity = jQuery("#quantity_"+id).val();
		 var unpaid = jQuery("#unpaid_"+id).val();

		 jQuery.ajax({
				method: 'get',
				url : '<?php echo cp_auction_plugin_url('url');?>/index.php/?_update_pending='+id+'&quantity='+quantity+'&unpaid='+unpaid,
				dataType : 'text',
				success: function() { window.location.reload(true); }
				});
}
</script>

	<div class="shadowblock_out">

		<div class="shadowblock">

			<div id="main-cart" class="aws-box">

	<h2 class="dotted"><?php echo the_title(); ?></h2>

	<?php if ( $error ) { ?>
	<div class="notice error"><?php echo "".$error.""; ?></div>
	<?php } if ( $success ) { ?>

<p><?php _e( 'The order is now registered as pending payment, quantity can no longer be adjusted!', 'auctionPlugin' ); ?></p>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column"><div style="text-align: left;"><?php _e('Product', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone"><?php _e('Price', 'auctionPlugin'); ?></th>
		<th data-hide="phone"><?php _e('Quantity', 'auctionPlugin'); ?></th>
		<th class="footable-last-column" data-hide="phone"><?php _e('Total', 'auctionPlugin'); ?></th>
		</tr>
		</thead>
		<tbody>

<?php
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$prods = $wpdb->get_results( "SELECT * FROM {$table_name} WHERE uid = '$seller' AND buyer = '$uid' AND unpaid > '0' AND paid != '2'" );
	if( $prods ) {
	foreach($prods as $prod) {

	if ( $prod->ref != "process" ) {

	$quantity = $prod->unpaid;
	$my_type = get_post_meta( $prod->pid, 'cp_auction_my_auction_type', true );
	$bid = $prod->amount;
	$howmany = get_post_meta( $prod->pid, 'cp_auction_howmany', true );
	$howmany_added = get_post_meta( $prod->pid, 'cp_auction_howmany_added', true );

	if( $my_type == "normal" || $my_type == "reverse" ) {
	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$query = "INSERT INTO {$table_bids} (pid, datemade, bid, uid, winner, type) VALUES (%d, %d, %f, %d, %d, %s)"; 
	$wpdb->query($wpdb->prepare($query, $prod->pid, $tm, $prod->amount, $uid, '1', 'classified'));

	$my_post = array();
	$my_post['ID'] = $prod->pid;
	$my_post['post_status'] = 'draft';
	wp_update_post( $my_post );

	update_post_meta( $prod->pid,'cp_ad_sold', 'yes' );
	update_post_meta( $prod->pid,'cp_ad_sold_date', $tm );
	}

	if( $howmany > '0' || $howmany_added == "yes" ) {
	if( $prod->unpaid > $howmany ) $quantity = $howmany;
	$how_many = $howmany - $quantity;
	update_post_meta( $prod->pid,'cp_auction_howmany', $how_many );
	if( $how_many < 1 ) {
	update_post_meta( $prod->pid, 'cp_ad_sold', 'yes' );
	update_post_meta( $prod->pid, 'cp_ad_sold_date', $tm );
	update_post_meta( $prod->pid, 'cp_auction_howmany', '0' );
	update_post_meta( $prod->pid, 'cp_auction_howmany_added', 'none' );
	}
	}
	else {
	if( $howmany != '0' || $howmany_added == "none" ) {
	update_post_meta( $prod->pid, 'cp_ad_sold', 'yes' );
	update_post_meta( $prod->pid, 'cp_ad_sold_date', $tm );
	update_post_meta( $prod->pid, 'cp_auction_howmany', '0' );
	}
	elseif( $howmany_added == "unlimited" ) {
	delete_post_meta( $prod->pid, 'cp_ad_sold' );
	delete_post_meta( $prod->pid, 'cp_ad_sold_date' );
	}
	}

	$tot = ($prod->amount - $prod->discount) * $quantity;
	$unpaid_tot = ($prod->amount - $prod->discount) * $quantity;
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$where_array = array('id' => $prod->id);
	$wpdb->update($table_name, array('unpaid' => $quantity, 'net_total' => $tot, 'unpaid_total' => $unpaid_tot, 'ref' => 'process'), $where_array);

	}

	}
	}

	$sub_total = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(net_total) FROM {$table_name} WHERE uid = %s AND buyer = %s AND unpaid > %s AND paid != %s", $seller, $uid, '0', '2' ) );
	$sum_unpaid = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(unpaid) FROM {$table_name} WHERE uid = %s AND buyer = %s AND unpaid > %s AND paid != %s", $seller, $uid, '0', '2' ) );

	$country = get_user_meta( $uid, 'cp_country', true );
	if ( empty( $country ) && get_option('cp_auction_enable_geoip') == "yes" )
	$country = do_shortcode( '[geoip_detect2 property="country"]' );

	$free_shipping = get_user_meta( $seller, 'cp_auction_free_shipping', true );
	$shipping = cp_auction_calculate_shipping( $seller, $uid );
	if ( ( $free_shipping > '0' ) && ( $sub_total >= $free_shipping ) ) $shipping = false;
	if( $option == "cih" ) $shipping = false;

	$sum = ($sub_total + $shipping);

	$amounts = cp_auction_calculate_end_fees($option, $sum, $seller);
	$values	= explode("|",$amounts);
	if( isset( $values[0] ) ) $mc_gross = $values[0];
	if( isset( $values[1] ) ) $mc_fee = $values[1];
	if( isset( $values[2] ) ) $fees = $values[2];
	if( isset( $values[3] ) ) $pb = $values[3];

	$tax = get_user_meta( $seller, 'cp_auction_tax_vat', true );
	if(( empty( $tax )) || ( $tax == 0 )) $tax = 0;
	$taxes = cp_auction_taxes( $mc_gross, $tax );

	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$order = $wpdb->get_var( "SELECT order_id FROM {$table_name} WHERE uid = '$seller' AND buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY order_id DESC" );
	if ( $order < 1 )
	$order = $wpdb->get_var( "SELECT order_id FROM {$table_name} ORDER BY order_id DESC" );
	else $order = $order - 1;

	$items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$seller' AND buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY datemade ASC", ARRAY_A);
	$i=1;
	if( $items ) {
	$empty = false;
	foreach($items as $item) {

	$id = $item['id'];
	$order_id = $item['order_id'];
	$pid = $item['pid'];
	$post = get_post($pid);
	$item_name = $item['item_name'];
	$amount = $item['amount'];
	$discount = $item['discount'];
	$special_delivery = $item['special_delivery'];
	$cp_shipping = $item['shipping'];
	$unpaid = $item['unpaid'];
	$net_total = $item['net_total'];
	$total = ($amount - $discount) * $unpaid;
	$item_price = $amount - $discount;
	$ads_url = '<a title="'.$item_name.'" href="'.get_permalink($pid).'">'.$item_name.'</a>';
	$author = get_userdata( $seller );
	$sum_fees = ($special_delivery + $cp_shipping + $net_total);
	$calc_fees = cp_auction_calculate_end_fees($option, $sum_fees, $seller);
	$value	= explode("|",$calc_fees);
	if( isset( $value[1] ) ) $mcfee = $value[1];
	$unpaid_total = ($net_total + $mcfee + $special_delivery);

   echo '<tr id="tr_'.$id.'" class="even">
	<td class="text-left">'.$ads_url.'</td>
	<td class="text-right">'.cp_display_price( $item_price, $cs, false ).'</td>';
   echo '<td align="center">'.$unpaid.'</td>';
   echo '<td class="text-right footable-last-column">'.cp_display_price( $total, $cs, false ).'</td>
	</tr>';

	$options = get_option('cp_auction_purchase_'.$uid.'');
	if( isset( $seller ) ) {
	$list = explode(',', $options[$seller]);
	if( ! in_array( $pid, $list ) ) {
	@$options[$seller] .= ''.$pid.',';
	}
	if ( ! empty( $options ) )
   	update_option('cp_auction_purchase_'.$uid.'', $options);
	}
	if ( empty( $order_id ) || $order_id < 1 ) {
	$order_id = $order + 1;
	$where_array = array('id' => $id);
	if ( $option == "cih" ) {
	$wpdb->update($table_name, array('order_id' => $order_id, 'special_delivery' => '0.00', 'shipping' => '0.00', 'mc_fee' => '0.00', 'tax' => $taxes, 'unpaid_total' => $net_total), $where_array);
	} else {
	$wpdb->update($table_name, array('order_id' => $order_id, 'mc_fee' => $mcfee, 'tax' => $taxes, 'unpaid_total' => $unpaid_total), $where_array);
	}
	}
	}

	if( $mc_gross > 0 ) {
		$cto = false; 
		if( $option == 'paypal' ) {
		if ( $enabl == "yes" && class_exists("Currencyr") ) $cto = '('.currencyr_exchange( $mc_gross, strtolower( $cuto ) ).' '.$cuto.')';
		}
	echo '<tr class="sub"><td colspan="3" class="text-right"><strong>'.__('Sub Total:', 'auctionPlugin').'</td><td class="text-right"><strong>'.cp_display_price( $sub_total, $cs, false ).'</td></tr>';
	if ( $shipping > 0 )
	echo '<tr class="sub"><td colspan="3" class="text-right"><strong>'.__('Shipping:', 'auctionPlugin').'</td><td class="text-right"><strong>'.cp_display_price( $shipping, $cs, false ).'</td></tr>';
	if ( $option != "cod" && $fees > 0 )
	echo '<tr class="sub"><td colspan="3" class="text-right"><strong>'.sprintf(__('Transaction Fees %s%s:', 'auctionPlugin'), $fees, $pb).'</td><td class="text-right"><strong>'.cp_display_price( $mc_fee, $cs, false ).'</td></tr>';
	if ( $option == "cod" && $fees > 0 )
	echo '<tr class="sub"><td colspan="3" class="text-right"><strong>'.__('COD Fees:', 'auctionPlugin').'</td><td class="text-right"><strong>'.cp_display_price( $mc_fee, $cs, false ).'</td></tr>';
	if ( $tax > 0 )
	echo '<tr class="sub"><td colspan="3" class="text-right"><strong>'.sprintf(__('Hereof %s%s Tax / VAT:', 'auctionPlugin'), $tax, '%').'</td><td class="text-right"><strong>'.cp_display_price( $taxes, $cs, false ).'</td></tr>';
	echo '<tr class="total"><td colspan="3" class="text-right"><strong>'.__('Order Total:', 'auctionPlugin').'</td><td class="text-right"><strong>'.cp_display_price( $mc_gross, $cs, false ).'</strong> '.$cto.'</td></tr>';

	}
}
else {
   $empty = true;
   echo '<tr><td colspan="4">'.__('Your cart is currently empty!', 'auctionPlugin').'</td></tr>';
   }

	?>
	</tbody>
	</table>

<div class="clear5"></div>

<p style="text-align: center; float: right;"><a href="<?php echo cp_auction_url($cpurl['cart'], '?shopping_cart=1'); ?>"><?php _e('Return to Shopping Cart', 'auctionPlugin'); ?></a></p>

<?php
	if( $option == 'cih' ) {
	echo '<form class="payform" action="" method="post" enctype="multipart/form-data">';
	
	$mc_currency = get_user_meta( $seller, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;

	$uid_data = get_userdata( $uid );
	$seller_data = get_userdata( $seller );

	$options = get_option('cp_auction_purchase_'.$uid.'');
	$array = explode(',', $options[$seller]);
	$list = implode(",", $array);
	$post_ids = trim($list, ',');

	$cih_arg = array(
	'order_id' => $order_id,
	'post_ids' => $post_ids,
	'post_author' => $seller,
	'uid' => $uid,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'trans_type' => 'Cash in Hand',
	'mc_currency' => $mc_currency,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $uid_data->user_email,
	'receiver_email' => $seller_data->user_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'quantity' => $sum_unpaid,
	'mc_fee' => '0.00',
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'howto_pay' => 'cih',
	'cp_shipping' => '',
	'type' => 'purchase'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $cih_arg ) ).'">';
	echo '<input type="submit" name="submit" value="'.__('Confirm','auctionPlugin').'" class="mbtn btn_orange cp-width" />';
	echo '</form>';
	}
	elseif( $option == 'cod' ) {
	echo '<form class="payform" action="" method="post" enctype="multipart/form-data">';

	$mc_currency = get_user_meta( $seller, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;

	$uid_data = get_userdata( $uid );
	$seller_data = get_userdata( $seller );

	$options = get_option('cp_auction_purchase_'.$uid.'');
	$array = explode(',', $options[$seller]);
	$list = implode(",", $array);
	$post_ids = trim($list, ',');

	$cod_arg = array(
	'order_id' => $order_id,
	'post_ids' => $post_ids,
	'post_author' => $seller,
	'uid' => $uid,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'trans_type' => 'Cash on Delivery',
	'mc_currency' => $mc_currency,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $uid_data->user_email,
	'receiver_email' => $seller_data->user_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'quantity' => $sum_unpaid,
	'mc_fee' => cp_auction_sanitize_amount($mc_fee),
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'howto_pay' => 'cod',
	'cp_shipping' => $shipping,
	'type' => 'purchase'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $cod_arg ) ).'">';
	echo '<input type="submit" name="submit" value="'.__('Confirm','auctionPlugin').'" class="mbtn btn_orange cp-width" />';
	echo '</form>';

	// Bank Transfer Payment
        } elseif( $option == 'bank_transfer' ) {
        echo '<form class="payform" action="'.get_bloginfo('wpurl').'/?_pay_bank_transfer=1" method="post" enctype="multipart/form-data">';

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;

	$mc_currency = get_user_meta( $seller, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;

	$options = get_option('cp_auction_purchase_'.$uid.'');
	$array = explode(',', $options[$seller]);
	$list = implode(",", $array);
	$post_ids = trim($list, ',');

	$bt_arg = array(
	'order_id' => $order_id,
	'post_ids' => $post_ids,
	'post_author' => $seller,
	'uid' => $uid,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'txn_id' => 'UBT'.$pid.'A'.time().'P',
	'item_name' => '',
	'trans_type' => 'Bank Transfer',
	'mc_currency' => $mc_currency,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'quantity' => $sum_unpaid,
	'mc_fee' => cp_auction_sanitize_amount($mc_fee),
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'howto_pay' => 'bank_transfer',
	'cp_shipping' => $shipping,
	'type' => 'purchase'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $bt_arg ) ).'">';

        echo '<input type="submit" name="submit_bank_transfer" value="'.__('Confirm','auctionPlugin').'" class="mbtn btn_orange cp-width" />';
	echo '</form>';
        }
	elseif( $option == 'paypal' ) {
	// PayPal Payment, directly to seller..
	echo '<form class="payform" action="'.get_bloginfo('wpurl').'/?_process_payment=1" method="post" enctype="multipart/form-data">';

	$receiver_email = get_user_meta( $seller, 'paypal_email', true );

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;

	$mc_currency = get_user_meta( $seller, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;

	$conversion = false;
	if ( $enabl == "yes" && class_exists("Currencyr") )
	$conversion = currencyr_exchange( $mc_gross, strtolower( $cuto ) );

	$options = get_option('cp_auction_purchase_'.$uid.'');
	$array = explode(',', $options[$seller]);
	$list = implode(",", $array);
	$post_ids = trim($list, ',');

	$pp_arg = array(
	'order_id' => $order_id,
	'post_id' => '',
	'post_ids' => $post_ids,
	'post_author' => $seller,
	'uid' => $uid,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'txn_id' => '',
	'item_name' => '',
	'trans_type' => 'Direct by PayPal',
	'mc_currency' => $mc_currency,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'receiver_email' => $receiver_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'moved_credits' => '',
	'available' => '',
	'quantity' => $sum_unpaid,
	'mc_fee' => cp_auction_sanitize_amount($mc_fee),
	'conversion' => cp_auction_sanitize_amount($conversion),
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'howto_pay' => 'paypal',
	'cp_shipping' => $shipping,
	'type' => 'purchase'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $pp_arg ) ).'">';

	echo '<div class="clear15"></div>';
	if( get_option('cp_auction_plugin_button') == 1 ) {
	echo '<input type="image" src="http://images.paypal.com/images/x-click-but1.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">';
	} elseif( get_option('cp_auction_plugin_button') == 2 ) {
	echo '<input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">';
	} else {
	echo '<input type="submit" name="submit" value="'.__('Pay with PayPal','auctionPlugin').'" class="mbtn btn_orange cp-width" />';
	}
	echo '</form>';

	} elseif( $option == 'item_credits' ) {

	// Credit Payment, directly to seller..
	echo '<form class="payform" action="'.get_bloginfo('wpurl').'/?_process_purchase=1" method="post" enctype="multipart/form-data">';

	$receiver_email = get_user_meta( $seller, 'paypal_email', true );
	if( empty($receiver_email) ) $receiver_email = $author->user_email;

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;
	$users_credits = get_user_meta( $uid, 'cp_credits', true );

	$mc_currency = get_user_meta( $seller, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;

	$options = get_option('cp_auction_purchase_'.$uid.'');
	$array = explode(',', $options[$seller]);
	$list = implode(",", $array);
	$post_ids = trim($list, ',');

	$credit_arg = array(
	'order_id' => $order_id,
	'post_ids' => $post_ids,
	'post_author' => $seller,
	'uid' => $uid,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'txn_id' => 'CRE'.$pid.'A'.time().'P',
	'item_name' => '',
	'trans_type' => 'Direct by Credits',
	'mc_currency' => $mc_currency,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'receiver_email' => $receiver_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'quantity' => $sum_unpaid,
	'mc_fee' => cp_auction_sanitize_amount($mc_fee),
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'howto_pay' => 'item_credits',
	'users_credits' => $users_credits,
	'cp_shipping' => $shipping,
	'type' => 'purchase'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $credit_arg ) ).'">';

	echo '<input type="submit" name="credit_payment" value="'.__('Pay with Credits','auctionPlugin').'" class="mbtn btn_orange cp-width" />';
	echo '</form>';

        } elseif( $option == 'cre_escrow' ) {
        echo '<form class="payform" action="'.get_bloginfo('wpurl').'/?_pay_cre_escrow=1" method="post" enctype="multipart/form-data">';

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;
	$users_credits = get_user_meta( $uid, 'cp_credits', true );

	$mc_currency = get_user_meta( $seller, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;

	$options = get_option('cp_auction_purchase_'.$uid.'');
	$array = explode(',', $options[$seller]);
	$list = implode(",", $array);
	$post_ids = trim($list, ',');

	$cre_arg = array(
	'order_id' => $order_id,
	'post_ids' => $post_ids,
	'post_author' => $seller,
	'uid' => $uid,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'txn_id' => 'CRE'.$pid.'A'.time().'P',
	'item_name' => '',
	'trans_type' => 'Credits Escrow',
	'mc_currency' => $mc_currency,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'quantity' => $sum_unpaid,
	'mc_fee' => cp_auction_sanitize_amount($mc_fee),
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'howto_pay' => 'cre_escrow',
	'users_credits' => $users_credits,
	'cp_shipping' => $shipping,
	'type' => 'escrow'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $cre_arg ) ).'">';

        echo '<input type="submit" name="submit_credits" value="'.get_option('cp_auction_plugin_pay_escrow_credits').'" class="mbtn btn_orange cp-width" />';
        echo '</form>';

        // Bank Transfer Escrow Payment
        } elseif( $option == 'bt_escrow' ) {
        echo '<form class="payform" action="'.get_bloginfo('wpurl').'/?_pay_bt_escrow=1" method="post" enctype="multipart/form-data">';

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;

	$mc_currency = get_user_meta( $seller, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;

	$options = get_option('cp_auction_purchase_'.$uid.'');
	$array = explode(',', $options[$seller]);
	$list = implode(",", $array);
	$post_ids = trim($list, ',');

	$bt_arg = array(
	'order_id' => $order_id,
	'post_ids' => $post_ids,
	'post_author' => $seller,
	'uid' => $uid,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'txn_id' => 'BT'.$pid.'A'.time().'P',
	'item_name' => '',
	'trans_type' => 'Banktransfer Escrow',
	'mc_currency' => $mc_currency,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'quantity' => $sum_unpaid,
	'mc_fee' => cp_auction_sanitize_amount($mc_fee),
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'howto_pay' => 'bt_escrow',
	'cp_shipping' => $shipping,
	'type' => 'escrow'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $bt_arg ) ).'">';

        echo '<input type="submit" name="submit_btransfer" value="'.get_option('cp_auction_plugin_pay_bt_escrow').'" class="mbtn btn_orange cp-width" />';
	echo '</form>';
        }

        // PayPal Adaptive Payment
	elseif( $option == 'pp_adaptive' ) {

	$receiver_email = get_user_meta( $seller, 'paypal_email', true );
	if( empty($receiver_email) ) $receiver_email = $author->user_email;

	$payer_email = get_user_meta( $uid, 'paypal_email', true );
	if( empty($payer_email) ) $payer_email = $user->user_email;

	$mc_currency = get_user_meta( $seller, 'currency', true );
	if( empty($mc_currency) ) $mc_currency = $cp_options->currency_code;

	$conversion = false;
	if ( $enabl == "yes" && class_exists("Currencyr") )
	$conversion = currencyr_exchange( $mc_gross, strtolower( $cuto ) );

	$options = get_option('cp_auction_purchase_'.$uid.'');
	$array = explode(',', $options[$seller]);
	$list = implode(",", $array);
	$post_ids = trim($list, ',');

	echo '<div class="clear15"></div>';
	echo '<form action="'.get_bloginfo('wpurl').'/?pay_chained=1" method="post" enctype="multipart/form-data">';

	$adaptive = array(
	'order_id' => $order_id,
	'post_id' => '',
	'post_ids' => $post_ids,
	'post_author' => $seller,
	'uid' => $uid,
	'ip_address' => $ip_address,
	'datemade' => time(),
	'payment_date' => time(),
	'txn_id' => '',
	'item_name' => '',
	'trans_type' => 'PayPal Adaptive',
	'mc_currency' => $mc_currency,
	'last_name' => get_user_meta( $uid, 'last_name', true ),
	'first_name' => get_user_meta( $uid, 'first_name', true ),
	'payer_email' => $payer_email,
	'receiver_email' => $receiver_email,
	'address_country' => get_user_meta( $uid, 'cp_country', true ),
	'address_state' => get_user_meta( $uid, 'cp_state', true ),
	'address_country_code' => "N/A",
	'address_zip' => get_user_meta( $uid, 'cp_zipcode', true ),
	'address_street' => get_user_meta( $uid, 'cp_street', true ),
	'moved_credits' => '',
	'available' => '',
	'quantity' => $sum_unpaid,
	'mc_fee' => cp_auction_sanitize_amount($mc_fee),
	'conversion' => cp_auction_sanitize_amount($conversion),
	'mc_gross' => cp_auction_sanitize_amount($mc_gross),
	'howto_pay' => 'paypal',
	'cp_shipping' => $shipping,
	'type' => 'purchase'
	);

	echo '<input type="hidden" name="args" value="'.base64_encode( serialize( $adaptive ) ).'">';

	if( get_option('cp_auction_plugin_button') == 1 ) {
	echo '<input type="image" src="http://images.paypal.com/images/x-click-but1.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">';
	} elseif( get_option('cp_auction_plugin_button') == 2 ) {
	echo '<input type="image" src="https://www.paypal.com/en_GB/i/btn/btn_xpressCheckout.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">';
	} else {
	echo '<input type="submit" name="submit" value="'.__('Pay with PayPal','auctionPlugin').'" class="mbtn btn_orange cp-width" />';
	}
	echo '</form>';
	}
	// End PayPal Adaptive Payment

} if ( $seller && !$success ) { ?>

<p><?php _e('Please enter your billing address and confirm your order.', 'auctionPlugin'); ?></p>

<div class="clear10"></div>

<form action="" method="post">

<h3><?php _e('Billing Address', 'auctionPlugin'); ?></h3>

<div class="clear10"></div>

<div class="billing">

	<?php if ( !isset($first_label) ) $first_label = ''.__('First Name', 'auctionPlugin').''; ?>
	<p><label for="first_name"><?php echo $first_label; ?>:</label><br/>
	<input class="text required" name="first_name" id="first_name" value="<?php echo $user->first_name; ?>" type="text"></p>

	<?php if ( !isset($last_label) ) $last_label = ''.__('Last Name', 'auctionPlugin').''; ?>
	<p><label for="last_name"><?php echo $last_label; ?>:</label><br/>
	<input class="text required" name="last_name" id="last_name" value="<?php echo $user->last_name; ?>" type="text"></p>

<div class="clr"></div>

	<?php if ( !isset($street_label) ) $street_label = ''.__('Address', 'auctionPlugin').''; ?>
	<p><label for="cp_street"><?php echo $street_label; ?>:</label><br/>
	<input class="text required" name="cp_street" id="cp_street" value="<?php echo $user->cp_street; ?>" type="text"></p>

	<?php if ( !isset($zip_label) ) $zip_label = ''.__('Postcode / Zip', 'auctionPlugin').''; ?>
	<p><label for="cp_zipcode"><?php echo $zip_label; ?>:</label><br/>
	<input class="text required" name="cp_zipcode" id="cp_zipcode" value="<?php echo $user->cp_zipcode; ?>" type="text"></p>

<div class="clr"></div>

	<?php if ( !isset($city_label) ) $city_label = ''.__('Town / City', 'auctionPlugin').''; ?>
	<p><label for="cp_city"><?php echo $city_label; ?>:</label><br/>
	<input class="text required" name="cp_city" id="cp_city" value="<?php echo $user->cp_city; ?>" type="text"></p>

	<?php if ( !isset($country_label) ) $country_label = ''.__('Country', 'auctionPlugin').''; ?>
	<p><label for="cp_country"><?php echo $country_label; ?>:</label><br/>
	<?php cp_auction_get_dropdown('cp_country', $uid); ?></p>

<div class="clr"></div>

	<?php if ( !isset($email_label) ) $email_label = ''.__('Email Address', 'auctionPlugin').''; ?>
	<p><label for="user_email"><?php echo $email_label; ?>:</label><br/>
	<input class="text required" name="user_email" id="user_email" value="<?php echo $user->user_email; ?>" type="text"></p>

	<p><label for="cp_phone"><?php _e('Phone:', 'auctionPlugin'); ?></label><br/>
	<input class="text" name="cp_phone" id="cp_phone" value="<?php echo $user->cp_phone; ?>" type="text"></p>

</div><!-- billing -->

<div class="clear10"></div>

<h3><?php _e('Confirm Order', 'auctionPlugin'); ?></h3>

<div class="clear10"></div>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column"><div style="text-align: left;"><?php _e('Product', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone"><?php _e('Price', 'auctionPlugin'); ?></th>
		<th data-hide="phone"><?php _e('Quantity', 'auctionPlugin'); ?></th>
		<th data-hide="phone"><?php _e('Total', 'auctionPlugin'); ?></th>
		<th class="footable-last-column" data-hide="phone"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $post, $wpdb;
	$todayDate = date_i18n('Y-m-d H:i:s', strtotime("current_time('mysql')"), true); // current date
	$tm = strtotime($todayDate);
	$table_name = $wpdb->prefix . "cp_auction_plugin_purchases";
	$items = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$seller' AND buyer = '$uid' AND unpaid > '0' AND paid != '2' ORDER BY datemade ASC", ARRAY_A);
	$i=1;
	if( $items ) {
	$empty = false;
	foreach($items as $item) {

	$id = $item['id'];
	$pid = $item['pid'];
	$item_name = $item['item_name'];
	$amount = $item['amount'];
	$discount = $item['discount'];
	$numbers = $item['numbers'];
	$unpaid = $item['unpaid'];
	$total = ($amount - $discount) * $unpaid;
	$item_price = $amount - $discount;
	$ads_url = '<a title="'.$item_name.'" href="'.get_permalink($pid).'">'.truncate($item_name, 15).'</a>';
	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );
	$author = get_userdata( $seller );
	$howmany = get_post_meta($pid, 'cp_auction_howmany', true);
	$howmany_added = get_post_meta($pid, 'cp_auction_howmany_added', true);

   echo '<tr id="tr_'.$id.'" class="even">
	<td class="text-left">'.$ads_url.'</td>
	<td class="text-right">'.cp_display_price( $item_price, $cs, false ).'</td>';
	if( $howmany < 2 && $howmany_added != "unlimited" ) {
   echo '<td align="center">1</td>';
	} else if ( $item['ref'] == "process" ) {
   echo '<td align="center">'.$unpaid.'</td>';
	} else {
   echo '<td align="right"><input id="unpaid_'.$id.'" name="unpaid" value="'.$unpaid.'" type="hidden"><input class="cart-quantity" id="quantity_'.$id.'" type="text" name="quantity" value="'.$unpaid.'"></td>';
	}
   echo '<td class="text-right">'.cp_display_price( $total, $cs, false ).'</td>
	<td class="text-center footable-last-column">'; if( $my_type == "classified" && $item['ref'] != "process" ) { echo '<a title="'.__('Adjust the quantity of this product.', 'auctionPlugin').'" href="javascript: void(0)" onclick="update_pending(\''.$id.'\')">'.__('Update', 'auctionPlugin').'</a> | '; } echo '<a title="'.__('Contact', 'auctionPlugin').' '.$author->first_name.' '.$author->last_name.' ('.$author->user_login.')" href="'.cp_auction_url($cpurl['contact'], '?_contact_user=1', 'uid='.$seller.'').'">'.__('Contact', 'auctionPlugin').'</a> | <a href="#" class="delete-complete-purchase" title="'.__('Remove this product from cart.', 'auctionPlugin').'" id="'.$id.'">'.__('Delete', 'auctionPlugin').'</a></td>
	</tr>';

	}

	$sub_total = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(net_total) FROM {$table_name} WHERE uid = %s AND buyer = %s AND unpaid > %s AND paid != %s", $seller, $uid, '0', '2' ) );
	$sum_unpaid = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(unpaid) FROM {$table_name} WHERE uid = %s AND buyer = %s AND unpaid > %s AND paid != %s", $seller, $uid, '0', '2' ) );

	$country = get_user_meta( $uid, 'cp_country', true );
	if ( empty( $country ) && get_option('cp_auction_enable_geoip') == "yes" )
	$country = do_shortcode( '[geoip_detect2 property="country"]' );

	$not = false;
	$shipping = false;
	$cp_shipping_options = get_post_meta( $pid, 'cp_shipping_options', true );
	$shipping_options = get_localized_field_values( 'cp_shipping_options', $cp_shipping_options );
	if ( $shipping_options == "" ) $not = __( 'This product can not be shipped!', 'auctionPlugin' );
	else if ( $shipping_options == "Free delivery" ) $not = __( 'Free Shipping', 'auctionPlugin' );
	else $shipping = cp_auction_calculate_shipping( $seller, $uid );

	$free_shipping = get_user_meta( $seller, 'cp_auction_free_shipping', true );
	if ( ( $free_shipping > '0' ) && ( $sub_total >= $free_shipping ) ) $shipping = false;

	$cih = get_user_meta( $seller, 'cp_accept_cih_payment', true );
	$cod = get_user_meta( $seller, 'cp_accept_cod_payment', true );
	$bankt = get_user_meta( $seller, 'cp_accept_bank_transfer_payment', true );
	$pp_email = get_user_meta( $seller, 'paypal_email', true );

	$sum = ($sub_total + $shipping);

	if( $sum > 0 ) {
	$cto = false; if ( $enabl == "yes" && class_exists("Currencyr") ) $cto = '('.currencyr_exchange( $sum, strtolower( $cuto ) ).' '.$cuto.')';
	$cto1 = false; if ( $enabl == "yes" && class_exists("Currencyr") ) $cto1 = currencyr_exchange( $sum, strtolower( $cuto ) );
	echo '<tr><td colspan="5"></td></tr>';
	echo '<tr class="cart"><td colspan="5" class="text-right"><strong>'.__('Order Total:', 'auctionPlugin').' '.cp_display_price( $sum, $cs, false ).'</strong> '.$cto.''; if( $shipping > 0 ) echo ' ('.__('incl.', 'auctionPlugin').' '.cp_display_price( $shipping, $cs, false ).' '.__('shipping', 'auctionPlugin').')';
	}
}
else {
   $empty = true;
   echo '<tr><td colspan="5">'.__('Your cart is currently empty!', 'auctionPlugin').'</td></tr>';
   }

	?>
	</tbody>
	</table>

<div class="clear5"></div>

<p style="text-align: center; float: right;"><a href="<?php echo cp_auction_url($cpurl['cart'], '?shopping_cart=1'); ?>"><?php _e('Return to Shopping Cart', 'auctionPlugin'); ?></a></p>

<div class="clear15"></div>

<h3><?php _e('Payment Options', 'auctionPlugin'); ?></h3>

<div class="clear10"></div>

<?php
	$bt = '';
	$cpay = '';
	if( get_option('cp_auction_plugin_pay_admin') == "yes" && get_option('cp_auction_plugin_pay_user') == "yes" ) {
	$pay = "both";
	} elseif( get_option('cp_auction_plugin_pay_admin') == "yes" && get_option('cp_auction_plugin_pay_user') == "no" ) {
	$pay = "admin";
	} else {
	$pay = "user";
	}
	$users_choice = get_user_meta( $seller, 'cp_use_safe_payment', true );
	if( empty( $users_choice ) ) $users_choice = "yes";
	if( $pay == "both" && $users_choice == "no" ) {
	$pp = ''.__('Pay with PayPal','auctionPlugin').'';
	}
	if( $pay == "both" && $users_choice == "yes" ) {
	$pp = get_option('cp_auction_plugin_pay_escrow');
	$escrow = get_option('cp_auction_plugin_escrow_info');
	$bt = "yes";
	}	
	if( $pay == "admin" ) {
	$pp = get_option('cp_auction_plugin_pay_escrow');
	$escrow = get_option('cp_auction_plugin_escrow_info');
	$bt = "yes";
	}
	if( $pay == "user" ) {
	$pp = ''.__('Pay with PayPal','auctionPlugin').'';
	}
	$escrow = get_option('cp_auction_plugin_escrow_info');
	$accept_credits = get_user_meta( $seller, 'cp_accept_credit_payment', true );
	$esc_cre_total = cp_auction_calculate_esc_fees($sum, 'cre_escrow', get_option('cp_auction_plugin_esc_credits_fee'));
	$esc_bt_total = cp_auction_calculate_esc_fees($sum, 'bt_escrow', get_option('cp_auction_plugin_esc_bt_fee'));
	$credit_fees = get_user_meta( $seller, 'cp_auction_credit_fees', true ); // Percent or flat
	$cre_total = cp_auction_calculate_fees($sum, 'item_credits', $credit_fees, $uid);
	$crefees = ( $cre_total - $sum );

	if( get_option('cp_auction_plugin_credits_escrow') == "yes" && $value >= $esc_cre_total && $bt == "yes" ) $cpay = "yes";

	$country = get_user_meta( $uid, 'cp_country', true );
	if ( empty( $country ) && get_option('cp_auction_enable_geoip') == "yes" )
	$country = do_shortcode( '[geoip_detect2 property="country"]' );

	$shipping = cp_auction_calculate_shipping( $seller, $uid );

	$cod_fee = get_user_meta( $seller, 'cp_auction_cod_fees', true );
	$bankt_fee = get_user_meta( $seller, 'cp_auction_bank_transfer_fees', true );
	$bankt_total = cp_auction_calculate_fees($sum, 'bank_transfer', $bankt_fee, $uid);
	$banktfee = ( $bankt_total - $sum );
	$pp_fees = get_user_meta( $seller, 'cp_auction_paypal_fees', true );
	$pp_total = cp_auction_calculate_fees($sum, 'paypal', $pp_fees, $uid);
	$ppfees = ( $pp_total - $sum );

	// Calculate escrow fees
	$esc_crefees = ( $esc_cre_total - $sum );
	$esc_btfees = ( $esc_bt_total - $sum );

if ( $enabl == "yes" && class_exists("Currencyr") && get_option('cp_auction_plugin_paypal') == "yes" && $pp_email == true && $bt != "yes" ) {

	echo '<p>'.sprintf( __( '<strong>NOTE</strong>: PayPal payments are converted from <strong>%s</strong> to <strong>%s</strong>.', 'auctionPlugin' ), cp_display_price( $sum, $cs, false ), ''.$cto1.' '.$cuto.'' ).'</p>';

}

if( get_option('cp_auction_plugin_actfee') > 0 ) { ?>
<input type="radio" name="option" id="pp_adaptive" value="pp_adaptive"> <label for="pp_adaptive"><?php _e('PayPal - Pay with <strong>credit card</strong> if you don`t have a PayPal account', 'auctionPlugin'); ?> ( <?php echo cp_display_price( $shipping, $cs, false ); ?> ).</label><br/>

<?php } else { ?>

<?php if( get_option('cp_auction_plugin_cih') == "yes" && $cih == "yes" && $bt != "yes" ) { ?>
<input type="radio" name="option" id="cih" value="cih"> <label for="cih"><?php _e('Cash in hand when the goods are picked up', 'auctionPlugin'); ?> ( <?php _e('No S&H', 'auctionPlugin'); ?> ).</label><div class="clear5"></div>

<?php } if( get_option('cp_auction_plugin_cod') == "yes" && $cod == "yes" && $bt != "yes" ) { ?>
<input type="radio" name="option" id="cod" value="cod"> <label for="cod"><?php _e('Cash on Delivery (COD) - Delivery to your nearest COD office', 'auctionPlugin'); ?> ( <?php echo cp_display_price( ($shipping+$cod_fee), $cs, false ); ?> <?php _e('S&H', 'auctionPlugin'); ?> ).</label><div class="clear5"></div>

<?php } if( get_option('cp_auction_plugin_bank_transfer') == "yes" && $bankt == "yes" && $bt != "yes" ) { ?>
<input type="radio" name="option" id="bank_transfer" value="bank_transfer"> <label for="bank_transfer"><?php _e('Bank Transfer - Send money directly to our bank account', 'auctionPlugin'); ?> ( <?php echo cp_display_price( ($shipping+$banktfee), $cs, false ); ?> <?php _e('S&H', 'auctionPlugin'); ?> ).</label><div class="clear5"></div>

<?php } if( get_option('cp_auction_plugin_paypal') == "yes" && $pp_email == true && $bt != "yes" ) { ?>
<input type="radio" name="option" id="paypal" value="paypal"> <label for="paypal"><?php _e('PayPal - Pay with <strong>credit card</strong> if you don`t have a PayPal account', 'auctionPlugin'); ?> ( <?php echo cp_display_price( ($shipping+$ppfees), $cs, false ); ?> <?php _e('S&H', 'auctionPlugin'); ?> ).</label><div class="clear5"></div>

<?php } if(( function_exists('aws_cp_settings_transactions') && $accept_credits == "yes" && $value >= $cre_total ) && ( $bt != "yes" )) { ?>
<input type="radio" name="option" id="item_credits" value="item_credits"> <label for="item_credits"><?php _e('Credits - Available Credits / Value:', 'auctionPlugin'); ?> <?php echo $available_credits; ?> / <?php echo cp_display_price( $value, 'ad', false ); ?> ( <?php echo cp_display_price( ($shipping+$crefees), $cs, false ); ?> <?php _e('S&H', 'auctionPlugin'); ?> ).</label><div class="clear5"></div>

<?php } if( $cpay == "yes" ) { ?>
<input type="radio" name="option" id="cre_escrow" value="cre_escrow"> <label for="cre_escrow"><?php _e('Credits Escrow - Available Credits / Value:', 'auctionPlugin'); ?> <?php echo $available_credits; ?> / <?php echo cp_display_price( $value, 'ad', false ); ?> ( <?php echo cp_display_price( ($shipping+$esc_crefees), $cs, false ); ?> <?php _e('S&H', 'auctionPlugin'); ?> ).</label><div class="clear5"></div>

<?php } if( get_option('cp_auction_plugin_esc_bank_transfer') == "yes" && $bt == "yes" && get_option('cp_auction_plugin_pay_admin') == "yes" ) { ?>
<input type="radio" name="option" id="bt_escrow" value="bt_escrow"> <label for="bt_escrow"><?php _e('Bank Transfer Escrow - Safe payment', 'auctionPlugin'); ?> ( <?php echo cp_display_price( ($shipping+$esc_btfees), $cs, false ); ?> <?php _e('S&H', 'auctionPlugin'); ?> ).</label><div class="clear5"></div>
<?php }

if( ( get_option('cp_auction_plugin_esc_bank_transfer') == "yes" && $bt == "yes" && get_option('cp_auction_plugin_pay_admin') == "yes" ) || ( $cpay == "yes" ) ) {
echo '<div class="clear10"></div>';
echo '<div class="page_info">'.nl2br($escrow).'</div>';
}
} ?>

<div class="clear20"></div>

<p style="text-align: center; float: right;"><input type="button" class="mbtn btn_orange cp-width" onClick="parent.location='<?php echo get_bloginfo('wpurl'); ?>'" value="<?php _e('Continue Shopping', 'auctionPlugin'); ?>"> &nbsp;&nbsp; <input type="hidden" name="pid" value="<?php echo $pid; ?>"><input type="hidden" name="seller" value="<?php echo $seller; ?>"><input type="submit" name="process-order" value="<?php _e('Process Order','auctionPlugin'); ?>" class="mbtn btn_orange cp-width" /></p>

	</form>

<div class="clr"></div>

<p><?php _e( '<strong>Note</strong>: Quantity can no longer be adjusted after the order is processed.', 'auctionPlugin' ); ?></p>

	<?php } elseif ( !$seller ) { ?>

	<?php if ( function_exists('cp_auction_display_cart') ) cp_auction_display_cart(); ?> 

	<?php } ?>

		</div><!-- /main-cart aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>