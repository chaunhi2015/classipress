<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $cp_options, $post, $wpdb, $i;
	$current_user = wp_get_current_user(); // grabs the user info and puts into vars
	$display_user_name = cp_get_user_name();
	$maxposts = get_option('posts_per_page');
	$only_admins = get_option('cp_auction_only_admins_post_auctions');

	$uid = $current_user->ID;

	$ct = get_option('stylesheet');
	$curr_symbol = $cp_options->curr_symbol;
	$bump_price = cp_display_price( get_option('cp_auction_bump_ad_price'), $curr_symbol, false );
	$upg_price = cp_display_price( get_option('cp_auction_upgrade_ad_price'), $curr_symbol, false );

	$allowed_actions = array( 'pause', 'restart', 'delete', 'setPrivate', 'setSold', 'unsetSold', 'setPick', 'unsetPick' );

		if ( isset( $_GET['action'] ) && in_array( $_GET['action'], $allowed_actions ) ) {

		if ( isset( $_GET['aid'] ) && is_numeric( $_GET['aid'] ) ) {

		$d = trim( $_GET['action'] );
		$aid = appthemes_numbers_only( $_GET['aid'] );

		// make sure author matches ad, and ad exist
		$sql = $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE ID = %d AND post_author = %d AND post_type = %s", $aid, $current_user->ID, APP_POST_TYPE );
		$post = $wpdb->get_row( $sql );
		if ( $post == null )
			return;

		$expire_time = strtotime( get_post_meta( $post->ID, 'cp_sys_expire_date', true ) );
		$is_expired = ( current_time( 'timestamp' ) > $expire_time && $post->post_status == 'draft' );
		$is_pending = ( $post->post_status == 'pending' );

		if ( $d == 'pause' && ! $is_expired && ! $is_pending ) {
			wp_update_post( array( 'ID' => $post->ID, 'post_status' => 'draft' ) );
			$title = ''.__('Paused', 'auctionPlugin').' - '.$post->post_title.'';
			$paused = "yes";

		} elseif ( $d == 'setPrivate' && ! $is_expired && ! $is_pending ) {
			wp_update_post( array( 'ID' => $post->ID, 'post_status' => 'private' ) );
			$title = ''.__('Private', 'auctionPlugin').' - '.$post->post_title.'';
			$private = "yes";

		} elseif ( $d == 'restart' && ! $is_expired && ! $is_pending ) {
			wp_update_post( array( 'ID' => $post->ID, 'post_status' => 'publish' ) );
			$title = ''.__('Published', 'auctionPlugin').' - '.$post->post_title.'';
			$restarted = "yes";

		} elseif ( $d == 'delete' ) {
			cp_delete_ad_listing( $post->ID );
			$deleted = "yes";

		} elseif ( $d == 'setSold' ) {
			update_post_meta( $post->ID, 'cp_ad_sold', 'yes' );
			$markedsold = "yes";

		} elseif ( $d == 'unsetSold' ) {
			update_post_meta( $post->ID, 'cp_ad_sold', 'no' );
			$unmarkedsold = "yes";

		} elseif ( $d == 'setPick' ) {
			update_post_meta( $post->ID, 'cp_ad_pick', 'yes' );
			$markedpick = "yes";

		} elseif ( $d == 'unsetPick' ) {
			update_post_meta( $post->ID, 'cp_ad_pick', 'no' );
			$unmarkedpick = "yes";
		}
	}
}

if ( isset( $_POST['save_quicky'] ) ) {

	global $aws_errors;

	$price['cp_price'] = isset( $_POST['cp_price'] ) ? esc_attr( $_POST['cp_price'] ) : '';
	foreach ( $_POST['cp_price'] as $item => $value ) { // Cycle through the $price array!
		if ( ! is_numeric( $value ) ) $value = '0';
		$my_type = get_post_meta( $item, 'cp_auction_my_auction_type', true );
		if ( $my_type == "normal" || $my_type == "reverse" )
		update_post_meta( $item, 'cp_start_price', $value );
		update_post_meta( $item, 'cp_price', $value );
		if ( empty( $value ) ) delete_post_meta( $item, 'cp_price' ); // Delete if empty
	}
	$quantity['cp_quantity'] = isset( $_POST['cp_auction_howmany'] ) ? esc_attr( $_POST['cp_auction_howmany'] ) : '';
	foreach ( $_POST['cp_auction_howmany'] as $item => $value ) { // Cycle through the $quantity array!
		if ( ! is_numeric( $value ) ) $value = '0';
		if ( empty( $value ) || $value == '0' ) {
		$value = '0';
		$my_type = get_post_meta( $item, 'cp_auction_my_auction_type', true );
		if ( $my_type == "normal" || $my_type == "reverse" )
		update_post_meta( $item, 'cp_auction_howmany_added', "none" );
		else update_post_meta( $item, 'cp_auction_howmany_added', "unlimited" );
		} else {
		update_post_meta( $item, 'cp_auction_howmany_added', "yes" );
		}
		update_post_meta( $item, 'cp_auction_howmany', $value );

	}

	add_action( 'appthemes_notices', 'aws_commerce_show_notice' );

}

if ( isset( $_POST['save_quicky_paged'] ) ) {

	global $aws_errors;

	$paged = isset( $_POST['quicky_paged'] ) ? esc_attr( $_POST['quicky_paged'] ) : '';

	update_user_meta( $current_user->ID, 'quicky_paged', $paged );
}

?>

<?php cp_auction_header(); ?>

<div class="shadowblock_out" id="noise">

	<div class="shadowblock">

		<div class="aws-box">

                        <h2 class="dotted"><?php echo the_title(); ?></h2>

            <?php if(isset($info)) { ?>
            <div class="notice success"><?php echo "".$info.""; ?></div>
            <?php } ?>
            <?php if(isset($msg)) { ?>
            <div class="notice error"><?php echo "".$msg.""; ?></div>
            <?php } ?>

	<?php 
	if ( isset($paused) ) {
			appthemes_display_notice( 'success', __( 'Ad has been paused.', 'auctionPlugin' ) );
		} elseif ( isset($restarted) ) {
			appthemes_display_notice( 'success', __( 'Ad has been published.', 'auctionPlugin' ) );
		} elseif ( isset($deleted) ) {
			appthemes_display_notice( 'success', __( 'Ad has been deleted.', 'auctionPlugin' ) );
		} elseif ( isset($markedsold) ) {
			appthemes_display_notice( 'success', __( 'Ad has been marked as sold.', 'auctionPlugin' ) );
		} elseif ( isset($private) ) {
			appthemes_display_notice( 'success', __( 'Ad has been marked as private.', 'auctionPlugin' ) );
		} elseif ( isset($unmarkedsold) ) {
			appthemes_display_notice( 'success', __( 'Ad has been unmarked as sold.', 'auctionPlugin' ) );
		} elseif ( isset($markedpick) ) {
			appthemes_display_notice( 'success', __( 'Ad has been marked as pending pick up.', 'auctionPlugin' ) );
		} elseif ( isset($unmarkedpick) ) {
			appthemes_display_notice( 'success', __( 'Ad has been unmarked as pending pick up.', 'auctionPlugin' ) );
		} elseif ( isset($renew_disabled) ) {
			appthemes_display_notice( 'error', __( 'You can not relist this ad. Relisting is currently disabled.', 'auctionPlugin' ) );
		} elseif ( isset($renew_invalid_id) ) {
			appthemes_display_notice( 'error', __( 'You can not relist this ad. Invalid ID of an ad.', 'auctionPlugin' ) );
		} elseif ( isset($renew_not_expired) ) {
			appthemes_display_notice( 'error', __( 'You can not relist this ad. Ad is not expired.', 'auctionPlugin' ) );
		} elseif ( isset($edit_invalid_id) ) {
			appthemes_display_notice( 'error', __( 'You can not edit this ad. Invalid ID of an ad.', 'auctionPlugin' ) );
		} elseif ( isset($edit_invalid_author) ) {
			appthemes_display_notice( 'error', __( "You can not edit this ad. It's not your ad.", 'auctionPlugin' ) );
		} elseif ( isset($edit_invalid_type) ) {
			appthemes_display_notice( 'error', __( 'You can not edit this ad. This is not an ad.', 'auctionPlugin' ) );
		} elseif ( isset($edit_disabled) ) {
			appthemes_display_notice( 'error', __( 'You can not edit this ad. Editing is currently disabled.', 'auctionPlugin' ) );
		} elseif ( isset($edit_pending) ) {
			appthemes_display_notice( 'error', __( 'You can not edit this ad. Ad is not yet approved.', 'auctionPlugin' ) );
		} elseif ( isset($edit_expired) ) {
			appthemes_display_notice( 'error', __( 'You can not edit this ad. Ad is expired.', 'auctionPlugin' ) );
		} elseif ( isset($payments_disabled) ) {
			appthemes_display_notice( 'error', __( 'Payments are currently disabled. You cannot purchase anything.', 'auctionPlugin' ) );
		} ?>

	<p><?php _e( 'Below you will find all of your ads. Click on one of the tabs to display a specific ad type. If you have any questions, please contact the site administrator.', 'auctionPlugin' ); ?></p>

	<?php if ( get_option( 'cp_auction_bump_ad_enabled' ) == "yes" ) { ?>
	<p><?php _e( '<strong>Bumping up your Ad</strong> will reset the post date of your Ad to the time of the bump, thus moving your Ad from its current position in the listings back up to the top of the first page at the cost of', 'auctionPlugin' ); ?> <strong><?php echo $bump_price; ?></strong><?php if(get_option('cp_auction_bump_extend_exp') > 0) { ?><?php _e( ', and at the same time extend the ad duration with', 'auctionPlugin' ); ?> <strong><?php echo get_option('cp_auction_bump_extend_exp'); ?> <?php _e('days','auctionPlugin'); ?></strong>.<?php } ?></p>
	<?php } ?>

	<?php if ( get_option( 'cp_auction_upgrade_ad_enabled' ) == "yes" ) { ?>
	<p><?php _e( '<strong>Make your Ad featured</strong>, featured Ads are randomly selected to appear on rotation at the top in the main category they’ve been posted in. Your ads will stand out among other ads at the cost of', 'auctionPlugin' ); ?> <strong><?php echo $upg_price; ?></strong><?php if(get_option('cp_auction_upgrade_extend_exp') > 0) { ?><?php _e( ', and at the same time extend the ad duration with', 'auctionPlugin' ); ?> <strong><?php echo get_option('cp_auction_upgrade_extend_exp'); ?> <?php _e('days','auctionPlugin'); ?></strong>.<?php } ?></p>
	<?php } ?>

			</div><!--/aws-box-->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<?php if( $ct == "jibo" || get_option( 'cp_auction_slide_in_ads' ) == "yes" ) { ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({
    activate: function (e, ui) {
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' });
    },
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
    active: $.cookie('homeTab')
});
});
</script>
<?php } else { ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({
    activate: function (e, ui) {
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' });
    },
    active: $.cookie('homeTab')
});
});
</script>

<?php } if( $ct == "eclassify" ) {
	$tabs = "tabnavig"; ?>

	<div class="tabcontrol">

		<ul class="tabnavig">
<?php } else {
	$tabs = "tabmenu"; ?>

	<div class="tabhome">

		<ul class="tabmenu">
<?php } ?>

	<?php if(( get_option('cp_auction_plugin_auctiontype') == "classified" ) || (( get_option('cp_auction_plugin_auctiontype') == "mixed" ) && ( get_option('cp_auction_plugin_classified') == true ))) { ?>
	<li><a href="#block1"><span class="big"><?php _e('Classifieds', 'auctionPlugin'); ?></span></a></li>
	<?php } if(( get_option('cp_auction_plugin_auctiontype') == "wanted" ) || (( get_option('cp_auction_plugin_auctiontype') == "mixed" ) && ( get_option('cp_auction_plugin_wanted') == true ))) { ?>
	<li><a href="#block3"><span class="big"><?php _e('Wanted', 'auctionPlugin'); ?></span></a></li>
	<?php } if(( get_option('cp_auction_plugin_auctiontype') == "normal" || get_option('cp_auction_plugin_auctiontype') == "reverse" || $only_admins == "yes" ) || ( get_option('cp_auction_plugin_auctiontype') == "mixed" 
	&& ( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true || $only_admins == "yes" ))) { ?>
	<li><a href="#block4"><span class="big"><?php _e('Auctions', 'auctionPlugin'); ?></span></a></li>
	<?php } ?>
	<li><a href="#block9"><span class="big"><?php _e('Quick Update', 'auctionPlugin'); ?></span></a></li>
	<?php if ( get_option('CP_VERSION') > '3.4.1' ) { ?>
	<li><a href="#block10"><span class="big"><?php _e('My Ad Orders', 'auctionPlugin'); ?></span></a></li>
	<?php } ?>
	</ul>

<script type="text/javascript">
jQuery('ul.<?php echo $tabs; ?> a').click(function() {
var URL = window.location.href;
var NewURL = URL.split('page')[0];
window.history.pushState('', '', NewURL);
if ( NewURL != URL ) {
window.location = window.location.href;
}
});
</script>

	<?php if(( get_option('cp_auction_plugin_auctiontype') == "classified" ) || (( get_option('cp_auction_plugin_auctiontype') == "mixed" ) && ( get_option('cp_auction_plugin_classified') == true ))) { ?>

	<div id="block1">

	<div class="clr"></div>
<div class="tabunder"><span class="big"><?php _e('Classified Ads', 'auctionPlugin'); ?> / <strong><span class="colour"><?php _e( 'All My Classified Ads', 'auctionPlugin' ); ?></span></strong></span></div>

	<div class="shadowblock_out">

		<div class="shadowblock">

<?php

	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args = array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish, pending, private, draft', 
	'author' => $current_user->ID, 
	'paged' => $paged, 
	'meta_key' => 'cp_auction_my_auction_type', 
	'meta_query' => 
		array( 
		'relation' => 'OR',
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => array( 'wanted', 'normal', 'reverse' ),
			'compare' => 'NOT EXISTS',
			'value' => ''
			), 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => 'classified', 
			'compare' => 'IN', 
			), 
		), 
	);
	$classified = new WP_Query( $args );

	// build the row counter depending on what page we're on
	$posts_per_page = $classified->get( 'posts_per_page' );
	if ( $paged == 1 ) $i = 0; else $i = $paged * $posts_per_page - $posts_per_page;

	// Pagination fix
	$temp_query = $wp_query;
	$wp_query   = NULL;
	$wp_query   = $classified;
	?>

	<?php if ( $classified->have_posts() ) : ?>

	<p><?php _e( 'Below you will find a listing of all your classified ads. Click on one of the options to perform a specific task. If you have any questions, please contact the site administrator.', 'auctionPlugin' ); ?></p>

	<table border="0" cellpadding="4" cellspacing="1" class="tblwide footable">
		<thead>
			<tr>
				<th class="listing-count" data-class="expand">&nbsp;</th>
				<th class="listing-title">&nbsp;<?php _e( 'Title', 'auctionPlugin' ); ?></th>
				<?php if ( current_theme_supports( 'app-stats' ) ) { ?>
				<th class="listing-views" data-hide="phone"><?php _e( 'Views', 'auctionPlugin' ); ?></th>
				<?php } if( ( get_option( 'cp_auction_bump_ad_enabled' ) == "yes" || get_option( 'cp_auction_upgrade_ad_enabled' ) == "yes" ) && ( get_option( 'cp_auction_bump_ad_link' ) == "table" ) ) { ?>
				<th class="listing-bump" data-hide="phone"><?php _e( 'Bump Up', 'auctionPlugin' ); ?></th>
				<?php } ?>
				<th class="listing-status" data-hide="phone"><?php _e( 'Status', 'auctionPlugin' ); ?></th>
				<th class="listing-options" data-hide="phone"><?php _e( 'Options', 'auctionPlugin' ); ?></th>
			</tr>
		</thead>
	<tbody>

	<?php cp_auction_userpanel_selector( $classified ); ?>

	</tbody>

	</table>

	<?php if(function_exists('appthemes_pagination')) appthemes_pagination( '', '', $classified );

	// Reset main query object
	$wp_query = NULL;
	$wp_query = $temp_query; ?>

	<?php else : ?>

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'You currently have no classified ads.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

		<?php endif; ?>

		<?php wp_reset_query(); ?>

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

	</div><!-- /block1 -->

	<?php } if(( get_option('cp_auction_plugin_auctiontype') == "wanted" ) || (( get_option('cp_auction_plugin_auctiontype') == "mixed" ) && ( get_option('cp_auction_plugin_wanted') == true ))) { ?>

    <div id="block3">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><?php _e('Wanted Ads', 'auctionPlugin'); ?> / <strong><span class="colour"><?php _e( 'All My Wanted Ads', 'auctionPlugin' ); ?></span></strong></span></div>

	<div class="shadowblock_out">

		<div class="shadowblock">

<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args = array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish, pending, private, draft', 
	'author' => $current_user->ID, 
	'paged' => $paged, 
	'meta_key' => 'cp_auction_my_auction_type', 
	'meta_query' => 
		array( 
		'relation' => 'OR',
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => array( 'classified', 'normal', 'reverse' ),
			'compare' => 'NOT EXISTS',
			'value' => ''
			), 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => 'wanted', 
			'compare' => 'IN', 
			), 
		), 
	);
	$wanted = new WP_Query( $args );

	// build the row counter depending on what page we're on
	$posts_per_page = $wanted->get( 'posts_per_page' );
	if ( $paged == 1 ) $i = 0; else $i = $paged * $posts_per_page - $posts_per_page;

	// Pagination fix
	$temp_query = $wp_query;
	$wp_query   = NULL;
	$wp_query   = $wanted;
	?>

	<?php if ( $wanted->have_posts() ) : ?>

	<p><?php _e( 'Below you will find a listing of all your wanted ads. Click on one of the options to perform a specific task. If you have any questions, please contact the site administrator.', 'auctionPlugin' ); ?></p>

	<table border="0" cellpadding="4" cellspacing="1" class="tblwide footable">
		<thead>
			<tr>
				<th class="listing-count" data-class="expand">&nbsp;</th>
				<th class="listing-title">&nbsp;<?php _e( 'Title', 'auctionPlugin' ); ?></th>
				<?php if ( current_theme_supports( 'app-stats' ) ) { ?>
				<th class="listing-views" data-hide="phone"><?php _e( 'Views', 'auctionPlugin' ); ?></th>
				<?php } if( ( get_option( 'cp_auction_bump_ad_enabled' ) == "yes" || get_option( 'cp_auction_upgrade_ad_enabled' ) == "yes" ) && ( get_option( 'cp_auction_bump_ad_link' ) == "table" ) ) { ?>
				<th class="listing-bump" data-hide="phone"><?php _e( 'Bump Up', 'auctionPlugin' ); ?></th>
				<?php } ?>
				<th class="listing-status" data-hide="phone"><?php _e( 'Status', 'auctionPlugin' ); ?></th>
				<th class="listing-options" data-hide="phone"><?php _e( 'Options', 'auctionPlugin' ); ?></th>
			</tr>
		</thead>
	<tbody>

	<?php cp_auction_userpanel_selector( $wanted ); ?>

	</tbody>

	</table>

	<?php if(function_exists('appthemes_pagination')) appthemes_pagination( '', '', $wanted );

	// Reset main query object
	$wp_query = NULL;
	$wp_query = $temp_query; ?>

	<?php else : ?>

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'You currently have no Wanted ads.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

		<?php endif; ?>

		<?php wp_reset_query(); ?>

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

	</div><!-- /block3 - tab_content -->

	<!-- /block3 -->

	<?php } if(( get_option('cp_auction_plugin_auctiontype') == "normal" || get_option('cp_auction_plugin_auctiontype') == "reverse" || $only_admins == "yes" ) || ( get_option('cp_auction_plugin_auctiontype') == "mixed" 
	&& ( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true || $only_admins == "yes" ))) { ?>

    <div id="block4">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><?php _e('Auction Listings', 'auctionPlugin'); ?> / <strong><span class="colour"><?php _e( 'All My Auctions', 'auctionPlugin' ); ?></span></strong></span></div>

	<div class="shadowblock_out">

		<div class="shadowblock">

<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	$args = array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish, pending, private, draft', 
	'author' => $current_user->ID, 
	'paged' => $paged, 
	'meta_key' => 'cp_auction_my_auction_type', 
	'meta_query' => 
		array( 
		'relation' => 'OR', 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => array( 'classified', 'wanted' ), 
			'compare' => 'NOT EXISTS', 
			'value' => ''
			), 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => array( 'normal', 'reverse' ), 
			'compare' => 'IN', 
			), 
		), 
	);
	$auction = new WP_Query( $args );

	// build the row counter depending on what page we're on
	$posts_per_page = $auction->get( 'posts_per_page' );
	if ( $paged == 1 ) $i = 0; else $i = $paged * $posts_per_page - $posts_per_page;

	// Pagination fix
	$temp_query = $wp_query;
	$wp_query   = NULL;
	$wp_query   = $auction;
	?>

	<?php if ( $auction->have_posts() ) : ?>

	<p><?php _e( 'Below you will find a listing of all your auction listings. Click on one of the options to perform a specific task. If you have any questions, please contact the site administrator.', 'auctionPlugin' ); ?></p>

	<table border="0" cellpadding="4" cellspacing="1" class="tblwide footable">
		<thead>
			<tr>
				<th class="listing-count" data-class="expand">&nbsp;</th>
				<th class="listing-title">&nbsp;<?php _e( 'Title', 'auctionPlugin' ); ?></th>
				<?php if ( current_theme_supports( 'app-stats' ) ) { ?>
				<th class="listing-views" data-hide="phone"><?php _e( 'Views', 'auctionPlugin' ); ?></th>
				<?php } if( ( get_option( 'cp_auction_bump_ad_enabled' ) == "yes" || get_option( 'cp_auction_upgrade_ad_enabled' ) == "yes" ) && ( get_option( 'cp_auction_bump_ad_link' ) == "table" ) ) { ?>
				<th class="listing-bump" data-hide="phone"><?php _e( 'Bump Up', 'auctionPlugin' ); ?></th>
				<?php } ?>
				<th class="listing-status" data-hide="phone"><?php _e( 'Status', 'auctionPlugin' ); ?></th>
				<th class="listing-options" data-hide="phone"><?php _e( 'Options', 'auctionPlugin' ); ?></th>
			</tr>
		</thead>
	<tbody>

	<?php cp_auction_userpanel_selector( $auction ); ?>

	</tbody>

	</table>

	<?php if(function_exists('appthemes_pagination')) appthemes_pagination( '', '', $auction );

	// Reset main query object
	$wp_query = NULL;
	$wp_query = $temp_query; ?>

	<?php else : ?>

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'You currently have no auctions listed.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

		<?php endif; ?>

		<?php wp_reset_query(); ?>

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

	    </div><!-- /block4 - tab_content -->

	<?php } ?><!-- /block4 -->

				<!-- tab 9 -->
					<div id="block9">

<style type="text/css">
.button {
	padding: 0px 5px;
	border-radius: 0;
}
td input, input[type="text"] {
	background: #FFF none repeat scroll 0% 0%;
	border: 1px solid #BBB;
	border-radius: 0;
	padding: 2px 5px 2px 5px;
}
input[type="text"]:disabled {
	background: #eee;
}
</style>

						<div class="clr"></div>

				<div class="tabunder"><span class="big"><?php _e( 'Quick Update', 'auctionPlugin' ); ?> / <strong><span class="colour"><?php _e( 'Update Your Ads', 'auctionPlugin' ); ?></span></strong></span></div>

				<div class="shadowblock_out">

					<div class="shadowblock">

					<?php
						// setup the pagination and query
						$quicky_paged = get_user_meta( $current_user->ID, 'quicky_paged', true );
						if ( empty( $quicky_paged ) ) $quicky_paged = '10';
						$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
						$quicky = new WP_Query( array( 'author' => $current_user->ID, 'posts_per_page' => $quicky_paged, 'post_type' => 'ad_listing', 'post_status' => array( 'publish', 'pending', 'draft' ), 'paged' => $paged ) );

						// build the row counter depending on what page we're on
						$posts_per_page = $quicky->get( 'posts_per_page' );
						if ( $paged == 1 ) $i = 0; else $i = $paged * $posts_per_page - $posts_per_page;

						// Pagination fix
						$temp_query = $wp_query;
						$wp_query   = NULL;
						$wp_query   = $quicky;
						?>

						<?php if ( $quicky->have_posts() ) : ?>

						<p><?php _e( 'Below is a list of all your ads, for a quick and easy updating. <strong>Note:</strong> Enter a <strong>0</strong> to disable the display of the number of items in stock, OR to indicate that the item are stocked in an unlimited number.', 'auctionPlugin' ); ?></p>
						<p><?php _e( '<strong>Note</strong>: Use numeric values only, ie. 10 or 10.00', 'auctionPlugin' ); ?></p>

	<div class="clear5"></div>

	<form class="loginform" name="mainform" action="" method="post" id="mainform">
	<div class="screen-options">
		<label for="edit_ad_listing_per_page"><?php _e( 'Number of Ads per page:', 'auctionPlugin' ); ?></label>
		<input step="1" min="1" max="999" class="screen-per-page" name="quicky_paged" id="edit_ad_listing_per_page" maxlength="3" value="<?php echo $quicky_paged; ?>" style="width: 50px;"  type="number">
		<input name="save_quicky_paged" id="screen-options-apply" class="button" value="<?php _e( 'Apply', 'auctionPlugin' ); ?>" type="submit">
	</div>
	</form>

	<div class="clear5"></div>

						<table border="0" cellpadding="4" cellspacing="1" class="tblwide footable">
							<thead>
								<tr>
									<th width="5%" data-class="expand">&nbsp;</th>
									<th width="45%" class="text-left">&nbsp;<?php _e( 'Title', 'auctionPlugin' ); ?></th>
									<th width="25%" data-hide="phone"><?php _e( 'Price', 'auctionPlugin' ); ?></th>
									<th width="25%" data-hide="phone"><div style="text-align: center;"><?php _e( 'In Stock', 'auctionPlugin' ); ?></div></th>
								</tr>
							</thead>
							<tbody>

			<form class="loginform" name="mainform" action="" method="post" id="mainform">

			<?php while( $quicky->have_posts() ) : $quicky->the_post(); $i++; ?>

			<?php

			$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
			$in_stock = get_post_meta( $post->ID, 'cp_auction_howmany', true );
			$stock_status = get_post_meta ( $post->ID, 'cp_auction_howmany_added', true );
			if ( $my_type == "normal" || $my_type == "reverse" ) $in_stock = __( 'auction', 'auctionPlugin' );
			if ( $in_stock == '0' && $stock_status == "unlimited" ) $in_stock = __( 'unlimited', 'auctionPlugin' );

			if ( $in_stock == "auction" ) $na = "disabled";
			else $na = false;

			?>

	<tr class="even">

		<td class="text-right"><?php echo $i; ?>.</td>
		<?php if ( $post->post_status !== "publish" ) { ?>
		<td class="text-left"><div class="show-adthumb-left"><?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?></div><strong>
		<?php if ( mb_strlen( $post->post_title ) >= 25 ) echo mb_substr( $post->post_title, 0, 25 ) . '..'; else echo $post->post_title; ?></strong><?php if( $ct == "simply-responsive-cp" ) { ?>
		<div class="metad"><span class="folder"> &nbsp; <?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="clockb"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
		<?php } elseif( $ct == "flatpress" ) { ?>
		<div class="meta"><span class="folder"><?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="clock"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
		<?php } else { ?>
		<div class="meta"><span class="folder"><?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="clock"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
		<?php } ?>
		</td>
		<?php } else { ?>
		<td class="text-left"><div class="show-adthumb-left"><?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?></div><strong><a title="<?php echo $post->post_title; ?>" href="<?php echo get_permalink( $post->ID ); ?>"><?php if ( mb_strlen( $post->post_title ) >= 25 ) echo mb_substr( $post->post_title, 0, 25 ) . '..'; else echo $post->post_title; ?></a></strong>
		<?php if( $ct == "simply-responsive-cp" ) { ?>
		<div class="metad"><span class="folder"> &nbsp; <?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="clockb"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
		<?php } elseif( $ct == "flatpress" ) { ?>
		<div class="meta"><span class="folder"><?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="clock"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
		<?php } else { ?>
		<div class="meta"><span class="folder"><?php echo get_the_term_list(get_the_id(), APP_TAX_CAT, '', ', ', ''); ?></span> | <span class="clock"><span><?php echo appthemes_display_date( $post->post_date, 'date' ); ?></span></span></div>
		<?php } ?>
		</td>
		<?php } ?>
		<td class="text-right"><input type="text" name="cp_price[<?php echo $post->ID; ?>]" value="<?php echo get_post_meta( $post->ID, 'cp_price', true ); ?>" size="8" style="width: 100px; text-align: right;" /></td>

		<td class="text-center"><input type="text" name="cp_auction_howmany[<?php echo $post->ID; ?>]" value="<?php echo $in_stock; ?>" size="8" style="width: 100px; text-align: center;" <?php echo $na; ?>/></td>

	</tr>

			<?php endwhile; ?>

						</tbody>

					</table>

	<div class="clear10"></div>

	<p class="submit">
		<input class="btn_orange" type="submit" name="save_quicky" id="save_quicky" value="<?php _e( 'Save Changes', 'auctionPlugin' ); ?>" />
	</p>

			</form>

						<?php if(function_exists('appthemes_pagination')) appthemes_pagination( '', '', $quicky );

						// Reset main query object
						$wp_query = NULL;
						$wp_query = $temp_query; ?>

						<?php else : ?>

							<div class="clear10"></div>
							<p class="text-center"><?php _e( 'You currently have no items to update.', 'auctionPlugin' ); ?></p>
							<div class="clear10"></div>

						<?php endif; ?>

						<?php wp_reset_query(); ?>

			</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

	    </div><!-- /block9 - tab_content -->

<?php if ( get_option('CP_VERSION') > '3.4.1' ) { ?>

				<!-- tab 10 -->
					<div id="block10">

					<div class="clr"></div>

				<div class="tabunder"><span class="big"><?php _e( 'Ad Orders', 'auctionPlugin' ); ?> / <strong><span class="colour"><?php _e( 'View Your Ad Orders', 'auctionPlugin' ); ?></span></strong></span></div>

				<div class="shadowblock_out">

					<div class="shadowblock">

<?php if ( ( $orders = cp_get_user_dashboard_orders() ) || get_query_var('order_status') ) : ?>

	<p><?php _e( 'Below is your Order history. You can use the provided filter to filter all the orders.', 'auctionPlugin'); ?></p>

	<form class="filter" method="get" action="<?php echo esc_url( CP_DASHBOARD_ORDERS_URL ) ?>" >
		<input type="hidden" name="tab" value="orders" />
		<?php foreach( cp_get_order_statuses_verbiages() as $order_status => $name ): ?>

			<?php $checked = (bool) ( ! get_query_var('order_status') || in_array( $order_status, get_query_var('order_status') ) ); ?>

			<p>
				<input type="checkbox" name="order_status[]" value="<?php echo esc_attr( $order_status ); ?>" <?php checked( $checked ); ?> />
				<label for="<?php echo esc_attr( $order_status ); ?>"><?php echo $name; ?></label>
			</p>

		<?php endforeach; ?>

		<p><input type="submit" value="<?php esc_attr_e( 'Filter', 'auctionPlugin' ); ?>" class="submit"></p>

		<?php if ( get_query_var('order_status') ): ?>
			&mdash; <a href="<?php echo esc_url( add_query_arg( 'tab', 'orders', CP_DASHBOARD_ORDERS_URL ) ); ?>"><?php _e( 'Remove Filters', 'auctionPlugin' ); ?></a>
		<?php endif; ?>

		<div class="clr"></div>
	</form>

	<?php if ( empty( $orders ) ): ?>

		<div class="pad20"><?php _e( 'No Orders found.', 'auctionPlugin' ); ?></div>

	<?php else: ?>

		<div class="orders-history-legend">
			<h4><?php _e( 'Legend', 'auctionPlugin' ); ?></h4>
			<div class="orders-history-statuses">
				<?php _e( 'Pending', 'auctionPlugin' ); ?>
				<br/><?php _e( 'Failed', 'auctionPlugin' ); ?>
				<br/><?php _e( 'Completed', 'auctionPlugin' ); ?>
				<br/><?php _e( 'Activated', 'auctionPlugin' ); ?>
			</div>
			<div>
				<span><?php echo __( 'Order not processed.', 'auctionPlugin' ); ?></span>
				<br/><span><?php echo __( 'Order failed or manually canceled.', 'auctionPlugin' ); ?></span>
				<br/><span><?php echo __( 'Order processed succesfully but pending moderation before activation.', 'auctionPlugin' ); ?></span>
				<br/><span><?php echo __( 'Order processed succesfully and activated.', 'auctionPlugin' ); ?></span>
			</div>
		</div>

		<table cellpadding="0" cellspacing="0" class="tblwide footable tablet footable-loaded">
			<thead>
				<tr>
					<th data-class="expand"><?php _e( 'ID', 'auctionPlugin' ); ?></th>
					<th class="text-center" data-hide="phone"><?php _e( 'Date', 'auctionPlugin' ); ?></th>
					<th class="text-left" data-hide="phone"><?php _e( 'Order Summary', 'auctionPlugin' ); ?></th>
					<th class="text-center" data-hide="phone"><?php _e( 'Price', 'auctionPlugin' ); ?></th>
					<th class="text-center" data-hide="phone"><?php _e( 'Payment/Status', 'auctionPlugin' ); ?></th>
				</tr>
			</thead>
			<tbody>

			<?php if ( $orders->have_posts() ) : ?>

				<?php while ( $orders->have_posts() ) : $orders->the_post(); ?>

					<?php $order = appthemes_get_order( $orders->post->ID ); ?>
						<tr>
							<td class="order-history-id text-center">#<?php the_ID(); ?></td>
							<td class="date text-center"><strong><?php the_time(__('j M','auctionPlugin')); ?></strong><br/><span class="year"><?php the_time(__('Y','auctionPlugin')); ?></span></td>
							<td class="order-history-summary left">
								<span class="order-history-ad"><?php the_order_ad_link( $order ); ?></span>
								<?php echo cp_get_the_order_summary( $order, $output_type = 'html' ); ?>
							</td>
							<td class="order-history-price center"><?php echo appthemes_get_price( $order->get_total() ); ?></td>
							<td class="order-history-payment center"><?php the_orders_history_payment( $order ); ?></td>
						</tr>

				<?php endwhile; ?>

			<?php else: ?>
				<tr><td colspan="7"><?php _e( 'No Orders found.', 'auctionPlugin' ); ?></td></tr>
			<?php endif; ?>

			</tbody>
		</table>

		<?php appthemes_pagination( '', '', $orders ); ?>

	<?php endif; ?>

<?php else: ?>

	<div class="pad5"></div>
	<p><?php _e( 'You don\'t have any Orders, yet.', 'auctionPlugin' ); ?></p>
	<div class="pad5"></div>

<?php endif; ?>

<?php wp_reset_postdata(); ?>

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

	    </div><!-- /block10 - tab_content -->

<?php } ?>

	    </div><!-- /tab_container -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
}
?>