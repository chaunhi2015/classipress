<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $post, $wpdb, $cpurl;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$msg = "";

	// check if user has purchased any products if not redirect back to dashboard
	$table_uploads = $wpdb->prefix . "cp_auction_plugin_uploads";
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$checkposts = $wpdb->get_results("SELECT * FROM {$table_uploads} LEFT JOIN {$table_purchases} ON {$table_uploads}.pid = {$table_purchases}.pid WHERE {$table_uploads}.type = 'upload' AND {$table_purchases}.buyer = '$uid' AND {$table_purchases}.paid = '1' AND {$table_purchases}.status = 'paid'");
	if( ! $checkposts && ! current_user_can( 'manage_options' ) ) {
	$redirect = cp_auction_url($cpurl['userpanel'], '?_user_panel=1', '&pay');
	wp_redirect( $redirect );
	exit();
	}

	if( isset( $_POST['save_settings'] ) ) {
	$subscribe = isset( $_POST['subscribe'] ) ? esc_attr( $_POST['subscribe'] ) : '';
	if( $subscribe == "yes" ) {
	update_user_meta( $uid, 'cp_subscribe_update_email', $subscribe );
	$msg = "".__('You have been subscribed.','auctionPlugin')."";
	} else {
	delete_user_meta( $uid, 'cp_subscribe_update_email' );
	$msg = "".__('You have been unsubscribed.','auctionPlugin')."";
	}
	}

//$log = get_option( 'cp_auction_plugin_log_downloads' );
if( empty( $log ) ) $log = false;

	$fullpath = cp_auction_plugin_upload_dir();

// Allow direct file download (hotlinking)?
// Empty - allow hotlinking
// If set to nonempty value (Example: example.com) will only allow downloads when referrer contains this text
if ( ! defined( 'ALLOWED_REFERRER' ) ) {
	define( 'ALLOWED_REFERRER', '' );
}

// log downloads?  true/false
if ( ! defined( 'LOG_DOWNLOADS' ) ) {
	define( 'LOG_DOWNLOADS', true );
}

// log file name
if ( ! defined( 'LOG_FILE' ) ) {
	define( 'LOG_FILE', 'downloads.log' );
}

// Allowed extensions list in format 'extension' => 'mime type'
// If myme type is set to empty string then script will try to detect mime type 
// itself, which would only work if you have Mimetype or Fileinfo extensions
// installed on server.
$allowed_ext = array (

  // archives
  'zip' => 'application/zip',

  // documents
  'pdf' => 'application/pdf',
  'doc' => 'application/msword',
  'xls' => 'application/vnd.ms-excel',
  'ppt' => 'application/vnd.ms-powerpoint',
  'pps' => 'application/vnd.ms-powerpoint',
  
  // executables
  'exe' => 'application/octet-stream',

  // images
  'gif' => 'image/gif',
  'png' => 'image/png',
  'jpg' => 'image/jpeg',
  'jpeg' => 'image/jpeg',

  // audio
  'mp3' => 'audio/mpeg',
  'wav' => 'audio/x-wav',

  // video
  'mpeg' => 'video/mpeg',
  'mpg' => 'video/mpeg',
  'mpe' => 'video/mpeg',
  'mov' => 'video/quicktime',
  'avi' => 'video/x-msvideo'
);

$action = ""; if( isset($_GET['_my_downloads']) ) $action = $_GET['_my_downloads'];

if( $action == 2 ) {

	      global $wpdb;

// If hotlinking not allowed then make hackers think there are some server problems
if (ALLOWED_REFERRER !== ''
&& (!isset($_SERVER['HTTP_REFERER']) || strpos(strtoupper($_SERVER['HTTP_REFERER']),strtoupper(ALLOWED_REFERRER)) === false)
) {

  die("<div class='info'>".__('Internal server error. Please contact system administrator.', 'auctionPlugin')."</div>");
}

// Make sure program execution doesn't time out
// Set maximum script execution time in seconds (0 means no limit)
set_time_limit(0);

if (!isset($_GET['f']) || empty($_GET['f'])) {
  die("<div class='info'>".__('Please specify file name for download.', 'auctionPlugin')."</div>");
}

// Nullbyte hack fix
if (strpos($_GET['f'], "\0") !== FALSE) die('');

// Get real file name.
// Remove any path info to avoid hacking by adding relative path, etc.
$fname = "".basename($_GET['f']."aws");

// Get the fullpath from db.
$table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
$row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE filename = '$fname'");
$fullpath = $row->fullpath;

// Check if the file exists
// Check in subfolders too
function find_file ($dirname, $fname, &$file_path) {

  $dir = opendir($dirname);

  while ($file = readdir($dir)) {
    if (empty($file_path) && $file != '.' && $file != '..') {
      if (is_dir($dirname.'/'.$file)) {
        find_file($dirname.'/'.$file, $fname, $file_path);
      }
      else {
        if (file_exists($dirname.'/'.$fname)) {
          $file_path = $dirname.'/'.$fname;
          return;
        }
      }
    }
  }

} // find_file

// get full file path (including subfolders)
$file_path = '';
find_file($fullpath, $fname, $file_path);

if (!is_file($file_path)) {
  die("<div class='info'>".__('File does not exist. Make sure you specified correct file name.', 'auctionPlugin')."</div>"); 
}

// file size in bytes
$fsize = filesize($file_path); 

// file extension
//$fext = strtolower(substr(strrchr($fname,"."),1));
$ids = strtolower(substr(strrchr($fname,"."),1));
$res = explode("aws", $ids);
$fext = $res[0];

// check if allowed extension
if (!array_key_exists($fext, $allowed_ext)) {
  die("<div class='info'>".__('Not allowed file type.', 'auctionPlugin')."</div>"); 
}

// get mime type
if ($allowed_ext[$fext] == '') {
  $mtype = '';
  // mime type is not set, get from server settings
  if (function_exists('mime_content_type')) {
    $mtype = mime_content_type($file_path);
  }
  else if (function_exists('finfo_file')) {
    $finfo = finfo_open(FILEINFO_MIME); // return mime type
    $mtype = finfo_file($finfo, $file_path);
    finfo_close($finfo);  
  }
  if ($mtype == '') {
    $mtype = "application/force-download";
  }
}
else {
  // get mime type defined by admin
  $mtype = $allowed_ext[$fext];
}

// Browser will try to save file with this filename, regardless original filename.
// You can override it if needed.

if (!isset($_GET['fc']) || empty($_GET['fc'])) {
  $asfname = $fname;
}
else {
  // remove some bad chars
  $asfname = str_replace(array('"',"'",'\\','/'), '', $_GET['fc']);
  if ($asfname === '') $asfname = 'NoName';
}

// set headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Type: $mtype");
header("Content-Disposition: attachment; filename=\"$asfname\"");
header("Content-Transfer-Encoding: binary");
header("Content-Length: " . $fsize);

// download
// @readfile($file_path);
$file = @fopen($file_path,"rb");
if ($file) {
  while(!feof($file)) {
    print(fread($file, 1024*8));
    flush();
    if (connection_status()!=0) {
      @fclose($file);
      die();
    }
  }
  @fclose($file);
}

// log downloads
if (!LOG_DOWNLOADS) die();

	global $wpdb;
      $table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
      $row = $wpdb->get_row("SELECT * FROM {$table_name} WHERE filename = '$fname'");

      $pid = $row->pid;
      $sid = $row->post_author;
      
      $IP = cp_auction_users_ip();
      $m_size = $fsize;
      $m_ip = mysql_real_escape_string($IP);
      $tm = time();
      $table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
      $query = "INSERT INTO {$table_name} (pid, post_author, buyer, fname, filename, fullpath, download, size, ip, datemade, type) VALUES (%d, %d, %d, %s, %s, %s, %s, %d, %s, %d, %s)";
      $wpdb->query($wpdb->prepare($query, $pid, $sid, $uid, $asfname, $fname, $fullpath, $download, $m_size, $m_ip, $tm, 'download'));

$f = @fopen(LOG_FILE, 'a+');
if ($f) {
  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
  @fclose($f);
}
}
	cp_auction_header();
?>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="my-downloads" class="aws-box">

            	<h2 class="dotted"><?php echo the_title(); ?></h2>

	<?php if( $msg ) { ?><div class="notice success"><?php echo $msg; ?></div><div class="clear10"></div><?php } ?>

	<form method="post" action="">
	<?php if(get_user_meta( $uid, 'cp_subscribe_update_email', true ) == "yes") $sub = ''.__('Unsubscribe', 'auctionPlugin').'';
	else $sub = ''.__('Subscribe', 'auctionPlugin').''; ?>
	<input type="checkbox" name="subscribe" value="yes" <?php if(get_user_meta( $uid, 'cp_subscribe_update_email', true ) == "yes") echo 'checked="checked"'; ?>> <?php if(get_user_meta( $uid, 'cp_subscribe_update_email', true ) == "yes") echo ''.__('Uncheck this box if you no longer want to be notified about product updates.', 'auctionPlugin').''; else echo ''.__('Check this box if you want to be notified about product updates.', 'auctionPlugin').''; ?>

	<div class="clear10"></div>

	<input type="submit" name="save_settings" value="<?php echo $sub; ?>" class="mbtn btn_orange" />
	</form>

	<div class="clear15"></div>

<?php if(( get_option('cp_auction_plugin_admins_only') == 'yes' ) && ( get_option('cp_auction_plugin_important_info') )) {

	echo '<div class="page_info">'.nl2br(get_option('cp_auction_plugin_important_info')).'</div>';
	echo '<div class="clear20"></div>';

	} ?>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Item', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:195px"><?php _e('Filename', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:80px"><div style="text-align: center;"><?php _e('Size', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:80px"><div style="text-align: center;"><?php _e('Available', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?> &nbsp;</div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_uploads = $wpdb->prefix . "cp_auction_plugin_uploads";
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$numposts = $wpdb->get_results("SELECT * FROM {$table_uploads} LEFT JOIN {$table_purchases} ON {$table_uploads}.pid = {$table_purchases}.pid WHERE {$table_uploads}.type = 'upload' AND {$table_purchases}.buyer = '$uid' AND {$table_purchases}.paid = '1' AND {$table_purchases}.status = 'paid'");

	$i=0;
	if( $numposts ) {
	$rowclass = '';
        foreach( $numposts as $num ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

	$i++;
	$pid = $num->pid;
	$table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$pid' AND type = 'upload'");

	$id		= $res->id;
	$fname 		= $res->fname;
	$ids 		= $res->filename;
	$ext 		= explode("aws", $ids);
	$filename 	= $ext[0];
	$size		= "".round($res->size / 1024)." KB";
	$ip		= $res->ip;
	$download	= "Download";

	global $post;
	$post = get_post( $pid );
	$title = $post->post_title;
	$available = date_i18n('d.m.y', $res->datemade, true);

	if( $id > 0 ) {
   echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column"><strong>'.$title.'</strong></td>
	<td class="text-left">'.$fname.'</td>
	<td class="text-right">'.$size.'</td>
	<td class="text-center">'.$available.'</td>
	<td class="text-center footable-last-column"><a href="'.get_bloginfo('wpurl').'/?_my_downloads=2&f='.$filename.'&fc='.$fname.'">'.$download.'</a></td>
	</tr>';
	}
}
}
	if( empty( $numposts ) || empty( $id ) ) {
   	echo '<tr><td colspan="5">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
	}
	?>
	</tbody>
	</table>

	<div class="clear50"></div>

<script type="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, windowname, 'width=800,height=500,scrollbars=yes');
return false;
}
//-->
</script>

<h1 class="single dotted"><?php _e('Payment History', 'auctionPlugin'); ?></h1>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Author', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:50px"><?php _e('Order ID', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:100px"><div style="text-align: center;"><?php _e('Date', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:50px"><?php _e('Quantity', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:80px"><?php _e('Total Paid', 'auctionPlugin'); ?></th>
		<th class="footable-last-column" data-hide="phone" style="width:70px"><div style="text-align: center;"><?php _e('Details', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$numposts = $wpdb->get_results("SELECT * FROM {$table_transactions} WHERE uid = '$uid' AND post_author > '0' AND status = 'complete' ORDER BY id DESC");
	$i=1;
	if( $numposts ) {
        foreach( $numposts as $num ) {

	$id = $num->id;
	$order_id = $num->order_id;
	$author	  = $num->post_author;
	$quantity = $num->quantity;
	$mc_gross = $num->mc_gross;
	$paiddate = date_i18n('d M. Y', $num->payment_date, true);
	$seller   = get_userdata($author);

	if ( $order_id > 0 ) {
   echo '<tr id="'.$id.'" class="even">
	<td class="text-left footable-first-column"><a href="'.get_author_posts_url($author).'">'.$seller->user_login.'</a></td>
	<td class="text-right">'.$order_id.'</td>
	<td class="text-center">'.$paiddate.'</td>
	<td class="text-center">'.$quantity.' '.__('items', 'auctionPlugin').'</td>
	<td class="text-right">'.cp_auction_format_amount($mc_gross).'</td>
	<td class="text-center footable-last-column"><a href="'.get_bloginfo('wpurl').'/?get_invoice='.$order_id.'" onclick="return popup(this, \'notes\')">'.__('Preview', 'auctionPlugin').'</a></td>
	</tr>';
	}
}
} else {
   echo '<tr><td colspan="6">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

	</div><!-- /my downloads aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>