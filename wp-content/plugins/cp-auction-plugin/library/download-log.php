<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $wpdb;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$before = ""; $after = "";

	cp_auction_header();
?>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="download-log" class="aws-box">

            	<h2 class="dotted"><?php echo the_title(); ?></h2>

	<p><?php _e('Keep control of who is downloading your products / files.', 'auctionPlugin'); ?>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('User', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:90px"><div style="text-align: left;"><?php _e('IP Address', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:195px"><div style="text-align: left;"><?php _e('Filename', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:60px"><div style="text-align: center;"><?php _e('Date', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone" style="width:60px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php

global $wpdb;
$table_uploads = $wpdb->prefix . "cp_auction_plugin_uploads";

$results_per_page = 30;
$paginate_links = "";
$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
 
$results = $wpdb->get_results("SELECT * FROM {$table_uploads} WHERE post_author = '$uid' AND type = 'download' ORDER BY id DESC");

if( $wpdb->num_rows != 0 ) {

	global $wp_rewrite;
 
$pagination_args = array(
	'base' => @esc_url( add_query_arg('paged','%#%') ),
	'format' => '',
	'total' => ceil(sizeof($results)/$results_per_page),
	'current' => $current,
	'show_all' => false,
	'type' => 'plain',
	'prev_text' => '&lsaquo;&lsaquo;',
	'next_text' => '&rsaquo;&rsaquo;',
);
 
if( $wp_rewrite->using_permalinks() )
	$pagination_args['base'] = trailingslashit( esc_url( remove_query_arg('s',get_pagenum_link(1) ) ) ) . 'page/%#%/';
 
if( !empty($wp_query->query_vars['s']) )
	$pagination_args['add_args'] = array('s'=>get_query_var('s'));

	if( $pagination_args['total'] > 1 )
	$paginate_links .= '<span class="total">' . sprintf( __( 'Page %s of %s', APP_TD ), $pagination_args['current'], $pagination_args['total'] ) . '</span>';

	$paginate_links .= paginate_links($pagination_args);

	$start = ($current - 1) * $results_per_page;
	$end = $start + $results_per_page;
	$end = (sizeof($results) < $end) ? sizeof($results) : $end;
	$t=1;
	$rowclass = '';
	for ($i=$start;$i < $end ;++$i ) {
	$result = $results[$i];
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

	$id		= $result->id;
	$pid		= $result->pid;
	$user_id	= $result->buyer;
	$fname 		= $result->fname;
	$ip		= $result->ip;

	$user_login = get_the_author_meta('user_login', $user_id);
	$downdate = date_i18n('d.m.y', $result->datemade, true);

   echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column"><strong>'.$user_login.'</strong></td>
	<td class="text-left">'.$ip.'</td>
	<td class="text-left">'.$fname.'</td>
	<td class="text-center">'.$downdate.'</td>
	<td class="text-center footable-last-column"><a href="#" class="delete-log" id="'.$id.'">'.__('Delete', 'auctionPlugin').'</a></td>
	</tr>';

}
	echo '</tbody></table>';
	echo '<div class="clear10"></div>';
	echo $before . '<div class="pages">';
	echo $paginate_links;
	echo '</div><div class="clr"></div>' . $after;
	$paginate_links = '';
}
else { ?>

	<tr><td colspan="5"><?php _e('No Records Found!', 'auctionPlugin'); ?></td></tr>
	</tbody>
	</table>

<?php } ?>

	</div><!-- /download-log aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>