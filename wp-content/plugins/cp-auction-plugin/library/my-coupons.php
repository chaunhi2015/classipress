<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	$cid = ""; if( isset($_GET['cid']) ) $cid = $_GET['cid'];

	global $current_user, $cpurl, $cp_options;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$ct = get_option('stylesheet');

	$user_posts = cp_auction_count_user_posts( $uid );
	$user = get_userdata($uid);
	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$currency = get_user_meta( $uid, 'currency', true );
	if( ! $currency ) $currency = $cp_options->currency_code;

	$msg = "";
	$error = "";
	$coupon_code = "";
	$coupon_pid = "";
	$coupon_desc = "";
	$coupon_discount = "";
	$coupon_discount_type = "";
	$coupon_start_date = "";
	$coupon_expire_date = "";
	$coupon_status = "";
	$coupon_max_use_count = "";
	$coupon_status = "";

if( $cid > 0 ) {

	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_coupons";
	$coupon = $wpdb->get_row("SELECT * FROM {$table_name} WHERE coupon_id = '$cid'", ARRAY_A);

	$coupon_code = $coupon['coupon_code'];
	$coupon_pid = $coupon['coupon_pid'];
	$coupon_desc = $coupon['coupon_desc'];
	$coupon_discount = $coupon['coupon_discount'];
	$coupon_discount_type = $coupon['coupon_discount_type'];
	$coupon_start_date = $coupon['coupon_start_date'];
	$coupon_expire_date = $coupon['coupon_expire_date'];
	$coupon_status = $coupon['coupon_status'];
	$coupon_max_use_count = $coupon['coupon_max_use_count'];
	$coupon_status = $coupon['coupon_status'];

}

if(isset($_POST['add_coupon'])) {

	$coupon_id = isset( $_POST['coupon_id'] ) ? esc_attr( $_POST['coupon_id'] ) : '';
	$coupon_uid = isset( $_POST['coupon_uid'] ) ? esc_attr( $_POST['coupon_uid'] ) : '';
	$coupon_pid = isset( $_POST['page_id'] ) ? esc_attr( $_POST['page_id'] ) : '';
	$coupon_code = isset( $_POST['coupon_code'] ) ? esc_attr( $_POST['coupon_code'] ) : '';
	$coupon_desc = isset( $_POST['coupon_desc'] ) ? esc_attr( $_POST['coupon_desc'] ) : '';
	$coupon_discount = isset( $_POST['coupon_discount'] ) ? esc_attr( $_POST['coupon_discount'] ) : '';
	$coupon_discount_type = isset( $_POST['coupon_discount_type'] ) ? esc_attr( $_POST['coupon_discount_type'] ) : '';
	$cp_start_date = isset( $_POST['coupon_start_date'] ) ? esc_attr( $_POST['coupon_start_date'] ) : '';
	$cp_expire_date = isset( $_POST['coupon_expire_date'] ) ? esc_attr( $_POST['coupon_expire_date'] ) : '';
	$coupon_status = isset( $_POST['coupon_status'] ) ? esc_attr( $_POST['coupon_status'] ) : '';
	$coupon_max_use_count = isset( $_POST['coupon_max_use_count'] ) ? esc_attr( $_POST['coupon_max_use_count'] ) : '';
	$coupon_owner = isset( $_POST['coupon_owner'] ) ? esc_attr( $_POST['coupon_owner'] ) : '';
	$coupon_start_date = date_i18n('Y-m-d H:i:s', strtotime($cp_start_date), true);
	$coupon_expire_date = date_i18n('Y-m-d H:i:s', strtotime($cp_expire_date), true);
	$coupon_created = $date = date_i18n('Y-m-d H:i:s', false, true);
	$coupon_modified = $date = date_i18n('Y-m-d H:i:s', false, true);
	$reset_usage = isset( $_POST['reset_usage'] ) ? esc_attr( $_POST['reset_usage'] ) : '';

	if(( empty($coupon_code) ) || ( empty($coupon_discount) ) || ( empty($coupon_discount_type) ) || ( empty($cp_start_date) ) || ( empty($cp_expire_date) ) || ( $coupon_max_use_count < 0 )) {
	$error = '<strong>'.__('Please fill in all fields!', 'auctionPlugin').'</strong>';
	} else {
	if( $coupon_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_coupons";
	$where_array = array('coupon_id' => $coupon_id, 'coupon_uid' => $coupon_uid);
	if( $reset_usage = "yes" ) {
	$coupon_use_count = 0;
	$wpdb->update($table_name, array('coupon_pid' => $coupon_pid, 'coupon_code' => $coupon_code, 'coupon_desc' => $coupon_desc, 'coupon_discount' => $coupon_discount, 'coupon_discount_type' => $coupon_discount_type, 'coupon_start_date' => $coupon_start_date, 'coupon_expire_date' => $coupon_expire_date, 'coupon_status' => $coupon_status, 'coupon_use_count' => $coupon_use_count, 'coupon_max_use_count' => $coupon_max_use_count, 'coupon_modified' => $coupon_modified), $where_array);
	} else {
	$wpdb->update($table_name, array('coupon_pid' => $coupon_pid, 'coupon_code' => $coupon_code, 'coupon_desc' => $coupon_desc, 'coupon_discount' => $coupon_discount, 'coupon_discount_type' => $coupon_discount_type, 'coupon_start_date' => $coupon_start_date, 'coupon_expire_date' => $coupon_expire_date, 'coupon_status' => $coupon_status, 'coupon_max_use_count' => $coupon_max_use_count, 'coupon_modified' => $coupon_modified), $where_array);
	}

	$msg = ''.__('The coupon code was updated!', 'auctionPlugin').'';
	} elseif( empty( $coupon_id ) ) {
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_coupons";
	$query = "INSERT INTO {$table_name} (coupon_uid, coupon_pid, coupon_code, coupon_desc, coupon_discount, coupon_discount_type, coupon_start_date, coupon_expire_date, coupon_status, coupon_max_use_count, coupon_owner, coupon_created, coupon_modified) VALUES (%d, %d, %s, %s, %d, %s, %s , %s , %s , %d , %s, %s, %s)"; 
    	$wpdb->query($wpdb->prepare($query, $coupon_uid, $coupon_pid, $coupon_code, $coupon_desc, $coupon_discount, $coupon_discount_type, $coupon_start_date, $coupon_expire_date, $coupon_status, $coupon_max_use_count, $coupon_owner, $coupon_created, $coupon_modified));

	$msg = ''.__('The coupon code was created!', 'auctionPlugin').'';
	}
}

}

?>

<?php cp_auction_header(); ?>

<div class="shadowblock_out" id="noise">

	<div class="shadowblock">

            	<h2 class="dotted"><?php echo the_title(); ?></h2>

		<div class="aws-box">

	<?php if( $msg ) { ?>
	<div class="notice success"><?php echo $msg; ?></div><br />
	<?php } ?>

	<?php if( $error ) { ?>
	<div class="notice error"><?php echo $error; ?></div><br />
	<?php } ?>

	<p><?php _e('Create coupons to offer special discounts to your marketplace customers.', 'auctionPlugin'); ?></p>

	<p><?php _e('This will allow you to setup and configure your coupon codes. You can change value of the coupon, set the coupon to be a % value, set it to be valid within a set time frame, limit the campaign to a specific item or all items, and you can set the coupons so they can only be used once.', 'auctionPlugin'); ?></p>

			</div><!--/aws-box-->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({ 
    activate: function (e, ui) { 
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' }); 
    },
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
    active: $.cookie('homeTab')             
});
});
</script>

<?php if( $ct == "eclassify" ) { ?>

	<div class="tabcontrol">

		<ul class="tabnavig">
<?php } else { ?>

	<div class="tabhome">

		<ul class="tabmenu">
<?php } ?>

    <li><a href="#coupons"><span class="big"><?php _e('Coupons', 'auctionPlugin'); ?></span></a></li>
    <?php if( $user_posts > 0 ) { ?>
    <li><a href="#add"><span class="big"><?php _e('Add Coupons', 'auctionPlugin'); ?></span></a></li>
    <?php } ?>
	</ul>

    <div id="coupons">

	<div class="clr"></div>

<div class="tabunder"><div class="tabs_title"><strong><span class="colour"><?php _e('Coupon Stats', 'auctionPlugin'); ?></span></strong></div></div>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div class="aws-box">

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:100px"><div style="text-align: left;">&nbsp; <?php _e('Code', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:70px"><?php _e('Discount', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:80px"><?php _e('Max Usage', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:50px"><?php _e('Usage', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:100px"><?php _e('Expires', 'auctionPlugin'); ?></th>
		<th data-hide="phone" style="width:50px"><?php _e('Active', 'auctionPlugin'); ?></th>
		<th class="footable-last-column" data-hide="phone" style="width:50px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_coupons";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE coupon_uid = '$uid'", ARRAY_A);
	$i=1;
	if( $results ) {
	$rowclass = '';
        foreach( $results as $result ) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';

	$id	= $result['coupon_id'];
	$pid	= $result['coupon_pid'];
	$code	= $result['coupon_code'];
	$discount 	= $result['coupon_discount'];
	$discount_type 	= $result['coupon_discount_type'];
	$use_count 	= $result['coupon_use_count'];
	$max_use_count 	= $result['coupon_max_use_count'];
	$expire_date 	= $result['coupon_expire_date'];
	$status 	= $result['coupon_status'];
	$exp_date = date_i18n('d M. Y', strtotime($expire_date), true);

	if( $discount_type == "%" ) $type = "%";
	if( $max_use_count == 0 ) $max_use_count = "Unlimited";

   echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column"><strong><a href="'.cp_auction_url($cpurl['coupons'], '?_my_coupons=1', 'cid='.$id.'').'#add">'.$code.'</a></strong></td>
	<td class="text-right">'.$discount.' '.$type.'</td>
	<td class="text-center">'.$max_use_count.'</td>
	<td class="text-center">'.$use_count.'</td>
	<td class="text-left">'.$exp_date.'</td>
	<td class="text-center">'.$status.'</td>
	<td class="text-center footable-last-column"><a href="#" class="delete-coupon" title="'.__('Delete coupon.', 'auctionPlugin').'" id="'.$id.'">'.__('Delete', 'auctionPlugin').'</a></td>
	</tr>';

	}
	} else {
	echo '<tr><td colspan="7">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
}

?>
	</tbody>
	</table>

		</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

</div><!-- /coupons - tab_content -->

<?php if( $user_posts > 0 ) { ?>

    <div id="add">

	<div class="clr"></div>

<div class="tabunder"><div class="tabs_title"><strong><span class="colour"><?php _e('Add New Coupon', 'auctionPlugin'); ?></span></strong></div></div>

   <script type="text/javascript">
      jQuery(document).ready(function(){
         jQuery('#coupon_start_date').datetimepicker({
            dateFormat : 'm/d/yy'
         });
      });
   </script>

   <script type="text/javascript">
      jQuery(document).ready(function(){
         jQuery('#coupon_expire_date').datetimepicker({
            dateFormat : 'm/d/yy'
         });
      });
   </script>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div class="aws-box">

	<form class="cp-ecommerce" name="mainform" action="" method="post" id="mainform">

<p>
	<label for="coupon_code"><a title="<?php _e('Create a coupon code that you wish to use. This will be the actual coupon that you pass out to your customers.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Create a coupon code that you wish to use. This will be the actual coupon that you pass out to your customers.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Code:', 'auctionPlugin'); ?></label>
	<input name="coupon_code" id="coupon_code" value="<?php echo $coupon_code; ?>" class="required" minlength="3" type="text">
</p>

<p>
	<label for="coupon_desc"><a title="<?php _e('Enter a description of your new coupon. It will not be visible on your site.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a description of your new coupon. It will not be visible on your site.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Description:', 'auctionPlugin'); ?></label>
	<textarea name="coupon_desc" id="coupon_desc" class="required" minlength="5"><?php echo $coupon_desc; ?></textarea>
</p>

<p>
	<label for="coupon_discount"><a title="<?php _e('Enter a numeric value for this coupon (do not enter a currency symbol or commas).', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a numeric value for this coupon (do not enter a currency symbol or commas).', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Discount:', 'auctionPlugin'); ?></label>
	<input name="coupon_discount" id="coupon_discount" value="<?php echo $coupon_discount; ?>" class="required" minlength="1" type="text">
</p>

<p>
	<label for="coupon_discount_type"><a title="<?php _e('Either a fixed price or percentage discount.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Either a fixed price or percentage discount.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Type of Discount:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" name="coupon_discount_type" id="coupon_discount_type" class="required valid"><option value="%" <?php if($coupon_discount_type == "%") echo 'selected="selected"'; ?>>%</option><option value="<?php echo $currency; ?>" <?php if($coupon_discount_type == $currency) echo 'selected="selected"'; ?>><?php echo $currency; ?></option></select>
</p>

<p>
	<label for="coupon_max_use_count"><a title="<?php _e('Enter a numeric value for the times you wish this coupon to be usable. Enter 0 for unlimited usage.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter a numeric value for the times you wish this coupon to be usable. Enter 0 for unlimited usage.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Max Usage:', 'auctionPlugin'); ?></label>
	<input name="coupon_max_use_count" id="coupon_max_use_count" value="<?php echo $coupon_max_use_count; ?>" class="required" minlength="1" type="text">
</p>

<p>
	<label for="coupon_start_date"><a title="<?php _e('Enter the start date for this coupon to begin working.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter the start date for this coupon to begin working.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Start Date:', 'auctionPlugin'); ?></label>
	<input name="coupon_start_date" id="coupon_start_date" value="<?php echo $coupon_start_date; ?>" class="required" minlength="1" type="text">
</p>

<p>
	<label for="coupon_expire_date"><a title="<?php _e('Enter the expiration date for this coupon to stop working.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enter the expiration date for this coupon to stop working.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Expire Date:', 'auctionPlugin'); ?></label>
	<input name="coupon_expire_date" id="coupon_expire_date" value="<?php echo $coupon_expire_date; ?>" minlength="1" type="text">
</p>

<p>
	<label for="coupon_status"><a title="<?php _e('If you do not want this coupon available for use, select inactive.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('If you do not want this coupon available for use, select inactive.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Coupon Status:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" name="coupon_status" id="coupon_status" class="required valid"><option value="active" <?php if($coupon_status == "active") echo 'selected="selected"'; ?>><?php _e('Active', 'auctionPlugin'); ?></option><option value="inactive" <?php if($coupon_status == "inactive") echo 'selected="selected"'; ?>><?php _e('Inactive', 'auctionPlugin'); ?></option></select>
</p>

<p>
	<label for="coupon_pid"><a title="<?php _e('Select the product that you want this code to work for.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Select the product that you want this code to work for.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Select Item:', 'auctionPlugin'); ?></label>
	<?php get_ad_coupons_dropdown($uid, $coupon_pid); ?>
</p>

	<?php if( $cid > 0 ) { ?>
<p>
	<label for="reset_usage"><a title="<?php _e('Reset the usage counter.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Reset the usage counter.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Reset Usage:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" name="reset_usage" id="reset_usage" class="valid"><option value="no"><?php _e('No', 'auctionPlugin'); ?></option><option value="yes"><?php _e('Yes', 'auctionPlugin'); ?></option></select>
</p>

	<?php } ?>

	<div class="clear10"></div>

	<?php if( $cid > 0 ) $txt_btn = ''.__('Update Coupon', 'auctionPlugin').'';
	else $txt_btn = ''.__('Create New Coupon', 'auctionPlugin').''; ?>

		<p class="submit"><input class="btn_orange" name="add_coupon" value="<?php echo $txt_btn; ?>" type="submit"></p>
                    <input name="coupon_id" value="<?php echo $cid; ?>" type="hidden">
                    <input name="coupon_uid" value="<?php echo $uid; ?>" type="hidden">
                    <input name="coupon_owner" value="<?php echo $user->user_login; ?>" type="hidden">

                </form>

	</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

	</div><!-- /add - tab_content -->

<?php } ?>

	</div><!-- /tab_control -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>