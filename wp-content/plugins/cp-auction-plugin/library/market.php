<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$ct = get_option('stylesheet');
	$maxposts = get_option('posts_per_page');
	$only_admins = get_option('cp_auction_only_admins_post_auctions');

?>

<?php cp_auction_header(); ?>

<?php if( $ct == "grid-mod" ) { ?>
<!--/ Possible issue in Grid child theme cause these extra divs -->
<script type='text/javascript' src='<?php get_bloginfo('wpurl'); ?>/wp-content/themes/grid-mod/js/grid-mod.js?ver=2.3'></script>
<div>
<?php } ?>

<div class="shadowblock_out" id="noise">

	<div class="shadowblock">

		<div class="aws-box">

            <h2 class="dotted"><?php echo the_title(); ?></h2>

	<p><?php _e('Enjoy The Marketplace, one of the premier shopping malls online. Learn more about our shops, advertisers and more!', 'auctionPlugin'); ?></p>

			</div><!-- / aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({ 
    activate: function (e, ui) { 
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' }); 
    },
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
    active: $.cookie('homeTab')             
});
});
</script>

<?php if( $ct == "eclassify" ) {
	$tabs = "tabnavig"; ?>

	<div class="tabcontrol">

		<ul class="tabnavig">
<?php } else {
	$tabs = "tabmenu"; ?>

	<div class="tabhome">

		<ul class="tabmenu">
<?php } ?>

		<?php if(( get_option('cp_auction_plugin_auctiontype') == "classified" ) || (( get_option('cp_auction_plugin_auctiontype') == "mixed" ) && ( get_option('cp_auction_plugin_classified') == true ))) { ?>
		<li><a href="#block4"><span class="big"><?php _e('Classifieds', 'auctionPlugin'); ?></span></a></li>
		<?php } if(( get_option('cp_auction_plugin_auctiontype') == "wanted" ) || (( get_option('cp_auction_plugin_auctiontype') == "mixed" ) && ( get_option('cp_auction_plugin_wanted') == true ))) { ?>
		<li><a href="#block7"><span class="big"><?php _e( 'Wanted', 'auctionPlugin' ); ?></span></a></li>
		<?php } if(( get_option('cp_auction_plugin_auctiontype') == "normal" || get_option('cp_auction_plugin_auctiontype') == "reverse" || $only_admins == "yes" ) || ( get_option('cp_auction_plugin_auctiontype') == "mixed" 
	&& ( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true || $only_admins == "yes" ))) { ?>
		<li><a href="#block6"><span class="big"><?php _e( 'Auctions', 'auctionPlugin' ); ?></span></a></li>
		<?php } ?>
		<li><a href="#block14"><span class="big"><?php _e( 'Categorized', 'auctionPlugin' ); ?></span></a></li>
		<?php if( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true || $only_admins == "yes" ) { ?>
		<li><a href="#block15"><span class="big"><?php _e( 'Buy Now', 'auctionPlugin' ); ?></span></a></li>
		<?php } if( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true || $only_admins == "yes" ) { ?>
		<li><a href="#block16"><span class="big"><?php _e( 'Last Chance', 'auctionPlugin' ); ?></span></a></li>
		<?php } ?>
	</ul>

<script type="text/javascript">
jQuery('ul.<?php echo $tabs; ?> a').click(function() {
var URL = window.location.href;
var NewURL = URL.split('page')[0];
window.history.pushState('', '', NewURL);
if ( NewURL != URL ) {
window.location = window.location.href;
}
});
</script>

					<?php if(( get_option('cp_auction_plugin_auctiontype') == "classified" ) || (( get_option('cp_auction_plugin_auctiontype') == "mixed" ) && ( get_option('cp_auction_plugin_classified') == true ))) { ?>

					<div id="block4">

						<div class="clr"></div>

						<div class="tabunder"><span class="big"><?php _e( 'Classified Ads', 'auctionPlugin' ); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span></div>

<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'ignore_sticky_posts' => 1, 
	'paged' => $paged, 
	'meta_query' => 
		array( 
		'relation' => 'OR',
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => array( 'wanted', 'normal', 'reverse' ),
			'compare' => 'NOT EXISTS',
			'value' => ''
			), 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => 'classified'
			), 
		), 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

						<div class="sellmyapps">
						<?php get_template_part( 'loop', 'ad_listing' ); ?>
						</div>

					</div><!-- /block4 -->

					<?php } if(( get_option('cp_auction_plugin_auctiontype') == "wanted" ) || (( get_option('cp_auction_plugin_auctiontype') == "mixed" ) && ( get_option('cp_auction_plugin_wanted') == true ))) { ?>

					<div id="block7">

						<div class="clr"></div>

						<div class="tabunder"><span class="big"><?php _e( 'Wanted Ads', 'auctionPlugin' ); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span></div>

<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'ignore_sticky_posts' => 1, 
	'paged' => $paged, 
	'meta_query' => 
		array( 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => 'wanted' 
			) 
		) 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

						<div class="sellmyapps">
						<?php get_template_part( 'loop', 'ad_listing' ); ?>
						</div>

					</div><!-- /block7 -->

					<?php } if(( get_option('cp_auction_plugin_auctiontype') == "normal" || get_option('cp_auction_plugin_auctiontype') == "reverse" || $only_admins == "yes" ) || ( get_option('cp_auction_plugin_auctiontype') == "mixed" && ( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true || $only_admins == "yes" ))) { ?>

					<div id="block6">

						<div class="clr"></div>

						<div class="tabunder"><span class="big"><?php _e( 'Auctions', 'auctionPlugin' ); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span></div>

<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'ignore_sticky_posts' => 1, 
	'paged' => $paged, 
	'meta_query' => 
		array( 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => array( 'normal', 'reverse' ), 
			'compare' => 'IN'
			), 
			array( 
			'key' => 'cp_end_date', 
			'value' => 0, 
			'compare' => '>'
			) 
		) 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

						<div class="sellmyapps">
						<?php get_template_part( 'loop', 'ad_listing' ); ?>
						</div>

					</div><!-- /block6 -->

				<?php } ?>

        <div id="block14">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e( 'Listed by Category', 'auctionPlugin' ); ?></span></strong></span> - <?php _e('View all listings by category, just select a category from the drop down below.', 'auctionPlugin'); ?></div>

	<?php cp_auction_list_by_category(); ?>

        </div><!-- /block14 - tab_content -->

<?php if( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true || $only_admins == "yes" ) { ?>

    <div id="block15">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e( 'Buy it Now Ads', 'auctionPlugin' ); ?></span></strong></span>  - <?php _e('Listed below is all auctions that can be purchased directly. If you choose to purchase an item the auction will close automatically and a payment link will be activated in your user panel.', 'auctionPlugin'); ?></div>

	<?php cp_auction_buy_now_auctions(); ?>

    </div><!-- /block15 - tab_content -->

<?php } if( get_option('cp_auction_plugin_normal') == true || get_option('cp_auction_plugin_reverse') == true || $only_admins == "yes" ) { ?>

    <div id="block16">

	<div class="clr"></div>

<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e( 'Last Chance Auctions', 'auctionPlugin' ); ?></span></strong></span> - <?php _e('The list below gives you an overview of auctions that will close within the next hours.', 'auctionPlugin'); ?></div>

	<?php cp_auction_soon_to_close(); ?>

	</div><!-- /block16 - tab_content -->

	<?php } ?>

      </div><!-- /tab_control -->

<?php cp_auction_footer('page'); ?>