<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $post, $wpdb, $cpurl, $cp_options;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}

	// check if user has posted any ads if not redirect back to dashboard
	$table_uploads = $wpdb->prefix . "cp_auction_plugin_uploads";
	$table_posts = $wpdb->prefix . "posts";
	$checkposts = $wpdb->get_results("SELECT * FROM {$table_uploads} LEFT JOIN {$table_posts} ON {$table_uploads}.pid = {$table_posts}.ID WHERE {$table_uploads}.post_author = '$uid' AND {$table_uploads}.type != 'download' AND {$table_posts}.post_type = '".APP_POST_TYPE."' AND {$table_posts}.post_status = 'publish' OR {$table_posts}.post_status = 'pending'");
	if( ! $checkposts && ! current_user_can( 'manage_options' ) ) {
	$redirect = cp_auction_url($cpurl['userpanel'], '?_user_panel=1', '&ads');
	wp_redirect( $redirect );
	exit();
	}

	$verified = true;
	if( get_option('cp_auction_plugin_verification') > 0 ) {
	$user_verified = get_user_meta( $uid, 'cp_auction_account_verified', true );
	if( $user_verified ) $verified = true;
	if( empty( $user_verified )) $verified = false;
	}

	$msg = "";
	$errors = "";
	$message = "";
	$action = ""; if( isset($_GET['_file_upload']) ) $action = $_GET['_file_upload'];

	if( isset( $_POST['save_settings'] ) ) {
	$enable_email = isset( $_POST['enable_email'] ) ? esc_attr( $_POST['enable_email'] ) : '';
	$message = isset( $_POST['message'] ) ? esc_attr( $_POST['message'] ) : '';
	if( $enable_email == "yes" ) {
	update_user_meta( $uid, 'cp_enable_update_email', $enable_email );
	} else {
	delete_user_meta( $uid, 'cp_enable_update_email' );
	}
	if( $message ) {
	update_user_meta( $uid, 'cp_auction_update_msg', stripslashes( html_entity_decode( $message ) ) );
	} else {
	delete_user_meta( $uid, 'cp_auction_update_msg' );
	}
	$msg = "".__('Settings was saved.','auctionPlugin')."";
	}

$fullpath = cp_auction_plugin_upload_dir();

// Folder to upload files to. Must end with slash /
if ( ! defined( 'DESTINATION_FOLDER' ) ) {
	define( 'DESTINATION_FOLDER', $fullpath );
}

// Maximum allowed file size, Kb
// Set to zero to allow any size
$file_size = get_option( 'cp_auction_plugin_max_size' );
if ( ! defined( 'MAX_FILE_SIZE' ) ) {
	define( 'MAX_FILE_SIZE', $file_size );
}

// Allowed file extensions. Will only allow these extensions if not empty.
// Example: $exts = array('avi','mov','doc');
$list = get_option('cp_auction_file_extensions');
if( $list ) {
$exts = explode(',', $list);
} else {
$exts = array( 'zip', 'txt', 'doc', 'xls', 'pps', 'ppt', 'pdf', 'mp3', 'wav', 'mpeg', 'mpg', 'mpe', 'mov', 'avi', 'gif', 'png', 'jpg', 'jpeg' );
}

// Totally blocked file extensions.
$blocked_exts = array( 'bat', 'exe', 'cmd', 'sh', 'php', 'pl', 'cgi', '386', 'dll', 'com', 'torrent', 'js', 'app', 'jar', 'pif', 'vb', 'vbscript', 'wsf', 'asp', 'cer', 'csr', 'jsp', 'drv', 'sys', 'ade', 'adp', 'bas', 'chm', 'cpl', 'crt', 'csh', 'fxp', 'hlp', 'hta', 'inf', 'ins', 'isp', 'jse', 'htaccess', 'htpasswd', 'ksh', 'lnk', 'mdb', 'mde', 'mdt', 'mdw', 'msc', 'msi', 'msp', 'mst', 'ops', 'pcd', 'prg', 'reg', 'scr', 'sct', 'shb', 'shs', 'url', 'vbe', 'vbs', 'wsc', 'wsf', 'wsh' );

// rename file after upload? false - leave original, true - rename to some unique filename
if ( ! defined( 'RENAME_FILE' ) ) {
	define( 'RENAME_FILE', true );
}

// put a string to append to the uploaded file name (after extension);
// this will reduce the risk of being hacked by uploading potentially unsafe files;
// sample strings: aaa, my, etc.
if ( ! defined( 'APPEND_STRING' ) ) {
	define( 'APPEND_STRING', 'aws' );
}

// Need uploads log? Logs would be saved in the MySql database.
if ( ! defined( 'DO_LOG' ) ) {
	define( 'DO_LOG', true );
}

// Allow script to work long enough to upload big files (in seconds, 2 days by default)
@set_time_limit(300);

// following may need to be uncommented in case of problems
// ini_set("session.gc_maxlifetime","10800");

function showUploadForm($message='') {
  $max_file_size_tag = '';
  if (MAX_FILE_SIZE > 0) {
    // convert to bytes
    $max_file_size_tag = "<input name='MAX_FILE_SIZE' value='".(MAX_FILE_SIZE*1024)."' type='hidden' >\n";
  }
  if( isset($_GET['pid']) > 0 ) {
?>

<form method="post" enctype="multipart/form-data">
  <div><?php echo $message; ?></div><?php echo $max_file_size_tag; ?>

<?php _e('Select file to upload:', 'auctionPlugin'); ?> <input type="file" size="20" name="filename">
  <input type="hidden" value="<?php echo $_GET['pid']; ?>" name="pid">
  <input type="submit" value="<?php _e('Upload', 'auctionPlugin'); ?>" name="submit">
</form>
<div class="clear10"></div>
<?php
$list = get_option('cp_auction_file_extensions');
if( $list ) {
$array = explode(',', $list);
$tag = implode(', ', $array);
echo ''.__('File Formats:','auctionPlugin').' <font style="text-transform: uppercase;">'.$tag.'</font>.';
} else {
echo ''.__('File Formats:','auctionPlugin').' <font style="text-transform: uppercase;">zip, txt, doc, xls, pps, ppt, pdf, mp3, wav, mpeg, mpg, mpe, mov, avi, gif, png, jpg, jpeg</font>.';
}
}
}

if(isset($_POST['submit'])) {
// errors list
$errors = array();

$message = '';

// we should not exceed php.ini max file size
$ini_maxsize = ini_get('upload_max_filesize');
if (!is_numeric($ini_maxsize)) {
  if (strpos($ini_maxsize, 'M') !== false)
    $ini_maxsize = intval($ini_maxsize)*1024*1024;
  elseif (strpos($ini_maxsize, 'K') !== false)
    $ini_maxsize = intval($ini_maxsize)*1024;
  elseif (strpos($ini_maxsize, 'G') !== false)
    $ini_maxsize = intval($ini_maxsize)*1024*1024*1024;
}
if ($ini_maxsize < MAX_FILE_SIZE*1024) {
  $errors[] = "<div class='notice error'><strong>".__('Alert! Maximum upload file size in php.ini (upload_max_filesize) is less than script`s MAX_FILE_SIZE', 'auctionPlugin')."</strong></div><br /><br />";
}

else {
  
  while(true) {

    // make sure destination folder exists
    if (!@file_exists(DESTINATION_FOLDER)) {
      $errors[] = "<div class='notice error'><strong>".__('Destination folder does not exist or no permissions to see it.', 'auctionPlugin')."</strong></div><br /><br />";
      break;
    }

    // check for upload errors
    $error_code = $_FILES['filename']['error'];
    if ($error_code != UPLOAD_ERR_OK) {
      switch($error_code) {
        case UPLOAD_ERR_INI_SIZE: 
          // uploaded file exceeds the upload_max_filesize directive in php.ini
          $errors[] = "<div class='notice error'><strong>".__('File is too big (1).', 'auctionPlugin')."</strong></div><br /><br />";
          break;
        case UPLOAD_ERR_FORM_SIZE: 
          // uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form
          $errors[] = "<div class='notice error'><strong>".__('File is too big (2).', 'auctionPlugin')."</strong></div><br /><br />";
          break;
        case UPLOAD_ERR_PARTIAL:
          // uploaded file was only partially uploaded.
          $errors[] = "<div class='notice error'><strong>".__('Could not upload file (1).', 'auctionPlugin')."</strong></div><br /><br />";
          break;
        case UPLOAD_ERR_NO_FILE:
          // No file was uploaded
          $errors[] = "<div class='notice error'><strong>".__('Could not upload file (2).', 'auctionPlugin')."</strong></div><br /><br />";
          break;
        case UPLOAD_ERR_NO_TMP_DIR:
          // Missing a temporary folder
          $errors[] = "<div class='notice error'><strong>".__('Could not upload file (3).', 'auctionPlugin')."</strong></div><br /><br />";
          break;
        case UPLOAD_ERR_CANT_WRITE:
          // Failed to write file to disk
          $errors[] = "<div class='notice error'><strong>".__('Could not upload file (4).', 'auctionPlugin')."</strong></div><br /><br />";
          break;
        case 8:
          // File upload stopped by extension
          $errors[] = "<div class='notice error'><strong>".__('Could not upload file (5).', 'auctionPlugin')."</strong></div><br /><br />";
          break;
      } // switch

      // leave the while loop
      break;
    }

    // get file name (not including path)
    $filename = @basename($_FILES['filename']['name']);

    // filename of temp uploaded file
    $tmp_filename = $_FILES['filename']['tmp_name'];

    $file_ext = @strtolower(@strrchr($filename,"."));
    if (@strpos($file_ext,'.') === false) { // no dot? strange
      $errors[] = "<div class='notice error'><strong>".__('Suspicious file name or could not determine file extension.', 'auctionPlugin')."</strong></div><br /><br />";
      break;
    }
    $file_ext = @substr($file_ext, 1); // remove dot

    // check file type if needed
    if (count($exts)) {   /// some day maybe check also $_FILES['user_file']['type']
      if (!@in_array($file_ext, $exts)) {
        $errors[] = "<div class='notice error'><strong>".__('Files of this type are not allowed for upload.', 'auctionPlugin')."</strong></div><br /><br />";
        break;
      }
    }

    // check file type if in blocked list
    if (count($blocked_exts)) {
      if (@in_array($file_ext, $blocked_exts)) {
        $errors[] = "<div class='notice error'><strong>".__('Files of this type are not allowed for upload.', 'auctionPlugin')."</strong></div><br /><br />";
        break;
      }
    }

    if (count($exts)) {   /// some day maybe check also $_FILES['user_file']['type']
      if (!@in_array($file_ext, $exts)) {
        $errors[] = "<div class='notice error'><strong>".__('Files of this type are not allowed for upload.', 'auctionPlugin')."</strong></div><br /><br />";
        break;
      }
    }

    // destination filename, rename if set to
    $dest_filename = $filename;
    if (RENAME_FILE) {
      $dest_filename = md5(uniqid(rand(), true)) . '.' . $file_ext;
    }
    // append predefined string for safety
    $dest_filename = $dest_filename . APPEND_STRING;

    // get size
    $filesize = intval($_FILES["filename"]["size"]); // filesize($tmp_filename);

    // make sure file size is ok
    if (MAX_FILE_SIZE > 0 && MAX_FILE_SIZE*1024 < $filesize) {
      $errors[] = "<div class='notice error'><strong>".__('File is too big (3).', 'auctionPlugin')."</strong></div><br /><br />";
      break;
    }

    if (!@move_uploaded_file($tmp_filename , DESTINATION_FOLDER . $dest_filename)) {
      $errors[] = "<div class='notice error'><strong>".__('Could not upload file (6).', 'auctionPlugin')."</strong></div><br /><br />";
      break;
    }

    if ( DO_LOG ) {
      global $wpdb;
      $pid = $_POST['pid'];
      $IP = cp_auction_users_ip();
      $m_ip = $IP;
      $m_size = $filesize;
      $m_fname = $dest_filename;
      $full_path = "".DESTINATION_FOLDER."";
      $ids 	= $dest_filename;
      $res 	= explode("aws", $ids);
      $fname = $res[0];
      $download = "".get_bloginfo('wpurl')."/?_my_downloads=2&f=".$fname."&fc=".$filename."";
      $tm = time();
      $table_name = $wpdb->prefix . "cp_auction_plugin_uploads";
      $where_array = array('pid' => $pid, 'post_author' => $uid, 'buyer' => '0', 'type' => '');
      $result = $wpdb->update($table_name, array('fname' => $filename, 'filename' => $m_fname, 'fullpath' => $full_path, 'download' => $download, 'size' => $m_size, 'ip' => $m_ip, 'datemade' => $tm, 'type' => 'upload'), $where_array);
      if ($result === false) {
        $errors[] = "<div class='notice error'><strong>".__('File was not saved in database.', 'auctionPlugin')."</strong></div><br /><br />";
        break;
      }
    } // if (DO_LOG)

    if( get_user_meta( $uid, 'cp_enable_update_email', true ) == "yes" ) {
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$results = $wpdb->get_results("SELECT * FROM {$table_purchases} WHERE pid = '$pid' AND paid = '1' AND status = 'paid'");
	if( $results ) {
	foreach( $results as $res ) {
	if( get_user_meta( $res->buyer, 'cp_subscribe_update_email', true ) == "yes" ) {
	cp_auction_new_file_uploaded_buyers($pid, $res->buyer);
	}
	}
    }
    }
    	cp_auction_new_file_uploaded_admin( $pid );

    $redirect = cp_auction_url($cpurl['upload'], '?_file_upload=2');
    wp_redirect( $redirect );
    exit();

    break;

  } // while(true)

  // Errors. Show upload form.
  $message = join('',$errors);

}
}
	cp_auction_header();

?>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="file-upload" class="aws-box">

            	<h2 class="dotted"><?php echo the_title(); ?></h2>

	<?php if( $action == 2 ) { ?>
	<div class="notice success"><?php _e('File was successfully uploaded!', 'auctionPlugin'); ?></div>
	<div class="clear10"></div>
	<?php } ?>

	<?php if( $msg ) { ?><div class="notice success"><?php echo $msg; ?></div><div class="clear10"></div><?php } ?>

	<?php if(( get_option('cp_auction_plugin_upl_verification') == "yes" ) && ( ! $verified )) {
	echo '<div class="page_info">'.nl2br(get_option('cp_auction_plugin_verification_info')).'</div>';
	echo '<div class="clear20"></div>';
	} ?>
	<?php if( get_option('cp_auction_upload_page_info') ) {
	echo '<div class="page_info">'.nl2br(get_option('cp_auction_upload_page_info')).'</div>';
	echo '<div class="clear20"></div>';
	} ?>

	<form class="cp-ecommerce" name="mainform" action="" method="post" id="mainform">

<p>
	<label for="enable_email"><a title="<?php _e('Enable this option and your customers will be notified about any file / plugin that has been updated and re-uploaded.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('Enable this option and your customers will be notified about any file / plugin that has been updated and re-uploaded.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Notification Email:', 'auctionPlugin'); ?></label>
	<select class="<?php echo $new_class; ?>" tabindex="2" id="enable_email" name="enable_email"><?php echo cp_auction_yes_no(get_user_meta( $uid, 'cp_enable_update_email', true )); ?></select>
</p>

<p>
	<label for="message"><a title="<?php _e('In this field you can add additional information regarding the file / product that you are about to upload, this may be important information regarding the installation or the use of this new file / product.', 'auctionPlugin'); ?>" href="#" tip="<?php _e('In this field you can add additional information regarding the file / product that you are about to upload, this may be important information regarding the installation or the use of this new file / product.', 'auctionPlugin'); ?>" tabindex="99"><span class="dashicons-before helpico"></span></a><?php _e('Additional Message:', 'auctionPlugin'); ?></label>
	<textarea tabindex="2" name="message" id="message"><?php echo get_user_meta( $uid, 'cp_auction_update_msg', true ); ?></textarea>
</p>
	<div class="clr"></div>

	<p><?php _e('In the above textarea you can add additional information regarding the file / product that you are about to upload, this may be important information regarding the installation or the use of this new file / product.', 'auctionPlugin'); ?></p>

	<div class="clear10"></div>

<p class="submit">
	<input tabindex="3" class="mbtn btn_orange" name="save_settings" id="save_settings" value="<?php _e('Save Settings', 'auctionPlugin'); ?>" type="submit">
</p>

</form>

	<div class="clear15"></div>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:150px"><div style="text-align: left;">&nbsp; <?php _e('Ad Title', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:195px"><div style="text-align: left;"><?php _e('Filename', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:60px"><div style="text-align: center;"><?php _e('Size', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone" style="width:60px"><div style="text-align: center;"><?php _e('Date', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone" style="width:60px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$rowclass = '';
	$table_uploads = $wpdb->prefix . "cp_auction_plugin_uploads";
	$table_posts = $wpdb->prefix . "posts";
	$numposts = $wpdb->get_results("SELECT * FROM {$table_uploads} LEFT JOIN {$table_posts} ON {$table_uploads}.pid = {$table_posts}.ID WHERE {$table_uploads}.post_author = '$uid' AND {$table_uploads}.type != 'download' AND {$table_posts}.post_type = '".APP_POST_TYPE."' AND {$table_posts}.post_status = 'publish' OR {$table_posts}.post_status = 'pending'");

	$i=0;
	if( $numposts ) {
        foreach( $numposts as $num ) {

	$post_id = $num->pid;

	$ad_type = get_post_meta( $post_id, 'cp_auction_my_auction_type', true );
	$avail = get_option('cp_auction_uploads');
	$upl = explode(',', $avail['cp_ad_types']);

	if ( ! empty( $ad_type ) && in_array($ad_type, $upl) ) {
	$postid = $num->pid;

	$i++;
	global $post;
	$post = get_post($postid);

	$ad_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$avail = get_option('cp_auction_uploads');
	$upl = explode(',', $avail['cp_ad_types']);

	$auctions = ""; $classified = ""; $approves = "";
	if( get_option('cp_auction_force_upload') == "yes" ) $auctions = "normal,reverse";
	if( get_option('cp_auction_mrp_force_upload') == "yes" ) $classified = "classified";
	if( get_option('cp_auction_plugin_approves') == "yes" && $ad_type = "normal" || $ad_type = "reverse" ) $approves = "auctions";
	if( get_option('cp_auction_plugin_mrp_approves') == "yes" && $ad_type = "classified" ) $approves = "classified";

	$list = ''.$auctions.','.$classified.'';
	$array = explode( ',', $list );
	$new_list = implode(",", $array);
	$trimmed = trim($new_list, ',');
	$forced = explode( ',', $trimmed );

	if( in_array($ad_type, $forced) ) {

	if( ( $num->fname ) && ( $approves == "auctions" || $approves == "classified" ) ) {
	cp_auction_upload_approves_email($post, $post->ID, $uid);
	}
	elseif( $num->fname && $post->post_status == "pending" ) {
	$my_post = array();
	$my_post['ID'] = $post->ID;
	$my_post['post_status'] = "publish";
	wp_update_post( $my_post );
	}
	}

	$id		= $num->id;
	$filename	= $num->fname;
	$size		= "".round($num->size / 1024)." KB";
	$ip		= $num->ip;
	$datemade	= date_i18n('d.m.y', $num->datemade, true);
	$type		= $num->type;
	if( empty($num->fname) ) $filename = "---";
	if( empty($num->size) ) $size = "---";
	if( $num->datemade < 1 ) $datemade = "---";
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$ads_title = '<a title="'.$post->post_title.'" href="'.get_permalink($post->ID).'"><strong>'.truncate(strip_tags($post->post_title), 15, '..').'</strong></a>';

   echo '<tr id="tr_'.$id.'" class="'.$rowclass.'">';
   if ( $post->post_status == "publish" )
   echo '<td class="text-left footable-first-column">'.$ads_title.'</td>';
   else
   echo '<td class="text-left footable-first-column"><a title="'.$post->post_title.'"><strong>'.truncate(strip_tags($post->post_title), 20, '..').'</strong></a></td>';
   echo '<td class="text-left">'.$filename.'</td>';
   echo '<td class="text-right">'.$size.'</td>';
   echo '<td class="text-center">'.$datemade.'</td>';
	if( $type == "upload" ) {
   echo '<td class="text-center"><a href="#" class="delete-uploads" id="'.$id.'">'.__('Delete', 'auctionPlugin').'</a></td>';
	} else {
	if( $verified ) {
   echo '<td class="text-center"><a href="'.cp_auction_url($cpurl['upload'], '?_file_upload=1', 'pid='.$post->ID.'').'">'.__('Upload','auctionPlugin').'</a></td>';
   	} else {
   	echo '<td class="text-center footable-last-column"><a href="'.cp_auction_url($cpurl['userpanel'], '?_user_panel=1').'">'.__('Verify', 'auctionPlugin').'</a></td>';
   	}
   	}
   echo '</tr>';
	}
	elseif( $numposts ) {
	echo '<tr><td colspan="5">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
	}
}
}
else {
   echo '<tr><td colspan="5">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

	<div class="clear20"></div>

<?php

if (!isset($_POST['submit'])) {
  showUploadForm();
} else {
  // Errors. Show upload form.
  showUploadForm($message);
}
?>

	</div><!-- /fil-upload aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>