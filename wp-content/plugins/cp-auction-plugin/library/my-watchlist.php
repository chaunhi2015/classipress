<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $cpurl;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	cp_auction_header(); ?>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="my-watchlist" class="aws-box">

	<h2 class="dotted"><?php echo the_title(); ?></h2>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" style="width:200px"><div style="text-align: left;">&nbsp; <?php _e('Auction', 'auctionPlugin'); ?></div></th>
		<th data-hide="phone"><div style="text-align: left;"><?php _e('Author', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone" style="width:150px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_watchlist";
	$watching = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' ORDER BY id DESC", ARRAY_A);
	$i = 0;
	if( $watching ) {
	$rowclass = '';
	foreach($watching as $watch) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$i++;
	$id = $watch['id'];
	$aid = $watch['aid'];
	global $post;
	$post = get_post($aid);
	
	$user_info = get_userdata($post->post_author);

?>

<?php
   echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-left footable-first-column"><strong><a title="'.__('View ad.', 'auctionPlugin').'" href="'.get_permalink($aid).'">'.$post->post_title.'</strong></a></td>
	<td class="text-left"><strong><a title="'.__('View profile.', 'auctionPlugin').'" href="'.get_author_posts_url($post->post_author).'">'.$user_info->user_login.'</a></strong></td>
	<td class="text-center footable-last-column"><a title="'.__('View all ads by this user.', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.'').'">'.__('View Ads', 'auctionPlugin').'</a> | <a href="#" class="delete-watchlist" title="'.__('Delete from your watchlist.', 'auctionPlugin').'" id="'.$id.'">'.__('Delete', 'auctionPlugin').'</a></td>
	</tr>';

	}
	
} else {
   echo '<tr><td colspan="3">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
	?>
	</tbody>
	</table>

		</div><!-- /my-watchlist aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>