<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	if ( get_option('cp_auction_post_access') != "yes" ) {
	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();
	}

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $cpurl;

    	$current_user = wp_get_current_user();
    	$uid = $current_user->ID;

	if ( get_option( 'cp_version' ) < '3.4' )
	$pid = cp_auction_get_post_id('_wp_page_template', 'tpl-add-new.php');
	else $pid = cp_auction_get_post_id('_wp_page_template', 'create-listing.php');
	$slugs = get_post($pid);
	if( $slugs ) $slug = $slugs->post_name;
	else $slug = "add-new";
?>

<?php cp_auction_header(false, false, false, 'full-block'); ?>

			<!-- full block -->
	<div class="shadowblock_out">

		<div class="shadowblock">

			<div id="post-new" class="aws-box">

<div id="step1">

<script type="text/javascript">
jQuery(document).ready(function($) {
$('#cp_auction_my_auction_type').change(function(){
if($('#cp_auction_my_auction_type').val() === '')
   {
   return false;
   }
else if($('#aws_ecommerce_adtype').val() != '')
   {
   jQuery("#mainform").prepend('<div class="addnew-redirect"><?php _e('You are being redirected, please wait...', 'auctionPlugin' ); ?></div>');
   jQuery("div.aws-loader-here").addClass('addnew-loader');
   window.location = '<?php echo get_bloginfo('wpurl'); ?>/<?php echo $slug; ?>/?type='+$(this).val();
   }
else
   {
   jQuery("#mainform").prepend('<div class="addnew-redirect"><?php _e('You are being redirected, please wait...', 'auctionPlugin' ); ?></div>');
   jQuery("div.aws-loader-here").addClass('addnew-loader');
   window.location = '<?php echo get_bloginfo('wpurl'); ?>/<?php echo $slug; ?>/?type='+$(this).val();
   }
});
});
</script>

	<?php if ( get_option('stylesheet') == "flatpress" ) { ?>
	<img src="<?php echo appthemes_locate_template_uri('images/step1-' . fl_get_option( 'fl_stylesheet' ) . '.gif'); ?>" alt="" class="stepimg" />
	<?php } else { ?>
	<img src="<?php echo appthemes_locate_template_uri('images/step1.gif'); ?>" alt="" class="stepimg" />

	<?php }
		do_action( 'appthemes_notices' );

	if( check_profile_settings($uid) == false ) { ?>

	<p><?php _e( '<strong>ERROR!!</strong> It is likely that something is missing in your profile settings, or you have enabled the payment platform PayPal and forgotten to enter your PayPal email address.', 'auctionPlugin' ); ?></p>

	<p><?php _e( 'Please check your settings and try again...', 'auctionPlugin' ); ?></p>

	<div class="clear30"></div>

	<p style="text-align: center"><a href="<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1'); ?>" class="btn_orange"><?php _e('Proceed >>','auctionPlugin'); ?></a></p>

	<div class="clear30"></div>

<?php
}
else {

		// display the custom message
		if( get_option('cp_auction_plugin_page_info') ) {
		echo '<div class="page_info">'.nl2br(get_option('cp_auction_plugin_page_info')).'</div>';
		} ?>

	<p class="dotted">&nbsp;</p>

</div>

	<form name="mainform" id="mainform" class="form_step" action="" method="post">

		<ol>

			<li>
			<div class="labelwrapper"><label><?php _e( 'Select Ad Type:', 'auctionPlugin' ); ?></label></div>
			<select class="dropdownlist" name="cp_auction_my_auction_type" id="cp_auction_my_auction_type"><?php echo cp_auction_select_auction_type(); ?></select>
			<div class="clr"></div><div class="aws-loader-here"></div>
			</li>

		</ol>
	</form>
<?php } ?>

			</div><!-- /post-new aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user', 'full-block');
} else { 
cp_auction_footer('page', 'full-block');
}
?>