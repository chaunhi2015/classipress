<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $post, $cpurl, $wpdb;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$id = false; if( isset( $_GET['id'] ) ) $id = $_GET['id'];
	$acceptance = false; if( isset( $_GET['acc'] ) ) $acceptance = $_GET['acc'];

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	$post = get_post( $res->pid );
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	$option = get_option( 'cp_auction_page_titles' );
?>

<?php cp_auction_header();

	if( $acceptance == "yes" ) {

	$tm = time();

	$howmany = get_post_meta($post->ID, 'cp_auction_howmany', true);
	$howmany_added = get_post_meta($post->ID, 'cp_auction_howmany_added', true);

	if ( $my_type == "normal" || $my_type == "reverse" ) {
	$my_post = array();
	$my_post['ID'] = $post->ID;
	$my_post['post_status'] = 'draft';
	wp_update_post( $my_post );

	update_post_meta( $post->ID, 'cp_ad_sold', 'yes' );
	update_post_meta( $post->ID, 'cp_ad_sold_date', $tm );
	update_post_meta( $post->ID, 'winner', $res->uid );
	update_post_meta( $post->ID, 'winner_bid', $res->id );
	update_post_meta( $post->ID, 'winner_amount', $res->bid );

	if ( $my_type == "normal" ) {

	$total = $res->bid;
	$unpaid_total = $res->bid;
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$query = "INSERT INTO {$table_purchases} (pid, uid, buyer, item_name, amount, discount, mc_gross, datemade, numbers, unpaid, net_total, unpaid_total, type, status) VALUES (%d, %d, %d, %s, %f, %f, %f, %d, %d, %d, %f, %f, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $res->uid, $post->post_title, $res->bid, '0', $res->bid, $tm, '1', '1', $res->bid, $res->bid, 'auction', 'unpaid'));

	$country = get_user_meta( $res->uid, 'cp_country', true );
	if ( empty( $country ) && get_option('cp_auction_enable_geoip') == "yes" )
	$country = do_shortcode( '[geoip_detect2 property="country"]' );

	$cp_shipping = cp_auction_add_shipping( $post->ID, $post->post_author, $res->uid, $country );

	}

	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$where_array = array('id' => $res->id);
	$wpdb->update($table_bids, array('winner' => 1, 'status' => 'accepted'), $where_array);

	cp_auction_choose_winner_email( $post->ID, $post, $res->uid, $res->bid );
	}
	else {
	$quantity = 1;
	if( $howmany > 0 || $howmany_added == "yes" ) {
	if( $quantity > $howmany ) $quantity = $howmany;
	$how_many = $howmany - $quantity;
	update_post_meta($post->ID,'cp_auction_howmany',$how_many);
	if( $how_many < 1 ) {
	update_post_meta( $post->ID, 'cp_ad_sold', 'yes' );
	update_post_meta( $post->ID, 'cp_ad_sold_date', $tm );
	update_post_meta( $post->ID, 'cp_auction_howmany', '0' );
	update_post_meta( $post->ID, 'cp_auction_howmany_added', 'none' );
	}
	}
	else {
	if( $howmany != 0 || $howmany_added == "none" ) {
	update_post_meta( $post->ID, 'cp_ad_sold', 'yes' );
	update_post_meta( $post->ID, 'cp_ad_sold_date', $tm );
	update_post_meta( $post->ID, 'cp_auction_howmany', '0' );
	}
	elseif( $howmany_added == "unlimited" ) {
	delete_post_meta( $post->ID, 'cp_ad_sold' );
	delete_post_meta( $post->ID, 'cp_ad_sold_date' );
	}
	}
	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$where_array = array('id' => $res->id);
	$wpdb->update($table_bids, array('winner' => 1, 'status' => 'accepted'), $where_array);
	cp_auction_choose_wanted_winner_email( $post->ID, $post, $res->uid, $res->bid );
	}

?>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

            	<h2 class="dotted"><?php echo $option['title_winner']; ?> - <?php echo $post->post_title; ?></h2>

		<?php _e('Winner has been chosen. Redirecting to dashboard...', 'auctionPlugin'); ?>
	<meta http-equiv="refresh" content="2;url=<?php echo cp_auction_url($cpurl['userpanel'], '?_user_panel=1'); ?>" />

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

	<?php	} else { ?>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

	<h2 class="dotted"><?php echo $option['title_winner']; ?> - <?php echo $post->post_title; ?></h2>

<?php if( $my_type != "wanted" ) { ?>
<?php _e('You have chosen the bid of', 'auctionPlugin'); ?> <?php echo cp_display_price( $res->bid, 'ad', false ); ?> <?php _e('as a winner for your auction. Continue?', 'auctionPlugin'); ?><br/><br/>
<?php } else { ?>
<?php _e('You have chosen the offer of', 'auctionPlugin'); ?> <?php echo cp_display_price( $res->bid, 'ad', false ); ?> <?php _e('as a winner for your Wanted advert. Continue?', 'auctionPlugin'); ?><br/><br/>
<?php } ?>

<p class="btn2">
<a href="<?php echo cp_auction_url($cpurl['winner'], '?_choose_winner=1', 'id='.$id.'&acc=yes'); ?>" class="btn_orange"><?php _e('Yes', 'auctionPlugin'); ?></a> 

<input type="button" class="btn_orange" value="<?php _e('No', 'auctionPlugin'); ?>" onclick="history.back(-1)" />
</p>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

           <?php } ?>

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>