<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

?>

<?php cp_auction_header(false, false, false, 'full-block'); ?>

	<!-- full block -->
	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

<div id="step1">

	<h2 class="dotted"><?php echo the_title(); ?></h2>

	<img src="<?php echo appthemes_locate_template_uri('images/step1.gif'); ?>" alt="" class="stepimg" />

	<span class="text-center"><h2><?php _e( 'Sorry, you can not access this service!', 'auctionPlugin' ); ?></h2></span>

</div>

			</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user', 'full-block');
} else { 
cp_auction_footer('page', 'full-block');
}
?>