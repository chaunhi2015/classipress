<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	function wpse15850_body_classes( $classes ){
    	return array( 'page', 'page-template', 'page-template-cpauction', 'logged-in' );
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $wpdb;

    	$current_user = wp_get_current_user();
    	$uid = $current_user->ID;

	if( isset( $_GET['unsubscribe'] ) ) {
	$mail = $_GET['unsubscribe'];
	$option = get_option( 'cp_auction_unsubscribed' );
	if( empty( $option ) ) $option = $mail;
	else $option = ''.$option.','.$mail.'';
	if( ! empty( $option ) )
	update_option( 'cp_auction_unsubscribed', $option );
	}
?>

<?php cp_auction_header(__('Unsubscribe', 'auctionPlugin'), __('Unsubscribe', 'auctionPlugin'), false, 'full-block'); ?>

			<!-- full block -->
	<div class="shadowblock_out">

		<div class="shadowblock">

			<div id="post-new" class="aws-box">

			<h2 class="dotted"><?php _e('Thank You!', 'auctionPlugin'); ?></h2>

			<div class="clear10"></div>

			<p><?php _e('Your e-mail address', 'auctionPlugin'); ?> <?php if( isset( $_GET['unsubscribe'] ) ) echo $_GET['unsubscribe']; ?> <?php _e('has been unsubscribed from any future promotions.', 'auctionPlugin'); ?></p>

			<div class="clear50"></div>

			</div><!-- /post-new aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user', 'full-block');
} else { 
cp_auction_footer('page', 'full-block');
}
?>