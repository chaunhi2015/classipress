<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$ct = get_option('stylesheet');

	$userid = ""; if ( isset( $_GET['uid'] ) ) $userid = $_GET['uid'];
	$pid = ""; if ( isset( $_GET['pid'] ) ) $pid = $_GET['pid'];
	if ( $pid ) $posts = get_post($pid);
	$user_info = get_userdata($userid);
	$display_user_name = cp_get_user_name($userid);
	$confirm = ''.__('You agree that you have read our policies!','auctionPlugin').'';

	$user_login = $current_user->user_login;
	$author = get_userdata($userid);
	$author_login = $author->user_login;

	$msg = "";
	$info = "";

			//----contact seller		

			if(isset($_POST['submit_contact']))
			{
			$info = 1;
			$your_name	= trim($_POST['your_name']);
			$your_email	= trim($_POST['your_email']);
			$your_subject	= trim($_POST['your_subject']);
			$your_message	= nl2br(trim(strip_tags($_POST['your_message'])));

			if($your_name == "") $info = 2;
			if(!cp_auction_check_email_address($your_email)) $info = 2;
			if($your_subject == "") $info = 2;
			if($your_message == "") $info = 2;

			// get the submitted math answer
    			$rand_post_total = (int)$_POST['rand_total'];

    			// compare the submitted answer to the real answer
    			$rand_total = (int)$_POST['rand_num'] + (int)$_POST['rand_num2'];

    			// if it's a match then send the email
    			if ($rand_total != $rand_post_total) $info = 3;
    			if( !cp_auction_check_email_address( $your_email ) ) $info = 4;

			if($info == 2) $msg = "".__('Please enter all fields correctly!','auctionPlugin')."";
			if($info == 3) $msg = "".__('Incorrect captcha answer!','auctionPlugin')."";
			if($info == 4) $msg = "".__('Please enter an valid email address!','auctionPlugin')."";

			if($info == 1) {

			cp_auction_contact_user_email( $your_message, $your_subject, $your_name, $your_email, $userid );

			$info = 5;
			$msg = "".__('Your message was sent!','auctionPlugin')."";

			}
			}

			//----report fraud

			if(isset($_POST['submit_report']))
			{
			$info = 1;
			$your_name	= trim($_POST['your_name']);
			$your_email	= trim($_POST['your_email']);
			$your_message	= nl2br(trim(strip_tags($_POST['your_message'])));

			if($your_name == "") $info = 6;
			if(!cp_auction_check_email_address($your_email)) $info = 6;
			if($your_message == "") $info = 6;

			// get the submitted math answer
    			$rand_post_total = (int)$_POST['rand_total'];

    			// compare the submitted answer to the real answer
    			$rand_total = (int)$_POST['rand_num'] + (int)$_POST['rand_num2'];

    			// if it's a match then send the email
    			if ($rand_total != $rand_post_total) $info = 7;
    			if( !cp_auction_check_email_address( $your_email ) ) $info = 8;

			if($info == 6) $msgs = "".__('Please enter all fields correctly!','auctionPlugin')."";
			if($info == 7) $msgs = "".__('Incorrect captcha answer!','auctionPlugin')."";
			if($info == 8) $msgs = "".__('Please enter an valid email address!','auctionPlugin')."";

			if($info == 1) {

			cp_auction_report_fraud_email( $posts, $display_user_name, $your_message, $your_name, $your_email, $userid );

			$info = 9;
			$msgs = "".__('Your fraud report was sent!','auctionPlugin')."";

			}
			}

			//----report this ad

			if(isset($_POST['submit_report_ad']))
			{
			$info = 1;
			$your_name	= trim($_POST['your_name']);
			$your_email	= trim($_POST['your_email']);
			$your_message	= nl2br(trim(strip_tags($_POST['your_message'])));

			if($your_name == "") $info = 10;
			if(!cp_auction_check_email_address($your_email)) $info = 10;
			if($your_message == "") $info = 10;

			// get the submitted math answer
    			$rand_post_total = (int)$_POST['rand_total'];

    			// compare the submitted answer to the real answer
    			$rand_total = (int)$_POST['rand_num'] + (int)$_POST['rand_num2'];

    			// if it's a match then send the email
    			if ($rand_total != $rand_post_total) $info = 11;
    			if( !cp_auction_check_email_address( $your_email ) ) $info = 12;

			if($info == 10) $msgs = "".__('Please enter all fields correctly!','auctionPlugin')."";
			if($info == 11) $msgs = "".__('Incorrect captcha answer!','auctionPlugin')."";
			if($info == 12) $msgs = "".__('Please enter an valid email address!','auctionPlugin')."";

			if($info == 1) {

			cp_auction_report_ad_email( $posts, $display_user_name, $your_message, $your_name, $your_email, $userid );

			$info = 13;
			$msgs = "".__('Your ad report was sent!','auctionPlugin')."";

			}
			}

?>

<?php cp_auction_header(); ?>

<div class="shadowblock_out" id="noise">

	<div class="shadowblock">

		<div class="aws-box">

	<?php if($userid != $uid) { ?>
            <h2 class="dotted"><?php _e('Contact or report', 'auctionPlugin'); ?> "<?php echo $display_user_name; ?>"</h2>
            <?php } else { ?>
            <h2 class="dotted"><?php _e('About', 'auctionPlugin'); ?> "<?php echo $display_user_name; ?>"</h2>
            <?php } ?>

		<div id="user-photo"><?php appthemes_get_profile_pic($user_info->ID, $user_info->user_email, 96); ?></div>

		<div class="author-main">

		<ul class="author-info">
		<li><strong><?php _e( 'Member Since:', 'auctionPlugin' ); ?></strong> <?php echo appthemes_display_date( $user_info->user_registered, 'date' ); ?></li>
		<?php if ( !empty($user_info->user_url) ) { ?><li><strong><?php _e( 'Website:', 'auctionPlugin' ); ?></strong> <a href="<?php echo esc_url($user_info->user_url); ?>"><?php echo strip_tags( $user_info->user_url ); ?></a></li><?php } ?>
		<?php if ( !empty($user_info->twitter_id) ) { ?><li><div class="twitterico"></div><a href="http://twitter.com/<?php echo urlencode($user_info->twitter_id); ?>" target="_blank"><?php _e( 'Twitter', 'auctionPlugin' ); ?></a></li><?php } ?>
		<?php if ( !empty($user_info->facebook_id) ) { ?><li><div class="facebookico"></div><a href="http://facebook.com/<?php echo urlencode($user_info->facebook_id); ?>" target="_blank"><?php _e( 'Facebook', 'auctionPlugin' ); ?></a></li><?php } ?>
		</ul>

		<?php cp_author_info( 'page' ); ?>

		</div>

		<p><?php echo nl2br( $user_info->user_description ); ?></p>
		<div class="dotted"></div>

		<div class="pad20"></div>
		<?php if(function_exists("cp_rate_users")) {  cp_rate_users($user_info->ID, false); } ?>

			</div><!--/aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({ 
    activate: function (e, ui) { 
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' }); 
    },
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
    active: $.cookie('homeTab')             
});
});
</script>

<?php if( $ct == "eclassify" ) { ?>

	<div class="tabcontrol">

		<ul class="tabnavig">
<?php } else { ?>

	<div class="tabhome">

		<ul class="tabmenu">
<?php } ?>

    <?php if($userid != $uid) { ?>
    <li><a href="#contact"><span class="big"><?php _e('Contact User', 'auctionPlugin'); ?></span></a></li>
    <li><a href="#report"><span class="big"><?php _e('Report User as Fraudulent', 'auctionPlugin'); ?></span></a></li>
    <?php if ( $pid > 0 ) { ?>
    <li><a href="#report_ad"><span class="big"><?php _e('Report Inappropriate Content', 'auctionPlugin'); ?></span></a></li>
        <?php } } ?>
</ul>

	<?php if($userid != $uid) { ?>

    <div id="report">

	<div class="clr"></div>

	<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('Report Fraud', 'auctionPlugin'); ?></span></strong></span></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

<?php if ( is_user_logged_in() ) { ?>
	    <?php if($info == 9) { ?>
            <div class="notice success"><?php echo "".$msgs.""; ?></div>
            <?php } ?>

            <?php if(($info == 6) || ($info == 7) || ($info == 8)) { ?>
            <div class="notice error"><?php echo "".$msgs.""; ?></div>
            <?php } ?>

	<?php _e('Enter the requested information below to report any fraudulent activity by this user. Describe the fraud attempt as accurately as possible in the description textfield.', 'auctionPlugin'); ?>

	<div class="clear15"></div>

	<form class="cp-ecommerce" action="" method="post">

<p>
	<label for="your_name"><?php _e('Your Name:', 'auctionPlugin'); ?></label>
	<input tabindex="10" id="your_name" name="your_name" value="" type="text">
</p>

<p>
	<label for="your_email"><?php _e('Your Email:', 'auctionPlugin'); ?></label>
	<input tabindex="11" id="your_email" name="your_email" value="" type="text">
</p>

<p class="mod-label">
	<label for="subject"><?php _e('Subject:', 'auctionPlugin'); ?></label>
	<span id="subject"><?php _e('Re: Fraud attempt by user,', 'auctionPlugin'); ?> <?php echo $display_user_name; ?></span>
	<div class="clr"></div>
</p>

<p>
	<label for="contact_message"><?php _e('Description:', 'auctionPlugin'); ?></label>
	<textarea tabindex="12" id="contact_message" name="your_message"></textarea>
</p>

<?php
	// create a random set of numbers for spam prevention
	$randomNum = '';
	$randomNum2 = '';
	$randomNumTotal = '';

	$rand_num = rand(0,9);
	$rand_num2 = rand(0,9);
	$randomNumTotal = $randomNum + $randomNum2;
?>

<p>
	<label for="rand_total"><?php _e('Sum of', 'auctionPlugin'); ?> <?php echo $rand_num; ?> + <?php echo $rand_num2; ?> =</label>
	<input tabindex="13" id="rand_total" name="rand_total" minlength="1" value="" type="text" />
</p>

	<input type="hidden" name="rand_num" value="<?php echo $rand_num; ?>" />
	<input type="hidden" name="rand_num2" value="<?php echo $rand_num2; ?>" />

<p class="submit">
	<input tabindex="14" type="submit" name="submit_report" onclick="return confirm('<?php echo $confirm; ?>')" value="<?php _e('Submit', 'auctionPlugin'); ?>" class="btn_orange" />
</p>

	</form>

      <?php } else { ?>
        <div class="padd10"><?php _e('Sorry, but you must be logged in before you can report fraud.', 'auctionPlugin'); ?></div>
        <?php } ?>

			</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

</div><!-- /report - tab_content -->

<?php } if($userid != $uid && $pid > 0 ) { ?>

    <div id="report_ad">

	<div class="clr"></div>

	<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('Report this Ad:', 'auctionPlugin'); ?> <a target="_blank" href="<?php echo get_permalink($posts->ID); ?>"><?php echo $posts->post_title; ?></a></span></strong></span></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

<?php if ( is_user_logged_in() ) { ?>
	    <?php if($info == 13) { ?>
            <div class="notice success"><?php echo "".$msgs.""; ?></div>
            <?php } ?>

            <?php if(($info == 10) || ($info == 11) || ($info == 12)) { ?>
            <div class="notice error"><?php echo "".$msgs.""; ?></div>
            <?php } ?>

	<?php _e('Enter the requested information below if you believe that the ad listing', 'auctionPlugin'); ?> <a target="_blank" href="<?php echo get_permalink($posts->ID); ?>"><?php echo $posts->post_title; ?></a> <?php _e('or parts of it violates our Terms & Policies.', 'auctionPlugin'); ?>

	<div class="clear15"></div>

	<form class="cp-ecommerce" action="" method="post">

<p>
	<label for="your_name"><?php _e('Your Name:', 'auctionPlugin'); ?></label>
	<input tabindex="15" id="your_name" name="your_name" value="" type="text">
</p>

<p>
	<label for="your_email"><?php _e('Your Email:', 'auctionPlugin'); ?></label>
	<input tabindex="16" id="your_email" name="your_email" value="" type="text">
</p>

<p class="mod-label">
	<label for="subject"><?php _e('Subject:', 'auctionPlugin'); ?></label>
	<span id="subject"><?php _e('Re: Inappropriate content,', 'auctionPlugin'); ?> <?php echo $posts->post_title; ?></span>
	<div class="clr"></div>
</p>

<p>
	<label for="contact_message"><?php _e('Description:', 'auctionPlugin'); ?></label>
	<textarea tabindex="17" id="contact_message" name="your_message"></textarea><div class="clr"></div><div class="info-textarea"><small><p><?php _e('Please give us some details why you believe this ad is suspicious.', 'auctionPlugin'); ?></a></p></small></div>
</p>

<?php
	// create a random set of numbers for spam prevention
	$randomNum = '';
	$randomNum2 = '';
	$randomNumTotal = '';

	$rand_num = rand(0,9);
	$rand_num2 = rand(0,9);
	$randomNumTotal = $randomNum + $randomNum2;
?>

<p>
	<label for="rand_total"><?php _e('Sum of', 'auctionPlugin'); ?> <?php echo $rand_num; ?> + <?php echo $rand_num2; ?> =</label>
	<input tabindex="18" id="rand_total" name="rand_total" minlength="1" value="" type="text" />
</p>

	<input type="hidden" name="rand_num" value="<?php echo $rand_num; ?>" />
	<input type="hidden" name="rand_num2" value="<?php echo $rand_num2; ?>" />

<p class="submit">
	<input tabindex="19" type="submit" name="submit_report_ad" onclick="return confirm('<?php echo $confirm; ?>')" value="<?php _e('Submit', 'auctionPlugin'); ?>" class="btn_orange" />
</p>

	</form>

      <?php } else { ?>
        <div class="padd10"><?php _e('Sorry, but you must be logged in before you can report the ad.', 'auctionPlugin'); ?></div>
        <?php } ?>

			</div><!-- /aws-box -->

		</div><!-- /shadowblock -->

	</div><!-- /shadowblock_out -->

</div><!-- /report_ad - tab_content -->

	<?php } if($userid != $uid) { ?>

    <div id="contact">

	<div class="clr"></div>

	<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('Contact User', 'auctionPlugin'); ?></span></strong></span></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

<?php if ( is_user_logged_in() ) { ?>	
	    <?php if($info == 5) { ?>
            <div class="notice success"><?php echo "".$msg.""; ?></div>
            <?php } ?>

            <?php if(($info == 2) || ($info == 3) || ($info == 4)) { ?>
            <div class="notice error"><?php echo "".$msg.""; ?></div>
            <?php } ?>

	<?php _e('You may contact this user by entering the requested information in the form below.', 'auctionPlugin'); ?>

	<div class="clear15"></div>

	<form class="cp-ecommerce" action="" method="post">

<p>
	<label for="your_name"><?php _e('Your Name:', 'auctionPlugin'); ?></label>
	<input tabindex="20" id="your_name" name="your_name" value="" type="text">
</p>

<p>
	<label for="your_email"><?php _e('Your Email:', 'auctionPlugin'); ?></label>
	<input tabindex="21" id="your_email" name="your_email" value="" type="text">
</p>

<p>
	<label for="subject"><?php _e('Subject:', 'auctionPlugin'); ?></label>
	<input tabindex="22" id="subject" name="your_subject" value="" type="text">
</p>

<p>
	<label for="contact_message"><?php _e('Description:', 'auctionPlugin'); ?></label>
	<textarea tabindex="23" id="contact_message" name="your_message"></textarea>
</p>

<?php
	// create a random set of numbers for spam prevention
	$randomNum = '';
	$randomNum2 = '';
	$randomNumTotal = '';

	$rand_num = rand(0,9);
	$rand_num2 = rand(0,9);
	$randomNumTotal = $randomNum + $randomNum2;
?>

<p>
	<label for="rand_total"><?php _e('Sum of', 'auctionPlugin'); ?> <?php echo $rand_num; ?> + <?php echo $rand_num2; ?> =</label>
	<input tabindex="24" id="rand_total" name="rand_total" minlength="1" value="" type="text" />
</p>

	<input type="hidden" name="rand_num" value="<?php echo $rand_num; ?>" />
	<input type="hidden" name="rand_num2" value="<?php echo $rand_num2; ?>" />

<p class="submit">
	<input tabindex="25" type="submit" name="submit_contact" onclick="return confirm('<?php echo $confirm; ?>')" value="<?php _e('Submit', 'auctionPlugin'); ?>" class="btn_orange" />
</p>

	</form>

      <?php } else { ?>
        <div class="padd10"><?php _e('Sorry, but you must be logged in before you can contact this user.', 'auctionPlugin'); ?></div>
        <?php } ?>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

</div><!-- /contact - tab_content -->

<?php } ?><!-- /if userid -->

</div><!-- /tab_home -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>