<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	appthemes_auth_redirect_login(); // if not logged in, redirect to login page
	nocache_headers();

	function wpse15850_body_classes( $classes ) {
	$classes[] = "page-template-cpauction";
    	return $classes;
	}
	add_filter( 'body_class', 'wpse15850_body_classes', 10, 2 );

	global $current_user, $cpurl;
    	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$role = cp_auction_get_user_role($uid);

	cp_auction_header(); ?>

	<div class="shadowblock_out">

		<div class="shadowblock">

		<div id="my-favourites" class="aws-box">

            	<h2 class="dotted"><?php echo the_title(); ?></h2>

<table class="tblwide footable tablet footable-loaded" border="0" cellpadding="4" cellspacing="1">
		<thead>
		<tr>
		<th class="footable-first-column" data-class="expand" width="5px">&nbsp;</th>
		<th style="width:150px"><div style="text-align: left;"><?php _e('Author', 'auctionPlugin'); ?></div></th>
		<th class="footable-last-column" data-hide="phone" style="width:200px"><div style="text-align: center;"><?php _e('Action', 'auctionPlugin'); ?></div></th>
		</tr>
		</thead>
		<tbody>

<?php
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_auction_plugin_favorites";
	$favorites = $wpdb->get_results("SELECT * FROM {$table_name} WHERE uid = '$uid' ORDER BY id DESC", ARRAY_A);
	$i=0;
	if( $favorites ) {
	$rowclass = '';
	foreach($favorites as $favorite) {
	$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
	$i++;
	$id = $favorite['id'];
	$sid = $favorite['sid'];
	$user_info = get_userdata($sid);

	$cp_company_name = get_user_meta( $sid, 'company_name', true );
	if(empty($cp_company_name)) $cp_company_name = $user_info->user_login;

?>

<?php
   echo '<tr id="'.$id.'" class="'.$rowclass.'">
	<td class="text-right expand footable-first-column">#'.$i.'</td>
	<td class="text-left"><strong><a title="'.__('View other ads by this user.', 'auctionPlugin').'" href="'.cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$sid.'').'">'.$cp_company_name.'</a></strong></td>
	<td class="text-center footable-last-column"><a title="'.__('View profile.', 'auctionPlugin').'" href="'.get_author_posts_url($sid).'">'.__('View Profile', 'auctionPlugin').'</a> | <a href="#" class="delete-favorites" title="'.__('Delete from favourites.', 'auctionPlugin').'" id="'.$id.'">'.__('Delete', 'auctionPlugin').'</a></td>
	</tr>';

	}
	
} else {
   echo '<tr><td colspan="3">'.__('No Records Found!', 'auctionPlugin').'</td></tr>';
   }
?>

	</tbody>
	</table>

		</div><!-- /my-favourites aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

<?php if ( is_user_logged_in() ) {
cp_auction_footer('user');
} else { 
cp_auction_footer('page');
} ?>