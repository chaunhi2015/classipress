<?php

/*
Plugin Name: CP Auction [Arctic WebSolutions]
Plugin URI: https://www.arctic-websolutions.com
Version: 6.0.4
Description: The very best and most convenient way to publish your auctions with ClassiPress.
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


if ( ! defined( 'CP_AUCTION_PLUGIN_VERSION' ) ) {
	define( 'CP_AUCTION_PLUGIN_VERSION', '6.0.4' );
}

if ( ! defined( 'CP_AUCTION_PLUGIN_LATEST_RELEASE' ) ) {
	define( 'CP_AUCTION_PLUGIN_LATEST_RELEASE', '29 September 2016' );
}

if ( ! defined( 'CP_AUCTION_CLASSIPRESS_SUPPORTED' ) ) {
	define( 'CP_AUCTION_CLASSIPRESS_SUPPORTED', '3.5.7' );
}

if ( ! defined( 'CP_AUCTION_ITEM_NAME' ) ) {
	define( 'CP_AUCTION_ITEM_NAME', 'CP Auction Plugin' );
}

if ( ! defined( 'CP_AUCTION_STORE_URL' ) ) {
	define( 'CP_AUCTION_STORE_URL', 'http://www.arctic-websolutions.com' );
}

if ( ! defined( 'CP_AUCTION_TRANS_URL' ) ) {
	define( 'CP_AUCTION_TRANS_URL', '?page=cp-transactions&amp;tab=' );
}

if ( ! defined( 'CP_AUCTION_PLUGIN_FILE' ) ) {
	define( 'CP_AUCTION_PLUGIN_FILE', __FILE__ );
}

if ( ! defined( 'CP_AUCTION_PLUGIN_DIR' ) ) {
	define( 'CP_AUCTION_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'CP_AUCTION_TPL_PATH' ) ) {
	define( 'CP_AUCTION_TPL_PATH', plugin_dir_path( __FILE__ ) . 'templates' );
}


/*
|--------------------------------------------------------------------------
| DEREGISTER JQUERY IN DIFFERENT AREAS
|--------------------------------------------------------------------------
*/

function cp_auction_deregister() {

    $other = false;
    $child_theme = get_option('stylesheet');
    if( $child_theme == "flannel" ) $other = "flannel-scripts";
    if( $child_theme == "grid-mod" ) $other = "grid-mod-js";
    $handles = array('jqueryeasing', 'jcarousellite', $other);

    foreach( $handles as $handle ) {
        wp_deregister_script( $handle );
    }
}


/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION - EASY TRANSLATION USING CODESTYLING LOCALIZATION
|--------------------------------------------------------------------------
*/

function cp_auction_textdomain() {

	$locale = apply_filters('plugin_locale', get_locale(), 'auctionPlugin');

	load_textdomain( 'auctionPlugin', WP_LANG_DIR.'/cp-auction-plugin/auctionPlugin-'.$locale.'.mo' );
	load_plugin_textdomain( 'auctionPlugin', false, dirname( plugin_basename( CP_AUCTION_PLUGIN_FILE ) ) . '/languages/' );
}
if( !is_admin() )
add_action( 'init', 'cp_auction_textdomain' );

function cp_auction_admin_textdomain() {

	$locale = apply_filters('plugin_locale', get_locale(), 'auctionAdmin');

	load_textdomain( 'auctionAdmin', WP_LANG_DIR.'/cp-auction-plugin/auctionAdmin-'.$locale.'.mo' );
	load_plugin_textdomain( 'auctionAdmin', false, dirname( plugin_basename( CP_AUCTION_PLUGIN_FILE ) ) . '/languages/' );
}
if( is_admin() )
add_action( 'init', 'cp_auction_admin_textdomain' );


/*
|--------------------------------------------------------------------------
| ADD LINK TO SETTINGS MENU PAGE
|--------------------------------------------------------------------------
*/

function cp_auction_menu_new_items() {
    global $submenu, $cpurl;
    $slug = ''.$cpurl['new'].'/';
    if( ! empty($slug) )
    $submenu['cp-auction'][1] = array( __( 'Post an Ad', 'auctionPlugin' ), 'manage_options' , get_bloginfo('wpurl').'/'.$slug.'' );
    $submenu['cp-auction'][2] = array( __( 'View Dashboard', 'auctionPlugin' ), 'manage_options' , get_bloginfo('wpurl').'/dashboard/' );
}
add_action( 'admin_menu' , 'cp_auction_menu_new_items' );

function cp_auction_add_link() {

	global $cp_auction_admin_menu;
	$cp_auction_admin_menu = add_menu_page( __( 'CP Auction Settings', 'auctionPlugin' ), __( 'CP Auction', 'auctionPlugin' ), 'manage_options', 'cp-auction', 'cp_auction_plugin_settings', plugins_url( 'cp-auction-plugin/css/images/cp-auction16.png' ), '6,1' );

	global $cp_auction_settings;
	$cp_auction_settings = add_submenu_page( 'cp-auction', __('Settings','auctionPlugin'), __('Settings','auctionPlugin'), 'manage_options', 'cp-auction', 'cp_auction_plugin_settings');

	global $cp_auction_fakemenu, $cpurl;
	if( ! empty( $cpurl['new'] ) )
	$cp_auction_fakemenu = add_submenu_page( 'cp-auction', __('Fakemenu','auctionPlugin'), __('Fakemenu','auctionPlugin'),'manage_options', 'cp-fakemenu', 'cp_auction_plugin_fakemenu');

	global $cp_auction_all;
	$cp_auction_all = add_submenu_page( 'cp-auction', __('Ad Listings','auctionPlugin'), __('Ad Listings','auctionPlugin'),'manage_options', 'cp-all-listings', 'cp_auction_plugin_all_listings');

	global $cp_auction_transactions;
	$cp_auction_transactions = add_submenu_page( 'cp-auction', __('Transactions','auctionPlugin'), __('Transactions','auctionPlugin'),'manage_options', 'cp-transactions', 'cp_auction_plugin_transactions');

	global $cp_auction_downloads;
	$cp_auction_downloads = add_submenu_page( 'cp-auction', __('Downloads','auctionPlugin'), __('Downloads','auctionPlugin'),'manage_options', 'cp-downloads', 'cp_auction_plugin_downloads');

	global $cp_auction_add_purchase;
	$cp_auction_add_purchase = add_submenu_page( 'cp-auction', __('Purchases','auctionPlugin'), __('Purchases','auctionPlugin'),'manage_options', 'cp-add-purchase', 'cp_auction_plugin_all_purchases');

	global $cp_auction_my_auctions;
	$cp_auction_my_auctions = add_submenu_page( 'cp-auction', __('My Listings','auctionPlugin'), __('My Listings','auctionPlugin'),'manage_options', 'cp-my-listings', 'cp_auction_plugin_my_listings');

}
add_action( 'admin_menu', 'cp_auction_add_link', 5 );


/*
|--------------------------------------------------------------------------
| ADD SETTINGS LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'cp_auction_plugin_action_links' );
function cp_auction_plugin_action_links( $links ) {

return array_merge(
array(
'settings' => '<a href="' . get_bloginfo( 'url' ) . '/wp-admin/admin.php?page=cp-auction">'.__( 'Settings', 'auctionPlugin' ).'</a>'
),
$links
);
}


/*
|--------------------------------------------------------------------------
| ADD SUPPORT LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_row_meta', 'cp_auction_plugin_meta_links', 10, 2 );
function cp_auction_plugin_meta_links( $links, $file ) {

$plugin = plugin_basename(__FILE__);

// create link
if ( $file == $plugin ) {
return array_merge(
$links,
array( '<a href="http://www.arctic-websolutions.com/forums/" target="_blank">'.__( 'Support', 'auctionPlugin' ).'</a>' )
);
}
return $links;
}


/*
|--------------------------------------------------------------------------
| INCLUDES & PLUGIN ACTIVATION
|--------------------------------------------------------------------------
*/

include_once('scripts/globals.php');
if ( get_option( 'cp_version' ) > '3.4.1' )
include_once('scripts/functions35.php');
else include_once('scripts/functions.php');

include_once('scripts/hooks.php');
include_once('scripts/ajax-stuff.php');

if ( get_option( 'cp_version' ) < '3.4' )
include_once('scripts/step-functions.php');
else include_once('scripts/step-functions34.php');

include_once('scripts/hooks-frontpage.php');
include_once('scripts/tab-functions.php');
include_once('scripts/pay-functions.php');
include_once('scripts/shipping-functions.php');
include_once('scripts/buy-now-button.php');
include_once('scripts/scripts-enqueue.php');
include_once('scripts/upload-functions.php');
include_once('scripts/membership-functions.php');
include_once('scripts/coupon-functions.php');
include_once('scripts/user-functions.php');
include_once('scripts/avatar-functions.php');

function cp_auction_plugin_activation() {
	global $wp_version, $network_wide;

	$pay_escrow = get_option('cp_auction_plugin_pay_escrow');
	$escrow_credits = get_option('cp_auction_plugin_pay_escrow_credits');
	$bt_escrow = get_option('cp_auction_plugin_pay_bt_escrow');
	$show_auctions = get_option('cp_auction_plugin_show_auctions');
	$verification_info = get_option('cp_auction_plugin_verification_info');
	$wanted_info = get_option('cp_auction_plugin_wanted_info');
	$listing_period = get_option('cp_auction_plugin_max_duration');
	$post_ad_info = get_option('cp_auction_plugin_page_info');
	$conf_popup = get_option('cp_auction_confirmation_popup');
	$period = get_option('cp_auction_period_type');
	$select = get_option('cp_auction_dropdown_select_title');
	$classified = get_option('cp_auction_dropdown_classified_title');
	$wanted = get_option('cp_auction_dropdown_wanted_title');
	$normal = get_option('cp_auction_dropdown_auction_title');
	$reverse = get_option('cp_auction_dropdown_reverse_title');

    	cp_auction_plugin_activate( $network_wide );
	if ( get_option( 'cp_version' ) > '3.3.3' && get_option( 'cp_version' ) < '3.5.0' ) cp_auction_override_views();
	if ( $wp_version >= 4.3 && get_option( 'cp_version' ) < '3.5.0' ) cp_auction_override_registration();

	if( empty( $escrow_credits ) ) {
    	update_option('cp_auction_plugin_pay_escrow_credits', 'Credits Escrow');
    	} if( empty( $bt_escrow ) ) {
    	update_option('cp_auction_plugin_pay_bt_escrow', 'Bank Transfer Escrow');
    	} if( empty( $show_auctions ) ) {
    	update_option('cp_auction_plugin_show_auctions', '8');
    	} if( empty( $verification_info ) ) {
    	update_option('cp_auction_plugin_verification_info', 'Before you can make any purchases, you need to verify your account information, this is done by paying 0.00 to our PayPal account, the amount will be fully refunded once your profile is verified by one of our admins.');
    	} if( empty( $wanted_info ) ) {
    	update_option('cp_auction_plugin_wanted_info', '<strong>Place Offer</strong> - If you have the product that is in demand then you can use this option to submit a presentation of the product including your offer price. Describe your product in the best possible way.');
    	} if( empty( $post_ad_info ) ) {
    	update_option('cp_auction_plugin_page_info', 'You can add / edit information on this page by going to CP Auction -> Settings -> Misc.');
    	} if( empty( $conf_popup ) ) {
    	update_option('cp_auction_confirmation_popup', 'You agree that you have read our policies!');
    	} if( empty( $period ) ) {
    	update_option('cp_auction_period_type', 'days');
    	} if( empty( $select ) ) {
    	update_option('cp_auction_dropdown_select_title', 'Select Ad Type');
    	} if( empty( $classified ) ) {
    	update_option('cp_auction_dropdown_classified_title', 'Classified Ad');
    	} if( empty( $wanted ) ) {
    	update_option('cp_auction_dropdown_wanted_title', 'Wanted');
    	} if( empty( $normal ) ) {
    	update_option('cp_auction_dropdown_auction_title', 'Normal Auction');
    	} if( empty( $reverse ) ) {
    	update_option('cp_auction_dropdown_reverse_title', 'Reverse Auction');
    	}
    	
    	update_option('cp_auction_installed', 'yes');

}
register_activation_hook( __FILE__, 'cp_auction_plugin_activation' );


if( !is_admin() ) {
include_once('scripts/actions.php');
include_once('scripts/loop-functions.php');
include_once('scripts/dashboard-actions.php');
include_once('scripts/access-actions.php');
}

if( get_option('cp_auction_custom_wrapper') == "yes" )
include_once(ABSPATH . 'wp-content/plugins/cpauction-wrapper.php');
else include_once('scripts/wrapper.php');

if( is_admin() ) {
include_once('admin/functions.php');
include_once('admin/admin-menu.php');
include_once('admin/general-settings.php');
include_once('admin/pages.php');
include_once('admin/category-filtering.php');
include_once('admin/payment-settings.php');
include_once('admin/credit-handler.php');
include_once('admin/custom-fields.php');
include_once('admin/setup-instructions.php');
include_once('admin/frontpage.php');
include_once('admin/transactions.php');
include_once('admin/download-log.php');
include_once('admin/uploaded-files.php');
include_once('admin/add-purchase.php');
include_once('admin/pending-purchase.php');
include_once('admin/purchase-history.php');
include_once('admin/my-listings.php');
include_once('admin/ad-listings.php');
include_once('admin/shipping.php');
include_once('admin/misc-settings.php');
include_once('scripts/activate.php');
include_once('scripts/copyus.php');
}
include_once('scripts/widgets.php');
include_once('extensions/social-sharing.php');
include_once('emails/emails.php');


/*
|--------------------------------------------------------------------------
| FUNCTIONS, FILTERS & ACTIONS
|--------------------------------------------------------------------------
*/

//function cp_auction_plugin_sql($s) { return mysql_query($s); }
function cp_auction_plugin_url() { return get_bloginfo('wpurl')."/wp-content/plugins/cp-auction-plugin"; }

if ( ! defined( 'CP_AUCTION_DASHBOARD' ) ) {
global $cpurl;
$dashboard_url = cp_auction_url($cpurl['userpanel'], '?_user_panel=1', '');
	define( 'CP_AUCTION_DASHBOARD', $dashboard_url );
}

if ( ! defined( 'CP_AUCTION_MYADS' ) ) {
global $cpurl;
$myads_url = cp_auction_url($cpurl['myads'], '?_user_panel=1', '');
	define( 'CP_AUCTION_MYADS', $myads_url );
}

function get_prefix() {

	global $wpdb;
	return $wpdb->prefix;
}


function is_logged_in_user_owner($post) {

		$ok = false;
		if(is_user_logged_in())
		{
			global $current_user;
			$current_user = wp_get_current_user();

			if($current_user->ID == $post->post_author)
				$ok = true;

		}

		return $ok;
}


/*
|--------------------------------------------------------------------------
| SET METABOXES
|--------------------------------------------------------------------------
*/

function cp_auction_dashboard_widgets() {
global $wp_meta_boxes;

wp_add_dashboard_widget('cp_auction_widget', ''.__('Latest News From Arctic WebSolutions','auctionPlugin').'', 'cp_auction_dashboard_info');
}
add_action('wp_dashboard_setup', 'cp_auction_dashboard_widgets');

function cp_auction_dashboard_info() {

	echo '<div class="rss-widget">';

		wp_widget_rss_output(array(
			'url' => array('http://feeds.feedburner.com/ArcticWebsolutions'),
			'title' => 'Latest News From Arctic WebSolutions',
			'items' => 2,
			'show_summary' => 1, 
			'show_author' => 0,
			'show_date' => 1
		));
		echo "</div>";
		echo '<div class="dotted"></div>';
		echo '<a href="http://www.arctic-websolutions.com/forums" target="_blank">Support Forum</a> | <a href="http://www.arctic-websolutions.com/my-downloads/" target="_blank">Download</a> | <a href="http://www.arctic-websolutions.com/" target="_blank">Donate</a>';
}


/*
|--------------------------------------------------------------------------
| THEME TEMPLATE REDIRECTS
|--------------------------------------------------------------------------
*/

add_action( 'template_redirect', 'cp_auction_theme_template_redirect', 1 );

function cp_auction_theme_template_redirect() {

	    	global $current_user, $cpurl, $wp_query, $wp;
	    	$current_user = wp_get_current_user();
	    	$uid = $current_user->ID;
	    	//$wp_query->is_404 = false;

		$inst = false;
		$check = check_profile_settings($uid);
		$inst_folder = get_option('cp_auction_inst_folder');
		if( empty( $inst_folder ) ) $inst = false;
		else $inst = ''.get_option('cp_auction_inst_folder').'/';

	    	$stylesheet = get_option('stylesheet');
	    	if( $stylesheet != "pinterclass2" ) {
		add_action('wp_print_scripts', 'cp_auction_load_tabs_scripts');
		}

		if( is_author() )
		add_action('wp_print_scripts', 'cp_auction_load_tabs_scripts');

if( $cpurl['rewrite'] == "yes" ) {

		$cont = explode('?',$_SERVER["REQUEST_URI"]);
		$contr = explode('page',$_SERVER["REQUEST_URI"]);

		$download_log = $cpurl['log'];
                if( ! empty($download_log) ) {
		$uri = $cont[0];
		$reg = explode('/',$_SERVER["REQUEST_URI"]);
		if( in_array( 'page', $reg ) )
		$uri = $contr[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$download_log.'' ) {
		add_filter( 'the_title', 'cp_auction_download_log_page_title' );
		function cp_auction_download_log_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Download Log', 'auctionPlugin').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/download-log.php');
			exit;
		}
                }
		$my_coupons = $cpurl['coupons'];
                if( ! empty($my_coupons) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$my_coupons.'' ) {
		add_filter( 'the_title', 'cp_auction_my_coupons_page_title' );
		function cp_auction_my_coupons_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('My Coupons', 'auctionPlugin').'' );
		}
			add_action( 'wp_print_scripts', 'cp_auction_deregister', 100 );			
			add_action('wp_print_scripts', 'cp_auction_load_datepicker');
			add_action('wp_print_scripts', 'cp_auction_load_tabs_scripts');
			cp_load_form_scripts();
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/my-coupons.php');
			exit;
		}
                }
		$my_downloads = $cpurl['downloads'];
                if( ! empty($my_downloads) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$my_downloads.'' ) {
		add_filter( 'the_title', 'cp_auction_my_downloads_page_title' );
		function cp_auction_my_downloads_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('My Downloads', 'auctionPlugin').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/my-downloads.php');
			exit;
		}
                }
		$file_upload = $cpurl['upload'];
                if( ! empty($file_upload) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$file_upload.'' ) {
		add_filter( 'the_title', 'cp_auction_file_upload_page_title' );
		function cp_auction_file_upload_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Upload Files', 'auctionPlugin').'' );
		}
			cp_load_form_scripts();
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/file-upload.php');
			exit;
		}
		}
		$post_new = $cpurl['new'];
                if( ! empty($post_new) ) {
                global $slugs;
                if ( get_option( 'cp_version' ) < '3.4' )
		$pid = cp_auction_get_post_id('_wp_page_template', 'tpl-add-new.php');
		else $pid = cp_auction_get_post_id('_wp_page_template', 'create-listing.php');
		$slugs = get_post($pid);
		if( $slugs ) $slug = $slugs->post_name;
		else $slug = "add-new";
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$post_new.'' ) {
		if( isset( $_GET['type'] ) ) {
			update_user_meta($uid, 'cp_adtype', $_GET['type']);
			return;
		}
		if( isset( $_GET['step'] ) ) return;
		add_filter( 'the_title', 'cp_auction_post_new_page_title' );
		function cp_auction_post_new_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Submit Your Listing', 'auctionPlugin').'' );
		}
			if ( get_option('cp_auction_plugin_admins_only') == "yes" && ! current_user_can( 'manage_options' ) ) {
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/admins-only.php');
			exit;
			} else {
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/post-new.php');
			exit;
			}
		}
		else if( $uri == ''.$inst.''.$slug.'' && $check == false ) {

			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/post-new.php');
			exit;
		}
		}
		if( get_option('cp_auction_redirect_post_new') == "yes" ) {
		global $slugs;
		if ( get_option( 'cp_version' ) < '3.4' )
		$pid = cp_auction_get_post_id('_wp_page_template', 'tpl-add-new.php');
		else $pid = cp_auction_get_post_id('_wp_page_template', 'create-listing.php');
		$slugs = get_post($pid);
		if( $slugs ) $slug = $slugs->post_name;
		else $slug = "add-new";
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$slug.'' ) {
		if( isset( $_GET['type'] ) ) {
			update_user_meta($uid, 'cp_adtype', $_GET['type']);
			return;
		}
		if( isset( $_GET['step'] ) ) return;
		add_filter( 'the_title', 'cp_auction_redirect_post_new_title' );
		function cp_auction_redirect_post_new_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Submit Your Listing', 'auctionPlugin').'' );
		}
			if ( get_option('cp_auction_plugin_admins_only') == "yes" && ! current_user_can( 'manage_options' ) ) {
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/admins-only.php');
			exit;
			} else {
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/post-new.php');
			exit;
			}
		}
		}
		else if( $uri == ''.$inst.''.$slug.'' && $check == false ) {

			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/post-new.php');
			exit;
		}
		if( get_option('cp_auction_plugin_admins_only') == "yes" && ! current_user_can( 'manage_options' ) ) {
		global $slugs;
		if ( get_option( 'cp_version' ) < '3.4' )
		$pid = cp_auction_get_post_id('_wp_page_template', 'tpl-add-new.php');
		else $pid = cp_auction_get_post_id('_wp_page_template', 'create-listing.php');
		$slugs = get_post($pid);
		if( $slugs ) $slug = $slugs->post_name;
		else $slug = "add-new";
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$slug.'' ) {
		if( isset( $_GET['type'] ) ) {
			update_user_meta($uid, 'cp_adtype', $_GET['type']);
			return;
		}
		if( isset( $_GET['step'] ) ) return;
		add_filter( 'the_title', 'cp_auction_redirect_post_new_title' );
		function cp_auction_redirect_post_new_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Submit Your Listing', 'auctionPlugin').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/admins-only.php');
			exit;
		}
		}
		$affiliate = $cpurl['affiliate'];
                if( ! empty($affiliate) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$affiliate.'' ) {
		add_filter( 'the_title', 'cp_auction_affiliate_page_title' );
		function cp_auction_affiliate_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('My Affiliate Settings', 'auctionPlugin').'' );
		}
			add_action( 'wp_print_scripts', 'cp_auction_deregister', 100 );
			add_action('wp_print_scripts', 'cp_auction_load_tabs_scripts');
			cp_load_form_scripts();
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/aws-affiliate/includes/dashboard.php');
			exit;
		}
		}
		$contact_user = $cpurl['contact'];
                if( ! empty($contact_user) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$contact_user.'' ) {
		add_filter( 'the_title', 'cp_auction_contact_user_page_title' );
		function cp_auction_contact_user_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Contact', 'auctionPlugin').'' );
		}
			add_action( 'wp_print_scripts', 'cp_auction_deregister', 100 );
			if(function_exists("cp_rate_users"))
			add_action( 'wp_enqueue_scripts', 'cp_auction_cprate_scripts' );
			add_action('wp_print_scripts', 'cp_auction_load_tabs_scripts');
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/contact-user.php');
			exit;
		}
		}
		$choose_winner = $cpurl['winner'];
                if( ! empty($choose_winner) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$choose_winner.'' ) {
		add_filter( 'the_title', 'cp_auction_choose_winner_page_title' );
		function cp_auction_choose_winner_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Choose Winner', 'auctionPlugin').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/choose-winner.php');
			exit;
		}
		}
		$authors_listings = $cpurl['authors'];
                if( ! empty($authors_listings) ) {
		$uri = $cont[0];
		$reg = explode('/',$_SERVER["REQUEST_URI"]);
		if( in_array( 'page', $reg ) )
		$uri = $contr[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$authors_listings.'' ) {
		add_filter( 'the_title', 'cp_auction_authors_listings_page_title' );
		function cp_auction_authors_listings_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Welcome', 'auctionPlugin').'' );
		}
			add_action( 'wp_print_scripts', 'cp_auction_deregister', 100 );
			if(function_exists("cp_rate_users"))
			add_action( 'wp_enqueue_scripts', 'cp_auction_cprate_scripts' );
			add_action( 'wp_print_scripts', 'cp_auction_load_tabs_scripts' );
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/authors-listings.php');
			exit;
		}
		}
		$overview = $cpurl['overview'];
                if( ! empty($overview) ) {
		$uri = $cont[0];
		$reg = explode('/',$_SERVER["REQUEST_URI"]);
		if( in_array( 'page', $reg ) )
		$uri = $contr[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$overview.'' ) {
		add_filter( 'the_title', 'cp_auction_overview_page_title' );
		function cp_auction_overview_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Marketplace', 'auctionPlugin').'' );
		}
			add_action( 'wp_print_scripts', 'cp_auction_deregister', 100 );
			add_action( 'wp_print_scripts', 'cp_auction_load_tabs_scripts' );
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/market.php');
			exit;
		}
		}
		$dashboard = $cpurl['userpanel'];
		$filename = "dashboard.php";
		if ( get_option( 'cp_version' ) > '3.4.1' ) $filename = "dashboard35.php";
                if( ( $dashboard && isset( $_GET['pmaction'] ) ) || ( ! empty($dashboard) ) ) {
		$uri = $cont[0];
		$reg = explode('/',$_SERVER["REQUEST_URI"]);
		if( in_array( 'page', $reg ) )
		$uri = $contr[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$dashboard.'' ) {
		add_filter( 'the_title', 'cp_auction_dashboard_page_title' );
		function cp_auction_dashboard_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Dashboard', 'auctionPlugin').'' );
		}
			add_action( 'wp_print_scripts', 'cp_auction_deregister', 100 );
			add_action( 'wp_print_scripts', 'cp_auction_load_tabs_scripts' );
			cp_load_form_scripts();
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/'.$filename.'');
			exit;
		}
		}
		if( get_option('cp_auction_redirect_dashboard') == "yes" ) {
		$filename = "dashboard.php";
		if ( get_option( 'cp_version' ) > '3.4.1' ) $filename = "dashboard35.php";
		$dashboard = ''.$inst.''.$cpurl['userpanel'].'';
		$uri = $cont[0];
		$reg = explode('/',$_SERVER["REQUEST_URI"]);
		if( in_array( 'page', $reg ) )
		$uri = $contr[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.'dashboard' || $uri == $dashboard ) {
		add_filter( 'the_title', 'cp_auction_redirect_dashboard_title' );
		function cp_auction_redirect_dashboard_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Dashboard', 'auctionPlugin').'' );
		}
			add_action( 'wp_print_scripts', 'cp_auction_deregister', 100 );
			add_action( 'wp_print_scripts', 'cp_auction_load_tabs_scripts' );
			cp_load_form_scripts();
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/'.$filename.'');
			exit;
		}
		}
		$my_favorites = $cpurl['favorites'];
                if( ! empty($my_favorites) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$my_favorites.'' ) {
		add_filter( 'the_title', 'cp_auction_my_favorites_page_title' );
		function cp_auction_my_favorites_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('My Favourites', 'auctionPlugin').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/my-favourites.php');
			exit;
		}
		}		
		$my_watchlist = $cpurl['watchlist'];
                if( ! empty($my_watchlist) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$my_watchlist.'' ) {
		add_filter( 'the_title', 'cp_auction_my_watchlist_page_title' );
                function cp_auction_my_watchlist_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('My Watchlist', 'auctionPlugin').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/my-watchlist.php');
			exit;
		}
		}
		$shopping_cart = $cpurl['cart'];
                if( ! empty($shopping_cart) ) {
		$uri = $cont[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$shopping_cart.'' ) {
		add_filter( 'the_title', 'cp_auction_shopping_cart_page_title' );
                function cp_auction_shopping_cart_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Shopping Cart', 'auctionPlugin').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/shopping-cart.php');
			exit;
		}
		}
		$my_ads = $cpurl['myads'];
                if( ! empty($my_ads) ) {
		$uri = $cont[0];
		$reg = explode('/',$_SERVER["REQUEST_URI"]);
		if( in_array( 'page', $reg ) )
		$uri = $contr[0];
		$uri = trim($uri,"/");
		if( $uri == ''.$inst.''.$my_ads.'' ) {
		add_filter( 'the_title', 'cp_auction_my_ads_page_title' );
                function cp_auction_my_ads_page_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('My Ads', 'auctionPlugin').'' );
		}
			add_action( 'wp_print_scripts', 'cp_auction_deregister', 100 );
			add_action( 'wp_print_scripts', 'cp_auction_load_tabs_scripts' );
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/my-ads.php');
			exit;
		}
		}
}

		if(isset($_GET['get_invoice']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/invoice/invoice.php');
			exit;	
		}

		if(isset($_GET['pdf_invoice']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/invoice/invoice-pdf.php');
			exit;	
		}

		if(isset($_GET['_my_downloads']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/my-downloads.php');
			exit;	
		}

		if(isset($_GET['_file_upload']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/file-upload.php');
			exit;	
		}

		if(isset($_GET['confirmation']))
		{
			if( $_GET['confirmation'] == 1 ) {
			add_filter( 'the_title', 'cp_auction_paypal_success_title' );
                	function cp_auction_paypal_success_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Thank You', 'auctionPlugin').'' );
			}
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/paypal/paypal-success.php');
			exit;
			} elseif( $_GET['confirmation'] == 2 ) {
			add_filter( 'the_title', 'cp_auction_bt_success_title' );
                	function cp_auction_bt_success_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Thank You', 'auctionPlugin').'' );
			}
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/banktransfer/credits-purchase-success.php');
			exit;
			} elseif( $_GET['confirmation'] == 3 ) {
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/escrow/banktransfer-success.php');
			exit;
			} elseif( $_GET['confirmation'] == 4 ) {
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/escrow/credits-success.php');
			exit;
			} elseif( $_GET['confirmation'] == 5 ) {
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/credits/withdraw-success.php');
			exit;
			} elseif( $_GET['confirmation'] == 6 ) {
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/credits/purchase-success.php');
			exit;
			} elseif( $_GET['confirmation'] == 7 ) {
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/adaptive/success.php');
			exit;
			} elseif( $_GET['confirmation'] == 8 ) {
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/unsubscribe.php');
			exit;
			} elseif( $_GET['confirmation'] == 9 ) {
			add_filter( 'the_title', 'cp_auction_ubt_success_title' );
                	function cp_auction_ubt_success_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('Thank You', 'auctionPlugin').'' );
			}
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/banktransfer/bank-transfer-success.php');
			exit;
			} else {
			return false;
			exit;
			}
		}

		if(isset($_GET['shopping_cart']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/library/shopping-cart.php');
			exit;	
		}

/*
|--------------------------------------------------------------------------
| INCLUDE PAYMENT & CRONJOB URLS
|--------------------------------------------------------------------------
*/

		if(isset($_GET['_process_payment']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/paypal/process.php');
			exit;
		}
		
		if(isset($_GET['_process_purchase']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/credits/purchase-process.php');
			exit;
		}
		
		if(isset($_GET['_pay_bt_credits']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/banktransfer/credits-purchase.php');
			exit;
		}

		if(isset($_GET['_pay_bank_transfer']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/banktransfer/bank-transfer-process.php');
			exit;
		}

		if(isset($_GET['_pay_bt_escrow']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/escrow/banktransfer-process.php');
			exit;
		}
		
		if(isset($_GET['_pay_cre_escrow']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/escrow/credits-process.php');
			exit;
		}
		
		if(isset($_GET['pay_chained']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/adaptive/pay-chained.php');
			exit;
		}
		
		if(isset($_GET['ipn_chained']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/adaptive/ipn-chained.php');
			exit;
		}
		
		if(isset($_GET['_process_withdraw']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/gateways/credits/withdraw-process.php');
			exit;
		}
		if(isset($_GET['_process_cronjob']))
		{
			header("HTTP/1.1 200 OK");
			include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/cronjob.php');
			exit;
		}
}


/*
|--------------------------------------------------------------------------
| IF CUSTOM TEMPLATES EXISTS LOAD THEM FROM TEMPLATES DIRECTORY
|--------------------------------------------------------------------------
*/

function cp_auction_template_loader( $template ) {

	$stylesheet = strtolower( get_option( 'stylesheet' ) );
	if ( $stylesheet == "simply-responsive-cp-133" ) $stylesheet = "simply-responsive-cp";
	if ( $stylesheet == "skye-child-theme-1.0" || $stylesheet == "skye-child-theme-1.1" ) $stylesheet = "skye";

	$array = array( 'simply-responsive-cp', 'jibo', 'eclassify', 'ultraclassifieds', 'multiport', 'flatron', 'twinpress_classifieds', 'classiclean', 'classiestate', 'citrus-night', 'flatpress', 'rondell', 'rondell_370', 'phoenix', 'eldorado', 'skye' );

	$new_template = basename( $template );

	if ( ( get_option( 'cp_auction_enable_frontpage' ) !== "yes" ) && ( basename( $template ) == "tpl-ads-home.php" || basename( $template ) == "archive-ad_listing.php" ) )
		return $template;

	if ( get_option( 'cp_auction_enable_authorpage' ) !== "yes" && basename( $template ) == "author.php" )
		return $template;

	if ( get_option( 'cp_auction_do_not_replace_single' ) == "yes" && $new_template == "single-ad_listing.php" )
		return $template;

	if ( in_array( $stylesheet, $array ) && $new_template == "single-ad_listing.php" ) {
		$new_template = 'single-ad_listing_'.$stylesheet.'.php';
	}

	if ( file_exists( untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . $new_template ) )
		$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . $new_template;

	return $template;
}
add_filter( 'template_include', 'cp_auction_template_loader', 11 );


/*
|--------------------------------------------------------------------------
| LATEST LISTED WIDGET LOOP
|--------------------------------------------------------------------------
*/

function _cp_auction_get_post_things_widget($post, $t) {

		global $cp_options, $cpurl, $post;
		$pos = get_option('cp_auction_timer');
		$list = explode(',', $pos['cp_disable_timer']);
		$cp_start_price = get_post_meta($post->ID,'cp_start_price',true);
		$cp_buy_now = get_post_meta($post->ID,'cp_buy_now',true);
		if( empty($cp_buy_now) ) $cp_buy_now = get_post_meta($post->ID,'cp_price',true);
		if( is_numeric($cp_buy_now) ) $cp_buy_now = cp_display_price( $cp_buy_now, 'ad', false );
		$cbid = cp_auction_plugin_get_latest_bid($post->ID);
		$act = get_option('cp_auction_plugin_auctiontype');
		$auction_closed = ''.__('Auction','auctionPlugin').'';
		$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
		if( $act == "reverse" || $my_type == "reverse" ) {
		$auction_closed = ''.__('Advert','auctionPlugin').'';
		}
		$dt2 = get_post_meta($post->ID,'cp_end_date',true);
		$cp_ad_sold = end_auction($post->ID);

		$offset = get_option('gmt_offset');
		if($offset < 0) $a = "";
		if($offset >= 0) $a = "+";

		$i = $t+1;
		if( empty($dt2) ) {
		$dt2 = time(); ?>
		<div style="display: none;"><meta scheme="widgetdown<?php echo $i; ?>" name="event_msg" content="" /><meta scheme="widgetdown<?php echo $i; ?>" name="d_unit" content="" /><meta scheme="widgetdown<?php echo $i; ?>" name="d_units" content="" /><span id="widgetdown<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></div>
		<?php } ?>

        <div class="cp_auction_mine cp_widget">

        	<table width="100%">
            <tr>
            <td><?php if ( $cp_options->ad_images ) cp_ad_featured_thumbnail('ad-thumb'); ?></td>
            <td style="vertical-align:top;" width="92%">
            <div class="paddleft">
            <span class="prices_bc">
	    <h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= get_option('cp_auction_plugin_widget_title') ) echo mb_substr( get_the_title(), 0, get_option('cp_auction_plugin_widget_title') ).'...'; else the_title(); ?></a></h3>
	    <?php if( $my_type == "normal" || $my_type == "reverse" ) { ?>
	    <?php if($cp_start_price > 0) { ?>
            <?php _e('Start Price:', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_start_price, 'ad', false ); ?><br/>
            <?php } ?>
		<?php if(($act != 'freebid') && ($cp_buy_now > 0)) { ?>            
	    <?php _e('Buy Now Price:', 'auctionPlugin'); ?> <?php echo $cp_buy_now; ?><br/> <?php } ?>
	    <?php if($cp_start_price > 0) { ?>
            <strong><?php _e('Current Bid:', 'auctionPlugin'); ?></strong> <?php echo cp_display_price( $cbid, 'ad', false ); ?><br/>
            <?php }
            if( $dt2 && ! in_array("widgets", $list) ) { ?>
            <?php _e('Expires:', 'auctionPlugin'); ?> <font color="navy"><?php if($cp_ad_sold == 'yes') { echo ''.$auction_closed.' '.__('is closed.','auctionPlugin').''; ?></font>
			<?php } else
				{ ?></font><meta scheme="widgetdown<?php echo $i; ?>" name="event_msg" content="<?php echo $auction_closed; ?> <?php _e('is closed.', 'auctionPlugin'); ?>" /><meta scheme="widgetdown<?php echo $i; ?>" name="d_unit" content=" <?php _e('day', 'auctionPlugin'); ?>" /><meta scheme="widgetdown<?php echo $i; ?>" name="d_units" content=" <?php _e('days', 'auctionPlugin'); ?>" /><font color="navy"><span id="widgetdown<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></font>
				<?php }
				}
			}
	   if( empty($my_type) || $my_type == "classified" || $my_type == "wanted" ) {
	   if ( $my_type == "wanted" ) { ?>
	   <strong><?php _e('Max. Price:', 'auctionPlugin'); ?></strong> <?php echo $cp_buy_now; ?>
	   <?php } else { ?>
	   <strong><?php _e('Item Price:', 'auctionPlugin'); ?></strong> <?php echo $cp_buy_now; ?>
	   <?php } ?>
	   <br/>
	   <?php _e('Visit our online store for more<br />products,', 'auctionPlugin'); ?> <a href="<?php echo cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.''); ?>"><?php _e('click here!', 'auctionPlugin'); ?></a>
	   <?php } ?>
            </span>

            </div>
            </td>
            </tr>
            </table>
        </div>
<div class="clear10"></div>
<?php	
}


/*
|--------------------------------------------------------------------------
| FEATURED WIDGET LOOP
|--------------------------------------------------------------------------
*/

function _cp_auction_get_post_things_featured_widget($post, $t) {

		global $cp_options, $cpurl, $post;
		$pos = get_option('cp_auction_timer');
		$list = explode(',', $pos['cp_disable_timer']);
		$cp_start_price = get_post_meta($post->ID,'cp_start_price',true);
		$cp_buy_now = get_post_meta($post->ID,'cp_buy_now',true);
		if( empty($cp_buy_now) ) $cp_buy_now = get_post_meta($post->ID,'cp_price',true);
		if( is_numeric($cp_buy_now) ) $cp_buy_now = cp_display_price( $cp_buy_now, 'ad', false );
		$cp_company_name = get_user_meta( $post->post_author, 'company_name', true );
		$author = get_userdata($post->post_author);
		$website = $author->user_url;
		$cbid = cp_auction_plugin_get_latest_bid($post->ID);
		$act = get_option('cp_auction_plugin_auctiontype');
		$auction_closed = ''.__('Auction','auctionPlugin').'';
		$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
		if( $act == "reverse" || $my_type == "reverse" ) {
		$auction_closed = ''.__('Advert','auctionPlugin').'';
		}
		$dt2 = get_post_meta($post->ID,'cp_end_date',true);
		$cp_ad_sold = end_auction($post->ID);

		$offset = get_option('gmt_offset');
		if($offset < 0) $a = "";
		if($offset >= 0) $a = "+";

		$i = $t+1;
		if( empty($dt2) ) {
		$dt2 = time(); ?>
		<div style="display: none;"><meta scheme="fwidgetdown<?php echo $i; ?>" name="event_msg" content="" /><meta scheme="fwidgetdown<?php echo $i; ?>" name="d_unit" content="" /><meta scheme="fwidgetdown<?php echo $i; ?>" name="d_units" content="" /><span id="fwidgetdown<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></div>
		<?php } ?>

        <div class="cp_auction_mine cp_widget">

        	<table width="100%">
            <tr>
            <td><?php if ( $cp_options->ad_images ) cp_ad_featured_thumbnail('ad-thumb'); ?></td>
            <td style="vertical-align:top;" width="92%">
            <div class="paddleft">
            <span class="prices_bc">
	    <h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= get_option('cp_auction_plugin_widget_title') ) echo mb_substr( get_the_title(), 0, 100 ).'...'; else the_title(); ?></a></h3>
	    <?php if( $my_type == "normal" || $my_type == "reverse" ) { ?>
	    <?php if($cp_start_price > 0) { ?>
            <?php _e('Giá khởi điểm:', 'auctionPlugin'); ?> <?php echo str_replace("$","",cp_display_price( $cp_start_price, 'ad', false )). " VND"; ?><br/>
            <?php } ?>
		<?php if(($act != 'freebid') && ($cp_buy_now > 0)) { ?>            
	    <?php _e('Giá mua ngay:', 'auctionPlugin'); ?> <?php echo $cp_buy_now; ?><br/> <?php } ?>
	    <?php if($cp_start_price > 0) { ?>
            <strong><?php _e('Giá hiện tại:', 'auctionPlugin'); ?></strong> <?php echo str_replace("$","",cp_display_price( $cbid, 'ad', false )). " VND"; ?><br/>
            <?php }
            if( $dt2 && ! in_array("widgets", $list) ) { ?>
            <?php _e('Hết hạn:', 'auctionPlugin'); ?> <font color="navy"><?php if($cp_ad_sold == 'yes') { echo ''.$auction_closed.' '.__('được đóng.','auctionPlugin').''; ?></font>
			<?php } else
				{ ?></font><meta scheme="fwidgetdown<?php echo $i; ?>" name="event_msg" content="<?php echo $auction_closed; ?> <?php _e('được đóng.', 'auctionPlugin'); ?>" /><meta scheme="fwidgetdown<?php echo $i; ?>" name="d_unit" content=" <?php _e('day', 'auctionPlugin'); ?>" /><meta scheme="fwidgetdown<?php echo $i; ?>" name="d_units" content=" <?php _e('days', 'auctionPlugin'); ?>" /><font color="navy"><span id="fwidgetdown<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></font>
				<?php }
				}
			}
	   if( empty($my_type) || $my_type == "classified" || $my_type == "wanted" ) {
	   if ( $my_type == "wanted" ) { ?>
	   <strong><?php _e('Max. Price:', 'auctionPlugin'); ?></strong> <?php echo $cp_buy_now; ?>
	   <?php } else { ?>
		   <?php if( $cp_buy_now ) { ?>
		   <strong><?php _e('Giá sản phẩm:', 'auctionPlugin'); ?></strong> <?php echo str_replace("$","",$cp_buy_now) . " VND"; ?>
		   <?php } ?>
	   <?php } ?>
	   <br/>
	   <?php if( $cp_company_name ) { ?>
	   <?php _e('The most popular products', 'auctionPlugin'); ?><br/>
	   <?php _e('by', 'auctionPlugin'); ?><strong> <a href="<?php echo $website; ?>" target="_blank"><?php echo $cp_company_name; ?></a></strong>
	   <?php } else { ?>
	   <?php _e('Xem thêm sản phẩm khác của bạn <br />click ', 'auctionPlugin'); ?> <a href="<?php echo cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.''); ?>"><?php _e('xem thêm!', 'auctionPlugin'); ?></a>
	   <?php } } ?>
            </span>

            </div>
            </td>
            </tr>
            </table>
        </div>
<div class="clear10"></div>

<?php	
}


/*
|--------------------------------------------------------------------------
| CLOSING SOON WIDGET LOOP
|--------------------------------------------------------------------------
*/

function _cp_auction_get_post_things_last_widget($post, $t) {

		global $cp_options, $cpurl, $post;
		$pos = get_option('cp_auction_timer');
		$list = explode(',', $pos['cp_disable_timer']);
		$valid = get_post_meta($post->ID,'cp_sys_expire_date',true);
		$cp_start_price = get_post_meta($post->ID,'cp_start_price',true);
		$cp_buy_now = get_post_meta($post->ID,'cp_buy_now',true);
		$cbid = cp_auction_plugin_get_latest_bid($post->ID);
		$act = get_option('cp_auction_plugin_auctiontype');
		$auction_closed = ''.__('Auction','auctionPlugin').'';
		$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
		if( $act == "reverse" || $my_type == "reverse" ) {
		$auction_closed = ''.__('Advert','auctionPlugin').'';
		}
		$dt2 = get_post_meta($post->ID,'cp_end_date',true);
		$cp_ad_sold = end_auction($post->ID);

		$offset = get_option('gmt_offset');
		if($offset < 0) $a = "";
		if($offset >= 0) $a = "+";

		$i = $t+1;
		if( empty($dt2) ) {
		$dt2 = time(); ?>
		<div style="display: none;"><meta scheme="lwidgetdown<?php echo $i; ?>" name="event_msg" content="" /><meta scheme="lwidgetdown<?php echo $i; ?>" name="d_unit" content="" /><meta scheme="lwidgetdown<?php echo $i; ?>" name="d_units" content="" /><span id="lwidgetdown<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></div>
		<?php } ?>

        <div class="cp_auction_mine cp_widget">

        	<table width="100%">
            <tr>
            <td><?php if ( $cp_options->ad_images ) cp_ad_featured_thumbnail('ad-thumb'); ?></td>
            <td style="vertical-align:top;" width="92%">
            <div class="paddleft">
            <span class="prices_bc">
	    <h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= get_option('cp_auction_plugin_widget_title') ) echo mb_substr( get_the_title(), 0, get_option('cp_auction_plugin_widget_title') ).'...'; else the_title(); ?></a></h3>
	    <?php if( $my_type == "normal" || $my_type == "reverse" ) { ?>
	    <?php if($cp_start_price > 0) { ?>
            <?php _e('Start Price:', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_start_price, 'ad', false ); ?><br/>
            <?php } ?>
		<?php if(($act != 'wanted') && ($cp_buy_now > 0)) { ?>            
	    <?php _e('Buy Now Price:', 'auctionPlugin'); ?> <?php echo cp_display_price( $cp_buy_now, 'ad', false ); ?><br/> <?php } ?>
	    <?php if($cp_start_price > 0) { ?>
            <strong><?php _e('Current Bid:', 'auctionPlugin'); ?></strong> <?php echo cp_display_price( $cbid, 'ad', false ); ?><br/>
            <?php }
            if( $dt2 && ! in_array("widgets", $list) ) { ?>
            <?php _e('Expires:', 'auctionPlugin'); ?> <font color="navy"><?php if($cp_ad_sold == 'yes') { echo ''.$auction_closed.' '.__('is closed.','auctionPlugin').''; ?></font>
            <?php } else
				{ ?></font><meta scheme="lwidgetdown<?php echo $i; ?>" name="event_msg" content="<?php echo $auction_closed; ?> <?php _e('is closed.', 'auctionPlugin'); ?>" /><meta scheme="lwidgetdown<?php echo $i; ?>" name="d_unit" content=" <?php _e('day', 'auctionPlugin'); ?>" /><meta scheme="lwidgetdown<?php echo $i; ?>" name="d_units" content=" <?php _e('days', 'auctionPlugin'); ?>" /><font color="navy"><span id="lwidgetdown<?php echo $i; ?>"><?php echo date_i18n('Y-m-d H:i:s', $dt2, true); ?> GMT<?php echo $a; ?><?php echo $offset; ?></span></font>
				<?php }
				}
			}
	   if( empty($my_type) || $my_type == "classified" || $my_type == "wanted" ) {
	   if ( $my_type == "wanted" ) { ?>
	   <strong><?php _e('Max. Price:', 'auctionPlugin'); ?></strong> <?php echo cp_display_price( $cp_buy_now, 'ad', false ); ?>
	   <?php } else { ?>
	   <strong><?php _e('Item Price:', 'auctionPlugin'); ?></strong> <?php echo cp_display_price( $cp_buy_now, 'ad', false ); ?>
	   <?php } ?>
	   <br/>
	   <?php _e('Visit our online store for more<br />products,', 'auctionPlugin'); ?> <a href="<?php echo cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.''); ?>"><?php _e('click here!', 'auctionPlugin'); ?></a>
	   <?php } ?>
            </span>

            </div>
            </td>
            </tr>
            </table>
        </div>
<div class="clear10"></div>

<?php	
}


/*
|--------------------------------------------------------------------------
| WANTED ADS WIDGET LOOP
|--------------------------------------------------------------------------
*/

function _cp_auction_get_wanted_ads_widget($post) {

		global $cp_options, $cpurl, $post;
		$cp_max_price = get_post_meta($post->ID,'cp_max_price',true);
		$cp_want_to = get_post_meta($post->ID,'cp_want_to',true);
		$cp_swap_with = get_post_meta($post->ID,'cp_swap_with',true);
		$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
?>

        <div class="cp_auction_mine cp_widget">

        	<table width="100%">
            <tr>
            <td><?php if ( $cp_options->ad_images ) cp_ad_featured_thumbnail('ad-thumb'); ?></td>
            <td style="vertical-align:top;" width="92%">
            <div class="paddleft">
            <span class="prices_bc">
	    <h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= get_option('cp_auction_plugin_widget_title') ) echo mb_substr( get_the_title(), 0, get_option('cp_auction_plugin_widget_title') ).'...'; else the_title(); ?></a></h3>

	   <?php if (( $my_type == "wanted" || $cp_want_to ) && ( $cp_max_price )) { ?>
	   <strong><?php _e('Max. Price:', 'auctionPlugin'); ?></strong> <?php echo cp_display_price( $cp_max_price, 'ad', false ); ?>
	   <div class="clr"></div>
	   <?php } if( $cp_swap_with ) { ?>
	   <strong><?php _e('Swap With:', 'auctionPlugin'); ?></strong> <?php echo $cp_swap_with; ?>
	   <?php } ?>
	   <div class="clear5"></div>
	   <?php _e('Visit our profile for more<br />ads,', 'auctionPlugin'); ?> <a href="<?php echo cp_auction_url($cpurl['authors'], '?_authors_auctions=1', 'uid='.$post->post_author.''); ?>"><?php _e('click here!', 'auctionPlugin'); ?></a>

            </span>

            </div>
            </td>
            </tr>
            </table>
        </div>
<div class="clear10"></div>
<?php	
}


/*
|--------------------------------------------------------------------------
| DEACTIVATE / UNINSTALL CPAUCTION PLUGIN - WE ALSO USE UNINSTALL.PHP
|--------------------------------------------------------------------------
*/

function cp_auction_deactivate_actions() {

	update_option('cp_auction_installed', 'no');

}
register_deactivation_hook( __FILE__, 'cp_auction_deactivate_actions' );

function cp_auction_uninstall_actions() {

	require_once dirname( __FILE__ ) . '/admin/cp-uninstall.php';
}
if( get_option('cp_auction_plugin_uninstall') == "yes" )
register_uninstall_hook( __FILE__, 'cp_auction_uninstall_actions' );

?>