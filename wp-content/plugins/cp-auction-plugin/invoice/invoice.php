<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

	global $current_user, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;

	$id = ""; if(isset( $_GET['get_invoice'] )) $id = $_GET['get_invoice'];

	$table_transactions = $wpdb->prefix . "cp_auction_plugin_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_transactions} WHERE order_id = '$id'");
	if( $row ) {
	$author = get_userdata($row->post_author);
	$user = get_userdata($row->uid);
	$paid_date = date_i18n('d M. Y', $row->payment_date, true);
	$type = $row->trans_type;
	$shipping = $row->shipping;
	$mc_fee = $row->mc_fee;
	$mc_gross = $row->mc_gross;
	$amounts = cp_auction_calculate_end_fees($row->payment_option, $mc_gross, $row->post_author);
	}
	$values	= explode("|",$amounts);
	$fees = false;
	$cs = false;
	if( isset( $values[2] ) ) $fees = $values[2];
	if( isset( $values[3] ) ) $cs = $values[3];
	if( $type == "Cash in Hand" ) $paid_with = __('Cash in Hand', 'auctionPlugin');
	else if( $type == "Cash on Delivery" ) $paid_with = __('Cash on Delivery', 'auctionPlugin');
	else if( $type == "Bank Transfer" ) $paid_with = __('Bank Transfer', 'auctionPlugin');
	else if( $type == "Direct by PayPal" ) $paid_with = __('PayPal', 'auctionPlugin');
	else if( $type == "Direct by Credits" ) $paid_with = __('Credits', 'auctionPlugin');
	else if( $type == "Credits Escrow" ) $paid_with = __('Credits Escrow', 'auctionPlugin');
	else if( $type == "Banktransfer Escrow" ) $paid_with = __('Banktransfer Escrow', 'auctionPlugin');
	else if( $type == "PayPal Adaptive" ) $paid_with = __('PayPal', 'auctionPlugin');
	else $paid_with = __('Unspecified', 'auctionPlugin');

	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$items = $wpdb->get_results("SELECT * FROM {$table_purchases} WHERE order_id = '$id'");

	if( $uid != $row->uid && $uid != $row->post_author && ! current_user_can( 'manage_options' ) ) {
	echo __('Sorry, no invoice was found.','auctionAdmin'); ?>
	<script type="text/javascript">setTimeout( function() { window.close(); }, 3000);</script>
	<?php exit;
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>

<style type="text/css" media="print">
body {visibility:hidden;}
.print {visibility:visible;}
</style>

<script type="text/javascript">
function printpage(){
    window.print()
}
</script>

  </head>

  <body>

<?php if(function_exists('pf_show_link')) {
echo pf_show_link();
} else { ?>
<form>
<input type="button" value="<?php _e('Print Invoice','auctionPlugin'); ?>" onclick="printpage()">
</form>
<?php } ?>

<div class="print">
    <table width="100%" border="0" cellspacing="0" cellpadding="0"  bgcolor="#ffffff">
      <tbody>
        <tr>
          <td style="padding: 15px;"><center>
            <table width="700" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
              <tbody>
                <tr>
                  <td style="text-align: left; border: 0px #d9d9d9;">
                      <table id="header" style="line-height: 1.6; font-size: 12px; font-family: Helvetica, Arial, sans-serif; border: solid 1px #FFFFFF; width: 100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>
                          <tr>
                            <td style="color: #ffffff;" colspan="2" valign="bottom" height="30">.</td>
                          </tr>
                          <tr>
                            <td style="width: 65%; padding: 15px 0;" valign="baseline"><span style="font-weight:bold; font-size: 20px;"><?php if( $user->company_name ) { echo $user->company_name; ?><br /><?php } ?></span><span style="font-size: 18px;"><?php echo $user->first_name; ?> <?php echo $user->last_name; ?></span><br /><span style="font-size: 16px;"><?php if( $user->cp_street ) { echo $user->cp_street; ?><br /><?php } echo $user->cp_city; ?> <?php echo $user->cp_zipcode; ?>, <?php echo $user->cp_country; ?></span><br /><br /><br /><br /><span style="font-size: 16px;"><?php _e('Invoice number:','auctionPlugin'); ?> <?php echo $id; ?><br /><?php _e('Purchased Date:','auctionPlugin'); ?> <?php echo $paid_date; ?><br /><?php _e('Paid with:','auctionPlugin'); ?> <?php echo $paid_with; ?></span></td>
                            <td style="width: 35%; padding: 15px 0;" valign="baseline"><span style="font-weight:bold; font-size: 20px;"><?php if( $author->company_name ) { echo $author->company_name; ?><br /><?php } ?></span><span style="font-size: 18px;"><?php echo $author->first_name; ?> <?php echo $author->last_name; ?></span><br /><span style="font-size: 16px;"><?php if( $author->cp_street ) { echo $author->cp_street; ?><br /><?php } echo $author->cp_city; ?> <?php echo $author->cp_zipcode; ?>, <?php echo $author->cp_country; ?><br /><?php echo $author->user_email; ?></span></td>
                          </tr>
                        </tbody>
                      </table>
                      <table id="content" style="margin-top: 15px; line-height: 1.6; font-size: 16px; font-family: Arial, sans-serif;" width="700" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                        <tbody>

	<tr>	
	<td style="width: 100px; padding: 15px 0;"><strong><?php _e('Quantity','auctionPlugin'); ?></strong></td>
	<td style="padding: 15px 0;"><strong><?php _e('Product Name','auctionPlugin'); ?></strong></td>
	<td style="text-align: right; padding: 15px 0;"><strong><?php _e('Price','auctionPlugin'); ?></strong></td>
	<td style="text-align: right; padding: 15px 0;"><strong><?php _e('Discount','auctionPlugin'); ?></strong></td>
	<td style="text-align: right; padding: 15px 0;"><strong><?php _e('Sum','auctionPlugin'); ?></strong></td>
	</tr>
	<tr>
	<td colspan="5" valign="top"><hr style="border-top: solid 1px #d9d9d9;"></td>
	</tr>
<?php

	if ( $items ) {
	foreach( $items as $item ) {
	$pid = $item->pid;
	$author = get_userdata($item->uid);
	$user = get_userdata($item->buyer);
	$title = $item->item_name;
	$amount = $item->amount;
	$discount = $item->discount;
	$quantity = $item->numbers;
	$payment_date = date_i18n('d M. Y', $item->paiddate, true);
	$sum = ($amount * $quantity) - $discount;
	$sub = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(net_total) FROM {$table_purchases} WHERE order_id = %s", $id ) );
	$tax = get_user_meta( $item->uid, 'cp_auction_tax_vat', true );
	if(( empty( $tax )) || ( $tax == 0 )) $tax = 0;


	echo '<tr>';
	echo '<td>'.$quantity.'</td>';
	echo '<td>'.$title.'</td>';
	echo '<td align="right">'.cp_display_price( $amount, 'ad', false ).'</td>';
	echo '<td align="right">'.cp_display_price( $discount, 'ad', false ).'</td>';
	echo '<td align="right">'.cp_display_price( $sum, 'ad', false ).'</td>';
	echo '</tr>';

	}
}
?>

	<tr><td colspan="5" valign="top"><hr style="border-top: solid 1px #d9d9d9;"></td></tr>
	<tr><td colspan="4" valign="top" align="right"><strong><?php _e('Sub Total:','auctionPlugin'); ?></strong></td>
	<td align="right"><?php echo cp_display_price( $sub, 'ad', false ); ?></td></tr>
	<?php if( $type !== "Cash in Hand" ) { ?>
	<tr><td colspan="4" valign="top" align="right"><strong><?php _e('Shipping:','auctionPlugin'); ?></strong></td>
	<td align="right"><?php echo cp_display_price( $shipping, 'ad', false ); ?></td></tr>
	<?php } if( $type == "Cash on Delivery" ) { ?>
	<tr><td colspan="4" valign="top" align="right"><strong><?php echo __('COD Fees:', 'auctionPlugin'); ?></strong></td>
	<td align="right"><?php echo cp_display_price( $mc_fee, 'ad', false ); ?></td></tr>
	<?php } else if( $type !== "Cash in Hand" ) { ?>
	<tr><td colspan="4" valign="top" align="right"><strong><?php echo sprintf(__('Transaction Fees %s%s:', 'auctionPlugin'), $fees, $cs); ?></strong></td>
	<td align="right"><?php echo cp_display_price( $mc_fee, 'ad', false ); ?></td></tr>
	<?php } if( $tax > 0 ) { ?>
	<tr><td colspan="4" valign="top" align="right"><strong><?php echo sprintf(__('Hereof %s%s Tax / VAT:', 'auctionPlugin'), $tax, '%'); ?></strong></td>
	<td align="right"><?php echo cp_display_price( cp_auction_taxes( $mc_gross, $tax ), 'ad', false ); ?></td></tr>
	<?php } ?>

	<tr>
	<td colspan="2"></td><td colspan="3" valign="top"><hr style="border-top: solid 1px #d9d9d9;"></td>
	</tr>

	<tr><td colspan="4" valign="top" align="right"><strong><strong><?php _e('Sum Total:','auctionPlugin'); ?></strong></strong></td>
	<td align="right"><?php echo cp_display_price( $mc_gross, 'ad', false ); ?></td></tr>

	<tr>
	<td colspan="2"></td><td colspan="3" valign="top"><hr style="border-top: solid 1px #d9d9d9;"></td>
	</tr>
	</tbody>
	</table>

                  </td>
                </tr>
              </tbody>
            </table>
          </center></td>
        </tr>
      </tbody>
    </table>
   </div>
  </body>
</html>