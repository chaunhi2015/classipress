<?php
	global $wpdb;
	$ct = get_option('stylesheet');
	$maxposts = get_option('posts_per_page');
?>

<div class="content">

	<div class="content_botbg">

		<div class="content_res">

<?php if( $ct == "rondell_370" || $ct == "simply-responsive-cp" ) { ?>
	<script type='text/javascript' src='/wp-content/themes/classipress/includes/js/easing.js'></script> 
	<script src="/wp-content/themes/classipress/includes/js/jcarousellite.js" language="javascript" type="text/javascript"></script> 
<?php 
		if ( file_exists(STYLESHEETPATH . '/featured.php') ) include_once(STYLESHEETPATH . '/featured.php'); 
		else include_once(TEMPLATEPATH . '/featured.php'); 
} ?>

			<div id="breadcrumb">

				<?php if ( function_exists('cp_breadcrumb') ) cp_breadcrumb(); ?>

			</div>

	<?php
	// Figure out what we are displaying
	$sort = 'latest';
	$sorts = false;
	$author = false;

	if ( ! empty( $_GET['sort'] ) ) {
	$custs = $_GET['sort'];
	$cust = explode("|",$custs);
	if( isset( $cust[0] ) ) { $sorts = $cust[0]; } else { $sorts = false; }
	if( isset( $cust[1] ) ) { $author = $cust[1]; } else { $author = false; }
	}

	if ( ! empty( $sorts ) && in_array( $sorts, array( 'popular', 'random', 'featured', 'classifieds', 'wanted', 'auctions', 'tabone', 'tabtwo', 'tabthree', 'blog', 'closing', 'buynow', 'buyitnow', 'soldads' ) ) ) {
		$sort = $sorts;
	}

	$loop_type = 'ad_listing';

	if ( $sort == 'latest' ) {

		// show all ads but make sure the sticky featured ads don't show up first
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array(
				'relation' => 'OR',
					array(
					'key' => 'cp_ad_sold',
					'compare' => 'NOT EXISTS'
					),
					array(
					'key' => 'cp_ad_sold',
					'value' => array( 'no', '' ), 
					'compare' => 'IN'
					), 
				),
			), 
		) 
	);

	} else if ( $sort == 'random' ) {

		// show all random ads but make sure the sticky featured ads don't show up first
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 'post_type' => APP_POST_TYPE, 'ignore_sticky_posts' => 1, 'paged' => $paged, 'orderby' => 'rand' ) );

	} else if ( $sort == 'popular' ) {

		// show all most popular ads
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'meta_key' => 'cp_total_count',
		'orderby' => 'meta_value_num',
		'order' => 'DESC',
		'meta_query' => 
			array( 
				array( 
				'relation' => 'OR',
					array(
					'key' => 'cp_ad_sold',
					'compare' => 'NOT EXISTS'
					),
					array(
					'key' => 'cp_ad_sold',
					'value' => array( 'no', '' ), 
					'compare' => 'IN'
					), 
				),
			), 
		) 
	);

	} else if ( $sort == 'featured' ) {

		// show all featured ads
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'post__in' => get_option('sticky_posts'), 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array(
				'relation' => 'OR',
					array(
					'key' => 'cp_ad_sold',
					'compare' => 'NOT EXISTS'
					),
					array(
					'key' => 'cp_ad_sold',
					'value' => array( 'no', '' ), 
					'compare' => 'IN'
					), 
				),
			), 
		) 
	);

	} else if ( $sort == 'classifieds' ) {

		// setup the pagination and query
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array( 
				'key' => 'cp_auction_my_auction_type', 
				'value' => array( 'classified', '' ), 
				'compare' => 'IN'
				),
				array(
				'relation' => 'OR',
					array(
					'key' => 'cp_ad_sold',
					'compare' => 'NOT EXISTS'
					),
					array(
					'key' => 'cp_ad_sold',
					'value' => array( 'no', '' ), 
					'compare' => 'IN'
					), 
				),
			), 
		) 
	);

	} else if ( $sort == 'wanted' ) {

		// setup the pagination and query
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array( 
				'key' => 'cp_auction_my_auction_type', 
				'value' => 'wanted', 
				'compare' => 'IN'
				),
				array(
				'relation' => 'OR',
					array(
					'key' => 'cp_ad_sold',
					'compare' => 'NOT EXISTS'
					),
					array(
					'key' => 'cp_ad_sold',
					'value' => array( 'no', '' ), 
					'compare' => 'IN'
					), 
				),
			), 
		) 
	);

	} else if ( $sort == 'auctions' ) {

		// setup the pagination and query
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array( 
				'key' => 'cp_auction_my_auction_type', 
				'value' => array( 'normal', 'reverse' ), 
				'compare' => 'IN'
				),
				array(
				'relation' => 'OR',
					array(
					'key' => 'cp_ad_sold',
					'compare' => 'NOT EXISTS'
					),
					array(
					'key' => 'cp_ad_sold',
					'value' => array( 'no', '' ), 
					'compare' => 'IN'
					), 
				),
			), 
		) 
	);

	} else if ( $sort == 'tabone' ) {

		// show all ads but make sure the sticky featured ads don't show up first
		$cats_one = get_option('cp_auction_menu_tab_cats_one');
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'tax_query' => 
			array( 
				array( 
				'taxonomy' => 'ad_cat',
				'field' => 'term_id',
				'terms' => array($cats_one)
				) 
			) 
		) 
	);

	} else if ( $sort == 'tabtwo' ) {

		// show all ads but make sure the sticky featured ads don't show up first
		$cats_two = get_option('cp_auction_menu_tab_cats_two');
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'tax_query' => 
			array( 
				array( 
				'taxonomy' => 'ad_cat',
				'field' => 'term_id',
				'terms' => array($cats_two)
				) 
			) 
		) 
	);

	} else if ( $sort == 'tabthree' ) {

		// show all ads but make sure the sticky featured ads don't show up first
		$cats_three = get_option('cp_auction_menu_tab_cats_three');
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'tax_query' => 
			array( 
				array( 
				'taxonomy' => 'ad_cat',
				'field' => 'term_id',
				'terms' => array($cats_three)
				) 
			) 
		) 
	);

	} else if ( $sort == 'blog' ) {

		// show all posts
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => 'post', 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'paged' => $paged, 
		) 
	);

	} else if ( $sort == 'closing' ) {

		$hours = get_option('cp_auction_plugin_lastchance');
		$todayDate = date_i18n('m/d/Y H.i', strtotime("current_time('mysql')"), true); // current date
		$currentTime = strtotime($todayDate); //Change date into time
		$newtime = date_i18n('m/d/Y H.i', strtotime("$todayDate + $hours"), true);
		$date = strtotime($newtime);

		// setup the pagination and query
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array( 
				'key' => 'cp_auction_my_auction_type', 
				'value' => array( 'normal', 'reverse' ) 
				), 
				array( 
				'key' => 'cp_end_date', 
				'value' => $date,
				'compare' => '<'
				) 
			) 
		) 
	);

	} else if ( $sort == 'buynow' ) {

		// setup the pagination and query
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array( 
				'key' => 'cp_auction_my_auction_type', 
				'value' => 'normal', 'reverse' 
				), 
				array( 
				'key' => 'cp_buy_now', 
				'value' => 0, 
				'compare' => '>'
				) 
			) 
		) 
	);

	} else if ( $sort == 'buyitnow' ) {

		// setup the pagination and query
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array( 
				'key' => 'cp_auction_my_auction_type', 
				'value' => 'classified' 
				), 
				array( 
				'key' => 'cp_buy_now_enabled', 
				'compare' => 'EXISTS'
				) 
			) 
		) 
	);

	} else if ( $sort == 'soldads' ) {

		// setup the pagination and query
		$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
		query_posts( array( 
		'posts_per_page' => $maxposts, 
		'post_type' => APP_POST_TYPE, 
		'post_status' => 'publish', 
		'author__in' => $author, 
		'ignore_sticky_posts' => 1, 
		'paged' => $paged, 
		'meta_query' => 
			array( 
				array(
					'key' => 'cp_ad_sold',
					'value' => 'yes', 
					'compare' => 'IN'
					), 
				),
			) 
		);

	}


	?>
			<!-- left block -->
			<div class="content_left">

			<?php if ( $sort == 'blog' ) {
			get_template_part( 'loop' );

			wp_reset_query();
			}
			else {

if( $ct == "jibo" ) { ?>

	<div class="shadowblock_out">
		<div class="shadowblock">
		<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
		</div>
	</div>
</div>
	<div class="clr"></div>

<?php }

			if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
				include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
			} else {
				get_template_part( 'loop', $loop_type );
			}

			} ?>

			</div><!-- /content_left -->


			<?php get_sidebar(); ?>


			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->