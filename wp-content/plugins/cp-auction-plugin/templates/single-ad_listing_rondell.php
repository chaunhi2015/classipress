<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<div id="breadcrumb">

				<?php if ( function_exists('cp_breadcrumb') ) cp_breadcrumb(); ?>

			</div>

			<!-- <div style="width: 105px; height:16px; text-align: right; float: left; font-size:11px; margin-top:-10px; padding:0 10px 5px 5px;"> -->
				<?php // if($reported) : ?>
					<!-- <span id="reportedPost"><?php _e( 'Post Was Reported', APP_TD ); ?></span> -->
				<?php // else : ?>
					<!--	<a id="reportPost" href="?reportpost=<?php echo $post->ID; ?>"><?php _e( 'Report This Post', APP_TD ); ?></a> -->
				<?php // endif; ?>
			<!-- </div> -->

			<div class="clr"></div>

			<div class="content_left">

		<!-- Start new search bar -->

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php _e( 'Search Classified Ads', APP_TD ); ?></h2>

								<div class="pad2 dotted"></div>

									<center>

									<?php get_template_part( 'includes/theme', 'searchbar' ); ?>

									</center>

								<div class="clr"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

		<!-- End new search bar -->

				<?php do_action( 'appthemes_notices' ); ?>

				<?php appthemes_before_loop(); ?>

				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php appthemes_before_post(); ?>

						<?php appthemes_stats_update( $post->ID ); //records the page hit ?>

						<div class="shadowblock_out <?php cp_display_style( 'featured' ); ?>">

							<div class="shadowblock">

								<?php appthemes_before_post_title(); ?>

								<h1 class="single-listing"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>

								<div class="clr"></div>

								<?php appthemes_after_post_title(); ?>



								<div class="bigright <?php cp_display_style( 'ad_single_images' ); ?>">

									<ul>

									<?php
										// grab the category id for the functions below
										$cat_id = appthemes_get_custom_taxonomy( $post->ID, APP_TAX_CAT, 'term_id' );

										if ( get_post_meta($post->ID, 'cp_ad_sold', true) == 'yes' ) {
									?>
											<li id="cp_sold"><span><h3><?php _e( 'This item has been sold', APP_TD ); ?>!</h3></span></li><br />
									<?php
										}

										// 3.0+ display the custom fields instead (but not text areas)
										cp_get_ad_details( $post->ID, $cat_id );
									?>

										<li id="cp_listed"><span><?php _e( 'Listed:', APP_TD ); ?></span> <?php echo appthemes_display_date( $post->post_date ); ?></li>
									<?php if ( $expire_date = get_post_meta( $post->ID, 'cp_sys_expire_date', true ) ) { ?>
										<li id="cp_expires"><span><?php _e( 'Expires:', APP_TD ); ?></span> <?php echo cp_timeleft( $expire_date ); ?></li>
									<?php } ?>

									</ul>
							<br/>
			   					<div class="emailico"></div><a href="mailto:?subject=<?php the_title();?>&amp;body=<?php the_permalink() ?>" title="<?php _e( 'Click here to open your email-client and send this ad description to a friend.', APP_WPM_TD ); ?>"><div style="text-align:left; color:#077502"><u><b><?php _e( 'Email this advert to a friend', APP_WPM_TD ); ?></b></u></div></a>

								</div><!-- /bigright -->

								<?php if ( $cp_options->ad_images ) { ?>

									<div class="bigleft">

										<div id="main-pic">

											<?php cp_get_image_url(); ?>

											<div class="clr"></div>

										</div>

										<div id="thumbs-pic">

											<?php cp_get_image_url_single( $post->ID, 'thumbnail', $post->post_title, -1 ); ?>

											<div class="clr"></div>

										</div>

									</div><!-- /bigleft -->

								<?php } ?>

								<div class="clr"></div>

								<?php appthemes_before_post_content(); ?>

								<div class="single-main">

									<?php
										// 3.0+ display text areas in content area before content.
										cp_get_ad_details( $post->ID, $cat_id, 'content' );
									?>

									<h2 class="description-area"><?php _e( 'Description', APP_TD ); ?></h2>

									<?php the_content(); ?>

								</div>

						<?php if ( function_exists('cp_auction_buy_now_button') ) cp_auction_buy_now_button(); ?> 

						<br />
								<H2><?php _e( 'Ad Location', APP_WPM_TD ); ?></H2>

									<?php include_once ( TEMPLATEPATH . '/includes/sidebar-gmap.php' ); ?>
						<br />

					<?php
          
				        $custom_taxterms = wp_get_object_terms( $post->ID,
			                    'ad_cat', array('fields' => 'ids') );

				        $args = array(
				        'post_type' => 'ad_listing',
				        'post_status' => 'publish',
				        'posts_per_page' => 3, // how many related ads - you can edit this number
				        'orderby' => 'date',
					 'order' => 'DESC',
				        'tax_query' => array(
 				           array(
			            		'taxonomy' => 'ad_cat',
			            		'field' => 'id',
                				'terms' => $custom_taxterms
					            )
					        ),
				        'post__not_in' => array ($post->ID),
					        );
				        $related_items = new WP_Query( $args );
			        // loop over query
 			       if ($related_items->have_posts()) :
				?>
 			       <h2 class="description-area"><?php _e( 'Related Ads', APP_WPM_TD ); ?></h2>
				<?php
        while ( $related_items->have_posts() ) : $related_items->the_post();
        ?>
<ul>
		<li>
                <div class="post-left">

                </div>
        
                <div class="<?php cp_display_style( array( 'ad_images', 'ad_class' ) ); ?>">
                
        
                    <h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= 75 ) echo mb_substr( get_the_title(), 0, 75 ).'...'; else the_title(); ?></a></h3>
                    
                    <div class="clr"></div>
                    
                    <div class="clr"></div>
        
                    <p class="post-desc"><?php echo cp_get_content_preview( 160 ); ?></p>   
                    
                    	<div class="clr"></div>
        
                  </div>
		            <div class="pad5"></div>
                	
			<div class="clr"></div>

		</li>
        <?php
        endwhile;
        echo '</ul>';
        endif;
        wp_reset_postdata();
        ?>
								<?php appthemes_after_post_content(); ?>

							</div><!-- /shadowblock -->

						</div><!-- /shadowblock_out -->

						<?php appthemes_after_post(); ?>

					<?php endwhile; ?>

					<?php appthemes_after_endwhile(); ?>

				<?php else: ?>

					<?php appthemes_loop_else(); ?>

				<?php endif; ?>

				<div class="clr"></div>

				<?php appthemes_after_loop(); ?>

				<?php wp_reset_query(); ?>

				<div class="clr"></div>

				<?php comments_template( '/comments-ad_listing.php' ); ?>

			</div><!-- /content_left -->

			<?php if ( get_option('cp_auction_enable_custom_sidebar') == "yes" ) {
				include_once( CP_AUCTION_TPL_PATH .'/sidebars/custom-sidebar.php' );
			} else if ( get_option('cp_auction_enable_bid_widget') == "yes" || get_option('aws_bo_use_widget') == "yes" || get_option('cp_auction_enable_wanted_widget') == "yes" ) {
				include_once( CP_AUCTION_TPL_PATH .'/sidebars/sidebar-ad_rondell.php' );
			} else {
				get_sidebar( 'ad' );
			} ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
