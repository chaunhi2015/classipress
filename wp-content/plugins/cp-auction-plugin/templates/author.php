<?php
/**
 * Author profile page
 *
 */

global $curauth, $cp_options, $post, $wpdb;

/*
|--------------------------------------------------------------------------
| ORDER OF AUTHORPAGE TABBED MENU
|--------------------------------------------------------------------------
*/

function cp_auction_authorpage_tab_order() {
	global $wpdb;

	$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";
	$tabs = $wpdb->get_results("SELECT DISTINCT * FROM {$table_name} ORDER BY sequence ASC");

	foreach ( $tabs as $tab ) {

	if ( $tab->position == "author" || $tab->position == "both" )
	echo '<li><a href="#'.$tab->block.'"><span class="big">'.$tab->title.'</span></a></li>';

	}
}


	//This sets the $curauth variable
	$curauth = get_queried_object();

	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$author_posts_count = count_user_posts( $curauth->ID );
	$ct = get_option('stylesheet');
	$cpa = get_option('cp_auction_plugin_auctiontype');
	$maxposts = get_option('posts_per_page');
	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	if ( $ct == "classiestate" ) { $css = "pagings"; } else { $css = "paging"; }

	$cp_company = get_user_meta( $curauth->ID, 'company_name', true );
	if ( ! empty( $cp_company ) && ! empty( $curauth->user_url ) )
	$cp_company_name = '<a href="'.esc_url( $curauth->user_url ).'" target="_blank">'.$cp_company.'</a>';
	$cp_phone = get_user_meta( $curauth->ID, 'cp_phone', true );
	$classified_auction = get_option('cp_auction_enable_classified_auction');
	$only_admins = get_option('cp_auction_only_admins_post_auctions');
	$confirm = ''.__('You agree that you have read our policies!','auctionPlugin').'';
	$display_name = ''.$curauth->display_name.'\'s';
	$position = array( 'author', 'both' );
	$table_name = $wpdb->prefix . "cp_auction_plugin_tabs";
	$active = 0;

	$msg = "";
	$info = "";

			//----contact seller		

			if(isset($_POST['submit_contact']))
			{
			$info = 1;
			$your_name	= trim($_POST['your_name']);
			$your_email	= trim($_POST['your_email']);
			$your_subject	= trim($_POST['your_subject']);
			$your_message	= nl2br(trim(strip_tags($_POST['your_message'])));

			if($your_name == "") $info = 2;
			if(!cp_auction_check_email_address($your_email)) $info = 2;
			if($your_subject == "") $info = 2;
			if($your_message == "") $info = 2;

			// get the submitted math answer
    			$rand_post_total = (int)$_POST['rand_total'];

    			// compare the submitted answer to the real answer
    			$rand_total = (int)$_POST['rand_num'] + (int)$_POST['rand_num2'];

    			// if it's a match then send the email
    			if ($rand_total != $rand_post_total) $info = 3;
    			if( !cp_auction_check_email_address( $your_email ) ) $info = 4;

			if($info == 2) $msg = "".__('Please enter all fields correctly!','auctionPlugin')."";
			if($info == 3) $msg = "".__('Incorrect captcha answer!','auctionPlugin')."";
			if($info == 4) $msg = "".__('Please enter an valid email address!','auctionPlugin')."";

			if($info == 1) {

			cp_auction_contact_user_email( $your_message, $your_subject, $your_name, $your_email, $curauth->ID );

			$info = 5;
			$msg = "".__('Your message was sent!','auctionPlugin')."";

			}
			}

?>

<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<div id="breadcrumb">
				<?php if ( function_exists('cp_breadcrumb') ) cp_breadcrumb(); ?>
			</div>

			<!-- left block -->
			<div class="content_left">

				<div class="shadowblock_out" id="noise">

					<div class="shadowblock">

		<?php if( ( ! empty($cp_company) ) && ( $cpa == "classified" || $cpa == "wanted" || $cpa == "mixed" ) ) { ?>
		<h2 class="dotted"><?php echo $cp_company; ?></h2>
		<?php } else { ?>
		<h2 class="dotted"><?php echo $display_name; ?> <?php _e('listings', 'auctionPlugin'); ?></h2>
		<?php } ?>

		<?php if( get_user_meta( $curauth->ID, 'cp_auction_info_top', true ) ) {

		echo '<div class="page_info">'.nl2br( get_user_meta( $curauth->ID, 'cp_auction_info_top', true ) ).'</div>';
		echo '<div class="clear15"></div>';

		} ?>

		<?php if( $ct != "classiclean" && !headline_child_themes( get_option('stylesheet') ) ) { ?>
		<div class="post" id="noise">
		<?php } ?>

			<div id="user-photo"><?php appthemes_get_profile_pic($curauth->ID, $curauth->user_email, 96); ?></div>

					<div class="author-main">

								<ul class="author-info">
									<?php if ( !empty($cp_company_name) ) { ?><li><strong><?php _e( 'Company:', 'auctionPlugin' ); ?></strong> <?php echo $cp_company_name; ?></li><?php } ?>
									<li><strong><?php _e( 'Member Since:', APP_TD ); ?></strong> <?php echo appthemes_display_date( $curauth->user_registered, 'date', true ); ?></li>
									<?php if ( !empty($cp_phone) ) { ?><li><strong><?php _e( 'Phone:', 'auctionPlugin' ); ?></strong> <?php echo $cp_phone; ?></li><?php } ?>
									<?php if ( ! empty( $curauth->user_url ) ) { ?><li><div class="dashicons-before globeico"></div><a href="<?php echo esc_url( $curauth->user_url ); ?>"><?php echo strip_tags( $curauth->user_url ); ?></a></li><?php } ?>
									<?php if ( ! empty( $curauth->twitter_id ) ) { ?><li><div class="dashicons-before twitterico"></div><a href="http://twitter.com/<?php echo urlencode( $curauth->twitter_id ); ?>" target="_blank"><?php _e( 'Twitter', APP_TD ); ?></a></li><?php } ?>
									<?php if ( ! empty( $curauth->facebook_id ) ) { ?><li><div class="dashicons-before facebookico"></div><a href="<?php echo appthemes_make_fb_profile_url( $curauth->facebook_id ); ?>" target="_blank"><?php _e( 'Facebook', APP_TD ); ?></a></li><?php } ?>
								</ul>

								<?php cp_author_info( 'page' ); ?>

							</div>

							<div class="clr"></div>

							<div class="author-desc">
								<h3><?php _e( 'Description', APP_TD ); ?></h3>
								<p><?php echo nl2br( $curauth->user_description ); ?></p>
							</div>

						<div class="pad20"></div>
		 				<?php if(function_exists("cp_rate_users")) {  cp_rate_users($curauth->ID, false); } ?>

						<?php if( $ct != "classiclean" && !headline_child_themes( get_option('stylesheet') ) ) { ?>
						</div><!--/post-->
						<?php } ?>

					</div><!-- /shadowblock -->

				</div><!-- /shadowblock_out -->

<?php if( $ct == "jibo" || get_option( 'cp_auction_slide_in_ads' ) == "yes" ) { ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({
    activate: function (e, ui) {
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' });
    },
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
    active: $.cookie('homeTab')
});
});
</script>
<?php } else { ?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) { 
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({
    activate: function (e, ui) {
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' });
    },
    active: $.cookie('homeTab')
});
});
</script>

<?php } if( $ct == "eclassify" ) {
	$tabs = "tabnavig"; ?>

	<div class="tabcontrol">

		<ul class="tabnavig">
<?php } else {
	$tabs = "tabmenu"; ?>

	<div class="tabhome">

		<ul class="tabmenu">
<?php } ?>

		<?php cp_auction_authorpage_tab_order(); ?>

	</ul>

<script type="text/javascript">
var loc = window.location.href,
    index = loc.indexOf('#');
if ( index > 0 ) {
    history.pushState("", document.title, window.location.pathname);
}
</script>

<script type="text/javascript">
jQuery('ul.<?php echo $tabs; ?> a').click(function() {
var URL = window.location.href;
var NewURL = URL.split('page')[0];
window.history.pushState('', '', NewURL);
if ( NewURL != URL ) {
window.location = window.location.href;
}
});
</script>

<?php if( in_array( get_option( 'cp_auction_contact_tab_where' ), $position ) ) { ?>

    <div id="contact">

	<div class="clr"></div>

	<div class="tabunder"><span class="big"><strong><span class="colour"><?php _e('Contact User', 'auctionPlugin'); ?></span></strong></span></div>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="aws-box">

<?php if ( is_user_logged_in() ) { ?>	
	    <?php if($info == 5) { ?>
            <div class="notice success"><?php echo "".$msg.""; ?></div>
            <?php } ?>

            <?php if(($info == 2) || ($info == 3) || ($info == 4)) { ?>
            <div class="notice error"><?php echo "".$msg.""; ?></div>
            <?php } ?>

	<?php _e('You may contact this user by entering the requested information in the form below.', 'auctionPlugin'); ?>

	<div class="clear15"></div>

	<form class="cp-ecommerce" action="" method="post">

<p>
	<label for="your_name"><?php _e('Your Name:', 'auctionPlugin'); ?></label>
	<input tabindex="20" id="your_name" name="your_name" value="" type="text">
</p>

<p>
	<label for="your_email"><?php _e('Your Email:', 'auctionPlugin'); ?></label>
	<input tabindex="21" id="your_email" name="your_email" value="" type="text">
</p>

<p>
	<label for="subject"><?php _e('Subject:', 'auctionPlugin'); ?></label>
	<input tabindex="22" id="subject" name="your_subject" value="" type="text">
</p>

<p>
	<label for="contact_message"><?php _e('Description:', 'auctionPlugin'); ?></label>
	<textarea tabindex="23" id="contact_message" name="your_message"></textarea>
</p>

<?php
	// create a random set of numbers for spam prevention
	$randomNum = '';
	$randomNum2 = '';
	$randomNumTotal = '';

	$rand_num = rand(0,9);
	$rand_num2 = rand(0,9);
	$randomNumTotal = $randomNum + $randomNum2;
?>

<p>
	<label for="rand_total"><?php _e('Sum of', 'auctionPlugin'); ?> <?php echo $rand_num; ?> + <?php echo $rand_num2; ?> =</label>
	<input tabindex="24" id="rand_total" name="rand_total" minlength="1" value="" type="text" />
</p>

	<input type="hidden" name="rand_num" value="<?php echo $rand_num; ?>" />
	<input type="hidden" name="rand_num2" value="<?php echo $rand_num2; ?>" />

<p class="submit">
	<input tabindex="25" type="submit" name="submit_contact" onclick="return confirm('<?php echo $confirm; ?>')" value="<?php _e('Submit', 'auctionPlugin'); ?>" class="btn_orange" />
</p>

	</form>

      <?php } else { ?>
        <div class="padd10"><?php _e('Sorry, but you must be logged in before you can contact this user.', 'auctionPlugin'); ?></div>
        <?php } ?>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

</div><!-- /contact - tab_content -->

<?php } ?>

<?php
						remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
						$post_type_url = esc_url( add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) ) );
					?>

					<?php if( $ct == "jibo" && in_array( get_option( 'cp_auction_jibo_tab_where' ), $position ) ) { ?>

					<!-- tab 0 -->
					<div id="block0">
		
						<div class="clr"></div>

						<div class="tabunder"><span class="big"><?php _e( 'Welcome', 'auctionPlugin' ); ?> / <strong><span class="colour"><?php _e( 'Ad posting rules', 'auctionPlugin' ); ?></span></strong></span></div>

					<div class="shadowblock_out">

						<div class="shadowblock">

							<h2 class="dotted"><?php _e( 'Terms and conditions', 'auctionPlugin' ); ?></h2>

							<?php echo get_option('jibo_welcome_tab') ?>

								<div class="clr"></div>

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

					</div><!-- /block0 -->

					<?php } if( in_array( get_option( 'cp_auction_listed_tab_where' ), $position ) ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block1'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 1 -->
					<div id="block1">

						<div class="clr"></div>

						<?php if( ( $ct != "simply-responsive-cp" ) || ( $ct == "simply-responsive-cp" && get_option( 'aws_gridview_installed' ) == "yes" ) ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_listed_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// show all ads but make sure the sticky featured ads don't show up first
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'author__in' => $curauth->ID, 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$latest_url = esc_url( add_query_arg( array( 'sort' => 'latest|'.$curauth->ID.'' ), $post_type_url ) );
						?>
								<div class="<?php echo $css; ?>"><a href="<?php echo $latest_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block1 -->

					<?php } if( in_array( get_option( 'cp_auction_featured_tab_where' ), $position ) ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block2'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 2 -->
            				<div id="block2">

              				<div class="clr"></div>

					<?php if( $ct != "simply-responsive-cp" ) { ?>
					<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_featured_tab'); ?> / <strong><span class="colour"><?php _e( 'Featured Listings', 'auctionPlugin' ); ?></span></strong></span></div>
					<?php } ?>

                				<?php
                  					// show all featured ads
                					$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'author__in' => $curauth->ID, 
							'post__in' => get_option('sticky_posts'), 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

                				<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
									$featured_url = esc_url( add_query_arg( array( 'sort' => 'featured|'.$curauth->ID.'' ), $post_type_url ) );
						?>
									<div class="<?php echo $css; ?>"><a href="<?php echo $featured_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

            				</div><!-- /block2 -->

					<?php } if( in_array( get_option( 'cp_auction_popular_tab_where' ), $position ) ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block3'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 3 -->
					<div id="block3">

						<div class="clr"></div>

						<?php if( ( $ct != "simply-responsive-cp" ) || ( $ct == "simply-responsive-cp" && get_option( 'aws_gridview_installed' ) == "yes" ) ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_popular_tab'); ?> / <strong><span class="colour"><?php _e( 'Most Popular', 'auctionPlugin' ); ?></span></strong></span><?php if( $ct != "jibo" && get_option( 'aws_gridview_installed' ) == "yes" ) { ?><div class="select-view">
	
		<a href="javascript: void(0);" id="grid-views" class="grid-views" title="<?php _e( 'Gridview', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="list-views" class="list-views" title="<?php _e( 'Listview', 'auctionPlugin' ); ?>"></a>

	</div><?php } ?></div>
						<?php } ?>

						<?php
							// show all most popular ads
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'author__in' => $curauth->ID, 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_key' => 'cp_total_count',
							'orderby' => 'meta_value_num',
							'order' => 'DESC',
							'meta_query' => 
								array( 
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$popular_url = esc_url( add_query_arg( array( 'sort' => 'popular|'.$curauth->ID.'' ), $post_type_url ) );
						?>
								<div class="<?php echo $css; ?>"><a href="<?php echo $popular_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block3 -->

					<?php } if( in_array( get_option( 'cp_auction_classified_tab_where' ), $position ) ) {
					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block4'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 4 -->
					<div id="block4">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_classified_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span></div>
						<?php } ?>

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'author__in' => $curauth->ID, 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array( 
									'key' => 'cp_auction_my_auction_type', 
									'value' => array( 'classified', '' ), 
									'compare' => 'IN'
									),
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$classifieds_url = esc_url( add_query_arg( array( 'sort' => 'classifieds|'.$curauth->ID.'' ), $post_type_url ) );
						?>
								<div class="<?php echo $css; ?>"><a href="<?php echo $classifieds_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block4 -->

					<?php } if( in_array( get_option( 'cp_auction_auction_tab_where' ), $position ) ) {

					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block6'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 6 -->
					<div id="block6">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_auction_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span></div>
						<?php } ?>

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'author__in' => $curauth->ID, 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array( 
									'key' => 'cp_auction_my_auction_type', 
									'value' => array( 'normal', 'reverse' ), 
									'compare' => 'IN'
									),
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);
						
							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$auction_url = esc_url( add_query_arg( array( 'sort' => 'auctions|'.$curauth->ID.'' ), $post_type_url ) );
						?>
								<div class="<?php echo $css; ?>"><a href="<?php echo $auction_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block6 -->

					<?php } if( in_array( get_option( 'cp_auction_wanted_tab_where' ), $position ) ) {

					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block7'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 7 -->
					<div id="block7">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_wanted_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span></div>
						<?php } ?>

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'author__in' => $curauth->ID, 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array( 
									'key' => 'cp_auction_my_auction_type', 
									'value' => 'wanted', 
									'compare' => 'IN'
									),
									array(
									'relation' => 'OR',
										array(
										'key' => 'cp_ad_sold',
										'compare' => 'NOT EXISTS'
										),
										array(
										'key' => 'cp_ad_sold',
										'value' => array( 'no', '' ), 
										'compare' => 'IN'
										), 
									),
								), 
							) 
						);
						
							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$wanted_url = esc_url( add_query_arg( array( 'sort' => 'wanted|'.$curauth->ID.'' ), $post_type_url ) );
						?>
								<div class="<?php echo $css; ?>"><a href="<?php echo $wanted_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block7 -->

				<?php } if( in_array( get_option( 'cp_auction_categories_tab_where' ), $position ) ) { ?>

					<!-- tab 11 -->
            				<div id="block11">

              				<div class="clr"></div>

					<?php if( $ct != "simply-responsive-cp" ) { ?>
              				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_categories_tab'); ?> / <strong><span class="colour"><?php _e( 'Ad Categories', 'auctionPlugin' ); ?></span></strong></span></div>
					<?php } ?>

				<?php if( $ct == "simply-responsive-cp" ) { ?>
				<div class="shadowblock_out_cat">
				<?php } ?>

              				<div class="shadowblock_out">
 
                    					<div class="shadowblock">

								<h2 class="dotted"><?php _e( 'Categories', 'auctionPlugin' ); ?></h2>

							<?php if( get_option('cp_auction_toggle_cats') == "yes" ) { ?>
							<div id="directories" class="directory <?php cp_display_style( 'dir_cols' ); ?>">
							<?php } else { ?>
							<div id="directory" class="directory <?php cp_display_style( 'dir_cols' ); ?>">
							<?php } ?>

								<?php echo cp_create_categories_list( 'dir' ); ?>

								<div class="clr"></div>

							</div><!--/directory(ies) -->

						</div><!-- /shadowblock -->

						</div><!-- /shadowblock_out -->

				<?php if( $ct == "simply-responsive-cp" ) { ?>
				</div><!-- /shadowblock_out_cat -->
				<?php } ?>

            				</div><!-- /block11 -->

				<?php } if( in_array( get_option( 'cp_auction_blog_tab_where' ), $position ) ) { ?>

					<!-- tab 12 -->
					<div id="block12">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_blog_tab'); ?> / <strong><span class="colour"><?php _e( 'Latest News', 'auctionPlugin' ); ?></span></strong></span></div>
						<?php } ?>

						<div class="clr"></div>

						<?php
							// show all posts
							remove_action('appthemes_after_blog_endwhile', 'cp_do_pagination' );
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => 'post', 
							'post_status' => 'publish', 
							'author__in' => $curauth->ID, 
							'paged' => $paged 
							) 
						);

							$total_pages = max( 1, absint( $wp_query->max_num_pages ) );
						?>

						<?php get_template_part( 'loop' ); ?>

						<?php wp_reset_query(); ?>

						<?php
							if ( $total_pages > 1 ) {
								$blog_url = esc_url( add_query_arg( array( 'sort' => 'blog|'.$curauth->ID.'' ), $post_type_url ) );
						?>
								<div class="<?php echo $css; ?>"><a href="<?php echo $blog_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Posts', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts('post');
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'posts online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

						<div class="clear20"></div>

				</div><!-- /block12 -->

					<?php } if( in_array( get_option( 'cp_auction_category_tab_where' ), $position ) ) { ?>

					<!-- tab 14 -->
					<div id="block14">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_category_tab'); ?> / <strong><span class="colour"><?php _e('View ads by category', 'auctionPlugin'); ?></span></strong></span></div>
				<?php } ?>

				<?php cp_auction_author_list_by_category($curauth->ID); ?>

					</div><!-- /block14 -->

					<?php } if( in_array( get_option( 'cp_auction_buynow_tab_where' ), $position ) ) { ?>

					<!-- tab 15 -->
					<div id="block15">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_buynow_tab'); ?> / <strong><span class="colour"><?php _e('Just Listed', 'auctionPlugin'); ?></span></strong></span></div>
				<?php } ?>

				<?php cp_auction_author_buy_now_auctions($curauth->ID); ?>

					</div><!-- /block15 -->

					<?php } if( in_array( get_option( 'cp_auction_closing_tab_where' ), $position ) ) { ?>

					<!-- tab 16 -->
					<div id="block16">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_closing_tab'); ?> / <strong><span class="colour"><?php _e('Soon to close auctions', 'auctionPlugin'); ?></span></strong></span></div>
				<?php } ?>

				<?php cp_auction_author_soon_to_close($curauth->ID); ?>

					</div><!-- /block16 -->

					<?php } if( in_array( get_option( 'cp_auction_intention_tab_where' ), $position ) ) { ?>

					<!-- tab 17 -->
					<div id="block17">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_intention_tab'); ?> / <strong><span class="colour"><?php _e('View ads by ad type', 'auctionPlugin'); ?></span></strong></span></div>
				<?php } ?>

				<?php cp_auction_author_list_by_intention($curauth->ID); ?>

					</div><!-- /block17 -->

					<?php } if( in_array( get_option( 'cp_auction_buynow_classifieds_tab_where' ), $position ) ) { ?>

					<!-- tab 18 -->
					<div id="block18">

				<div class="clr"></div>

				<?php if( $ct != "simply-responsive-cp" ) { ?>
				<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_buynow_classifieds_tab'); ?> / <strong><span class="colour"><?php _e('Buy it Now Ads', 'auctionPlugin'); ?></span></strong></span></div>
				<?php } ?>

				<?php cp_auction_author_buy_now_classifieds($curauth->ID); ?>

					</div><!-- /block18 -->

					<?php } if( in_array( get_option( 'cp_auction_sold_ads_tab_where' ), $position ) ) {

					$odr = $wpdb->get_row("SELECT * FROM {$table_name} WHERE block = 'block19'");
					if( ( $odr ) && ( $odr->sequence == 1 || $odr->sequence == 2 ) ) $active = $active + 1;
					else $active = 0; ?>

					<!-- tab 19 -->
					<div id="block19">

						<div class="clr"></div>

						<?php if( $ct != "simply-responsive-cp" ) { ?>
						<div class="tabunder"><span class="big"><?php echo get_option('cp_auction_sold_ads_tab'); ?> / <strong><span class="colour"><?php _e( 'Just Listed', 'auctionPlugin' ); ?></span></strong></span></div>
						<?php } ?>

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							query_posts( array( 
							'posts_per_page' => $maxposts, 
							'post_type' => APP_POST_TYPE, 
							'post_status' => 'publish', 
							'author__in' => $curauth->ID, 
							'ignore_sticky_posts' => 1, 
							'paged' => $paged, 
							'meta_query' => 
								array( 
									array(
										'key' => 'cp_ad_sold',
										'value' => 'yes', 
										'compare' => 'IN'
										), 
									),
								) 
							);

							// build the row counter depending on what page we're on
							$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

<?php if( $ct == "jibo" && $active == 1 ) { ?>
	<div class="shadowblock_out">
<div class="shadowblock">
<div class="select-view">
		<div class="view-type"><?php _e( 'View type:', 'auctionPlugin' ); ?></div>
		<a href="javascript: void(0);" id="gridview" class="gridview" title="<?php _e( 'Grid View', 'auctionPlugin' ); ?>"></a>
		<a href="javascript: void(0);" id="listview" class="listview" title="<?php _e( 'List View', 'auctionPlugin' ); ?>"></a>
	</div>
	</div>
	</div>
	<div class="clr"></div>
<?php } ?>

						<?php if ( get_option( 'aws_gridview_installed' ) == "yes" ) {
							include( AWS_GV_PLUGIN_DIR .'/includes/loop-ad_listing.php' );
						} else {
							get_template_part( 'loop', 'ad_listing' );
						}

							if ( $total_pages > 1 ) {
							$sold_ads_url = esc_url( add_query_arg( array( 'sort' => 'soldads|'.$curauth->ID.'' ), $post_type_url ) );
						?>
								<div class="<?php echo $css; ?>"><a href="<?php echo $sold_ads_url; ?>"><?php if ( $ct == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php } ?>

					</div><!-- /block19 -->

			<?php } ?>

	</div><!-- /tab_control -->

			</div><!-- /content_left -->

			<?php get_sidebar( 'author' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->

<?php

/*
|--------------------------------------------------------------------------
| SEARCH / LIST ADS BY CATEGORY
|--------------------------------------------------------------------------
*/

function cp_auction_author_list_by_category($uid) {
	global $wp_query, $post, $cp_options;

	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$cpa = get_option('cp_auction_plugin_auctiontype');

?>
	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

<?php
	if( $cpa == "classified" || $cpa == "wanted" || $cpa == "mixed" || $cpa == "reverse" ) {
	$view_listings = ''.__('View Listings', 'auctionPlugin').'';
	} else {
	$view_listings = ''.__('View Auctions', 'auctionPlugin').'';
	} ?>

	<div class="clear5"></div>

	<form class="cp-ecommerce" action="" method="post" id="mainform">

        <?php if($cpa == "mixed") { ?>
<p>
	<label for="cp_auction_my_auction_type"><?php _e('Select the type of Ad:', 'auctionPlugin'); ?></label>
	<?php $auction_type = ""; if(isset($_POST['cp_auction_my_auction_type'])) $auction_type = $_POST['cp_auction_my_auction_type']; ?>
	<select class="<?php echo $new_class; ?>" name="cp_auction_my_auction_type" id="cp_auction_my_auction_type" tabindex="1"><?php echo cp_auction_select_auction_type($auction_type); ?></select>
</p>
	<?php } ?>
<p>
	<label for="category_parent"><?php _e('Select Category:', 'auctionPlugin'); ?></label>
	<?php $category_parent = ""; if(isset($_POST['category_parent'])) $category_parent = $_POST['category_parent']; ?>
	<?php echo cp_auction_get_cat_dropdown($category_parent, 7); ?>
</p>

	<div class="clear10"></div>

<p class="submit">
<input type="submit" name="view" value="<?php echo $view_listings; ?>" class="btn_orange" />
</p>

        </form>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

                <?php if(isset($_POST['view'])) {

		$cp_auction_category 	= isset( $_POST['category_parent'] ) ? esc_attr( $_POST['category_parent'] ) : '';
		$my_type	 	= isset( $_POST['cp_auction_my_auction_type'] ) ? esc_attr( $_POST['cp_auction_my_auction_type'] ) : '';
		if( empty( $my_type ) ) $my_type = get_option('cp_auction_plugin_auctiontype');

		global $wpdb;

		$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;

		$querystr = "
		SELECT * FROM $wpdb->posts
		LEFT JOIN $wpdb->postmeta m1 ON($wpdb->posts.ID = m1.post_id)
		LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
		LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
		WHERE $wpdb->term_taxonomy.term_id = '$cp_auction_category'
		AND $wpdb->term_taxonomy.taxonomy = '".APP_TAX_CAT."'
		AND (m1.meta_key = 'cp_auction_my_auction_type' AND m1.meta_value = '$my_type')
		AND $wpdb->posts.post_status = 'publish'
		AND $wpdb->posts.post_author = '$uid'
		AND $wpdb->posts.post_type = '".APP_POST_TYPE."'
		ORDER BY $wpdb->posts.post_date DESC";

		$rows = $wpdb->get_results($querystr, OBJECT);

		if( $wpdb->num_rows != 0 ) {

   		global $post;
   		foreach ($rows as $post) { 
     		setup_postdata($post);

		$cp_ad_sold = end_auction($post->ID);
		if( $cp_ad_sold != "yes" ) { ?>
		<div class="sellmyapps">
		<?php echo cp_auction_loop_ad_listing($post); ?>
		</div>
		<?php } ?>

		<?php } if(function_exists('appthemes_pagination')) appthemes_pagination(); ?>

	<?php } else { ?>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'Sorry, there are currently no ads listed.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

            </div><!-- /shadowblock_out -->
          
        </div><!-- /shadowblock -->  

	<?php }
	}
}


/*
|--------------------------------------------------------------------------
| SEARCH / LIST ADS BY AD TYPE CONTROLLED BY AUTHOR
|--------------------------------------------------------------------------
*/

function cp_auction_author_list_by_intention($uid) {
	global $wp_query, $post, $cp_options;

	$new_class = "postform";
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$cpa = get_option('cp_auction_plugin_auctiontype');

?>
	<div class="shadowblock_out">

		<div class="shadowblock">

			<div class="aws-box">

	<p><?php _e('<strong>Note</strong>: If you do not select a category you will then find all the ads in relation to the type of ad you are searching.', 'auctionPlugin'); ?></p>

<?php
	if( $cpa == "classified" || $cpa == "wanted" || $cpa == "mixed" || $cpa == "reverse" ) {
	$view_listings = ''.__('View Listings', 'auctionPlugin').'';
	} else {
	$view_listings = ''.__('View Auctions', 'auctionPlugin').'';
	} ?>

	<div class="clear5"></div>

	<form class="cp-ecommerce" action="" method="post" id="mainform">

<p>
	<label for="cp_auction_my_intention"><?php _e('Select the type of Ad:', 'auctionPlugin'); ?></label>
	<?php $list_type = ""; if(isset($_POST['cp_auction_my_intention'])) $list_type = $_POST['cp_auction_my_intention']; ?>
	<select class="<?php echo $new_class; ?>" name="cp_auction_my_intention" id="cp_auction_my_intention" tabindex="1"><?php echo cp_auction_select_ad_type($list_type); ?></select>
</p>

<p>
	<label for="category_parent"><?php _e('Select Category (optional):', 'auctionPlugin'); ?></label>
	<?php $category_parent = ""; if(isset($_POST['category_parent'])) $category_parent = $_POST['category_parent']; ?>
	<?php echo cp_auction_get_cat_dropdown($category_parent, 7); ?>
</p>

	<div class="clear10"></div>

<p class="submit">
<input type="submit" name="view" value="<?php echo $view_listings; ?>" class="btn_orange" />
</p>

        </form>

		</div><!-- /aws-box -->

	</div><!-- /shadowblock -->

</div><!-- /shadowblock_out -->

                <?php if(isset($_POST['view'])) {

		$cp_category 	= isset( $_POST['category_parent'] ) ? esc_attr( $_POST['category_parent'] ) : '';
		$type		= isset( $_POST['cp_auction_my_intention'] ) ? esc_attr( $_POST['cp_auction_my_intention'] ) : '';
		$trans		= get_localized_field_values('cp_iwant_to', $type, 'local');
		if( $cp_category < 1 ) $cp_category = false;

		global $wpdb;

		$current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;

		if( ! $cp_category ) {

		$querystr = "
		SELECT DISTINCT wposts.* 
		FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta 
		WHERE wposts.ID = wpostmeta.post_id 
		AND (wpostmeta.meta_key = 'cp_iwant_to' AND wpostmeta.meta_value = '$trans' 
		OR wpostmeta.meta_key = 'cp_want_to' AND wpostmeta.meta_value = '$trans') 
		AND wposts.post_status = 'publish' 
		AND wposts.post_author = '$uid' 
		AND wposts.post_type = '".APP_POST_TYPE."' 
		ORDER BY wposts.post_date DESC";

		} else {

		$querystr = "
		SELECT * FROM $wpdb->posts
		LEFT JOIN $wpdb->postmeta m1 ON($wpdb->posts.ID = m1.post_id)
		LEFT JOIN $wpdb->term_relationships ON($wpdb->posts.ID = $wpdb->term_relationships.object_id)
		LEFT JOIN $wpdb->term_taxonomy ON($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
		WHERE $wpdb->term_taxonomy.term_id = '$cp_category'
		AND $wpdb->term_taxonomy.taxonomy = '".APP_TAX_CAT."'
		AND (m1.meta_key = 'cp_iwant_to' AND m1.meta_value = '$trans')
		AND $wpdb->posts.post_status = 'publish'
		AND $wpdb->posts.post_author = '$uid'
		AND $wpdb->posts.post_type = '".APP_POST_TYPE."'
		ORDER BY $wpdb->posts.post_date DESC";

		}

		$rows = $wpdb->get_results($querystr, OBJECT);

		if( $wpdb->num_rows != 0 ) {

   		global $post;
   		foreach ($rows as $post) { 
     		setup_postdata($post);

		$cp_ad_sold = end_auction($post->ID);
		if( $cp_ad_sold != "yes" ) { ?>
		<div class="sellmyapps">
		<?php echo cp_auction_loop_ad_listing($post); ?>
		</div>
		<?php } ?>

		<?php } if(function_exists('appthemes_pagination')) appthemes_pagination(); ?>

	<?php } else { ?>

<div class="shadowblock_out">

	<div class="shadowblock">

		<div class="pad10"></div>
		<p class="text-center"><?php _e( 'Sorry, there are currently no ads listed.', 'auctionPlugin' ); ?></p>
		<div class="pad10"></div>

            </div><!-- /shadowblock_out -->
          
        </div><!-- /shadowblock -->  

	<?php }
	}
}


/*
|--------------------------------------------------------------------------
| AUCTION LAST CHANGE LISTINGS - SOON TO CLOSE
|--------------------------------------------------------------------------
*/

function cp_auction_author_soon_to_close($uid) {
	global $wp_query, $post;
	$maxposts = get_option('posts_per_page');

	remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
	$post_type_url = esc_url( add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) ) );
?>

	<?php
	global $wpdb;
	$hours = get_option('cp_auction_plugin_lastchance');
	$todayDate = date_i18n('m/d/Y H.i', strtotime("current_time('mysql')"), true); // current date
	$currentTime = strtotime($todayDate); //Change date into time
	$newtime = date_i18n('m/d/Y H.i', strtotime("$todayDate + $hours"), true);
	$date = strtotime($newtime);

	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'author__in' => $uid, 
	'paged' => $paged, 
	'meta_query' => 
		array( 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => array( 'normal', 'reverse' ) 
			), 
			array( 
			'key' => 'cp_end_date', 
			'value' => $date,
			'compare' => '<'
			) 
		) 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

						<div class="sellmyapps">
						<?php get_template_part( 'loop', 'ad_listing' ); ?>
						</div>

						<?php
							if ( $total_pages > 1 ) {
							$closing_url = esc_url( add_query_arg( array( 'sort' => 'closing|'.$curauth->ID.'' ), $post_type_url ) );
						?>
						<div class="<?php echo $css; ?>"><a href="<?php echo $closing_url; ?>"><?php if ( get_option('stylesheet') == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
						<?php }
}


/*
|--------------------------------------------------------------------------
| BUY NOW CLASSIFIEDS
|--------------------------------------------------------------------------
*/

function cp_auction_author_buy_now_classifieds($uid) {
	global $wp_query, $post;
	$maxposts = get_option('posts_per_page');

	remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
	$post_type_url = esc_url( add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) ) );
?>

	<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'author__in' => $uid, 
	'paged' => $paged, 
	'meta_query' => 
		array( 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => 'classified' 
			), 
			array( 
			'key' => 'cp_buy_now_enabled', 
			'compare' => 'EXISTS'
			) 
		) 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

			<div class="sellmyapps">
			<?php get_template_part( 'loop', 'ad_listing' ); ?>
			</div>

			<?php
				if ( $total_pages > 1 ) {
				$buynow_url = esc_url( add_query_arg( array( 'sort' => 'buyitnow|'.$curauth->ID.'' ), $post_type_url ) );
			?>
				<div class="<?php echo $css; ?>"><a href="<?php echo $buynow_url; ?>"><?php if ( get_option('stylesheet') == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
			<?php }
}


/*
|--------------------------------------------------------------------------
| BUY NOW AUCTIONS
|--------------------------------------------------------------------------
*/

function cp_auction_author_buy_now_auctions($uid) {
	global $wp_query, $post;
	$maxposts = get_option('posts_per_page');

	remove_action( 'appthemes_after_endwhile', 'cp_do_pagination' );
	$post_type_url = esc_url( add_query_arg( array( 'paged' => 2 ), get_post_type_archive_link( APP_POST_TYPE ) ) );
?>

	<?php
	// setup the pagination and query
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	query_posts( array( 
	'posts_per_page' => $maxposts, 
	'post_type' => APP_POST_TYPE, 
	'post_status' => 'publish', 
	'author__in' => $uid, 
	'paged' => $paged, 
	'meta_query' => 
		array( 
			array( 
			'key' => 'cp_auction_my_auction_type', 
			'value' => 'normal', 'reverse' 
			), 
			array( 
			'key' => 'cp_buy_now', 
			'value' => 0, 
			'compare' => '>'
			) 
		) 
	) 
);

	// build the row counter depending on what page we're on
	$total_pages = max( 1, absint( $wp_query->max_num_pages ) ); ?>

			<div class="sellmyapps">
			<?php get_template_part( 'loop', 'ad_listing' ); ?>
			</div>

			<?php
				if ( $total_pages > 1 ) {
				$buynow_url = esc_url( add_query_arg( array( 'sort' => 'buynow|'.$curauth->ID.'' ), $post_type_url ) );
			?>
				<div class="<?php echo $css; ?>"><a href="<?php echo $buynow_url; ?>"><?php if ( get_option('stylesheet') == "rondell" ) { ?><div class="paginationico"></div><?php } ?> <?php _e( 'View More Ads', 'auctionPlugin' ); ?> </a><span class="total-ads"><?php
$count_posts = wp_count_posts(APP_POST_TYPE);
$ads = $count_posts->publish;
echo "$ads ";?> <?php _e( 'ads online', 'auctionPlugin' ); ?>

	</span></div>
			<?php }
}