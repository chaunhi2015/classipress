<?php
global $current_user, $app_abbr, $gmap_active, $cp_options;
	$current_user = wp_get_current_user();

// make sure google maps has a valid address field before showing tab
$custom_fields = get_post_custom(); 

if (get_option('remove_maps') == 'yes') {
$gmap_active = false; 
}
elseif ( !empty($custom_fields[$app_abbr.'_zipcode']) || !empty($custom_fields[$app_abbr.'_country']) || 
	!empty($custom_fields[$app_abbr.'_state']) || !empty($custom_fields[$app_abbr.'_city']) || 
	!empty($custom_fields[$app_abbr.'_street']) ) {
	$gmap_active = true; 
}

$bid_widget = is_active_widget( false, false, 'cp_auctions_bidder_widget', true );
$bof_widget = is_active_widget( false, false, 'aws_bestoffer_widget', true );
$want_widget = is_active_widget( false, false, 'cp_auctions_wanted_widget', true );
$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
$boffer = get_post_meta( $post->ID, 'cp_bestoffer', true );
$allowdeals = get_post_meta( $post->ID, 'cp_allow_deals', true );
$allow_deals = get_localized_field_values('cp_allow_deals', $allowdeals);

if ( $my_type == "normal" && ! $bof_widget && ! $bid_widget ) $enable = true;
else if ( $boffer == true && ! $bof_widget ) $enable = true;
else if ( $allow_deals == "Yes" && ! $want_widget ) $enable = true;
else $enable = false;

?>

<!-- right sidebar -->
<div class="content_right">
	
<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('jibo_single_tabs') == undefined) { 
    	$.cookie('jibo_single_tabs', '<?php echo get_option('default_ad_tab') ?>', {expires:365, path:'/'});
	}
	$("#tabswithstyle").tabs({ 
    activate: function (e, ui) { 
        $.cookie('jibo_single_tabs', ui.newTab.index(), { expires: 365, path: '/' }); 
    },
<?php if ( $gmap_active ) { ?>
	//show: function(e, ui) {
            //if (ui.index == 0) { google.maps.event.trigger(map, "resize"); }
    //},
<?php } ?>
    active: $.cookie('jibo_single_tabs')             
});
});
</script>
	
	<div id="tabswithstyle" class="tabsidebar">

	<?php if ( $enable == true ) {

		cp_auction_bidder_box_tabbed();

	} else { ?>

		<ul class="tabmenu">
			<?php if ( $gmap_active ) { ?><li><a href="#priceblock1"><span class="big"><?php _e( 'Map', APP_TD ); ?></span></a></li><?php } ?>
			<li><a href="#priceblock2"><span class="big"><?php _e( 'Contact', APP_TD ); ?></span></a></li>
			<li><a href="#priceblock3"><span class="big"><?php _e( 'Poster', APP_TD ); ?></span></a></li>
		</ul>

	<?php } ?>


		<?php if ( $gmap_active ) { ?>

			<!-- tab 1 -->
			<div id="priceblock1">

				<div class="clr"></div>

				<div class="singletab">

					<?php get_template_part( 'includes/sidebar', 'gmap' ); ?>

				</div><!-- /singletab -->

			</div>

		<?php } ?>


		<!-- tab 2 -->
		<div id="priceblock2">

			<div class="clr"></div>

			<div class="singletab">

			<?php if ( ( $cp_options->ad_inquiry_form && is_user_logged_in() ) || ! $cp_options->ad_inquiry_form ) {

				get_template_part( 'includes/sidebar', 'contact' );

			} else {
			?>
				<div class="pad25"></div>
				<p class="contact_msg center"><strong><?php _e( 'You must be logged in to inquire about this ad.', APP_TD ); ?></strong></p>
				<div class="pad100"></div>
			<?php } ?>

			</div><!-- /singletab -->

		</div><!-- /priceblock2 -->


		<!-- tab 3 -->
		<div id="priceblock3">

			<div class="clr"></div>

			<div class="postertab">

				<div class="priceblocksmall dotted">

					<p class="member-title"><?php _e( 'Information about the ad poster', APP_TD ); ?></p>

					<div id="userphoto">
						<p class='image-thumb'><?php appthemes_get_profile_pic( get_the_author_meta('ID'), get_the_author_meta('user_email'), 64 ); ?></p>
					</div>

					<ul class="member">

						<li><span><?php _e( 'Listed by:', APP_TD ); ?></span>
							<a href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php the_author_meta('display_name'); ?></a>
						</li>

						<li><span><?php _e( 'Member Since:', APP_TD ); ?></span> <?php echo appthemes_display_date( get_the_author_meta('user_registered'), 'date' ); ?></li>

					</ul>

					<?php cp_author_info( 'sidebar-ad' ); ?>

					<div class="pad5"></div>

					<div class="clr"></div>

				</div>

				<div class="pad5"></div>

				<h3><?php _e( 'Other items listed by', APP_TD ); ?> <?php the_author_meta('display_name'); ?></h3>

				<div class="pad5"></div>

				<ul>

				<?php $other_items = new WP_Query( array( 'posts_per_page' => 5, 'post_type' => APP_POST_TYPE, 'post_status' => 'publish', 'author' => get_the_author_meta('ID'), 'orderby' => 'rand', 'post__not_in' => array( $post->ID ) ) ); ?>

				<?php if ( $other_items->have_posts() ) : ?>

					<?php while ( $other_items->have_posts() ) : $other_items->the_post(); ?>

						<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

					<?php endwhile; ?>

				<?php else: ?>

					<li><?php _e( 'No other ads by this poster found.', APP_TD ); ?></li>

				<?php endif; ?>

				<?php wp_reset_postdata(); ?>

				</ul>

				<div class="pad5"></div>

				<a href="<?php echo get_author_posts_url( get_the_author_meta('ID') ); ?>" class="btn"><span><?php _e( 'Latest items listed by', APP_TD ); ?> <?php the_author_meta('display_name'); ?> &raquo;</span></a>

			</div><!-- /singletab -->

		</div><!-- /priceblock3 -->

	</div><!-- /tabsidebar -->


	<?php appthemes_before_sidebar_widgets(); ?>

	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('sidebar_listing') ) : else : ?>

	<!-- no dynamic sidebar so don't do anything -->

	<?php endif; ?>

	<?php appthemes_after_sidebar_widgets(); ?>


</div><!-- /content_right -->
