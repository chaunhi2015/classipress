<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<div id="breadcrumb">

				<?php if ( function_exists('cp_breadcrumb') ) cp_breadcrumb(); ?>

			</div>

			<!-- <div style="width: 105px; height:16px; text-align: right; float: left; font-size:11px; margin-top:-10px; padding:0 10px 5px 5px;"> -->
				<?php // if($reported) : ?>
					<!-- <span id="reportedPost"><?php _e( 'Post Was Reported', APP_TD ); ?></span> -->
				<?php // else : ?>
					<!--	<a id="reportPost" href="?reportpost=<?php echo $post->ID; ?>"><?php _e( 'Report This Post', APP_TD ); ?></a> -->
				<?php // endif; ?>
			<!-- </div> -->

			<div class="clr"></div>

			<div class="content_left">

				<?php do_action( 'appthemes_notices' ); ?>

				<?php appthemes_before_loop(); ?>

				<?php if ( have_posts() ) : ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php appthemes_before_post(); ?>

						<?php appthemes_stats_update( $post->ID ); //records the page hit ?>
						
						<?php
							$jibo_ad_sold = get_post_meta($post->ID, 'cp_ad_sold', true);
							$sold = (get_option('cp_mark_sold') == 'yes' && ($jibo_ad_sold == 'yes'));
							$expire_time = strtotime( get_post_meta($post->ID, 'cp_sys_expire_date', true) );
					if (get_option('cp_mark_expired') == 'yes' && current_time('timestamp') > $expire_time) {
							$expired = true;
								}
	
?>

						<div class="shadowblock_out">

							<div class="shadowblock">

								<?php appthemes_before_post_title(); ?>

								<h1 class="single-listing <?php if ( $sold ) {echo 'sold';} ?> <?php if ( $expired ) {echo 'expired';} ?>"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>

								<div class="clr"></div>

								<?php appthemes_after_post_title(); ?>

								<div class="pad5 dotted"></div>
			
								<?php if ( $cp_options->ad_images ) { ?>

									<div class="bigleft">

										<div id="main-pic">

											<?php cp_get_image_url(); ?>

											<div class="clr"></div>

										</div>

										<div id="thumbs-pic">

											<?php cp_get_image_url_single( $post->ID, 'ad-small', $post->post_title, -1 ); ?>

											<div class="clr"></div>

										</div>

									</div><!-- /bigleft -->

								<?php } ?>

								<div class="clr"></div>
								
								
								<div class="bigright <?php cp_display_style( 'ad_single_images' ); ?>">
								<?php if ( $sold ) { ?>
											<div class="note"><?php _e( '<h1>This item has been sold.</h1>', APP_TD ); ?></div>
									<?php }
									elseif ( $expired ) {?>
											<div class="note"><?php _e( '<h1>This ad has expired.</h1>', APP_TD ); ?></div>	
								<?php } ?>
									

									<ul class="twocol-single <?php if ( $sold ) {echo 'sold';} ?> <?php if ( $expired ) {echo 'expired';} ?>">

									<?php
										// grab the category id for the functions below
										$cat_id = appthemes_get_custom_taxonomy( $post->ID, APP_TAX_CAT, 'term_id' );
										

										// 3.0+ display the custom fields instead (but not text areas)
										cp_get_ad_details( $post->ID, $cat_id );
									?>

										<li id="cp_listed"><span><?php _e( 'Listed:', APP_TD ); ?></span> <?php echo appthemes_display_date( $post->post_date ); ?></li>
									<?php if ( $expire_date = get_post_meta( $post->ID, 'cp_sys_expire_date', true ) ) { ?>
										<li id="cp_expires"><span><?php _e( 'Expires:', APP_TD ); ?></span> <?php echo cp_timeleft( $expire_date ); ?></li>
									<?php } ?>

									</ul>

								</div><!-- /bigright -->	
									<div class="clr"></div>

								<?php appthemes_before_post_content(); ?>

								<div class="single-main <?php if ( $sold ) {echo 'sold';} ?> <?php if ( $expired ) {echo 'expired';} ?>">

									<?php
										// 3.0+ display text areas in content area before content.
										cp_get_ad_details( $post->ID, $cat_id, 'content' );
									?>

									<h3 class="description-area"><?php _e( 'Description', APP_TD ); ?></h3>

									<?php the_content(); ?>

								</div>

								<?php appthemes_after_post_content(); ?>

							</div><!-- /shadowblock -->

						</div><!-- /shadowblock_out -->

						<?php appthemes_after_post(); ?>

					<?php endwhile; ?>

					<?php appthemes_after_endwhile(); ?>

				<?php else: ?>

					<?php appthemes_loop_else(); ?>

				<?php endif; ?>

				<div class="clr"></div>

				<?php appthemes_after_loop(); ?>

				<?php wp_reset_query(); ?>

				<div class="clr" id="related-ads"></div>
				
				<?php
global $post;
$terms = get_the_terms( $post->ID , 'ad_cat', 'string');
$jibo_meta = get_option('jibo_custom_meta');
$custom_meta = get_post_meta($post->ID, $jibo_meta, true);
$do_not_duplicate[] = $post->ID;
 if( !empty( $terms ) && get_option('cp_auction_view_all_ads') == '' ) {
    	foreach ($terms as $term) {
        query_posts( array(
        'ad_cat' => $term->slug,
        'showposts' => 5,
        'ignore_sticky_posts' => 1,
        'post__not_in' => $do_not_duplicate,
        'orderby' => 'rand',));
        if(have_posts()){
        while ( have_posts() ) : the_post(); $do_not_duplicate[] = $post->ID; ?>

		<div class="post-block-out pbo <?php cp_display_style( 'featured' ); ?>">
        
            <div class="post-block">
        
                <div class="post-left">
        
                    <?php if ( $cp_options->ad_images ) cp_ad_loop_thumbnail(); ?>
                
                </div>
        
                <div class="<?php cp_display_style( array( 'ad_images', 'ad_class' ) ); ?>">
                
                    <?php appthemes_before_post_title(); ?>
        
                    <h3><a href="<?php the_permalink(); ?>"><?php if ( mb_strlen( get_the_title() ) >= 60 ) echo ucfirst(strtolower ( mb_substr( get_the_title(), 0, 60 ))).'...'; else echo ucfirst(strtolower ( get_the_title())); ?></a></h3>
                    
                    <div class="clr"></div>
                    
                    <p class="post-meta">
        <span class="folder"><?php if ( $post->post_type == 'post' ) the_category(', '); else echo get_the_term_list( $post->ID, APP_TAX_CAT, '', ', ', '' ); ?></span> | <span class="owner"><?php if ( $cp_options->ad_gravatar_thumb ) appthemes_get_profile_pic( get_the_author_meta('ID'), get_the_author_meta('user_email'), 16 ) ?><?php the_author_posts_link(); ?></span> | <?php echo $custom_meta; ?> <span class="clock"><span><?php echo appthemes_date_posted($post->post_date); ?></span></span>
    </p>
                    
                    <div class="clr"></div>
                    
                    <?php appthemes_before_post_content(); ?>
        
                    <p class="post-desc"><?php echo ucfirst(strtolower (cp_get_content_preview( 220 ))); ?></p>
                    
                    <?php appthemes_after_post_content(); ?>
                    
                    <div class="clr"></div>
        
                </div>
        
                <div class="clr"></div>
        
            </div><!-- /post-block -->
          
        </div><!-- /post-block-out -->   

            <?php 
			endwhile; 
        }
    }
}
		wp_reset_query();
?>

				<?php comments_template( '/comments-ad_listing.php' ); ?>

			</div><!-- /content_left -->

			<?php if ( get_option('cp_auction_enable_custom_sidebar') == "yes" ) {
				include_once( CP_AUCTION_TPL_PATH .'/sidebars/custom-sidebar.php' );
			} else if ( get_option('cp_auction_enable_bid_widget') == "yes" || get_option('aws_bo_use_widget') == "yes" || get_option('cp_auction_enable_wanted_widget') == "yes" ) {
				include_once( CP_AUCTION_TPL_PATH .'/sidebars/sidebar-ad_jibo.php' );
			} else {
				get_sidebar( 'ad' );
			} ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
