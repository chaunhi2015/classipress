<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	require_once(ABSPATH . 'wp-load.php');
	require_once(ABSPATH . 'wp-includes/pluggable.php');

/*
|--------------------------------------------------------------------------
| PRUNES EXPIRED ADS FROM SITE, CRONJOB TO SET IN CPANEL
|--------------------------------------------------------------------------
*/

	global $wpdb;

$pids = $wpdb->get_results( "SELECT * FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = '".APP_POST_TYPE."'" );

if ( $pids ) {
	foreach( $pids as $pid ) {
	$expire_time = strtotime( get_post_meta( $pid->ID, 'cp_sys_expire_date', true ) );

	// if current date is past the expires date, change post status to draft
	if ( current_time( 'timestamp' ) > $expire_time ) {
		$my_post = array();
		$my_post['ID'] = $pid->ID;
		$my_post['post_status'] = 'draft';
		wp_update_post( $my_post );

		}

	}
}