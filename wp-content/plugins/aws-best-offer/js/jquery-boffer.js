/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

jQuery(document).ready(function($){
	attach_best_offer_links();
});

function attach_best_offer_links() {
	// error callback
	var best_offer_failed = function() {
		var c = window.adminpage ? 'warning' : 'error';
		jQuery(this).html(translatable_text_aws.BoffError)
			.removeClass('best-offer-loader')
			.addClass(c);
	};

	// attach ajax query
	jQuery('a.best-offer-link').click(function(){
		jQuery.ajax({
			url: cp_auction_ajax_script.ajaxurl,
			beforeSend: function() {
				jQuery(this).html('').addClass('best-offer-loader');
			},
			context: jQuery(this).parent(),
			dataType: 'json',
			data: {
				action: 'best_offer',
				security: jQuery(this).data('nonce'),
				post_id: jQuery(this).data('rel'),
			},
			success: function( data ) {
				if ( data && data.success ) {
					
				jQuery(this).html(translatable_text_aws.BoffEnabled)
					.removeClass('best-offer-loader')
					.addClass('info');

				} else {
					// failed
					best_offer_failed.call(this);
				}
			},
			error: function() {
				// http error
				best_offer_failed.call(this);
			}
		});
		jQuery(document).ajaxStop(function(){
			setTimeout(window.location.reload.bind(window.location), 100);
		});

		// disable default click behavior
		return false;
	});
}

jQuery(document).ready(function($) {

if ( translatable_text_aws.Featured == 'true' ) {

    $('input[name=featured_ad]').change(function() {
        if(this.checked)
            jQuery('#list_featured_ad').after( '<li id="list_cp_bestoffer"><div class="labelwrapper"><label><a title="' + translatable_text_aws.ToolTip + '" href="#" tip="' + translatable_text_aws.ToolTip + '" tabindex="999"><div class="helpico"></div></a>' + translatable_text_aws.EnableBestOffer + ' </label></div><ol class="checkboxes"><li><input value="Tick to enable" class="checkboxlist" name="cp_bestoffer[]" id="cp_bestoffer_1" type="checkbox">&nbsp;&nbsp;' + translatable_text_aws.TickToEnable + '</li></ol><div class="clr"></div></li>' );
        else
            jQuery('#list_cp_bestoffer').hide();
    });

}

});

jQuery(document).ready( function($) {
	$(".accept-offer").click( function() {
		var retVal = confirm(translatable_text_aws.ConfirmAccept);
   		if( retVal == true ) {
		var data = {
			action: 'aws_accept_offer',
                        post_var: $(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	$.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		$(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	$(".decline-offer").click( function() {
		var retVal = confirm(translatable_text_aws.ConfirmDecline);
   		if( retVal == true ) {
		var data = {
			action: 'aws_decline_offer',
                        post_var: $(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	$.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		$(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});

jQuery(document).ready( function($) {
	$(".delete-offer").click( function() {
		var retVal = confirm(translatable_text_aws.ConfirmDelete);
   		if( retVal == true ) {
		var data = {
			action: 'aws_delete_offer',
                        post_var: $(this).attr("id"),
		};
		// the_ajax_script.ajaxurl is a variable that will contain the url to the ajax processing file
	 	$.post(cp_auction_ajax_script.ajaxurl, data, function(response) {
			//alert(response);
		$(document).ajaxStop(function(){window.location.reload();});
	 	});
	 	}else{
		return false;
		}
	});
});