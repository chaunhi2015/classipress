<?php

/*
The following emails can be edited here, or by using the codestyling localization plugin.
 - NEW BEST OFFER WAS POSTED
 - BEST OFFER WAS ACCEPTED
 - BEST OFFER WAS DECLINED
 - BEST OFFER WAS DELETED
 - COUNTER OFFER WAS POSTED
 - BEST OFFER HAS EXPIRED
*/

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

global $post, $cpurl;

/*
|--------------------------------------------------------------------------
| NEW BEST OFFER WAS POSTED
|--------------------------------------------------------------------------
*/

function aws_bo_new_boffer_was_posted_email( $pid, $post, $uid, $bid, $type = '', $msg = '' ) {

	global $wpdb, $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');
	if( is_numeric( $pid ) ) $posts = get_post( $pid );
	$exp = get_option( 'aws_bo_expiration' );

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$boff_usage = $wpdb->get_var("SELECT SUM(boff_usage) FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$uid'");
	$boff_total = get_option('aws_bo_no_best_offer');

	// BIDDER NOTIFICATION
	$user = get_userdata($uid);
	$subject = "".__('New best offer posted to listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />
	<?php if( $type == "cash" ) { ?>
	<?php _e('You have posted a best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on the ad,','awsOffer'); ?> <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>. <?php _e('The seller should accept, reject or counteroffer within','awsOffer'); ?> <?php echo $exp; ?> <?php _e('hours.','awsOffer'); ?><br /><br />
	<?php } else { ?>
	<?php _e('You have just posted a best offer on the ad,','awsOffer'); ?> <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>, <?php _e('your best offer includes,','awsOffer'); ?> <?php if( is_numeric($pid) ) echo '<a href="'.get_permalink($pid).'">'.$posts->post_title.'</a>'; else echo $pid; ?><?php if( $bid > 0 ) echo ' '.__('and','awsOffer').' '.cp_display_price( $bid, 'ad', false ).' '.__('in cash.','awsOffer').''; else echo '.'; ?> <?php _e('The seller should accept, reject or counteroffer within','awsOffer'); ?> <?php echo $exp; ?> <?php _e('hours.','awsOffer'); ?><br /><br />
	<?php } if( $msg ) { ?>
	<?php _e('Your Message to Seller:','awsOffer'); ?><br />
	<?php echo $msg; ?><br /><br />
	<?php } ?>

	<?php _e('You have used','awsOffer'); ?> <strong><?php echo $boff_usage; ?></strong> <?php _e('of the','awsOffer'); ?> <strong><?php echo $boff_total; ?></strong> <?php _e('available best offers.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$user1 = get_userdata($post->post_author);
	$subject = "".__('New best offer posted to listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?><br /><br />
	<?php if( $type == "cash" ) { ?>
	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> <?php _e('has made a best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('for your ad,','awsOffer'); ?> <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>. <?php _e('You have','awsOffer'); ?> <?php echo $exp; ?> <?php _e('hours to accept, reject or counteroffer the buyer.','awsOffer'); ?><br /><br />
	<?php } else { ?>
	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> <?php _e('has made a best offer for your ad,','awsOffer'); ?> <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>, <?php _e('their best offer includes the product','awsOffer'); ?> <?php if( is_numeric($pid) ) echo '<a href="'.get_permalink($pid).'">'.$posts->post_title.'</a>'; else echo $pid; ?><?php if( $bid > 0 ) echo ' '.__('as well as','awsOffer').' '.cp_display_price( $bid, 'ad', false ).' '.__('in cash.','awsOffer').''; else echo '.'; ?> <?php _e('You have','awsOffer'); ?> <?php echo $exp; ?> <?php _e('hours to accept, reject or counteroffer the buyer.','awsOffer'); ?><br /><br />
	<?php } if( $msg ) { ?>
	<?php _e('Message from Bidder:','awsOffer'); ?><br />
	<?php echo $msg; ?><br /><br />
	<?php } ?>

	<?php _e('Bidder have used','awsOffer'); ?> <strong><?php echo $boff_usage; ?></strong> <?php _e('of their','awsOffer'); ?> <strong><?php echo $boff_total; ?></strong> <?php _e('available best offers.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for posting your listings with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'Content-type: text/'.$plain_html.'; charset=UTF-8' . "\r\n";
	$headers[] = "From: $site_name <$admin_email>";
	$headers[] = "Reply-To: $site_name <$admin_email>";

	wp_mail( $user1->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| BEST OFFER WAS ACCEPTED
|--------------------------------------------------------------------------
*/

function aws_bo_boffer_was_accepted_email( $pid, $post, $cid, $bid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($cid);
	$user1 = get_userdata($post->post_author);
	$subject = "".__('Best offer was accepted for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('Your best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on the ad,','awsOffer'); ?> <a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has been accepted.','awsOffer'); ?><br /><br />

	<?php _e('You should now contact the seller immediately to obtain the necessary payment information.','awsOffer'); ?><br /><br />

	<?php _e('Sellers Name:','awsOffer'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?><br />
	<?php _e('Sellers Email:','awsOffer'); ?> <?php echo $user1->user_email; ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$user1 = get_userdata($post->post_author);
	$subject = "".__('Best offer was accepted for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?><br /><br />

	<?php _e('The best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on your ad,','awsOffer'); ?> <a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has been accepted.','awsOffer'); ?><br /><br />

	<?php _e('You should now contact the buyer asap to agree with this as to how payment is to be performed.','awsOffer'); ?><br /><br />

	<?php _e('Bidders Name:','awsOffer'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br />
	<?php _e('Bidders Email:','awsOffer'); ?> <?php echo $user->user_email; ?><br /><br /><br />


	<?php _e('Thank you for posting your listings with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| BEST OFFER WAS DECLINED
|--------------------------------------------------------------------------
*/

function aws_bo_boffer_was_declined_email( $pid, $post, $cid, $bid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($cid);
	$subject = "".__('Best offer was declined for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('Your best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on the ad,','awsOffer'); ?> <a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has been rejected.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$user1 = get_userdata($post->post_author);
	$subject = "".__('Best offer was declined for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?><br /><br />

	<?php _e('The best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on your ad,','awsOffer'); ?> <a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has been rejected.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for posting your listings with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| BEST OFFER WAS DELETED
|--------------------------------------------------------------------------
*/

function aws_bo_boffer_was_deleted_email( $pid, $post, $cid, $bid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($cid);
	$subject = "".__('Best offer was deleted for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('Your best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on the ad,','awsOffer'); ?> <a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has been deleted.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$user1 = get_userdata($post->post_author);
	$subject = "".__('Best offer was canceled for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?><br /><br />

	<?php _e('The best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on your ad,','awsOffer'); ?> <a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has been deleted.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for posting your listings with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| COUNTER OFFER WAS POSTED BY USER
|--------------------------------------------------------------------------
*/

function aws_bo_uid_counter_offer_posted_email( $pid, $post, $cid, $bid, $msg = '' ) {

	global $post, $cpurl, $wpdb;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$uid_usage = $wpdb->get_var("SELECT SUM(uid_usage) FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$cid'");
	$uid_total = get_option('aws_bo_no_user_counter_offer');
	$exp = get_option( 'aws_bo_expiration' );

	// BIDDER NOTIFICATION
	$user = get_userdata($cid);
	$subject = "".__('Counter offer for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('You have posted a counteroffer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on the ad,','awsOffer'); ?> <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>. <?php _e('The seller should accept, reject or counteroffer within','awsOffer'); ?> <?php echo $exp; ?> <?php _e('hours.','awsOffer'); ?><br /><br />

	<?php _e('You have used','awsOffer'); ?> <strong><?php echo $uid_usage; ?></strong> <?php _e('of the','awsOffer'); ?> <strong><?php echo $uid_total; ?></strong> <?php _e('available counteroffers.','awsOffer'); ?><?php if( $msg ) echo '<br /><br />'; else echo '<br /><br /><br />'; ?>
	<?php if( $msg ) { ?>
	<?php _e('Your Message to Seller:','awsOffer'); ?><br />
	<?php echo $msg; ?><br /><br /><br />
	<?php } ?>


	<?php _e('Thank you for shopping with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$aut_usage = $wpdb->get_var("SELECT SUM(aut_usage) FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$cid'");
	$aut_total = get_option('aws_bo_no_author_counter_offer');
	
	$user1 = get_userdata($post->post_author);
	$subject = "".__('Counter offer for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?><br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> <?php _e('has made a counteroffer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('for your ad,','awsOffer'); ?> <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>. <?php _e('You have','awsOffer'); ?> <?php echo $exp; ?> <?php _e('hours to accept, reject or counteroffer the buyer.','awsOffer'); ?><br /><br />

	<?php _e('You have used','awsOffer'); ?> <strong><?php echo $aut_usage; ?></strong> <?php _e('of the','awsOffer'); ?> <strong><?php echo $aut_total; ?></strong> <?php _e('available counteroffers.','awsOffer'); ?><br /><br />
	<?php if( $msg ) { ?>
	<?php _e('Message from Bidder:','awsOffer'); ?><br />
	<?php echo $msg; ?><br /><br />
	<?php } ?>

	<?php _e('You must log in to your account to submit a counteroffer, accept or reject this offer.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for posting your listings with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| COUNTER OFFER WAS POSTED BY AUTHOR
|--------------------------------------------------------------------------
*/

function aws_bo_aut_counter_offer_posted_email( $pid, $post, $cid, $bid, $msg = '' ) {

	global $post, $cpurl, $wpdb;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$aut_usage = $wpdb->get_var("SELECT SUM(aut_usage) FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$cid'");
	$aut_total = get_option('aws_bo_no_author_counter_offer');
	$exp = get_option( 'aws_bo_expiration' );

	// AUTHOR NOTIFICATION
	$user = get_userdata($post->post_author);
	$subject = "".__('Counter offer for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('You have posted a counteroffer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on your ad,','awsOffer'); ?> <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>. <?php _e('The bidder should accept, reject or counteroffer within','awsOffer'); ?> <?php echo $exp; ?> <?php _e('hours.','awsOffer'); ?><br /><br />

	<?php _e('You have used','awsOffer'); ?> <strong><?php echo $aut_usage; ?></strong> <?php _e('of the','awsOffer'); ?> <strong><?php echo $aut_total; ?></strong> <?php _e('available counteroffers.','awsOffer'); ?><?php if( $msg ) echo '<br /><br />'; else echo '<br /><br /><br />'; ?>
	<?php if( $msg ) { ?>
	<?php _e('Your Message to Bidder:','awsOffer'); ?><br />
	<?php echo $msg; ?><br /><br /><br />
	<?php } ?>


	<?php _e('Thank you for posting your listings with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// BIDDER NOTIFICATION								
	$uid_usage = $wpdb->get_var("SELECT SUM(uid_usage) FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$cid'");
	$uid_total = get_option('aws_bo_no_user_counter_offer');
	
	$user1 = get_userdata($cid);
	$subject = "".__('Counter offer for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?><br /><br />

	<?php echo $user->first_name; ?> <?php echo $user->last_name; ?> <?php _e('has made a counteroffer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('for their ad,','awsOffer'); ?> <a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>. <?php _e('You have','awsOffer'); ?> <?php echo $exp; ?> <?php _e('hours to accept, reject or counteroffer the seller.','awsOffer'); ?><br /><br />

	<?php _e('You have used','awsOffer'); ?> <strong><?php echo $uid_usage; ?></strong> <?php _e('of the','awsOffer'); ?> <strong><?php echo $uid_total; ?></strong> <?php _e('available counteroffers.','awsOffer'); ?><br /><br />
	<?php if( $msg ) { ?>
	<?php _e('Message from Seller:','awsOffer'); ?><br />
	<?php echo $msg; ?><br /><br />
	<?php } ?>

	<?php _e('You must log in to your account to submit a counteroffer, accept or reject this offer.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );
}


/*
|--------------------------------------------------------------------------
| BEST OFFER HAS EXPIRED
|--------------------------------------------------------------------------
*/

function aws_bo_boffer_has_expired_email( $pid, $post, $cid, $bid ) {

	global $post, $cpurl;
	$plain_html = get_option('cp_auction_plugin_plain_html');
	$admin_email = get_bloginfo('admin_email');
	$site_name = get_bloginfo('name');

	// BIDDER NOTIFICATION
	$user = get_userdata($cid);
	$subject = "".__('Best offer has expired for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user->first_name; ?> <?php echo $user->last_name; ?><br /><br />

	<?php _e('Your best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on the ad,','awsOffer'); ?> <a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has expired and was deleted.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for shopping with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user->user_email, $subject, $message, $headers );


	// AUTHOR NOTIFICATION								
	$user1 = get_userdata($post->post_author);
	$subject = "".__('Best offer has expired for listing','awsOffer')." ".$post->post_title;

	ob_start();

	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-header.php');
	} ?>

	<?php _e('Dear','awsOffer'); ?> <?php echo $user1->first_name; ?> <?php echo $user1->last_name; ?><br /><br />

	<?php _e('The best offer of','awsOffer'); ?> <?php echo cp_display_price( $bid, 'ad', false ); ?> <?php _e('on your ad,','awsOffer'); ?> <a href="<?php echo get_permalink($pid); ?>"><?php echo $post->post_title; ?></a> <?php _e('has expired and was deleted.','awsOffer'); ?><br /><br /><br />


	<?php _e('Thank you for posting your listings with us!','awsOffer'); ?><br /><br />

	<?php echo get_bloginfo('name'); ?><br />
	<a href='<?php echo get_bloginfo('wpurl'); ?>'><?php echo get_bloginfo('wpurl'); ?></a><br /><br />

<?php
	if( get_option('cp_auction_plugin_plain_html') == "html" ) {
	include(ABSPATH . 'wp-content/plugins/cp-auction-plugin/emails/email-footer.php');
	}

	$message = ob_get_contents();

	if( get_option('cp_auction_plugin_plain_html') == "plain" ) $message = strip_tags($message);
	ob_end_clean();

	$headers   = array();
	$headers[] = 'MIME-Version: 1.0' . "\r\n";
	$headers[] = 'Content-type: text/'.$plain_html.'; charset='.get_bloginfo( 'charset' ).'' . "\r\n";
	$headers[] = 'From: '.$site_name.' <'.$admin_email.'>' . "\r\n";
	$headers[] = 'Reply-To: '.$site_name.' <'.$admin_email.'>';

	wp_mail( $user1->user_email, $subject, $message, $headers );
}