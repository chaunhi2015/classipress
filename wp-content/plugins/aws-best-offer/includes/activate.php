<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| UPDATE DATABASE
|--------------------------------------------------------------------------
*/

global $wpdb, $aws_bo_db_version;
	$aws_bo_db_version = "1.4";

function aws_bo_plugin_activate() {
	global $wpdb;

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	// check if it is a multisite network
	if (is_multisite()) {
	// check if the plugin has been activated on the network or on a single site
	if (is_plugin_active_for_network(__FILE__)) {
	// get ids of all sites
	$blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
	foreach ($blogids as $blog_id) {
	switch_to_blog($blog_id);
	// activate for each site
	aws_bo_plugin_install($blog_id);
	restore_current_blog();
	}
	}
	else
	{
	// activated on a single site, in a multi-site
	aws_bo_plugin_install($wpdb->blogid);
	}
	}
	else
	{
	// activated on a single site
	aws_bo_plugin_install($wpdb->blogid);
	}
}


function aws_bo_plugin_install( $blog_id ) {

	global $wpdb, $aws_bo_db_version;

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	if( ! get_option( 'aws_bo_db_version' ) ) {

	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$sql = "ALTER TABLE {$table_bids} ADD boff_usage INT( 8 ) NOT NULL";
	@$wpdb->query($sql);

	$sql1 = "ALTER TABLE {$table_bids} ADD uid_counter_offer DECIMAL( 8,2 ) NOT NULL";
	@$wpdb->query($sql1);

	$sql2 = "ALTER TABLE {$table_bids} ADD uid_date BIGINT( 20 ) NOT NULL";
	@$wpdb->query($sql2);

	$sql3 = "ALTER TABLE {$table_bids} ADD uid_usage INT( 8 ) NOT NULL";
	@$wpdb->query($sql3);

	$sql4 = "ALTER TABLE {$table_bids} ADD aut_counter_offer DECIMAL( 8,2 ) NOT NULL";
	@$wpdb->query($sql4);

	$sql5 = "ALTER TABLE {$table_bids} ADD aut_date BIGINT( 20 ) NOT NULL";
	@$wpdb->query($sql5);

	$sql6 = "ALTER TABLE {$table_bids} ADD aut_usage INT( 8 ) NOT NULL";
	@$wpdb->query($sql6);

	$sql7 = "ALTER TABLE {$table_bids} ADD latest_offer DECIMAL( 8,2 ) NOT NULL";
	@$wpdb->query($sql7);

	$sql8 = "ALTER TABLE {$table_bids} ADD acceptance VARCHAR( 20 ) NOT NULL";
	@$wpdb->query($sql8);

	$sql9 = "ALTER TABLE {$table_bids} ADD date_type VARCHAR( 20 ) NOT NULL";
	@$wpdb->query($sql9);

	$sql10 = "ALTER TABLE {$table_bids} ADD item_title VARCHAR( 75 ) NOT NULL";
	@$wpdb->query($sql10);

	$sql11 = "ALTER TABLE {$table_bids} ADD product_pid INT( 8 ) NOT NULL";
	@$wpdb->query($sql11);

	update_option( "aws_bo_db_version", $aws_bo_db_version );

	}

	if( get_option( 'aws_bo_db_version' ) < 1.1 ) {
	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$sql = "ALTER TABLE {$table_bids} MODIFY COLUMN uid_counter_offer DECIMAL( 8,2 ) NOT NULL";
	@$wpdb->query($sql);

	$sql1 = "ALTER TABLE {$table_bids} MODIFY COLUMN uid_date BIGINT( 20 ) NOT NULL";
	@$wpdb->query($sql1);

	$sql2 = "ALTER TABLE {$table_bids} MODIFY COLUMN aut_counter_offer DECIMAL( 8,2 ) NOT NULL";
	@$wpdb->query($sql2);

	$sql3 = "ALTER TABLE {$table_bids} MODIFY COLUMN aut_date BIGINT( 20 ) NOT NULL";
	@$wpdb->query($sql3);

	$sql4 = "ALTER TABLE {$table_bids} MODIFY COLUMN latest_offer DECIMAL( 8,2 ) NOT NULL";
	@$wpdb->query($sql4);

	}

	if( get_option( 'aws_bo_db_version' ) < 1.4 ) {
	$table_bids = $wpdb->prefix . "cp_auction_plugin_bids";
	$sql = "ALTER TABLE {$table_bids} ADD item_title VARCHAR( 75 ) NOT NULL";
	@$wpdb->query($sql);

	$sql1 = "ALTER TABLE {$table_bids} ADD product_pid INT( 8 ) NOT NULL";
	@$wpdb->query($sql1);
	}

	update_option( "aws_bo_db_version", $aws_bo_db_version );
}
?>