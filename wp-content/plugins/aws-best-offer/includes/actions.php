<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| ADD BEST OFFER FORM ON SINGLE AD PAGE - CLASSIFIEDS & AUCTION ADS
|--------------------------------------------------------------------------
*/

function aws_best_offer_box() { ?>

<style type="text/css">
p.success, div.success {
	margin:0 !important;
	font-weight: bold;
}
</style>

<?php
	global $current_user, $wpdb, $post, $cp_options;
	get_currentuserinfo();
	$uid = $current_user->ID;
	$user_can = current_user_can( 'manage_options' );

	$enable = get_option( 'aws_bo_enable_selection' );
	$options = get_option('aws_bo_selection');
	if( $enable == "yes" )
	$opt = explode(',', $options['selection']);
	else $opt = false;

	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );
	$my_type = get_post_meta( $post->ID, 'cp_auction_my_auction_type', true );
	if( empty( $my_type ) ) $my_type = "classified";
	$sold = get_post_meta( $post->ID, 'cp_ad_sold', true );
	$success = ""; if( isset($_GET['success']) ) $success = $_GET['success'];
	$cc = get_user_meta( $post->post_author, 'currency', true );
	if( empty( $cc ) || $cc == "-1" ) $cc = $cp_options->currency_code;

	$option = get_option('cp_auction_fields');
	$table_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_bestoffer'");
	if( $row ) {
	$fname = $row->field_name;
	$list = explode(',', $option[$fname]);
	} else {
	$list = false;
	}
	$class = "";

	if( ( $list && is_single() ) && ( in_array($my_type, $list) && $bestoffer == true ) ) {
	if( get_option('aws_bo_use_widget') == "yes" ) $class = "_widget";
	if( ( $list ) && ( ( $my_type == "normal" && in_array('normal', $list) || $my_type == "reverse" && in_array('reverse', $list) ) 
	&& ( get_option('cp_auction_enable_bid_widget') == "yes" && get_option('aws_bo_use_widget') == "yes" ) ) ) $class = "_widget";

	if(isset($_POST['best_offers'])) {

	global $wpdb, $post, $cp_options;
	$empty = false;
	$error = 0;
	$boff_usage = 0;
	$bid = 0;
	$item = false;
	$message = false;
	$product = false;
	if(isset($_POST['bid_anonymous'])) $bid_anonymous = $_POST['bid_anonymous'];
	if(isset($_POST['agreement'])) $agreement = $_POST['agreement'];
	if(isset($_POST['cash'])) $bid = cp_auction_sanitize_amount($_POST['cash']);
	if(isset($_POST['item'])) $item = $_POST['item'];
	if(isset($_POST['message'])) $message = $_POST['message'];
	if(isset($_POST['option'])) $product = $_POST['option'];
	$enabled = get_option('aws_bo_enable_selection');

	$tm = time();
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$boff_usage = $wpdb->get_var("SELECT SUM(boff_usage) FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$uid'");

	$latest = false;
	if(( $product == "cash-option1" && $bid > 0 ) || ( $enabled != "yes" && $bid > 0 )) {
	$latest = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$uid' AND type = 'bestoffer'");
	if( $latest ) {
	$id = $latest->id;
	$upd_usage = $latest->boff_usage + 1;
	$uid_usage = $latest->uid_usage + 1;
	}
	}

	if(empty($bid_anonymous)) {
	$bid_anonymous = "no";
	}
	if( empty($agreement) ) $error = 1;
	elseif( $bid_anonymous == "yes" && get_option('cp_allow_anonymous'.$post->post_author) == "no" ) $error = 3;
	elseif( is_user_logged_in() != true ) $error = 4;
	elseif( $uid == $post->post_author ) $error = 5;
	elseif( $bid && !is_numeric($bid) ) $error = 6;
	elseif( $boff_usage >= get_option( 'aws_bo_no_best_offer' ) ) $error = 7;
	elseif( $sold == "yes" ) $error = 8;
	elseif( $latest && $latest->bid == $latest->latest_offer ) $error = 9;
	elseif( $latest && $uid_usage >= get_option('clp_bo_no_user_counter_offer' ) ) $error = 10;

	if( $error == 0 ) {

	if(( $product == "cash-option1" && $bid > 0 ) || ( $enabled != "yes" && $bid > 0 )) {
	if( $latest && $latest->aut_counter_offer == $latest->latest_offer ) {
	$where_array = array('id' => $id);
	$wpdb->update($table_name, array('uid_counter_offer' => $bid, 'uid_usage' => $uid_usage, 'uid_date' => $tm, 'latest_offer' => $bid, 'date_type' => 'uid_date'), $where_array);
	aws_bo_uid_counter_offer_posted_email( $post->ID, $post, $cid, $bid, '' );
	} else {
	$query = "INSERT INTO {$table_name} (pid, post_author, datemade, bid, uid, anonymous, type, boff_usage, latest_offer, date_type) VALUES (%d, %s, %d, %f, %d, %s, %s, %d, %f, %s)"; 
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $tm, $bid, $uid, $bid_anonymous, 'bestoffer', '1', $bid, 'datemade'));
	}
	aws_bo_new_boffer_was_posted_email( $post->ID, $post, $uid, $bid, 'cash' );
	}
	elseif( $item && $product == "product-option1" ) {
	$query = "INSERT INTO {$table_name} (pid, post_author, datemade, bid, uid, message, anonymous, type, boff_usage, latest_offer, date_type, item_title) VALUES (%d, %s, %d, %f, %d, %s, %s, %s, %d, %f, %s, %s)"; 
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $tm, $bid, $uid, stripslashes(html_entity_decode($message)), $bid_anonymous, 'boffer-item', '1', $bid, 'datemade', $item));

	aws_bo_new_boffer_was_posted_email( $item, $post, $uid, $bid, 'item', $message );
	}
	elseif( is_numeric( $product ) ) {
	$query = "INSERT INTO {$table_name} (pid, post_author, datemade, bid, uid, message, anonymous, type, boff_usage, latest_offer, date_type, product_pid) VALUES (%d, %s, %d, %f, %d, %s, %s, %s, %d, %f, %s, %d)"; 
	$wpdb->query($wpdb->prepare($query, $post->ID, $post->post_author, $tm, $bid, $uid, stripslashes(html_entity_decode($message)), $bid_anonymous, 'boffer-ads', '1', $bid, 'datemade', $product));

	aws_bo_new_boffer_was_posted_email( $product, $post, $uid, $bid, 'ads', $message );
	}
	else {
	$empty = true;
	appthemes_display_notice( 'error', __( 'Please select an option / item!', 'awsOffer' ) );
	}
	if( $empty == false )
	appthemes_display_notice( 'success', __( 'Please wait while your offer is processed...', 'awsOffer' ) );

	$redirect = ''.get_permalink($post->ID).'?success=1';
	if( $empty == false )
	cp_auction_page_redirect($redirect);
	}
	if( $error == 1 ) appthemes_display_notice( 'error', __( 'You must agree that you have read our policies!','awsOffer' ) );
	elseif( $error == 3 ) appthemes_display_notice( 'error', __( 'Sorry, the author does not allow anonymous offers.','awsOffer' ) );
	elseif( $error == 4 ) appthemes_display_notice( 'error', __( 'You must be logged in to submit your best offer!', 'awsOffer' ) );
	elseif( $error == 5 ) appthemes_display_notice( 'error', __( 'You can not post offers on your own ads!', 'awsOffer' ) );
	elseif( $error == 6 ) appthemes_display_notice( 'error', __( 'Your best offer needs to be a numeric value!', 'awsOffer' ) );
	elseif( $error == 7 ) appthemes_display_notice( 'error', __( 'Sorry, you do not have permission to post more best offers!','awsOffer' ) );
	elseif( $error == 8 ) appthemes_display_notice( 'error', __( 'Sorry but this product is already sold!','awsOffer' ) );
	elseif( $error == 9 ) appthemes_display_notice( 'error', __( 'Sorry, but you have already posted an offer!','awsOffer' ) );
	elseif( $error == 10 ) appthemes_display_notice( 'error', __( 'Sorry, you do not have permission to post more counter offers!','awsOffer' ) );
}
	if( $success == 1 ) appthemes_display_notice( 'success', __( 'Your offer was registered!', 'awsOffer' ) );

?>

<script type="text/javascript">
function overlay() {
	el = document.getElementById("overlay");
	el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
}
</script>

<script type="text/javascript">
jQuery(document).ready(function($) {

   $('.cash-option2, .product-option2, .product-ads2').hide();

   $('#OpTion').on('change', function(){
if($('#OpTion').val() === '')
   {
   $('.cash-option2, .product-option2, .product-ads2').hide();
   }
else if($('#OpTion').val() === 'cash-option1')
   {
   $('#item, #message, #message2, #cash1, #cash2').prop('disabled', true);
   $('#cash').prop('disabled', false);
   $('.product-option2, .product-ads2').hide();
   $('.cash-option2').show();
   }
else if($('#OpTion').val() === 'product-option1')
   {
   $('#cash, #message2, #cash2').prop('disabled', true);
   $('#item, #message, #cash1').prop('disabled', false);
   $('.cash-option2, .product-ads2').hide();
   $('.product-option2').show();
   }
else if($('#OpTion').val() > '0')
   {
   $('#cash, #cash1, #item, #message').prop('disabled', true);
   $('#message2, #cash2').prop('disabled', false);
   $('.cash-option2, .product-option2').hide();
   $('.product-ads2').show();
   }
else
   {
   $('.cash-option2, .product-option2, .product-ads2').hide();
   }
});

<?php if( $cp_options->selectbox ) { ?>
   $('select').selectBox('destroy');
<?php } ?>

});
</script>

		<div id="overlay">
     		<div>
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'awsOffer'); ?></a>]</p>
		<?php echo nl2br(get_option('aws_bo_terms')); ?><br /><br />
     		<p align="right">[<a href='#' onclick='overlay()'><?php _e('CLOSE', 'awsOffer'); ?></a>]</p>
     		</div>
		</div>

<div class="offer_box<?php echo $class; ?>">

	<form method="post" action="">

	<div class="ads_wrap<?php echo $class; ?>">

	<?php if( get_option('aws_bo_info_text') ) echo '<div class="page_info">'.nl2br(get_option('aws_bo_info_text')).'</div>'; ?>

	<div class="clear10"></div>

<?php if( get_option('aws_bo_enable_selection') != "yes" ) { ?>

	<div class="item_detail bid">
	<div class="item_detail1"><label for="cash"> &nbsp; <?php _e('Your Offer', 'awsOffer'); ?> (<?php echo $cc; ?>):</label></div>
	<div class="item_detail2"><input type="text" name="cash" id="cash" value="" tabindex="1" /></div>
	</div>

<?php } else { ?>

	<div class="item_detail bid">
	<div class="item_detail1"><label for="option"> &nbsp; <?php _e('Choose Option:', 'awsOffer'); ?></label></div>
	<div class="item_detail2"><?php aws_bo_options_dropdown( $uid ); ?></div>
	</div>

	<div class="clear5"></div>

	<div id="cash-option1" class="cash-option2">
	<div class="item_detail bid">
	<div class="item_detail1"><label for="cash"> &nbsp; <?php _e('Your Offer', 'awsOffer'); ?> (<?php echo $cc; ?>):</label></div>
	<div class="item_detail2"><input type="text" name="cash" id="cash" value="" tabindex="1" /></div>
	</div>
	</div>

	<div id="product-option1" class="product-option2">
	<?php if( get_option('aws_bo_item_help') ) echo '<div class="page_info">'.nl2br(get_option('aws_bo_item_help')).'</div>'; ?>
	<div class="item_detail bid">
	<div class="item_detail1"><label for="item"> &nbsp; <?php _e('Product Name:', 'awsOffer'); ?></label></div>
	<div class="item_detail2"><input type="text" name="item" id="item" value="" tabindex="1" /></div>
	</div>
	<div class="clear5"></div>
	<div class="item_detail bid"> &nbsp; <label for="message"><?php _e('Describe your product:', 'awsOffer'); ?></label>
        <textarea name="message" id="message" tabindex="2"></textarea>
	</div>
	<?php if( $opt ) {
	if( in_array("combo", $opt )) { ?>
	<div class="clear5"></div>
	<?php if( get_option('aws_bo_amount_help') ) echo '<div class="page_info">'.nl2br(get_option('aws_bo_amount_help')).'</div>'; ?>
	<div class="item_detail bid">
	<div class="item_detail1"><label for="cash1"> &nbsp; <?php _e('Amount', 'awsOffer'); ?> (<?php echo $cc; ?>):</label></div>
	<div class="item_detail2"><input type="text" name="cash" id="cash1" value="" tabindex="1" /></div>
	</div>
	<?php }
	} ?>
	</div>

	<div id="product-ads1" class="product-ads2">
	<div class="clear5"></div>
	<div class="item_detail bid"> &nbsp; <label for="message2"><?php _e('Enter a message:', 'awsOffer'); ?></label>
        <textarea name="message" id="message2" tabindex="2"></textarea>
	</div>
	<?php if( $opt ) {
	if( in_array("combo", $opt )) { ?>
	<div class="clear5"></div>
	<?php if( get_option('aws_bo_amount_help') ) echo '<div class="page_info">'.nl2br(get_option('aws_bo_amount_help')).'</div>'; ?>
	<div class="item_detail bid">
	<div class="item_detail1"><label for="cash2"> &nbsp; <?php _e('Amount', 'awsOffer'); ?> (<?php echo $cc; ?>):</label></div>
	<div class="item_detail2"><input type="text" name="cash" id="cash2" value="" tabindex="1" /></div>
	</div>
	<?php }
	} ?>
	</div>

<?php } ?>

	<div class="clear10"></div>

	<?php $enable_anonymous = get_option('cp_auction_plugin_bid_anonymous');
	if($enable_anonymous == "yes") { ?>
	<div class="item_detail bid">
	<div class="item_detail1"><label for="no yes"> &nbsp; <?php _e('Anonymous:', 'awsOffer'); ?></label></div>
	<div class="item_detail2"><input type="radio" name="bid_anonymous" id="no" value="no" checked /> <label for="no"><?php _e('No', 'awsOffer'); ?></label>
	<input type="radio" name="bid_anonymous" id="yes" value="yes" /> <label for="yes"><?php _e('Yes', 'awsOffer'); ?></label></div>
	</div>
	<div class="clear5"></div>
	<?php } ?>

	<div class="item_detail bid">
	<div class="item_detail1"> &nbsp; <a href="#" title="<?php _e('Click here to read our policies!', 'awsOffer'); ?>" onclick="overlay()"><label for="agreement"><?php _e('Agreement:', 'awsOffer'); ?></a></div>
	<div class="item_detail2"><input type="checkbox" name="agreement" id="agreement" value="yes" tabindex="3" /> <label for="agreement"><?php _e('Yes I Agree.', 'awsOffer'); ?></label></div>
	</div>

	<div class="clear5"></div>

<?php if ( is_user_logged_in() ) { ?>
	<div class="ads_detail">
	<input type="submit" name="best_offers" value="<?php _e('Make an Offer!', 'awsOffer'); ?>" class="btn_orange ads_button" tabindex="2" />
	</div>

	<?php if( cp_auction_buy_now_button_standard( $post->ID ) && get_option('aws_bo_enable_buy_widget') == "yes" && $sold != "yes" ) { ?>

	<div class="clear10"></div>
	<?php if( get_option('aws_bo_buy_text') ) echo '<div class="page_info">'.nl2br(get_option('aws_bo_buy_text')).'</div><div class="clear5"></div>'; ?>
	<div class="text-right"><?php if( function_exists('cp_auction_buy_now_button_standard') ) echo cp_auction_buy_now_button_standard( $post->ID ); ?></div>
	<div class="clr"></div>

	<?php } ?>

<?php } else { ?>
	<div class="ads_detail">
	<a href="<?php echo wp_login_url( get_permalink() ); ?>" class="btn_orange ads_button" tabindex="2"><?php _e('Make an Offer!', 'awsOffer'); ?></a>
	<div class="clear5"></div>
	</div>

	<?php if( cp_auction_buy_now_button_standard( $post->ID ) && get_option('aws_bo_enable_buy_widget') == "yes" && $sold != "yes" ) { ?>

	<div class="clear10"></div>
	<?php if( get_option('aws_bo_buy_text') ) echo '<div class="page_info">'.nl2br(get_option('aws_bo_buy_text')).'</div><div class="clear5"></div>'; ?>
	<div class="clear5"></div>
	<div class="text-right"><a href="<?php echo wp_login_url( get_permalink() ); ?>" class="btn_orange ads_button" tabindex="2"><?php _e('Add to Cart', 'awsOffer'); ?></a></div>
	<div class="clr"></div>

	<?php } ?>

<?php } ?>

	<div class="clear5"></div>

	</div>

</form>

</div>

<div class="clr"></div>

<?php
}
}
if( get_option('aws_bo_use_hooks') != "yes" && get_option('aws_bo_use_widget') != "yes" )
add_action('appthemes_after_post_content', 'aws_best_offer_box');
if( get_option('aws_bo_use_hooks') == "yes" )
add_action('aws_best_offer_here', 'aws_best_offer_box');
if( get_option('aws_bo_use_widget') == "yes" )
add_action('aws_bestoffer_in_widget', 'aws_best_offer_box');


/*
|--------------------------------------------------------------------------
| RETURN MESSAGES ERROR OR SINGLE CLASSIFIED ADS
|--------------------------------------------------------------------------
*/

function aws_bo_ads_messages() {

	$return = ""; if(isset($_GET['return'])) $return = $_GET['return'];
	if( $return == 1 ) echo '<div class="clear10"></div><div class="notice error">'.__('You must agree that you have read our policies!','awsOffer').'</div>';
	if( $return == 2 ) echo '<div class="clear10"></div><div class="notice error">'.__('Your best offer needs to be a numeric value!','awsOffer').'</div>';
	if( $return == 3 ) echo '<div class="clear10"></div><div class="notice error">'.__('You can not post offers on your own listings!','awsOffer').'</div>';
	if( $return == 4 ) echo '<div class="clear10"></div><div class="notice error">'.__('Sorry, you do not have permission to post more best offers!','awsOffer').'</div>';
	if( $return == 5 ) echo '<div class="clear10"></div><div class="notice success">'.__('Your best offer was successfully delivered!','awsOffer').'</div>';
	if( $return == 6 ) echo '<div class="clear10"></div><div class="notice error">'.__('Sorry but this product is already sold!','awsOffer').'</div>';
	if( $return == 7 ) echo '<div class="clear10"></div><div class="notice error">'.__('You must be logged in to submit your best offer!','awsOffer').'</div>';
}
add_action('appthemes_after_post_title', 'aws_bo_ads_messages');

/*
|--------------------------------------------------------------------------
| RETURN MESSAGES ERROR OR INFO
|--------------------------------------------------------------------------
*/

function aws_bo_messages() {

	$return = ""; if(isset($_GET['return'])) $return = $_GET['return'];
	if( $return == 1 ) echo '<div class="notice error">'.__('You must agree that you have read our policies!','awsOffer').'</div>';
	if( $return == 2 ) echo '<div class="notice error">'.__('Your best offer needs to be a numeric value!','awsOffer').'</div>';
	if( $return == 3 ) echo '<div class="notice error">'.__('You can not post offers on your own ads!','awsOffer').'</div>';
	if( $return == 4 ) echo '<div class="notice error">'.__('Sorry, you do not have permission to post more best offers!','awsOffer').'</div>';
	if( $return == 5 ) echo '<div class="notice success">'.__('Your best offer was successfully delivered!','awsOffer').'</div>';
	if( $return == 6 ) echo '<div class="notice error">'.__('Sorry but this product is already sold!','awsOffer').'</div>';
	if( $return == 7 ) echo '<div class="notice error">'.__('You must be logged in to submit your best offer!','awsOffer').'</div>';
}
add_action('cp_auction_info_messages', 'aws_bo_messages');


/*
|--------------------------------------------------------------------------
| ADD BEST OFFER PRICE TO ITEMS I SOLD
|--------------------------------------------------------------------------
*/

function cp_auction_price_details_info() {

	global $wpdb, $post;

	$winner = get_post_meta($post->ID,'winner',true);
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$best = $wpdb->get_row("SELECT * FROM {$table_name} WHERE pid = '$post->ID' AND uid = '$winner' AND winner = '1' AND type = 'bestoffer'");
	if( $best ) {
	echo '<strong>'.__('Best Offer:', 'awsOffer').'</strong> '.cp_auction_format_amount($best->bid).'<br/>';
	} else {
	return false;
	}
}
add_action('cp_auction_price_details', 'cp_auction_price_details_info');