<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| AJAX ACTIONS
|--------------------------------------------------------------------------
*/

function aws_bo_accept_offer() {

	global $post, $wpdb;

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];
	$discount = 0;

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	$pid = $get->pid;
	$cid = $get->uid;
	$post = get_post($pid);

	if( get_post_status( $pid ) != "publish" ) return false;

	if( $get->latest_offer > 0 ) $win_bid = $get->latest_offer;
	else $win_bid = $get->bid;
	// update into bids table
	$where_array = array('id' => $id, 'pid' => $pid);
	$wpdb->update($table_name, array('bid' => cp_auction_sanitize_amount($win_bid), 'winner' => '1', 'acceptance' => 'Accepted'), $where_array);
	// add purchase
	$tm = time();
	$table_purchases = $wpdb->prefix . "cp_auction_plugin_purchases";
	$query = "INSERT INTO {$table_purchases} (pid, uid, buyer, item_name, amount, discount, mc_gross, datemade, numbers, unpaid, net_total, unpaid_total, type, status) VALUES (%d, %d, %d, %s, %f, %f, %f, %d, %d, %d, %f, %f, %s, %s)";
	$wpdb->query($wpdb->prepare($query, $pid, $post->post_author, $cid, $post->post_title, cp_auction_sanitize_amount($win_bid), $discount, cp_auction_sanitize_amount($win_bid), $tm, '1', '1', cp_auction_sanitize_amount($win_bid), cp_auction_sanitize_amount($win_bid), 'bestoffer', 'unpaid'));

	$my_type = get_post_meta( $pid, 'cp_auction_my_auction_type', true );

	if( $my_type == "normal" || $my_type == "reverse" ) {
	// end listing
	$my_post = array();
	$my_post['ID'] = $pid;
	$my_post['post_status'] = 'draft';
	wp_update_post( $my_post );

	update_post_meta( $pid, 'cp_ad_sold', 'yes' );
	update_post_meta( $pid, 'cp_ad_sold_date', $tm );
	update_post_meta( $pid, 'winner', $cid );
	update_post_meta( $pid, 'winner_bid', $win_bid );
	}
	else {
	update_post_meta( $pid, 'cp_ad_sold', 'yes' );
	update_post_meta( $pid, 'cp_ad_sold_date', $tm );
	update_post_meta( $pid, 'cp_auction_howmany', '0' );
	update_post_meta( $pid, 'cp_auction_howmany_added', 'none' );
	}
	aws_bo_boffer_was_accepted_email( $pid, $post, $cid, $win_bid );
	exit;
	}
	else {
		return false;
	}
}
add_action('wp_ajax_aws_accept_offer', 'aws_bo_accept_offer');


function aws_bo_decline_offer() {

	global $post, $wpdb;

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	$pid = $get->pid;
	$bid = $get->bid;
	$cid = $get->uid;
	$post = get_post($pid);

	if( get_post_status( $pid ) != "publish" ) return false;

	// update into db table
	$where_array = array('id' => $id, 'pid' => $pid);
	$wpdb->update($table_name, array('acceptance' => 'Declined'), $where_array);

	aws_bo_boffer_was_declined_email( $pid, $post, $cid, $bid );
	exit;
	}
	else {
		return false;
	}
}
add_action('wp_ajax_aws_decline_offer', 'aws_bo_decline_offer');


function aws_bo_delete_offer() {

	global $post, $wpdb;

	if ( isset( $_POST["post_var"] ) ) {
	$id = $_POST["post_var"];

	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$get = $wpdb->get_row("SELECT * FROM {$table_name} WHERE id = '$id'");
	$pid = $get->pid;
	$bid = $get->bid;
	$cid = $get->uid;
	$post = get_post($pid);

	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));

	aws_bo_boffer_was_deleted_email( $pid, $post, $cid, $bid );
	exit;
	}
	else {
		return false;
	}
}
add_action('wp_ajax_aws_delete_offer', 'aws_bo_delete_offer');