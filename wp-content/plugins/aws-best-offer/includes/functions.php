<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| ENABLE BEST OFFER FROM SINGLE POST IF FEATURED
|--------------------------------------------------------------------------
*/

add_action( 'appthemes_after_post_content', 'aws_bo_post_content', 15 );
function aws_bo_post_content() {
	global $post, $current_user;

	$boffer = get_post_meta( $post->ID, 'cp_bestoffer', true );

	if ( $current_user->ID != $post->post_author )
		return false;

	if ( ! is_singular( APP_POST_TYPE ) || ! is_sticky( $post->ID ) )
		return false;

	if ( $boffer ) return false;
?>
	<p class="best-offer">
		<span class="best-offer-icon"></span>
		<?php echo aws_bo_feature_ad_html( $post ); ?>
	</p>
<?php
}

function aws_bo_feature_ad_html( $post, $inner = '', $class = '' ) {

	$nonce = false;

	$title = esc_attr__( 'Enable best offer on your ad', 'awsOffer' );
	if ( empty( $inner ) ) {
		$inner = __( 'Enable Best Offer', 'awsOffer' );
	}
	$nonce = aws_bo_get_nonce( $post );
	$html = '<a class="best-offer-link ' . $class . '" href="#" data-rel="' . $post->ID . '" data-nonce="' . $nonce . '" title="' . $title . '">' . $inner . '</a>';

		return $html;

}

function aws_bo_get_nonce( $post ) {
	return wp_create_nonce( 'best-offer-' .  $post->ID );
}

add_action( 'wp_ajax_best_offer', 'ajax_enable_best_offer' );
function ajax_enable_best_offer() {

	if ( ! isset( $_GET['post_id'] ) ) {
		die();
	}
	$post_id = $_GET['post_id'];
	check_ajax_referer( 'best-offer-' . $post_id, 'security' );

	$post = get_post( $post_id );

	if ( ! $post ) {
		die();
	}

	$ret = aws_bo_enable_best_offer( $post_id );
	$response = array( 'success' => $ret );

	die( json_encode( $response ) );

	}

function aws_bo_enable_best_offer( $post_id ) {

	$post_id = absint( $post_id );
	if ( ! $post_id || ! is_sticky( $post_id ) ) {
		return false;
	}

	update_post_meta( $post_id, 'cp_bestoffer', __( 'Tick to enable', 'awsOffer' ) );

	return true;

}


/*
|--------------------------------------------------------------------------
| ENABLE OR DISABLE BEST OFFER FROM QUICK EDIT ADMIN
|--------------------------------------------------------------------------
*/

if ( get_option( 'cp_version' ) < '3.4' ) {
	add_filter( 'manage_ad_listing_posts_columns', 'aws_bo_manage_ads_columns', 11 );
} else {
	add_filter( 'manage_posts_columns', 'aws_bo_manage_posts_columns', 10, 2 );
}

function aws_bo_manage_ads_columns( $columns ) {

		$new_columns = array();
			
			foreach( $columns as $key => $value ) {
			
				// default-ly add every original column
				$new_columns[ $key ] = $value;
				
				/**
				 * If currently adding the title column,
				 * follow immediately with our custom columns.
				 */
				if ( $key == 'title' ) {
					$new_columns[ 'cp_bestoffer_column' ] = __( 'Best Offer', 'awsOffer' );
				}
					
			}
			
		return $new_columns;

}

function aws_bo_manage_posts_columns( $columns, $post_type ) {

	switch ( $post_type ) {
	
		case APP_POST_TYPE:
		
			// building a new array of column data
			$new_columns = array();
			
			foreach( $columns as $key => $value ) {
			
				// default-ly add every original column
				$new_columns[ $key ] = $value;
				
				/**
				 * If currently adding the title column,
				 * follow immediately with our custom columns.
				 */
				if ( $key == 'title' ) {
					$new_columns[ 'cp_bestoffer_column' ] = __( 'Best Offer', 'awsOffer' );
				}
					
			}
			
			return $new_columns;
			
	}
	
	return $columns;
	
}

add_action( 'manage_posts_custom_column', 'aws_bo_manage_posts_custom_column', 10, 2 );
function aws_bo_manage_posts_custom_column( $column_name, $post_id ) {

	switch( $column_name ) {

		case 'cp_bestoffer_column':
		$meta_value = false;
		$custom = get_post_custom();
    		if ( isset( $custom['cp_bestoffer'] ) ) $meta_value = $custom['cp_bestoffer'][0];

			if ( $meta_value ) {
			echo '<div style="visibility: hidden; display: none;" id="cp_bestoffer-' . $post_id . '">' . get_post_meta( $post_id, 'cp_bestoffer', true ) . '</div>';
			echo '<font color="green">'.__( 'Enabled', 'awsOffer' ).'</font>';
			} else {
			echo '<font color="red">'.__( 'Disabled', 'awsOffer' ).'</font>';
			}

			break;

	}
	
}

//add_action( 'bulk_edit_custom_box', 'aws_bo_quick_edit_custom_box', 10, 2 );
add_action( 'quick_edit_custom_box', 'aws_bo_quick_edit_custom_box', 10, 2 );
function aws_bo_quick_edit_custom_box( $column_name, $post_type ) {

	switch ( $post_type ) {
	
		case APP_POST_TYPE:
		
			switch( $column_name ) {

				case 'cp_bestoffer_column':
				
					?><fieldset class="inline-edit-col-left">
						<div class="inline-edit-col">
								<span class="input-text-wrap">
									<label style="display:inline;">
										<input type="checkbox" name="cp_bestoffer" value="<?php _e( 'Tick to enable', 'awsOffer' ); ?>" /> 
										<span class="checkbox-title"><?php _e( 'Enable Best Offer', 'awsOffer' ); ?></span>
									</label>
								</span>
						</div>
					</fieldset><?php

					break;

			}
			
			break;
			
	}
	
}

add_action( 'save_post', 'aws_bo_save_post', 10, 2 );
function aws_bo_save_post( $post_id, $post ) {

	if ( ! is_admin() )
		return;

	// pointless if $_POST is empty (this happens on bulk edit)
	if ( empty( $_POST ) )
		return $post_id;
		
	// verify quick edit nonce
	if ( isset( $_POST[ '_inline_edit' ] ) && ! wp_verify_nonce( $_POST[ '_inline_edit' ], 'inlineeditnonce' ) )
		return $post_id;
			
	// don't save for autosave
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return $post_id;
		
	// dont save for revisions
	if ( isset( $post->post_type ) && $post->post_type == 'revision' )
		return $post_id;
		
	switch( $post->post_type ) {
	
		case APP_POST_TYPE:

			if ( isset( $_REQUEST['cp_bestoffer'] ) ) {
    			update_post_meta( $post_id, 'cp_bestoffer', __( 'Tick to enable', 'awsOffer' ) );
  			} else {
    			delete_post_meta( $post_id, 'cp_bestoffer' );
			}

			break;

		}
	
}


/*
|--------------------------------------------------------------------------
| BEST OFFER HOOKS
|--------------------------------------------------------------------------
*/

function aws_best_offer_here() {
	do_action( 'aws_best_offer_here' );
}

function aws_bestoffer_in_widget() {
	do_action( 'aws_bestoffer_in_widget' );
}


/*
|--------------------------------------------------------------------------
| TRANSLATE .JS CONFIRM TEXT
|--------------------------------------------------------------------------
*/

function aws_bo_localize_vars() {

	if ( get_option( 'aws_bo_enable_offer' ) == "featured" ) $featured = "true";
	else $featured = "false";

    return array(
        'ConfirmAccept' => __('Please confirm your acceptance of this offer?', 'awsOffer'),
        'ConfirmDecline' => __('Are you sure you want to reject this offer?', 'awsOffer'),
        'ConfirmDelete' => __('Are you sure you want to delete this offer?', 'awsOffer'),
        'EnableBestOffer' => __('Enable Best Offer:', 'awsOffer'),
        'TickToEnable' => __('Tick to enable', 'awsOffer'),
        'Featured' => $featured,
        'ToolTip' => get_option( 'aws_bo_boffer_tooltip' ),
        'BoffError' => __('An error occurred.', 'awsOffer'),
        'BoffEnabled' => __('Best offer was successfully enabled! Please wait while page is refreshed.', 'awsOffer')
    );
} //End localize_vars


/*
|--------------------------------------------------------------------------
| REMOVE THE BEST OFFER LABEL AND VALUE FROM AD DETAILS
|--------------------------------------------------------------------------
*/

function cp_auction_remove_bestoffer_value() {

	return false;
}
if( get_option( 'aws_bo_auctions' ) == "yes" )
add_filter( 'cp_ad_details_cp_bestoffer', 'cp_auction_remove_bestoffer_value' );


/*
|--------------------------------------------------------------------------
| FIND ALL ADS BASED ON POST TYPE AND AUTHOR - DROPDOWN
|--------------------------------------------------------------------------
*/

function aws_bo_options_dropdown($uid = '', $select = '') {

	global $wpdb, $cp_options;
	$new_class = "";
	$opt = false;
    	if( !$cp_options->selectbox ) {
	$new_class = "cp-dropdown";
	}
	$enable = get_option( 'aws_bo_enable_selection' );
	$options = get_option('aws_bo_selection');
	if( $enable == "yes" )
	$opt = explode(',', $options['selection']);
	else $opt = false;

	if( $uid && $opt ) {
	if( in_array("ads", $opt )) {
	$table_name = $wpdb->prefix . "posts";
	$results = $wpdb->get_results("SELECT * FROM {$table_name} WHERE post_author = '$uid' AND post_status = 'publish' AND post_type = '".APP_POST_TYPE."'");
	}
	}
	else {
	$results = false;
	}

?>
<style type="text/css">
#page_id,
.selectBox,
.cp-dropdown {
	width:100% !important;
	margin-bottom: 0 !important;
}
</style>
<?php

	echo "<select class='cp-dropdown' name = 'option' id = 'OpTion'>";
      	echo "<option value=''>".__('Select Option','awsOffer')."</option>";
      	if( $opt ) { if( in_array("cash", $opt ))
      	echo "<option value='cash-option1'>".__('Offer an Amount','awsOffer')."</option>";
      	} if( $opt ) { if( in_array("item", $opt ))
      	echo "<option value='product-option1'>".__('Offer a Product','awsOffer')."</option>";
	}
	if( $results ) {
	echo "<option value=''>".__('Select an Advertised Product','awsOffer')."</option>";
	foreach($results as $res) {
	echo "<option value='$res->ID'> &nbsp; - $res->post_title</option>";
	}
	}
	echo "</select>";
}