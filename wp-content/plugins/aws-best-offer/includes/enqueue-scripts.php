<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| ADD NECESSARY SCRIPTS TO HEADER OR FOOTER
|--------------------------------------------------------------------------
*/

add_action( 'admin_print_scripts-edit.php', 'aws_bo_enqueue_quick_edit_admin_scripts' );
function aws_bo_enqueue_quick_edit_admin_scripts() {

	wp_enqueue_script( 'aws-quick-edit', trailingslashit( plugins_url( 'aws-best-offer' ) ) . 'js/admin.js', array( 'jquery', 'inline-edit-post' ), '', true );
	
}

function aws_bo_ajax_load_scripts() {
	// load our jquery file that sends the $.post request
	wp_enqueue_script( 'ajax-aws-bo', plugins_url( 'aws-best-offer/js/jquery-boffer.js', 'jquery', '1.0.0' ) );
	wp_localize_script( 'ajax-aws-bo', 'translatable_text_aws', aws_bo_localize_vars() );

	// make the ajaxurl var available to the above script
	wp_localize_script( 'ajax-aws-bo', 'aws_bo_ajax_script', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );	
}
if ( !is_admin() ) {
	add_action('wp_print_scripts', 'aws_bo_ajax_load_scripts');
}

function aws_bo_plugin_style_sheet() {

	wp_enqueue_style( 'aws-bo-style', plugins_url( 'aws-best-offer/css/style.css') );

}

// to speed things up, don't load these scripts in the WP back-end (which is the default)
if ( !is_admin() ) {
	add_action( 'wp_head','aws_bo_plugin_style_sheet' );
}