<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| AWS BEST OFFER SETTINGS PAGE
|--------------------------------------------------------------------------
*/

function aws_best_offer_settings() {
	global $wpdb;
	$msg = "";
	$opt = false;

	if(isset($_POST['save_settings'])) {
		$enable_bestoffer = isset( $_POST['aws_bo_enable_offer'] ) ? esc_attr( $_POST['aws_bo_enable_offer'] ) : '';
		$tooltip = isset( $_POST['aws_bo_boffer_tooltip'] ) ? esc_attr( $_POST['aws_bo_boffer_tooltip'] ) : '';
		$aws_classified = isset( $_POST['classified'] ) ? esc_attr( $_POST['classified'] ) : '';
		$aws_auctions = isset( $_POST['normal'] ) ? esc_attr( $_POST['normal'] ) : '';
		$enable_selection = isset( $_POST['aws_bo_enable_selection'] ) ? esc_attr( $_POST['aws_bo_enable_selection'] ) : '';
		$cash = isset( $_POST['cash'] ) ? esc_attr( $_POST['cash'] ) : '';
		$item = isset( $_POST['item'] ) ? esc_attr( $_POST['item'] ) : '';
		$ads = isset( $_POST['ads'] ) ? esc_attr( $_POST['ads'] ) : '';
		$combo = isset( $_POST['combo'] ) ? esc_attr( $_POST['combo'] ) : '';
		$best_offer = isset( $_POST['aws_bo_no_best_offer'] ) ? esc_attr( $_POST['aws_bo_no_best_offer'] ) : '';
		$user_counter_offer = isset( $_POST['aws_bo_no_user_counter_offer'] ) ? esc_attr( $_POST['aws_bo_no_user_counter_offer'] ) : '';
		$author_counter_offer = isset( $_POST['aws_bo_no_author_counter_offer'] ) ? esc_attr( $_POST['aws_bo_no_author_counter_offer'] ) : '';
		$aws_expiration = isset( $_POST['aws_bo_expiration'] ) ? esc_attr( $_POST['aws_bo_expiration'] ) : '';
		$info_text = isset( $_POST['aws_bo_info_text'] ) ? esc_attr( $_POST['aws_bo_info_text'] ) : '';
		$item_help = isset( $_POST['aws_bo_item_help'] ) ? esc_attr( $_POST['aws_bo_item_help'] ) : '';
		$amount_help = isset( $_POST['aws_bo_amount_help'] ) ? esc_attr( $_POST['aws_bo_amount_help'] ) : '';
		$del_data = isset( $_POST['aws_bo_uninstall'] ) ? esc_attr( $_POST['aws_bo_uninstall'] ) : '';
		$exp_valid = isset( $_POST['aws_bo_exp_valid'] ) ? esc_attr( $_POST['aws_bo_exp_valid'] ) : '';
		$use_hooks = isset( $_POST['aws_bo_use_hooks'] ) ? esc_attr( $_POST['aws_bo_use_hooks'] ) : '';
		$use_widget = isset( $_POST['aws_bo_use_widget'] ) ? esc_attr( $_POST['aws_bo_use_widget'] ) : '';
		$buy_widget = isset( $_POST['aws_bo_enable_buy_widget'] ) ? esc_attr( $_POST['aws_bo_enable_buy_widget'] ) : '';
		$buy_text = isset( $_POST['aws_bo_buy_text'] ) ? esc_attr( $_POST['aws_bo_buy_text'] ) : '';
		$terms = isset( $_POST['aws_bo_terms'] ) ? esc_attr( $_POST['aws_bo_terms'] ) : '';
		$negotiable = isset( $_POST['aws_bo_negotiable_price'] ) ? esc_attr( $_POST['aws_bo_negotiable_price'] ) : '';

		$list = ''.$aws_classified.','.$aws_auctions.'';
		$array = explode( ',', $list );
		$new_list = implode(",", $array);
		$field_ads = trim($new_list, ',');

		$opt = ''.$cash.','.$item.','.$ads.','.$combo.'';
		if( $opt ) {
		$arrays = explode( ',', $opt );
		$new_opt = implode(",", $arrays);
		$opt_fields = trim($new_opt, ',');
		}

		if( $enable_bestoffer == "yes" || $enable_bestoffer == "featured" ) {
		$created = date('Y-m-d h:i:s');
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = 'cp_bestoffer'");
		if( empty( $res ) ) {
		$query = "INSERT INTO {$table_name} (field_name, field_label, field_desc, field_type, field_values, field_perm, field_core, field_req, field_owner, field_created, field_modified) VALUES (%s, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s)";
		$wpdb->query($wpdb->prepare($query, 'cp_bestoffer', __('Enable Best Offer','awsOfferAdmin'), __('Enable Best Offer','awsOfferAdmin'), 'checkbox', __('Tick to enable','awsOfferAdmin'), '0', '1', '0', 'AWS Best Offer', $created, $created));
		}
		$table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
		$fres = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_bestoffer'");
		if( empty( $fres ) ) {
		$fquery = "INSERT INTO {$table_fields} (field_name, field_label, field_desc, field_type, field_values, field_perm, field_core, field_req, field_owner, field_created, field_modified) VALUES (%s, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s)";
		$wpdb->query($wpdb->prepare($fquery, 'cp_bestoffer', __('Enable Best Offer','awsOfferAdmin'), __('Enable Best Offer','awsOfferAdmin'), 'checkbox', __('Tick to enable','awsOfferAdmin'), '0', '1', '0', 'AWS Best Offer', $created, $created));
		}

		$option = get_option('cp_auction_fields');
		$options = get_option('cp_auction_details');
		$prices = get_option('cp_auction_pricefields');
		$option['cp_bestoffer'] = $field_ads;
		if ( ! empty( $option ) )
   		update_option('cp_auction_fields', $option);
   		$options['cp_bestoffer'] = 'no';
		if ( ! empty( $options ) )
   		update_option('cp_auction_details', $options);
   		$prices['cp_bestoffer'] = 'no';
		if ( ! empty( $prices ) )
   		update_option('cp_auction_pricefields', $prices);
		update_option( 'aws_bo_enable_offer', $enable_bestoffer );
		} else {
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE field_name = '%s'", 'cp_bestoffer'));

		$table_fields = $wpdb->prefix . "cp_auction_plugin_fields";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_fields} WHERE field_name = '%s'", 'cp_bestoffer'));
		delete_option( 'aws_bo_enable_offer' );
		}
		if( $tooltip ) {
		$table_name = $wpdb->prefix . "cp_ad_fields";
		$wpdb->query( $wpdb->prepare("UPDATE $table_name SET field_tooltip = %s WHERE field_name = %s", stripslashes(html_entity_decode($tooltip)), 'cp_bestoffer' ) );
		update_option( 'aws_bo_boffer_tooltip', stripslashes(html_entity_decode($tooltip)) );
		} else {
		delete_option( 'aws_bo_boffer_tooltip' );
		}
		if( $enable_selection == "yes" ) {
		update_option('aws_bo_enable_selection', $enable_selection);
		} else {
		delete_option( 'aws_bo_enable_selection' );
		delete_option( 'aws_bo_selection' );
		}
		if( $opt ) {
		$options = get_option('aws_bo_selection');
		$options['selection'] = $opt_fields;
		if ( ! empty( $options ) )
   		update_option('aws_bo_selection', $options);
   		}
		if( $best_offer ) {
		update_option( 'aws_bo_no_best_offer', $best_offer );
		} else {
		delete_option( 'aws_bo_no_best_offer' );
		}
		if( $user_counter_offer ) {
		update_option( 'aws_bo_no_user_counter_offer', $user_counter_offer );
		} else {
		delete_option( 'aws_bo_no_user_counter_offer' );
		}
		if( $author_counter_offer ) {
		update_option( 'aws_bo_no_author_counter_offer', $author_counter_offer );
		} else {
		delete_option( 'aws_bo_no_author_counter_offer' );
		}
		if( $aws_expiration ) {
		update_option( 'aws_bo_expiration', $aws_expiration );
		} else {
		delete_option( 'aws_bo_expiration' );
		}
		if( $info_text ) {
		update_option( 'aws_bo_info_text', stripslashes(html_entity_decode($info_text)) );
		} else {
		delete_option( 'aws_bo_info_text' );
		}
		if( $item_help ) {
		update_option( 'aws_bo_item_help', stripslashes(html_entity_decode($item_help)) );
		} else {
		delete_option( 'aws_bo_item_help' );
		}
		if( $amount_help ) {
		update_option( 'aws_bo_amount_help', stripslashes(html_entity_decode($amount_help)) );
		} else {
		delete_option( 'aws_bo_amount_help' );
		}
		if( $del_data == "yes" ) {
		update_option( 'aws_bo_uninstall', $del_data );
		} else {
		delete_option( 'aws_bo_uninstall' );
		}
		if( $exp_valid == "yes" ) {
		update_option( 'aws_bo_exp_valid', $exp_valid );
		} else {
		delete_option( 'aws_bo_exp_valid' );
		}
		if( $use_hooks == "yes" ) {
		update_option( 'aws_bo_use_hooks', $use_hooks );
		} else {
		delete_option( 'aws_bo_use_hooks' );
		}
		if( $use_widget == "yes" ) {
		update_option( 'aws_bo_use_widget', $use_widget );
		} else {
		delete_option( 'aws_bo_use_widget' );
		}
		if( $buy_widget == "yes" ) {
		update_option( 'aws_bo_enable_buy_widget', $buy_widget );
		} else {
		delete_option( 'aws_bo_enable_buy_widget' );
		}
		if( $buy_text ) {
		update_option( 'aws_bo_buy_text', stripslashes(html_entity_decode($buy_text)) );
		} else {
		delete_option( 'aws_bo_buy_text' );
		}
		if( $terms ) {
		update_option( 'aws_bo_terms', stripslashes(html_entity_decode($terms) ) );
		} else {
		delete_option( 'aws_bo_terms' );
		}
		if( $negotiable == "yes" ) {
		update_option( 'aws_bo_negotiable_price', $negotiable );
		} else {
		delete_option( 'aws_bo_negotiable_price' );
		}

		$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Settings was updated!','awsOfferAdmin').'</strong></p></div>';

}
	$list = false;
	$enabled = get_option( 'aws_bo_enable_offer' );
	$option = get_option('cp_auction_fields');
	$table_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_bestoffer'");
	if( ( $row ) && ( $enabled == "yes" || $enabled == "featured" ) )
	$list = explode(',', $option['cp_bestoffer']);
	else $list = false;


	$enable = get_option( 'aws_bo_enable_selection' );
	$options = get_option('aws_bo_selection');
	if( $enable == "yes" )
	$opt = explode(',', $options['selection']);
	else $opt = false;
?>

	<div class="wrap">
		<?php screen_icon( 'aws-best-offer' ); ?>
		<h2><?php _e( 'AWS Best Offer', 'awsOfferAdmin' ); ?></h2>

	<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Best Offer Settings', 'awsOfferAdmin'); ?></h3>
            <div class="inside">

	<form method="post" action="">

    <table class="form-table">

	<tr valign="top">
        <th scope="row"><?php _e('Enable Best Offer:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_enable_offer">
    <option value="" <?php if(get_option('aws_bo_enable_offer') == '') echo 'selected="selected"'; ?>><?php _e('Disabled', 'awsOfferAdmin'); ?></option>
    <option value="yes" <?php if(get_option('aws_bo_enable_offer') == 'yes') echo 'selected="selected"'; ?>><?php _e('All Ads', 'awsOfferAdmin'); ?></option>
    <option value="featured" <?php if(get_option('aws_bo_enable_offer') == 'featured') echo 'selected="selected"'; ?>><?php _e('Featured Ads', 'awsOfferAdmin'); ?></option>
    </select> <?php _e('Select your preferred setting. All ads or featured ads only.', 'awsOfferAdmin'); ?><br/>
   <small><?php _e('Making the best offer option available on all ads or featured ads only.', 'awsOfferAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Enter a Tooltip:', 'awsOfferAdmin'); ?></th>
    <td><textarea class="options" name="aws_bo_boffer_tooltip" rows="2" cols="60"><?php echo get_option('aws_bo_boffer_tooltip'); ?></textarea><br><small><?php _e('Enter some Best Offer information for the ad author. The tooltip will be applied to the Enable Best Offer checkbox in your ad submission forms.', 'awsOfferAdmin'); ?></small></td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Ad Types:', 'awsOfferAdmin'); ?></th>
   <td><input type="checkbox" name="classified" value="classified" <?php if( $list ) { if( in_array("classified", $list )) echo 'checked="checked"'; } ?>> <?php _e('Classified Ads.', 'awsOfferAdmin'); ?><br/>
   <input type="checkbox" name="normal" value="normal" <?php if( $list ) { if( in_array("normal", $list )) echo 'checked="checked"'; } ?>> <?php _e('Auction Ads.', 'awsOfferAdmin'); ?><br/>
   <small><?php _e('Select the type of ads that should make use of Best Offer.', 'awsOfferAdmin'); ?></small>
        </td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Enable Selectable Options:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_enable_selection"><?php echo cp_auction_yes_no(get_option('aws_bo_enable_selection')); ?></select> <?php _e('Enable an dropdown menu where users can choose between multiple offer options. Set options below.', 'awsOfferAdmin'); ?></td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Selectable Options:', 'awsOfferAdmin'); ?></th>
   <td><input type="checkbox" name="cash" value="cash" <?php if( $opt ) { if( in_array("cash", $opt )) echo 'checked="checked"'; } ?>> <?php _e('Offer an Amount.', 'awsOfferAdmin'); ?><br/>
   <input type="checkbox" name="item" value="item" <?php if( $opt ) { if( in_array("item", $opt )) echo 'checked="checked"'; } ?>> <?php _e('Offer a Product.', 'awsOfferAdmin'); ?><br/>
   <input type="checkbox" name="ads" value="ads" <?php if( $opt ) { if( in_array("ads", $opt )) echo 'checked="checked"'; } ?>> <?php _e('Offer an Advertised Product.', 'awsOfferAdmin'); ?><br/>
   <input type="checkbox" name="combo" value="combo" <?php if( $opt ) { if( in_array("combo", $opt )) echo 'checked="checked"'; } ?>> <?php _e('Combine cash and product.', 'awsOfferAdmin'); ?><br/>
   <small><?php _e('Select the options that should be available in the dropdown menu.', 'awsOfferAdmin'); ?></small>
        </td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('No. of Best Offers:', 'awsOfferAdmin'); ?></th>
    <td><input type="text" name="aws_bo_no_best_offer" value="<?php echo get_option('aws_bo_no_best_offer'); ?>" size="8" /> <?php _e('How many times any user can post a best offer on the same listing.', 'awsOfferAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Buyers Counter Offers:', 'awsOfferAdmin'); ?></th>
    <td><input type="text" name="aws_bo_no_user_counter_offer" value="<?php echo get_option('aws_bo_no_user_counter_offer'); ?>" size="8" /> <?php _e('How many times bidder can post a counter offer on the same listing.', 'awsOfferAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Authors Counter Offers:', 'awsOfferAdmin'); ?></th>
    <td><input type="text" name="aws_bo_no_author_counter_offer" value="<?php echo get_option('aws_bo_no_author_counter_offer'); ?>" size="8" /> <?php _e('How many times author can post a counter offer on the same listing.', 'awsOfferAdmin'); ?></td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('Best Offer Expiration:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_expiration">
    <option value="" <?php if(get_option('aws_bo_expiration') == '') echo 'selected="selected"'; ?>><?php _e('Select', 'awsOfferAdmin'); ?></option>
    <option value="6" <?php if(get_option('aws_bo_expiration') == '6') echo 'selected="selected"'; ?>><?php _e('6 hours', 'awsOfferAdmin'); ?></option>
    <option value="12" <?php if(get_option('aws_bo_expiration') == '12') echo 'selected="selected"'; ?>><?php _e('12 hours', 'awsOfferAdmin'); ?></option>
    <option value="24" <?php if(get_option('aws_bo_expiration') == '24') echo 'selected="selected"'; ?>><?php _e('24 hours', 'awsOfferAdmin'); ?></option>
    <option value="48" <?php if(get_option('aws_bo_expiration') == '48') echo 'selected="selected"'; ?>><?php _e('48 hours', 'awsOfferAdmin'); ?></option>
    <option value="60" <?php if(get_option('aws_bo_expiration') == '60') echo 'selected="selected"'; ?>><?php _e('60 hours', 'awsOfferAdmin'); ?></option>
    </select></td>
    </tr>

	<tr valign="top">
	<th scope="row"><?php _e('Expiry Only Counteroffers:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_exp_valid"><?php echo cp_auction_yes_no(get_option('aws_bo_exp_valid')); ?></select> <?php _e('Enable to use expiration on counter offers only.', 'awsOfferAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Information to Buyer:', 'awsOfferAdmin'); ?></th>
    <td><textarea class="options" name="aws_bo_info_text" rows="5" cols="80"><?php echo get_option('aws_bo_info_text'); ?></textarea><br><small><?php _e('Enter some Best Offer information for the buyer here. HTML can be used.', 'awsOfferAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Help with Product:', 'awsOfferAdmin'); ?></th>
    <td><textarea class="options" name="aws_bo_item_help" rows="5" cols="80"><?php echo get_option('aws_bo_item_help'); ?></textarea><br><small><?php _e('Let the buyer know why they should enter a product name. HTML can be used.', 'awsOfferAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Help with Amount:', 'awsOfferAdmin'); ?></th>
    <td><textarea class="options" name="aws_bo_amount_help" rows="5" cols="80"><?php echo get_option('aws_bo_amount_help'); ?></textarea><br><small><?php _e('Let the buyer know why they should add an amount. HTML can be used.', 'awsOfferAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Use Hooks:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_use_hooks"><?php echo cp_auction_yes_no(get_option('aws_bo_use_hooks')); ?></select> <?php _e('Use the hooks provided below.', 'awsOfferAdmin'); ?><br /><small><?php _e('Some child themes have placed the google maps just below the ad description, this may mean that the best offer box is placed at the very bottom of the ad. To make use of another placement put the below hooks where you want the the best offer box to appear in the single ad listing, <strong>OR</strong> use the widget option below.', 'awsOfferAdmin'); ?></small>

	<div class="code_box">
	&lt;?php if ( function_exists('aws_best_offer_here') ) aws_best_offer_here(); ?&gt;
	</div>

    </td>
    </tr>

	<tr valign="top">	
	<th scope="row"><?php _e('Best Offer Widget:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_use_widget"><?php echo cp_auction_yes_no(get_option('aws_bo_use_widget')); ?></select> <?php _e('Display an best offer option box in the sidebar.', 'awsOfferAdmin'); ?><br /><small><?php _e('Go to the widget settings and enable the CP Best Offer widget, <strong>OR</strong> leave the widget as is (disabled) to automatically apply the widget to the on top tabbed sidebar widget. <strong>For other settings that affect on the single ad page functionality visit the <a style="text-decoration: none;" href="/wp-admin/admin.php?page=cp-auction&tab=misc">Misc settings</a></strong>.', 'awsOfferAdmin'); ?></small></td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('Buy Now Button in Widget:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_enable_buy_widget"><?php echo cp_auction_yes_no(get_option('aws_bo_enable_buy_widget')); ?></select> <?php _e('Include the buy now button in the best offer option widget.', 'awsOfferAdmin'); ?></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Buy Now Information:', 'awsOfferAdmin'); ?></th>
    <td><textarea class="options" name="aws_bo_buy_text" rows="5" cols="80"><?php echo get_option('aws_bo_buy_text'); ?></textarea><br><small><?php _e('Enter some information / text just above the buy now button in the widget. HTML can be used.', 'awsOfferAdmin'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><?php _e('Terms & Conditions:', 'awsOfferAdmin'); ?></th>
    <td><textarea class="options" name="aws_bo_terms" rows="10" cols="80"><?php echo get_option('aws_bo_terms'); ?></textarea><br><small><?php _e('Enter your specific terms & conditions here. HTML can be used.', 'awsOfferAdmin'); ?></small></td>
    </tr>

	<tr valign="top">
        <th scope="row"><?php _e('Negotiable:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_negotiable_price"><?php echo cp_auction_yes_no(get_option('aws_bo_negotiable_price')); ?></select> <?php _e('Enable Best Offer on new ads automatically.', 'awsOfferAdmin'); ?><br><small><?php _e('Enable if you want to hide the <strong>Enable Best Offer</strong> checkbox and rather activate best offers on ads automatically if the <strong>Price Negotiable?</strong> checkbox is checked. Will not work if Featured Ads is set above.', 'awsOfferAdmin'); ?></small></td>
    </tr>

	<tr valign="top">	
	<th scope="row"><?php _e('Delete All Plugin Data:', 'awsOfferAdmin'); ?></th>
    <td><select name="aws_bo_uninstall"><?php echo cp_auction_yes_no(get_option('aws_bo_uninstall')); ?></select> <?php _e('Delete all data upon plugin uninstall.', 'awsOfferAdmin'); ?><br /><br />
    <?php _e('<strong>DISCLAIMER</strong>: If for any reason your data is lost, damaged or otherwise becomes unusable in any way or by any means in whichever way I will not take responsibility. You should always have a backup of your database. Use with caution and make a backup first!', 'awsOfferAdmin'); ?></td>
    </tr>

    <tr valign="top">
    <td colspan="2"><input type="submit" value="<?php _e('Save Settings', 'awsOfferAdmin'); ?>" name="save_settings" class="button-primary"/></td>
    </tr>
    </table>
	</form>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Tools & Shortcuts', 'awsOfferAdmin'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'awsOfferAdmin'); ?></th>
    <td><a class="button-secondary" href="http://dev.wp-build.com/wp-content/plugins/aws-best-offer/change-log.txt" onclick="javascript:void window.open('http://dev.wp-build.com/wp-content/plugins/aws-best-offer/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'awsOfferAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'awsOfferAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'awsOfferAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

	<?php if ( function_exists('cp_auction_admin_sidebar') ) cp_auction_admin_sidebar(); ?>

            </div>
          </div>
        </div>
</div>

<?php
}