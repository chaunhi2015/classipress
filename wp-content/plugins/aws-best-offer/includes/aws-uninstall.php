<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

    	$del_options = get_option('aws_bo_uninstall');

function uninstall_aws_boffer() {

	global $wpdb;

	$table_name = $wpdb->prefix . "cp_ad_fields";
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE field_name = '%s'", 'cp_bestoffer'));

	$aws_prefix = "aws_bo";
	$sql = "DELETE FROM ". $wpdb->options
         ." WHERE option_name LIKE '".$aws_prefix."%'";
	$wpdb->query($sql);

}

if ( $del_options == "yes" ) {
	uninstall_aws_boffer();
}
?>