<?php
/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| SELECT WHICH FORM FIELDS TO DISPLAY IN FORM
|--------------------------------------------------------------------------
*/

function aws_best_offer_display_ad_form_fields( $result ) {

if ( !$result )
	return false;

$post_id = false;

if ( isset( $_GET['listing_edit'] ) ) $post_id = $_GET['listing_edit'];
else if ( isset( $_GET['aid'] ) ) $post_id = $_GET['aid'];

// Hooks into cp_formbuilder_field and cp_formbuilder_review_field to disable
// fields not in use for the actual ad type.

if ( ! is_sticky( $post_id ) && get_option('aws_bo_enable_offer') == "featured" && $result->field_name == "cp_bestoffer" )
	return false;

else

	return $result;
}
add_filter( 'cp_formbuilder_field', 'aws_best_offer_display_ad_form_fields' );
//add_filter( 'cp_formbuilder_review_field', 'aws_best_offer_display_ad_form_fields' );