<?php

/*
Plugin Name: AWS Best Offer
Plugin URI: http://www.arctic-websolutions.com/
Description: Add Best Offer functionality to CP Auction Plugin for ClassiPress
Version: 1.3.7
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


if ( ! defined( 'AWS_BO_VERSION' ) ) {
	define( 'AWS_BO_VERSION', '1.3.7' );
}

if ( ! defined( 'AWS_BO_LATEST_RELEASE' ) ) {
	define( 'AWS_BO_LATEST_RELEASE', '19 August 2015' );
}

if ( ! defined( 'AWS_BO_PLUGIN_FILE' ) ) {
	define( 'AWS_BO_PLUGIN_FILE', __FILE__ );
}

/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION - EASY TRANSLATION USING CODESTYLING LOCALIZATION
|--------------------------------------------------------------------------
*/

function aws_bo_textdomain() {

	$locale = apply_filters('plugin_locale', get_locale(), 'awsOffer');

	load_textdomain( 'awsOffer', WP_LANG_DIR.'/aws-best-offer/awsOffer-'.$locale.'.mo' );
	load_plugin_textdomain( 'awsOffer', false, dirname( plugin_basename( AWS_BO_PLUGIN_FILE ) ) . '/languages/' );
}
if( !is_admin() )
add_action( 'init', 'aws_bo_textdomain' );

function aws_bo_admin_textdomain() {

	$locale = apply_filters('plugin_locale', get_locale(), 'awsOfferAdmin');

	load_textdomain( 'awsOfferAdmin', WP_LANG_DIR.'/aws-best-offer/awsOfferAdmin-'.$locale.'.mo' );
	load_plugin_textdomain( 'awsOfferAdmin', false, dirname( plugin_basename( AWS_BO_PLUGIN_FILE ) ) . '/languages/' );
}
if( is_admin() )
add_action( 'init', 'aws_bo_admin_textdomain' );


function aws_bo_new_menu_items() {
	global $aws_bo_admin_menu;
	$aws_bo_admin_menu = add_submenu_page( 'cp-auction', __( 'AWS Best Offer', 'awsOfferAdmin' ), __( 'Best Offer', 'awsOfferAdmin' ), 'manage_options', 'aws-bestoffer', 'aws_best_offer_settings' );
}
add_action( 'admin_menu', 'aws_bo_new_menu_items', 10 );


include_once('includes/functions.php');
include_once('includes/actions.php');
include_once('includes/ajax-functions.php');
include_once('includes/step-functions.php');
include_once('includes/activate.php');
include_once('includes/enqueue-scripts.php');
include_once('emails/emails.php');
include_once('includes/aws-settings.php');


/*
|--------------------------------------------------------------------------
| PLACE AN BEST OFFER OPTION IN SIDEBAR
|--------------------------------------------------------------------------
*/

class AWS_BestOffer_Widget extends WP_Widget {

function __construct() {
        $widget_ops = array( false, 'description' => __('Display an best offer option box in the sidebar.','awsOfferAdmin') );
        parent::__construct( false, 'AWS Best Offer', $widget_ops );
}

function widget( $args, $instance ) {

	global $current_user, $post, $wpdb;
    	get_currentuserinfo();
	$uid = $current_user->ID;

	$bestoffer = get_post_meta( $post->ID, 'cp_bestoffer', true );

if( $bestoffer == true && get_option('aws_bo_use_widget') == "yes" && is_singular(APP_POST_TYPE) ) {

		extract($args);
		if ( isset( $instance['title'] ) ) $title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;
		if ( ! empty( $title ) )
			echo $before_title . $title . $after_title;
			else
		echo $before_title; ?><?php $arr = get_option('AwsBestOfferBox_widget'); echo $arr['title']; ?><?php echo $after_title; ?>

	<div style="margin:10px 0;overflow:hidden">

	<?php aws_bestoffer_in_widget(); ?>

	</div><!-- /style -->

<?php
		echo $after_widget;
}
}

function update( $new_instance, $old_instance ) {

		$instance = array();
		if ( isset( $new_instance['title'] ) ) $instance['title'] = strip_tags( $new_instance['title'] );
		return $instance;
	}

function form( $instance ) {
 $options = get_option("AwsBestOfferBox_widget");
  if (!is_array( $options ))
	{

	$options = array(
	'title' => ''.__('Make an Offer','awsOfferAdmin').''
      );
  	}

  if (isset($_POST['submit']))
  {

    $options['title'] = htmlspecialchars($_POST['WidgetTitle']);
    update_option("AwsBestOfferBox_widget", $options);

  }

?>

  <table width="100%">
  <tr>
  <td>
    <label for="WidgetTitle"><?php _e('Widget Title:', 'awsOfferAdmin'); ?> </label></td>
   <td> <input type="text" id="WidgetTitle" name="WidgetTitle" value="<?php echo $options['title'];?>" /></td>
   </tr>

    </table>

    <input type="hidden" id="submit" name="submit" value="1" />
  </p>

<?php
}
}
add_action('widgets_init', create_function('', 'register_widget("AWS_BestOffer_Widget");'));


/*
|--------------------------------------------------------------------------
| SCHEDULE CRON JOBS
|--------------------------------------------------------------------------
*/

add_action( 'wp', 'aws_bo_setup_schedule' );
/**
 * On an early action hook, check if the hook is scheduled - if not, schedule it.
 */
function aws_bo_setup_schedule() {
	if ( ! wp_next_scheduled( 'aws_bo_hourly_event' ) ) {
		wp_schedule_event( time(), 'hourly', 'aws_bo_hourly_event');
	}
}

add_action( 'aws_bo_hourly_event', 'aws_bo_do_this_hourly' );
/**
 * On the scheduled action hook, run a function.
 */
function aws_bo_do_this_hourly() {
	// do something every hour
	global $post, $wpdb;
	$exp_valid = get_option('aws_bo_exp_valid');
	$exp_time = get_option('aws_bo_expiration');
	$nowtime = date_i18n("Y-m-d H:i:s");
	$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
	$numposts = $wpdb->get_results("SELECT * FROM {$table_name} WHERE type IN ('bestoffer', 'boffer-item','boffer-ads') AND acceptance = ''");

	if( $numposts ) {
        foreach( $numposts as $num ) {

	$id = $num->id;
	$pid = $num->pid;
	$cid = $num->uid;
	$post = get_post($pid);
	$bid = $num->latest_offer;
	$date_type = $num->date_type;
	$last_date = $num->$date_type;
	$lastdate = date_i18n('Y-m-d H:i:s', $last_date, true);
	$exp_date = date_i18n('Y-m-d H:i:s', strtotime($lastdate . ' + '.$exp_time.' hours'));

	if( $nowtime > $exp_date ) {
		if( ( $exp_valid == "yes" ) && ( $num->uid_counter_offer > 0 || $num->aut_counter_offer > 0 ) ) {
		aws_bo_boffer_has_expired_email( $pid, $post, $cid, $bid );
		$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d' AND date_type != 'datemade'", $id));
		} else if ( $exp_valid != "yes" ) {
		aws_bo_boffer_has_expired_email( $pid, $post, $cid, $bid );
		$table_name = $wpdb->prefix . "cp_auction_plugin_bids";
		$wpdb->query( 
		$wpdb->prepare("DELETE FROM {$table_name} WHERE id = '%d'", $id));
		}
	}
	}
}
}


/*
|--------------------------------------------------------------------------
| ACTIVATE / ENABLE AWS BEST OFFER PLUGIN
|--------------------------------------------------------------------------
*/

function aws_best_offer_plugin_activate() {

    	aws_bo_plugin_activate();

	$info_text = get_option('aws_bo_info_text');
	$item_help = get_option('aws_bo_item_help');
	$amount_help = get_option('aws_bo_amount_help');
	$buy_text = get_option('aws_bo_buy_text');
	if( empty( $info_text ) ) {
	update_option( 'aws_bo_info_text', '<strong>THE BEST OFFER</strong> - This option allows you to offer to purchase this item at the price you suggest, you can specify a product you want to swap with, or you can offer to swap with one of your already announced products. The seller can then accept your offer, reject it, or make a counteroffer.' );
	}
	if( empty( $item_help ) ) {
	update_option( 'aws_bo_item_help', 'Enter the Product Name and a short description of the product you want to trade with. Or <a href="http://dev.wp-build.com/add-new/">click here</a> to create an ad.' );
	}
	if( empty( $amount_help ) ) {
	update_option( 'aws_bo_amount_help', 'Improve your offer, add an amount in addition to the specified product.' );
	}
	if( empty( $buy_text ) ) {
	update_option( 'aws_bo_buy_text', 'Or use the opportunity below to ensure that you get this product, click the button to purchase this product now.' );
	}


	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}
register_activation_hook( __FILE__, 'aws_best_offer_plugin_activate' );


/*
|--------------------------------------------------------------------------
| UNINSTALL AWS BEST OFFER PLUGIN
|--------------------------------------------------------------------------
*/

function aws_boffer_uninstall_actions() {

	require_once dirname( __FILE__ ) . '/includes/aws-uninstall.php';
}
if( get_option('aws_bo_uninstall') == "yes" )
register_uninstall_hook( __FILE__, 'aws_boffer_uninstall_actions' );

?>