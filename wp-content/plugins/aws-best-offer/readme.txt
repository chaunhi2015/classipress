=== AWS Best Offer ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: wordpress, classipress, best offer, classipress theme, auction, cpauction
Requires at least: 3.9.2
Tested up to: 4.3.0
Stable tag: 1.3.7
License: GPL
License URI: http://www.gnu.org/licenses/gpl.html

---------------------------------------------------------------------------------------------------

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://offer.wp-build.com) - Testdrive every aspect of the plugin.
* [Manuals & Support](http://www.arctic-websolutions.com/best-offer/) - Setup instructions and support.

=== Some Admin Features ===

* Set which offer types should be available for bidder, cash, product or select product from already announced ad.
* Set how many times any user can post a best offer on the same listing.
* Set how many times bidder can post a counter offer on the same listing.
* Set how many times author can post a counter offer on the same listing.
* Set best offer expiration, 6 to 72 hours.
* Best offer option in widget.
* Set to include buy now button in place offer widget.
* Action hooks for better placement.

=== Some User Features ===

* Users can post best offers as cash, product, or select product from already announced ad.
* Author can enable/disable best offers on their posts.
* Author can accept offer, counteroffer or reject any offer.
* Both buyer and author can post counter offer.

=== Installation ===

* Download AWS Best Offer.
* Extract the downloaded .zip file. (Not necessary if filename dosen’t say so, ie. unzip-first)
* Login to your websites FTP and navigate to the “wp-contents/plugins/” folder.
* Upload the resulting ”aws-best-offer.zip” or “aws-best-offer-X.X.X.zip” folder to the websites plugin folder.
* Go to your websites Dashboard, access the “plugins” section on the left hand menu.
* Locate “AWS Best Offer” in the plugin list and click “activate”.

Or, install using WP`s plugin installation tool.
Make sure to backup your translations before any upgrade.

Visit your Auctions -> AWS Best Offer, configure any options as desired. That’s it!


=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.