<?php

/*
Copyright 2013 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| HOOKS USED FOR VIDEO PLACEMENT IN SINGLE ADS PAGE
|--------------------------------------------------------------------------
*/

function aws_ev_embedded_video() {
	do_action( 'aws_ev_embedded_video' );
}


/*
|--------------------------------------------------------------------------
| EMBED VIDEO AND REMOVE LABEL AND VALUES FROM AD DETAILS
|--------------------------------------------------------------------------
*/

function aws_ev_embed_video() {
	global $post;
	if( is_single() || is_singular( APP_POST_TYPE ) ) {

	$video_url = get_post_meta($post->ID, 'cp_embed_video', true);

	if ($video_url) { ?>

	<div class="clr"></div>
	<?php if( get_option('stylesheet') != "classipress-mydentity" ) { ?>
	<br/><p class="dotted"></p></br><H3><?php _e('Video','AwsEV'); ?></H3></br>
	<?php } else { ?>
	<div class="pad10"></div>
	<?php } ?>
	<div class="video-wrap">
	<?php echo wp_oembed_get( $video_url ); ?>
	</div>
    	<div class="clear10"></div>

<?php
	}
}
}
if( get_option('aws_ev_use_hooks') != "yes" && get_option('aws_ev_enable_video') == "yes" )
add_action( 'appthemes_after_post_content', 'aws_ev_embed_video' );
elseif( get_option('aws_ev_use_hooks') == "yes" && get_option('aws_ev_enable_video') == "yes" )
add_action( 'aws_ev_embedded_video', 'aws_ev_embed_video' );

function aws_ev_remove_video_details() {
	return false;
}
if( get_option('aws_ev_enable_video') == "yes" )
add_filter( 'cp_ad_details_cp_embed_video', 'aws_ev_remove_video_details' );


/*
|--------------------------------------------------------------------------
| GET YES / NO DROPDOWN
|--------------------------------------------------------------------------
*/

function aws_ev_yes_no( $select = '' ) {

$options = "";
$what = array(
		'no' => __('No','AwsEV'),
		'yes' => __('Yes','AwsEV')
		);

	foreach($what as $what_id=>$yes_no) {
	if($select == $what_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$what_id.'">'.$yes_no.'</option>';
	} else {
	$options .= '<option value="'.$what_id.'">'.$yes_no.'</option>';
	}
	}
	return $options;
}