<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| CREATE THE CARTPAUJ PM DASHBOARD PAGE
|--------------------------------------------------------------------------
*/

function aws_cc_create_page() {

global $admin_id;
$admin_id = get_current_user_id();

$page = get_option( 'aws_cc_page_id' );

if( $page < 1 ) {
$dashboard_page = array(
	'post_title' => 'My Messages',
	'post_content' => '[cartpauj-pm]',
	'comment_status' => 'closed',
	'post_status' => 'publish',
	'post_name' => 'my-messages',
	'post_type' => 'page',
	'post_author' => $admin_id
);

$page_id = wp_insert_post($dashboard_page);
add_post_meta( $page_id, 'aws_cc_dashboard_page', $admin_id );
add_post_meta( $page_id, '_wp_page_template', 'tpl-cartpauj-pm.php' );
add_option( 'aws_cc_page_id', $page_id );
}
}