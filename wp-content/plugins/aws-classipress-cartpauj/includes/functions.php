<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| DISPLAY SEND PM IN SINGLE ADS
|--------------------------------------------------------------------------
*/

if( get_option('aws_cc_enable') == "true" )
add_action('cp_action_after_ad_details', 'aws_cc_after_ad_details', 11, 3 );

function aws_cc_after_ad_details($details = '', $post, $type) {

	global $current_user, $post, $wpdb;
	get_currentuserinfo();
	$uid = $current_user->ID;
	$text = get_option("aws_cc_text_link");

	$pageID =  $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE post_content LIKE '%[cartpauj-pm]%' AND post_status = 'publish' AND post_type = 'page'");
	$pms_link = get_permalink($pageID);
	if( empty( $pageID ) ) $pms_link = ''.CP_DASHBOARD_URL.'';
	$allow_pms = get_user_meta( $post->post_author, 'cp_auction_allow_pms', true );

	if ( is_singular(APP_POST_TYPE) ) {

	if( $type == 'list' && get_option('aws_cc_pos_list') == '1' ) { ?>

	<div class="clear10"></div>
	<div class="pms-links">
	<li id="cp_pm"><span><a href="<?php echo $pms_link; ?>?pmaction=newmessage&to=<?php echo $post->post_author; ?>#pms"><?php echo $text; ?></a></span></li>
	</div>
<?php
}
elseif( $type == 'content' && get_option('aws_cc_pos_content') == '1' ) { ?>

	<div class="clear5"></div>
	<div class="pms-links">
	<strong><a href="<?php echo $pms_link; ?>?pmaction=newmessage&to=<?php echo $post->post_author; ?>#pms"><?php echo $text; ?></a></strong>
	</div>
	<div class="clear10"></div>
<?php
	}
}
}


/*
|--------------------------------------------------------------------------
| DISPLAY NEW PMs RECEIVED IN HEADER
|--------------------------------------------------------------------------
*/

if ( !function_exists('cp_login_head') && get_option('aws_cc_enable_header') == "true" ) {
	function cp_login_head() {

	if (is_user_logged_in()) :
	global $current_user, $wpdb;
	$current_user = wp_get_current_user();
	$uid = $current_user->ID;
	$display_user_name = cp_get_user_name();
	$logout_url = cp_logout_url();

	$table_messages = $wpdb->prefix . "cartpauj_pm_messages";
	$get_pms = $wpdb->get_results($wpdb->prepare("SELECT id FROM {$table_messages} WHERE (to_user = %d AND parent_id = 0 AND to_del <> 1 AND message_read = 0 AND last_sender <> %d) OR (from_user = %d AND parent_id = 0 AND from_del <> 1 AND message_read = 0 AND last_sender <> %d)", $uid, $uid, $uid, $uid));
	$pms = $wpdb->num_rows;

	$pageID =  $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE post_content LIKE '%[cartpauj-pm]%' AND post_status = 'publish' AND post_type = 'page'");
	$pms_link = get_permalink($pageID);
	if( empty( $pageID ) ) $pms_link = ''.CP_DASHBOARD_URL.'';
?>
	<?php _e( 'Welcome,', 'AwsCC' ); ?> <strong><?php echo $display_user_name; ?></strong> [ <a href="<?php echo CP_DASHBOARD_URL; ?>"><?php _e( 'My Dashboard', 'AwsCC' ); ?></a> | <a href="<?php echo $pms_link; ?>#pms"><?php _e('PMs', 'AwsCC'); ?> (<?php echo $pms; ?>)</a> | <a href="<?php echo $logout_url; ?>"><?php _e( 'Log out', 'AwsCC' ); ?></a> ]&nbsp;
	<?php else : ?>
	<?php _e( 'Welcome,', 'AwsCC' ); ?> <strong><?php _e( 'visitor!', 'AwsCC' ); ?></strong> [ <a href="<?php echo appthemes_get_registration_url(); ?>"><?php _e( 'Register', 'AwsCC' ); ?></a> | <a href="<?php echo wp_login_url(); ?>"><?php _e( 'Login', 'AwsCC' ); ?></a> ]&nbsp;
	<?php endif;
	}
}