=== AWS ClassiPress Cartpauj ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: classipress, classipress theme, cartpauj
Requires at least: 3.9.2
Tested up to: 4.2.2
Stable tag: 1.0.3
License: GPL
License URI: http://www.gnu.org/licenses/gpl.html

---------------------------------------------------------------------------------------------------

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://offer.wp-build.com) - Not open for backend testing.
* [Manuals & Support](http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Some Admin Features ===

* Super easy management of all settings.
* Enable on Classified Ads.

=== Some User Features ===

* Users can enable/disable PMs on their ads.

=== Installation ===

* [Instructions] (http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.