<?php

/*
Plugin Name: AWS ClassiPress Cartpauj
Plugin URI: http://www.arctic-websolutions.com/
Description: The very best and most convenient way to add Cartpauj PM to ClassiPress.
Version: 1.0.3
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

/*
Copyright 2013 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AWS_CC_VERSION' ) ) {
	define( 'AWS_CC_VERSION', '1.0.3' );
}

if ( ! defined( 'AWS_CC_LATEST_RELEASE' ) ) {
	define( 'AWS_CC_LATEST_RELEASE', '17 May 2015' );
}

if ( ! defined( 'AWS_CC_PLUGIN_FILE' ) ) {
	define( 'AWS_CC_PLUGIN_FILE', __FILE__ );
}

/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION - EASY TRANSLATION USING CODESTYLING LOCALIZATION
|--------------------------------------------------------------------------
*/

function aws_cc_textdomain() {
	load_plugin_textdomain( 'AwsCC', false, dirname( plugin_basename( AWS_CC_PLUGIN_FILE ) ) . '/languages/' );
}
add_action( 'init', 'aws_cc_textdomain' );


/*
|--------------------------------------------------------------------------
| ADD LINK TO SETTINGS MENU PAGE
|--------------------------------------------------------------------------
*/

function aws_cc_add_link() {

	global $aws_cc_admin_menu;
	$aws_cc_admin_menu = add_menu_page( __( 'AWS ClassiPress Cartpauj', 'AwsCC' ), __( 'ClassiPress PM', 'AwsCC' ), 'manage_options', 'aws-classipress-cartpauj', 'aws_classipress_cartpauj_settings', plugins_url( 'aws-classipress-cartpauj/css/images/pms16.png' ), '6,3' );

	global $aws_cc_settings;
	$aws_cc_settings = add_submenu_page( 'aws-classipress-cartpauj', __('Settings','AwsCC'), __('Settings','AwsCC'), 'manage_options', 'aws-classipress-cartpauj', 'aws_classipress_cartpauj_settings');

}
add_action( 'admin_menu', 'aws_cc_add_link' );


/*
|--------------------------------------------------------------------------
| ADD SETTINGS LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'aws_cc_action_links' );
function aws_cc_action_links( $links ) {

return array_merge(
array(
'settings' => '<a href="' . get_bloginfo( 'url' ) . '/wp-admin/admin.php?page=aws-classipress-cartpauj">'.__( 'Settings', 'AwsCC' ).'</a>'
),
$links
);
}


/*
|--------------------------------------------------------------------------
| ADD SUPPORT LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_row_meta', 'aws_cc_meta_links', 10, 2 );
function aws_cc_meta_links( $links, $file ) {

$plugin = plugin_basename(__FILE__);

// create link
if ( $file == $plugin ) {
return array_merge(
$links,
array( '<a href="http://www.arctic-websolutions.com/forums/" target="_blank">'.__( 'Support', 'AwsCC' ).'</a>' )
);
}
return $links;
}


/*
|--------------------------------------------------------------------------
| REGISTER ADMIN BACKEND / FRONTEND SCRIPTS AND STYLESHEETS
|--------------------------------------------------------------------------
*/

function aws_cc_admin_menu_scripts() {

	wp_enqueue_style( 'aws-cc-admin-style', plugins_url( 'aws-classipress-cartpauj/css/admin.css'), false, '1.0.0' );

}
if ( is_admin() ) {
	add_action( 'admin_print_styles', 'aws_cc_admin_menu_scripts' );
}

function aws_cc_frontend_scripts() {

	wp_enqueue_style( 'aws-cc-style', plugins_url( 'aws-classipress-cartpauj/css/style.css'), false, '1.0.0' );

}
if ( !is_admin() ) {
	add_action('wp_head','aws_cc_frontend_scripts');
}


/*
|--------------------------------------------------------------------------
| INCLUDES
|--------------------------------------------------------------------------
*/

include_once('includes/functions.php');
include_once('includes/create-pages.php');


/*
|--------------------------------------------------------------------------
| THEME TEMPLATE REDIRECTS
|--------------------------------------------------------------------------
*/

add_action( 'template_redirect', 'aws_cc_template_redirect', 1 );

function aws_cc_template_redirect() {

	    	global $wp_query, $wp;
	    	$wp_query->is_404 = false;

		$tmp = explode('?',$_SERVER["REQUEST_URI"]);
		$uri = array_shift($tmp);
		$uri = trim($uri,"/");
		if( $uri == "my-messages" ) {
		appthemes_auth_redirect_login(); // if not logged in, redirect to login page
		nocache_headers();
		add_filter( 'the_title', 'aws_cc_cartpauj_title' );
		function aws_cc_cartpauj_title( $title ) {
			if( $title ) return $title;
			else return utf8_decode( ''.__('My Messages', 'AwsCC').'' );
		}
			header("HTTP/1.1 200 OK");
			require(ABSPATH . 'wp-content/plugins/aws-classipress-cartpauj/library/my-messages.php');
			exit;
		}
}


/*
|--------------------------------------------------------------------------
| ACTIVATE / ENABLE CLASSIPRESS BEST OFFER PLUGIN
|--------------------------------------------------------------------------
*/

function aws_cc_plugin_activate() {

    	aws_cc_create_page();

	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}
register_activation_hook( __FILE__, 'aws_cc_plugin_activate' );


/*
|--------------------------------------------------------------------------
| SETTINGS & TRANSACTIONS TABLE
|--------------------------------------------------------------------------
*/

function aws_classipress_cartpauj_settings() {

	$msg = "";

		if(isset($_POST['form_submit'])) {

			$enable = isset( $_POST['aws_cc_enable'] ) ? esc_attr( $_POST['aws_cc_enable'] ) : '';
			$text_link = isset( $_POST['aws_cc_text_link'] ) ? esc_attr( $_POST['aws_cc_text_link'] ) : '';
			$enable_header = isset( $_POST['aws_cc_enable_header'] ) ? esc_attr( $_POST['aws_cc_enable_header'] ) : '';
			$pos_list = isset( $_POST['aws_cc_pos_list'] ) ? esc_attr( $_POST['aws_cc_pos_list'] ) : '';
			$post_content = isset( $_POST['aws_cc_pos_content'] ) ? esc_attr( $_POST['aws_cc_pos_content'] ) : '';

			if( $enable ) {
			update_option("aws_cc_enable", $enable);
			} else {
			delete_option("aws_cc_enable");
			}
			if( $text_link ) {
			update_option("aws_cc_text_link", $text_link);
			} else {
			delete_option("aws_cc_text_link");
			}
			if( $enable_header ) {
			update_option("aws_cc_enable_header", $enable_header);
			} else {
			delete_option("aws_cc_enable_header");
			}
			if( $pos_list ) {
			update_option("aws_cc_pos_list", $pos_list);
			} else {
			delete_option("aws_cc_pos_list");
			}
			if( $post_content ) {
			update_option("aws_cc_pos_content", $post_content);
			} else {
			delete_option("aws_cc_pos_content");
			}
		}
?>
	<div class="wrap">
		<?php screen_icon( 'aws-classipress-cartpauj' ); ?>
		<h2><?php _e( 'AWS ClassiPress Cartpauj', 'AwsCC' ); ?></h2>

<?php
	if(isset($_POST['form_submit'])) {
	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Settings was saved!','AwsCC').'</strong></p></div>';
	}
	?>

	<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Private Message Settings', 'AwsCC'); ?></h3>
            <div class="inside">

	<p><?php _e( 'A new page named <strong>My Messages</strong> has been created. To setup this new page with a link in the User Options menu go to Appearance -> Menus, create a new menu and name it eg. Dashboard, then go to Manage Locations and attach the new menu you just created to the Theme Dashboard and save. You now have a new customized User Options menu, ready to set up with all dashboard links and a link to My Messages page.', 'AwsCC' ); ?></p>

	<p><?php _e( 'If not done already, install <a style="text-decoration: none;" href="https://wordpress.org/plugins/cartpauj-pm/" target="_blank">Cartpauj PM</a> plugin. Send us an email if you want our customized version of Cartpauj PM.', 'AwsCC' ); ?></p>

	<div class="clear15"></div>

	<fieldset>
	<form name="option_form" method="post">

	<select name="aws_cc_enable" id="aws_cc_enable">
		<option value="true" <?php if (get_option("aws_cc_enable") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsCC'); ?></option>
		<option value="" <?php if (get_option("aws_cc_enable") == "false") echo 'selected'; ?>><?php _e('No', 'AwsCC'); ?></option>
	</select> <?php _e('Enable "Send PM" link in ads.', 'AwsCC'); ?>

	<div class="clear5"></div>

	<input type="text" name="aws_cc_text_link" id="aws_cc_text_link" value="<?php echo get_option("aws_cc_text_link"); ?>" size="40"> <?php _e('Text for your link, eg. Send PM.', 'AwsCC'); ?>

	<div class="clear10"></div>

	<input type="checkbox" name="aws_cc_pos_list" value="1" <?php if(get_option('aws_cc_pos_list') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned just above the "Listed: Date" in ad details.', 'AwsCC'); ?>

	<div class="clr"></div>

	<input type="checkbox" name="aws_cc_pos_content" value="1" <?php if(get_option('aws_cc_pos_content') == '1') echo 'checked="checked"'; ?>> <?php _e('Positioned in content area, just below the ad images.', 'AwsCC'); ?>

	<div class="clear10"></div>

	<select name="aws_cc_enable_header" id="aws_cc_enable_header">
		<option value="true" <?php if (get_option("aws_cc_enable_header") == "true") echo 'selected'; ?>><?php _e('Yes', 'AwsCC'); ?></option>
		<option value="" <?php if (get_option("aws_cc_enable_header") == "false") echo 'selected'; ?>><?php _e('No', 'AwsCC'); ?></option>
	</select> <?php _e('Enable "PMs (0)" in header, displays the number of new unread PMs.', 'AwsCC'); ?>

	<div class="clear15"></div>

	<?php _e('Please note that some child themes do not accept the "PMs (0)" in the header without modification on your end.', 'AwsCC'); ?>

	<div class="clear5"></div>

	<h4><?php _e('Finally, save your settings!', 'AwsCC'); ?></h4>

	<input type="submit" name="form_submit" value="<?php _e('Save Settings', 'AwsCC'); ?>" class="button-primary">

	</form>

	</fieldset>
           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'AwsCC'); ?></h3>
            <div class="inside">

    <table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'AwsCC'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'AwsCC'); ?></a></td>
    </tr>
    </tbody>
    </table>

<div class="clear15"></div>

<?php _e('<a style="text-decoration:none" href="http://www.arctic-websolutions.com" target="_blank"><strong>Arctic WebSolutions</strong></a> offers WordPress and ClassiPress plugins “as is” and with no implied meaning that they will function exactly as you would like or will be compatible with all 3rd party components and plugins. We do not offer support via email or otherwise support WordPress or other WordPress plugins we have not developed.', 'AwsCC'); ?>

<div class="clear15"></div>

<div class="admin_header"><?php _e( 'Other Plugins', 'AwsCC' ); ?></div>
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/bundle-pack/" target="_blank">Bundle Pack $143.90 off</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/cp-auction-social-sharing/" target="_blank">CP Auction & eCommerce</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-grid-view/" target="_blank">ClassiPress Grid View</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-projects-portfolio/" target="_blank">AWS Projects Portfolio
</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-affiliate-plugin/" target="_blank">AWS Affiliate Plugin</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-ecommerce/" target="_blank">ClassiPress eCommerce</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/simplepay4u-gateway/" target="_blank">SimplePay4u Gateway</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/grandchild-plugin/" target="_blank">AWS Grandchild Plugin</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-theme-customizer/" target="_blank">AWS Theme Customizer</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/best-offer-for-cp-auction/" target="_blank">CP Auction Best Offer</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-best-offer/" target="_blank">ClassiPress Best Offer</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-social-sharing/" target="_blank">ClassiPress Social Sharing</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-cartpauj-pm/" target="_blank">ClassiPress Cartpauj PM</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-deny-access/" target="_blank">AWS Deny Access</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-user-roles/" target="_blank">AWS User Roles</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-embed-video/" target="_blank">AWS Embed Video</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank">AWS Credit Payments</a>', 'AwsCC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">AppThemes Critic Reviews</a>', 'AwsCC' ); ?>

<div class="clear15"></div>

<?php _e('AppThemes has released a plugin <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> which has the same rating and review functionality as you can see around in their marketplace. The only minus is that it is not modded / available for ClassiPress Theme <strong>BUT</strong>, I have a modified version which you can review on our main site or any of our child theme demos.', 'AwsCC'); ?>

<div class="clear15"></div>

<?php _e('Purchase the <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> plugin through my <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">affiliate link</a>, send me a copy of your purchase receipt and i will send you my modified version of the Critic Reviews plugin.', 'AwsCC'); ?>

	<div class="clear15"></div>

	<p><strong><?php _e('Was this plugin useful? If you like it and it is/was useful, please consider donating.. Many thanks!', 'AwsCC'); ?></strong></p>

	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="RFJB58JUE8WUJ">
	<p style="text-align: center;">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	</p>
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>

            </div>
          </div>
        </div>
     </div>
</div>

<?php
}
?>