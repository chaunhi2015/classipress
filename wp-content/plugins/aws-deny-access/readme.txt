=== AWS Deny Access ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: classipress, classipress theme, security, deny, deny access, ip blocker
Requires at least: 3.9.2
Tested up to: 4.2.2
Stable tag: 1.0.5
License: GPL
License URI: http://www.gnu.org/licenses/gpl.html

---------------------------------------------------------------------------------------------------

=== Important links ===

* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://offer.wp-build.com) - Not open for backend testing.
* [Manuals & Support](http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Some Admin Features ===

* Super easy management of all settings.
* Deny access to pages / posts of your choice, set by user roles.
* Deny access to the single ad page if the visitor is not logged in.
* Deny access to pages / posts of your choice, if the visitor is not logged in.
* Block IP Addresses of your choice from reaching your website.

=== Some User Features ===

* No specific features

=== Installation ===

* [Instructions] (http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Recommended Translation ===

* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.