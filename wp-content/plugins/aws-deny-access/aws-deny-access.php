<?php

/*
Plugin Name: AWS Deny Access
Plugin URI: http://www.arctic-websolutions.com/
Description: The very best and most convenient way to add security to ClassiPress.
Version: 1.0.5
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

/*
Copyright 2013 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AWS_DA_VERSION' ) ) {
	define( 'AWS_DA_VERSION', '1.0.5' );
}

if ( ! defined( 'AWS_DA_LATEST_RELEASE' ) ) {
	define( 'AWS_DA_LATEST_RELEASE', '17 May 2015' );
}

if ( ! defined( 'AWS_DA_PLUGIN_FILE' ) ) {
	define( 'AWS_DA_PLUGIN_FILE', __FILE__ );
}

/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION - EASY TRANSLATION USING CODESTYLING LOCALIZATION
|--------------------------------------------------------------------------
*/

function aws_da_textdomain() {
	load_plugin_textdomain( 'AwsDA', false, dirname( plugin_basename( AWS_DA_PLUGIN_FILE ) ) . '/languages/' );
}
add_action( 'init', 'aws_da_textdomain' );


/*
|--------------------------------------------------------------------------
| ADD LINK TO SETTINGS MENU PAGE
|--------------------------------------------------------------------------
*/

function aws_da_add_link() {

	global $aws_da_admin_menu;
	$aws_da_admin_menu = add_menu_page( __( 'AWS Deny Access', 'AwsDA' ), __( 'Deny Access', 'AwsDA' ), 'manage_options', 'aws-deny-access', 'aws_deny_access_settings', plugins_url( 'aws-deny-access/css/images/aws-deny16.png' ), '6,4' );

	global $aws_da_settings;
	$aws_da_settings = add_submenu_page( 'aws-deny-access', __('Settings','AwsDA'), __('Settings','AwsDA'), 'manage_options', 'aws-deny-access', 'aws_deny_access_settings');

}
add_action( 'admin_menu', 'aws_da_add_link' );


/*
|--------------------------------------------------------------------------
| ADD SETTINGS LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'aws_da_action_links' );
function aws_da_action_links( $links ) {

return array_merge(
array(
'settings' => '<a href="' . get_bloginfo( 'url' ) . '/wp-admin/admin.php?page=aws-deny-access">'.__( 'Settings', 'AwsDA' ).'</a>'
),
$links
);
}


/*
|--------------------------------------------------------------------------
| ADD SUPPORT LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_row_meta', 'aws_da_meta_links', 10, 2 );
function aws_da_meta_links( $links, $file ) {

$plugin = plugin_basename(__FILE__);

// create link
if ( $file == $plugin ) {
return array_merge(
$links,
array( '<a href="http://www.arctic-websolutions.com/forums/" target="_blank">'.__( 'Support', 'AwsDA' ).'</a>' )
);
}
return $links;
}


/*
|--------------------------------------------------------------------------
| REGISTER ADMIN BACKEND / FRONTEND SCRIPTS AND STYLESHEETS
|--------------------------------------------------------------------------
*/

function aws_da_admin_menu_scripts() {

	wp_enqueue_style( 'aws-da-admin-style', plugins_url( 'aws-deny-access/css/admin.css'), false, '1.0.0' );

}
if ( is_admin() ) {
	add_action( 'admin_print_styles', 'aws_da_admin_menu_scripts' );
}

function aws_da_frontend_scripts() {

	wp_enqueue_style( 'aws-da-style', plugins_url( 'aws-deny-access/css/style.css'), false, '1.0.0' );

}
if ( !is_admin() ) {
	add_action('wp_head','aws_da_frontend_scripts');
}


/*
|--------------------------------------------------------------------------
| INCLUDES
|--------------------------------------------------------------------------
*/

include_once('includes/functions.php');


/*
|--------------------------------------------------------------------------
| AWS DENY ACCESS SETTINGS
|--------------------------------------------------------------------------
*/

function aws_deny_access_settings() {

	$msg = "";
	$ok = "";

	if(isset($_POST['save_access_info'])) {
		$single_ad = isset( $_POST['aws_da_access_single'] ) ? esc_attr( $_POST['aws_da_access_single'] ) : '';
		$page_ids = isset( $_POST['aws_da_access_pages'] ) ? esc_attr( $_POST['aws_da_access_pages'] ) : '';
		$access_title = isset( $_POST['aws_da_access_title'] ) ? esc_attr( $_POST['aws_da_access_title'] ) : '';
		$access_info = isset( $_POST['aws_da_access_info'] ) ? esc_attr( $_POST['aws_da_access_info'] ) : '';
		$blocked_roles = isset( $_POST['aws_da_blocked_roles'] ) ? esc_attr( $_POST['aws_da_blocked_roles'] ) : '';
		$access_posts = isset( $_POST['aws_da_access_posts'] ) ? esc_attr( $_POST['aws_da_access_posts'] ) : '';
		$roles_title = isset( $_POST['aws_da_roles_title'] ) ? esc_attr( $_POST['aws_da_roles_title'] ) : '';
		$roles_info = isset( $_POST['aws_da_roles_info'] ) ? esc_attr( $_POST['aws_da_roles_info'] ) : '';
		$blocked_ips = isset( $_POST['aws_da_blocked_ips'] ) ? esc_attr( $_POST['aws_da_blocked_ips'] ) : '';
		$redirect_url = isset( $_POST['aws_da_redirect_url'] ) ? esc_attr( $_POST['aws_da_redirect_url'] ) : '';

		if( $single_ad ) {
		update_option('aws_da_access_single',$single_ad);
		} else {
		delete_option('aws_da_access_single');
		}
		if( $page_ids ) {
		update_option('aws_da_access_pages',$page_ids);
		} else {
		delete_option('aws_da_access_pages');
		}
		if( $access_title ) {
		update_option('aws_da_access_title',$access_title);
		} else {
		delete_option('aws_da_access_title');
		}
		if( $access_info ) {
		update_option('aws_da_access_info',stripslashes(html_entity_decode($access_info)));
		} else {
		delete_option('aws_da_access_info');
		}
		if( $blocked_roles ) {
		update_option('aws_da_blocked_roles',$blocked_roles);
		} else {
		delete_option('aws_da_blocked_roles');
		}
		if( $access_posts ) {
		update_option('aws_da_access_posts',$access_posts);
		} else {
		delete_option('aws_da_access_posts');
		}
		if( $roles_title ) {
		update_option('aws_da_roles_title',$roles_title);
		} else {
		delete_option('aws_da_roles_title');
		}
		if( $roles_info ) {
		update_option('aws_da_roles_info',stripslashes(html_entity_decode($roles_info)));
		} else {
		delete_option('aws_da_roles_info');
		}
		if( $blocked_ips ) {
		update_option('aws_da_blocked_ips',$blocked_ips);
		} else {
		delete_option('aws_da_blocked_ips');
		}
		if( $redirect_url ) {
		update_option('aws_da_redirect_url',$redirect_url);
		} else {
		delete_option('aws_da_redirect_url');
		}
		$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Access settings was updated!','AwsDA').'</strong></p></div>';
		}

?>
	<div class="wrap">
		<?php screen_icon( 'aws-deny-access' ); ?>
		<h2><?php _e( 'AWS Deny Access', 'AwsDA' ); ?></h2>

	<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Deny Access Settings', 'AwsDA'); ?></h3>
            <div class="inside">

	<div class="clear10"></div>
	<div class="admin_header"><?php _e('Deny Access to Pages of Your Choice', 'AwsDA'); ?></div>
	<?php _e('Use the options below to deny access to pages of your choice on your website if the visitor is not logged in.', 'AwsDA'); ?>

<form method="post" action="">

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Single Ad Page:', 'AwsDA'); ?></th>
						<td><select name="aws_da_access_single"><?php echo aws_da_yes_no(get_option('aws_da_access_single')); ?></select> <?php _e('Deny access to the single ad page if the guest are not logged in.', 'AwsDA'); ?><br /><small><?php _e('Set this option to Yes if you want to deny access to the single ad page if the guest are not logged in. This option will deny access to all posts.', 'AwsDA'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Pages / Posts by ID:', 'AwsDA'); ?></th>
						<td><input type="text" name="aws_da_access_pages" value="<?php echo get_option('aws_da_access_pages'); ?>"> <?php _e('Enter the Page/Post IDs as comma separated list, eg. 9,10,14', 'AwsDA'); ?><br /><small><?php _e('This option is used to deny access to any standard Page or Post, by Page/Post ID.', 'AwsDA'); ?> <?php _e('Enter any page/post id wich you want to deny access to if the visitor is not logged in.', 'AwsDA'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Page Title:', 'AwsDA'); ?></th>
						<td><input type="text" name="aws_da_access_title" value="<?php echo get_option('aws_da_access_title'); ?>" size="40"> <?php _e('Enter a title for the page which visitors are redirected to.', 'AwsDA'); ?><br /><small><?php _e('Title could be something like: Please Log in to access this page.', 'AwsDA'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Page Information:', 'AwsDA'); ?></th>
						<td><textarea class="options" name="aws_da_access_info" rows="5" cols="80"><?php echo get_option('aws_da_access_info'); ?></textarea><br /><small><?php _e('Enter an explanatory text that will be visible on the login page. HTML can be used.', 'AwsDA'); ?></small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear20"></div>

<div class="admin_header"><?php _e('Deny Access by User Roles', 'AwsDA'); ?></div>
<?php _e('Use the options below to deny access to the different pages on your site by user roles.', 'AwsDA'); ?>

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('User Roles:', 'AwsDA'); ?></th>
						<td><select name="aws_da_blocked_roles"><option value=""><?php _e('Select Role', 'AwsDA'); ?></option><?php wp_dropdown_roles(get_option('aws_da_blocked_roles')); ?></select> <?php _e('Select the user role that you want to deny access.', 'AwsDA'); ?></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Pages / Posts by ID:', 'AwsDA'); ?></th>
						<td><input type="text" name="aws_da_access_posts" value="<?php echo get_option('aws_da_access_posts'); ?>"> <?php _e('Enter the Page/Post IDs as comma separated list, eg. 9,10,14', 'AwsDA'); ?><br /><small><?php _e('This option is used to deny access to any standard Page or Post, by Page/Post ID.', 'AwsDA'); ?> <?php _e('Enter any page/post id wich you want to deny access to on your site. Block users by user roles.', 'AwsDA'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Page Title:', 'AwsDA'); ?></th>
						<td><input type="text" name="aws_da_roles_title" value="<?php echo get_option('aws_da_roles_title'); ?>" size="40"> <?php _e('Enter a title for the page which users are redirected to.', 'AwsDA'); ?></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Page Information:', 'AwsDA'); ?></th>
						<td><textarea class="options" name="aws_da_roles_info" rows="5" cols="80"><?php echo get_option('aws_da_roles_info'); ?></textarea><br><small><?php _e('Enter an explanatory text that will be visible on the login page. HTML can be used.', 'AwsDA'); ?></small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear20"></div>

<div class="admin_header"><?php _e('Block IP Addresses from Reaching Your Site', 'AwsDA'); ?></div>
<?php _e('Use the options below to block any IP address of your choice.', 'AwsDA'); ?>

			<table class="form-table">
				<tbody>

					<tr valign="top">	
						<th scope="row" valign="top"><?php _e('Block IP Addresses:', 'AwsDA'); ?></th>
						<td><textarea class="options" name="aws_da_blocked_ips" rows="5" cols="80"><?php echo get_option('aws_da_blocked_ips'); ?></textarea><br><small><?php _e('Enter the IP addresses in a line separated with comma, eg. 111.111.111,222.222.222,333.333.333', 'AwsDA'); ?></small></td>
					</tr>

					<tr valign="top">	
						<th scope="row" valign="top"><a href="#" title="<?php _e('Enter full URL to which you want to forward those who have had their IP address blocked.', 'AwsDA'); ?>"><div class="helpico"></div></a><?php _e('URL Forwarding:', 'AwsDA'); ?></th>
						<td><input type="text" name="aws_da_redirect_url" value="<?php echo get_option('aws_da_redirect_url'); ?>" size="40"> <?php _e('Full URL to which you want to forward blocked visits.', 'AwsDA'); ?><br /><small><?php _e('Enter a complete URL to which you want to forward those who have had their IP address blocked', 'AwsDA'); ?>, eg. http://www.google.com/ (<?php _e('<strong>IMPORTANT</strong> Do NOT use any URL within your ClassiPress website.', 'AwsDA'); ?>)</small></td>
					</tr>

				</tbody>
			</table>

	<div class="clear10"></div>

	<strong><?php _e('Here’s how you can find the Page / Post IDs', 'AwsDA'); ?></strong><br />
	<?php _e('After you’ve logged into your WordPress dashboard, Go to Manage > Pages or Ads and from the list of Pages / Ads hover over the Page / Ad title you want to find the ID of. Every time you hover over, your browser’s status bar will show you a URL ending with a number like this http://www.your-domain.com/wp-admin/post.php?post=3648&action=edit. The number with which is in the URL is the Page / Post ID (3654 in this case).', 'AwsDA'); ?>
<br /><br />
	<?php _e('If you don’t see anything when you hover over, click on the Page link to open edit Page screen. Now, look in your browser’s address bar and you’ll find the URL with a number. and that number is your Page / Post ID.', 'AwsDA'); ?>

	<div class="clear10"></div>

	<p class="submit"><input type="submit" name="save_access_info" id="submit" class="button-primary" value="<?php _e('Save Settings', 'AwsDA'); ?>"  /></p>
 
		</form>

	<div class="clear10"></div>
           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'auctionAdmin'); ?></h3>
            <div class="inside">

<table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://dev.wp-build.com/wp-content/plugins/aws-deny-access/change-log.txt" onclick="javascript:void window.open('http://dev.wp-build.com/wp-content/plugins/aws-deny-access/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'auctionAdmin'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'auctionAdmin'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'auctionAdmin'); ?></a></td>
    </tr>
    </tbody>
    </table>

<div class="clear15"></div>

<?php _e('<a style="text-decoration:none" href="http://www.arctic-websolutions.com" target="_blank"><strong>Arctic WebSolutions</strong></a> offers WordPress and ClassiPress plugins “as is” and with no implied meaning that they will function exactly as you would like or will be compatible with all 3rd party components and plugins. We do not offer support via email or otherwise support WordPress or other WordPress plugins we have not developed.', 'AwsDA'); ?>

<div class="clear15"></div>

<div class="admin_header"><?php _e( 'Other Plugins', 'AwsDA' ); ?></div>
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/bundle-pack/" target="_blank">Bundle Pack $143.90 off</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/cp-auction-social-sharing/" target="_blank">CP Auction & eCommerce</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-grid-view/" target="_blank">ClassiPress Grid View</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-projects-portfolio/" target="_blank">AWS Projects Portfolio
</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-affiliate-plugin/" target="_blank">AWS Affiliate Plugin</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-ecommerce/" target="_blank">ClassiPress eCommerce</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/simplepay4u-gateway/" target="_blank">SimplePay4u Gateway</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/grandchild-plugin/" target="_blank">AWS Grandchild Plugin</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-theme-customizer/" target="_blank">AWS Theme Customizer</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/best-offer-for-cp-auction/" target="_blank">CP Auction Best Offer</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-best-offer/" target="_blank">ClassiPress Best Offer</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-social-sharing/" target="_blank">ClassiPress Social Sharing</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-cartpauj-pm/" target="_blank">ClassiPress Cartpauj PM</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-deny-access/" target="_blank">AWS Deny Access</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-user-roles/" target="_blank">AWS User Roles</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-embed-video/" target="_blank">AWS Embed Video</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank">AWS Credit Payments</a>', 'AwsDA' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">AppThemes Critic Reviews</a>', 'AwsDA' ); ?>

<div class="clear15"></div>

<?php _e('AppThemes has released a plugin <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> which has the same rating and review functionality as you can see around in their marketplace. The only minus is that it is not modded / available for ClassiPress Theme <strong>BUT</strong>, I have a modified version which you can review on our main site or any of our child theme demos.', 'AwsDA'); ?>

<div class="clear15"></div>

<?php _e('Purchase the <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> plugin through my <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">affiliate link</a>, send me a copy of your purchase receipt and i will send you my modified version of the Critic Reviews plugin.', 'AwsDA'); ?>

	<div class="clear15"></div>

	<p><strong><?php _e('Was this plugin useful? If you like it and it is/was useful, please consider donating.. Many thanks!', 'AwsDA'); ?></strong></p>

	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="RFJB58JUE8WUJ">
	<p style="text-align: center;">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	</p>
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>

            </div>
          </div>
        </div>
</div>
</div>

<?php
}
?>