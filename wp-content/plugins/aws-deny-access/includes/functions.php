<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| SIMPLE IP BLOCKER CODE
|--------------------------------------------------------------------------
*/

function aws_da_block_ip() {

$list = get_option('aws_da_blocked_ips');
$url = get_option('aws_da_redirect_url');
$deny = explode(',', $list);
if (in_array ($_SERVER['REMOTE_ADDR'], $deny)) {
   if( $url ) {
   header("location: ".$url."");
   exit();
   } else {
   header("location: http://www.google.com/");
   exit();
   }
   }
}
add_action('appthemes_init', 'aws_da_block_ip');


/*
|--------------------------------------------------------------------------
| GET USER ROLE BY USER ID
|--------------------------------------------------------------------------
*/

function aws_da_get_user_role( $user_id ) {
    $user = get_userdata( $user_id );
    return empty( $user ) ? array() : $user->roles;
}


/*
|--------------------------------------------------------------------------
| GET YES / NO DROPDOWN
|--------------------------------------------------------------------------
*/

function aws_da_yes_no( $select = '' ) {

$options = "";
$what = array(
		'no' => __('No','AwsDA'),
		'yes' => __('Yes','AwsDA')
		);

	foreach($what as $what_id=>$yes_no) {
	if($select == $what_id) {
	$sel = 'selected="selected"';
	$options .= '<option '.$sel.' value="'.$what_id.'">'.$yes_no.'</option>';
	} else {
	$options .= '<option value="'.$what_id.'">'.$yes_no.'</option>';
	}
	}
		return $options;
}


/*
|--------------------------------------------------------------------------
| ACTION NO ACCESS TO CHOOSEN CONTENTS OR PAGES BY VISITORS
|--------------------------------------------------------------------------
*/


function aws_da_no_access_out() {

	global $cp_auction;

$single = get_option('aws_da_access_single');
$pages = get_option('aws_da_access_pages');
$pageid = get_the_ID();
$ids = explode(',', $pages);

$page_id = false;
if (in_array($pageid, $ids)) {
	$page_id = "yes";
}

if (( $single == "yes" && is_singular( APP_POST_TYPE ) && !is_user_logged_in() ) || ( $page_id == "yes" && !is_user_logged_in() )) { ?>

<div class="shadowblock_out">

				<div class="shadowblock">

					<h2 class="dotted"><span class="colour"><?php echo get_option('aws_da_access_title'); ?></span></h2>

					<?php if( get_option('aws_da_access_info') ) { ?>
					<div class="notice success"><?php echo htmlentities(nl2br(get_option('aws_da_access_info'))); ?></div>
					<?php } ?>

					<div class="clear10"></div>

					<div class="padd10">
					<p><?php _e( 'Please complete the fields below to login to your account.', 'AwsDA' ); ?></p>
					</div>

						<form action="<?php echo wp_login_url(); ?>" method="post" class="loginform" id="login-form">
		
							<p>
								<label for="login_username"><?php _e( 'Username:', 'AwsDA' ); ?></label>
								<input type="text" class="text required" name="log" id="login_username" value="<?php if (isset($posted['login_username'])) echo esc_attr($posted['login_username']); ?>" />
							</p>

							<p>
								<label for="login_password"><?php _e( 'Password:', 'AwsDA' ); ?></label>
								<input type="password" class="text required" name="pwd" id="login_password" value="" />
							</p>

							<div class="clr"></div>

							<div id="checksave">

								<p class="rememberme">
									<input name="rememberme" class="checkbox" id="rememberme" value="forever" type="checkbox" checked="checked" />
									<label for="rememberme"><?php _e( 'Remember me', 'AwsDA' ); ?></label>
								</p>

								<p class="submit">
									<input type="submit" class="btn_orange" name="login" id="login" value="<?php _e( 'Login &raquo;', 'AwsDA' ); ?>" />
									<?php echo APP_Login::redirect_field(); ?>
									<input type="hidden" name="testcookie" value="1" />
								</p>

								<p class="lostpass">
									<a class="lostpass" href="<?php echo appthemes_get_password_recovery_url(); ?>" title="<?php _e( 'Password Lost and Found', 'AwsDA' ); ?>"><?php _e( 'Lost your password?', 'AwsDA' ); ?></a>
								</p>

								<?php wp_register('<p class="register">','</p>'); ?>

								<?php do_action('login_form'); ?>

							</div>

						</form>

						<!-- autofocus the field -->
						<script type="text/javascript">try{document.getElementById('login_username').focus();}catch(e){}</script>

					<div class="clr"></div>

				</div><!-- /shadowblock -->

			</div><!-- /shadowblock_out -->

			<?php if( $cp_auction['child_theme'] == "twinpress_classifieds") { ?>
            		</div><!-- /conl -->
			<?php } ?>

			</div><!-- /content_left -->

			<?php get_sidebar( 'page' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->

		<?php appthemes_before_footer(); ?>
		<?php get_footer( app_template_base() ); ?>
		<?php appthemes_after_footer(); ?>

<?php 
$stylesheet = strtolower(get_option('stylesheet'));
if(( $stylesheet == "citrus-night" ) || ( $stylesheet == "eldorado") || ( $stylesheet == "Phoenix_2_1" ) || ( $stylesheet == "greenymarketplace" )) { ?>
	</div><!-- /wrapper -->
	<?php } ?>

<?php wp_footer(); ?>

	<?php appthemes_after(); ?>

</body>

</html>

<?php
exit;
}
}
add_action('appthemes_before_loop', 'aws_da_no_access_out');
add_action('appthemes_before_page_loop', 'aws_da_no_access_out');
add_action('appthemes_before_blog_loop', 'aws_da_no_access_out');


/*
|--------------------------------------------------------------------------
| ACTION NO ACCESS TO CHOOSEN CONTENTS OR PAGES BY LOGGED IN USERS
|--------------------------------------------------------------------------
*/

function aws_da_no_access_in() {

	global $current_user, $cp_auction;
    	get_currentuserinfo();
    	$uid = $current_user->ID;
    	$array = aws_da_get_user_role($uid);

$roles = get_option('aws_da_blocked_roles');
$posts = get_option('aws_da_access_posts');
$pageid = get_the_ID();
$ids = explode(',', $posts);

$page_id = false;
if (in_array($pageid, $ids)) {
	$page_id = "yes";
}

$blocked = false;
if (in_array($roles, $array)) {
	$blocked = "yes";
}

if ( $page_id == "yes" && $blocked == "yes" && is_user_logged_in() ) { ?>

<div class="shadowblock_out">

				<div class="shadowblock">

					<h2 class="dotted"><span class="colour"><?php echo get_option('aws_da_roles_title'); ?></span></h2>

					<?php if( get_option('aws_da_access_info') ) { ?>
					<div class="notice success"><?php echo nl2br(get_option('aws_da_roles_info')); ?></div>
					<?php } ?>

					<div class="clear10"></div>

					<div class="padd10">
					<p><?php _e( 'Please complete the fields below to login to your account.', 'AwsDA' ); ?></p>
					</div>

						<form action="<?php echo wp_login_url(); ?>" method="post" class="loginform" id="login-form">
		
							<p>
								<label for="login_username"><?php _e( 'Username:', 'AwsDA' ); ?></label>
								<input type="text" class="text required" name="log" id="login_username" value="<?php if (isset($posted['login_username'])) echo esc_attr($posted['login_username']); ?>" />
							</p>

							<p>
								<label for="login_password"><?php _e( 'Password:', 'AwsDA' ); ?></label>
								<input type="password" class="text required" name="pwd" id="login_password" value="" />
							</p>

							<div class="clr"></div>

							<div id="checksave">

								<p class="rememberme">
									<input name="rememberme" class="checkbox" id="rememberme" value="forever" type="checkbox" checked="checked" />
									<label for="rememberme"><?php _e( 'Remember me', 'AwsDA' ); ?></label>
								</p>

								<p class="submit">
									<input type="submit" class="btn_orange" name="login" id="login" value="<?php _e( 'Login &raquo;', 'AwsDA' ); ?>" />
									<?php echo APP_Login::redirect_field(); ?>
									<input type="hidden" name="testcookie" value="1" />
								</p>

								<p class="lostpass">
									<a class="lostpass" href="<?php echo appthemes_get_password_recovery_url(); ?>" title="<?php _e( 'Password Lost and Found', 'AwsDA' ); ?>"><?php _e( 'Lost your password?', 'AwsDA' ); ?></a>
								</p>

								<?php wp_register('<p class="register">','</p>'); ?>

								<?php do_action('login_form'); ?>

							</div>

						</form>

						<!-- autofocus the field -->
						<script type="text/javascript">try{document.getElementById('login_username').focus();}catch(e){}</script>

					<div class="clr"></div>

				</div><!-- /shadowblock -->

			</div><!-- /shadowblock_out -->

			<?php if( $cp_auction['child_theme'] == "twinpress_classifieds") { ?>
            		</div><!-- /conl -->
			<?php } ?>

			</div><!-- /content_left -->

			<?php get_sidebar( 'page' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->

		<?php appthemes_before_footer(); ?>
		<?php get_footer( app_template_base() ); ?>
		<?php appthemes_after_footer(); ?>

<?php 
$stylesheet = strtolower(get_option('stylesheet'));
if(( $stylesheet == "citrus-night" ) || ( $stylesheet == "eldorado") || ( $stylesheet == "Phoenix_2_1" ) || ( $stylesheet == "greenymarketplace" )) { ?>
	</div><!-- /wrapper -->
	<?php } ?>

<?php wp_footer(); ?>

	<?php appthemes_after(); ?>

</body>

</html>

<?php
exit;
}
}
add_action('appthemes_before_loop', 'aws_da_no_access_in');
add_action('appthemes_before_page_loop', 'aws_da_no_access_in');
add_action('appthemes_before_blog_loop', 'aws_da_no_access_in');