<?php
/*
Classipress Rate Authors PRO
Current Version: 4.0
Plugin Author: Julio Gallegos
Author URL: http://wprabbits.com
*/


function register_cprate_settings() {
//register our settings
	
	/* Admin Options */
	register_setting('cprate-settings-group','cprate_trk_ip');
	register_setting('cprate-settings-group','cprate_req_feedback_mod');
	register_setting('cprate-settings-group','cprate_req_feedback_mod_msg');
	register_setting('cprate-settings-group','cprate_admin_note');
	register_setting('cprate-settings-group','cprate_admin_note_email');
	
	/* Author Page Options */
	register_setting('cprate-settings-group','cprate_enable_legend');
	register_setting('cprate-settings-group','cprate_date_format');
	register_setting('cprate-settings-group','cprate_multi_feedback');
	register_setting('cprate-settings-group','cprate_enable_replyback');
	register_setting('cprate-settings-group','cprate_rules');
	register_setting('cprate-settings-group','cprate_enable_edit_feedback');
	register_setting('cprate-settings-group','cprate_poster_edit_time');
	register_setting('cprate-settings-group','cprate_enable_edit_feedback_msg');
	register_setting('cprate-settings-group','cprate_edited_feedback_msg');

	/* Advanced Options */
	register_setting('cprate-settings-group','cprate_deletedb');
	
	/* Plugin Title Headers */
	register_setting('cprate-settings-group','cprate_header1');
	register_setting('cprate-settings-group','cprate_header2');
	
	/* Plugin Table Headers */
	register_setting('cprate-settings-group','cprate_rating');
	register_setting('cprate-settings-group','cprate_feedback');
	register_setting('cprate-settings-group','cprate_ratedby');
	register_setting('cprate-settings-group','cprate_date');
	register_setting('cprate-settings-group','cprate_sub_comment');
	register_setting('cprate-settings-group','cprate_reply');
    register_setting('cprate-settings-group','cprate_showmore');
    register_setting('cprate-settings-group','cprate_showall');
	
	/* Plugin Form Fields */
	register_setting('cprate-settings-group','cprate_button');
	register_setting('cprate-settings-group','cprate_cancel');
	register_setting('cprate-settings-group','cprate_save');
	register_setting('cprate-settings-group','cprate_submit');
	register_setting('cprate-settings-group','cprate_rating_msg');
	register_setting('cprate-settings-group','cprate_comnt_msg');
	register_setting('cprate-settings-group','cprate_thanks');
	register_setting('cprate-settings-group','cprate_submit_msg');
	register_setting('cprate-settings-group','cprate_editsaved_msg');
	register_setting('cprate-settings-group','cprate_edit_exp_msg');
	register_setting('cprate-settings-group','cprate_five_star');
	register_setting('cprate-settings-group','cprate_four_star');
	register_setting('cprate-settings-group','cprate_three_star');
	register_setting('cprate-settings-group','cprate_two_star');
	register_setting('cprate-settings-group','cprate_one_star');
	
	
	/* Moderate Feedback Form Fields */
	register_setting('cprate-mod-settings-group','cprate_mod_records_show');
	register_setting('cprate-mod-settings-group','cprate_mod_sort_order');
	register_setting('cprate-mod-settings-group','cprate_show_mod_btn');
	/* Feedback Reports Form Fields */
	register_setting('cprate-toprate-settings-group','cprate_records_show_toprate');
	register_setting('cprate-toprate-settings-group','cprate_sort_toprate');	
}

function cprate_setup_options() {
global $rules_txt,$cprate_db_version;
	/* Plugin Options */
	if (get_option('cprate_rules') == '') update_option('cprate_rules', $rules_txt);
   	if (get_option('cprate_db_version') == '') update_option('cprate_db_version',$cprate_db_version);
	
	/* Admin Options */
    if (get_option('cprate_req_feedback_mod_msg') == '') update_option('cprate_req_feedback_mod_msg', 'This Feedback is awaiting Moderation');
    if (get_option('cprate_admin_note_email') == '') update_option('cprate_admin_note_email', get_option('admin_email'));
	
	/* Author Page Options */
    if (get_option('cprate_date_format') == '') update_option('cprate_date_format', 'Y-m-d');
    if (get_option('cprate_poster_edit_time') == '') update_option('cprate_poster_edit_time', 5);
    if (get_option('cprate_edited_feedback_msg') == '') update_option('cprate_edited_feedback_msg', 'edited');
		
	/* Plugin Title Headers */
    if (get_option('cprate_header1') == '') update_option('cprate_header1', 'Overall Rating');
    if (get_option('cprate_header2') == '') update_option('cprate_header2', 'Total Ratings');
	
	/* Plugin Table Headers */
    if (get_option('cprate_rating') == '') update_option('cprate_rating', 'Rating');
    if (get_option('cprate_feedback') == '') update_option('cprate_feedback', 'Feedback');
    if (get_option('cprate_ratedby') == '') update_option('cprate_ratedby', 'Rated By');
    if (get_option('cprate_date') == '') update_option('cprate_date', 'Date');
    if (get_option('cprate_sub_comment') == '') update_option('cprate_sub_comment', 'Reply back from Author');
    if (get_option('cprate_reply') == '') update_option('cprate_reply', 'Reply');
    if (get_option('cprate_showmore') == '') update_option('cprate_showmore', 'Show More');
    if (get_option('cprate_showall') == '') update_option('cprate_showall', 'Show All');
	
    	
	/* Plugin Form Fields */
	if (get_option('cprate_button') == '') update_option('cprate_button', 'Leave Feedback');
    if (get_option('cprate_cancel') == '') update_option('cprate_cancel', 'Cancel');
    if (get_option('cprate_save') == '') update_option('cprate_save', 'Save');
    if (get_option('cprate_submit') == '') update_option('cprate_submit', 'Submit');
    if (get_option('cprate_rating_msg') == '') update_option('cprate_rating_msg', 'Your Rating');
    if (get_option('cprate_comnt_msg') == '') update_option('cprate_comnt_msg', 'Feedback Comments');
    if (get_option('cprate_thanks') == '') update_option('cprate_thanks', 'Thank You!');
    if (get_option('cprate_submit_msg') == '') update_option('cprate_submit_msg', 'Submitting Feedback...');
    if (get_option('cprate_editsaved_msg') == '') update_option('cprate_editsaved_msg', 'Your Feedback has been updated!');
    if (get_option('cprate_edit_exp_msg') == '') update_option('cprate_edit_exp_msg', 'The time limit to edit this Feedback has expired');
	if (get_option('cprate_five_star') == '') update_option('cprate_five_star', '5 stars');
	if (get_option('cprate_four_star') == '') update_option('cprate_four_star', '4 stars');
	if (get_option('cprate_three_star') == '') update_option('cprate_three_star', '3 stars');
	if (get_option('cprate_two_star') == '') update_option('cprate_two_star', '2 stars');
	if (get_option('cprate_one_star') == '') update_option('cprate_one_star', '1 stars');
	
	/* Moderate Feedback Form Fields */
    if (get_option('cprate_mod_records_show') == '') update_option('cprate_mod_records_show', 10);
    if (get_option('cprate_mod_sort_order') == '') update_option('cprate_mod_sort_order', "default");
    if (get_option('cprate_records_show_toprate') == '') update_option('cprate_records_show_toprate', 10);
    if (get_option('cprate_sort_toprate') == '') update_option('cprate_sort_toprate', "default");		
}

function cprate_plugin_menu() {
	add_menu_page( 'CP Rate Author Settings', 'CP Rate Author', 'manage_options', 'cprate-settings-page', 'cprate_plugin_options', plugins_url('/css/images/full-star-small.png' , dirname(__FILE__) ) );	
	add_submenu_page('cprate-settings-page', __('Settings', 'cprate'), __('Settings', 'cprate'), "manage_options", 'cprate-settings-page', "cprate_plugin_options");
	add_submenu_page("cprate-settings-page", __('Moderate Feedback', 'cprate'), __('Moderate', 'cprate'), "manage_options", "moderate-feedback", "cprate_plugin_feedbacktable");
	add_submenu_page("cprate-settings-page", __('Highest Rated', 'cprate'), __('Highest Rated', 'cprate'), "manage_options", "highest-rated", "cprate_plugin_highestrated");
	wp_register_style( 'cprate-admin-css', plugins_url('/cprate-admin.css', __FILE__) );
	wp_register_style( 'cprate-admin-css-boot', plugins_url('/bootstrap-switch.css', __FILE__) );
	wp_register_script('cprate_admin_js', plugins_url('/cprate-admin.js', __FILE__) , array('jquery'), '2.50', true);
	wp_register_script('cprate_admin_boot', plugins_url('/bootstrap-switch.min.js', __FILE__) , array('jquery'), '2.50', true);
	wp_localize_script('cprate_admin_js', 'cpRateVars', 
		array(
			'cprate_ajax' => admin_url('admin-ajax.php'),
			'edit_feedback_nonce' => wp_create_nonce( 'myajax_edit_feedback_nonce' ),
			'edit_sub_feedback_nonce' => wp_create_nonce( 'myajax_edit_sub_feedback_nonce' )
	));
	add_action( 'admin_init', 'register_cprate_settings' );
	add_action( 'admin_init', 'cprate_setup_options' );
}

add_action('admin_enqueue_scripts', 'cprate_plugin_admin_scripts');  
      
function cprate_plugin_admin_scripts() {  
	// Include JS/CSS only if we're on our options page  
	if (cprate_plugin_screen()) { 
		wp_enqueue_style( 'cprate-admin-css' );
		wp_enqueue_style( 'cprate-admin-css-boot' );
		wp_enqueue_script( 'cprate_admin_js' );
		wp_enqueue_script( 'cprate_admin_boot' );
	}
} 
 
// Check if we're on our options page  
function cprate_plugin_screen() {  
	$screen = get_current_screen();  
	if (is_object($screen) && ($screen->id == 'toplevel_page_cprate-settings-page' || $screen->id == 'cp-rate-author_page_moderate-feedback' || $screen->id == 'cp-rate-author_page_highest-rated')) {  
		return true;  
	} else {  
		return false;  
	}  
}

function cprate_top_rated($id) {
	$ratecal = cprate_calculate($id);
	$count = round($ratecal);
	$rateperc = $ratecal/5 * 100;
	return print_all_stars($count).'(<span id="cprate_per">'.$rateperc. '%</span>)';
}


function delete_feedback() {
	global $wpdb;
		$id = $_POST['row_id'];
		$wpdb->query("DELETE FROM $wpdb->prefix" . "rate_users WHERE id =" .$id);
	exit;
}
add_action('wp_ajax_cprate_delete_feedback', 'delete_feedback');

function approve_feedback() {
	global $wpdb;
		$id = $_POST['row_id'];
		$wpdb->query("UPDATE $wpdb->prefix" . "rate_users SET feedback_moderate=0 WHERE id =" .$id);
	exit;

}
add_action('wp_ajax_cprate_approve_feedback', 'approve_feedback');

function cprate_plugin_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

?>
<div class="wrap" id="cprate_wrapper">
<h2><?php echo "CP Rate Author Settings Page"; ?></h2>
<form method="post" action="options.php">
<?php 	
	settings_fields( 'cprate-settings-group' );
    do_settings_sections( 'cprate-settings-group' ); 
	$checked = ' checked="checked" ';
	if( isset($_GET['settings-updated']) ) { ?>
        <div id="message" class="updated">
            <p><strong><?php _e('Settings saved.') ?></strong></p>
        </div>
<?php } ?>
	<div class="cp-tabs">
         <ul class="tab-links">
            <li class="active"><a href="#tab1">General</a></li>
            <li><a href="#tab2">Field Text</a></li>
            <li><a href="#tab3">Advanced</a></li>
        </ul>
    </div>
	<div class="cptab-content active-cptab" id="tab1">
        <h3 class="cprate-header"><?php echo __("Admin Options", "cprate") ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row" class="wide" ><?php echo __("Track User IP Address:", "cprate") ?></th>
                <td>
                	<input class="boot" type="checkbox" name="cprate_trk_ip" <?php if(get_option('cprate_trk_ip')) echo $checked  ?>  />
                </td>
            </tr>
			<tr valign="top">
                <th scope="row" class="wide" ><?php echo __("Require Feedback to be Moderated by Site Admin:", "cprate") ?></th>
                <td>
                	<input class="boot opt-box" type="checkbox" name="cprate_req_feedback_mod" <?php if(get_option('cprate_req_feedback_mod')) echo $checked  ?>  />
                    <label style="padding:0 0 0 15px" for="cprate_req_feedback_mod_msg"><?php echo __("Message to display: ", "cprate") ?></label>
                    <input size="35" type="text" name="cprate_req_feedback_mod_msg" value="<?php echo get_option('cprate_req_feedback_mod_msg'); ?>" />
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="wide" ><?php echo __("Send Notifications to Site Admin when Moderation is needed on new Feedback: ", "cprate") ?>
                <span style="color:blue;display:block">(Moderation must be Turned ON)</span>
                </th>
                <td>
                	<input class="boot opt-box" type="checkbox" name="cprate_admin_note" <?php if(get_option('cprate_admin_note')) echo $checked  ?>  />
                    <label style="padding:0 30px 0 15px" for="cprate_admin_note_email"><?php echo __("Email Address: ", "cprate") ?></label>
                    <input size="35" type="text" name="cprate_admin_note_email" value="<?php echo get_option('cprate_admin_note_email'); ?>" />
                </td>
            </tr>    	
        </table>
        <h3 class="cprate-header"><?php echo __("Author Page Options", "cprate") ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row" class="wide" ><?php echo __("Show Ratings Legend:", "cprate") ?></th> 			
                <td><input class="boot cp_box" type="checkbox" name="cprate_enable_legend" <?php if(get_option('cprate_enable_legend')) echo $checked  ?>  /></td>
            </tr>
			<tr valign="top">
			    <th scope="row" class="wide" ><?php echo __("Set Date Format:", "cprate") ?></th> 			
			    <td>
			    	<label style="padding-bottom: 10px;" title="Y-m-d"><input name="cprate_date_format" type="radio" value="Y-m-d"  <?php if(get_option('cprate_date_format') == 'Y-m-d') echo $checked  ?>> 2015-11-26</label><br>
			    	<label style="padding-bottom: 10px;" title="Y-d-m"><input name="cprate_date_format" type="radio" value="Y-d-m"  <?php if(get_option('cprate_date_format') == 'Y-d-m') echo $checked  ?>> 2015-26-11</label><br>
					<label style="padding-bottom: 10px;" title="m-d-Y"><input name="cprate_date_format" type="radio" value="m-d-Y"  <?php if(get_option('cprate_date_format') == 'm-d-Y') echo $checked  ?>> 11-26-2015</label><br>
					<label style="padding-bottom: 10px;" title="d-m-Y"><input name="cprate_date_format" type="radio" value="d-m-Y"  <?php if(get_option('cprate_date_format') == 'd-m-Y') echo $checked  ?>> 26-11-2015</label><br>
			    	<label style="padding-bottom: 10px;" title="m/d/Y"><input name="cprate_date_format" type="radio" value="m/d/Y"  <?php if(get_option('cprate_date_format') == 'm/d/Y') echo $checked  ?>> 11/26/2015</label><br>
			    	<label style="padding-bottom: 10px;" title="d/m/Y"><input name="cprate_date_format" type="radio" value="d/m/Y"  <?php if(get_option('cprate_date_format') == 'd/m/Y') echo $checked  ?>> 26/11/2015</label><br>
			    </td>
			</tr>
            <tr valign="top">
                <th scope="row" class="wide" ><?php echo __("Allow users to post more than 1 Feedback per Author:", "cprate") ?></th>			
                <td><input class="boot cp_box" type="checkbox" name="cprate_multi_feedback" <?php if(get_option('cprate_multi_feedback')) echo $checked  ?>  /></td>
            </tr>
            <tr valign="top">
                <th scope="row" class="wide"><?php echo __("Enable Author to Reply Back to Feedback:", "cprate") ?></th>			
                <td><input class="boot" type="checkbox" name="cprate_enable_replyback" <?php if(get_option('cprate_enable_replyback')) echo $checked  ?>  /></td>
            </tr>
        </table>
        <table class="form-table">
            <tr valign="top">
                <th scope="row" class="wide">
					<?php echo __("Allow user to edit Feedback:", "cprate") ?>
                	<span style="color:blue">(Moderation must be Turned OFF)</span>
                </th>			
                <td class="clicker">
                	<input class="boot opt-box" type="checkbox" name="cprate_enable_edit_feedback" 
					<?php  if(get_option('cprate_req_feedback_mod')) {
								echo 'disabled';
							} else {
								if(get_option('cprate_enable_edit_feedback')) echo $checked; 
								
							}?>  />
                    <label style="padding:0 0 0 15px" for="cprate_poster_edit_time"><?php echo __("Time allowed to edit: ", "cprate") ?></label>
                	<input  size="4" type="text" name="cprate_poster_edit_time" value="<?php echo get_option('cprate_poster_edit_time'); ?>" />
					<?php echo __("(In Minutes. Enter 0 for unlimited)", "cprate") ?>
                </td>
            </tr>
            <tr valign="top">
                <th scope="row" class="wide">
					<?php echo __("Show message when Feedback has been edited:", "cprate") ?>
                </th>			
                <td class="clicker">
                	<input class="boot opt-box" type="checkbox" name="cprate_enable_edit_feedback_msg" <?php if(get_option('cprate_enable_edit_feedback_msg')) echo $checked; ?>  />
                    <label for="cprate_edited_feedback_msg"><?php echo __("Message: ", "cprate") ?></label>
                	<input  size="13" type="text" name="cprate_edited_feedback_msg" value="<?php echo get_option('cprate_edited_feedback_msg'); ?>" />
                </td>
            </tr>
        </table>
        <h3 class="cprate-header"><?php echo __("Feedback Disclaimer to User ", "cprate") ?></h3>
        <table class="form-table">
            <tr valign="top">
                <td>
                	<?php 
                		$settings = array( 'media_buttons' => false );
                		wp_editor(get_option('cprate_rules'), $name = 'cprate_rules', $settings); 
                	?>
                </td>
            </tr>
            <tr>
            	<td>Database Version <?php echo get_option("cprate_db_version") ?></td>
            </tr>
        </table>
    </div><!--//general -->
            
    <div class="cptab-content" id="tab2">
    	<h3 class="cprate-header"><?php echo __("Header Fields", "cprate") ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Header 1 - Author Page:<br /><span>(Overall Rating:)<span></th>
                <td><input size="40" type="text" name="cprate_header1" value="<?php echo get_option('cprate_header1'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Header 2 - Author Page:<br /><span>(Total Ratings:)<span></th>
                <td><input size="40" type="text" name="cprate_header2" value="<?php echo get_option('cprate_header2'); ?>" /></td>
            </tr>
        </table>
    	<h3 class="cprate-header"><?php echo __("Table Fields", "cprate") ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Column 1 - Header:<br /><span>(Rating)<span></th>
                <td><input size="40" type="text" name="cprate_rating" value="<?php echo get_option('cprate_rating'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Column 2 - Header:<br /><span>(Feedback)<span></th>
                <td><input size="40" type="text" name="cprate_feedback" value="<?php echo get_option('cprate_feedback'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Column 3 - Header:<br /><span>(Rated By)<span></th>
                <td><input size="40" type="text" name="cprate_ratedby" value="<?php echo get_option('cprate_ratedby'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Column 4 - Header:<br /><span>(Date)<span></th>
                <td><input size="40" type="text" name="cprate_date" value="<?php echo get_option('cprate_date'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Sub-Comment Text:<br /><span>(Reply back from Author:)<span></th>
                <td><input size="40" type="text" name="cprate_sub_comment" value="<?php echo get_option('cprate_sub_comment'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Reply Button:<br /><span>(Reply)<span></th>
                <td><input size="40" type="text" name="cprate_reply" value="<?php echo get_option('cprate_reply'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Show More (Pagination):<br /><span>(Show X More)<span></th>
                <td><input size="40" type="text" name="cprate_showmore" value="<?php echo get_option('cprate_showmore'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Show All (Pagination):<br /><span>(Show All)<span></th>
                <td><input size="40" type="text" name="cprate_showall" value="<?php echo get_option('cprate_showall'); ?>" /></td>
            </tr>

        </table>
    	<h3 class="cprate-header"><?php echo __("Form Fields", "cprate") ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Feedback Button:<br /><span>(Leave Feedback)<span></th>
                <td><input size="40" type="text" name="cprate_button" value="<?php echo get_option('cprate_button'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Cancel Button:<br /><span>(Cancel)<span></th>
                <td><input size="40" type="text" name="cprate_cancel" value="<?php echo get_option('cprate_cancel'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Save Button:<br /><span>(Save)<span></th>
                <td><input size="40" type="text" name="cprate_save" value="<?php echo get_option('cprate_save'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Submit Button:<br /><span>(Submit)<span></th>
                <td><input size="40" type="text" name="cprate_submit" value="<?php echo get_option('cprate_submit'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Your Rating Message:<br /><span>(Your Rating)<span></th>
                <td><input size="40" type="text" name="cprate_rating_msg" value="<?php echo get_option('cprate_rating_msg'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Feedback Comments:<br /><span>(Feedback Comments)<span></th>
                <td><input size="40" type="text" name="cprate_comnt_msg" value="<?php echo get_option('cprate_comnt_msg'); ?>" /></td>
            </tr>
            
            <tr valign="top">
                <th scope="row">Thank You Message:<br /><span>(Thank You!)<span></th>
                <td><input size="40" type="text" name="cprate_thanks" value="<?php echo get_option('cprate_thanks'); ?>" /></td>
            </tr> 
            <tr valign="top">
                <th scope="row">Submitting Feedback Message:<br /><span>(Submitting Feedback...)<span></th>
                <td><input size="40" type="text" name="cprate_submit_msg" value="<?php echo get_option('cprate_submit_msg'); ?>" /></td>
            </tr> 
            <tr valign="top">
                <th scope="row">Feedback Updated Message:<br /><span>(Your Feedback has been updated!)<span></th>
                <td><input size="40" type="text" name="cprate_editsaved_msg" value="<?php echo get_option('cprate_editsaved_msg'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">Edit Time Expired:<br /><span>(The time limit to edit this Feedback has expired)<span></th>
                <td><input size="40" type="text" name="cprate_edit_exp_msg" value="<?php echo get_option('cprate_edit_exp_msg'); ?>" /></td>
            </tr>  
        </table>
    	<h3 class="cprate-header"><?php echo __("Legend Fields", "cprate") ?></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">5 stars:</th>
                <td><input size="40" type="text" name="cprate_five_star" value="<?php echo get_option('cprate_five_star'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">4 stars:</th>
                <td><input size="40" type="text" name="cprate_four_star" value="<?php echo get_option('cprate_four_star'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">3 stars:</th>
                <td><input size="40" type="text" name="cprate_three_star" value="<?php echo get_option('cprate_three_star'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">2 stars:</th>
                <td><input size="40" type="text" name="cprate_two_star" value="<?php echo get_option('cprate_two_star'); ?>" /></td>
            </tr>
            <tr valign="top">
                <th scope="row">1 stars:</th>
                <td><input size="40" type="text" name="cprate_one_star" value="<?php echo get_option('cprate_one_star'); ?>" /></td>
            </tr>
        </table>
	</div><!--//fields -->
        
    <div class="cptab-content" id="tab3">      
        <h3 class="cprate-header"><?php echo __("Uninstall Options", "cprate") ?> <span>(This cannot be undone)</span></h3>
        <table class="form-table">
            <tr valign="top">
                <th scope="row" class="wide"><?php echo __("Delete Database and Options on Uninstall:", "cprate") ?></th>			
                <td><input class="boot" type="checkbox" name="cprate_deletedb" <?php if(get_option('cprate_deletedb')) echo $checked  ?>  /></td>
            </tr>
        </table>
    </div><!--//advanced -->
    <p class="submit">
    	<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </p>
</form>
</div>
<?php } 


function cprate_plugin_feedbacktable() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}	
	echo '<h2>'. __('CP Rate Moderate Feedback', 'cprate').'</h2>';
	echo '<div id="cprate_moderate_wrap" class="wrap">';
	echo cprate_plugin_printtable();
	echo '</div>';	
}

function cprate_plugin_printtable() {
global $wpdb, $wp_query;

	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	
	if (function_exists('is_multisite') && is_multisite()) {
		$main_prefix = $wpdb->get_blog_prefix(BLOG_ID_CURRENT_SITE);
	} else {
		$main_prefix = $wpdb->prefix;
	}
		
	$pagenum_mod = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
	$limit_mod = get_option('cprate_mod_records_show');
	$offset_mod = ( $pagenum_mod - 1 ) * $limit_mod;
	$sortby_mod_id = 'by';
	$sort_mod_sql ='';
	$need_mod_sql = '';
	$fb_mod_enabled = get_option('cprate_req_feedback_mod');
	$show_mod_only = get_option('cprate_show_mod_btn');
	$sort_by_mod = get_option('cprate_mod_sort_order');
	$auth_reply = get_option('cprate_enable_replyback');
	$ip_enabled = get_option('cprate_trk_ip');
	
		if($sort_by_mod == "default") {
			$sort_mod_sql = ' p.id DESC ';
		} elseif($sort_by_mod == "date_newest") {
			$sort_mod_sql = ' p.cprate_time DESC ';
		} elseif($sort_by_mod == "date_oldest") {
			$sort_mod_sql = ' p.cprate_time ASC ';
		} elseif($sort_by_mod == "author_az") {
			$sortby_mod_id = 'to';
			$sort_mod_sql = ' user_login ASC ';
		} elseif($sort_by_mod == "author_za") {
			$sortby_mod_id = 'to';
			$sort_mod_sql = ' user_login DESC ';	
		} elseif($sort_by_mod == "rater_az") {
			$sort_mod_sql = ' user_login ASC ';
		} elseif($sort_by_mod == "rater_za") {
			$sort_mod_sql = ' user_login DESC ';
		}
		
		if(($show_mod_only) && ($fb_mod_enabled)) { 
			$need_mod_sql = " AND p.feedback_moderate=1";
		}
		$direct_link = $_SERVER['REQUEST_URI'];
		$post_id = explode("ID=", $direct_link);
		if($post_id[1]) {
			$pid = $post_id[1];
			$sql_txt = "SELECT *, user_login FROM $wpdb->prefix"."rate_users AS p INNER JOIN $main_prefix"."users on given_".$sortby_mod_id."_user_id = $main_prefix";
			$sql_txt .="users.ID $need_mod_sql WHERE p.id='$pid'";
			$entries = $wpdb->get_results($sql_txt);
		} else {
			$sql_txt = "SELECT *, user_login FROM $wpdb->prefix"."rate_users AS p INNER JOIN $main_prefix"."users on given_".$sortby_mod_id."_user_id = $main_prefix";
			$sql_txt .="users.ID $need_mod_sql ORDER BY $sort_mod_sql LIMIT $offset_mod, $limit_mod";
			$entries = $wpdb->get_results($sql_txt);
		}
	
		 
?>
<div id="cprate_moderate_table">
    <div id="cprate_sorter">
    <form method="post" action="options.php">
        <?php
            settings_fields('cprate-mod-settings-group');
            do_settings_sections('cprate-mod-settings-group');
        ?>
            <div class="sortedby"><span><?php echo __('Sort By: ', 'cprate') ?></span><select name="cprate_mod_sort_order">
            <option value="">Sort Order</option>
                <option value="default" <?php selected( $sort_by_mod,  "default"); ?>>Default</option>
                <option value="date_newest" <?php selected( $sort_by_mod, "date_newest"); ?>>Newest</option>
                <option value="date_oldest" <?php selected( $sort_by_mod, "date_oldest"); ?>>Oldest</option>
                <option value="author_az" <?php selected( $sort_by_mod, "author_az"); ?>>Author A-Z</option>
                <option value="author_za" <?php selected( $sort_by_mod, "author_za"); ?>>Author Z-A</option>
                <option value="rater_az" <?php selected( $sort_by_mod, "rater_az"); ?>>Rated By A-Z</option>
                <option value="rater_za" <?php selected( $sort_by_mod, "rater_za"); ?>>Rated By Z-A</option>
            </select>
            <?php if($fb_mod_enabled) { ?>
                <input name="cprate_show_mod_btn" type="checkbox" <?php if($show_mod_only) { echo 'checked="checked"';} ?> /><span><?php echo __('Need Moderations Only', 'cprate') ?></span>
            <?php } ?>
                </div>
                <div class="rowlimit"><span><?php echo __('Records to show: ', 'cprate') ?></span><select name="cprate_mod_records_show">
                <option value="10" <?php selected( $limit_mod, 10 ); ?>>10</option> 
                <option value="20" <?php selected( $limit_mod, 20 ); ?>>20</option>
                <option value="30" <?php selected( $limit_mod, 30 ); ?>>30</option>
                <option value="40" <?php selected( $limit_mod, 40 ); ?>>40</option>
                <option value="50" <?php selected( $limit_mod, 50 ); ?>>50</option>
                <option value="100" <?php selected( $limit_mod, 100 ); ?>>100</option>
           </select>
            </div>
            <div class="clearthis"></div>
        </form>
    </div>
    <table class="widefat" id="cprate_moderate">
        <thead>
            <tr>
                <th scope="col" class="cprate_col_1"><?php echo __('Record Id', 'cprate') ?></th>
                <th scope="col" class="cprate_col_2"><?php echo __('Author Name', 'cprate') ?></th>
                <th scope="col" class="cprate_col_3"><?php echo get_option('cprate_rating') ?></th>
                <th scope="col" class="cprate_col_4"><?php echo get_option('cprate_feedback') ?></th>
                <?php if($auth_reply) { ?>
                    <th scope="col" class="cprate_col_5"><?php echo  __('Author Reply', 'cprate') ?></th>
                 <? } ?>   	
                <th scope="col" class="cprate_col_6"><?php echo get_option('cprate_ratedby') ?></th>
                <th scope="col" class="cprate_col_7"><?php echo get_option('cprate_date') ?></th>
                <?php if($ip_enabled) { ?>
                    <th scope="col" class="cprate_col_11"><?php echo  __('IP Address', 'cprate') ?></th>
                 <? } ?>   	
                <?php if($fb_mod_enabled) { ?>
                    <th scope="col" class="cprate_col_8"><?php echo  __('Status', 'cprate') ?></th>
                 <? } ?>   	
                <th scope="col" class="cprate_col_9"><?php echo __('Action', 'cprate') ?></th>
                <?php if($fb_mod_enabled) { ?>
                            <th scope="col" class="cprate_col_10"><input type="checkbox" id="selAll" /><span><?php echo __('Bulk Action', 'cprate') ?></span></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php if( $entries ) { 
                $count = 1;
                $class = '';
                foreach( $entries as $entry ) {
                    $class = ( $count % 2 == 0 ) ? ' class="alternate"' : '';
                    $pid = ' id="' .$entry->id . '"';
					$feed_txt="";
					$post_feed_txt="";
                ?>
                <tr<?php echo $pid . $class;  ?>>
                    <td class="cprate_col_1"><?php echo $entry->id; ?></td>
                    <td class="cprate_col_2"><?php $name = get_userdata( $entry->given_to_user_id ); echo $name->user_login; ?></td>
                    <td class="cprate_col_3"><?php echo print_all_stars($entry->star_rating); ?></td>
                    <td class="cprate_col_4">
						<?php 
                        	$feed_txt .= '<div id="'.$entry->id.'">'.$entry->feed_back;
									$feed_txt .='<img class="show_edit_box" title="Edit" src="'.plugins_url( '/css/images/edit.png', dirname(__FILE__) ).'" />';
									$feed_txt .='<div style="display:none" id="edit_comments_wrapper_'.$entry->id.'"  class="edit-com cp-collapsed">';
										$feed_txt .='<textarea id="text_edit_comments_'.$entry->id.'">'.$entry->feed_back.'</textarea>';
										$feed_txt .="<input type='button' value='".get_option('cprate_save')."' name='".$entry->id."' class='save_edit_comments' />";
										$feed_txt .='<input class="hide_edit_box" type="button" value="'.get_option('cprate_cancel').'" />';
										$feed_txt .='<span id="cprate-error-rb">';
									$feed_txt .='</div>';
							$feed_txt .= '</div>';
							echo $feed_txt;
                         ?>
                    </td>
					<?php if($auth_reply) { ?>
                    	<td class="cprate_col_5">
						<?php 
							if($entry->post_feed_back) {
								$post_feed_txt .= '<div id="'.$entry->id.'">'.$entry->post_feed_back;
										$post_feed_txt .='<img class="show_edit_box" title="Edit" src="'.plugins_url( '/css/images/edit.png', dirname(__FILE__) ).'" />';
										$post_feed_txt .='<div style="display:none" id="edit_comments_wrapper_'.$entry->id.'"  class="edit-com cp-collapsed">';
											$post_feed_txt .='<textarea id="text_edit_comments_'.$entry->id.'">'.$entry->post_feed_back.'</textarea>';
											$post_feed_txt .="<input type='button' value='".get_option('cprate_save')."' name='".$entry->id."' class='save_edit_sub_comments' />";
											$post_feed_txt .='<input class="hide_edit_box" type="button" value="'.get_option('cprate_cancel').'" />';
											$post_feed_txt .='<span id="cprate-error-rb">';
										$post_feed_txt .='</div>';
								$post_feed_txt .= '</div>';
								echo $post_feed_txt;
							}
                         ?>
                        </td>
                     <? } ?>   	
                    <td class="cprate_col_6"><?php $name = get_userdata( $entry->given_by_user_id ); echo $name->user_login; ?></td>
                    <td class="cprate_col_7"><?php echo date('Y-m-d H:i:s', strtotime($entry->cprate_time)) ?></td>
					<?php if($ip_enabled) { ?>
                        <td scope="col" class="cprate_col_11"><?php echo $entry->user_ip; ?></td>
                     <? } ?>   	
                    <?php if($fb_mod_enabled) { ?>
                        <td class="cprate_col_8">
                            <?php 
								if($entry->feedback_moderate == 0) { 
									echo '<font color="#009900">'.__('Approved', 'cprate').'</font>';
								} else { 
                                    echo '<font color="#FF0000">'.__('Pending', 'cprate').'</font>';
                                }
                            ?>
                         </td>    
                    <?php } ?>
                    <td class="action cprate_col_9">
                        <img alt="" title="<?php echo __('delete this record') ?>" class="remove_fb" src="<?php echo plugins_url('/css/images/delete.png' , dirname(__FILE__) ); ?>" />
                        <?php if(($fb_mod_enabled) && $entry->feedback_moderate == 1) { ?>
                            <img alt="" title="<?php echo __('approve this feedback') ?>" class="approve_fb" src="<?php echo plugins_url('/css/images/checkbox.png' , dirname(__FILE__) ); ?>" />
                        <? } ?>
                    </td>
                    <td class="bulk cprate_col_10">
                        <?php if(($fb_mod_enabled) && $entry->feedback_moderate == 1) { ?>
                                <input type="checkbox" class="bulk_action" /><img alt="" class="bulk_action_img" src="<?php echo plugins_url('/css/images/loading.gif' , dirname(__FILE__) ); ?>" />
                        <? } ?>
                    </td>    	
                </tr>
                <?php
                    $count++;
                }
                if($fb_mod_enabled) { ?>
                    <tr>
                        <td colspan="9">
                            <span><b><?php echo __('Moderation of all posts is ON', 'cprate') ?></b></span>
                            <input id="approve_all_slected" type="button" value="<?php echo __('Approve', 'cprate') ?>"  />
                            <input id="delete_all_slected" type="button" value="<?php echo __('Delete', 'cprate') ?>"  />
                        </td>
                    </tr>
                <? } 
    
            } else { ?>
            <tr>
                <td colspan="2">No posts yet</td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
<?php
		if(($show_mod_only) && ($fb_mod_enabled)) { 
			$mod_sql = " WHERE $wpdb->prefix"."rate_users.feedback_moderate=1";
		}
 
		$total_mod_rows = $wpdb->get_var( "SELECT COUNT(`id`) FROM $wpdb->prefix" . "rate_users" .$mod_sql );
		$total_mod_pages = ceil( $total_mod_rows / $limit_mod );
		
			if(strpos($_SERVER['REQUEST_URI'],'page=moderate-feedback')) {
				$this_base = add_query_arg( 'pagenum', '%#%' );
			} else {
				$this_base = str_replace ('-ajax','',add_query_arg( 'page=moderate-feedback&pagenum', '%#%' ));
			}		
		
		$mod_page_links = paginate_links( array(
			'base' => $this_base,
			'format' => '',
			'prev_text' => __( '&laquo;', 'aag' ),
			'next_text' => __( '&raquo;', 'aag' ),
			'total' => $total_mod_pages,
			'current' => $pagenum_mod
		) );
		
		$mod_pagin = '<div class="tablenav"><div class="total">'. __('Total Records: ', 'cprate') .$total_mod_rows.'</div>';
			if ( $mod_page_links ) {
				$mod_pagin .= '<div class="tablenav-pages" style="margin: 1em 0">' . $mod_page_links . '</div>';
			}
		$mod_pagin .= '</div><div class="clear"></div>';
		echo $mod_pagin;
?>        
</div>	

<?php }

add_action('wp_ajax_print_mod_pagination_action', 'cprate_plugin_printtable');
add_action('wp_ajax_nopriv_print_mod_pagination_action', 'cprate_plugin_printtable');

/////////////////       Highest Rated Author Page     ///////////////////////////////////////
function cprate_plugin_highestrated() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}?>
    
	<h2><?php echo __('Highest Rated Author Page', 'cprate') ?></h2>
   
		<?php
        global $wpdb;
        
        $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
        $limit = get_option('cprate_records_show_toprate');
        $offset = ( $pagenum - 1 ) * $limit;
        
        $sort_toprate = get_option('cprate_sort_toprate');
            if($sort_toprate == "default") {
                $sort = ' p.given_to_user_id DESC ';
            } elseif ($sort_toprate == "highest_rated") {
                $sort = ' avg_rating DESC ';
            } elseif ($sort_toprate == "lowest_rated") {
                $sort = ' avg_rating ASC ';
				
            } elseif ($sort_toprate == "highest_score") {
                $sort = ' total_count DESC ';
            } elseif ($sort_toprate == "lowest_score") {
                $sort = ' total_count ASC ';
				
            } elseif ($sort_toprate == "author_az") {
                $sort = ' user_login ASC ';
            } elseif ($sort_toprate == "author_za") {
                $sort = ' user_login DESC ';
            } 
			
		if (function_exists('is_multisite') && is_multisite()) {
			$main_prefix = $wpdb->get_blog_prefix(BLOG_ID_CURRENT_SITE);
		} else {
			$main_prefix = $wpdb->prefix;
		}
				
        $sql_txt = "SELECT p.given_to_user_id, avg(p.star_rating/5) * 100 AS avg_rating, COUNT(p.star_rating) AS total_count FROM $wpdb->prefix"."rate_users AS p INNER JOIN $main_prefix";
		$sql_txt .= "users on p.given_to_user_id = $main_prefix"."users.ID GROUP BY given_to_user_id ORDER BY $sort LIMIT $offset, $limit";		
        $entries = $wpdb->get_results($sql_txt);
                 
        ?>
	<div id="cprate_toprate_wrap" class="wrap">        
        <div id="cprate_sorter">
        <form method="post" action="options.php" id="cprate_sort_form">
            <?php
                settings_fields( 'cprate-toprate-settings-group' );
                do_settings_sections( 'cprate-toprate-settings-group' ); 
            ?>
               <div class="sortedby"><span><?php echo __('Sort By: ', 'cprate') ?></span><select name="cprate_sort_toprate">
                    <option value="default" <?php selected( $sort_toprate,  "default"); ?>>Default</option>
                    <option value="highest_rated" <?php selected( $sort_toprate,  "highest_rated"); ?>>Highest AVG Rating</option>
                    <option value="lowest_rated" <?php selected( $sort_toprate, "lowest_rated"); ?>>Lowest AVG Rating</option>
                    <option value="highest_score" <?php selected( $sort_toprate,  "highest_score"); ?>>Highest Score</option>
                    <option value="lowest_score" <?php selected( $sort_toprate, "lowest_score"); ?>>Lowest Score</option>
                    <option value="author_az" <?php selected( $sort_toprate, "author_az"); ?>>Author A-Z</option>
                    <option value="author_za" <?php selected( $sort_toprate, "author_za"); ?>>Author Z-A</option>
                </select>
                </div>
                <div class="rowlimit">
                 <span><?php echo __('Records to show: ', 'cprate') ?></span><select name="cprate_records_show_toprate">
                    <option value="10" <?php selected( $limit, 10 ); ?>>10</option> 
                    <option value="20" <?php selected( $limit, 20 ); ?>>20</option>
                    <option value="30" <?php selected( $limit, 30 ); ?>>30</option>
                    <option value="40" <?php selected( $limit, 40 ); ?>>40</option>
                    <option value="50" <?php selected( $limit, 50 ); ?>>50</option>
                    <option value="100" <?php selected( $limit, 100 ); ?>>100</option>
               </select>   
                </div>
            </form>
        </div>
            
        <?php if($enable_fbmod) { ?>
            <div id="mod-on-error"><?php echo __('* These Scores and Ratings do not include Un-Moderated Feedback', 'cprate') ?></div>
        <?php } ?>
        <table class="widefat" id="cprate_moderate">
            <thead>
                <tr>
                    <th scope="col" class="cprate_col4"><?php echo __('Author Name', 'cprate') ?></th>
                    <th scope="col" class="cprate_col4"><?php echo get_option('cprate_rating') ?></th>
                    <th scope="col" class="cprate_col4"><?php echo __('Total Score', 'cprate') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php if( $entries ) { ?>
         
                    <?php
                    $count = 1;
                    $class = '';
                    foreach( $entries as $entry ) {
                        $class = ( $count % 2 == 0 ) ? ' class="alternate"' : '';
                        $author = $entry->given_to_user_id;
                    ?>
                    <tr>
                        <td><?php $name = get_userdata($author); echo $name->user_login; ?></td>
                        <td><?php echo cprate_top_rated($author); ?></td>
                        <td><?php echo cprating_count($author); ?></td>
                    </tr>
         
                    <?php
                        $count++;
                    }
                    ?>
         
                <?php } else { ?>
                <tr>
                    <td colspan="2">No posts yet</td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
         
        <?php		
        $total = $wpdb->get_var( "SELECT COUNT(DISTINCT given_to_user_id) FROM $wpdb->prefix" . "rate_users" );
        $num_of_pages = ceil( $total / $limit );
        $page_links = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'prev_text' => __( '&laquo;', 'aag' ),
            'next_text' => __( '&raquo;', 'aag' ),
            'total' => $num_of_pages,
            'current' => $pagenum
        ) );
         
        if ( $page_links ) {
            echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
        }
		?>
</div>
	
<?php } 
add_action( 'admin_menu', 'cprate_plugin_menu' );
?>