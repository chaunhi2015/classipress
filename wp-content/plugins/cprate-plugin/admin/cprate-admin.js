/*
Classipress Rate Authors PRO
Current Version: 4.0
Plugin Author: Julio Gallegos
Author URL: http://wprabbits.com
*/

jQuery(document).ready(function($) { 

	//// *********************   Main Settings Page ********************////////	
	$('ul.tab-links').find('a').on('click', function(e)  {
		var tab = $(this).attr('href'); 
			$('div.cptab-content').removeClass('active-cptab');
			$(this).parent().siblings().removeClass('active').end().addClass('active');
			$(tab).addClass('active-cptab');
			e.preventDefault();
	});

	var testNumeric = function(field) {
		var is = $(field);
		var name = is.attr('name');
			if(is.val() != "") {
				var value = is.val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
				var intRegex = /^\d+$/;
				if(!intRegex.test(value) && (!$('#' + name).length)) {
					is.parent().append("<div id='" + name + "' class='cprate_error'>Field must be numeric</div>");
					is.click(function() { $('#' + name).remove() });
				}
			} 	
	}
	
	
	//Activate BootStrap
	$("input.boot").bootstrapSwitch();
	if($('input.opt-box').length) {
		$('input.opt-box').on('switchChange.bootstrapSwitch',function(e,i) {
			if (this.checked) {
				$(this).closest('tr').find('input:text').prop('disabled', false);
			} else {
				$(this).closest('tr').find('input:text').prop('disabled', true);
			}
		}).each(function(i,e) {
			if (e.checked) {
				$(e).closest('tr').find('input:text').prop('disabled', false);
			} else {
				$(e).closest('tr').find('input:text').prop('disabled', true);
			}
		});
	}
	
	$("input[name='cprate_req_feedback_mod']").click(function() {
		if(this.checked) {
			$("input[name='cprate_enable_edit_feedback']").add("input[name='cprate_enable_edit_feedback_msg']").attr({'disabled':true, 'checked':false}).trigger("change");
		} else {
			$("input[name='cprate_enable_edit_feedback']").add("input[name='cprate_enable_edit_feedback_msg']").attr('disabled',false);
		}
	});
	

	
//// *********************   Moderator Page ********************////////
	
	$('#cprate_moderate_wrap').on("change","select[name='cprate_mod_sort_order'], select[name='cprate_mod_records_show'], select[name='cprate_sort_toprate'],select[name='cprate_records_show_toprate']",function() {
		$(this).closest('form').submit();
	});
	$('#cprate_moderate_wrap').on("click","input[name='cprate_show_mod_btn']",function() {
		$(this).closest('form').submit();
	});
	
	$("#cprate_moderate").on("click","img.show_edit_box, input.hide_edit_box",function(){
		var par = $(this).closest('td').find("div.edit-com");
			if(par.hasClass('cp-collapsed')) {
				$(par).removeClass('cp-collapsed');
				$(par).slideDown(500);
			} else {
				$(par).addClass('cp-collapsed');
				$(par).slideUp(500);
			}				
	});
	
	$("td.cprate_col_4").on("click", "input.save_edit_comments",function() {
		var $this = $(this);
		var id = $this.attr("name");
		var par = $this.closest("td");
		var edit_comments_wrap = par.find("div.edit-com");
		var edit_comments_text = par.find("textarea");	
			if(edit_comments_text.val() === "") {
				par.find("#cprate-error-rb").show().text("You must enter a comment").css("color","red");
				edit_comments_text.on("change keydown",function() {
					par.find("#cprate-error-rb").hide().text("");	
				});
			}
			else {
				edit_comments_wrap.empty().html("<div style='color:red'>Saving ...</div>");
						var data = {
							action: "save_edit_comments_action",
							edit_feedback_nonce: cpRateVars.edit_feedback_nonce,
							edit_comments_id: id,
							edit_comments_text: edit_comments_text.val(),
							mode_edit:"feed"	
						};
				
					$.post(cpRateVars.cprate_ajax, data, function(response) {
						var resp=response;
						response=resp.substring(0,resp.length-1);
						edit_comments_wrap.html("<div style='color:green'>Saved</div>").delay(500).fadeOut('slow',function() {
							edit_comments_wrap.parent().html(response).end().remove();	
						});
					});
			}
	});
	
	$("td.cprate_col_5").on("click", "input.save_edit_sub_comments",function() {
		var $this = $(this);
		var id = $this.attr("name");
		var par = $this.closest("td");
		var edit_comments_wrap = par.find("div.edit-com");
		var edit_comments_text = par.find("textarea");	
			if(edit_comments_text.val() === "") {
				par.find("#cprate-error-rb").show().text("You must enter a comment").css("color","red");
				edit_comments_text.on("change keydown",function() {
					par.find("#cprate-error-rb").hide().text("");	
				});
			}
			else {
				edit_comments_wrap.empty().html("<div style='color:red'>Saving ...</div>");
						var data = {
							action: "save_edit_comments_action",
							edit_feedback_nonce: cpRateVars.edit_feedback_nonce,
							edit_comments_id: id,
							edit_comments_text: edit_comments_text.val(),
							mode_edit:"post_feed"	
						};
				
					$.post(cpRateVars.cprate_ajax, data, function(response) {
						var resp=response;
						response=resp.substring(0,resp.length-1);
						edit_comments_wrap.html("<div style='color:green'>Saved</div>").delay(500).fadeOut('slow',function() {
							edit_comments_wrap.parent().html(response).end().remove();	
						});
					});
			}
	});

	
	// Delete Selected Rating
	$('#cprate_moderate_wrap').on('click','img.remove_fb',function() {
		if (confirm('Are you sure you want to delete this record?')) {
			var is = $(this);
			var row = is.parent().parent();
			var data = {
					action: "cprate_delete_feedback",
					row_id: row.attr('id')
				};
				is.attr('src',is.attr('src').replace('delete.png','loading.gif'));
				$.post(ajaxurl, data, function(response) {
						row.hide('slow', function(){ row.remove(); });
				});
    		}
    	return false;  
	});
	
	// Approve Selected Rating
	$('#cprate_moderate_wrap').on('click','img.approve_fb',function() {

		var is = $(this);
		var row = is.parent().parent();
		var data = {
				action: "cprate_approve_feedback",
				row_id: row.attr('id')
			};
		is.attr('src',is.attr('src').replace('checkbox.png','loading.gif'));
		$.post(ajaxurl, data, function(response) {
				row.css('background-color','inherit');
				is.hide('slow');
		});
	});
	
	//bulk action
	var modboxes = $("#cprate_moderate").find("input.bulk_action");
		if(modboxes.length === 0) {
			$('#delete_all_slected, #approve_all_slected, #selAll').attr("disabled", true).css("color","#DDD");
		} else {
			$('#selAll, input.bulk_action').attr("checked",false).attr("disabled", false);
			$('#cprate_moderate_wrap').on('click','#selAll',function() {	
				if(this.checked) {
					$("#cprate_moderate").find("input.bulk_action").attr("checked",true);
				} else {
					$("#cprate_moderate").find("input.bulk_action").attr("checked",false);
				}		
			});
			$('#cprate_moderate_wrap').on('click','#approve_all_slected',function() {	
				var appboxes = $("#cprate_moderate").find("input.bulk_action:checked");
					if(appboxes.length == 0) {
						alert("There are no items selected!");
					} else {
						if(confirm("Are you sure you want to Bulk Approve "+ appboxes.length +" item(s)?")) {
							appboxes.each(function() {
								var me = $(this);
								var row = me.closest("tr");
								var data = {
										action: "cprate_approve_feedback",
										row_id: row.attr('id')
								};
								var data2 = {
										action: "print_mod_pagination_action"
								};
									me.next().show();
									$.post(ajaxurl, data, function(response) {
											row.css('background-color','inherit');
											me.hide('slow').remove();
											row.find("font").fadeOut().attr("color","#009900").text("Approved").fadeIn();
											row.find("img.bulk_action_img, img.approve_fb").hide('slow').remove();
											if($("input.bulk_action").length === 0) {
												$('#selAll').attr("disabled", true);
											}
											$('#selAll').attr("checked",false);
											$.post(ajaxurl, data2, function(response2) {
												
												$('#cprate_moderate_table').remove();
												$('#cprate_moderate_wrap').append($(response2))
	
											});
									});
							});
						}
					}
			});
			$('#cprate_moderate_wrap').on('click','#delete_all_slected',function() {
				var delboxes = $("#cprate_moderate").find("input.bulk_action:checked");
					if(delboxes.length == 0) {
						alert("There are no items selected!");
					} else {
						if(confirm("Are you sure you want to Bulk Delete "+delboxes.length+" item(s)?")) {
							delboxes.each(function() {
								var me = $(this);
								var row = me.closest("tr");
								var data = {
										action: "cprate_delete_feedback",
										row_id: row.attr('id')
									};
									me.next().show();
									$.post(ajaxurl, data, function(response) {
											me.hide('slow', function(){ 
												row.remove();
												if($("input.bulk_action").length === 0) {
													$('#selAll').attr("disabled", true);
												}
												$('#selAll').attr("checked",false); 
											});
									});
							});
						}
					}
			});			
		}
	

});