<?php  
/*
Plugin Name: Classipress Rate Authors PRO
Plugin URI: http://wprabbits.com/
Description: Plugin to rate Authors and submit Feedback with a sub comment feature
Version: 4.0
Author: Julio Gallegos
Author URI: http://wprabbits.com/
License: GPL
*/

global $cprate_db_version, $rules_txt,$legacy_install, $site_prefix;

$cprate_db_version = "1.0";
$rules_txt ='<h3>The Rules</h3>';
$rules_txt.='<p>The site Administrator(s) reserves the right, but undertake no duty, to review, edit, move, or delete ';
$rules_txt.='any feedback submitted by users, in our sole discretion, without notice, including but not limited to the following reasons:</p>';
$rules_txt.='<ul><li>Do not post obscene, hateful, offensive, defamatory, abusive, harassing or profane material. </li>';
$rules_txt.='<li>Do not engage in personal attacks. </li>'; 
$rules_txt.='<li>Do not post personal information such as e-mail address, telephone number, street address or any other personal information. </li>';
$rules_txt.='<li>Do not post links to another sites. </li>';
$rules_txt.='<li>Keep your feedback on topic for the particular Author you are leaving it for. </li>'; 
$rules_txt.='<li>Do not use multiple accounts to post Feedback. Your IP address may be recorded. </li></ul>';


function cprate_install_fun($table_prefix) {

   $table_name = $table_prefix . "rate_users";
   $sql = "CREATE TABLE $table_name (
	  id mediumint(9) NOT NULL AUTO_INCREMENT,
	  cprate_time datetime NOT NULL,
	  given_to_user_id MEDIUMINT,
	  given_by_user_id MEDIUMINT,
	  star_rating TINYINT(5),
	  feed_back TEXT CHARACTER SET utf8 COLLATE utf8_general_ci,
	  post_feed_back TEXT CHARACTER SET utf8 COLLATE utf8_general_ci,
	  feedback_moderate TINYINT(1),
	  UNIQUE KEY id (id)
    );";
	return $sql;
}

//// Creates our table to store user ratings
function cprate_install() {
   global $wpdb,$rate_db_version;
   	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	if (function_exists('is_multisite') && is_multisite()) {
		// check if it is a network activation - if so, run the activation function for each blog id
			$old_blog = $wpdb->blogid;
			$blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
			foreach ($blogids as $blog_id) {
				switch_to_blog($blog_id);
				$sql = cprate_install_fun($wpdb->prefix);
				dbDelta($sql);
				add_option("cprate_db_version", $cprate_db_version);
				cprate_install_plugin();
				cprate_upgrade_db();
			}
			switch_to_blog($old_blog);
			return;
	} 
	else {
		$sql = cprate_install_fun($wpdb->prefix);
		dbDelta($sql);
		add_option("cprate_db_version", $cprate_db_version);
		cprate_install_plugin();
		cprate_upgrade_db();
	}
}

function cprate_upgrade_db() {
   	global $wpdb,$rate_db_version;
	
	$new_db_version="2.0";
	
	if($rate_db_version != $new_db_version) {
	   $table_name = $wpdb->prefix . "rate_users";
	   $sql = "CREATE TABLE $table_name (
		  id mediumint(9) NOT NULL AUTO_INCREMENT,
		  cprate_time datetime NOT NULL,
		  user_ip TEXT CHARACTER SET utf8 COLLATE utf8_general_ci,
		  given_to_user_id MEDIUMINT,
		  given_by_user_id MEDIUMINT,
		  star_rating TINYINT(5),
		  feed_back TEXT CHARACTER SET utf8 COLLATE utf8_general_ci,
		  feed_back_edit_time datetime,
		  post_feed_back TEXT CHARACTER SET utf8 COLLATE utf8_general_ci,
		  post_feed_back_edit_time datetime,
		  feedback_moderate TINYINT(1),
		  UNIQUE KEY id (id)
		);";
		$alter = "ALTER TABLE $table_name 
			MODIFY feed_back_edit_time DATETIME  AFTER feed_back,
			MODIFY post_feed_back_edit_time DATETIME  AFTER `post_feed_back";
   		update_option("cprate_db_version", $new_db_version);
	   	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  		dbDelta($sql);
		$wpdb->query($alter);
	}
}
function cprate_install_plugin(){
	update_option('cprate_trk_ip', true);
	update_option('cprate_enable_replyback', true);
	update_option('cprate_enable_legend', true);
	update_option('cprate_rules',$rules_txt);
}
register_activation_hook(__FILE__,'cprate_install');

if ( is_admin() ) {
	require_once dirname( __FILE__ ) . '/admin/cprate-admin.php';
}
add_action('appthemes_after_post', 'add_adpage_vars');
add_action('wp_enqueue_scripts', 'cprate_scripts' );
add_action('wp_ajax_cprate_post_feedback_action', 'cprate_post_feedback');
add_action('wp_ajax_nopriv_cprate_post_feedback_action', 'cprate_post_feedback');
add_action('wp_ajax_save_sub_comments_action', 'save_sub_comments_callback');
add_action('wp_ajax_nopriv_save_sub_comments_action', 'save_sub_comments_callback');
add_action('wp_ajax_save_edit_comments_action', 'save_edit_comments_callback');
add_action('wp_ajax_nopriv_save_edit_comments_action', 'save_edit_comments_callback');
add_action('wp_ajax_save_edit_sub_comments_action', 'save_edit_sub_comments_callback');
add_action('wp_ajax_nopriv_save_edit_sub_comments_action', 'save_edit_sub_comments_callback');


function add_adpage_vars() {
	global $post;
		wp_localize_script('cprate_ad_js', 'cprateAdPage', 
		array(
			'cprate_ajax' => admin_url('admin-ajax.php'),
			'cprate_id' => $post->post_author
		));
}

function cprate_run() {
	global $wp_query, $legacy_install, $post;
		
		$page = $wp_query->query_vars['pagename'];
		$post_user_id = $post->post_author;
		$curr_user_id = wp_get_current_user()->ID;
		$page_slug = get_page_template_slug( $post->ID ); 
		
			if(!$legacy_install && is_author()) {
				cp_rate_users($post_user_id ,true);
			}
			if($page == 'dashboard' || $page_slug == 'tpl-dashboard.php') {
				echo cprate_my_rating($curr_user_id);
				echo cprate_my_score($curr_user_id);
       			wp_enqueue_script('cprate_auth_js');
				wp_enqueue_style('cprate-css');
			}
}

add_action('appthemes_before_sidebar_widgets', 'cprate_run');


//// Loads our CSS and JS files only on pages were we need them
function cprate_scripts() {	
	wp_register_style( 'cprate-css', plugins_url('/css/cprate.css', __FILE__) );
	wp_register_script('cprate_plugin', plugins_url('/js/cprate-jqplugin.js', __FILE__) , array('jquery'), '2.0', true);
	wp_register_script('cprate_js', plugins_url('/js/cprate.js', __FILE__) , array('jquery'), '2.0', true);
	wp_register_script('cprate_ad_js', plugins_url('/js/cprate-adpage.js', __FILE__) , array('jquery'), '2.0', true);
	wp_register_script('cprate_auth_js', plugins_url('/js/cprate-authpage.js', __FILE__) , array('jquery'), '2.0', true);
	if(is_author()) {
		wp_enqueue_style( 'cprate-css' );
		wp_enqueue_script( 'cprate_plugin' );
       	wp_enqueue_script( 'cprate_js' );
	} elseif(is_single()) {
		wp_enqueue_style('cprate-css');
       	wp_enqueue_script('cprate_ad_js');
	}
}

function cprate_calculate($id) {
	global $wpdb;
		$fbswitch = (get_option('cprate_req_feedback_mod')) ? " AND feedback_moderate=0": '';
		$rating = $wpdb->get_var("SELECT avg(star_rating) FROM $wpdb->prefix" . "rate_users where given_to_user_id=".$id. $fbswitch); 		
	return round($rating,2);
}

function cprating_count($id) {
	global $wpdb;
		$fbswitch = (get_option('cprate_req_feedback_mod')) ? " AND feedback_moderate=0": '';				
		$cprating = $wpdb->get_var("SELECT count(star_rating) FROM $wpdb->prefix" . "rate_users where given_to_user_id=".$id. $fbswitch);
		$cprating_count = (empty($cprating)) ? 0 : $cprating;
	return $cprating_count;
}

function print_all_stars($count) {
	$num_stars = 5;
	$output = "";
		for($i=0;$i<$num_stars;$i++) {
			$type_star = ($i < $count) ? 'full' : 'empty';
			$output .= "<img class='cp-rate-stars' src='". plugins_url('/css/images/'.$type_star.'-star-small.png', __FILE__) ."' />";
		}
	return  $output;
}

function cprate_my_rating($id) {
		$ratecal = cprate_calculate($id);
		$count = round($ratecal);
		$rateperc = $ratecal/5 * 100;
		$myrating = '<h3 class="dotted">'.get_option('cprate_header1').': <span>'; 
		$myrating .= print_all_stars($count);
		$myrating .='</span> ( <span id="cprate_per">'.$rateperc. '%</span> ) </h3>';
	return $myrating;
}

function cprate_my_score($id) {
		$myrating = '<h3 class="dotted">'.get_option('cprate_header2').': '; 
		$myrating .='( <span id="total_count">' .cprating_count($id). '</span>  ) </h3>';
	return $myrating;
}


function cprate_my_legend($id) {
	global $wpdb;
		$all=0;$five=0;$four=0;$three=0;$two=0;$one=0;
		$fbswitch = (get_option('cprate_req_feedback_mod')) ? " AND feedback_moderate=0": '';				
		$cprating = $wpdb->get_results("SELECT star_rating FROM $wpdb->prefix" . "rate_users where given_to_user_id=".$id. $fbswitch);
			foreach ($cprating as $cp) {
				switch ($cp->star_rating) {
					case 5:
						$five++;
						break;
					case 4:
						$four++;
						break;
					case 3:
						$three++;
						break;
					case 2:
						$two++;
						break;
					case 1:
						$one++;
						break;
				}
				$all++;
			}
			
		$per = function($stars, $all) {
			return round($stars/$all,2)*100;
		};
			
		$legend_tbl = '<table id="review-legend"><tbody>'; 
	  		$legend_tbl .= '<tr><td class="stars">'.get_option('cprate_five_star').'</td><td class="bar"><div style="width:'.$per($five,$all).'%"></div></td><td class="perc">('.$five.')</td></tr>';
	  		$legend_tbl .= '<tr><td class="stars">'.get_option('cprate_four_star').'</td><td class="bar"><div style="width:'.$per($four,$all).'%"></div></td><td class="perc">('.$four.')</td></tr>';
	  		$legend_tbl .= '<tr><td class="stars">'.get_option('cprate_three_star').'</td><td class="bar"><div style="width:'.$per($three,$all).'%"></div></td><td class="perc">('.$three.')</td></tr>';
	  		$legend_tbl .= '<tr><td class="stars">'.get_option('cprate_two_star').'</td><td class="bar"><div style="width:'.$per($two,$all).'%"></div></td><td class="perc">('.$two.')</td></tr>';
	  		$legend_tbl .= '<tr><td class="stars">'.get_option('cprate_one_star').'</td><td class="bar"><div style="width:'.$per($one,$all).'%"></div></td><td class="perc">('.$one.')</td></tr>';
		$legend_tbl .= '</tbody></table>';
	return $legend_tbl;
}

function cprate_my_adpage_rating() {
		$this_id = $_POST['this_id'];
		$ratecal = cprate_calculate($this_id);
		$count = round($ratecal);
		$rateperc = $ratecal/5 * 100;
		$myrating = '<li><div class="cprate-myrating">' .get_option('cprate_header1'). ': <span>'; 
		$myrating .= print_all_stars($count);
		$myrating .='</span> (<span id="cprate_per">'.$rateperc. '%</span>) </div></li>';
		$myrating .='<li><div class="cprate-myrating">' . get_option('cprate_header2')  . ' ( <span id="total_count">' .cprating_count($this_id). '</span> )</div></li>';
	echo $myrating;
exit;
}
add_action('wp_ajax_cprate_my_rating_action', 'cprate_my_adpage_rating');
add_action('wp_ajax_nopriv_cprate_my_rating_action', 'cprate_my_adpage_rating');

function cprate_feedback_form() {	
		$content = '<div id="cprate_form_wrapper">';
			$content .='<input id="leave_feedback" type="button" value="' .get_option('cprate_button'). '" />';
			$content .='<input style="display:none" id="cancel_feedback" type="button" value="' .get_option('cprate_cancel'). '" />';		
			$content .='<div id="cprate_feedback_form" style="display:none">';
				$content .='<div id="cprate_rules"><pre>'.get_option('cprate_rules').'</pre></div>';
				$content .='<div id="stars-wrapper"></div>';
				$content .='<div id="your_rating">'.get_option('cprate_rating_msg').': <span id="rating_show">not set</span></div>';					
				$content .='<label>'.get_option('cprate_comnt_msg').'</label><br />';
				$content .='<textarea name="cprate_comments" id="cprate_comments"></textarea><br />';
				$content .='<input type="button" value="'.get_option('cprate_submit').'" onclick="SubmitFeedback()"  />';
				$content .='<span id="cprate-error"></span>';
			$content .='</div>';
			$content .='<div class="pad5"></div>';
		$content .='</div>';
		$content .='<div id="cprate_loading" style="display:none">';
			$content .='<p>'.get_option('cprate_thanks').'</p>';
			$content .='<img src="'.plugins_url('/css/images/cprate_progress_bar.gif', __FILE__).'" alt="" />';
			$content .='<span>'.get_option('cprate_submit_msg').'</span>';
		$content .='</div>';
	return $content;
}


function feedback_already_exists($id) {
	global $wpdb;
		$user = wp_get_current_user();	
		$myrows = $wpdb->get_results( "SELECT 1 from $wpdb->prefix" . "rate_users where given_to_user_id=".$id." and given_by_user_id =".$user->ID);
		foreach ($myrows as $row) {
			return true;
		}
return false;
}

function cprate_feedback_history($id) {
	global $wpdb, $current_user;
		get_currentuserinfo();
		$userid=$current_user->ID;
		$enable_replyback = get_option('cprate_enable_replyback');
		$enable_fbmod = get_option('cprate_req_feedback_mod');
		$enanle_edit = get_option('cprate_enable_edit_feedback');
		
		
		if (function_exists('is_multisite') && is_multisite()) {
			$main_prefix = $wpdb->get_blog_prefix(BLOG_ID_CURRENT_SITE);
		} else {
			$main_prefix = $wpdb->prefix;
		}
		
		$myrows = $wpdb->get_results( "SELECT *, user_login FROM $wpdb->prefix" . "rate_users AS p inner join $main_prefix"."users on p.given_by_user_id = $main_prefix"."users.ID where given_to_user_id=".$id." ORDER BY p.id DESC");
		//$output = '<h3 class="dotted">'.get_option('cprate_header2').' ( <span id="total_count">'.cprating_count($id).'</span> )</h3>';
		$output ='<table id="cprate_feedback_history">';
			$output.='<thead>';
				$output.='<th class="cpone">'.get_option('cprate_rating').'</th>';
				$output.='<th class="cptwo">'.get_option('cprate_feedback').'</th>';
				$output.='<th class="cpthre">'.get_option('cprate_ratedby').'</th>';
				$output.='<th class="cpfour">'.get_option('cprate_date').'</th>';
			$output.='</thead>';
			$output.='<tbody id="tbody_feedback_history">';
			
				foreach ($myrows as $row) {
				
					
					$edit_allowed_time = (get_option('cprate_poster_edit_time') == 0) ? '0': get_option('cprate_poster_edit_time');
					$editable_time = strtotime("+$edit_allowed_time minutes",strtotime($row->cprate_time));
					$now = strtotime(current_time('mysql'));
					$star_rating_class = '';
					 	switch ($row->star_rating) {
							case 5:
								$star_rating_class = 'five_star';
								break;
							case 4:
								$star_rating_class = 'four_star';
								break;
							case 3:
								$star_rating_class = 'three_star';
								break;
							case 2:
								$star_rating_class = 'two_star';
								break;
							case 1:
								$star_rating_class = 'one_star';
								break;
					 	}
					$edited_msg = '';
					
					if(get_option('cprate_enable_edit_feedback_msg') && $row->feed_back_edit_time) {
						$edited_msg = '<span>('.get_option('cprate_edited_feedback_msg').')</span>';
					}
										
					$output .= '<tr class="'.$star_rating_class.'">';
					
						$output .= '<td class="cpone">'.print_all_stars($row->star_rating).'</td>';
							
									if(($enable_fbmod) && $row->feedback_moderate == 1) {
									
										$output .= '<td class="cprate-fbmod">'.get_option('cprate_req_feedback_mod_msg').'</td>';
										
									} else { 
										$output .= '<td class="cptwo">';
											$output .= '<div id="'.$row->id.'">'.$row->feed_back;
											
												// If Editing of Feedback is turned ON
												if(($enanle_edit) && (is_user_logged_in()) && 
													$current_user->user_login==$row->user_login && (!$enable_fbmod) &&
													($editable_time > $now || $edit_allowed_time == '0')
												) {
														$output .='<img class="show_edit_box" title="Edit" src="'.plugins_url( 'css/images/edit.png', __FILE__ ).'" />';
														$output .='<div style="display:none" id="edit_comments_wrapper_'.$row->id.'"  class="edit-com cp-collapsed">';
															$output .='<textarea id="text_edit_comments_'.$row->id.'">'.$row->feed_back.'</textarea>';
															$output .='<input type="button" value="'.get_option('cprate_save').'" onclick="save_edit_comments('.$row->id.')" />';
															$output .='<input class="hide_edit_box" type="button" value="'.get_option('cprate_cancel').'" />';
															$output .='<span id="cprate-error-rb">';
														$output .='</div>';
												}
												
												$output .= '</div>';
												
												// If Reply Back to Feedback is turned ON
												if($enable_replyback == true && $row->post_feed_back == "" && is_user_logged_in() == true && $id==$userid) {
													$output .='<div id="sub_comments_wrapper_'.$row->id.'"  class="reply-back">';
														$output .='<input class="show_sub_box" type="button" value="'.get_option('cprate_reply').'" />';
														$output .='<div class="cp-collapsed" style="display:none">';
															$output .='<textarea id="text_sub_comments_'.$row->id.'"></textarea>';
															$output .='<input type="button" value="'.get_option('cprate_submit').'" onclick="save_sub_comments('.$row->id.')" />';
															$output .='<span id="cprate-error-rb">';
														$output .='</div>';
													$output .='</div>';
													
												} 
												
												// If Reply Back to Feedback is turned ON and editable
												elseif($enable_replyback == true && $row->post_feed_back != "") {
													if(is_user_logged_in() == true && $id==$userid && ($enanle_edit) &&
													($editable_time > $now || $edit_allowed_time == '0')) {
														$output .='<div class="sub-comments-quote"><span>'.get_option('cprate_sub_comment').':  </span>'.$row->post_feed_back;
															$output .='<img class="show_sub_edit_box" title="Edit" src="'.plugins_url( 'css/images/edit.png', __FILE__ ).'" />';
															$output .='<div style="display:none" id="edit_sub_comments_wrapper_'.$row->id.'"  class="edit-sub-com cp-collapsed">';
																$output .='<textarea id="text_edit_sub_comments_'.$row->id.'">'.$row->post_feed_back.'</textarea>';
																$output .='<input type="button" value="'.get_option('cprate_save').'" onclick="save_edit_sub_comments('.$row->id.')" />';
																$output .='<input class="hide_edit_sub_box" type="button" value="'.get_option('cprate_cancel').'" />';
																$output .='<span id="cprate-error-rb">';
															$output .='</div>';
														$output .='</div>';
													}else {
														$output .='<div class="sub-comments-quote"><span>'.get_option('cprate_sub_comment').':  </span>'.$row->post_feed_back.'</div>';
													}
												}
										$output .= '</td>';
									} 
								$output.= '<td class="cpthre"><a href="' . get_site_url(). '/author/' .$row->user_login.  '">' .$row->user_login. '</a></td>'; 
								$output.= '<td class="cpfour">' .date(get_option('cprate_date_format'), strtotime($row->cprate_time)). '</td>';							
							$output.= '</tr>';
						} 

			$output.= '</tbody>';
		$output.='</table>';
	
	return $output;
}

function cprate_getIP() {
	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		$ip = $_SERVER['HTTP_CLIENT_IP']; // ip from share internet
	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR']; // ip from proxy
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}

// send notification email to admin on Moderation ON
function cprate_email_mod($text, $to_auth,$posted_by,$newid) {
	
	$auth = get_userdata($to_auth);
	$poster = get_userdata($posted_by);
    $mailto = get_option('cprate_admin_note_email');
    $subject = __( 'New Feedback Requires Moderation', APP_TD );
	$url = admin_url( 'admin.php?page=moderate-feedback&ID='.$newid);

    // The blogname option is escaped with esc_html on the way into the database in sanitize_option
    // we want to reverse this for the plain text arena of emails.
    $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $message  = __( 'Dear Admin,', APP_TD ) . "\r\n\r\n";
    $message .= sprintf( __( 'The following Feedback was posted on %s and requires Moderation', APP_TD ), $blogname ) . "\r\n\r\n";
    $message .= __( 'Feedback Details', APP_TD ) . "\r\n";
    $message .= __( '-----------------', APP_TD ) . "\r\n";
    $message .= __( 'Text: ', APP_TD ) . $text . "\r\n";
    $message .= __( 'Given to User: ', APP_TD ) . $auth->user_login. "\r\n";
    $message .= __( 'Given by: ', APP_TD ) . $poster->user_login. "\r\n\r\n";
    $message .= __( '-----------------', APP_TD ) . "\r\n\r\n";
    $message .= __( 'Moderate this Feedback: ', APP_TD ) . $url. "\r\n\r\n\r\n";
    $message .= __( 'Regards,', APP_TD ) . "\r\n\r\n\r\n\r\n";
    wp_mail( $mailto, $subject, $message );
}

	
function cprate_post_feedback() {
	global $wpdb;
		$post_feedback_nonce = $_POST['post_feedback_nonce'];
			if(! wp_verify_nonce( $post_feedback_nonce, 'myajax_post_feedback_nonce')) {
				die( 'You failed to be authenticated!');
			}
		$cprate_time = current_time('mysql');	
		$given_to_user_id = $_POST['given_to_user_id'];
		$star_rating = $_POST['star_rating'];
		$feed_back = $_POST['feed_back'];
		$user = wp_get_current_user();
		$given_by_user_id = $user->ID;
		$feedback_moderate = (get_option('cprate_req_feedback_mod')) ? 1 : 0;
			if(get_option('cprate_trk_ip')) {
				$user_ip = cprate_getIP();
				$feedbackdata = compact('cprate_time', 'given_to_user_id', 'given_by_user_id', 'star_rating', 'feed_back', 'feedback_moderate','user_ip');
			} else {
				$feedbackdata = compact('cprate_time', 'given_to_user_id', 'given_by_user_id', 'star_rating', 'feed_back', 'feedback_moderate');
			}
		$response = $wpdb->insert($wpdb->prefix."rate_users", $feedbackdata);
		$newid = $wpdb->insert_id;
		
			if(get_option('cprate_req_feedback_mod') && get_option('cprate_admin_note')) {
				cprate_email_mod($feed_back, $given_to_user_id,$given_by_user_id,$newid);
			}
		
		$avrg_rating = cprate_calculate($given_to_user_id)/5 * 100;
			$newrow = "<tr class='cprate-newrow'>";
				$newrow .="<td class='cpone'>".print_all_stars($star_rating)."</td>";
				$newrow .="<td class='cptwo'>";
					$newrow .="<div id='".$newid."'>".$feed_back;
					if(get_option('cprate_enable_edit_feedback') && $feedback_moderate == 0) {
						$newrow .='<img class="show_edit_box" title="Edit" src="'.plugins_url( 'css/images/edit.png', __FILE__ ).'" />';
						$newrow .='<div style="display:none" id="edit_comments_wrapper_'.$newid.'"  class="edit-com cp-collapsed">';
							$newrow .='<textarea id="text_edit_comments_'.$newid.'">'.$feed_back.'</textarea>';
							$newrow .='<input type="button" value="'.get_option('cprate_save').'" onclick="save_edit_comments('.$newid.')" />';
							$newrow .='<input class="hide_edit_box" type="button" value="'.get_option('cprate_cancel').'" />';
							$newrow .='<span id="cprate-error-rb">';
						$newrow .='</div>';
					} 
					$newrow .="</div>";
					$newrow .="<input type='hidden' id='new_rating' value='".$avrg_rating."' />";
				$newrow .="</td>";
				$newrow .="<td class='cpthre'><a href='".get_site_url()."/author/".$user->user_login."'>".$user->user_login."</a></td>";
				$newrow .="<td class='cpfour'>".date('Y-m-d', strtotime($cprate_time))."</td>";
			$newrow .="</tr>";
	echo $newrow;
}

function save_edit_comments_callback() {	
	global $wpdb;
		$edit_feedback_nonce = $_POST['edit_feedback_nonce'];
			if(! wp_verify_nonce( $edit_feedback_nonce, 'myajax_edit_feedback_nonce')) {
				die( 'You failed to be authenticated!');
			}
		$id = $_POST['edit_comments_id'];
		$text = $_POST['edit_comments_text'];
		$mode_edit = $_POST['mode_edit'];
			$get_post_time = $wpdb->get_row("SELECT cprate_time FROM $wpdb->prefix"."rate_users WHERE id ='".$id."'");
			$post_time = strtotime($get_post_time->cprate_time);
			$edit_min = get_option('cprate_poster_edit_time');
			$editable_time = strtotime("+$edit_min minutes",$post_time);
			$now_time = strtotime(current_time('mysql'));
				if($editable_time > $now_time || get_option('cprate_poster_edit_time') == '0' && $mode_edit == '') {
					$wpdb->query("UPDATE $wpdb->prefix"."rate_users SET feed_back='".$text."', feed_back_edit_time='".current_time('mysql')."' WHERE id=".$id);
						$output = $text;
						$output .='<img class="show_edit_box" title="Edit" src="'.plugins_url( 'css/images/edit.png', __FILE__ ).'" />';
						$output .='<div style="display:none" id="edit_comments_wrapper_'.$id.'"  class="edit-com cp-collapsed">';
							$output .='<textarea id="text_edit_comments_'.$id.'">'.$text.'</textarea>';
							$output .='<input type="button" value="'.get_option('cprate_save').'" onclick="save_edit_comments('.$id.')" />';
							$output .='<input class="hide_edit_box" type="button" value="'.get_option('cprate_cancel').'" />';
							$output .='<span id="cprate-error-rb">';
						$output .='</div>';
						
				}else if($mode_edit == 'feed') {
					$wpdb->query("UPDATE $wpdb->prefix"."rate_users SET feed_back='".$text."', feed_back_edit_time='".current_time('mysql')."' WHERE id=".$id);
						$output = $text;
						$output .='<img class="show_edit_box" title="Edit" src="'.plugins_url( 'css/images/edit.png', __FILE__ ).'" />';
						$output .='<div style="display:none" id="edit_comments_wrapper_'.$id.'"  class="edit-com cp-collapsed">';
							$output .='<textarea id="text_edit_comments_'.$id.'">'.$text.'</textarea>';
							$output .='<input type="button" class="save_edit_comments" value="'.get_option('cprate_save').'" name="'.$id.'" />';
							$output .='<input class="hide_edit_box" type="button" value="'.get_option('cprate_cancel').'" />';
							$output .='<span id="cprate-error-rb">';
						$output .='</div>';
				}else if($mode_edit == 'post_feed') {
					$wpdb->query("UPDATE $wpdb->prefix"."rate_users SET post_feed_back='".$text."', post_feed_back_edit_time='".current_time('mysql')."' WHERE id=".$id);
						$output = $text;
						$output .='<img class="show_edit_box" title="Edit" src="'.plugins_url( 'css/images/edit.png', __FILE__ ).'" />';
						$output .='<div style="display:none" id="edit_comments_wrapper_'.$id.'"  class="edit-com cp-collapsed">';
							$output .='<textarea id="text_edit_comments_'.$id.'">'.$text.'</textarea>';
							$output .='<input type="button" class="save_edit_sub_comments" value="'.get_option('cprate_save').'" name="'.$id.'" />';
							$output .='<input class="hide_edit_box" type="button" value="'.get_option('cprate_cancel').'" />';
							$output .='<span id="cprate-error-rb">';
						$output .='</div>';
				} else {
					$output='<span class="expired">'.get_option('cprate_edit_exp_msg').'</span>';
				}
	echo $output;
}

function save_sub_comments_callback() {	
	global $wpdb;
		$post_feedback_nonce = $_POST['post_feedback_nonce'];
			if(! wp_verify_nonce( $post_feedback_nonce, 'myajax_post_feedback_nonce')) {
				die( 'You failed to be authenticated!');
			}
		$id = $_POST['sub_comments_id'];
		$text = $_POST['sub_comments_text'];
		$wpdb->query("UPDATE $wpdb->prefix"."rate_users SET post_feed_back ='".$text."', post_feed_back_edit_time ='".current_time('mysql')."' WHERE id = ".$id);
			if(get_option('cprate_enable_edit_feedback')) {
				$output ='<span>'.get_option('cprate_sub_comment').':  </span>'.$text;
				$output .='<img class="show_sub_edit_box" title="Edit" src="'.plugins_url( 'css/images/edit.png', __FILE__ ).'" />';
				$output .='<div style="display:none" id="edit_sub_comments_wrapper_'.$id.'"  class="edit-sub-com cp-collapsed">';
					$output .='<textarea id="text_edit_sub_comments_'.$id.'">'.$text.'</textarea>';
					$output .='<input type="button" value="'.get_option('cprate_save').'" onclick="save_edit_sub_comments('.$id.')" />';
					$output .='<input class="hide_edit_sub_box" type="button" value="'.get_option('cprate_cancel').'" />';
					$output .='<span id="cprate-error-rb">';
				$output .='</div>';
			} else {
				$output ='<div class="sub-comments-quote"><span>'.get_option('cprate_sub_comment').':  </span>'.$text.'</div>';
			}	
	echo $output;	
}

function save_edit_sub_comments_callback() {	
	global $wpdb;
		$edit_sub_feedback_nonce = $_POST['edit_sub_feedback_nonce'];
			if(! wp_verify_nonce( $edit_sub_feedback_nonce, 'myajax_edit_sub_feedback_nonce')) {
				die( 'You failed to be authenticated!');
			}
		$id = $_POST['edit_sub_comments_id'];
		$text = $_POST['edit_sub_comments_text'];
			$get_post_time = $wpdb->get_row("SELECT post_feed_back_edit_time FROM $wpdb->prefix"."rate_users WHERE id ='".$id."'");
			$post_time = strtotime($get_post_time->cprate_time);
			$edit_min = get_option('cprate_poster_edit_time');
			$editable_time = strtotime("+$edit_min minutes",$post_time);
			$now_time = strtotime(current_time('mysql'));
				if($editable_time > $now_time || get_option('cprate_poster_edit_time') == '0') {
					$wpdb->query("UPDATE $wpdb->prefix"."rate_users SET post_feed_back='".$text."', feed_back_edit_time='".current_time('mysql')."' WHERE id=".$id);
						$output ='<span>'.get_option('cprate_sub_comment').':  </span>'.$text;
						$output .='<img class="show_sub_edit_box" title="Edit" src="'.plugins_url( 'css/images/edit.png', __FILE__ ).'" />';
						$output .='<div style="display:none" id="edit_sub_comments_wrapper_'.$id.'"  class="edit-sub-com cp-collapsed">';
							$output .='<textarea id="text_edit_sub_comments_'.$id.'">'.$text.'</textarea>';
							$output .='<input type="button" value="'.get_option('cprate_save').'" onclick="save_edit_sub_comments('.$id.')" />';
							$output .='<input class="hide_edit_sub_box" type="button" value="'.get_option('cprate_cancel').'" />';
							$output .='<span id="cprate-error-rb">';
						$output .='</div>';
				} else {
					$output='<span class="expired">'.get_option('cprate_edit_exp_msg').'</span>';
				}
		echo $output;	
}

function print_cprate($id,$hide) {
	global $current_user;
		get_currentuserinfo();
		$hide_this = ($hide) ? 'style="display:none"' :'';
		$userid=$current_user->ID;
		$cprate = '<div id="cprate_wrapper" '.$hide_this.'>';
			if ((is_user_logged_in()) && ($id!=$userid) ) {
				if(feedback_already_exists($id) == false || (get_option('cprate_multi_feedback'))) {
					$cprate .= cprate_feedback_form();
				}
			}
		$cprate .= cprate_my_rating($id);
		$cprate .= cprate_my_score($id);
		$cprate .= (get_option('cprate_enable_legend')) ? cprate_my_legend($id) : '';
		$cprate .= cprate_feedback_history($id);
		$cprate .= '</div>';
		echo $cprate;
}


function cp_rate_users($id,$hide) {
	global $legacy_install;
	$legacy_install = true;
	$star = plugins_url('cprate/css/images/empty-star-small.png');
	$count = cprating_count($id);
	$mod_req_switch = 'off';
		if(get_option('cprate_req_feedback_mod')) {
			$mod_req_switch = 'on';
		}
		wp_localize_script('cprate_js', 'cpRateVars', 
			array(
				'cprate_ajax' => admin_url('admin-ajax.php'),
				'cprate_showmore' => get_option('cprate_showmore'),
				'cprate_showall' => get_option('cprate_showall'),
				'cprate_reply' => get_option('cprate_reply'),
				'cprate_cancel' => get_option('cprate_cancel'),
				'cprate_editsaved_msg' => get_option('cprate_editsaved_msg'),
				'stars' => $star, 
				'total_count' => $count, 
				'given_to_user_id' => $id,
				'mod_req_set' => $mod_req_switch, 
				'post_feedback_nonce' => wp_create_nonce( 'myajax_post_feedback_nonce' ),
				'edit_feedback_nonce' => wp_create_nonce( 'myajax_edit_feedback_nonce' ),
				'edit_sub_feedback_nonce' => wp_create_nonce( 'myajax_edit_sub_feedback_nonce' )
			));
		print_cprate($id,$hide);
}?>