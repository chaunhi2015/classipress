/*
Classipress Rate Authors PRO
Current Version: 4.0
Plugin Author: Julio Gallegos
Author URL: http://wprabbits.com
*/

jQuery(document).ready(function($) {   
		var data = {
			action: "cprate_my_rating_action",
			this_id: cprateAdPage.cprate_id
		};
		$.post(cprateAdPage.cprate_ajax, data, function(response) {	
			var resp=response;
			response=resp.substring(0,resp.length-1);
			$("ul.member").first().append(response);		
		});
});