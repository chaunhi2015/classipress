/*
Classipress Rate Authors PRO
Current Version: 4.0
Plugin Author: Julio Gallegos
Author URL: http://wprabbits.com
*/


jQuery(document).ready(function($) {
	var wrap = $("div.content_right").find("#cprate_wrapper");
		if(wrap) {
			$("#user-photo").parent().append($("div.content_right").find("#cprate_wrapper").show());
		}
	
	$("#cprate_comments").val(""); 
  	$('#stars-wrapper').ratings(5).on('ratingchanged', function(event, data) {
    	$('#rating_show').text(data.rating);
  	});
	$("#leave_feedback").click(function() {
		$("#cprate_feedback_form").slideDown(500);
		$(this).hide();
		$("#cancel_feedback").show();
	});
	$("#cancel_feedback").click(function() {
		$("#cprate_feedback_form").slideUp(500);
		$(this).hide();
		$("#leave_feedback").show();
	});
	$("#tbody_feedback_history").on("click","img.show_edit_box, input.hide_edit_box",function(){
		var par = $(this).closest("td").find("div.edit-com");
			if(par.hasClass('cp-collapsed')) {
				$(par).removeClass('cp-collapsed');
				$(par).slideDown(500);
			} else {
				$(par).addClass('cp-collapsed');
				$(par).slideUp(500);
			}				
	}).on("click","img.show_sub_edit_box, input.hide_edit_sub_box",function(){
		var par = $(this).closest("td").find("div.edit-sub-com");
			if(par.hasClass('cp-collapsed')) {
				$(par).removeClass('cp-collapsed');
				$(par).slideDown(500);
			} else {
				$(par).addClass('cp-collapsed');
				$(par).slideUp(500);
			}				
	}).on("click","input.show_sub_box",function(){
		var par = $(this).next();
			if(par.hasClass('cp-collapsed')) {
				$(this).val(cpRateVars.cprate_cancel);
				$(par).removeClass('cp-collapsed');
				$(par).slideDown(500);
			} else {
				$(par).addClass('cp-collapsed');
				$(par).slideUp(500);
				$(this).val(cpRateVars.cprate_reply);
			}				
	});
		
	var numShown = 10; // Initial rows shown & index
	var numMore = 5; // Increment
	var cprateTbl = $('#tbody_feedback_history');
	var numRows = cprateTbl.find('tr').length; // Total # rows
	var showmore_var = cpRateVars.cprate_showmore.split(" ");
	var showmore = showmore_var[0]+ ' <span>' + numMore + '</span> '+showmore_var[1];
	var showall =  cpRateVars.cprate_showall;
	
	var pager ='<div id="cprate_more"><a class="showmore" href="javascript:void(0)" >'+showmore+'</a>';
		pager+='<nobr> | </nobr><a class="showall" href="javascript:void(0)" >'+showall+'</a></div>';
		
		if(numRows > 10) {
			cprateTbl.find('tr:gt(' + (numShown - 1) + ')').hide().end().parent().after(pager);
			if ( numRows - numShown < numMore ) {
				$('#cprate_more').find('span').html(numRows - numShown);
			}
			$('#cprate_more').find('a.showmore').click(function(){
				numShown = numShown + numMore;
					if ( numShown >= numRows ) {
						 $('#cprate_more').remove();
					}
					if ( numRows - numShown < numMore ) {
						 $('#cprate_more').find('span').html(numRows - numShown);
					}
				cprateTbl.find('tr:lt('+numShown+')').show('slow');
			});
			$('#cprate_more').find('a.showall').click(function(){
					cprateTbl.find('tr').show('slow');
					$('#cprate_more').remove();
			});
		}
});

function removeErr(tag, id) {
	jQuery(tag).on("mousedown",function() {
		if(id == 1) {
			jQuery(".error1").hide('slow').removeClass('error1');
		}
		else if(id == 2) {
			jQuery(".error2").hide('slow').removeClass('error2');
		}
		else if(id == 3) {
			jQuery(".error3").hide('slow').removeClass('error3');
		}
	});	
}

function save_edit_comments(id) {
	var edit_comments_wrap = jQuery("#edit_comments_wrapper_"+id);
	var edit_comments_text = jQuery("#text_edit_comments_"+id);	
		if(edit_comments_text.val() === "") {
			jQuery("#cprate-error-rb").show().text("You must enter a comment").addClass("error3");
			removeErr(edit_comments_text, 3);
		}
		else {
			edit_comments_wrap.empty().html("<div style='color:red'>Saving ...</div>");
				var data = {
					action: "save_edit_comments_action",
					edit_feedback_nonce: cpRateVars.edit_feedback_nonce,
					edit_comments_id: id,
					edit_comments_text: edit_comments_text.val()	
				};
			
				jQuery.post(cpRateVars.cprate_ajax, data, function(response) {
					var resp=response;
						response=resp.substring(0,resp.length-1);
						if(response.indexOf('expired') > 1) {
							jQuery("#edit_comments_wrapper_"+id).remove();
							jQuery('div#'+id).html(response);
						} else {
							edit_comments_wrap.html("<div style='color:green'>"+cpRateVars.cprate_editsaved_msg+"</div>").delay(500).fadeOut('slow',function() {
								edit_comments_wrap.parent().html(response).end().remove();	
							});
						}
					}
				);
		}
}

function save_sub_comments(id) {
	var sub_comments_wrap = jQuery("#sub_comments_wrapper_"+id);
	var sub_comments_text = jQuery("#text_sub_comments_"+id);
		if(sub_comments_text.val() === "") {
			jQuery("#cprate-error-rb").text("You must enter a comment").addClass("error3").show();
			removeErr(sub_comments_text, 3);
		}
		else {
			sub_comments_wrap.empty().html("<div style='color:red'>Saving your reply...</div>");
				var data = {
					action: "save_sub_comments_action",
					post_feedback_nonce: cpRateVars.post_feedback_nonce,
					sub_comments_id: id,
					sub_comments_text: sub_comments_text.val()	
				};
				jQuery.post(cpRateVars.cprate_ajax, data, function(response) {
					var resp=response;
						response=resp.substring(0,resp.length-1);
						sub_comments_wrap.hide().before(response).remove();
						sub_comments_wrap.fadeOut('slow',function() {
								sub_comments_wrap.parent().html(response).end().remove();	
						});
					}
				);
		}
}

function save_edit_sub_comments(id) {
	var edit_sub_comments_wrap = jQuery("#edit_sub_comments_wrapper_"+id);
	var edit_sub_comments_text = jQuery("#text_edit_sub_comments_"+id);
		if(edit_sub_comments_text.val() === "") {
			jQuery("#cprate-error-rb").text("You must enter a comment").addClass("error3").show();
			removeErr(edit_sub_comments_text, 3);
		}
		else {
			jQuery("#edit_sub_comments_wrapper_"+id).empty().html("<div style='color:red'>Saving ...</div>");
				var data = {
					action: "save_edit_sub_comments_action",
					edit_sub_feedback_nonce: cpRateVars.edit_sub_feedback_nonce,
					edit_sub_comments_id: id,
					edit_sub_comments_text: edit_sub_comments_text.val()	
				};
				jQuery.post(cpRateVars.cprate_ajax, data, function(response) {
					var resp=response;
						response=resp.substring(0,resp.length-1);
						edit_sub_comments_wrap.html("<div style='color:green'>"+cpRateVars.cprate_editsaved_msg+"</div>").delay(500).fadeOut('slow',function() {
								edit_sub_comments_wrap.parent().html(response).end().remove();	
						});
					}
				);
		}
}


function SubmitFeedback() {
	var star_rating=0;
	var feed_back = jQuery("#cprate_comments");
	var errmsg = jQuery("#cprate-error");
	var starwrap = jQuery("#stars-wrapper");
	
	if(jQuery("#rating_show").html()>=1){
		star_rating=jQuery("#rating_show").html();
	}
	if(!starwrap.find('div').hasClass('jquery-ratings-full')) {
		errmsg.show().text("You must select at least 1 star").addClass("error1");
		removeErr(starwrap, 1);
	}
	else if(feed_back.val() === "") {
		errmsg.show().text("Please enter your feedback").addClass("error2");
		removeErr(feed_back, 2);
	}
	else {
			
		jQuery("#cprate_form_wrapper").hide();
		jQuery("#cprate_loading").show();
		
		var data = {
			action: "cprate_post_feedback_action",
			post_feedback_nonce: cpRateVars.post_feedback_nonce,
			given_to_user_id: cpRateVars.given_to_user_id,
			star_rating: star_rating,
			feed_back: feed_back.val()};
			
			jQuery.post(cpRateVars.cprate_ajax, data, function(response) {
				jQuery("#cprate_feedback_form").remove();
				jQuery("#cprate_loading").remove();
				jQuery("#tbody_feedback_history").prepend(response);
					if(cpRateVars.mod_req_set == "off") {
						cpRateVars.total_count++;
					}
				jQuery("#total_count").html(cpRateVars.total_count);
				jQuery("#cprate_per").html(jQuery("#new_rating").val() + '%');
				setTimeout(function(){jQuery("tr.cprate-newrow").removeClass("cprate-newrow")}, 3000);
			});
	}
}
