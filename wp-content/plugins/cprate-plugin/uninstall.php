<?php
	global $wpdb;
	
	if (!defined( 'WP_UNINSTALL_PLUGIN' )) {
		exit();
	}
	
	delete_option('cprate_enable_replyback');
		
	delete_option('cprate_header1');
	delete_option('cprate_header2');
	delete_option('cprate_rating');
	delete_option('cprate_feedback');
	delete_option('cprate_ratedby');
	delete_option('cprate_date');
	delete_option('cprate_sub_comment');
	delete_option('cprate_reply');
	
	delete_option('cprate_button');
	delete_option('cprate_cancel');
	delete_option('cprate_submit');
	delete_option('cprate_rating_msg');
	delete_option('cprate_comnt_msg');
	delete_option('cprate_thanks');
	delete_option('cprate_submit_msg');
	
	delete_option('cprate_multi_feedback');
	delete_option('cprate_req_feedback_mod');
	delete_option('cprate_req_feedback_mod_msg');

	delete_option('cprate_auth_edit_time');
	delete_option('cprate_mod_records_show');
	delete_option('cprate_mod_sort_order');
	delete_option('cprate_records_show_toprate');
	delete_option('cprate_sort_toprate');
	
	/* Plugin Fields */
	unregister_setting('cprate-settings-group','cprate_header1');
	unregister_setting('cprate-settings-group','cprate_header2');
	unregister_setting('cprate-settings-group','cprate_rating');
	unregister_setting('cprate-settings-group','cprate_feedback');
	unregister_setting('cprate-settings-group','cprate_ratedby');
	unregister_setting('cprate-settings-group','cprate_date');
	unregister_setting('cprate-settings-group','cprate_sub_comment');
	unregister_setting('cprate-settings-group','cprate_reply');
	
	/* Form Fields */
	unregister_setting('cprate-settings-group','cprate_button');
	unregister_setting('cprate-settings-group','cprate_cancel');
	unregister_setting('cprate-settings-group','cprate_submit');
	unregister_setting('cprate-settings-group','cprate_thanks');
	unregister_setting('cprate-settings-group','cprate_submit_msg');
	unregister_setting('cprate-settings-group','cprate_rating_msg');
	unregister_setting('cprate-settings-group','cprate_comnt_msg');
	
	/* Plugin Options */
	unregister_setting('cprate-settings-group','cprate_multi_feedback');
	unregister_setting('cprate-settings-group','cprate_req_feedback_mod');
	unregister_setting('cprate-settings-group','cprate_req_feedback_mod_msg');
	unregister_setting('cprate-settings-group','cprate_rules');
	
	/* Feedback Edits */
	unregister_setting('cprate-settings-group','cprate_enable_edit_feedback');
	unregister_setting('cprate-settings-group','cprate_poster_edit_time');
	//unregister_setting('cprate-settings-group','cprate_req_replyback_mod');
	unregister_setting('cprate-settings-group','cprate_enable_replyback');
	unregister_setting('cprate-settings-group','cprate_enable_edit_replyback');
	unregister_setting('cprate-settings-group','cprate_auth_edit_time');
	
	/* Moderate Feedback Form Fields */
	unregister_setting('cprate-mod-settings-group','cprate_mod_records_show');
	unregister_setting('cprate-mod-settings-group','cprate_mod_sort_order');
	unregister_setting('cprate-mod-settings-group','cprate_show_mod_btn');
	/* Feedback Reports Form Fields */
	unregister_setting('cprate-toprate-settings-group','cprate_records_show_toprate');
	unregister_setting('cprate-toprate-settings-group','cprate_sort_toprate');

  	if(get_option('cprate_deletedb')) {
		delete_option('cprate_deletedb');
			$table_name = $wpdb->prefix."rate_users";
			$wpdb->query('DROP TABLE IF EXISTS '.$table_name); 
	}
?>