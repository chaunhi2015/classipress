=== ClassiPress eCommerce ===
Contributors: metuza
Donate link: http://www.arctic-websolutions.com/
Tags: classipress, ecommerce plugin, classipress theme, classipress ecommerce
Requires at least: 3.9.2
Tested up to: 4.2.2
Stable tag: 1.0.6

Tested against:
ClassiPress: 3.3.3+
AppThemes Invoice: 0.2.1
Skrill Gateway: 1.2

---------------------------------------------------------------------------------------------------

=== Important links ===
* [Plugin Website] (http://www.arctic-websolutions.com) - Plugin overview.
* [Demo](http://ecommerce.wp-build.com) - Testdrive every aspect of the plugin.
* [Manuals & Support](http://www.arctic-websolutions.com/) - Setup instructions and support.

=== Some Admin Features ===
* Coming..

=== Some User Features ===
* Coming..

=== Installation ===
 - Download ClassiPress eCommerce Plugin.
 - Extract the downloaded .zip file. (Not necessary if filename dosen`t say so, ie. UNZIP)
 - Login to your websites FTP and navigate to the "wp-contents/plugins/" folder.
 - Upload the resulting "classipress-ecommerce.zip" or "classipress-ecommerce-X.X.X.zip" folder to the websites plugin folder.
 - Go to your websites Dashboard, access the "plugins" section on the left hand menu.
 - Locate "ClassiPress eCommerce" in the plugin list and click "activate".
 - Make sure to backup your translations before any upgrade.

OR you can use WP plugin installer, just extract the downloaded .zip file and upload the "classipress-ecommerce-X.X.X.zip".

Visit your CLP eCommerce -> Settings, configure any options as desired. That’s it!

=== Important Information ===
* You should never update ClassiPress Theme or the AppThemes Invoice Plugin without first checking compatibility with the installed version of ClassiPress eCommerce Plugin.
* If you install (first time) or update the AppThemes Invoice Plugin so you must always deactivate / reactivate the ClassiPress eCommerce Plugin to make sure all files are beeing updated.
* You will always find the supported versions of ClassiPress Theme and the AppThemes Invoice Plugin in the header of this file or the change-log. 

If you have questions please post them in our forum, http://www.arctic-websolutions.com/forums

=== Recommended Translation ===
* [Translation] (http://www.code-styling.de/english/) - CodeStyling Localization Plugin.

