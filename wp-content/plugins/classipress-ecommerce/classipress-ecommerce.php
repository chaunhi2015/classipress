<?php

/*
Plugin Name: ClassiPress eCommerce
Plugin URI: http://www.arctic-websolutions.com
Description: The very best and most convenient way to add ecommerce functionality to your ClassiPress website.
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com
Version: 1.0.6
Requires at least: 3.9.2
Tested up to: 4.2.2
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Define plugin name
if ( ! defined( 'CLP_EC_PLUGIN' ) ) {
	define( 'CLP_EC_PLUGIN', 'ClassiPress eCommerce' );
}

// Define DB version
if ( ! defined( 'CLP_EC_DB_VERSION' ) ) {
	define( 'CLP_EC_DB_VERSION', '0.4 - October 25, 2014' );
}

// Define plugin version
if ( ! defined( 'CLP_EC_PLUGIN_VERSION' ) ) {
	define( 'CLP_EC_PLUGIN_VERSION', '1.0.6 - May 27, 2015' );
}

// Define the root directory path of this plugin
if ( ! defined( 'CLP_EC_PLUGIN_DIR' ) ) {
	define( 'CLP_EC_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

// Define the uri of this plugin
if ( ! defined( 'CLP_EC_PLUGIN_URI' ) ) {
	define( 'CLP_EC_PLUGIN_URI', plugin_dir_url( __FILE__ ) );
}

// Define the language translation textdomain
if ( ! defined( 'CLP_EC' ) ) {
	define( 'CLP_EC', 'ecommerce' );
}

	// Load the necessary files
	include_once( CLP_EC_PLUGIN_DIR .'/framework/loader.php' );
	include_once( CLP_EC_PLUGIN_DIR .'/includes/globals.php' );
	include_once( CLP_EC_PLUGIN_DIR .'/includes/functions.php' );
	include_once( CLP_EC_PLUGIN_DIR .'/includes/actions.php' );
	if ( get_option( 'cp_version' ) < '3.4' )
	include_once( CLP_EC_PLUGIN_DIR .'/includes/step-functions.php' );
	else include_once( CLP_EC_PLUGIN_DIR .'/includes/step-functions34.php' );
	include_once( CLP_EC_PLUGIN_DIR .'/includes/ajax-functions.php' );
	include_once( CLP_EC_PLUGIN_DIR .'/includes/setup-orders.php' );


global $wp_rewrite;
$wp_rewrite = new WP_Rewrite();

define( 'CLP_CART_URL', get_permalink( awsolutions_get_meta_postid( '_wp_page_template', 'tpl-shopping-basket.php' ) ) );
define( 'CLP_TRANS_URL', get_permalink( awsolutions_get_meta_postid( '_wp_page_template', 'tpl-transactions.php' ) ) );
define( 'CLP_SUCCESS_URL', get_permalink( awsolutions_get_meta_postid( '_wp_page_template', 'tpl-success.php' ) ) );


function awsolutions_ec_plugin_activate() {
	global $wpdb;

	include_once( dirname( __FILE__ ).'/includes/install.php' );
		awsolutions_plugin_activate();

	include_once( dirname( __FILE__ ).'/includes/copyus.php' );
		awsolutions_paypal_gateway();
		awsolutions_invoice_plugin();
		awsolutions_skrill_gateway();
		awsolutions_bank_transfer_gateway();
		awsolutions_order_select_override();

	$table_fields = $wpdb->prefix . "cp_ad_fields";

	$field1 = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_enable_payment'");
	if( $field1 ) {
	$where_array = array('field_name' => 'cp_enable_payment');
	$wpdb->update($table_fields, array( 'field_perm' => '1', 'field_core' => '1', 'field_req' => '0' ), $where_array);
	}
	$field2 = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_item_name'");
	if( $field2 ) {
	$where_array = array('field_name' => 'cp_item_name');
	$wpdb->update($table_fields, array( 'field_perm' => '1', 'field_core' => '1', 'field_req' => '1' ), $where_array);
	}
	$field3 = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_quantity'");
	if( $field3 ) {
	$where_array = array('field_name' => 'cp_quantity');
	$wpdb->update($table_fields, array( 'field_perm' => '1', 'field_core' => '1', 'field_req' => '1' ), $where_array);
	}

}
register_activation_hook( __FILE__, 'awsolutions_ec_plugin_activate' );


// CLP eCommerce Plugin Class.
class CLP_eCommerce_Plugin {

	// Initializes the plugin by setting filters and administration functions.
	function __construct() {

	add_action( 'plugins_loaded', array( $this, 'plugin_loaded' ) );
	add_action( 'admin_menu', array( $this, 'admin_menu' ), 99 );
	add_action( 'wp_print_scripts', array( $this, 'enqueue_scripts' ) );
	add_action( 'wp_head', array( $this, 'enqueue_styles' ) );
	add_filter( 'template_include', array( $this, 'awsolutions_template_include' ), 11 );

	$post_tab = false; if( isset( $_POST['tab'] ) ) $post_tab = $_POST['tab'];
	$tab = isset( $_GET['tab'] ) ? $_GET['tab'] : $post_tab;
	if( ! isset( $_GET['tab'] ) && empty( $post_tab ) ) $tab = 'clp_general';
	
	$this->tab = awsolutions_admin_pages( $tab );
	//add_filter( $this->tab->get_option_group() .'_settings_validate', array( &$this, 'validate_settings' ) );

	}

	function plugin_loaded() {

		// Load the language translation textdomain
		load_plugin_textdomain( CLP_EC, false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	}

	function admin_menu() {

		// Setup all links in wp admin
		$page = add_menu_page( __( 'CLP eCommerce Management', CLP_EC ), __( 'CLP eCommerce', CLP_EC ), 'manage_options', 'clp-ecommerce', array( $this, 'settings_page' ) );

		add_action( 'admin_print_styles-'.$page, array( $this, 'awsolutions_admin_styles' ) );
		add_action( 'admin_print_scripts-'.$page, array( $this, 'awsolutions_admin_scripts' ) );

	}

	// Enqueue the admin css files
	function awsolutions_admin_styles() {

		wp_register_style( 'clp-admin-style', plugins_url( 'styles/admin.css', __FILE__ ), array(), '1.0' );
		wp_enqueue_style( 'clp-admin-style' );

	}

	// Enqueue the admin script files
	function awsolutions_admin_scripts() {

		// Enqueue the ajax scripts used by plugin admin
		wp_register_script( 'clp-admin-ajax', plugins_url( 'jquery/admin.js', __FILE__ ), array( 'jquery' ), '1.0' );
		wp_enqueue_script( 'clp-admin-ajax' );

		// make the ajaxurl var available to the above script
		wp_localize_script( 'clp-admin-ajax', 'clp_admin_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

	}

	function enqueue_styles() {
		global $clp_options;

		// Enqueue the main css used by plugin
		wp_register_style( 'clp-ec-style', plugins_url( 'styles/style.css', __FILE__ ), array(), '1.0' );
		wp_enqueue_style( 'clp-ec-style' );

		$stylesheet = $clp_options['child_theme'];
		$file = plugin_dir_path(__FILE__). '/styles/childs/'.$stylesheet.'.css';
		if( file_exists( $file ) ) {
		wp_register_style( 'clp-child-style', plugins_url( 'styles/childs/'.$stylesheet.'.css', __FILE__ ), array(), '1.0' );
		wp_enqueue_style( 'clp-child-style' );
		}

	}

	function enqueue_scripts() {

		// Enqueue the main scripts used by plugin
		if ( ! is_admin() ) {
		wp_register_script( 'clp-ec-ajax', plugins_url( 'jquery/scripts.js', __FILE__ ), array( 'jquery' ), '1.0' );
		wp_enqueue_script( 'clp-ec-ajax' );
		wp_localize_script( 'clp-ec-ajax', 'clp_ec_lang', awsolutions_localize_vars() );
		wp_localize_script( 'clp-ec-ajax', 'clp_ec_opt', awsolutions_option_vars() );

		// make the ajaxurl var available to the above script
		wp_localize_script( 'clp-ec-ajax', 'clp_ec_ajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
		}

	}

	function settings_page() {

		// The admin options/settings page
		$current = isset($_GET['tab']) ? $_GET['tab'] : 'clp_general';
		if(isset($_GET['tab'])) $current = $_GET['tab']; ?>

	<div class="wrap">

		<h2><?php _e( 'ClassiPress eCommerce Management', CLP_EC ); ?></h2>

		<div class="message"></div>

	<?php
		$tabs = awsolutions_admin_tabs();
		$links = array();
		foreach( $tabs as $tab => $name ) :
		if ( $tab == $current ) :
		$links[] = "<a class='nav-tab nav-tab-active' href='?page=clp-ecommerce&tab=$tab'>$name</a>";
		else :
		$links[] = "<a class='nav-tab' href='?page=clp-ecommerce&tab=$tab'>$name</a>";
		endif;
		endforeach;
		echo '<h2 class="nav-tab-wrapper">';
		foreach ( $links as $link )
		echo $link;
		echo '</h2>';
	?>

		<div class="metabox-holder has-right-sidebar">

		<div id="post-body">
			<div id="post-body-content">

			<div class="postbox">
				<div class="inside">

				<?php
				// Output the menu tabs and settings forms
				if ( isset ( $_GET['tab'] ) ) $tab = $_GET['tab'];
				else $tab = 'clp_general';

					$this->tab->settings( $tab ); ?>

				</div>
			</div>
			</div>
		</div>

	<div class="inner-sidebar">

	<div class="postbox">

<style>
.postbox p {
	font-size: 13px;
}
.form-table td,
.form-table td p {
	font-size: 13px;
}
</style>

		<h3 style="cursor:default;"><?php _e('Information', CLP_EC); ?></h3>
		<div class="inside">

	<table class="form-table">
	<tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Readme:', CLP_EC); ?></th>
    <td><a class="button-secondary" href="<?php get_bloginfo('wpurl'); ?>/wp-content/plugins/classipress-ecommerce/readme.txt" onclick="javascript:void window.open('<?php get_bloginfo('wpurl'); ?>/wp-content/plugins/classipress-ecommerce/readme.txt','1346613040193','width=1240,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Readme File', CLP_EC); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', CLP_EC); ?></th>
    <td><a class="button-secondary" href="http://ecommerce.wp-build.net/wp-content/plugins/classipress-ecommerce/change-log.txt" onclick="javascript:void window.open('http://ecommerce.wp-build.net/wp-content/plugins/classipress-ecommerce/change-log.txt','1346613040193','width=1240,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', CLP_EC); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', CLP_EC); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', CLP_EC); ?></a></td>
    </tr>
    </tbody>
    </table>

<div class="clear15"></div>

<?php _e('<a style="text-decoration:none" href="http://www.arctic-websolutions.com" target="_blank"><strong>Arctic WebSolutions</strong></a> offers WordPress and ClassiPress plugins “as is” and with no implied meaning that they will function exactly as you would like or will be compatible with all 3rd party components and plugins. We do not offer support via email or otherwise support WordPress or other WordPress plugins we have not developed.', CLP_EC); ?>

<div class="clear15"></div>

<div class="admin_header"><?php _e( 'Other Plugins by AWSolutions', CLP_EC ); ?></div>
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/bundle-pack/" target="_blank">Bundle Pack $269.00 off</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/cp-auction-social-sharing/" target="_blank">CP Auction & eCommerce</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-grid-view/" target="_blank">ClassiPress Grid View</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-projects-portfolio/" target="_blank">AWS Projects Portfolio
</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-affiliate-plugin/" target="_blank">AWS Affiliate Plugin</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-ecommerce/" target="_blank">ClassiPress eCommerce</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/simplepay4u-gateway/" target="_blank">SimplePay4u Gateway</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/grandchild-plugin/" target="_blank">AWS Grandchild Plugin</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-theme-customizer/" target="_blank">AWS Theme Customizer</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/best-offer-for-cp-auction/" target="_blank">CP Auction Best Offer</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-best-offer/" target="_blank">ClassiPress Best Offer</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-social-sharing/" target="_blank">ClassiPress Social Sharing</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-cartpauj-pm/" target="_blank">ClassiPress Cartpauj PM</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-deny-access/" target="_blank">AWS Deny Access</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-user-roles/" target="_blank">AWS User Roles</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-embed-video/" target="_blank">AWS Embed Video</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank">AWS Credit Payments</a>', CLP_EC ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">AppThemes Critic Reviews</a>', CLP_EC ); ?>

<div class="clear15"></div>

<?php _e('AppThemes has released a plugin <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> which has the same rating and review functionality as you can see around in their marketplace. The only minus is that it is not modded / available for ClassiPress Theme <strong>BUT</strong>, I have a modified version which you can review on our main site or any of our child theme demos.', CLP_EC); ?>

<div class="clear15"></div>

<?php _e('Purchase the <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> plugin through my <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">affiliate link</a>, send me a copy of your purchase receipt and i will send you my modified version of the Critic Reviews plugin.', CLP_EC); ?>

<div class="clear15"></div>

	<p><strong><?php _e('Was this plugin useful? If you like it and it is/was useful, please consider donating.. Many thanks!', CLP_EC); ?></strong></p>

	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="RFJB58JUE8WUJ">
	<p style="text-align: center;">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	</p>
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>

<div class="clear"></div>

<?php _e('<strong>Delete Orders:</strong> If you have performed several test transactions so you can use the below option to run a query that will delete ALL registered orders. Make sure to backup your database before you attempt to delete all orders.', CLP_EC); ?>

	<table class="form-table">
	<tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Delete All Orders:', CLP_EC); ?></th>
    <td><?php wp_nonce_field( 'ajax-admin-nonce', 'security' ); ?><button id="delete-test-orders" class="button-secondary"><?php _e('Delete', CLP_EC); ?></button></td>
    </tr>
    </tbody>
    </table>

<?php _e('<strong>WARNING!!</strong> The above option will delete ALL registered orders!! <strong>Never use this option on a live site</strong>.', CLP_EC); ?>

            </div>
          </div>
        </div>
	</div>
</div>

<?php
}

	function validate_settings( $input ) {
		// Do your settings validation here
		// Same as $sanitize_callback from http://codex.wordpress.org/Function_Reference/register_setting
		//return $input;
	}

	function awsolutions_template_include( $template ) {

		$new_template = basename( $template );
		if ( get_option( 'cp_version' ) < '3.4' && $new_template == 'tpl-dashboard.php' ) {
		$new_template = 'tpl-dashboard33.php';
		}

		if ( get_option( 'cp_version' ) < '3.4' ) {
		$tpl_array = array( 'order-checkout.php', 'order-summary.php', 'tpl-dashboard33.php' );
		} else {
		$tpl_array = array( 'tpl-dashboard.php' );
		}
		
		// Search for templates in plugin 'templates' dir, and load if exists
		if ( in_array($new_template, $tpl_array) && file_exists( untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . $new_template ) )
		$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . $new_template;

		return $template;
	}

}
new CLP_eCommerce_Plugin();


// CLP eCommerce Get Template Class.
class CLpPageTemplater {

	// A Unique Identifier
	protected $plugin_slug;

	// A reference to an instance of this class.
        private static $instance;

	// The array of templates that this plugin tracks.
        protected $templates;

	// Returns an instance of this class.
        public static function get_instance() {

                if( null == self::$instance ) {
                        self::$instance = new CLpPageTemplater();
                } 
                	return self::$instance;
        }

	// Initializes the plugin by setting filters and administration functions.
        private function __construct() {

                $this->templates = array();

                // Add a filter to the attributes metabox to inject template into the cache.
                add_filter( 'page_attributes_dropdown_pages_args', array( $this, 'register_project_templates' ) );

                // Add a filter to the save post to inject out template into the page cache
                add_filter( 'wp_insert_post_data', array( $this, 'register_project_templates' ) );

                // Add a filter to the template include to determine if the page has our template assigned and return it's path
                add_filter( 'template_include', array( $this, 'view_project_template') );

                // Add your templates to this array.
                $this->templates = array(
                	'tpl-shopping-basket.php' => __( 'Shopping Cart', CLP_EC ),
                	'tpl-success.php' => __( 'Payment Success', CLP_EC )
                );
        } 


	// Adds our template to the pages cache in order to trick WordPress into thinking the template file exists where it doens't really exist.
        public function register_project_templates( $atts ) {

                // Create the key used for the themes cache
                $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

                // Retrieve the cache list. If it doesn't exist, or it's empty prepare an array
                $templates = wp_get_theme()->get_page_templates();
                if ( empty( $templates ) ) {
                        $templates = array();
                }

                // New cache, therefore remove the old one
                wp_cache_delete( $cache_key , 'themes');

                // Now add our template to the list of templates by merging our templates with the existing templates array from the cache.
                $templates = array_merge( $templates, $this->templates );

                // Add the modified cache to allow WordPress to pick it up for listing available templates
                wp_cache_add( $cache_key, $templates, 'themes', 1800 );

                return $atts;

        }

	// Checks if the template is assigned to the page
        public function view_project_template( $template ) {

                global $post;

                if (!isset($this->templates[get_post_meta( $post->ID, '_wp_page_template', true )] ) ) {

                        return $template;

                }

                $file = plugin_dir_path(__FILE__). '/templates/'. get_post_meta( $post->ID, '_wp_page_template', true );

                // Just to be safe, we check if the file exist first
                if( file_exists( $file ) ) {
                        return $file;
                }
			else { echo $file; }

                return $template;

        } 

}
add_action( 'plugins_loaded', array( 'CLpPageTemplater', 'get_instance' ) );


// Deactivate the plugin
function awsolutions_deactivate_aws_ecommerce() {
	global $wpdb;

	$table_fields = $wpdb->prefix . "cp_ad_fields";

	$field1 = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_enable_payment'");
	if( $field1 ) {
	$where_array = array('field_name' => 'cp_enable_payment');
	$wpdb->update($table_fields, array( 'field_perm' => '0', 'field_core' => '0', 'field_req' => '0' ), $where_array);
	}
	$field2 = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_item_name'");
	if( $field2 ) {
	$where_array = array('field_name' => 'cp_item_name');
	$wpdb->update($table_fields, array( 'field_perm' => '0', 'field_core' => '0', 'field_req' => '0' ), $where_array);
	}
	$field3 = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_quantity'");
	if( $field3 ) {
	$where_array = array('field_name' => 'cp_quantity');
	$wpdb->update($table_fields, array( 'field_perm' => '0', 'field_core' => '0', 'field_req' => '0' ), $where_array);
	}

}
register_deactivation_hook( __FILE__, 'awsolutions_deactivate_aws_ecommerce' );


// Uninstall the plugin
function awsolutions_uninstall_aws_ecommerce() {

	require_once dirname( __FILE__ ) . '/includes/aws-uninstall.php';
}
if( aws_get_setting( 'clp_general', 'main', 'delete_data' ) == "yes" )
register_uninstall_hook( __FILE__, 'awsolutions_uninstall_aws_ecommerce' );

?>