jQuery(document).ready(function($) {

	var sum_total = +clp_ec_opt.SumTotal;

	if ( clp_ec_opt.EnableSideBar == 'true' && clp_ec_opt.LoggedIn == 'true' && clp_ec_opt.ItemsInCart > 0 ) {
	jQuery('div.content_right').prepend( "<div class='shadowblock_out widget-shopping-cart' id='widget-shopping-cart'><div class='shadowblock'><h2 class='dotted'>" + clp_ec_lang.CartSummary + " (" + clp_ec_opt.ItemsInCart + " items)</h2><div class='cart-img'></div><center><h3>" + clp_ec_lang.Total + " " + cp_currency_position(sum_total.toFixed(2)) + "</h3><div class='padd10'></div><button onclick=\"location.href='" + clp_ec_opt.CartURL + "'\">" + clp_ec_lang.ProceedCheckout + "</button></center></div></div>" );
	}

	clp_add_to_cart();
	clp_setup_cart_orders();
	clp_setup_direct_order();
	clp_empty_cart();
	clp_mark_as_paid();
	clp_mark_as_failed();
	clp_update_order();
	clp_view_order_details();

});


// display the showbox when items added to cart
function clp_add_to_cart() {

	// add showbox/shadow <div/>`s if not previously added
	jQuery('.add-cart').click(function() {

		var theShowbox = jQuery('<div id="showbox"/>');
		var theShadow = jQuery('<div id="showbox-shadow"/>');
		var theCloseUp = jQuery('<div id="close-up"/>');
		var pid = jQuery(this).attr("id");
		var new_item = jQuery("#new-item").val();
		var mode = jQuery("#mode").val();
		var result = jQuery("#result").val();
		var price = +clp_ec_opt.ItemPrice;
		var reg_quantity = +jQuery("#reg-quantity").val();
		var add_quantity = +jQuery("#quantity_" + pid + "").val();
		var quantity = add_quantity + reg_quantity;
		var items_total = +clp_ec_opt.ItemsInCart + add_quantity;
		var price_tax = +clp_ec_opt.ItemPriceTax * add_quantity;
		var sum_total = +clp_ec_opt.SummerTotal + price_tax;
		var net_total = price * quantity;
		var net_totals = price * add_quantity;
		var subtotal = +clp_ec_opt.SumSubTotal + net_total;
		var subtotals = +clp_ec_opt.SumSubTotal + net_totals;
		var security = jQuery("#security_" + pid + "").val();

		jQuery(theShadow).click(function(e){
			closeShowbox();
		});

		jQuery(theCloseUp).click(function(e){
			closeShowbox();
		});

		if( ! mode ) {
		jQuery('body').append(theShadow);
		jQuery('body').append(theShowbox);
		}

		// request AJAX content
		jQuery.ajax({
			url: clp_ec_ajax.ajaxurl,
			beforeSend: function() {
				jQuery(".loader_" + pid + "").addClass('clp-loader');
			},
			type : "post",
			dataType : "json",
			data : {
				action: 'setup_order',
				post_id: pid,
				price: price,
				quantity: add_quantity,
				security: security,
				sub_total: net_total,
				type: 'cart'
	},			
	success: function( data ) {
	if( data && data.success ) {

	jQuery(".loader_" + pid + "").removeClass('clp-loader');
	jQuery('#showbox').empty();

	if( new_item == 'true' ) {
	if( mode == 'simple' ) {
	jQuery(".notice").remove();
	jQuery(".content_left").prepend("<div class='notice success'><div class='checked_green'></div><div><strong><a href='" + clp_ec_opt.AdURL + "'>" + clp_ec_opt.ItemName + "</a> " + clp_ec_lang.WasAddedCart + " <a href='" + clp_ec_opt.CartURL + "'>" + clp_ec_lang.ViewCart + "</a></strong></div></div>");
	} else if( mode == 'sidebar' ) {
	jQuery(".notice").remove();
	jQuery(".content_left").prepend("<div class='notice success'><div class='checked_green'></div><div><strong><a href='" + clp_ec_opt.AdURL + "'>" + clp_ec_opt.ItemName + "</a> " + clp_ec_lang.WasAddedCart + "</strong></div></div>");
	if( clp_ec_opt.ItemsInCart < 1 ) {
	jQuery("div.content_right").prepend("<div class='shadowblock_out widget-shopping-cart' id='widget-shopping-cart'><div class='shadowblock'><h2 class='dotted'>" + clp_ec_lang.CartSummary + " (" + items_total + " items)</h2><div class='cart-img'></div><center><h3>" + clp_ec_lang.Total + " " + cp_currency_position(sum_total.toFixed(2)) + "</h3><div class='padd10'></div><button onclick=\"location.href='" + clp_ec_opt.CartURL + "'\">" + clp_ec_lang.ProceedCheckout + "</button></center></div></div>");
	} else {
	jQuery("#widget-shopping-cart").html("<div class='shadowblock'><h2 class='dotted'>" + clp_ec_lang.CartSummary + " (" + items_total + " items)</h2><div class='cart-img'></div><center><h3>" + clp_ec_lang.Total + " " + cp_currency_position(sum_total.toFixed(2)) + "</h3><div class='padd10'></div><button onclick=\"location.href='" + clp_ec_opt.CartURL + "'\">" + clp_ec_lang.ProceedCheckout + "</button></center></div>");
	}
	} else {
	jQuery('#showbox').append("<div class='showbox_out'><div class='showbox_in'><h2 class='dotted'>" + clp_ec_lang.ShoppingCart + "</h2>" + result + "<div class='cart-item'><a href='" + clp_ec_opt.AdURL + "'>" + clp_ec_opt.ItemName + "</a><br /><div class='quantity'>" + quantity + " x " + cp_currency_position(price.toFixed(2)) + "</div>" + clp_ec_opt.ImgURL + "</div><div class='dotted'></div><div class='padd5'></div><strong>" + clp_ec_lang.SubTotal + "</strong> " + cp_currency_position(subtotal.toFixed(2)) + "<div class='padd10'></div>" + clp_ec_opt.LinkOpt + "<button onclick=\"location.href='" + clp_ec_opt.CartURL + "'\">" + clp_ec_lang.Checkout + "</button></div></div>");

	jQuery('.showbox_out .showbox_in').prepend(theCloseUp);
	}
	if( clp_ec_opt.HeaderLink == 'true' ) {
	jQuery(".head-quantity").html("" + items_total + "");
	}
	if( clp_ec_opt.PrimaryLink == 'true' ) {
	jQuery("span.primary-quantity").html("" + items_total + "");
	}
	}
	else {
	if( mode == 'simple' ) {
	jQuery(".notice").remove();
	jQuery(".content_left").prepend("<div class='notice success'><div class='checked_green'></div><div><strong><a href='" + clp_ec_opt.AdURL + "'>" + clp_ec_opt.ItemName + "</a> " + clp_ec_lang.WasAddedCart + " <a href='" + clp_ec_opt.CartURL + "'>" + clp_ec_lang.ViewCart + "</a></strong></div></div>");
	} else if( mode == 'sidebar' ) {
	jQuery(".notice").remove();
	jQuery(".content_left").prepend("<div class='notice success'><div class='checked_green'></div><div><strong><a href='" + clp_ec_opt.AdURL + "'>" + clp_ec_opt.ItemName + "</a> " + clp_ec_lang.WasAddedCart + " </strong></div></div>");
	jQuery("#widget-shopping-cart").html("<div class='shadowblock'><h2 class='dotted'>" + clp_ec_lang.CartSummary + " (" + items_total + " items)</h2><div class='cart-img'></div><center><h3>" + clp_ec_lang.Total + " " + cp_currency_position(sum_total.toFixed(2)) + "</h3><div class='padd10'></div><button onclick=\"location.href='" + clp_ec_opt.CartURL + "'\">" + clp_ec_lang.ProceedCheckout + "</button></center></div>");
	} else {
	jQuery('#showbox').append("<div class='showbox_out'><div class='showbox_in'><h2 class='dotted'>" + clp_ec_lang.ShoppingCart + "</h2>" + result + "<div class='padd5'></div><strong>" + clp_ec_lang.SubTotal + "</strong> " + cp_currency_position(subtotals.toFixed(2)) + "<div class='padd10'></div>" + clp_ec_opt.LinkOpt + "<button onclick=\"location.href='" + clp_ec_opt.CartURL + "'\">" + clp_ec_lang.Checkout + "</button></div></div>");

	jQuery('.showbox_out .showbox_in').prepend(theCloseUp);

	jQuery("div.quantity_" + pid + "").html("" + quantity + " x " + cp_currency_position(price.toFixed(2)) + "");
	}
	if( clp_ec_opt.HeaderLink == 'true' ) {
	jQuery(".head-quantity").html("" + items_total + "");
	}
	if( clp_ec_opt.PrimaryLink == 'true' ) {
	jQuery("span.primary-quantity").html("" + items_total + "");
	}
	}

	// move the showbox to the current window top + 100px
	jQuery('#showbox').css('top', jQuery(window).scrollTop() + 100 + 'px');

	// display the showbox
	jQuery('#showbox').show();
	jQuery('#showbox-shadow').show();

	}
	else {

		jQuery(".notice").remove();
		jQuery(".loader_" + pid + "").removeClass('clp-loader');
		jQuery(".content_left").prepend(data.notices);

		}
		}
	   });

	// disable default click behavior
	return false;

   });

}

// close the showbox
function closeShowbox() {

	// hide showbox and shadow <div/>`s
	jQuery('#showbox').hide();
	jQuery('#showbox-shadow').hide();

	// remove contents of showbox in case a video or other content is actively playing
	jQuery('#showbox').empty();
}


// Process shopping cart orders.
function clp_setup_cart_orders() {

	// attach ajax query
	jQuery('.cart-orders').click(function() {

	var post_ids = jQuery(this).attr("id");
	var security = jQuery("#security").val();

		jQuery.ajax({
			url: clp_ec_ajax.ajaxurl,
			beforeSend: function() {
				jQuery(".loader").addClass('clp-loader');
			},
			type : "post",
			dataType : "json",
			data : {
				action: 'process_cart_orders',
				post_ids: post_ids,
				security: security,
				type: 'direct'
			},
			success: function( data ) {
				if ( data && data.success ) {

					// redirect to order url
					location.href = data.url;

				} else {

					jQuery(".notice").remove();
					jQuery(".loader").removeClass('clp-loader');
					jQuery(".message").append(data.notices);

				}
			}
		});
		// disable default click behavior
		return false;
     });
}


// Setup direct orders, and process same.
function clp_setup_direct_order() {

	// attach ajax query
	jQuery('.direct-order').click(function() {

	var post_id = jQuery(this).attr("id");
	var quantity = jQuery("#quantity_" + post_id + "").val();
	var security = jQuery("#security_" + post_id + "").val();

		jQuery.ajax({
			url: clp_ec_ajax.ajaxurl,
			beforeSend: function() {
				jQuery(".loader_" + post_id + "").addClass('clp-loader');
			},
			type : "post",
			dataType : "json",
			data : {
				action: 'setup_order',
				post_id: post_id,
				quantity: quantity,
				security: security,
				type: 'direct'
			},
			success: function( data ) {
				if ( data && data.success ) {

					// redirect to order url
					location.href = data.url;

				} else {

					jQuery(".notice").remove();
					jQuery(".loader_" + post_id + "").removeClass('clp-loader');
					jQuery(".content_left").prepend(data.notices);

				}
			}
		});
		// disable default click behavior
		return false;
     });
}


// Remove item(s) from shopping cart.
function clp_empty_cart() {

	// attach ajax query
	jQuery('.empty-cart').click(function() {

	var post_id = jQuery(this).attr("id");
	var order_id = jQuery("#order_" + post_id + "").val();
	var security = jQuery("#security_" + post_id + "").val();
	var slug = jQuery("#slug").val();

		jQuery.ajax({
			url: clp_ec_ajax.ajaxurl,
			beforeSend: function() {
				jQuery(".loader_" + post_id + "").addClass('clp-loader');
			},
			type : "post",
			dataType : "json",
			data : {
				action: 'empty_shopping_cart',
				post_id: post_id,
				order_id: order_id,
				security: security,
			},
			success: function( data ) {
				if ( data && data.success ) {

					jQuery(".notice").remove();
					jQuery(".loader_" + post_id + "").removeClass('clp-loader');
					jQuery(".message").append(data.notices);

				} else {

					jQuery(".notice").remove();
					jQuery(".loader_" + post_id + "").removeClass('clp-loader');
					jQuery(".message").append(data.notices);

				}
			}
		});

		jQuery(document).ajaxStop(function(){
		setTimeout("window.location = '" + slug + "'",100);
	 });

     });

}


// Author mark item as paid.
function clp_mark_as_paid() {

	// attach ajax query
	jQuery('.mark-paid').click(function() {

	var post_id = jQuery(this).attr("id");
	var security = jQuery("#security_" + post_id + "").val();
	var slug = jQuery("#slug").val();

		jQuery.ajax({
			url: clp_ec_ajax.ajaxurl,
			beforeSend: function() {
				jQuery(".loader_" + post_id + "").addClass('clp-loader');
			},
			type : "post",
			dataType : "json",
			data : {
				action: 'mark_paid',
				post_id: post_id,
				order_id: post_id,
				security: security,
			},
			success: function( data ) {
				if ( data && data.success ) {

					jQuery(".notice").remove();
					jQuery(".loader_" + post_id + "").removeClass('clp-loader');
					jQuery(".message").append(data.notices);

				} else {

					jQuery(".notice").remove();
					jQuery(".loader_" + post_id + "").removeClass('clp-loader');
					jQuery(".message").append(data.notices);

				}
			}
		});

		jQuery(document).ajaxStop(function(){
		setTimeout("window.location = '" + slug + "/#block3'",100);
	 });

     });

}


// Author mark item as failed.
function clp_mark_as_failed() {

	// attach ajax query
	jQuery('.mark-failed').click(function() {

	var post_id = jQuery(this).attr("id");
	var security = jQuery("#security_" + post_id + "").val();
	var slug = jQuery("#slug").val();

		jQuery.ajax({
			url: clp_ec_ajax.ajaxurl,
			beforeSend: function() {
				jQuery(".loader_" + post_id + "").addClass('clp-loader');
			},
			type : "post",
			dataType : "json",
			data : {
				action: 'mark_failed',
				post_id: post_id,
				order_id: post_id,
				security: security,
			},
			success: function( data ) {
				if ( data && data.success ) {

				jQuery(".notice").remove();
				jQuery(".loader_" + post_id + "").removeClass('clp-loader');
				jQuery(".message").append(data.notices);

				} else {

					jQuery(".notice").remove();
					jQuery(".loader_" + post_id + "").removeClass('clp-loader');
					jQuery(".message").append(data.notices);

				}
			}
		});

		jQuery(document).ajaxStop(function(){
		setTimeout("window.location = '" + slug + "/#block3'",100);
	 });

     });

}


// Buyer adjust item quantity from within shopping cart.
function clp_update_order() {

	// attach ajax query
	jQuery('.update-order').click(function() {

	var post_id = jQuery(this).attr("id");
	var row_id = jQuery("#id_" + post_id + "").val();
	var quantity = jQuery("#quantity_" + post_id + "").val();
	var slug = jQuery("#slug").val();
	var security = jQuery("#security_" + post_id + "").val();

		jQuery.ajax({
			url: clp_ec_ajax.ajaxurl,
			beforeSend: function() {
				jQuery(".loader_" + post_id + "").addClass('clp-loader');
			},
			type : "post",
			dataType : "json",
			data : {
				action: 'update_order',
				row_id: row_id,
				quantity: quantity,
				security: security,
			},
			success: function( data ) {
				if ( data && data.success ) {

					jQuery(".notice").remove();
					jQuery(".loader_" + post_id + "").removeClass('clp-loader');
					jQuery(".message").append(data.notices);

				} else {

					jQuery(".notice").remove();
					jQuery(".loader_" + post_id + "").removeClass('clp-loader');
					jQuery(".message").append(data.notices);

				}
			}
		});

		jQuery(document).ajaxStop(function(){
		setTimeout("window.location = '" + slug + "'",100);
	 });

     });

}


// View order details from frontend dashboard.
function clp_view_order_details() {

	// add showbox/shadow <div/>`s if not previously added
	jQuery('.view-details').click(function() {

		var theShowbox = jQuery('<div id="showbox"/>');
		var theShadow = jQuery('<div id="showbox-shadow"/>');
		var theCloseUp = jQuery('<div id="close-up"/>');
		var result = jQuery(this).attr("id");

		jQuery(theShadow).click(function(e){
			closeShowbox();
		});

		jQuery(theCloseUp).click(function(e){
			closeShowbox();
		});

		jQuery('body').append(theShadow);
		jQuery('body').append(theShowbox);

		// request AJAX content
		jQuery.ajax({
			url: clp_ec_ajax.ajaxurl,
			type : "GET",
			dataType : "json",
			success: function( data ) {

				jQuery('#showbox').append("<div class='showbox_out'><div class='showbox_in'><h2 class='dotted'>" + clp_ec_lang.OrderDetails + "</h2>" + result + "</div></div>");
				jQuery('.showbox_out .showbox_in').prepend(theCloseUp);

				// move the showbox to the current window top + 100px
				jQuery('#showbox').css('top', jQuery(window).scrollTop() + 100 + 'px');

				// display the showbox
				jQuery('#showbox').show();
				jQuery('#showbox-shadow').show();

			},
			error: function() {
				alert('AJAX Failure!');
			}

	   });

   });

}