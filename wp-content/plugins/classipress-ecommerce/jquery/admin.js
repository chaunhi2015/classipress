jQuery(document).ready(function($) {

	clp_delete_all_orders();

});


// Delete all orders, use with care.
function clp_delete_all_orders() {

	// attach ajax query
	jQuery('#delete-test-orders').click(function() {

	var security = jQuery("#security").val();

		jQuery.ajax({
			url: clp_admin_ajax.ajaxurl,
			type : "post",
			dataType : "json",
			data : {
				action: 'delete_test_orders',
				security: security,
			},
			success: function( data ) {
				if ( data && data.success ) {

					jQuery(".message").append(data.notices);

				} else {

					jQuery(".message").append(data.notices);

				}
			}
		});

     });

}