<?php
/**
 * Template Name: User Dashboard
 *
 * @package ClassiPress\Templates
 * @author  AppThemes
 * @since   ClassiPress 3.0
 */

global $i, $current_user, $wpdb, $clp_options;

$current_user = wp_get_current_user(); // grabs the user info and puts into vars
$display_user_name = cp_get_user_name();

$my_orders = get_user_meta( $current_user->ID, 'clp_my_orders', true );
if ( ! $my_orders ) $my_orders = array('0');
$my_sales = get_user_meta( $current_user->ID, 'clp_my_sales', true );
if ( ! $my_sales ) $my_sales = array('0');
$slug = basename( get_permalink() );

if ( isset( $_POST['save_changes'] ) ) {
	global $clp_errors;

	$paypal_email = isset( $_POST['paypal_email'] ) ? esc_attr( $_POST['paypal_email'] ) : '';
	$skrill_account_id = isset( $_POST['skrill_account_id'] ) ? esc_attr( $_POST['skrill_account_id'] ) : '';
	$skrill_email = isset( $_POST['skrill_email'] ) ? esc_attr( $_POST['skrill_email'] ) : '';
	$simplepay4u_email = isset( $_POST['simplepay4u_email'] ) ? esc_attr( $_POST['simplepay4u_email'] ) : '';
	$simplepay4u_nocards = isset( $_POST['simplepay4u_nocards'] ) ? esc_attr( $_POST['simplepay4u_nocards'] ) : '';
	$bt_email = isset( $_POST['bt_email'] ) ? esc_attr( $_POST['bt_email'] ) : '';
	$bt_message = isset( $_POST['bt_message'] ) ? esc_attr( $_POST['bt_message'] ) : '';

	if ( $paypal_email && ! $clp_errors ) {
		if ( is_email( $paypal_email ) ) {
		update_user_meta( $current_user->ID, 'paypal_email', $paypal_email );
		} else {
		$clp_errors = "email";
		}
	}
	else {
		delete_user_meta( $current_user->ID, 'paypal_email' );
	}

	if ( $skrill_account_id && ! $clp_errors ) {
		update_user_meta( $current_user->ID, 'skrill_account_id', $skrill_account_id );
	} else {
		delete_user_meta( $current_user->ID, 'skrill_account_id' );
	}

	if ( $skrill_email && ! $clp_errors ) {
		if ( is_email( $skrill_email ) ) {
		update_user_meta( $current_user->ID, 'skrill_email', $skrill_email );
		} else {
		$clp_errors = "email";
		}
	}
	else {
		delete_user_meta( $current_user->ID, 'skrill_email' );
	}

	if ( $simplepay4u_email && ! $clp_errors ) {
		if ( is_email( $simplepay4u_email ) || $simplepay4u_email == "touser" ) {
		update_user_meta( $current_user->ID, 'simplepay4u_email', $simplepay4u_email );
		} else {
		$clp_errors = "email";
		}
	}
	else {
		delete_user_meta( $current_user->ID, 'simplepay4u' );
	}

	if ( $simplepay4u_nocards && ! $clp_errors ) {
		update_user_meta( $current_user->ID, 'simplepay4u_nocards', $simplepay4u_nocards );
	} else {
		delete_user_meta( $current_user->ID, 'simplepay4u_nocards' );
	}

	if ( $bt_email && ! $clp_errors ) {
		if ( is_email( $bt_email ) ) {
		update_user_meta( $current_user->ID, 'bt_email', $bt_email );
		} else {
		$clp_errors = "email";
		}
	}
	else {
		delete_user_meta( $current_user->ID, 'bt_email' );
	}

	if ( $bt_message && ! $clp_errors ) {
		update_user_meta( $current_user->ID, 'bt_message', stripslashes( html_entity_decode( $bt_message ) ) );
	} else {
		delete_user_meta( $current_user->ID, 'bt_message' );
	}

		add_action( 'appthemes_notices', 'awsolutions_show_notice' );

}
?>


<div class="content user-dashboard">

	<div class="content_botbg">

		<div class="content_res">

			<!-- left block -->
			<div class="content_left">

				<?php do_action( 'appthemes_notices' ); ?>

				<div class="message"></div>

				<div class="shadowblock_out">

					<div class="shadowblock">

						<h1 class="single dotted"><?php _e( 'Bảng điều khiển', APP_TD ); ?></h1>

					</div><!-- /shadowblock -->

				</div><!-- /shadowblock_out -->

<?php if ( $clp_options['child_theme'] == "jibo" ) { ?>

<script type="text/javascript">
	jQuery(document).ready(function($) {
	if($.cookie('homeTab') == undefined) {
    	$.cookie('homeTab', '<?php echo get_option('default_tab') ?>', {expires:365, path:'/'});
	}
	$(".tabhome").tabs({
    activate: function (e, ui) {
        $.cookie('homeTab', ui.newTab.index(), { expires: 365, path: '/' });
    },
	hide:{effect: "slide", direction:"right"},
	show:{effect: "blind"},
    active: $.cookie('homeTab')
});
});
</script>

	<?php $tabs = "tabmenu"; ?>

				<div class="tabhome">

					<ul class="tabmenu">

<?php } else {

	$tabs = "tabnavig"; ?>

				<div class="tabcontrol">

					<ul class="tabnavig">
<?php } ?>
						<li><a href="#block0"><span class="big"><?php _e( 'Bài Viết', CLP_EC ); ?></span></a></li>
						<li><a href="#block1"><span class="big"><?php _e( 'Cài Đặt', CLP_EC ); ?></span></a></li>
						<li><a href="#block2"><span class="big"><?php _e( 'Đơn hàng', CLP_EC ); ?></span></a></li>
						<li><a href="#block3"><span class="big"><?php _e( 'Sales', CLP_EC ); ?></span></a></li>
					</ul>

<script type="text/javascript">
jQuery('ul.<?php echo $tabs; ?> a').click(function() {
var URL = window.location.href;
var NewURL = URL.split('#')[0];
window.history.pushState('', '', NewURL);
});
</script>

					<!-- tab 0 -->
					<div id="block0">

						<div class="clr"></div>

						<div class="undertab"><span class="big"><?php _e( 'Bài viết', CLP_EC ); ?> / <strong><span class="colour"><?php _e( 'Classified Ads', CLP_EC ); ?></span></strong></span></div>

				<div class="shadowblock_out">

					<div class="shadowblock">

						<?php if ( $listings = cp_get_user_dashboard_listings() ) : ?>

						<?php
							// build the row counter depending on what page we're on
							$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
							$posts_per_page = $listings->get( 'posts_per_page' );
							$i = ( $paged != 1 ) ? ( $paged * $posts_per_page - $posts_per_page ) : 0;
						?>

						<p><?php _e( 'Dưới đây bạn sẽ tìm thấy một danh sách của tất cả các bài viết của bạn. Nhấp chuột vào một trong các tùy chọn để thực hiện một nhiệm vụ cụ thể. Nếu bạn có bất kỳ câu hỏi, xin vui lòng liên hệ với người quản trị trang web.', APP_TD ); ?></p>

						<table border="0" cellpadding="4" cellspacing="1" class="tblwide footable">
							<thead>
								<tr>
									<th class="listing-count" data-class="expand">&nbsp;</th>
									<th class="listing-title">&nbsp;<?php _e( 'Tiêu đề', APP_TD ); ?></th>
									<?php if ( current_theme_supports( 'app-stats' ) ) { ?>
										<th class="listing-views" data-hide="phone"><?php _e( 'Lượt xem', APP_TD ); ?></th>
									<?php } ?>
									<th class="listing-status" data-hide="phone"><?php _e( 'Tình trạng', APP_TD ); ?></th>
									<th class="listing-options" data-hide="phone"><?php _e( 'Xóa', APP_TD ); ?></th>
								</tr>
							</thead>
							<tbody>

							<?php while ( $listings->have_posts() ) : $listings->the_post(); $i++; ?>

								<?php get_template_part( 'content-dashboard', get_post_type() ); ?>

							<?php endwhile; ?>

							</tbody>

						</table>

						<?php appthemes_pagination( '', '', $listings ); ?>

						<?php else : ?>

							<div class="pad10"></div>
							<p class="text-center"><?php _e( 'You currently have no classified ads.', APP_TD ); ?></p>
							<div class="pad10"></div>

						<?php endif; ?>

						<?php wp_reset_query(); ?>

					</div><!-- /block0 -->

					</div><!-- /shadowblock -->

				</div><!-- /shadowblock_out -->

					<!-- tab 1 -->
					<div id="block1">

						<div class="clr"></div>

						<div class="undertab"><span class="big"><?php _e( 'My Settings', CLP_EC ); ?> / <strong><span class="colour"><?php _e( 'Account Settings', CLP_EC ); ?></span></strong></span></div>

		<div class="shadowblock_out">

			<div class="shadowblock">

	<p>
	<?php echo nl2br( aws_get_setting( 'clp_general', 'main', 'message' ) ); ?>
	</p>

		<div class="padd10"></div>



		<form class="loginform" name="mainform" action="#block1" method="post" id="mainform">

<?php if ( awsolutions_enabled_gateways( 'paypal' ) == 1 && $clp_options['paypal'] == "paypal" ) { ?>

	<h3>
		<?php _e( 'PayPal Gateway:', CLP_EC ); ?>
	</h3>

		<div class="padd10"></div>

	<p>
		<label for="paypal_email"><?php _e( 'PayPal Email Address:', CLP_EC ); ?></label>
		<input tabindex="1" type="text" class="text" name="paypal_email" id="paypal_email" value="<?php echo get_user_meta( $current_user->ID, 'paypal_email', true ); ?>" />
	</p>

		<div class="padd10"></div>

<?php } if ( awsolutions_enabled_gateways( 'skrill' ) == 1 && $clp_options['skrill'] == "skrill" ) { ?>

	<h3>
		<?php _e( 'Skrill Gateway:', CLP_EC ); ?>
	</h3>

		<div class="padd10"></div>

	<p>
		<label for="skrill_account_id"><?php _e( 'Skrill Account ID:', CLP_EC ); ?></label>
		<input tabindex="2" type="text" class="text" name="skrill_account_id" id="skrill_account_id" value="<?php echo get_user_meta( $current_user->ID, 'skrill_account_id', true ); ?>" />
	</p>

	<p>
		<label for="skrill_email"><?php _e( 'Skrill Email Address:', CLP_EC ); ?></label>
		<input tabindex="3" type="text" class="text" name="skrill_email" id="skrill_email" value="<?php echo get_user_meta( $current_user->ID, 'skrill_email', true ); ?>" />
	</p>

		<div class="padd10"></div>

<?php } if ( awsolutions_enabled_gateways( 'simplepay4u' ) == 1 && $clp_options['simplepay4u'] == "simplepay4u" ) { ?>

	<h3>
		<?php _e( 'SimplePay4u Gateway:', CLP_EC ); ?>
	</h3>

		<div class="padd10"></div>

	<p>
		<label for="simplepay4u_email"><?php _e( 'SimplePay4u Email:', CLP_EC ); ?></label>
		<input tabindex="2" type="text" class="text" name="simplepay4u_email" id="simplepay4u_email" value="<?php echo get_user_meta( $current_user->ID, 'simplepay4u_email', true ); ?>" />
	</p>

	<p>
		<?php if ( get_user_meta( $current_user->ID, 'simplepay4u_nocards', true ) == "Y" ) ?>
		<label for="simplepay4u_nocards"><?php _e( 'Allow Card Payments:', CLP_EC ); ?></label>
		<select class="dropdown dropdownlist" name="simplepay4u_nocards" id="simplepay4u_nocards" />
		<option value="Y" <?php if ( get_user_meta( $current_user->ID, 'simplepay4u_nocards', true ) == "Y" ) { echo "selected"; } ?>><?php _e( 'No', CLP_EC ); ?></option>
		<option value="N" <?php if ( get_user_meta( $current_user->ID, 'simplepay4u_nocards', true ) == "N" ) { echo "selected"; } ?>><?php _e( 'Yes', CLP_EC ); ?></option>
		</select>
	</p>

		<div class="padd10"></div>

<?php } if ( awsolutions_enabled_gateways( 'bank-transfer' ) == 1 && $clp_options['bank_transfer'] == "bank_transfer" ) { ?>

	<h3>
		<?php _e( 'Bank Transfer:', CLP_EC ); ?>
	</h3>

		<div class="padd10"></div>

	<p>
		<label for="bt_email"><?php _e( 'Your Contact Email:', CLP_EC ); ?></label>
		<input tabindex="4" type="text" class="text" name="bt_email" id="bt_email" value="<?php echo get_user_meta( $current_user->ID, 'bt_email', true ); ?>" />
	</p>

	<p>
		<label for="bt_message"><?php _e( 'Transfer Information:', CLP_EC ); ?></label>
		<textarea id="bt_message" class="textarea" name="bt_message" tabindex="5"><?php echo get_user_meta( $current_user->ID, 'bt_message', true ); ?></textarea>
	</p>

		<div class="clr"></div>

		<div class="padd10"></div>

<?php } ?>

		<div id="checksave">

	<p class="submit">
		<input tabindex="6" class="btn_orange" type="submit" name="save_changes" id="save_changes" value="<?php _e( 'Save Changes', CLP_EC ); ?>" />
	</p>

		</div>

		</form>

					</div><!-- /block1 -->

					</div><!-- /shadowblock -->

				</div><!-- /shadowblock_out -->

					<!-- tab 2 -->

					<div id="block2">

						<div class="clr"></div>

						<div class="undertab"><span class="big"><?php _e( 'My Orders', CLP_EC ); ?> / <strong><span class="colour"><?php _e( 'Transaction Overview', CLP_EC ); ?></span></strong></span></div>

				<div class="shadowblock_out">

					<div class="shadowblock">

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							$orders = new WP_Query( array( 'posts_per_page' => 10, 'post_type' => 'transaction', 'post_status' => array( 'tr_completed', 'tr_activated', 'tr_pending' ), 'post__in' => $my_orders, 'paged' => $paged ) );

							// build the row counter depending on what page we're on
							$posts_per_page = $orders->get( 'posts_per_page' );
							if ( $paged == 1 ) $i = 0; else $i = $paged * $posts_per_page - $posts_per_page;
						?>

						<?php if ( $orders->have_posts() ) : ?>

						<p><?php _e( 'Below you will find a listing of all your orders. Click on one of the options to perform a specific task. If you have any questions, please contact the site administrator.', CLP_EC ); ?></p>

						<table border="0" cellpadding="4" cellspacing="1" class="tblwide footable">
							<thead>
								<tr>
									<th width="5px" data-class="expand">&nbsp;</th>
									<th width="60px" class="text-left">&nbsp;<?php _e( 'OrderID', CLP_EC ); ?></th>
									<th width="100px" data-hide="phone"><div style="text-align: center;"><?php _e( 'Date', CLP_EC ); ?></div></th>
									<th width="100px" data-hide="phone"><?php _e( 'Seller', CLP_EC ); ?></th>
									<th width="80px" data-hide="phone"><?php _e( 'Status', CLP_EC ); ?></th>
									<th width="90px" data-hide="phone"><div style="text-align: center;"><?php _e( 'Options', CLP_EC ); ?></div></th>
								</tr>
							</thead>
							<tbody>

							<?php while( $orders->have_posts() ) : $orders->the_post(); $i++; ?>

								<?php

									$table_trans = $wpdb->prefix . "clp_transactions";
									$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$post->ID'");
									$paid_date = date_i18n('Y-m-d H:i:s', $row->paid_date, true);

									$results = $wpdb->get_results("SELECT * FROM {$table_trans} WHERE order_id = '$post->ID'");
									$result = false;
									$taxes = 0;
									$sub_total = false;
									foreach ( $results as $res ) {
									$img_url = awsolutions_image_url( $res->post_id );
									$ad_url = get_permalink( $res->post_id );
									$price = cp_display_price( $res->price, 'ad', false );
									$item_title = $res->item_name;
									if ( empty( $item_title ) ) $item_title = $res->post_title;
									$result .= "<div class='cart-item'><a href='$ad_url'>$item_title</a><br /><div class='quantity_$res->post_id'>$res->quantity x $price</div>$img_url</div><div class='dotted'></div>";

									$taxes += $res->taxes;
									$sub_total += $res->sub_total;
									}

									$total = $taxes + $sub_total;
									$result = "$result<div class='padd5'></div><div class='sub-total'><span class='label-one'>".__( 'Subtotal:', CLP_EC )."<span class='label-two'>".cp_display_price( $sub_total, 'ad', false )."</span></span></div><div class='clr'></div><div class='taxes'><span class='label-one'>".__( 'Taxes:', CLP_EC )."<span class='label-two'>".cp_display_price( $taxes, 'ad', false )."</span></span></div><div class='clr'></div><div class='dotted'></div><div class='total'><span class='label-one'>".__( 'Total:', CLP_EC )."<span class='label-two' style='font-weight: bold;'>".cp_display_price( $total, 'ad', false )."</span></span></div>";

									// now let's figure out what the transaction status and options should be
									// it's a live and published ad
									if ( $post->post_status == 'tr_activated' ) {

										$post_status = 'live';
										$post_status_name = __( '<strong>Paid</strong>', CLP_EC ) . '<br /><p class="small">(' . $paid_date . ')</p>';
										$fontcolor = '#33CC33';

									// it's a pending ad which gives us several possibilities
									} elseif ( $post->post_status == 'tr_pending' || $post->post_status == 'tr_completed' ) {

										if ( cp_have_pending_payment( $post->ID ) ) {
											$post_status = 'pending_payment';
											$post_status_name = __( 'Awaiting payment', CLP_EC );
											$fontcolor = '#C00202';
											$postimage = '';
											$postalt = '';
											$postaction = 'pending';
										} else {
											$post_status = 'pending';
											$post_status_name = __( 'Awaiting approval', CLP_EC );
											$fontcolor = '#C00202';
											$postimage = '';
											$postalt = '';
											$postaction = 'pending';
										}

									} elseif ( $post->post_status == 'tr_failed' ) {

										// current date is past the expires date so mark ad ended
										if ( current_time('timestamp') > $expire_time ) {
											$post_status = 'failed_payment';
											$post_status_name = __( 'Payment failed', CLP_EC ) . '<br /><p class="small">(' . $expire_date . ')</p>';
											$fontcolor = '#666666';
											$postimage = '';
											$postalt = '';
											$postaction = 'failed';
										}

									} else {
										$post_status = '&mdash;';
									}
								?>


								<tr class="even">
									<td class="text-right"><?php echo $i; ?>.</td>

									<td><button class="view-details" id="<?php echo $result; ?>" title="<?php _e( 'View order details.', CLP_EC ); ?>"><?php echo $post->ID; ?></button></td>

									<td><?php echo appthemes_display_date( $row->paid_date, 'date' ); ?></td>

									<td class="text-center"><a href="<?php echo get_author_posts_url( $row->post_author ); ?>"><?php echo get_the_author_meta( 'display_name', $row->post_author ); ?></a></td>

									<td class="text-center"><span style="color:<?php echo $fontcolor; ?>;"><?php echo $post_status_name; ?></span></td>

									<td class="text-center">
										<?php

											// delete
											//$delete_url = add_query_arg( array( 'aid' => $post->ID, 'action' => 'delete' ), CLP_TRANS_URL );
											//echo html( 'a', array( 'href' => $delete_url, 'onclick' => 'return confirmBeforeDeleteAd();', 'title' => __( 'Delete Ad', CLP_EC ) ), __( 'Delete', CLP_EC ) ) . ' ';

											// pay, reset gateway
											if ( $post_status == 'pending_payment' ) {
												if ( $clp_options['cp_version'] < '3.4' )
												$order_url = cp_get_order_permalink( $post->ID );
												else $order_url = appthemes_get_order_url( $post->ID );
												$cancel_url = get_the_order_cancel_url( $post->ID );
												echo '<br />' . html( 'a', array( 'href' => $order_url ), __( 'Pay now', APP_TD ) );
												if ( $clp_options['cp_version'] < '3.4' )
												echo '<br />' . html( 'a', array( 'href' => esc_url( add_query_arg( 'cancel', true, $order_url ) ), 'title' => __( 'Reset Payment Gateway', APP_TD ) ), __( 'Reset Gateway', APP_TD ) );
												else echo '<br />' . html( 'a', array( 'href' => $cancel_url ), __( 'Reset Gateway', APP_TD ) );
											}
											else if ( $post_status == 'pending' ) {
											if ( defined( 'NC_INVOICES_TD' ) ) {
											$invoice = ''.get_bloginfo('wpurl').'/wp-content/plugins/appthemes-invoices/invoice.php?id='.$post->ID.'&type=pdf';
											$invoice_img = html( 'img', array( 'src' => CLP_EC_PLUGIN_URI . '/styles/images/invoice.png', 'border' => '0', 'title' => __( 'Download Invoice', CLP_EC ), 'alt' => __( 'Invoice', CLP_EC ) ) );
											echo html( 'a', array( 'href' => $invoice, 'target' => '_blank', 'title' => __( 'Download Invoice', CLP_EC ) ), $invoice_img ) . ' ';
											}
											else {
											echo __( 'N/A', CLP_EC );
											}
											}
											else if ( $post_status == 'live' ) {
											if ( defined( 'NC_INVOICES_TD' ) ) {
											$invoice = ''.get_bloginfo('wpurl').'/wp-content/plugins/appthemes-invoices/invoice.php?id='.$post->ID.'&type=pdf';
											$invoice_img = html( 'img', array( 'src' => CLP_EC_PLUGIN_URI . '/styles/images/invoice.png', 'border' => '0', 'title' => __( 'Download Invoice', CLP_EC ), 'alt' => __( 'Invoice', CLP_EC ) ) );
											echo html( 'a', array( 'href' => $invoice, 'target' => '_blank', 'title' => __( 'Download Invoice', CLP_EC ) ), $invoice_img ) . ' ';
											}
											else {
											echo __( 'N/A', CLP_EC );
											}
											}
										?>

									</td>

								</tr>

							<?php endwhile; ?>

							</tbody>

						</table>

						<?php appthemes_pagination( '', '', $orders ); ?>

						<?php else : ?>

							<div class="padd10"></div>
							<p class="text-center"><?php _e( 'You currently have no orders.', CLP_EC ); ?></p>
							<div class="padd10"></div>

						<?php endif; ?>

						<?php wp_reset_query(); ?>

					</div><!-- /block2 -->

					</div><!-- /shadowblock -->

				</div><!-- /shadowblock_out -->

					<!-- tab 3 -->
					<div id="block3">

						<div class="clr"></div>

						<div class="undertab"><span class="big"><?php _e( 'My Sales', CLP_EC ); ?> / <strong><span class="colour"><?php _e( 'Transaction Overview', CLP_EC ); ?></span></strong></span></div>

				<div class="shadowblock_out">

					<div class="shadowblock">

						<?php
							// setup the pagination and query
							$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
							$sales = new WP_Query( array( 'posts_per_page' => 10, 'post_type' => 'transaction', 'post_status' => array( 'tr_completed', 'tr_activated', 'tr_pending' ), 'post__in' => $my_sales, 'paged' => $paged ) );

							// build the row counter depending on what page we're on
							$posts_per_page = $sales->get( 'posts_per_page' );
							if ( $paged == 1 ) $i = 0; else $i = $paged * $posts_per_page - $posts_per_page;
						?>

						<?php if ( $sales->have_posts() ) : ?>

						<p><?php _e( 'Below you will find a listing of all your sales. Click on one of the options to perform a specific task. If you have any questions, please contact the site administrator.', CLP_EC ); ?></p>

						<table border="0" cellpadding="4" cellspacing="1" class="tblwide footable">
							<thead>
								<tr>
									<th width="5px" data-class="expand">&nbsp;</th>
									<th width="60px" class="text-left">&nbsp;<?php _e( 'OrderID', CLP_EC ); ?></th>
									<th width="100px" data-hide="phone"><div style="text-align: center;"><?php _e( 'Date', CLP_EC ); ?></div></th>
									<th width="100px" data-hide="phone"><?php _e( 'Buyer', CLP_EC ); ?></th>
									<th width="80px" data-hide="phone"><?php _e( 'Status', CLP_EC ); ?></th>
									<th width="105px" data-hide="phone"><div style="text-align: center;"><?php _e( 'Options', CLP_EC ); ?></div></th>
								</tr>
							</thead>
							<tbody>

							<?php while( $sales->have_posts() ) : $sales->the_post(); $i++; ?>

								<?php

									$table_trans = $wpdb->prefix . "clp_transactions";
									$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$post->ID'");
									$paid_date = date_i18n('Y-m-d H:i:s', $row->paid_date, true);

									$results = $wpdb->get_results("SELECT * FROM {$table_trans} WHERE order_id = '$post->ID'");
									$result = false;
									$taxes = 0;
									$sub_total = false;
									foreach ( $results as $res ) {
									$img_url = awsolutions_image_url( $res->post_id );
									$ad_url = get_permalink( $res->post_id );
									$price = cp_display_price( $res->price, 'ad', false );
									$item_title = $res->item_name;
									if ( empty( $item_title ) ) $item_title = $res->post_title;
									$result .= "<div class='cart-item'><a href='$ad_url'>$item_title</a><br /><div class='quantity_$res->post_id'>$res->quantity x $price</div>$img_url</div><div class='dotted'></div>";

									$taxes += $res->taxes;
									$sub_total += $res->sub_total;
									}

									$total = $taxes + $sub_total;
									$result = "$result<div class='padd5'></div><div class='sub-total'><span class='label-one'>".__( 'Subtotal:', CLP_EC )."<span class='label-two'>".cp_display_price( $sub_total, 'ad', false )."</span></span></div><div class='clr'></div><div class='taxes'><span class='label-one'>".__( 'Taxes:', CLP_EC )."<span class='label-two'>".cp_display_price( $taxes, 'ad', false )."</span></span></div><div class='clr'></div><div class='dotted'></div><div class='total'><span class='label-one'>".__( 'Total:', CLP_EC )."<span class='label-two' style='font-weight: bold;'>".cp_display_price( $total, 'ad', false )."</span></span></div>";

									// now let's figure out what transaction status and options should be
									// it's a live and published ad
									if ( $post->post_status == 'tr_activated' ) {

										$post_status = 'live';
										$post_status_name = __( '<strong>Paid</strong>', CLP_EC ) . '<br /><p class="small">(' . $paid_date . ')</p>';
										$fontcolor = '#33CC33';

									// it's a pending ad which gives us several possibilities
									} elseif ( $post->post_status == 'tr_pending' || $post->post_status == 'tr_completed' ) {

										if ( cp_have_pending_payment( $post->ID ) ) {
											$post_status = 'pending_payment';
											$post_status_name = __( 'Awaiting payment', CLP_EC );
											$fontcolor = '#C00202';
											$postimage = '';
											$postalt = '';
											$postaction = 'pending';
										} else {
											$post_status = 'pending';
											$post_status_name = __( 'Awaiting approval', CLP_EC );
											$fontcolor = '#C00202';
											$postimage = '';
											$postalt = '';
											$postaction = 'pending';
										}

									} elseif ( $post->post_status == 'tr_failed' ) {

										// current date is past the expires date so mark ad ended
										if ( current_time('timestamp') > $expire_time ) {
											$post_status = 'failed_payment';
											$post_status_name = __( 'Payment failed', CLP_EC ) . '<br /><p class="small">(' . $expire_date . ')</p>';
											$fontcolor = '#666666';
											$postimage = '';
											$postalt = '';
											$postaction = 'failed';
										}

									} else {
										$post_status = '&mdash;';
									}
								?>


								<tr class="even">
									<td class="text-right"><?php echo $i; ?>.</td>

									<td><button class="view-details" id="<?php echo $result; ?>" title="<?php _e( 'View order details.', CLP_EC ); ?>"><?php echo $post->ID; ?></button></td>

									<td><?php echo appthemes_display_date( $row->paid_date, 'date' ); ?></td>

									<td class="text-center"><a href="<?php echo get_author_posts_url( $row->user_id ); ?>"><?php echo get_the_author_meta( 'display_name', $row->user_id ); ?></a></td>

									<td class="text-center"><span style="color:<?php echo $fontcolor; ?>;"><?php echo $post_status_name; ?></span></td>

									<td class="text-center">
										<?php

											// delete
											//$delete_url = add_query_arg( array( 'aid' => $post->ID, 'action' => 'delete' ), CLP_TRANS_URL );
											//echo html( 'a', array( 'href' => $delete_url, 'onclick' => 'return confirmBeforeDeleteAd();', 'title' => __( 'Delete Ad', CLP_EC ) ), __( 'Delete', CLP_EC ) ) . ' ';

											// mark paid
											if ( $post_status == 'pending_payment' ) {
											$nonce = wp_nonce_field( 'ajax-activate-nonce-'.$post->ID.'', 'security_'.$post->ID.'' );
											$mark_paid = __( 'Mark Paid', CLP_EC );
											$mark_failed = __( 'Failed', CLP_EC );
											echo "<input id='slug' type='hidden' value='$slug'>";
											echo "<button class='mark-paid' id='$post->ID' title='".__( 'Mark this purchase as paid', CLP_EC )."'>$mark_paid</button>";
											echo "<div class='clr'></div>";
											echo "<button class='mark-failed' id='$post->ID' title='".__( 'Mark this purchase as failed', CLP_EC )."'>$mark_failed</button>";
											}
											else if ( $post_status == 'pending' ) {
											$nonce = wp_nonce_field( 'ajax-activate-nonce-'.$post->ID.'', 'security_'.$post->ID.'' );
											$mark_paid = __( 'Approve', CLP_EC );
											$mark_failed = __( 'Reject', CLP_EC );
											echo "<input id='slug' type='hidden' value='$slug'>";
											echo "<button class='mark-paid' id='$post->ID' title='".__( 'Mark this purchase as paid', CLP_EC )."'>$mark_paid</button>";
											echo "<div class='clr'></div>";
											echo "<button class='mark-failed' id='$post->ID' title='".__( 'Mark this purchase as failed', CLP_EC )."'>$mark_failed</button>";
											}
											// view invoices
											else if ( $post_status == 'live' ) {
											if ( defined( 'NC_INVOICES_TD' ) ) {
											$invoice = ''.get_bloginfo('wpurl').'/wp-content/plugins/appthemes-invoices/invoice.php?id='.$post->ID.'&type=pdf';
											$invoice_img = html( 'img', array( 'src' => CLP_EC_PLUGIN_URI . '/styles/images/invoice.png', 'border' => '0', 'title' => __( 'Download Invoice', CLP_EC ), 'alt' => __( 'Invoice', CLP_EC ) ) );
											echo html( 'a', array( 'href' => $invoice, 'target' => '_blank', 'title' => __( 'Download Invoice', CLP_EC ) ), $invoice_img ) . ' ';
											}
											else {
											echo __( 'N/A', CLP_EC );
											}
											}

										?>

									</td>

								</tr>

							<?php endwhile; ?>

							</tbody>

						</table>

						<?php appthemes_pagination( '', '', $sales ); ?>

						<?php else : ?>

							<div class="padd10"></div>
							<p class="text-center"><?php _e( 'You currently have no sales.', CLP_EC ); ?></p>
							<div class="padd10"></div>

						<?php endif; ?>

						<?php wp_reset_query(); ?>

					</div><!-- /block3 -->

					</div><!-- /shadowblock -->

				</div><!-- /shadowblock_out -->

				</div><!-- /tabcontrol -->

			</div><!-- /content_left -->

			<?php get_sidebar( 'user' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
