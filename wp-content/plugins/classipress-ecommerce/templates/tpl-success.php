<?php
/*
 * Template Name: Payment Success
 *
 * This template must be assigned to a page
 * in order for it to work correctly
 *
*/

$current_user = wp_get_current_user(); // grabs the user info and puts into vars
$display_user_name = cp_get_user_name();
?>

<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<div id="breadcrumb">

				<?php if ( function_exists('cp_breadcrumb') ) cp_breadcrumb(); ?>

			</div>

				<div class="content_left">

					<div class="shadowblock_out">

						<div class="shadowblock">

							<div class="post">

	<h1 class="single dotted"><?php the_title(); ?></h1>

	<p><?php echo "".sprintf(__( 'Thank you for your payment. Your transaction has been completed, and a receipt for your purchase has been emailed to you. You may log into your account at <a style="text-decoration: none;" href="%s" target="_blank">www.paypal.com</a> to view details of this transaction.', CLP_EC), 'https://www.paypal.com' ).""; ?></p>

							</div><!--/post-->

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

			</div><!-- /content_left -->

			<?php get_sidebar( 'user' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
