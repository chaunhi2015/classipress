<?php
/*
Template Name: Shopping Cart
 *
 * This template must be assigned to a page
 * in order for it to work correctly
 *
*/

global $current_user, $wpdb, $cp_options;
$current_user = wp_get_current_user(); // grabs the user info and puts into vars
$slug = basename( get_permalink() );

?>

<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<div id="breadcrumb">

				<?php if ( function_exists('cp_breadcrumb') ) cp_breadcrumb(); ?>

			</div>

	<div class="content_left">

	<div class="message"></div>

		<div class="shadowblock_out">

			<div class="shadowblock">

				<div class="post">

		<h1 class="single dotted"><?php the_title(); ?></h1>

		<?php
		$table_trans = $wpdb->prefix . "clp_transactions";
		$sub_total = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(sub_total) FROM {$table_trans} WHERE order_id < '1' AND user_id = %s AND trans_type = %s AND paid_date = %s", $current_user->ID, 'cart', '' ) );
		$items = $wpdb->get_results("SELECT * FROM {$table_trans} WHERE order_id < '1' AND user_id = '$current_user->ID' AND trans_type = 'cart' AND paid_date = ''");

		if ( $items ) :
		echo "<table class='cart-table'>";
		$post_ids = false;
		$quantity = false;
		$taxes = 0;
		$rowclass = false;
		foreach ( $items as $item ) {
		$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
		$img_url = awsolutions_image_url( $item->post_id );
		$ad_url = get_permalink( $item->post_id );
		$price = cp_display_price( $item->price, 'ad', false );
		$item_name = $item->item_name;
		if ( empty( $item_name ) ) $item_name = $item->post_title;
		if ( $item->status == "failed" ) $status = "<font color='red'>".sprintf( __( 'The order "%s" has status as failed, please try to pay again, remove it or contact the administration.', CLP_EC ), $item_name )."</font>";
		else $status = false;
		$nonce = wp_nonce_field( 'ajax-cart-nonce-'.$item->post_id.'', 'security_'.$item->post_id.'' );

		echo "$nonce
			<input id='id_$item->post_id' type='hidden' value='$item->id'>
			<input id='slug' type='hidden' value='$slug'>
			<input id='order_$item->post_id' type='hidden' value='$item->order_id'>";

		echo "<tr class='item_$item->post_id cart-item $rowclass'>";
		echo "<td width='20%'><a href='$ad_url'>$item_name</a><br /><div class='quantity'>$item->quantity x $price</div></td>
			<td width='8%'><input id='quantity_$item->post_id' class='cart-quantity' type='text' value='$item->quantity'></td>
			<td width='8%'><button class='update-order cart-btn' id='$item->post_id'>".__( 'Update', CLP_EC )."</button></td>
			<td width='8%'><button class='empty-cart cart-btn' id='$item->post_id'>".__( 'Remove', CLP_EC )."</button></td>
			<td width='8%'><div class='loader_$item->post_id'></td>
			<td width='40%'>$img_url</td>";
		echo "</tr>";
		echo "<tr class='$rowclass'><td colspan='6'>$status<div class='dotted'></div></td></tr>";

		$taxes += $item->taxes;
		$post_ids .= ''.$item->post_id.',';

		} ?>

		</table>

			<div class="cart-summary">

				<div class="padd5"></div>

					<?php if ( $taxes > 0 ) { ?>
					<div class="sub-total"><span class="label-one"><?php _e( 'Subtotal:', CLP_EC ); ?><span class="label-two"><?php echo cp_display_price( $sub_total, 'ad', false ); ?></span></span></div>

					<div class="clr"></div>
					<div class="taxes"><span class="label-one"><?php _e( 'Taxes:', CLP_EC ); ?><span class="label-two"><?php echo cp_display_price( $taxes, 'ad', false ); ?></span></span></div>

					<div class="clr"></div>
					<div class="dotted"></div>
					<?php } ?>
					<div class="total"><span class="label-one"><?php _e( 'Total:', CLP_EC ); ?><span class="label-two" style="font-weight: bold;"><?php echo cp_display_price( $sub_total + $taxes, 'ad', false ); ?></span></span></div>

			</div>

	<?php else :

	$table_trans = $wpdb->prefix . "clp_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id > '0' AND user_id = '$current_user->ID' AND paid_date = ''");

	if ( $row ) {

	if ( $row->order_id > 0 && $row->status !== "paid" ) {

		$url = '<a style="text-decoration: none;" href="'.CP_DASHBOARD_URL.'">'.__( 'click here', CLP_EC ).'</a>';

		echo '<p>'.sprintf(__( 'Your cart is empty but you have unpaid purchases, %s to pay for your purchase!', CLP_EC ), $url).'</p>';

	}

	}
	else {

		echo '<p>'.__( 'Your cart is empty.', CLP_EC ).'</p>';

	}

	endif; ?>

				<div class="padd10"></div>

				<button onclick="location.href='<?php echo get_bloginfo('wpurl'); ?>'"><?php _e( 'Continue Shopping', CLP_EC ); ?></button> <?php if ( $items ) { wp_nonce_field( 'ajax-cart-nonce-'.trim($post_ids, ',').'', 'security' ); ?><button onclick="location.href='<?php echo CLP_CART_URL; ?>'"><?php _e( 'Refresh Cart', CLP_EC ); ?></button> <button class="cart-orders" id="<?php echo trim($post_ids, ','); ?>"><?php _e( 'Make Payment', CLP_EC ); ?></button><?php } ?>

						<div class="clr"></div>

							</div><!--/post-->

						</div><!-- /shadowblock -->

					</div><!-- /shadowblock_out -->

			</div><!-- /content_left -->

			<?php get_sidebar( 'user' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
