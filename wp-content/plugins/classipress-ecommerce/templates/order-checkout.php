<?php

	global $current_user, $wpdb, $cp_options, $clp_options;

	$curauth = get_queried_object();
	$order = appthemes_get_order( $curauth->ID );
	$item = awsolutions_get_order_type( $curauth->ID );
?>

<div class="content">

	<div class="content_botbg">

		<div class="content_res">

			<div id="breadcrumb"><?php cp_breadcrumb(); ?></div>

			<div class="content_left">

			<div class="shadowblock_out">

				<div class="shadowblock">

					<div class="post">

						<h1 class="single dotted"><?php _e( 'Order Summary', CLP_EC ); ?></h1>

						<div class="order-summary">

						<?php if ( $item == 'product' ) { ?>

						<table class="cart-table">

						<?php
						$table_trans = $wpdb->prefix . "clp_transactions";
						$orders = $wpdb->get_results("SELECT * FROM {$table_trans} WHERE order_id = '$curauth->ID'");
						$charged_tax = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(taxes) FROM {$table_trans} WHERE order_id = %s", $curauth->ID ) );
						$sub_total = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(sub_total) FROM {$table_trans} WHERE order_id = %s", $curauth->ID ) );

						if ( $orders ) {
						$rowclass = false;
						foreach ( $orders as $row ) {
						$rowclass = ( 'even' == $rowclass ) ? 'alt' : 'even';
						$img_url = awsolutions_image_url( $row->post_id );
						$ad_url = get_permalink( $row->post_id );
						$price = cp_display_price( $row->price, 'ad', false );
						$item_name = $row->item_name;
						if ( empty( $item_name ) ) $item_name = $row->post_title;

						echo "<tr class='cart-item $rowclass'>";
						echo "<td width='50%'><a href='$ad_url'>$item_name</a><br /><div class='quantity'>$row->quantity x $price</div></td>
						<td width='50%'>$img_url</td>";
						echo "</tr>";
						echo "<tr><td colspan='2'><div class='dotted'></div></td></tr>";
						}
						}
						?>

						</table>

						<div class="padd5"></div>

						<span style="width: 200px;"><?php _e( '<strong>Subtotal:</strong>', CLP_EC ); ?><span style="float: right; margin-right: 400px;"><?php echo cp_display_price( $sub_total, 'ad', false ); ?></span></span>
						<?php if ( $charged_tax > 0 ) { ?>
						<div class="clr"></div>
						<span style="width: 200px;"><?php _e( '<strong>Taxes:</strong>', CLP_EC ); ?><span style="float: right; margin-right: 400px;"><?php echo cp_display_price( $charged_tax, 'ad', false ); ?></span></span>
						<?php } ?>
						<div class="dotted"></div>
						<span style="width: 200px;"><strong><?php _e( 'Total:', CLP_EC ); ?><span style="float: right; margin-right: 400px;"><?php echo cp_display_price( $order->get_total(), 'ad', false ); ?></strong></span></span>

						<div class="clr"></div>

						<?php if ( $clp_options['buttons'] != "direct" ) { ?>
						<div class="cart-link"><a href="<?php echo CLP_CART_URL; ?>"><?php _e( 'Shopping Cart', CLP_EC ); ?></a></div>
						<?php } ?>

						<div class="padd10"></div>

							<form action="<?php the_order_return_url(); ?>" method="POST">
								<p><?php _e( 'Please select a method for processing your payment:', CLP_EC ); ?></p>
								<?php awsolutions_gateway_dropdown( $order ); ?>
								<button class="btn_orange" type="submit"><?php _e( 'Process', CLP_EC ); ?></button>
							</form>

						<?php } else { ?>

						<?php the_order_summary(); ?>

							<form action="<?php the_order_return_url(); ?>" method="POST">
								<p><?php _e( 'Please select a method for processing your payment:', APP_TD ); ?></p>
								<?php appthemes_list_gateway_dropdown(); ?>
								<button class="btn_orange" type="submit"><?php _e( 'Submit', APP_TD ); ?></button>
							</form>

						<?php } ?>

						</div>

						<div class="clr"></div>

					</div><!--/post-->

				</div><!-- /shadowblock -->

			</div><!-- /shadowblock_out -->

			</div><!-- /content_left -->

			<?php get_sidebar( 'user' ); ?>

			<div class="clr"></div>

		</div><!-- /content_res -->

	</div><!-- /content_botbg -->

</div><!-- /content -->
