<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

// Do the necessary changes to the PayPal gateway.
function awsolutions_paypal_gateway() {

	$TemplateFileSourceURL1 = CLP_EC_PLUGIN_DIR . 'includes/gateways/paypal/paypal-form.php';
	$TemplateFileSourceURL2 = CLP_EC_PLUGIN_DIR . 'includes/gateways/paypal/paypal.php';

	$TemplateFileTargetURL1 = get_stylesheet_directory() . '/includes/payments/gateways/paypal/paypal-form.php';
	$TemplateFileTargetURL2 = get_stylesheet_directory() . '/includes/payments/gateways/paypal/paypal.php';

	if ( !file_exists( $TemplateFileSourceURL1 ) || !file_exists( $TemplateFileSourceURL2 ) ) {
        return FALSE;
	}

	$GetTemplate1 = file_get_contents( $TemplateFileSourceURL1 );
	$GetTemplate2 = file_get_contents( $TemplateFileSourceURL2 );

	if ( !$GetTemplate1 || !$GetTemplate2 ) {
	return FALSE;
	}

	$WriteTemplate1 = file_put_contents( $TemplateFileTargetURL1, $GetTemplate1 );

	$WriteTemplate2= file_put_contents( $TemplateFileTargetURL2, $GetTemplate2 );
	if ( !$WriteTemplate1 || !$WriteTemplate2 ) {
	return FALSE;
	}

	return TRUE;

}

// Do the necessary changes to the Invoice plugin.
function awsolutions_invoice_plugin() {

	if ( defined( 'NC_INVOICES_BASE_DIR' ) ) {

	$TemplateFileSourceURL1 = CLP_EC_PLUGIN_DIR . 'includes/invoice/InvoiceDOMPDF.php';
	$TemplateFileSourceURL2 = CLP_EC_PLUGIN_DIR . 'includes/invoice/InvoiceHTML.php';

	$TemplateFileTargetURL1 = NC_INVOICES_BASE_DIR . '/inc/InvoiceDOMPDF.php';
	$TemplateFileTargetURL2 = NC_INVOICES_BASE_DIR . '/inc/InvoiceHTML.php';

	if ( !file_exists( $TemplateFileSourceURL1 ) || !file_exists( $TemplateFileSourceURL2 ) ) {
        return FALSE;
	}

	$GetTemplate1 = file_get_contents( $TemplateFileSourceURL1 );
	$GetTemplate2 = file_get_contents( $TemplateFileSourceURL2 );

	if ( !$GetTemplate1 || !$GetTemplate2 ) {
	return FALSE;
	}

	$WriteTemplate1 = file_put_contents( $TemplateFileTargetURL1, $GetTemplate1 );

	$WriteTemplate2= file_put_contents( $TemplateFileTargetURL2, $GetTemplate2 );
	if ( !$WriteTemplate1 || !$WriteTemplate2 ) {
	return FALSE;
	}

	return TRUE;

	}

}

// Do the necessary changes to the Skrill gateway.
function awsolutions_skrill_gateway() {

	if ( defined( 'APP_SK_TD' ) ) {

	$TemplateFileSourceURL1 = CLP_EC_PLUGIN_DIR . 'includes/gateways/skrill/skrill-gateway.php';

	$TemplateFileTargetURL1 = ABSPATH . 'wp-content/plugins/appthemes-skrill/skrill-gateway.php';

	if ( !file_exists( $TemplateFileSourceURL1 ) ) {
        return FALSE;
	}

	$GetTemplate1 = file_get_contents( $TemplateFileSourceURL1 );

	if ( !$GetTemplate1 ) {
	return FALSE;
	}

	$WriteTemplate1 = file_put_contents( $TemplateFileTargetURL1, $GetTemplate1 );

	if ( !$WriteTemplate1 ) {
	return FALSE;
	}

	return TRUE;

	}

}

// Do the necessary changes to the Bank Transfer gateway.
function awsolutions_bank_transfer_gateway() {

	$TemplateFileSourceURL1 = CLP_EC_PLUGIN_DIR . 'includes/gateways/bank-transfer/template/order-bank-transfer.php';
	$TemplateFileSourceURL2 = CLP_EC_PLUGIN_DIR . 'includes/gateways/bank-transfer/bt-emails.php';

	$TemplateFileTargetURL1 = get_stylesheet_directory() . '/includes/payments/gateways/bank-transfer/template/order-bank-transfer.php';
	$TemplateFileTargetURL2 = get_stylesheet_directory() . '/includes/payments/gateways/bank-transfer/bt-emails.php';

	if ( !file_exists( $TemplateFileSourceURL1 ) || !file_exists( $TemplateFileSourceURL2 ) ) {
        return FALSE;
	}

	$GetTemplate1 = file_get_contents( $TemplateFileSourceURL1 );
	$GetTemplate2 = file_get_contents( $TemplateFileSourceURL2 );

	if ( !$GetTemplate1 || !$GetTemplate2 ) {
	return FALSE;
	}

	$WriteTemplate1 = file_put_contents( $TemplateFileTargetURL1, $GetTemplate1 );

	$WriteTemplate2= file_put_contents( $TemplateFileTargetURL2, $GetTemplate2 );
	if ( !$WriteTemplate1 || !$WriteTemplate2 ) {
	return FALSE;
	}

	return TRUE;

}

// Do the necessary changes to the order-select.php file.
function awsolutions_order_select_override() {

	$TemplateFileSourceURL1 = CLP_EC_PLUGIN_DIR . 'includes/order/order-select.php';

	$TemplateFileTargetURL1 = get_stylesheet_directory() . '/order-select.php';

	if ( !file_exists( $TemplateFileSourceURL1 ) ) {
        return FALSE;
	}

	$GetTemplate1 = file_get_contents( $TemplateFileSourceURL1 );

	if ( !$GetTemplate1 ) {
	return FALSE;
	}

	$WriteTemplate1 = file_put_contents( $TemplateFileTargetURL1, $GetTemplate1 );

	if ( !$WriteTemplate1 ) {
	return FALSE;
	}

	return TRUE;

}