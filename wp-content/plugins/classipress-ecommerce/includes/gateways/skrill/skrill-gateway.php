<?php

class APP_Skrill extends APP_Boomerang {

	protected $options;


	public function __construct() {
		parent::__construct( 'skrill', array(
			'admin' => __( 'Skrill', APP_SK_TD ),
			'dropdown' => __( 'Skrill', APP_SK_TD ),
		) );
	}


	public function create_form( $order, $options ) {

		$recipient = get_user_by( 'id', $order->get_author() );

		if ( function_exists('awsolutions_check_receiver') ) $skrill_email = awsolutions_check_receiver( $order, 'skrill' );
		if ( empty( $skrill_email ) || $skrill_email == false ) $skrill_email = $options['email'];

		$fields = array(
			APP_Skrill_Fields::PAY_TO_EMAIL => $skrill_email,
			APP_Skrill_Fields::CURRENCY => APP_Gateway_Registry::get_options()->currency_code,
			APP_Skrill_Fields::LANGUAGE => $options['language'],

			APP_Skrill_Fields::TRANSACTION_ID => $order->get_id(),
			APP_Skrill_Fields::AMOUNT => $order->get_total(),

			APP_Skrill_Fields::DETAIL_DESCRIPION => $order->get_description(),
			APP_Skrill_Fields::DETAIL_TEXT => $order->get_id(),

			APP_Skrill_Fields::FIRST_NAME => empty( $recipient->first_name ) ? $recipient->user_login : $recipient->first_name,
			APP_Skrill_Fields::LAST_NAME => empty( $recipient->last_name ) ? $recipient->user_login : $recipient->last_name,
			APP_Skrill_Fields::EMAIL => $recipient->user_email,

			APP_Skrill_Fields::RETURN_URL => $this->get_return_url( $order ),
			APP_Skrill_Fields::RETURN_URL_TEXT => sprintf( __( 'Click here to back to %s', APP_SK_TD), get_bloginfo( 'name' ) ),
			APP_Skrill_Fields::RETURN_URL_TARGET => '1',

			APP_Skrill_Fields::CANCEL_URL => site_url( '/' ),
			APP_Skrill_Fields::CANCEL_URL_TARGET => '1',

			APP_Skrill_Fields::STATUS_URL => $this->get_ipn_url( $order ),
			APP_Skrill_Fields::STATUS_URL2 => get_bloginfo( 'admin_email' ),
		);

		$form = array(
			'accept-charset' => 'utf-8',
			'action' => APP_Skrill_Fields::POST_URL,
			'id' => 'create-listing',
			'method' => 'POST',
			'name' => 'skrill_payform',
		);
		$this->redirect( $form, $fields, __( 'You are now being redirected to Skrill.', APP_SK_TD ) );

	}


	public function form() {

		$fields = array (
			array(
				'title' => __( 'Account ID', APP_SK_TD ),
				'desc' => '',
				'tip' => __( 'Enter your Skrill account ID.', APP_SK_TD ),
				'name' => 'merchant_id',
				'type' => 'text',
			),
			array(
				'title' => __( 'Email Address', APP_SK_TD ),
				'desc' => '',
				'tip' => __( 'Enter your Skrill email address.', APP_SK_TD ),
				'name' => 'email',
				'type' => 'text',
			),
			array(
				'title' => __( 'Language', APP_SK_TD ),
				'desc' => '',
				'tip' => __( 'Choose default language of payment form. Customer can change it to another on Skrill site.', APP_SK_TD ),
				'name' => 'language',
				'type' => 'select',
				'values' => array(
					'EN' => __( 'English', APP_SK_TD ),
					'DE' => __( 'German', APP_SK_TD ),
					'PL' => __( 'Polish', APP_SK_TD ),
					'ES' => __( 'Spanish', APP_SK_TD ),
					'FR' => __( 'French', APP_SK_TD ),
					'IT' => __( 'Italian', APP_SK_TD ),
					'GR' => __( 'Greek', APP_SK_TD ),
					'RO' => __( 'Romanian', APP_SK_TD ),
					'RU' => __( 'Russian', APP_SK_TD ),
					'TR' => __( 'Turkish', APP_SK_TD ),
					'CN' => __( 'Chinese', APP_SK_TD ),
					'CZ' => __( 'Czech', APP_SK_TD ),
					'NL' => __( 'Dutch', APP_SK_TD ),
					'DA' => __( 'Danish', APP_SK_TD ),
					'SV' => __( 'Swedish', APP_SK_TD ),
					'FI' => __( 'Finnish', APP_SK_TD ),
				),
			),
		);

		return array(
			array(
				'title' => __( 'Skrill', APP_SK_TD ),
				'fields' => $fields,
			)
		);

	}


	public function process( $order, $options ) {

		if ( ! $this->is_returning() ) {
			$this->create_form( $order, $options );
		} else {
			//get_template_part( 'order-summary' );
		}

	}


	public function handle_ipn() {

		if ( self::is_valid_ipn_request() ) {
			self::process_ipn();
		}

	}


	protected function is_valid_ipn_request() {

		if ( ! isset( $_GET['action'] ) || $_GET['action'] != 'skrill_ipn' ) {
			return false;
		}

		if ( $_SERVER['HTTP_USER_AGENT'] != 'Moneybookers Merchant Payment Agent' ) {
			return false;
		}

		$expected = array( 'merchant_id', 'transaction_id', 'mb_amount', 'mb_currency', 'status', 'md5sig' );
		foreach ( $expected as $key ) {
			if ( ! isset( $_POST[ $key ] ) ) {
				return false;
			}
		}

		$options = APP_Gateway_Registry::get_gateway_options( 'skrill' );
		$order = appthemes_get_order( $_POST['transaction_id'] );

		if ( function_exists('awsolutions_check_receiver') ) $merchant_id = awsolutions_check_receiver( $order, 'skrill_id' );
		if ( empty( $merchant_id ) || $merchant_id == false ) $merchant_id = $options['merchant_id'];

		$skrill_checksum = strtoupper( md5( $_POST['merchant_id'] . $_POST['transaction_id'] . $_POST['mb_amount'] . $_POST['mb_currency'] . $_POST['status'] ) );

		if ( $_POST['merchant_id'] != $merchant_id || $_POST['md5sig'] != $skrill_checksum ) {
			return false;
		}

		return true;
	}


	protected function process_ipn() {

		$order = appthemes_get_order( $_POST['transaction_id'] );
		if ( ! $order ) {
			return false;
		}

		echo 'OK';

		switch( $_POST['status'] ) {
			case '0':
				if ( method_exists( $order, 'pending' ) ) {
					$order->pending();
				}
				break;
			case '2':
				$order->complete();
				break;
			case '-1':
			case '-2':
				if ( method_exists( $order, 'failed' ) ) {
					$order->failed();
				}
				break;
		}

		exit();
	}


	protected function get_ipn_url( $order ) {

		return add_query_arg( array( 'action' => 'skrill_ipn' ), admin_url( 'admin-ajax.php' ) );
	}

}