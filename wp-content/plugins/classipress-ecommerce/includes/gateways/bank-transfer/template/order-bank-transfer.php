<?php

	$message = false;
	$bt_email = get_option('admin_email');
	if ( function_exists('awsolutions_check_receiver') ) {
	$message = awsolutions_check_receiver( $order, 'bt_message' );
	$bt_email = awsolutions_check_receiver( $order, 'bank-transfer' );
	if ( empty( $bt_email ) ) $bt_email = get_option('admin_email');
	}

?>

<style type="text/css">
		#bank-transfer fieldset{
			margin-bottom: 20px;
		}
		#bank-transfer .content{
			width: auto;
			padding: 20px;
		}
		#bank-transfer pre{
			font-family: Arial, Helvetica, sans-serif;
			font-size: 12px;
			padding-top: 10px;
			white-space: pre-wrap;
		}
	</style>
	<div class="section-head"><h2><?php _e( 'Bank Transfer', APP_TD ); ?></h2></div>
	<form id="bank-transfer">
		<fieldset>
			<div class="featured-head"><h3><?php _e( 'Instructions:', APP_TD ); ?></h3></div>

			<div class="content">
				<?php if ( $message ) { ?>
				<pre><?php echo ( isset( $message ) ) ? $message : '' ; ?></pre>
				<?php } else { ?>
				<pre><?php echo ( isset( $options['message'] ) ) ? $options['message'] : '' ; ?></pre>
				<?php } ?>
			</div>
		</fieldset>
		<fieldset>
			<div class="featured-head"><h3><?php _e( 'Order Information:', APP_TD ); ?></h3></div>

			<div class="content">
<pre><?php _e( 'Order ID:', APP_TD ); ?> <?php echo $order->get_id(); ?> 
<?php _e( 'Order Total:', APP_TD ); ?> <?php echo appthemes_get_price( $order->get_total(), $order->get_currency() ); ?></pre>

<div class="pad10"></div>

<p>
<?php _e( 'For questions or problems, please contact us directly at', APP_TD ) ?> <a style="text-decoration: none;" href="mailto:<?php echo $bt_email; ?>"><?php echo $bt_email; ?></a>
</p>

<p>
<?php printf( __( 'To cancel this request and use a regular gateway instead, <a style="text-decoration: none;" href="%s">click here</a>.', APP_TD ), get_the_order_cancel_url() ); ?>
</p>

			</div>
		</fieldset>
	</form>
	<div class="clear"></div>