<?php
/**
 * Install script to create database tables and then insert default data.
 * Only run if theme is being activated.
 *
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


global $clp_ec_db_version, $clp_ec_db_date, $cp_options, $wpdb;

$clp_ec_db_version = '0.4';
$clp_ec_db_date = "October 26, 2014";

function awsolutions_plugin_activate() {
	global $wpdb;

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	// check if it is a multisite network
	if (is_multisite()) {
	// check if the plugin has been activated on the network or on a single site
	if (is_plugin_active_for_network(__FILE__)) {
	// get ids of all sites
	$blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
	foreach ($blogids as $blog_id) {
	switch_to_blog($blog_id);
	// activate for each site
	awsolutions_plugin_install($blog_id);
	restore_current_blog();
	}
	}
	else
	{
	// activated on a single site, in a multi-site
	awsolutions_plugin_install($wpdb->blogid);
	}
	}
	else
	{
	// activated on a single site
	awsolutions_plugin_install($wpdb->blogid);
	}
}

function awsolutions_plugin_install( $blog_id ) {

	global $clp_ec_db_version, $clp_ec_db_date, $cp_options, $wpdb;

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

if( get_option( "clp_ec_db_version" ) < '0.1' ) {

	// Create the transactions database table
	$table_trans = $wpdb->prefix . "clp_transactions";
	if($wpdb->get_var("show tables like '$table_trans'") != $table_trans) {

	$sql = "CREATE TABLE $table_trans (
	`id` bigint( 20 ) NOT NULL auto_increment,
	`order_id` bigint( 20 ) NOT NULL,
	`post_id` bigint( 20 ) NOT NULL,
	`post_title` text NOT NULL,
	`item_name` text NOT NULL,
	`post_author` bigint( 20 ) NOT NULL,
	`price` decimal( 12,2 ) NOT NULL,
	`taxes` decimal( 8,2 ) NOT NULL,
	`user_id` bigint( 20 ) NOT NULL,
	`quantity` int( 4 ) NOT NULL default '0',
	`sub_total` decimal( 12,2 ) NOT NULL,
	`trans_type` varchar( 12 ) NOT NULL,
	`paid_date` bigint( 20 ) NOT NULL,
	`status` varchar( 12 ) NOT NULL,
	PRIMARY KEY  (`id`)
	);";

	dbDelta($sql);

	}

	// Insert default data into tables
	$field_sql = "SELECT field_id FROM $wpdb->cp_ad_fields WHERE field_name = %s LIMIT 1";

	// Enable payment field
	$wpdb->get_results( $wpdb->prepare($field_sql, 'cp_enable_payment') );
	if ( $wpdb->num_rows == 0 ) {

	$wpdb->insert( $wpdb->cp_ad_fields, array(
		'field_name' => 'cp_enable_payment',
		'field_label' => 'Enable Payment',
		'field_desc' => 'This is the enable payment in ads field and it is mandatory if ClassiPress eCommerce is enabled. It is a core eCommerce field and should not be deleted.',
		'field_type' => 'checkbox',
		'field_values' => 'Tick to enable',
		'field_tooltip' => 'Give users the ability to pay you online. This option demands that you have entered your payment preference in dashboard settings.',
		'field_search' => '0',
		'field_perm' => '0',
		'field_core' => '1',
		'field_req' => '0',
		'field_owner' => 'AWSolutions',
		'field_created' => current_time('mysql'),
		'field_modified' => current_time('mysql'),
		'field_min_length' => '0'
	) );

	}

	// Item name field
	$wpdb->get_results( $wpdb->prepare($field_sql, 'cp_item_name') );
	if ( $wpdb->num_rows == 0 ) {

	$wpdb->insert( $wpdb->cp_ad_fields, array(
		'field_name' => 'cp_item_name',
		'field_label' => 'Item Name',
		'field_desc' => 'This is the item name field and it is mandatory if ClassiPress eCommerce is enabled. It is a core eCommerce field and should not be deleted.',
		'field_type' => 'text box',
		'field_values' => '',
		'field_tooltip' => 'Enter the product name of the product you wish to sell.',
		'field_search' => '0',
		'field_perm' => '0',
		'field_core' => '1',
		'field_req' => '1',
		'field_owner' => 'AWSolutions',
		'field_created' => current_time('mysql'),
		'field_modified' => current_time('mysql'),
		'field_min_length' => '0'
	) );

	}

	// Item quantity field
	$wpdb->get_results( $wpdb->prepare($field_sql, 'cp_quantity') );
	if ( $wpdb->num_rows == 0 ) {

	$wpdb->insert( $wpdb->cp_ad_fields, array(
		'field_name' => 'cp_quantity',
		'field_label' => 'Quantity',
		'field_desc' => 'This is the quantity field and it is mandatory if ClassiPress eCommerce is enabled. It is a core eCommerce field and should not be deleted.',
		'field_type' => 'text box',
		'field_values' => '',
		'field_tooltip' => 'Enter a 0 if the supply of this product is virtually unlimited.',
		'field_search' => '0',
		'field_perm' => '0',
		'field_core' => '1',
		'field_req' => '1',
		'field_owner' => 'AWSolutions',
		'field_created' => current_time('mysql'),
		'field_modified' => current_time('mysql'),
		'field_min_length' => '0'
	) );

	}

	awsolutions_create_pages();

	update_option( "clp_ec_db_version", $clp_ec_db_version );

	}

	update_option( "clp_ec_db_version", $clp_ec_db_version );

}


// Create the necessary pages and assign the templates to them
function awsolutions_create_pages() {
    global $wpdb, $app_abbr;

    $out = array();

    // first check and make sure this page doesn't already exist
    $sql = "SELECT ID FROM " . $wpdb->posts . " WHERE post_name = 'shopping-basket' LIMIT 1";

    $wpdb->get_results($sql);

    if($wpdb->num_rows == 0) {

        // first create the shopping-basket page
        $my_page = array(
        'post_status' => 'publish',
        'post_type' => 'page',
        'post_author' => 1,
        'post_name' => 'shopping-basket',
        'post_title' => 'Shopping Cart'
        );

        // Insert the page into the database
        $page_id = wp_insert_post($my_page);

        // Assign the page template to the new page
        update_post_meta($page_id, '_wp_page_template', 'tpl-shopping-basket.php');

        $out[] = $page_id;

    }

    // first check and make sure this page doesn't already exist
    $sql = "SELECT ID FROM " . $wpdb->posts . " WHERE post_name = 'success' LIMIT 1";

    $wpdb->get_results($sql);

    if($wpdb->num_rows == 0) {

        // first create the payment success page
        $my_page = array(
        'post_status' => 'publish',
        'post_type' => 'page',
        'post_author' => 1,
        'post_name' => 'success',
        'post_title' => 'Thank You'
        );

        // Insert the page into the database
        $page_id = wp_insert_post($my_page);

        // Assign the page template to the new page
        update_post_meta($page_id, '_wp_page_template', 'tpl-success.php');

        $out[] = $page_id;

    }


    // check to see if array of page ids is empty
    // if not, add them to the pages to be excluded
    // from the nav meta option.
    if (!empty($out)) {
    	global $app_abbr;
        
        // take the array and put elements into a comma separated string
        $exclude_pages = implode(',', $out);

        // now insert the excluded pages meta option and the values if the option doesn't already exist
        if(get_option($app_abbr.'_excluded_pages') == false)
            update_option($app_abbr.'_excluded_pages', appthemes_clean($exclude_pages));

    }

}
?>