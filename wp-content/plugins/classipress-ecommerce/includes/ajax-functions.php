<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


// Emptying the shopping cart, item by item
function awsolutions_empty_shopping_cart() {
	global $post, $current_user, $wpdb;

	if ( isset( $_POST["post_id"] ) ) {

	$post_id = $_POST["post_id"];
	$order_id = $_POST["order_id"];
	$available = get_post_meta( $post_id, 'cp_quantity', true );
	$unlimited = get_post_meta( $post_id, 'cp_quantity_unlimited', true );
	$table_trans = $wpdb->prefix . "clp_transactions";

	check_ajax_referer( 'ajax-cart-nonce-'.$post_id.'', 'security' );

	if ( $order_id > 0 ) {

		$order = appthemes_get_order( $order_id );
		$numbers = $wpdb->get_results("SELECT * FROM {$table_trans} WHERE order_id = '$order_id'");
		$count = $wpdb->num_rows;

		$res = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$order_id' AND post_id = '$post_id' AND paid_date = ''");

		if ( $res->status == 'failed' ) {
		$adjust = $available + $res->quantity;
		if ( $unlimited == "no" )
		update_post_meta( $post_id, 'cp_quantity', $adjust );
		$wpdb->query($wpdb->prepare("DELETE FROM {$table_trans} WHERE id = '%d'", $res->id));
		}
		else if ( $count > 1 ) {
		$order->remove_item( 'product', '', $post_id );
		$adjust = $available + $res->quantity;
		if ( $unlimited == "no" )
		update_post_meta( $post_id, 'cp_quantity', $adjust );
		$wpdb->query($wpdb->prepare("DELETE FROM {$table_trans} WHERE id = '%d'", $res->id));

		$orders = $wpdb->get_results("SELECT * FROM {$table_trans} WHERE order_id = '$order_id'");
		foreach ( $orders as $row ) {
		$order->remove_item( 'product', '', $row->post_id );
		$order->add_item( 'product', $row->sub_total, $row->post_id );
		}
		appthemes_payments_add_tax( $order );
		}
		else if ( $count == 1 ) {
		$order->failed();
		$wpdb->query($wpdb->prepare("DELETE FROM {$table_trans} WHERE id = '%d'", $res->id));
		$adjust = $available + $res->quantity;
		if ( $unlimited == "no" )
		update_post_meta( $post_id, 'cp_quantity', $adjust );
		}
		else {
			return false;
		}

		$msg = '<div class="notice success"><div>'.__( 'Item was removed, please wait until the cart is refreshed...', CLP_EC ).'</div></div>';
		$response = array( 'success' => true, 'notices' => $msg );

	}
	else {

		$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '' AND post_id = '$post_id' AND user_id = '$current_user->ID'");

		if ( $row ) {
		$adjust = $available + $row->quantity;
		if ( $unlimited == "no" )
		update_post_meta( $post_id, 'cp_quantity', $adjust );

		$wpdb->query($wpdb->prepare("DELETE FROM {$table_trans} WHERE id = %d", $row->id));
		}

		$msg = '<div class="notice success"><div>'.__( 'Item was removed, please wait until the cart is refreshed...', CLP_EC ).'</div></div>';
		$response = array( 'success' => true, 'notices' => $msg );

		}

		exit( json_encode( $response ) );

	}
}
add_action( 'wp_ajax_empty_shopping_cart', 'awsolutions_empty_shopping_cart' );


// Mark purchase as paid
function awsolutions_mark_as_paid() {
	global $post, $wpdb;

	if ( isset( $_POST["post_id"] ) ) {
	$order_id = $_POST["order_id"];

	$post = get_post( $_POST["post_id"] );
	check_ajax_referer( 'ajax-activate-nonce-'.$post->ID.'', 'security' );

	$order = appthemes_get_order( $order_id );
	$order->activate();

	$tm = time();
	$table_trans = $wpdb->prefix . "clp_transactions";
	$where_array = array( 'order_id' => $order_id );
	$wpdb->update( $table_trans, array( 'paid_date' => $tm, 'status' => 'paid' ), $where_array );

	$msg = '<div class="notice success"><div>'.__( 'The purchase was marked as paid, please wait until the transactions is refreshed...', CLP_EC ).'</div></div>';
	$response = array( 'success' => true, 'notices' => $msg );

	exit( json_encode( $response ) );

	}
}
add_action( 'wp_ajax_mark_paid', 'awsolutions_mark_as_paid' );


// Mark purchase as failed
function awsolutions_mark_as_failed() {
	global $post, $wpdb;

	if ( isset( $_POST["post_id"] ) ) {
	$order_id = $_POST["order_id"];

	$post = get_post( $_POST["post_id"] );
	check_ajax_referer( 'ajax-activate-nonce-'.$post->ID.'', 'security' );

	$order = appthemes_get_order( $order_id );
	$order->failed();

	$table_trans = $wpdb->prefix . "clp_transactions";
	$where_array = array( 'order_id' => $order_id );
	$wpdb->update( $table_trans, array( 'paid_date' => '', 'status' => 'failed' ), $where_array );

	$msg = '<div class="notice success"><div>'.__( 'The purchase was marked as failed, please wait until the transactions is refreshed...', CLP_EC ).'</div></div>';
	$response = array( 'success' => true, 'notices' => $msg );

	exit( json_encode( $response ) );

	}
}
add_action( 'wp_ajax_mark_failed', 'awsolutions_mark_as_failed' );


// Update quantity with seller and shopping cart
function awsolutions_update_quantity() {
	global $post, $wpdb;

	if ( isset( $_POST["row_id"] ) ) {
	$id = $_POST["row_id"];
	$quantity = $_POST["quantity"];

	$table_trans = $wpdb->prefix . "clp_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE id = '$id'");

	$post = get_post( $row->post_id );
	check_ajax_referer( 'ajax-cart-nonce-'.$post->ID.'', 'security' );
	$available = get_post_meta( $post->ID, 'cp_quantity', true );
	$unlimited = get_post_meta( $post->ID, 'cp_quantity_unlimited', true );
	$price = get_post_meta( $post->ID, 'cp_price', true );
	$sub_total = $price * $quantity;

	$adjust = $available;
	if ( $quantity > $row->quantity ) $adjust = $available - ( $quantity - $row->quantity );
	else if ( $quantity < $row->quantity ) $adjust = $available + ( $row->quantity - $quantity );

	if ( $unlimited == "no" && $adjust > -1 ) {
	update_post_meta( $post->ID, 'cp_quantity', $adjust );
	}
	else if ( $unlimited == "no" && $adjust < 0 ) {
	$msg = '<div class="notice error"><div>'.__( 'Unfortunately, the seller does not have the desired number available!', CLP_EC ).'</div></div>';
	$response = array( 'success' => false, 'notices' => $msg );
	exit( json_encode( $response ) );
	}

	$charged_tax = awsolutions_get_charged_tax( '', $sub_total );
	$where_array = array( 'id' => $id );
	$wpdb->update( $table_trans, array( 'taxes' => $charged_tax, 'quantity' => $quantity, 'sub_total' => $sub_total ), $where_array );

	$msg = '<div class="notice success"><div>'.__( 'The quantity was updated, please wait until the cart is refreshed...', CLP_EC ).'</div></div>';
	$response = array( 'success' => true, 'notices' => $msg );

	exit( json_encode( $response ) );

	}
}
add_action( 'wp_ajax_update_order', 'awsolutions_update_quantity' );


// This function will delete all orders, use with care.
function awsolutions_delete_test_orders() {
	global $wpdb;

	check_ajax_referer( 'ajax-admin-nonce', 'security' );

	$table_p2p = $wpdb->prefix . "p2p";
	$table_p2pmeta = $wpdb->prefix . "p2pmeta";
	$orders = $wpdb->get_results("SELECT * FROM {$table_p2p}");

	if ( $orders ) {
	foreach ( $orders as $res ) {

		wp_delete_post( $res->p2p_from, true );

	}
		$wpdb->query("TRUNCATE TABLE {$table_p2p}");
		$wpdb->query("TRUNCATE TABLE {$table_p2pmeta}");
	}

	$table_posts = $wpdb->prefix . "posts";
	$pids = $wpdb->get_results("SELECT * FROM {$table_posts} WHERE post_type = 'transaction'");

	if ( $pids ) {
	foreach ( $pids as $pid ) {

		wp_delete_post( $pid->ID, true );

	}
	}

	$table_trans = $wpdb->prefix . "clp_transactions";
	$wpdb->query("TRUNCATE TABLE {$table_trans}");

	$table_umeta = $wpdb->prefix . "usermeta";
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_umeta} WHERE meta_key = %s", 'clp_my_orders'));
	$wpdb->query($wpdb->prepare("DELETE FROM {$table_umeta} WHERE meta_key = %s", 'clp_my_sales'));

	$msg = '<div class="updated"><p>'.__( '<strong>The query was run and all orders should now have been deleted.</strong>', CLP_EC ).'</p></div>';
	$response = array( 'success' => true, 'notices' => $msg );

	exit( json_encode( $response ) );
}
add_action( 'wp_ajax_delete_test_orders', 'awsolutions_delete_test_orders' );