<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	$general = aws_get_settings( 'clp_general' );

$GLOBALS['clp_options'] = array(
	'buttons' => aws_get_setting( 'clp_general', 'main', 'buttons' ),
	'buttons_placement' => aws_get_setting( 'clp_general', 'main', 'buttons_placement' ),
	'options_style' => aws_get_setting( 'clp_general', 'main', 'options_style' ),
	'cart_options' => aws_get_setting( 'clp_general', 'main', 'cart_options' ),
	'cart_link_header' => aws_get_setting( 'clp_general', 'main', 'cart_link_header' ),
	'cart_link_primary' => aws_get_setting( 'clp_general', 'main', 'cart_link_primary' ),
	'link_options' => aws_get_setting( 'clp_general', 'main', 'link_options' ),
	'paypal' => aws_get_setting( 'clp_general', 'main', 'gateways_paypal' ),
	'skrill' => aws_get_setting( 'clp_general', 'main', 'gateways_skrill' ),
	'simplepay4u' => aws_get_setting( 'clp_general', 'main', 'gateways_simplepay4u' ),
	'bank_transfer' => aws_get_setting( 'clp_general', 'main', 'gateways_bank_transfer' ),
	'cp_version' => get_option('cp_version'),
	'child_theme' => strtolower(get_option('stylesheet'))
	);