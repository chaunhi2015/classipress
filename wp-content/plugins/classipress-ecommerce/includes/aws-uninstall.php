<?php

/*
Copyright 2012 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

    	$del_options = aws_get_setting( 'clp_general', 'main', 'delete_data' );

function uninstall_classipress_ecommerce() {

	global $wpdb;

	$table_trans = $wpdb->prefix . "clp_transactions";
	$wpdb->query("DROP TABLE IF EXISTS $table_trans");

	delete_option( 'clp_general_settings' );
	delete_option( 'clp_ec_db_version' );

	$table_fields = $wpdb->prefix . "cp_ad_fields";
	$row = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_enable_payment'");
	if( $row ) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_fields} WHERE field_name = '%s'", 'cp_enable_payment'));
	}
	$rows = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_item_name'");
	if( $rows ) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_fields} WHERE field_name = '%s'", 'cp_item_name'));
	}
	$rowss = $wpdb->get_row("SELECT * FROM {$table_fields} WHERE field_name = 'cp_quantity'");
	if( $rowss ) {
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_fields} WHERE field_name = '%s'", 'cp_quantity'));
	}

	$shopping_id = awsolutions_get_meta_postid( '_wp_page_template', 'tpl-shopping-cart.php' );
	if ( $shopping_id ) wp_delete_post( $shopping_id, true );

	$basket_id = awsolutions_get_meta_postid( '_wp_page_template', 'tpl-shopping-basket.php' );
	if ( $basket_id ) wp_delete_post( $basket_id, true );

	$success_id = awsolutions_get_meta_postid( '_wp_page_template', 'tpl-success.php' );
	if ( $success_id ) wp_delete_post( $success_id, true );

	$all_user_ids = get_users( 'fields=ID' );
	foreach ( $all_user_ids as $user_id ) {
	delete_user_meta( $user_id, 'clp_my_orders' );
	delete_user_meta( $user_id, 'clp_my_sales' );
	delete_user_meta( $user_id, 'paypal_email' );
	delete_user_meta( $user_id, 'skrill_account_id' );
	delete_user_meta( $user_id, 'skrill_email' );
	delete_user_meta( $user_id, 'bt_email' );
	delete_user_meta( $user_id, 'bt_message' );
	delete_user_meta( $user_id, 'simplepay4u_nocards' );
	delete_user_meta( $user_id, 'simplepay4u_email' );
	}

}

if ( $del_options == "yes" ) {
	uninstall_classipress_ecommerce();
}
?>