<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	global $clp_options, $cp_options;


// Generates the menu tabs for the settings pages.
function awsolutions_admin_tabs() {

	$tabs = array(
		'clp_general' => __( 'General', CLP_EC )
		);

	$tabs = apply_filters( 'awsolutions_admin_tabs', $tabs );

	return $tabs;

}


// Generates the links for the menu tabs.
function awsolutions_admin_pages( $tab ) {

	$page = new AWS_Solutions_Framework( CLP_EC_PLUGIN_DIR . '/admin/'.$tab.'.php', $tab );
	$file = CLP_EC_PLUGIN_DIR . '/admin/'.$tab.'.php';

	if( file_exists( $file ) ) return $page;
	else return apply_filters( 'awsolutions_admin_pages', $tab );

}


// Return jquery/ajax cart values.
function awsolutions_option_vars() {
	global $post, $current_user, $clp_options, $cp_options, $wpdb;

	if ( ! $post )
		return false;

	if ( is_user_logged_in() ) $logged_in = "true";
	else $logged_in = false;

	$price = get_post_meta( $post->ID, 'cp_price', true );
	$tax = awsolutions_get_charged_tax( '', $price );
	if ( $clp_options['cart_options'] == "sidebar" ) $sidebar = "true";
	else $sidebar = false;
	if ( $clp_options['cart_link_header'] == "header" ) $header_link = "true";
	else $header_link = false;
	if ( $clp_options['cart_link_primary'] == "primary" ) $primary_link = "true";
	else $primary_link = false;
	if ( $clp_options['link_options'] == "seller" ) $link = "<button onclick=\"location.href='".get_author_posts_url( $post->post_author )."'\">Continue Shopping</button> &nbsp; &nbsp; ";
	else if ( $clp_options['link_options'] == "home" ) $link = "<button onclick=\"location.href='".get_bloginfo('wpurl')."'\">Continue Shopping</button> &nbsp; &nbsp; ";
	else $link = false;

	$table_trans = $wpdb->prefix . "clp_transactions";
	$items = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(quantity) FROM {$table_trans} WHERE order_id < '1' AND user_id = %s AND trans_type = %s AND paid_date = %s", $current_user->ID, 'cart', '' ) );
	$sub_total = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(sub_total) FROM {$table_trans} WHERE order_id < '1' AND user_id = %s AND trans_type = %s AND paid_date = %s", $current_user->ID, 'cart', '' ) );
	$taxes = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(taxes) FROM {$table_trans} WHERE order_id < '1' AND user_id = %s AND trans_type = %s AND paid_date = %s", $current_user->ID, 'cart', '' ) );

	$item_name = get_post_meta( $post->ID, 'cp_item_name', true );
	if ( empty( $item_name ) ) $item_name = $post->post_title;

	return array(
		'EnableSideBar' => $sidebar,
		'ItemName' => $item_name,
		'AdURL' => get_permalink( $post->ID ),
		'ItemsInCart' => $items,
		'SumSubTotal' => $sub_total,
		'SumTaxes' => $taxes,
		'SumTotal' => $sub_total + $taxes,
		'SummerTotal' => $sub_total + $taxes,
		'ItemPrice' => $price,
		'ItemPriceTax' => $price + $tax,
		'LinkOpt' => $link,
		'ImgURL' => awsolutions_image_url( $post->ID ),
		'CartURL' => CLP_CART_URL,
		'LoggedIn' => $logged_in,
		'HeaderLink' => $header_link,
		'PrimaryLink' => $primary_link
	);

}


// Translate the jquery/ajax text strings.
function awsolutions_localize_vars() {

	return array(
		'ContinueShop' => __( 'Continue Shopping', CLP_EC ),
		'Checkout' => __( 'Checkout', CLP_EC ),
		'ProceedCheckout' => __( 'Proceed to Checkout', CLP_EC ),
		'ViewCart' => __( 'View Cart', CLP_EC ),
		'SubTotal' => __( 'Subtotal:', CLP_EC ),
		'CartSummary' => __( 'Cart Summary', CLP_EC ),
		'ShoppingCart' => __( 'Order Overview', CLP_EC ),
		'Taxes' => __( 'Taxes:', CLP_EC ),
		'Total' => __( 'Total:', CLP_EC ),
		'YourCartEmpty' => __( 'Your cart is empty.', CLP_EC ),
		'OrderDetails' => __( 'Order Details', CLP_EC ),
		'WasAddedCart' => __( 'was just added to your cart.', CLP_EC ),
	);

}


// Get post id by postmeta key and value.
function awsolutions_get_meta_postid( $key, $value ) {

	global $wpdb;
	$key = esc_sql( $key );
	$value = esc_sql( $value );
	$meta = $wpdb->get_results("SELECT * FROM `".$wpdb->postmeta."` WHERE meta_key='".$key."' AND meta_value='".$value."'");
	if (is_array($meta) && !empty($meta) && isset($meta[0])) {
	$meta = $meta[0];
	}	
	if (is_object($meta)) {
	return $meta->post_id;
	}
	else {
	return false;
	}
}


// Returns associated listing ID for given order, false if not found.
function awsolutions_get_order_ad_id( $order_id ) {

	$order = appthemes_get_order( $order_id );
	foreach ( $order->get_items( 'product' ) as $item ) {
		return $item['post_id'];
	}

	return false;
}


// Check the order type from order array.
function awsolutions_get_order_type( $order_id ) {

	$order = appthemes_get_order( $order_id );
	foreach ( $order->get_items( 'product' ) as $item ) {
		return $item['type'];
	}

	return false;
}


// Delete complete order based on order id.
function awsolutions_delete_complete_order( $order_id ) {

	global $wpdb;

	$table_p2p = $wpdb->prefix . "p2p";
	$table_p2pmeta = $wpdb->prefix . "p2pmeta";

	$orders = $wpdb->get_results("SELECT * FROM {$table_p2p} WHERE p2p_from = '$order_id'");
	if ( $orders ) {
		foreach ( $orders as $row ) {
		$wpdb->query($wpdb->prepare("DELETE FROM {$table_p2pmeta} WHERE p2p_id = '%d'", $row->p2p_id));
		}
	}

	$wpdb->query($wpdb->prepare("DELETE FROM {$table_p2p} WHERE p2p_from = '%d'", $order_id));
	wp_delete_post( $order_id, true );

}


// Get author id based on order id.
function awsolutions_get_author( $order_id ) {
	global $wpdb;

	$table_trans = $wpdb->prefix . "clp_transactions";
	$res = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE order_id = '$order_id'");

	if ( $res ) return $res->post_author;
	else return false;

}


// Check the post type from order array.
function awsolutions_get_item( $order_id ) {

	$order = appthemes_get_order( $order_id );
	foreach ( $order->get_items() as $item ) {
		return $item;
	}

	return false;
}


function awsolutions_update_order_table() {

	global $post, $clp_options, $current_user, $wpdb;

	$order = get_order();
	$order_id = $order->get_id();
	$item = awsolutions_get_order_type( $order_id );
	
	if ( $item !== 'product' )
		return;

	$post_ids = explode( ",",$_GET['process_cart'] );
	$table_trans = $wpdb->prefix . "clp_transactions";

	foreach( $post_ids as $post_id ) {
	$post = get_post( $post_id );
	$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE post_id = '$post_id' AND user_id = '$current_user->ID' AND paid_date = ''" );
	if ( $row ) {
		if ( $row->order_id !== $order_id ) awsolutions_delete_complete_order( $row->order_id );
		$wpdb->query( $wpdb->prepare("UPDATE $table_trans SET order_id = %d, status = %s WHERE id = %d", $order_id, 'pending', $row->id ) );
		}
	}

	$buyer = get_user_meta( $current_user->ID, 'clp_my_orders', true );
	$buyer[] = $order_id;
	update_user_meta( $current_user->ID, 'clp_my_orders', $buyer );

	$seller = get_user_meta( $post->post_author, 'clp_my_sales', true );
	$seller[] = $order_id;
	update_user_meta( $post->post_author, 'clp_my_sales', $seller );

	$complete_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	update_post_meta( $order_id, 'complete_url', $complete_url );
	update_post_meta( $order_id, 'cancel_url', $complete_url );

}
add_action( 'appthemes_before_order_summary', 'awsolutions_update_order_table' );



// Check the payment receiver etc.
function awsolutions_check_receiver( $order, $type ) {

	$result = false;
	$item = awsolutions_get_order_type( $order->get_id() );
	$author_id = awsolutions_get_author( $order->get_id() );

	if ( $type == "paypal" && $item == "product" ) {
		$result = get_user_meta( $author_id, 'paypal_email', true );
	}
	else if ( $type == "skrill" && $item == "product" ) {
		$result = get_user_meta( $author_id, 'skrill_email', true );
	}
	else if ( $type == "skrill_id" && $item == "product" ) {
		$result = get_user_meta( $author_id, 'skrill_account_id', true );
	}
	else if ( $type == "bank-transfer" && $item == "product" ) {
		$result = get_user_meta( $author_id, 'bt_email', true );
	}
	else if ( $type == "bt_message" && $item == "product" ) {
		$result = get_user_meta( $author_id, 'bt_message', true );
	}
	else if ( $type == "simplepay4u" && $item == "product" ) {
		$result = get_user_meta( $author_id, 'simplepay4u_email', true );
	}
	else if ( $type == "nocards" && $item == "product" ) {
		$result = get_user_meta( $author_id, 'simplepay4u_nocards', true );
	}
	else {
		$result = false;
	}

	if ( $result ) return $result;
	else return false;
}


// Get the main image associated to the ad used on the single page.
if (!function_exists('awsolutions_image_url')) {
	function awsolutions_image_url( $post_id ) {
		global $post, $wpdb;

	$args = array( 'post_parent' => $post_id, 'post_type' => 'attachment', 'numberposts' => 1, 'post_mime_type' => 'image', 'post_status' => 'inherit', 'order' => 'ASC', 'orderby' => 'ID' ); 
	$attachments = get_posts( $args );
	if ( $attachments ) {
		foreach ( $attachments as $attachment ) {
			$src = wp_get_attachment_image_src( $attachment->ID, "attached-image" );
			$alt = get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true );
			$src_url = $src['0'];

			return "<img src='$src_url' alt='$alt' title='$alt' height='37' width='50' />";
			}
		}
		else {
			$no_img = appthemes_locate_template_uri('images/no-thumb.jpg');

			return "<img src='$no_img' alt='' title='' height='37' width='50' />";
		}
	}
}


// Get the taxt rate.
function awsolutions_get_taxes() {

	if ( ! defined( 'APP_POST_TYPE' ) )
		return;

	$options = APP_Gateway_Registry::get_options();
	$tax_rate = $options->tax_charge;

	if ( $tax_rate > 0 ) return $tax_rate;
	else return false;

}


// Calculate the charged tax.
function awsolutions_get_charged_tax( $order = '', $price = '' ) {

	if ( ! defined( 'APP_POST_TYPE' ) )
		return;

	$options = APP_Gateway_Registry::get_options();
	$tax_rate = $options->tax_charge;

	if ( $order ) {
	$total = $order->get_total();
	if ( ( $price ) && ( $price < $total ) ) {
	$total = $price;
	$charged_tax = $total * ( $tax_rate / 100 );
	}
	else {
		$charged_tax = 0;
	}
	}
	else {
	$charged_tax = $price * ( $tax_rate / 100 );
	}	

	if( $charged_tax == 0 )
		return;

	return $charged_tax;

}


// Setup the gateway dropdown for the order checkout page.
function awsolutions_gateway_dropdown( $order ) {
	global $clp_options;

	$author_id = awsolutions_get_author( $order->get_id() );
	$user_bt = get_user_meta( $author_id, 'bt_email', true );
	$user_paypal = get_user_meta( $author_id, 'paypal_email', true );
	$user_skrill = get_user_meta( $author_id, 'skrill_email', true );
	$user_simplepay4u = get_user_meta( $author_id, 'simplepay4u_email', true );

	if ( awsolutions_enabled_gateways( 'bank-transfer' ) == 1 && $clp_options['bank_transfer'] == "bank_transfer" && $user_bt ) $bt = true;
	else $bt = false;
	if ( awsolutions_enabled_gateways( 'paypal' ) == 1 && $clp_options['paypal'] == "paypal" && $user_paypal ) $paypal = true;
	else $paypal = false;
	if ( awsolutions_enabled_gateways( 'skrill' ) == 1 && $clp_options['skrill'] == "skrill" && $user_skrill ) $skrill = true;
	else $skrill = false;
	if ( awsolutions_enabled_gateways( 'simplepay4u' ) == 1 && $clp_options['simplepay4u'] == "simplepay4u" && $user_simplepay4u ) $simplepay4u = true;
	else $simplepay4u = false;

	$gateways = array(
		'' => __( 'Select..', CLP_EC ),
		'bank-transfer' => $bt == true ? __( 'Bank Transfer', CLP_EC ) : '',
		'paypal' => $paypal == true ? 'PayPal' : '',
		'skrill' => $skrill == true ? 'Skrill' : '',
		'simplepay4u' => $simplepay4u == true ? 'SimplePay4u' : ''
		);

	echo scbForms::input( array(
		'type' => 'select',
		'name' => 'payment_gateway',
		'values' => $gateways,
		'extra' => array( 'class' => 'required' )
	) );

}


// Checks if a given gateway is enabled.
function awsolutions_enabled_gateways( $gateway_id ) {
	global $cp_options;

	if( ! is_string( $gateway_id ) )
		trigger_error( 'Gateway ID must be a string', E_USER_WARNING );

	$enabled_gateways = $cp_options->gateways['enabled'];

	if ( isset( $enabled_gateways[$gateway_id] ) ) return $enabled_gateways[$gateway_id];
}


// Display the Cart (0) link in header bar.
if ( !function_exists( 'cp_login_head' ) && $clp_options['cart_link_header'] == "header" && ! stristr( $clp_options['child_theme'], 'headline' ) ) {
	function cp_login_head() {

	if ( is_user_logged_in() ) :
	global $current_user, $wpdb;
	$display_user_name = cp_get_user_name();
	$logout_url = cp_logout_url();

	$table_trans = $wpdb->prefix . "clp_transactions";
	$items = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(quantity) FROM {$table_trans} WHERE order_id < '1' AND user_id = %s AND trans_type = %s AND paid_date = %s", $current_user->ID, 'cart', '' ) );
	if ( empty( $items ) ) $items = 0;
?>

	<?php _e( 'Welcome,', CLP_EC ); ?> <strong><?php echo $display_user_name; ?></strong> [ <a href="<?php echo CP_DASHBOARD_URL; ?>"><?php _e( 'My Dashboard', CLP_EC ); ?></a> | <a href="<?php echo CLP_CART_URL; ?>"><?php _e( 'Cart', CLP_EC ); ?> (<span class="head-quantity"><?php echo $items; ?></span>)</a> | <a href="<?php echo $logout_url; ?>"><?php _e( 'Log out', CLP_EC ); ?></a> ]&nbsp;
	<?php else : ?>
	<?php _e( 'Welcome,', CLP_EC ); ?> <strong><?php _e( 'visitor!', CLP_EC ); ?></strong> [ <a href="<?php echo appthemes_get_registration_url(); ?>"><?php _e( 'Register', CLP_EC ); ?></a> | <a href="<?php echo wp_login_url(); ?>"><?php _e( 'Login', CLP_EC ); ?></a> ]&nbsp;
	<?php endif;

	}
}


if (!function_exists('fl_login_head') && $clp_options['child_theme'] == "flatpress" && $clp_options['cart_link_header'] == "header" ) {
	function fl_login_head() {

	if ( is_user_logged_in() ) :
	global $current_user, $wpdb;
	$display_user_name = cp_get_user_name();
	$logout_url = cp_logout_url();

	$table_trans = $wpdb->prefix . "clp_transactions";
	$items = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(quantity) FROM {$table_trans} WHERE order_id < '1' AND user_id = %s AND trans_type = %s AND paid_date = %s", $current_user->ID, 'cart', '' ) );
	if ( empty( $items ) ) $items = 0;
?>

	<a href="<?php echo CP_DASHBOARD_URL; ?>"><?php _e( 'My Dashboard', CLP_EC ); ?></a> | <a href="<?php echo CLP_CART_URL; ?>"><?php _e( 'Cart', CLP_EC ); ?> (<span class="head-quantity"><?php echo $items; ?></span>)</a> | <a href="<?php echo $logout_url; ?>"><?php _e( 'Log out', CLP_EC ); ?></a> |&nbsp;
	<?php else : ?>
	<a href="<?php echo appthemes_get_registration_url(); ?>"><?php _e( 'Register', CLP_EC ); ?></a> | <a href="<?php echo wp_login_url(); ?>"><?php _e( 'Login', CLP_EC ); ?></a> |&nbsp;
	<?php endif;

	}
}


// Display the Cart (0) link into header / primary menu.
function awsolutions_menu_filter( $items, $args ) {
	global $current_user, $wpdb, $clp_options;

	if( $args->theme_location == "primary" && $clp_options['cart_link_primary'] == "primary" ) {

	$table_trans = $wpdb->prefix . "clp_transactions";
	$count = $wpdb->get_var( $wpdb->prepare( "SELECT SUM(quantity) FROM {$table_trans} WHERE order_id < '1' AND user_id = %s AND trans_type = %s AND paid_date = %s", $current_user->ID, 'cart', '' ) );
	if ( empty( $count ) ) $count = 0;

	if ( is_page_template( 'tpl-shopping-cart.php' ) )
		$class = "current-menu-item";
	else
		$class = false;    

	// Our content we are adding to the menu output
	$ext = '<li class="'.$class.'"><a href="'.CLP_CART_URL.'">'.__( 'Cart', CLP_EC ).' (<span class="primary-quantity">'.$count.'</span>)</a></li>';

	// setting $items to our content plus the existing output
	$items = $items . $ext;

	// Simple return our new $items variable
	return $items;

	}
	else {

	// for other menus
	return $items;

	}
}
add_filter( 'wp_nav_menu_items', 'awsolutions_menu_filter', 10, 2 );


// Display success and error notice.
function awsolutions_show_notice( $clp_errors ) {
	global $clp_errors;

	if ( isset( $_POST['save_changes'] ) ) {

		if ( $clp_errors == "email" ) {
		appthemes_display_notice( 'error', __( 'Please use an valid email address!', CLP_EC ) );
		} else {
		appthemes_display_notice( 'success', __( 'Changes was saved successfully!', CLP_EC ) );
		}

	}

}