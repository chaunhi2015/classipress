<?php

/* Load PDF library */
require_once( NC_INVOICES_BASE_DIR.'lib/dompdf/dompdf_config.inc.php' );

class InvoiceDOMPDF extends Invoice {

	protected function get_pdf_output() {
		$pdf_output = self::$options['pdf_output'];
		return $pdf_output;
	}

	protected function get_pdf_paper() {
		$pdf_paper = self::$options['pdf_paper'];
		return $pdf_paper;
	}

	public function output() {

		$type = false;
		if ( isset( $_GET['id'] ) && function_exists('awsolutions_get_order_type') ) {
		$type = awsolutions_get_order_type( $_GET['id'] );
		$author_id = awsolutions_get_author( $_GET['id'] );
		$get_tax_id = get_user_meta( $author_id, '_invoices_tax_id', true );
		}

		if ( strlen( $this->get_logo_url() ) > 0 ) :
			$logo = '<img class="logo" src="'.$this->get_logo_url().'" />';
			/* Set image size... */
			list( $width, $height ) = getimagesize( $this->get_logo_url() );
			$logo_width = ( $width > 0 ) ? $width.'px' : '150px';
		else :
			$logo = '<strong>'.get_bloginfo( 'name' ).'</strong>';
			$logo_width = '0';
		endif;
		
		if ( $this->tax_enabled() ) :
			$title = self::$labels['title_tax'];
		else :
			$title = self::$labels['title'];
		endif;

		$tax_id = '';
		if ( ( $this->tax_enabled() ) && ( strlen( $this->get_tax_id() ) > 0 ) ) :
			$tax_id = '<strong>'.self::$labels['tax_id'].'</strong><p>'.$this->get_tax_id().'</p>';
		endif;

		$customer_address = '';
		if ( $this->get_customer_address() ) :
			$customer_address = '<strong>'.self::$labels['customer_address'].'</strong><p>'.nl2br( $this->get_customer_address() ).'</p>';
		endif;

		$customer_tax_id = '';
		if ( $this->tax_enabled() && $this->get_customer_tax_id() ) :
			$customer_tax_id = '<strong>'.self::$labels['customer_tax_id'].'</strong><p>'.$this->get_customer_tax_id().'</p>';
		endif;

		$fontstack = array(
			'courier' => '"Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace',
			'helvetica' => '"Helvetica Neue", Helvetica, Arial, sans-serif',
			'times' => 'TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif',
			'opensans' => 'OpenSans',
			'inconsolata' => 'Inconsolata',
		);

		if ( $type == "product" ) {
			$logo = '<strong>'.get_user_meta( $author_id, '_invoices_name', true ).'</strong>';
			$get_address = nl2br( get_user_meta( $author_id, '_invoices_address', true ) );
			if ( ( $this->tax_enabled() ) && ( strlen( $get_tax_id ) > 0 ) ) :
			$tax_id = '<strong>'.self::$labels['tax_id'].'</strong><p>'.$get_tax_id.'</p>';
			endif;
			$description = false;
		}
		else {
			$get_address = $this->get_address();
			$description = $this->get_description();
		}

		$font  = $fontstack[ 'inconsolata' ];
		$html  = '
		<html>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<meta name="author" content="NomadCode.net"/>
			<meta name="keywords" content="NomadCode.net, AppThemes, Invoices"/>
			<title>'.$this->get_title().'</title>
		<style>

			body {
				font-family: '.$font.';
				font-size: 14px;
			}	
				
			table {
				width: 100%;
				border-collapse: collapse;
			}
			
			.top {
				vertical-align: top;
			}
			
			img.logo {
				width: '.$logo_width.';
				max-width: 350px;
			}

			.top {
				vertical-align: top;
			}
			
			table#details p {
				margin-top: 0;
			}

			tr {
				vertical-align: top;
			}

			table#items td,
			table#items th {
				padding: 5px;
			}
			
			p#message {
				padding-top: 20px;
				font-style: italic;
			}

			.full {
				width: 100%;
			}

			.half {
				width: 50%;
			}
			
			.nowrap {
				white-space: nowrap;
			}
			
			.center {
				text-align: center;
			}
			
			.right {
				text-align: right;
			}
			
			.left {
				text-align: left;
			}
			
			.noborder {
				border-width: 0;
			}
			
			.border {
				border: 1px solid black;
			}
			
			.border-heavy {
				border-width: 2px;
			}

			small {
				font-size: 12px;
			}

		</style>
		</head>  
		<body>

			<table>
				<tr>
					<td class="half">
						<div>'.$logo.'</div>
					</td>
					<td class="top">
						<table>
							<tr>
								<td class="full"></td>
								<td class="nowrap">
								<p>'.$get_address.'</p>
								'.$tax_id.'
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<hr/>

			<div class="center">
				<p><strong>'.$title.'</strong></p>
				<p><strong>'.$description.'</strong></p>
			</div>

			<table id="details">
				<tr>
					<td class="half">
						<strong>'.self::$labels['order_ref'].'</strong>
						<p>'.$this->get_reference().'</p>
						<strong>'.self::$labels['customer_name'].'</strong>
						<p>'.$this->get_customer_name().'</p>
						'.$customer_address.'
						'.$customer_tax_id.'
					</td>
					<td class="half">
						<table>
							<tr>
								<td class="full"></td>
								<td class="nowrap">
									<strong>'.self::$labels['order_date'].'</strong>
									<p>'.$this->get_date().'</p>
									<strong>'.self::$labels['generated'].'</strong>
									<p>'.appthemes_display_date( time(), 'date' ).'</p>
									<strong>'.self::$labels['order_status'].'</strong>
									<p>'.$this->get_status().'</p>
									<strong>'.self::$labels['order_gateway'].'</strong>
									<p>'.$this->get_gateway().'</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<br/>

			<table id="items">
				<tr>
					<th class="full left border">'.self::$labels['item'].'</th>
					<th class="nowrap center border">'.self::$labels['quantity'].'</th>
					<th class="nowrap center border">'.self::$labels['price'].'</th>
				</tr>
		';
		$items = $this->get_items();
		$post_id = $_GET['id'];

		global $wpdb;

		foreach ( $items as $item ) {
		$post_ID = $item['post_id'];

		if ( defined( 'CLP_EC_PLUGIN' ) ) {
		$table_trans = $wpdb->prefix . "clp_transactions";
		$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE order_id = '$post_id' AND post_id = '$post_ID'" );
		}

			if ( ! APP_Item_Registry::is_registered( $item['type'] ) ){
				$title = __( 'This item could not be recognized. It might be from another theme or an uninstalled plugin.', NC_INVOICES_TD );
				$price = 0;
			} else {
				$ptype_obj = get_post_type_object( $item['post']->post_type );
				$item_link = ( $ptype_obj->public ) ? html_link( get_permalink( $item['post_id'] ), $item['post']->post_title ) : '';

				if ( defined( 'CLP_EC_PLUGIN' ) && $item['type'] == "product" ) {
				$title = $row->item_name;
				if ( empty( $title ) ) $title = $item['post']->post_title;
				$quantity = $row->quantity;
				} else {
				$title = APP_Item_Registry::get_title( $item['type'] );
				$quantity = 1;
				}
				$price = appthemes_get_price( $item['price'], $this->get_order()->get_currency() );
			}			
			
			$html .= '<tr>';
			$html .= '<td class="full left border">';
			$html .= $title;
			$html .= '</td>';
			$html .= '<td class="nowrap center border">';
			$html .= $quantity;
			$html .= '</td>';
			$html .= '<td class="nowrap right border">';
			$html .= $price;
			$html .= '</td>';
			$html .= '</tr>';
		}

			if ( $this->tax_enabled() ) :

			$html.= '
				<tr>
					<td colspan="3"></td>
				</tr>
				<tr>
					<th colspan="2" class="full right">'.self::$labels['order_subtotal'].'</th>
					<td class="nowrap right border">'.$this->get_total( true ).'</td>
				</tr>
				<tr>
					<th colspan="2" class="full right">'.self::$labels['order_tax_rate'].'</th>
					<td class="nowrap right border">'.$this->get_tax_rate( ).'</td>
				</tr>
				<tr>
					<th colspan="2" class="full right">'.self::$labels['order_tax_amount'].'</th>
					<td class="nowrap right border">'.$this->get_tax_amount( ).'</td>
				</tr>
			';
			endif;

		$html    .= '
				<tr>
					<td colspan="3"></td>
				</tr>
				<tr>
					<th colspan="2" class="full right">'.self::$labels['order_total'].'</th>
					<td class="nowrap right border border-heavy"><strong>'.$this->get_total().'</strong></td>
				</tr>
			</table>

			<p id="message" class="center">'.$this->get_message().'</p>

		</body>
		</html>';
		
		$filename = get_bloginfo( 'name' ).' '.self::$labels['invoice'].' '.$this->get_reference().'.pdf';
		$filename = sanitize_file_name( $filename );
		$dompdf   = new DOMPDF();
		$dompdf->set_paper( $this->get_pdf_paper() );
		$dompdf->set_option( 'enable_remote', TRUE );
		$dompdf->set_option( 'dpi', 96 );
		$dompdf->load_html( $html, 'UTF-8' );
		$dompdf->render();
		$dompdf->stream( $filename, array( 'Attachment' => $this->get_pdf_output() ) );
	}

}