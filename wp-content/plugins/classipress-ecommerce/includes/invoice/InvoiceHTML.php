<?php

class InvoiceHTML extends Invoice {

	public function output() {

		$type = false;
		if ( isset( $_GET['id'] ) && function_exists('awsolutions_get_order_type') ) {
		$type = awsolutions_get_order_type( $_GET['id'] );
		$author_id = awsolutions_get_author( $_GET['id'] );
		$get_tax_id = get_user_meta( $author_id, '_invoices_tax_id', true );
		}

		$fontstack = array(
			'courier' => '"Courier New", Courier, "Lucida Sans Typewriter", "Lucida Typewriter", monospace;',
			'helvetica' => '"Helvetica Neue", Helvetica, Arial, sans-serif;',
			'times' => 'TimesNewRoman, "Times New Roman", Times, Baskerville, Georgia, serif;',
			'opensans' => 'OpenSans, "Helvetica Neue", Helvetica, Arial, sans-serif;',
			'inconsolata' => 'Inconsolata, "Helvetica Neue", Helvetica, Arial, sans-serif;',
		);
		$font = $fontstack[ $this->get_font() ];
		
		?>
		<!DOCTYPE html>
		<html>
		<head>
			<meta charset=utf-8 />
			<title><?php echo $this->get_title(); ?></title>
			<!--[if IE]>
				<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
			<style type="text/css">

				/*** CSS Reset ***/

				/* http://meyerweb.com/eric/tools/css/reset/ 
				   v2.0 | 20110126
				   License: none (public domain)
				*/

				html, body, div, span, applet, object, iframe,
				h1, h2, h3, h4, h5, h6, p, blockquote, pre,
				a, abbr, acronym, address, big, cite, code,
				del, dfn, em, img, ins, kbd, q, s, samp,
				small, strike, strong, sub, sup, tt, var,
				b, u, i, center,
				dl, dt, dd, ol, ul, li,
				fieldset, form, label, legend,
				table, caption, tbody, tfoot, thead, tr, th, td,
				article, aside, canvas, details, embed, 
				figure, figcaption, footer, header, hgroup, 
				menu, nav, output, ruby, section, summary,
				time, mark, audio, video {
					margin: 0;
					padding: 0;
					border: 0;
					font-size: 100%;
					font: inherit;
					vertical-align: baseline;
				}
				/* HTML5 display-role reset for older browsers */
				article, aside, details, figcaption, figure, 
				footer, header, hgroup, menu, nav, section {
					display: block;
				}
				body {
					line-height: 1;
				}
				ol, ul {
					list-style: none;
				}
				blockquote, q {
					quotes: none;
				}
				blockquote:before, blockquote:after,
				q:before, q:after {
					content: '';
					content: none;
				}
				table {
					border-collapse: collapse;
					border-spacing: 0;
				}

				/*** Invoice CSS ***/
				
				@page { 
					margin: 0.5cm;
				}
				
				body {
					font-family: <?php echo $font.PHP_EOL; ?>;
					font-size: 12pt;
					line-height: 1.3;            
					width: 670px;
					margin: 0 auto;
				}
				
				p {
					margin-bottom: 0.5em;
				}
				
				strong {
					font-weight: bold;
				}
				
				table {
					margin: 10px 0;
					width: 100%;
					border-collapse: collapse;
				}
				
				th, td {
					border: 1px solid #000;
				}
				
				th {
					font-weight: bold;
				}
				
				td {
					vertical-align: top;
				}
				
				.fullwidth {
					width: 100%;
				}
				
				.halfwidth {
					width: 50%;
				}
						
				td.total {
					text-align: right;
					border-width: 0;
					font-weight: bold;
				}

				small {
					font-size: 0.8em;
				}
				
				.logo {
					max-width: 95%;
					height: auto;
				}
				
				.left {
					text-align: left;
				}

				.right {
					text-align: right;
				}

				.center {
					text-align: center;
					margin-left: auto;
					margin-right: auto;
				}
				
				.border {
					border: 1px solid #000000;
				}
				
				.border-heavy {
					border-width: 2px;
				}

				.border-top {
					border-top-width: 1px;
					border-style: solid;
				}

				.border-bottom {
					border-bottom-width: 1px;
					border-style: solid;
				}
				
				.noborder {
					border: none;
				}
				
				.padding {
					padding: 10px;
				}

				.padding-half {
					padding: 5px;
				}
				
				.emphasis {
					font-style: italic;
				}
				
				.two-col {
					display: inline-block;
					width: 48%;
					margin-right: 4%;
					float: left;
				}
			
				.last {
					margin-right: 0 !important;
					clear: right;
				}
				
				.row {
					margin-top: 10px;
					overflow: hidden;
				}
				
				.faded {
					color: #666;
					border-color: #666;
				}
				
				.nowrap {
					white-space: nowrap;
				}
				
				.floatleft {
					float: left;
				}

				.floatright {
					float: right;
				}
				
				.clear {
					clear: both;
				}
				
				.details {
					max-width: 50%;
				}
			   
			</style>
		</head>
		<body>

			<div class="details floatleft">
				<?php if ( $type == "product" ) { ?>
				<strong><?php echo get_user_meta( $author_id, '_invoices_name', true ); ?></strong>
				<?php } else { ?>
				<?php if ( strlen( $this->get_logo_url() ) > 0 ) : ?>
				<img class="logo" src="<?php echo $this->get_logo_url(); ?>" />
				<?php else : ?>
				<strong><?php echo get_bloginfo( 'name' ); ?></strong>
				<?php endif; ?>
				<?php } ?>
			</div>

			<div class="details floatright">
				<?php if ( $type == "product" ) { ?>
				<p><?php echo nl2br( get_user_meta( $author_id, '_invoices_address', true ) ); ?></p>
				<?php if ( ( $this->tax_enabled() ) && ( strlen( $get_tax_id ) > 0 ) ) : ?>
				<strong><?php echo self::$labels['tax_id']; ?></strong>
				<p><?php echo $get_tax_id; ?></p>
				<?php endif; ?>
				<?php } else { ?>
				<p><?php echo $this->get_address(); ?></p>
				<?php if ( ( $this->tax_enabled() ) && ( strlen( $this->get_tax_id() ) > 0 ) ) : ?>
				<strong><?php echo self::$labels['tax_id']; ?></strong>
				<p><?php echo $this->get_tax_id(); ?></p>
				<?php endif; ?>
				<?php } ?>
			</div>
			
			<hr class="clear" />

			<div class="center">
				<p><strong>
					<?php
					if ( $this->tax_enabled() ) :
						echo self::$labels['title_tax'];
					else :
						echo self::$labels['title'];
					endif;
					?>
				</strong></p>
				<?php if ( $type != "product" ) { ?>
				<p><strong><?php echo( $this->get_description() ); ?></strong></p>
				<?php } ?>
			</div>

			<div class="details floatleft">

				<strong><?php echo self::$labels['order_ref']; ?></strong>
				<p><?php echo $this->get_reference(); ?></p>

				<?php if ( $this->get_customer_name() ) : ?>
				<strong><?php echo self::$labels['customer_name']; ?></strong>
				<p><?php echo $this->get_customer_name(); ?></p>
				<?php endif; ?>

				<?php if ( $this->get_customer_address() ) : ?>
				<strong><?php echo self::$labels['customer_address']; ?></strong>
				<p><?php echo nl2br( $this->get_customer_address() ); ?></p>
				<?php endif; ?>

				<?php if ( $this->tax_enabled() && $this->get_customer_tax_id() ) : ?>
				<strong><?php echo self::$labels['customer_tax_id']; ?></strong>
				<p><?php echo $this->get_customer_tax_id(); ?></p>
				<?php endif; ?>
			</div>

			<div class="details floatright">
				<strong><?php echo self::$labels['order_date']; ?></strong>
				<p><?php echo $this->get_date(); ?></p>
				<strong><?php echo self::$labels['generated']; ?></strong>
				<p><?php echo appthemes_display_date( time(), 'date' ); ?></p>
				<strong><?php echo self::$labels['order_status']; ?></strong>
				<p><?php echo $this->get_status(); ?></p>
				<strong><?php echo self::$labels['order_gateway']; ?></strong>
				<p><?php echo $this->get_gateway(); ?></p>
			</div>
			
			<br class="clear" />

			<table>
				<tr>
					<th class="left padding-half"><?php echo self::$labels['item']; ?></th>
					<th class="center padding-half"><?php echo self::$labels['quantity']; ?></th>
					<th class="center padding-half"><?php echo self::$labels['price']; ?></th>
				</tr>
				
		<?php
		$items = $this->get_items();
		$post_id = $_GET['id'];

		global $wpdb;

		foreach ( $items as $item ) {
		$post_ID = $item['post_id'];

		if ( defined( 'CLP_EC_PLUGIN' ) ) {
		$table_trans = $wpdb->prefix . "clp_transactions";
		$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE order_id = '$post_id' AND post_id = '$post_ID'" );
		}

			if ( ! APP_Item_Registry::is_registered( $item['type'] ) ){
				$title = __( 'This item could not be recognized. It might be from another theme or an uninstalled plugin.', NC_INVOICES_TD );
				$price = 0;
			} else {
				$ptype_obj = get_post_type_object( $item['post']->post_type );
				$item_link = ( $ptype_obj->public ) ? html_link( get_permalink( $item['post_id'] ), $item['post']->post_title ) : '';

				if ( defined( 'CLP_EC_PLUGIN' ) && $item['type'] == "product" ) {
				$title = $row->item_name;
				if ( empty( $title ) ) $title = $item['post']->post_title;
				$quantity = $row->quantity;
				} else {
				$title = APP_Item_Registry::get_title( $item['type'] );
				$quantity = 1;
				}
				$price = appthemes_get_price( $item['price'], $this->get_order()->get_currency() );
			}
				
					echo '<tr>';
					echo '<td class="fullwidth padding-half">';
					echo $title;
					echo '</td>';
					echo '<td class="center padding-half">';
					echo '1';
					echo '</td>';
					echo '<td class="right padding-half">';
					echo $price;
					echo '</td>';
					echo '</tr>';
					
				}
				?>

				<?php if ( $this->tax_enabled() ) : ?>
				<tr>
					<td class="noborder padding-half"></td>
				</tr>
				<tr>
					<td colspan="2" class="noborder right padding-half"><strong><?php echo self::$labels['order_subtotal']; ?></strong></td>
					<td class="border right padding-half"><strong><?php echo $this->get_total( true ); ?></strong></td>
				</tr>
				<tr>
					<td colspan="2" class="noborder right padding-half"><strong><?php echo self::$labels['order_tax_rate']; ?></strong></td>
					<td class="border right padding-half"><strong><?php echo $this->get_tax_rate( ); ?></strong></td>
				</tr>
				<tr>
					<td colspan="2" class="noborder right padding-half"><strong><?php echo self::$labels['order_tax_amount']; ?></strong></td>
					<td class="border right padding-half"><strong><?php echo $this->get_tax_amount( ); ?></strong></td>
				</tr>
				<?php endif; ?>

				<tr>
					<td class="noborder padding-half"></td>
				</tr>

				<tr>
					<td colspan="2" class="noborder right padding-half"><strong><?php echo self::$labels['order_total']; ?></strong></td>
					<td class="border border-heavy right padding-half"><strong><?php echo $this->get_total(); ?></strong></td>
				</tr>

			<?php echo apply_filters('invoice_after_total', '', $this ); ?>

			</table>
			
			<p class="padding center emphasis">
				<?php echo $this->get_message(); ?>
			</p>
						
		</body>
		</html>
		<?php
	
	}
}