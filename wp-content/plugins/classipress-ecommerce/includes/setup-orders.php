<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


class CLP_Setup_Order {

	function __construct() {

		$this->CLP_PRODUCT = 'product';

		add_action( 'wp_ajax_process_cart_orders', array( $this, 'ajax_process_cart_orders' ) );
		add_action( 'wp_ajax_setup_order', array( $this, 'ajax_setup_order' ) );
		add_action( 'wp_ajax_nopriv_setup_order', array( $this, 'ajax_setup_order' ) );
		add_action( 'after_setup_theme', array( $this, 'setup_orders' ), 1000 );
		add_action( 'appthemes_transaction_completed', array( $this, 'payments_handle_purchase_completed' ) );
		add_action( 'appthemes_transaction_activated', array( $this, 'payments_handle_purchase_activated' ) );

	}

	// Process shopping cart orders.
	function ajax_process_cart_orders() {
		global $post, $clp_options, $current_user, $wpdb;

		if ( isset( $_POST["post_ids"] ) ) {

		check_ajax_referer( 'ajax-cart-nonce-'.$_POST["post_ids"].'', 'security' );

		$post_ids = explode( ",",$_POST["post_ids"] );
		$table_trans = $wpdb->prefix . "clp_transactions";

		foreach( $post_ids as $post_id ) {
		$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE post_id = '$post_id' AND user_id = '$current_user->ID' AND paid_date = ''" );
		if ( $row ) {
			$order_id = $row->order_id;
		}
			$ids .= ''.$order_id.',';
		}

		$cust = explode(",",$ids);
		$pid = $cust[0];

		if ( $clp_options['cp_version'] < '3.4' ) {

			if ( $pid > 0 ) {
			$order = appthemes_get_order( $pid );
			}
			else {
			$order = appthemes_new_order();
			do_action( 'appthemes_create_order', $order );
			}
			$url = $this->new_cart_orders( $_POST["post_ids"], $order );

		}
		else {
			$url = $this->new_cart_orders( $_POST["post_ids"] );
		}

			$ret = empty( $url ) ? false : true;
			$response = array( 'success' => $ret, 'url' => $url );

		exit( json_encode( $response ) );

		}
	}

	// Setup shopping cart orders and return order summary/gateway url.
	function new_cart_orders( $post_array, $order = false ) {
		global $post, $clp_options, $current_user, $wpdb;

		$post_ids = explode( ",",$post_array );
		$table_trans = $wpdb->prefix . "clp_transactions";

		if ( $clp_options['cp_version'] < '3.4' ) {

			foreach( $post_ids as $post_id ) {
			$post = get_post( $post_id );
			$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE post_id = '$post_id' AND user_id = '$current_user->ID' AND paid_date = ''" );
			if ( $row ) {
			$order->remove_item( $this->CLP_PRODUCT, '', $post_id );
			$order->add_item( $this->CLP_PRODUCT, $row->sub_total, $post_id );
			$order->set_description( $row->item_name );

			$wpdb->query( $wpdb->prepare("UPDATE $table_trans SET order_id = %d, status = %s WHERE id = %d", $order->get_id(), 'pending', $row->id ) );

			$buyer = get_user_meta( $current_user->ID, 'clp_my_orders', true );
			$buyer[] = $order->get_id();
			update_user_meta( $current_user->ID, 'clp_my_orders', $buyer );

			$seller = get_user_meta( $post->post_author, 'clp_my_sales', true );
			$seller[] = $order->get_id();
			update_user_meta( $post->post_author, 'clp_my_sales', $seller );
			}
			}

			appthemes_payments_add_tax( $order );
			$url = $order->get_return_url();

		}
		else {
			$url = esc_url( add_query_arg( 'process_cart', $post_array, CLP_CART_URL ) );
		}

		return $url;

	}

	// Setup direct orders, and process same.
	function ajax_setup_order() {
		global $post, $current_user, $wpdb;

		if ( isset( $_POST["post_id"] ) ) {

		$post = get_post( $_POST["post_id"] );
		$item_name = get_post_meta( $post->ID, 'cp_item_name', true );
		if ( empty( $item_name ) ) $item_name = $post->post_title;
		$available = get_post_meta( $post->ID, 'cp_quantity', true );
		$unlimited = get_post_meta( $post->ID, 'cp_quantity_unlimited', true );
		$quantity = false; if ( isset( $_POST['quantity'] ) ) $quantity = $_POST['quantity'];
		$type = false; if ( isset( $_POST['type'] ) ) $type = $_POST['type'];

		check_ajax_referer( 'ajax-cart-nonce-'.$post->ID.'', 'security' );

		$table_trans = $wpdb->prefix . "clp_transactions";
		// Check if the item may already be added to users shopping cart.
		$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE post_id = '$post->ID' AND user_id = '$current_user->ID' AND status != 'paid'");
		// Make sure that the shopping cart contains only items from the same seller. 
		$res = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE user_id = '$current_user->ID' AND status != 'paid'");

		if( !is_user_logged_in() ) {
		$msg = '<div class="notice error"><div>'.__( 'You must be logged in before you can perform any purchase!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		}
		else if( !is_numeric( $_POST["quantity"] ) ) {
		$msg = '<div class="notice error"><div>'.__( 'Quantity field accept only numeric values!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		}
		else if( $current_user->ID == $post->post_author ) {
		$msg = '<div class="notice error"><div>'.__( 'You can not buy your own products!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		}
		else if( get_post_meta( $post->ID, 'cp_ad_sold', true ) == "yes" ) {
		$msg = '<div class="notice error"><div>'.__( 'Sorry, this product is sold out!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		}
		else if( ( $res && $type == "cart" ) && ( $post->post_author != $res->post_author ) ) {
		$msg = '<div class="notice error"><div>'.sprintf( __( 'Sorry, you can not add items to cart from several different sellers, %s', CLP_EC ), '<a href="'.CLP_CART_URL.'">'.__( 'view cart', CLP_EC ).'</a>.' ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		}
		else {

		if ( $type == "direct" ) {

			$url = $this->new_direct_order( $post, $item_name, $available, $unlimited, $quantity );
			$ret = empty( $url ) ? false : true;
			$response = array( 'success' => $ret, 'url' => $url, 'type' => 'direct' );

		exit( json_encode( $response ) );

		}
		else if ( $type == "cart" ) {

		if ( $row ) {
		$quantity = $_POST["quantity"] + $row->quantity;
		$adjust = $available - $_POST["quantity"];
		if ( $unlimited == "no" && $adjust > -1 ) {
		update_post_meta( $post->ID, 'cp_quantity', $adjust );
		}
		else if ( $unlimited == "no" && $adjust < 0 ) {
		$msg = '<div class="notice error"><div>'.__( 'Unfortunately, the seller does not have the desired number available!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		exit( json_encode( $response ) );
		}
		$charged_tax = awsolutions_get_charged_tax( '', $_POST["sub_total"] );
		$wpdb->query( $wpdb->prepare("UPDATE $table_trans SET taxes = %f, quantity = %d, sub_total = %f, trans_type = %s WHERE post_id = %d AND user_id = %d AND ( status = %s OR status = %s )", $charged_tax, $quantity, $_POST["sub_total"], 'cart', $post->ID, $current_user->ID, 'cart', 'pending' ) );
		}
		else {
		$adjust = $available - $_POST["quantity"];
		if ( $unlimited == "no" && $adjust > -1 ) {
		update_post_meta( $post->ID, 'cp_quantity', $adjust );
		}
		else if ( $unlimited == "no" && $adjust < 0 ) {
		$msg = '<div class="notice error"><div>'.__( 'Unfortunately, the seller does not have the desired number available!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		exit( json_encode( $response ) );
		}
		$charged_tax = awsolutions_get_charged_tax( '', $_POST["sub_total"] );
		$query = "INSERT INTO {$table_trans} ( post_id, post_title, item_name, post_author, price, taxes, user_id, quantity, sub_total, trans_type, status ) VALUES ( %d, %s, %s, %d, %f, %f, %d, %d, %f, %s, %s )";
		$wpdb->query( $wpdb->prepare( $query, $post->ID, $post->post_title, $item_name, $post->post_author, $_POST["price"], $charged_tax, $current_user->ID, $_POST["quantity"], $_POST["sub_total"], 'cart', 'cart' ) );
		}
		$response = array( 'success' => true, 'notices' => '' );
		}
		}

		exit( json_encode( $response ) );

		}

	}

	// Setup direct orders and return order summary/gateway url.
	function new_direct_order( $post, $item_name, $available, $unlimited, $quantity ) {
		global $post, $clp_options, $current_user, $wpdb;

		$price = get_post_meta( $post->ID, 'cp_price', true );
		$sub_total = $price * $quantity;

		$table_trans = $wpdb->prefix . "clp_transactions";
		$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE post_id = '$post->ID' AND user_id = '$current_user->ID' AND status != 'paid'" );

		if ( $row && $row->status == "pending" ) {

		$adjust = $available;
		if ( $quantity > $row->quantity ) $adjust = $available - ( $quantity - $row->quantity );
		else if ( $quantity < $row->quantity ) $adjust = $available + ( $row->quantity - $quantity );

		if ( $unlimited == "no" && $adjust > -1 ) {
		update_post_meta( $post->ID, 'cp_quantity', $adjust );
		}
		else if ( $unlimited == "no" && $adjust < 0 ) {
		$msg = '<div class="notice error"><div>'.__( 'Unfortunately, the seller does not have the desired number available!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		exit( json_encode( $response ) );
		}

		if ( $clp_options['cp_version'] < '3.4' ) {

		$order = appthemes_get_order( $row->order_id );

		$order->remove_item( $this->CLP_PRODUCT, '', $post->ID );
		$order->add_item( $this->CLP_PRODUCT, $sub_total, $post->ID );

		appthemes_payments_add_tax( $order );

		$charged_tax = awsolutions_get_charged_tax( '', $sub_total );
		$wpdb->query( $wpdb->prepare("UPDATE $table_trans SET taxes = %f, quantity = %d, sub_total = %f WHERE id = %d", $charged_tax, $quantity, $sub_total, $row->id ) );

		$url = $order->get_return_url();

		}
		else {

		$charged_tax = awsolutions_get_charged_tax( '', $sub_total );
		$wpdb->query( $wpdb->prepare("UPDATE $table_trans SET taxes = %f, quantity = %d, sub_total = %f WHERE id = %d", $charged_tax, $quantity, $sub_total, $row->id ) );

		$url = esc_url( add_query_arg( 'process_cart', $post->ID, get_permalink( $post->ID ) ) );

		}

		return $url;

		}
		else {

		if ( $clp_options['cp_version'] < '3.4' ) {

		$order = appthemes_new_order();
		$order->add_item( $this->CLP_PRODUCT, $sub_total, $post->ID );
		$order->set_description( $item_name );
		do_action( 'appthemes_create_order', $order );

		$adjust = $available - $quantity;
		if ( $unlimited == "no" && $adjust > -1 ) {
		update_post_meta( $post->ID, 'cp_quantity', $adjust );
		}
		else if ( $unlimited == "no" && $adjust < 0 ) {
		$msg = '<div class="notice error"><div>'.__( 'Unfortunately, the seller does not have the desired number available!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		exit( json_encode( $response ) );
		}
		$charged_tax = awsolutions_get_charged_tax( '', $sub_total );
		$query = "INSERT INTO {$table_trans} ( order_id, post_id, post_title, item_name, post_author, price, taxes, user_id, quantity, sub_total, trans_type, status ) VALUES ( %d, %d, %s, %s, %d, %f, %f, %d, %d, %f, %s, %s )";
		$wpdb->query( $wpdb->prepare( $query, $order->get_id(), $post->ID, $post->post_title, $item_name, $post->post_author, $price, $charged_tax, $current_user->ID, $quantity, $sub_total, 'direct', 'pending' ) );

		$buyer = get_user_meta( $current_user->ID, 'clp_my_orders', true );
		$buyer[] = $order->get_id();
		update_user_meta( $current_user->ID, 'clp_my_orders', $buyer );

		$seller = get_user_meta( $post->post_author, 'clp_my_sales', true );
		$seller[] = $order->get_id();
		update_user_meta( $post->post_author, 'clp_my_sales', $seller );

		$url = $order->get_return_url();

		}
		else {

		$adjust = $available - $quantity;
		if ( $unlimited == "no" && $adjust > -1 ) {
		update_post_meta( $post->ID, 'cp_quantity', $adjust );
		}
		else if ( $unlimited == "no" && $adjust < 0 ) {
		$msg = '<div class="notice error"><div>'.__( 'Unfortunately, the seller does not have the desired number available!', CLP_EC ).'</div></div>';
		$response = array( 'success' => false, 'notices' => $msg );
		exit( json_encode( $response ) );
		}
		$charged_tax = awsolutions_get_charged_tax( '', $sub_total );
		$query = "INSERT INTO {$table_trans} ( post_id, post_title, item_name, post_author, price, taxes, user_id, quantity, sub_total, trans_type, status ) VALUES ( %d, %s, %s, %d, %f, %f, %d, %d, %f, %s, %s )";
		$wpdb->query( $wpdb->prepare( $query, $post->ID, $post->post_title, $item_name, $post->post_author, $price, $charged_tax, $current_user->ID, $quantity, $sub_total, 'direct', 'pending' ) );

		$url = esc_url( add_query_arg( 'process_cart', $post->ID, get_permalink( $post->ID ) ) );

		}

		return $url;

		}

	}

	function payments_enabled() {
		global $cp_options;

		if ( ! current_theme_supports( 'app-payments' ) || ! current_theme_supports( 'app-price-format' ) ) {
			return false;
		}

		return true;
	}

	function setup_orders() {

	global $clp_options;

		if ( ! defined( 'APP_POST_TYPE' ) ) {
			return;
		}

		$args = array(
			'type' => $this->CLP_PRODUCT,
			'title' => __( 'Product', CLP_EC ),
			'meta' => array(),
		);
		APP_Item_Registry::register( $args['type'], $args['title'], $args['meta'] );

		if ( class_exists( 'APP_Checkout_Step' ) && $clp_options['cp_version'] > '3.3.3' ) {
			require_once( 'order.php' );
		}

	}

	function payments_handle_purchase_completed( $order ) {
		global $current_user, $wpdb;

		$post_id = $this->_cp_get_order_ad_id( $order );
		if ( ! $post_id ) {
			return;
		}

		$order->activate();

		$tm = time();
		$table_trans = $wpdb->prefix . "clp_transactions";
		$where_array = array( 'order_id' => $order->get_id() );
		$wpdb->update( $table_trans, array( 'paid_date' => $tm ), $where_array );

		if ( ! is_admin() ) {
			if ( did_action( 'wp_head' ) ) {
				cp_js_redirect( CLP_SUCCESS_URL );
			} else {
				wp_redirect( CLP_SUCCESS_URL );
			}
		}
	}

	function payments_handle_purchase_activated( $order ) {
		global $current_user, $wpdb;

		$post_id = $this->_cp_get_order_ad_id( $order );
		if ( ! $post_id ) {
			return;
		}

		$table_trans = $wpdb->prefix . "clp_transactions";
		$where_array = array( 'order_id' => $order->get_id() );
		$wpdb->update( $table_trans, array( 'status' => 'paid' ), $where_array );
	}

	function _cp_get_order_ad_id( $order ) {

		foreach ( $order->get_items( $this->CLP_PRODUCT ) as $item ) {
			return $item['post_id'];
		}
		return false;
	}
}

$CLP_Setup_Order = new CLP_Setup_Order();