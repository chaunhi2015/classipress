<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	global $clp_options, $post;


// Hooks into cp_ad_details_field to disable fields not to display in details.
function awsolutions_ad_details_fields( $result ) {

	if ( !$result )
		return false;

if( $result->field_name == "cp_enable_payment" ) return false;
if( $result->field_name == "cp_item_name" ) return false;
if( $result->field_name == "cp_quantity" ) return false;

else

return $result;
}
add_filter( 'cp_ad_details_field', 'awsolutions_ad_details_fields' );


// Allows to hook after ad details
function awsolutions_after_ad_details( $results, $post, $location ) {
	global $post;

	if( $location == "list" ) {
	$quantity = get_post_meta( $post->ID, 'cp_quantity', true );
	$unlimited = get_post_meta( $post->ID, 'cp_quantity_unlimited', true );

?>

	<div class="pad5"></div>

	<ul>

		<li id="cp_item_name" class=""><span><?php _e( 'Item Name:', CLP_EC ); ?></span> <strong><?php echo get_post_meta( $post->ID, 'cp_item_name', true ); ?></strong></li>
		<?php if ( $quantity == 0 && $unlimited == "yes" ) { ?>
		<li id="cp_quantity" class=""><span><?php _e( 'Availability:', CLP_EC ); ?></span> <?php _e( 'In stock', CLP_EC ); ?></li>
		<?php } else {
			if ( empty( $quantity ) ) $quantity = __( 'Sold out', CLP_EC ); ?>
		<li id="cp_quantity" class=""><span><?php _e( 'Quantity:', CLP_EC ); ?></span> <?php echo $quantity; ?></li>
		<?php } ?>

	</ul>
<?php
	}
}
add_action('cp_action_after_ad_details', 'awsolutions_after_ad_details', 10, 3);


// Do the necessary changes to the new ad listing
function awsolutions_new_listing( $post_id ) {

	$quantity = get_post_meta( $post_id, 'cp_quantity', true );


    	if ( ! wp_is_post_revision( $post_id ) ) {
	remove_action('cp_action_add_new_listing', 'awsolutions_new_listing');

	if ( $quantity == 0 ) {
	update_post_meta( $post_id, 'cp_quantity_unlimited', 'yes' );
	}
	else {
	update_post_meta( $post_id, 'cp_quantity_unlimited', 'no' );
	}

	add_action('cp_action_add_new_listing', 'awsolutions_new_listing');
	}
}
add_action( 'cp_action_add_new_listing', 'awsolutions_new_listing', 10, 1 );


// Do the necessary changes during ad editing
function awsolutions_edit_listing( $post_id ) {

	$quantity = get_post_meta( $post_id, 'cp_quantity', true );


    	if ( ! wp_is_post_revision( $post_id ) ) {
	remove_action('cp_action_add_new_listing', 'awsolutions_edit_listing');

	if ( $quantity == 0 ) {
	update_post_meta( $post_id, 'cp_quantity_unlimited', 'yes' );
	}
	else {
	update_post_meta( $post_id, 'cp_quantity_unlimited', 'no' );
	}

	add_action('cp_action_add_new_listing', 'awsolutions_edit_listing');
	}
}
add_action( 'cp_action_update_listing', 'awsolutions_edit_listing', 10, 1 );