<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


class CLP_eCommerce_View extends APP_View {

	function condition() {
		return ! empty( $_GET['process_cart'] );
	}

	function template_redirect() {
		global $CLP_Setup_Order;

		if ( empty( $CLP_Setup_Order ) )
			exit();

		appthemes_require_login();

	}

	function template_include( $path ) {

		$url = esc_url( add_query_arg( 'process_cart', $_GET['process_cart'], get_permalink() ) );

		appthemes_setup_checkout( 'process-cart', $url );

		$step_found = appthemes_process_checkout();

		if ( ! $step_found ) {
			return locate_template( '404.php' );
		}

		// use existing CP template
		return locate_template( 'edit-listing.php' );
	}

}

class CLP_eCommerce_Checkout_Step extends APP_Checkout_Step {

	public function __construct() {
		parent::__construct( 'add-item', array(
			'register_to' => array(
				'process-cart',
			)
		) );
	}

	public function process( $order, $checkout ) {
		global $CLP_Setup_Order, $post, $clp_options, $current_user, $wpdb;

		if ( empty( $CLP_Setup_Order ) )
			exit();

		$post_ids = explode( ",",$_GET['process_cart'] );
		$table_trans = $wpdb->prefix . "clp_transactions";

		foreach( $post_ids as $post_id ) {
		$post = get_post( $post_id );
		$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE post_id = '$post_id' AND user_id = '$current_user->ID' AND paid_date = ''" );
		if ( $row ) {
		$order->add_item( $CLP_Setup_Order->CLP_PRODUCT, $row->sub_total, $post_id );
		$order->set_description( $row->item_name );
		do_action( 'appthemes_create_order', $order );

		}
		}

		$this->finish_step();
	}
}

class CLP_eCommerce_Gateway_Select extends CP_Gateway_Select {

	public function __construct() {
		APP_Checkout_Step::__construct( 'gateway-select', array(
			'register_to' => array(
				'process-cart',
			)
		) );
	}

}

class CLP_eCommerce_Gateway_Process extends CP_Gateway_Process {

	public function __construct() {
		APP_Checkout_Step::__construct( 'gateway-process', array(
			'register_to' => array(
				'process-cart',
			)
		) );
	}

}

class CLP_eCommerce_Order_Summary extends CP_Order_Summary {

	public function __construct() {
		APP_Checkout_Step::__construct( 'order-summary', array(
			'register_to' => array(
				'process-cart',
			)
		) );
	}

}

new CLP_eCommerce_View;
new CLP_eCommerce_Checkout_Step;
new CLP_eCommerce_Gateway_Select;
new CLP_eCommerce_Gateway_Process;
new CLP_eCommerce_Order_Summary;