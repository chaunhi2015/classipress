<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

	global $clp_options;


// Unhook default ClassiPress functions
function unhooks_classipress_functions() {
	remove_action( 'manage_' . APPTHEMES_ORDER_PTYPE . '_posts_custom_column', 'appthemes_order_add_column_data', 10, 2 );
	remove_action( 'edit_form_advanced', 'appthemes_display_order_summary_table' );
}
add_action('init','unhooks_classipress_functions');


if ( ! defined( 'APPTHEMES_ORDER_PTYPE' ) ) {
	define( 'APPTHEMES_ORDER_PTYPE', 'transaction' );
}

/**
 * Outputs NEW column data for orders
 * @param  string $column_index Name of the column being processed
 * @param  int $post_id ID of order being dispalyed
 * @return void               
 */
function awsolutions_order_add_column_data( $column_index, $post_id ) {
	global $wpdb;

	$order = appthemes_get_order( $post_id );
	$table_trans = $wpdb->prefix . "clp_transactions";
	$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE order_id = '$post_id'" );

	switch( $column_index ){

		case 'order' :
			echo '<a href="' . get_edit_post_link( $post_id ) . '">' . $order->get_ID() . '</a>';
			break;

		case 'order_author':
			$user = get_userdata( $order->get_author() );
			echo $user->display_name;
			echo '<br>';
			echo $order->get_ip_address();
			break;

		case 'item' :

			if ( $row )
			$count = $row->quantity;
			else $count = count( $order->get_items() );
			$string = _n( 'Purchased %s item', 'Purchased %s items', $count, CLP_EC );

			printf( $string, $count );
			break;

		case 'price':
			$currency = $order->get_currency();
			if( !empty( $currency ) ){
				echo appthemes_get_price( $order->get_total(), $order->get_currency() );
			}else{
				echo appthemes_get_price( $order->get_total() );
			}
			break;

		case 'payment':

			$gateway_id = $order->get_gateway();

			if ( !empty( $gateway_id ) ) {
				$gateway = APP_Gateway_Registry::get_gateway( $gateway_id );
				if( $gateway ){
					echo $gateway->display_name( 'admin' );
				}else{
					_e( 'Unknown', CLP_EC );
				}
			}else{
				_e( 'Undecided', CLP_EC );
			}

			echo '</br>';

			$status = $order->get_display_status();
			if( $order->get_status() == APPTHEMES_ORDER_PENDING ){
				echo '<strong>' . ucfirst( $status ) . '</strong>';
			}else{
				echo ucfirst( $status );
			}

			break;

		case 'status':
			echo ucfirst( $order->get_status() );
			break;

		case 'order_date':
			$order_post = get_post( $order->get_ID() );
			if ( '0000-00-00 00:00:00' == $order_post->post_date ) {
				$t_time = $h_time = __( 'Unpublished', CLP_EC );
				$time_diff = 0;
			} else {
				$t_time = get_the_time( _x( 'Y/m/d g:i:s A', 'Order Date Format', CLP_EC ) );
				$m_time = $order_post->post_date;
				$time = get_post_time( 'G', true, $order_post );

				$time_diff = time() - $time;

				if ( $time_diff > 0 && $time_diff < 24*60*60 )
					$h_time = sprintf( __( '%s ago', CLP_EC ), human_time_diff( $time ) );
				else
					$h_time = mysql2date( _x( 'Y/m/d', 'Order Date Format', CLP_EC ), $m_time );
			}
			echo '<abbr title="' . $t_time . '">' . $h_time . '</abbr>';

			break;
	}

}
add_action( 'manage_' . APPTHEMES_ORDER_PTYPE . '_posts_custom_column', 'awsolutions_order_add_column_data', 10, 2 );


// Displays the order summary table.
function awsolutions_display_order_summary_table() {
	global $post, $wpdb;

	if ( APPTHEMES_ORDER_PTYPE !== $post->post_type )
		return;

	if ( isset( $_GET['post'] ) ) $post_id = $_GET['post'];
	$order = appthemes_get_order( $post_id );

	?>
	<style type="text/css">
		#admin-order-summary tbody td{
			padding-top: 10px;
			padding-bottom: 10px;
		}
		#admin-order-summary{
			margin-bottom: 20px;
		}
		#normal-sortables, #post-body-content{
			display: none;
		}
	</style>

	<table class="widefat" id="admin-order-summary">
		<thead>
			<tr>
			<th><?php _e( 'Product', CLP_EC ); ?></th>
			<th><?php _e( 'Price', CLP_EC ); ?></th>
			<th><?php _e( 'Quantity', CLP_EC ); ?></th>
			<th><?php _e( 'Affects', CLP_EC ); ?></th>
			</tr>
		</thead>

		<tbody>

			<?php
			$items = array_reverse($order->get_items());
			$table_trans = $wpdb->prefix . "clp_transactions";
			foreach ( $items as $item ) {
			$post_ID = $item['post_id'];
			$row = $wpdb->get_row( "SELECT * FROM {$table_trans} WHERE order_id = '$post_id' AND post_id = '$post_ID'" );

			$ptype_obj = get_post_type_object( $item['post']->post_type );
			if ( $item['type'] == "product" )
			$item_link = ( $ptype_obj->public ) ? html_link( get_permalink( $item['post_id'] ), $item['post']->post_title ) : '';
			else $item_link = ( $ptype_obj->public ) ? html_link( get_permalink( $item['post_id'] ), __( 'Transaction', CLP_EC ) ) : '';
			if ( $item['type'] == "product" ) $count = ''.$row->quantity.' '.__( 'items', CLP_EC ).'';
			else $count = '1 '.__( 'items', CLP_EC ).'';

			echo '<tr>';
			if ( $item['type'] == "product" ) {
			echo '<td>'.$row->item_name.'</td>';
			} else {
			echo '<td>'.APP_Item_Registry::get_title( $item['type'] ).'</td>';
			}
			echo '<td>'.appthemes_get_price( $item['price'], $order->currency ).'</td>';
			echo '<td>'.$count.'</td>';
			echo '<td>'.$item_link.'</td>';
			echo '</tr>';
			}
			?>

		</tbody>

		<tfoot>
			<tr>
			<th><?php _e( 'Total', CLP_EC ); ?></th>
			<th><?php echo appthemes_get_price( $order->get_total(), $order->currency ); ?></th>
			<th></th>
			<th></th></tr>
		</tfoot>
	</table>

<?php
}
add_action( 'edit_form_advanced', 'awsolutions_display_order_summary_table' );


// Generates the shopping options / buttons for the single ads listings
function awsolutions_purchase_buttons() {
	global $post, $current_user, $clp_options, $cp_options, $wpdb;

	$enabled = get_post_meta( $post->ID, 'cp_enable_payment', true );
	$available = get_post_meta( $post->ID, 'cp_quantity', true );
	$unlimited = get_post_meta( $post->ID, 'cp_quantity_unlimited', true );

	if ( ( is_single() && $enabled ) && ( $unlimited == "yes" || $available > 0 ) ) {

	$table_trans = $wpdb->prefix . "clp_transactions";
	$row = $wpdb->get_row("SELECT * FROM {$table_trans} WHERE post_id = '$post->ID' AND user_id = '$current_user->ID' AND trans_type = 'cart' AND paid_date = ''");
	if ( $row ) $reg_quantity = $row->quantity;
	else $reg_quantity = 0;

	if ( $row->order_id > 0 && $row->status !== "paid" ) {

		$url = '<a style="text-decoration: none;" href="'.CP_DASHBOARD_URL.'">'.__( 'click here', CLP_EC ).'</a>';

		echo '<div class="clr"></div>';
		echo '<p class="text-center">'.sprintf(__( '<strong>You have already purchased this product, %s to pay for your purchase!</strong>', CLP_EC ), $url).'</p>';
		echo '<div class="clr"></div>';

		return;

	}

	$item_name = get_post_meta( $post->ID, 'cp_item_name', true );
	if ( empty( $item_name ) ) $item_name = $post->post_title;
?>

	<?php if ( $clp_options['cart_options'] == "simple" ) { ?>
	<input id="mode" type="hidden" value="simple">
	<?php } else if ( $clp_options['cart_options'] == "sidebar" ) { ?>
	<input id="mode" type="hidden" value="sidebar">
	<?php } ?>

	<?php if ( ! $row ) { ?>
	<input id="new-item" type="hidden" value="true">
	<?php
	}

	$orders = $wpdb->get_results("SELECT * FROM {$table_trans} WHERE user_id = '$current_user->ID' AND trans_type = 'cart' AND paid_date = ''");

	$result = false;
	foreach ( $orders as $res ) {
	$img_url = awsolutions_image_url( $res->post_id );
	$ad_url = get_permalink( $res->post_id );
	$price = cp_display_price( $res->price, 'ad', false );
	$item_title = $res->item_name;
	if ( empty( $item_title ) ) $item_title = $res->post_title;
	$result .= "<div class='cart-item'><a href='$ad_url'>$item_title</a><br /><div class='quantity_$res->post_id'>$res->quantity x $price</div>$img_url</div><div class='dotted'></div>";
	}

?>

	<input id="result" type="hidden" value="<?php echo $result; ?>">
	<input id="reg-quantity" type="hidden" value="<?php echo $reg_quantity; ?>">
	<?php wp_nonce_field( 'ajax-cart-nonce-'.$post->ID.'', 'security_'.$post->ID.'' ); ?>

	<div class="clr"></div>

<div class="not-responsive">

	<?php if ( $clp_options['options_style'] == "block" ) { ?>

	<div class="padd10"></div><div class="loader_<?php echo $post->ID; ?>"></div>
	<?php if ( ( $unlimited == "yes" || $available > 1 ) && ( $clp_options['buttons'] != "direct" ) ) { ?>
	<input id="quantity_<?php echo $post->ID; ?>" class="cart-quantity" type="text" value="1">
	<?php } else if ( ( $available == 1 ) || ( $unlimited == "yes" || $available > 0 && $clp_options['buttons'] == "direct" ) ) { ?>
	<input id="quantity_<?php echo $post->ID; ?>" type="hidden" value="1">
	<?php } ?>
	<div class="clr"></div>
	<?php if ( $clp_options['buttons'] == "cart" ) { ?>
	<button class="add-cart clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Add to Cart', CLP_EC ); ?></button>
	<?php } else if ( $clp_options['buttons'] == "direct" ) { ?>
	<button class="direct-order clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Buy it Now', CLP_EC ); ?></button>
	<?php } else if ( $clp_options['buttons'] == "both" ) { ?>
	<button class="direct-order clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Buy it Now', CLP_EC ); ?></button>
	<div class="clr"></div>
	<button class="add-cart clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Add to Cart', CLP_EC ); ?></button>
	<?php } ?>

	<?php } else if ( $clp_options['options_style'] == "row" ) { ?>

	<div class="padd10"></div>

	<table border="0" cellpadding="4" cellspacing="1" class="tblwide">
		<thead>
			<tr>
				<th width="5px" data-class="expand">&nbsp;</th>
				<th class="text-left">&nbsp;<?php _e( 'Product', CLP_EC ); ?></th>
				<th width="100px"><?php _e( 'Seller', CLP_EC ); ?></th>
				<th width="60px"><?php _e( 'Quantity', CLP_EC ); ?></th>
				<?php if ( $clp_options['buttons'] == "both" ) { ?>
				<th width="260px"><div style="text-align: center;"><?php _e( 'Options', CLP_EC ); ?></div></th>
				<?php } else { ?>
				<th width="150px"><div style="text-align: center;"><?php _e( 'Options', CLP_EC ); ?></div></th>
				<?php } ?>
			</tr>
		</thead>
	<tbody>

	<tr class="even">
	<td></td>
	<td><strong><?php echo $item_name; ?></strong></td>
	<td class="text-center"><a href="<?php echo get_author_posts_url( $post->post_author ); ?>"><?php echo get_the_author_meta( 'display_name', $post->post_author ); ?></a></td>
	<td class="text-center"><?php if ( ( $unlimited == "yes" || $available > 1 ) && ( $clp_options['buttons'] != "direct" ) ) { ?>
	<input id="quantity_<?php echo $post->ID; ?>" class="cart-quantity" type="text" value="1">
	<?php } else if ( ( $available == 1 ) || ( $unlimited == "yes" || $available > 0 && $clp_options['buttons'] == "direct" ) ) { ?>
	<input id="quantity_<?php echo $post->ID; ?>" type="hidden" value="1">1
	<?php } ?></td>
	<td class="text-center"><div class="loader_<?php echo $post->ID; ?>"></div>
	<?php if ( $clp_options['buttons'] == "cart" ) { ?>
	<button class="add-cart clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Add to Cart', CLP_EC ); ?></button>
	<?php } else if ( $clp_options['buttons'] == "direct" ) { ?>
	<button class="direct-order clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Buy it Now', CLP_EC ); ?></button>
	<?php } else if ( $clp_options['buttons'] == "both" ) { ?>
	<button class="direct-order clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Buy it Now', CLP_EC ); ?></button>
	<button class="add-cart clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Add to Cart', CLP_EC ); ?></button>
	<?php } ?></td>
	</tr>

	</tbody>

	</table>

	<?php } ?>

</div><!-- /not-responsive -->

<div class="clp-responsive">

	<div class="padd10"></div><div class="loader_<?php echo $post->ID; ?>"></div>
	<?php if ( ( $unlimited == "yes" || $available > 1 ) && ( $clp_options['buttons'] != "direct" ) ) { ?>
	<input id="quantity_<?php echo $post->ID; ?>" class="cart-quantity" type="text" value="1">
	<?php } else if ( ( $available == 1 ) || ( $unlimited == "yes" || $available > 0 && $clp_options['buttons'] == "direct" ) ) { ?>
	<input id="quantity_<?php echo $post->ID; ?>" type="hidden" value="1">
	<?php } ?>
	<div class="clr"></div>
	<?php if ( $clp_options['buttons'] == "cart" ) { ?>
	<button class="add-cart clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Add to Cart', CLP_EC ); ?></button>
	<?php } else if ( $clp_options['buttons'] == "direct" ) { ?>
	<button class="direct-order clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Buy it Now', CLP_EC ); ?></button>
	<?php } else if ( $clp_options['buttons'] == "both" ) { ?>
	<button class="direct-order clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Buy it Now', CLP_EC ); ?></button>
	<div class="clr"></div>
	<button class="add-cart clp-btn cart-btn" id="<?php echo $post->ID; ?>"><?php _e( 'Add to Cart', CLP_EC ); ?></button>
	<?php } ?>

</div><!-- /clp-responsive -->

	<div class="padd10"></div>

	<div class="clr"></div>

	<?php } ?>

<?php
}
if ( $clp_options['buttons'] != "" && $clp_options['buttons_placement'] == "below" )
add_action( 'appthemes_after_post_content', 'awsolutions_purchase_buttons' );
if ( $clp_options['buttons'] != "" && $clp_options['buttons_placement'] == "above" )
add_action( 'appthemes_before_post_content', 'awsolutions_purchase_buttons' );