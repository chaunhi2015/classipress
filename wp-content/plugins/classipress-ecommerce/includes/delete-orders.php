<?php

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function awsolutions_delete_test_orders() {
	global $wpdb;

	$table_p2p = $wpdb->prefix . "p2p";
	$table_p2pmeta = $wpdb->prefix . "p2pmeta";
	$orders = $wpdb->get_results("SELECT * FROM {$table_p2p}");

	if ( $orders ) {
	foreach ( $orders as $res ) {

		wp_delete_post( $res->p2p_from, true );

	}
		$wpdb->query("TRUNCATE TABLE {$table_p2p}");
		$wpdb->query("TRUNCATE TABLE {$table_p2pmeta}");
	}

	$table_posts = $wpdb->prefix . "posts";
	$pids = $wpdb->get_results("SELECT * FROM {$table_posts} WHERE post_type = 'transaction'");

	if ( $pids ) {
	foreach ( $pids as $pid ) {

		wp_delete_post( $pid->ID, true );

	}
	}
}
add_action( 'wp_ajax_delete_test_orders', 'awsolutions_delete_test_orders' );