<?php

/**
 * Define the general settings
 */

add_filter( 'aws_register_settings', 'clp_general' );

function clp_general() {

    // General Settings section
    $general_settings[] = array(
        'section_id' => 'main',
        'section_title' => __( 'General Settings', CLP_EC ),
        'section_description' => __( 'For ClassiPress eCommerce to function optimally, the following custom fields are made available for the kind of form layout you are using, <strong>cp_enable_payment</strong>, <strong>cp_item_name</strong> and <strong>cp_quantity</strong>. ClassiPress eCommerce supports the <a style="text-decoration: none;" href="http://marketplace.appthemes.com/plugins/invoices/?aid=20153" target="_blank">AppThemes Invoices</a> plugin and <a style="text-decoration: none;" href="http://marketplace.appthemes.com/plugins/payment-gateways/skrill/?aid=20153" target="_blank">Skrill Payment Gateway</a> plugin, so please read the <strong>Important Information</strong> section of the readme file before you install any of these plugins, OR attempt to update these or the ClassiPress Theme.<br /><br /><strong>Plugin Version:</strong> '.CLP_EC_PLUGIN_VERSION.'.<br /><strong>DB Version:</strong> '.CLP_EC_DB_VERSION.'.<br /><br />', CLP_EC ),
        'section_order' => 5,
        'fields' => array(
            array(
                'id' => 'buttons',
                'title' => __( 'Enable Shopping Options:', CLP_EC ),
                'desc' => __( 'Select the shopping options / buttons you want to make available on the single ad listing.', CLP_EC ),
                'type' => 'radio',
                'std' => '',
                'choices' => array(
                    '' => __( 'Disabled.', CLP_EC ),
                    'direct' => __( 'Buy it Now.', CLP_EC ),
                    'cart' => __( 'Add to Cart.', CLP_EC ),
                    'both' => __( 'Both Options.', CLP_EC ),
                )
            ),
            array(
                'id' => 'gateways',
                'title' => __( 'Gateways:', CLP_EC ),
                'desc' => __( 'Select the payment gateways to use with user to user payments. Gateway must be activated in ClassiPress payment settings.', CLP_EC ),
                'type' => 'checkboxes',
                'std' => array(
                    'paypal'
                ),
                'choices' => array(
                    'bank_transfer' => __( 'Bank Transfer.', CLP_EC ),
                    'paypal' => 'PayPal.',
                    'skrill' => 'Skrill. <a style="text-decoration: none;" href="http://marketplace.appthemes.com/plugins/payment-gateways/skrill/?aid=20153" target="_blank">Get Skrill from the marketplace</a>.',
                    'simplepay4u' => 'SimplePay4u'
                )
            ),
            array(
                'id' => 'buttons_placement',
                'title' => __( 'Button Placement:', CLP_EC ),
                'desc' => __( 'Select the placement of the shopping options / buttons.', CLP_EC ),
                'type' => 'radio',
                'std' => 'below',
                'choices' => array(
                    'above' => __( 'Above Ad Description.', CLP_EC ),
                    'below' => __( 'Below Ad Description.', CLP_EC ),
                )
            ),
            array(
                'id' => 'options_style',
                'title' => __( 'Options Style:', CLP_EC ),
                'desc' => __( 'Select the style of the shopping options / buttons.', CLP_EC ),
                'type' => 'radio',
                'std' => 'row',
                'choices' => array(
                    'row' => __( 'Shown As a Row.', CLP_EC ),
                    'block' => __( 'Shown As a Block.', CLP_EC ),
                )
            ),
            array(
                'id' => 'cart_options',
                'title' => __( 'Add to Cart Mode:', CLP_EC ),
                'desc' => __( 'Select the "Add to Cart" success display setting that works best for your theme / child theme.', CLP_EC ),
                'type' => 'radio',
                'std' => 'simple',
                'choices' => array(
                    'simple' => __( 'Displays a simple notice, "Itemname was just added to your cart."', CLP_EC ),
                    'sidebar' => __( 'Displays the above notice and a cart summary widget.', CLP_EC ),
                    '' => __( 'Displays the cart summary in an lightbox popup.', CLP_EC ),
                )
            ),
            array(
                'id' => 'cart_link',
                'title' => __( 'Cart Link:', CLP_EC ),
                'desc' => __( 'Add a link to the shopping cart which includes an item counter, ie. Cart (0).', CLP_EC ),
                'type' => 'checkboxes',
                'std' => array(
                    'header',
                    'primary'
                ),
                'choices' => array(
                    'header' => 'Place the link in the welcome / login bar.',
                    'primary' => 'Place the link in the primary menu.'
                )
            ),
            array(
                'id' => 'link_options',
                'title' => __( 'Link in Pop Up:', CLP_EC ),
                'desc' => __( 'Select the destination for the Continue Shopping link / button in cart overview popup.', CLP_EC ),
                'type' => 'radio',
                'std' => 'seller',
                'choices' => array(
                    '' => __( 'Disabled, no link.', CLP_EC ),
                    'seller' => __( 'Sellers ad listings.', CLP_EC ),
                    'home' => __( 'Frontpage.', CLP_EC ),
                )
            ),
            array(
                'id' => 'message',
                'title' => __( 'Info in Dashboard:', CLP_EC ),
                'desc' => __( 'Enter any information you want to share with your customers. The information entered will be displayed in the settings tab of frontend dashboard.', CLP_EC ),
                'type' => 'editor',
                'std' => 'Before you start selling on <strong>SITENAME</strong>, setup your preferred payment gateway, once you have your payment gateway account information setup and/or enabled in your ads, buyers have the option of paying you online.'
            ),
            array(
                'id' => 'delete_data',
                'title' => __( 'Delete Data:', CLP_EC ),
                'desc' => __( 'Enable this option to delete ALL data upon plugin deletion.', CLP_EC ),
                'type' => 'select',
                'std' => '',
                'choices' => array(
                    '' => __( 'No', CLP_EC ),
                    'yes' => __( 'Yes', CLP_EC )
                )
            )
        )
    );

    return $general_settings;
}