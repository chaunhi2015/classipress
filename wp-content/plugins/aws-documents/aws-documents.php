<?php

/*
Plugin Name: AWS Documents
Plugin URI: http://www.arctic-websolutions.com/
Description: The very best and most convenient way to add document downloads to ClassiPress & CP Auction.
Version: 1.0.1
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com/
*/

/*
Copyright 2013 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! defined( 'AWS_DOC_VERSION' ) ) {
	define( 'AWS_DOC_VERSION', '1.0.1' );
}

if ( ! defined( 'AWS_DOC_LATEST_RELEASE' ) ) {
	define( 'AWS_DOC_LATEST_RELEASE', '10 July 2015' );
}

if ( ! defined( 'AWS_DOC_PLUGIN_FILE' ) ) {
	define( 'AWS_DOC_PLUGIN_FILE', __FILE__ );
}

/*
|--------------------------------------------------------------------------
| INTERNATIONALIZATION - EASY TRANSLATION USING CODESTYLING LOCALIZATION
|--------------------------------------------------------------------------
*/

function aws_doc_textdomain() {
	load_plugin_textdomain( 'AwsDOC', false, dirname( plugin_basename( AWS_DOC_PLUGIN_FILE ) ) . '/languages/' );
}
add_action( 'init', 'aws_doc_textdomain' );


/*
|--------------------------------------------------------------------------
| ADD LINK TO SETTINGS MENU PAGE
|--------------------------------------------------------------------------
*/

function aws_doc_add_link() {

	add_menu_page( __( 'AWS Documents', 'AwsDOC' ), __( 'AWS Documents', 'AwsDOC' ), 'manage_options', 'aws-documents', 'aws_documents_settings', plugins_url( 'aws-documents/css/images/embed16.png' ), '6,5' );

}
add_action( 'admin_menu', 'aws_doc_add_link' );


/*
|--------------------------------------------------------------------------
| ADD SETTINGS LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'aws_doc_action_links' );
function aws_doc_action_links( $links ) {

return array_merge(
array(
'settings' => '<a href="' . get_bloginfo( 'url' ) . '/wp-admin/admin.php?page=aws-documents">'.__( 'Settings', 'AwsDOC' ).'</a>'
),
$links
);
}


/*
|--------------------------------------------------------------------------
| ADD SUPPORT LINK TO PLUGIN PAGE
|--------------------------------------------------------------------------
*/

add_filter( 'plugin_row_meta', 'aws_doc_meta_links', 10, 2 );
function aws_doc_meta_links( $links, $file ) {

$plugin = plugin_basename(__FILE__);

// create link
if ( $file == $plugin ) {
return array_merge(
$links,
array( '<a href="http://www.arctic-websolutions.com/forums/" target="_blank">'.__( 'Support', 'AwsDOC' ).'</a>' )
);
}
return $links;
}


/*
|--------------------------------------------------------------------------
| REGISTER ADMIN BACKEND / FRONTEND SCRIPTS AND STYLESHEETS
|--------------------------------------------------------------------------
*/

function aws_doc_admin_scripts() {

	wp_enqueue_style( 'aws-doc-style', plugins_url( 'aws-documents/css/admin.css'), false, '1.0.0' );

}
if ( is_admin() ) {
	add_action( 'admin_print_styles', 'aws_doc_admin_scripts' );
}

function aws_doc_frontend_scripts() {

	wp_enqueue_style( 'aws-doc-style', plugins_url( 'aws-documents/css/style.css'), false, '1.0.0' );

}
if ( !is_admin() ) {
	add_action('wp_head','aws_doc_frontend_scripts');
}


/*
|--------------------------------------------------------------------------
| INCLUDES
|--------------------------------------------------------------------------
*/

include_once('includes/functions.php');


/*
|--------------------------------------------------------------------------
| AWS DOCUMENTS SETTINGS
|--------------------------------------------------------------------------
*/

function aws_documents_settings() {

	global $wpdb;

	$msg = "";
	$tool_tip = "";

	if(isset($_POST['save_settings'])) {

	$tooltip = isset( $_POST['aws_doc_tooltip'] ) ? esc_attr( $_POST['aws_doc_tooltip'] ) : '';
	$enable_documents = isset( $_POST['aws_doc_enable_documents'] ) ? esc_attr( $_POST['aws_doc_enable_documents'] ) : '';
	$classified = isset( $_POST['classified'] ) ? esc_attr( $_POST['classified'] ) : '';
	$wanted = isset( $_POST['wanted'] ) ? esc_attr( $_POST['wanted'] ) : '';
	$normal = isset( $_POST['normal'] ) ? esc_attr( $_POST['normal'] ) : '';
	$reverse = isset( $_POST['reverse'] ) ? esc_attr( $_POST['reverse'] ) : '';
	$use_hooks = isset( $_POST['aws_doc_use_hooks'] ) ? esc_attr( $_POST['aws_doc_use_hooks'] ) : '';
	$label = isset( $_POST['aws_doc_button_text'] ) ? esc_attr( $_POST['aws_doc_button_text'] ) : '';
	$float = isset( $_POST['aws_doc_button_float'] ) ? esc_attr( $_POST['aws_doc_button_float'] ) : '';

	$option = get_option('cp_auction_fields');
	$list = ''.$classified.','.$wanted.','.$normal.','.$reverse.'';
	$array = explode( ',', $list );
	$new_list = implode(",", $array);
	$field_ads = trim($new_list, ',');

	if( $tooltip ) {
	update_option('aws_doc_tooltip',$tooltip);
	} else {
	delete_option('aws_doc_tooltip');
	}
	if( $enable_documents == "yes" ) {
	global $wpdb;
	$created = date('Y-m-d h:i:s');
	$table_name = $wpdb->prefix . "cp_ad_fields";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = 'cp_documents_url'");
	if( $res )
	$field_owner = $res->field_owner;

	if( !$res ) {
	$query = "INSERT INTO {$table_name} (field_name, field_label, field_desc, field_type, field_tooltip, field_perm, field_core, field_req, field_owner, field_created, field_modified) VALUES (%s, %s, %s, %s, %s, %d, %d, %d, %s, %s, %s)";
	$wpdb->query($wpdb->prepare($query, 'cp_documents_url', ''.__('Documents URL','AwsDOC').'', ''.__('This is the download documents field for all ad types. It is a core AWS Documents field and should not be deleted unless it is deactivated in settings.','AwsDOC').'', 'text box', $tooltip, '0', '1', '0', 'AWS Documents', $created, $created));
	if( ! $option ) $option = array();
	$option['cp_documents_url'] = $field_ads;
	if ( ! empty( $option ) )
   	update_option('cp_auction_fields', $option);
	} elseif( $res && $field_owner == "AWS Documents" ) {
	$where_array = array('field_name' => 'cp_documents_url', 'field_owner' => 'AWS Documents');
	$wpdb->update($table_name, array('field_tooltip' => $tooltip, 'field_modified' => $created), $where_array);
	$option['cp_documents_url'] = $field_ads;
	if ( ! empty( $option ) )
   	update_option('cp_auction_fields', $option);
	}
	update_option('aws_doc_enable_documents',$enable_documents);
	} else {
	global $wpdb;
	$table_name = $wpdb->prefix . "cp_ad_fields";
	$wpdb->query( 
	$wpdb->prepare("DELETE FROM {$table_name} WHERE field_name = '%s'", 'cp_documents_url'));
	delete_option('aws_doc_enable_documents');
	}
	if( $use_hooks == "yes" ) {
	update_option('aws_doc_use_hooks',$use_hooks);
	} else {
	delete_option('aws_doc_use_hooks');
	}
	if( $label ) {
	update_option('aws_doc_button_text',$label);
	} else {
	delete_option('aws_doc_button_text');
	}
	if( $float ) {
	update_option('aws_doc_button_float',$float);
	} else {
	delete_option('aws_doc_button_float');
	}

	$msg = '<div id="setting-error-settings_updated" class="updated settings-error"><p><strong>'.__('Settings was saved!','AwsDOC').'</strong></p></div>';
}

	$option = get_option('cp_auction_fields');
	$table_name = $wpdb->prefix . "cp_ad_fields";
	$res = $wpdb->get_row("SELECT * FROM {$table_name} WHERE field_name = 'cp_documents_url'");
	if( $res ) {
	$tool_tip = $res->field_tooltip;
	}
	if( isset( $option['cp_documents_url'] ) )
	$list = explode(',', $option['cp_documents_url']);
	else $list = false;

?>

	<div class="wrap">
		<?php screen_icon( 'aws-documents' ); ?>
		<h2><?php _e( 'AWS Documents', 'AwsDOC' ); ?></h2>

	<?php if($msg) echo $msg; ?>

	<div class="metabox-holder has-right-sidebar">

	<div id="post-body">
	<div id="post-body-content">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('AWS Documents Settings', 'AwsDOC'); ?></h3>
            <div class="inside">

<form method="post" action="">

    <table class="form-table">
    <tbody>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Enable this option if you want to allow users to display an documents download button in their ads, google drive, dropbox or other provider urls.', 'AwsDOC'); ?>"><div class="helpico"></div></a><?php _e('Enable AWS Documents:', 'AwsDOC'); ?></th>
    <td><select name="aws_doc_enable_documents">
    <option value="" <?php if(get_option('aws_doc_enable_documents') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'AwsDOC'); ?></option>
    <option value="yes" <?php if(get_option('aws_doc_enable_documents') == "yes") echo 'selected="selected"'; ?>><?php _e('Enabled', 'AwsDOC'); ?></option>
    </select> <?php _e('Allow author to display an download documents button.', 'AwsDOC'); ?><br /><small><?php _e('Activation of the download documents function will create a custom field named Documents URL, this field will appear on the submit ad page.', 'AwsDOC'); ?></small></td>
    </tr>

	<?php if ( get_option( 'cp_auction_installed' ) == "yes" ) { ?>

	<tr valign="top">
	<th scope="row"><a href="#" title="<?php _e('Select the type of ads where the download documents button should be available.', 'AwsDOC'); ?>"><div class="helpico"></div></a><?php _e('Ad Types:', 'AwsDOC'); ?></th>

        <td><input type="checkbox" name="classified" value="classified" <?php if( $list && in_array("classified", $list)) echo 'checked="checked"'; ?>> <?php _e('Classified Ads', 'AwsDOC'); ?><br/>
   <input type="checkbox" name="wanted" value="wanted" <?php if( $list && in_array("wanted", $list)) echo 'checked="checked"'; ?>> <?php _e('Wanted Ads', 'AwsDOC'); ?><br/>
   <input type="checkbox" name="normal" value="normal" <?php if( $list && in_array("normal", $list)) echo 'checked="checked"'; ?>> <?php _e('Normal Auction Listings', 'AwsDOC'); ?><br/>
   <input type="checkbox" name="reverse" value="reverse" <?php if( $list && in_array("reverse", $list)) echo 'checked="checked"'; ?>> <?php _e('Reverse Auction Listings', 'AwsDOC'); ?>

	<div class="clear20"></div>
	</td>
    </tr>

	<?php } ?>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('This will create a ? tooltip icon next to the Documents URL field on the submit ad page.', 'AwsDOC'); ?>"><div class="helpico"></div></a><?php _e('Field Tooltip:', 'AwsDOC'); ?></th>
    <td><textarea class="options" name="aws_doc_tooltip" rows="4" cols="60"><?php echo $tool_tip; ?></textarea><br><small><?php _e('Now it’s up to you to explain this to your customers using the field tooltip, which will create a ? tooltip icon next to the Documents URL field on the submit ad page. NO use of HTML.', 'AwsDOC'); ?></small></td>
    </tr>

        <tr valign="top">
        <th scope="row"><a href="#" title="<?php _e('Enable this option if you want to move the document download link to another area of the single ad listing.', 'AwsDOC'); ?>"><div class="helpico"></div></a><?php _e('Enable Hooks', 'AwsDOC'); ?></th>
    <td><select name="aws_doc_use_hooks">
    <option value="" <?php if(get_option('aws_doc_use_hooks') == "") echo 'selected="selected"'; ?>><?php _e('Disabled', 'AwsDOC'); ?></option>
    <option value="yes" <?php if(get_option('aws_doc_use_hooks') == "yes") echo 'selected="selected"'; ?>><?php _e('Enabled', 'AwsDOC'); ?></option>
    </select> <?php _e('Use the hooks provided.', 'AwsDOC'); ?><br /><small><?php _e('Some child themes have placed the google maps just below the ad description, this may mean that the download button is placed at the very bottom of the ad. To make use of another placement put the below hooks where you want the the download button to appear in the single ad listing.', 'AwsDOC'); ?></small>

	<div class="code_box">
	&lt;?php if ( function_exists('aws_doc_download_documents') ) aws_doc_download_documents(); ?&gt;
	</div>

    </td>
    </tr>

	<tr valign="top">
	<th scope="row" valign="top"><?php _e('Button Text:', 'AwsDOC'); ?></th>
	<td><input type="text" name="aws_doc_button_text" value="<?php echo get_option('aws_doc_button_text'); ?>"> <?php _e('Enter a text / title for the download button.', 'AwsDOC'); ?><br /><small><?php _e('Enter a text / title for the download button, ie. Download Documents.', 'AwsDOC'); ?></small></td>
	</tr>

	<tr valign="top">	
	<th scope="row" valign="top"><?php _e('Button Float:', 'AwsDOC'); ?></th>
	<td><select name="aws_doc_button_float">
	<option value="" <?php if(get_option('aws_doc_button_float') == "") echo 'selected="selected"'; ?>><?php _e('Right', 'AwsDOC'); ?></option>
	<option value="left" <?php if(get_option('aws_doc_button_float') == "left") echo 'selected="selected"'; ?>><?php _e('Left', 'AwsDOC'); ?></option>
	<option value="center" <?php if(get_option('aws_doc_button_float') == "center") echo 'selected="selected"'; ?>><?php _e('Center', 'AwsDOC'); ?></option>
	</select> <?php _e('Position the button, float center, left or right.', 'AwsDOC'); ?></td>
	</tr>

    </tbody>
    </table>

    <p class="submit">
    <input type="submit" name="save_settings" class="button-primary" value="<?php _e('Save Settings', 'AwsDOC'); ?>" />
    </p>
    </form>

           </div>
         </div>
       </div>
     </div>

    	<div class="inner-sidebar">

        <div class="postbox">
            <h3 style="cursor:default;"><?php _e('Information', 'AwsDOC'); ?></h3>
            <div class="inside">

<table class="form-table">
    <tbody>
        <tr valign="top">
        <th scope="row"><?php _e('Version:', 'AwsDOC'); ?></th>
    <td><a class="button-secondary" href="http://cpauction.wp-build.net/wp-content/plugins/aws-documents/change-log.txt" onclick="javascript:void window.open('http://cpauction.wp-build.net/wp-content/plugins/aws-documents/change-log.txt','1346613040193','width=720,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=200,top=100');return false;"><?php _e('Check for updates', 'AwsDOC'); ?></a></td>
    </tr>
        <tr valign="top">
        <th scope="row"><?php _e('Support:', 'AwsDOC'); ?></th>
    <td><a class="button-secondary" href="http://www.arctic-websolutions.com/forums/" target="_blank"><?php _e('Join the Forums', 'AwsDOC'); ?></a></td>
    </tr>
    </tbody>
    </table>

<div class="clear15"></div>

<?php _e('<a style="text-decoration:none" href="http://www.arctic-websolutions.com" target="_blank"><strong>Arctic WebSolutions</strong></a> offers WordPress and ClassiPress plugins “as is” and with no implied meaning that they will function exactly as you would like or will be compatible with all 3rd party components and plugins. We do not offer support via email or otherwise support WordPress or other WordPress plugins we have not developed.', 'AwsDOC'); ?>

<div class="clear15"></div>

<div class="admin_header"><?php _e( 'Other Plugins', 'AwsDOC' ); ?></div>
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/bundle-pack/" target="_blank">Bundle Pack $317.00 off</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/cp-auction-social-sharing/" target="_blank">CP Auction & eCommerce</a>', 'clpOffer' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-grid-view/" target="_blank">ClassiPress Grid View</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-projects-portfolio/" target="_blank">AWS Projects Portfolio</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-affiliate-plugin/" target="_blank">AWS Affiliate Plugin</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-ecommerce/" target="_blank">ClassiPress eCommerce</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/simplepay4u-gateway/" target="_blank">SimplePay4u Gateway</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/grandchild-plugin/" target="_blank">AWS Grandchild Plugin</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-theme-customizer/" target="_blank">AWS Theme Customizer</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/best-offer-for-cp-auction/" target="_blank">CP Auction Best Offer</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-best-offer/" target="_blank">ClassiPress Best Offer</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-social-sharing/" target="_blank">ClassiPress Social Sharing</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/classipress-cartpauj-pm/" target="_blank">ClassiPress Cartpauj PM</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-deny-access/" target="_blank">AWS Deny Access</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-user-roles/" target="_blank">AWS User Roles</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-embed-video/" target="_blank">AWS Embed Video</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://www.arctic-websolutions.com/ads/aws-credit-payments/" target="_blank">AWS Credit Payments</a>', 'AwsDOC' ); ?><br />
 - <?php _e( '<a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">AppThemes Critic Reviews</a>', 'AwsDOC' ); ?>

<div class="clear15"></div>

<?php _e('AppThemes has released a plugin <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> which has the same rating and review functionality as you can see around in their marketplace. The only minus is that it is not modded / available for ClassiPress Theme <strong>BUT</strong>, I have a modified version which you can review on our main site or any of our child theme demos.', 'AwsDOC'); ?>

<div class="clear15"></div>

<?php _e('Purchase the <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">Critic Reviews</a> plugin through my <a style="text-decoration:none" href="http://marketplace.appthemes.com/plugins/critic/?aid=20153" target="_blank">affiliate link</a>, send me a copy of your purchase receipt and i will send you my modified version of the Critic Reviews plugin.', 'AwsDOC'); ?>

	<div class="clear15"></div>

	<p><strong><?php _e('Was this plugin useful? If you like it and it is/was useful, please consider donating.. Many thanks!', 'AwsDOC'); ?></strong></p>

	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="RFJB58JUE8WUJ">
	<p style="text-align: center;">
	<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
	</p>
	<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
	</form>

            </div>
          </div>
        </div>
</div>
</div>

<?php
}
?>