<?php

/*
Copyright 2013 Rune Kristoffersen - Arctic WebSolutions (email: post@arctic-websolutions.no)
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


/*
|--------------------------------------------------------------------------
| HOOKS USED FOR DOWNLOAD BUTTON PLACEMENT IN SINGLE ADS PAGE
|--------------------------------------------------------------------------
*/

function aws_doc_download_documents() {
	do_action( 'aws_doc_download_documents' );
}


/*
|--------------------------------------------------------------------------
| ADD THE DOCUMENTS DOCUMENTS DOWNLOAD BUTTON
|--------------------------------------------------------------------------
*/

function aws_doc_document_downloads() {
	global $post;
	if( is_single() || is_singular( APP_POST_TYPE ) ) {

	$documents_url = get_post_meta($post->ID, 'cp_documents_url', true);
	$label = get_option('aws_doc_button_text');
	if ( empty( $label ) ) $label = __('Download Documents', 'AwsDOC');
	$float = get_option('aws_doc_button_float');

	if( $documents_url ) { ?>

	    <div class="clear5"></div>
	    <?php if( $float == "left" ) { ?><div class="text-left"><?php } else if( $float == "center" ) { ?><div class="text-center"><?php } else { ?><div class="text-right"><?php } ?><a href="<?php echo $documents_url; ?>" target="_blank" class="mbtn btn_orange" type="button"><?php echo $label; ?></a></div>
	    <div class="clear10"></div>

<?php
	    }

}
}
if( get_option('aws_doc_use_hooks') !== "yes" && get_option('aws_doc_enable_documents') == "yes" )
add_action( 'appthemes_after_post_content', 'aws_doc_document_downloads' );
else if( get_option('aws_doc_use_hooks') == "yes" && get_option('aws_doc_enable_documents') == "yes" )
add_action( 'aws_doc_download_documents', 'aws_doc_document_downloads' );


/*
|--------------------------------------------------------------------------
| REMOVE LABEL AND VALUES FROM AD DETAILS
|--------------------------------------------------------------------------
*/

function aws_doc_remove_documents_details() {
	return false;
}
if( get_option('aws_doc_enable_documents') == "yes" )
add_filter( 'cp_ad_details_cp_documents_url', 'aws_doc_remove_documents_details' );