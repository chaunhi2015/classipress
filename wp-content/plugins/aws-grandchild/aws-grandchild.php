<?php
/*
Plugin Name: AWS Grandchild
Description: Grandchild theme for ClassiPress child themes.
Plugin URI: http://www.arctic-websolutions.com
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com
Version: 1.0.3
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;


// Define the root directory path of this plugin
if ( ! defined( 'AWS_GC_PLUGIN_DIR' ) ) {
	define( 'AWS_GC_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

	// Load the custom functions
	include_once( AWS_GC_PLUGIN_DIR .'/includes/functions.php' );

// Enqueue the custom css file
function awsolutions_grandchild_styles() {
	wp_register_style( 'aws-grandchild-style', plugins_url( '/styles/styles.css', __FILE__ ), array(), '1.0' );
	wp_enqueue_style( 'aws-grandchild-style' );
}
add_action( 'wp_head', 'awsolutions_grandchild_styles', 500 );


// Enqueue the custom script file
function awsolutions_grandchild_scripts() {
	wp_register_script( 'aws-grandchild-script', plugins_url( '/js/scripts.js', __FILE__ ), array( 'jquery' ), '1.0' );
	wp_enqueue_script( 'aws-grandchild-script' );
}
add_action( 'wp_head', 'awsolutions_grandchild_scripts', 99 );


// If custom templates exists load them from 'templates' dir.
function awsolutions_grandchild_template_loader( $template ) {

	if ( file_exists( untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . basename( $template ) ) )
		$template = untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' . basename( $template );

	return $template;
}
add_filter( 'template_include', 'awsolutions_grandchild_template_loader', 11 );


// AWSolutions Get Template Class.
class AWS_GrandChildTemplater {

	// A Unique Identifier
	protected $plugin_slug;

	// A reference to an instance of this class.
        private static $instance;

	// The array of templates that this plugin tracks.
        protected $templates;

	// Returns an instance of this class.
        public static function get_instance() {

                if( null == self::$instance ) {
                        self::$instance = new AWS_GrandChildTemplater();
                } 
                	return self::$instance;
        }

	// Initializes the plugin by setting filters and administration functions.
        private function __construct() {

                $this->templates = array();

                // Add a filter to the attributes metabox to inject template into the cache.
                add_filter( 'page_attributes_dropdown_pages_args', array( $this, 'register_project_templates' ) );

                // Add a filter to the save post to inject out template into the page cache
                add_filter( 'wp_insert_post_data', array( $this, 'register_project_templates' ) );

                // Add a filter to the template include to determine if the page has our template assigned and return it's path
                add_filter( 'template_include', array( $this, 'view_project_template') );

                // Add your templates to this array.
                $this->templates = array(
                	'loop-ad_listing.php' => 'Your Template Name',
                	'tpl-your-template2.php' => 'Your Template2 Name'
                );
        } 


	// Adds our template to the pages cache in order to trick WordPress into thinking the template file exists where it doens't really exist.
        public function register_project_templates( $atts ) {

                // Create the key used for the themes cache
                $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

                // Retrieve the cache list. If it doesn't exist, or it's empty prepare an array
                $templates = wp_get_theme()->get_page_templates();
                if ( empty( $templates ) ) {
                        $templates = array();
                }

                // New cache, therefore remove the old one
                wp_cache_delete( $cache_key , 'themes');

                // Now add our template to the list of templates by merging our templates with the existing templates array from the cache.
                $templates = array_merge( $templates, $this->templates );

                // Add the modified cache to allow WordPress to pick it up for listing available templates
                wp_cache_add( $cache_key, $templates, 'themes', 1800 );

                return $atts;

        }

	// Checks if the template is assigned to the page
        public function view_project_template( $template ) {

                global $post;

                if ( $post && !isset( $this->templates[get_post_meta( $post->ID, '_wp_page_template', true )] ) ) {

                        return $template;

                }

                if ( $post ) $file = plugin_dir_path(__FILE__). '/templates/'. get_post_meta( $post->ID, '_wp_page_template', true );

                // Just to be safe, we check if the file exist first
                if( $post && file_exists( $file ) ) {
                        return $file;
                }

                return $template;

        } 

}
add_action( 'plugins_loaded', array( 'AWS_GrandChildTemplater', 'get_instance' ) );