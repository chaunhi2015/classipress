Plugin Name: AWS Grandchild
Description: Grandchild theme for ClassiPress child themes.
Plugin URI: http://www.arctic-websolutions.com
Author: Rune Kristoffersen
Author URI: http://www.arctic-websolutions.com
Version: 1.0.3

---------------------------------------------------------------------------------------------------

=== Installation ===
 - Download ClassiPress AWS Grandchild.
 - Extract the downloaded .zip file. (Not necessary if filename dosen`t say so, ie. UNZIP)
 - Login to your websites FTP and navigate to the "wp-contents/plugins/" folder.
 - Upload the resulting "aws-grandchild.zip" or "aws-grandchild-X.X.X.zip" folder to the websites plugin folder.
 - Go to your websites Dashboard, access the "plugins" section on the left hand menu.
 - Locate "AWS Grandchild" in the plugin list and click "activate".

OR you can use WP plugin installer, just extract the downloaded .zip file and upload the "aws-grandchild-X.X.X.zip".

IMPORTANT: To avoid your customizations beeing overwritten when an update is available you should only upload and replace the changed file(s) as stated in the change-log.txt

That’s it!

=== How to use the grandchild plugin ===
The plugin works as any child theme, that means if you want to customize any of you child theme template files just copy
the file into the "templates" directory of the AWS Grandchild plugin, and do your changes there.

If you want to change any of the CSS used by your child theme, classipress or any other plugin just copy the orginal CSS
code and paste it into the styles.css file located in the "styles" directory of the AWS Grandchild plugin, and do your changes there.

There is also a custom JS script file located in the "js" directory, here you can add any jquery/javascript needed for your customizations.

For any custom functions use the functions.php file located in the "includes" directory, "/plugins/aws-grandchild/includes/functions.php"
just add your functions below the existing content.

If you have questions please post them in our forum, http://www.arctic-websolutions.com/forums OR check out our FAQ page, http://www.arctic-websolutions.com/grandchild-plugin-faq/